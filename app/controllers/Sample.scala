package controllers

import com.google.inject.Inject
import play.api.i18n.Lang
import play.api.mvc.{AbstractController, ControllerComponents}
import play.api.mvc
import play.api.mvc.AnyContent

class Sample @Inject() (cc: ControllerComponents)
    extends AbstractController(cc) {

  val lang: Lang = Lang("fr")

  def index: mvc.Action[AnyContent] =
    Action {
      Ok(views.html.sample.sample("XiVOxc API Sample Page"))
    }
}
