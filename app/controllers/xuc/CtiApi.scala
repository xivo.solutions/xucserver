package controllers.xuc

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.{AuthenticatedAction, RequestResultHelper}
import services.config.{ConfigRepository, ConfigServerRequester}
import play.api.Logger
import play.api.mvc.*
import xivo.xuc.XucBaseConfig
import play.api.libs.json.*

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import models.{GetLineConfigInUse, LineConfig, XivoUser, XucUser}
import org.apache.pekko.pattern.ask
import org.apache.pekko.util.Timeout
import services.{ActorIds, GetRouter, Router}
import services.config.ConfigDispatcher.LineConfigQueryById

import scala.concurrent.ExecutionContext.Implicits.global

class CtiApi @Inject() (
    cfgRepo: ConfigRepository,
    config: XucBaseConfig,
    parsers: PlayBodyParsers,
    @Named(ActorIds.CtiRouterFactoryId) ctiRouterFactory: ActorRef,
    cc: ControllerComponents
)(implicit system: ActorSystem)
    extends AbstractController(cc)
    with RequestResultHelper {
  override val log: Logger                           = Logger(getClass.getName)
  val serviceName                                    = "CtiApi"
  implicit val contentParser: BodyParser[AnyContent] = parsers.anyContent
  implicit val configRepository: ConfigRepository    = cfgRepo
  implicit val timeout: Timeout                      = 5.seconds

  def getAgentConfig: Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async {
      implicit request =>
        {
          request.user.fold(
            xivoUser => {
              cfgRepo.getCtiUser(xivoUser.username.getOrElse("")) match {
                case Some(ctiUser) =>
                  val maybeAgent =
                    ctiUser.agentId.flatMap(cfgRepo.getAgent).map(Json.toJson)
                  Future[Result](
                    Ok(Json.toJson(SuccessReturn(data = maybeAgent)))
                  )
                case None =>
                  Future[Result](Ok(Json.toJson(SuccessReturn())))
              }
            },
            * => {
              Future[Result](Ok(Json.toJson(SuccessReturn())))
            }
          )
        }
    }

  def getAgentLineConfig: Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async {
      implicit request =>
        {
          request.user.fold(
            xivoUser => {
              for {
                router <- (ctiRouterFactory ? GetRouter(
                  XucUser(xivoUser.username.getOrElse(""), xivoUser)
                )).mapTo[Router]
                maybeLineConfig <- (router.ref ? GetLineConfigInUse)
                  .mapTo[Option[LineConfig]]
              } yield maybeLineConfig match
                case Some(lineConfig) =>
                  val lineConfigJson = Json.toJson(lineConfig)
                  Ok(Json.toJson(SuccessReturn(data = Some(lineConfigJson))))
                case None => Ok(Json.toJson(SuccessReturn()))
            },
            _ => Future.successful(Ok(Json.toJson(SuccessReturn())))
          )
        }
    }

  def getLineConfig: Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async {
      implicit request =>
        {
          request.user.fold(
            xivoUser => {
              cfgRepo.getLineConfig(
                LineConfigQueryById(xivoUser.id.toInt)
              ) match {
                case Some(lineConfig) =>
                  Future[Result](
                    Ok(
                      Json.toJson(
                        SuccessReturn(data = Some(Json.toJson(lineConfig)))
                      )
                    )
                  )
                case None =>
                  Future[Result](Ok(Json.toJson(SuccessReturn())))
              }
            },
            * => {
              Future[Result](Ok(Json.toJson(SuccessReturn())))
            }
          )
        }
    }

  def getAgentQueueMembers(): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async {
      implicit request =>
        {
          request.user.fold(
            xivoUser => {
              cfgRepo.getCtiUser(xivoUser.username.getOrElse("")) match {
                case Some(ctiUser) =>
                  val queues = cfgRepo
                    .getAgentQueueMembers()
                    .filter(item =>
                      item.agentId.equals(ctiUser.agentId.getOrElse(0))
                    )
                    .map(Json.toJson)

                  Future[Result](
                    Ok(
                      Json.toJson(
                        SuccessReturn(data = Some(Json.toJson(queues)))
                      )
                    )
                  )
                case None =>
                  Future[Result](Ok(Json.toJson(SuccessReturn())))
              }
            },
            * => {
              Future[Result](Ok(Json.toJson(SuccessReturn())))
            }
          )
        }
    }
}
