package controllers.xuc

import com.google.inject.Inject
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents}
import xivo.xuc.XucBaseConfig
import xivo.xucstats.XucBaseStatsConfig

import scala.concurrent.ExecutionContext
import play.api.mvc

class Config @Inject() (
    xucConfig: XucBaseConfig,
    xucStatsConfig: XucBaseStatsConfig,
    cc: ControllerComponents
)(implicit ec: ExecutionContext)
    extends AbstractController(cc) {

  def getStatsConfig(path: String): mvc.Action[AnyContent] =
    Action {
      xucStatsConfig.getValue(path) match {
        case Some(c) => Ok(Json.toJson(c))
        case None    => NotFound
      }
    }
}
