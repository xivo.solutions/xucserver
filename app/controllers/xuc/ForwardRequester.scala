package controllers.xuc

import play.api.Logger
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc._
import play.api.libs.ws.WSBodyWritables.writeableOf_JsValue
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._
import play.api.libs.json.JsValue

object ForwardRequester {
  val log: Logger               = Logger(getClass.getName)
  val WsTimeout: FiniteDuration = 2000.millis
  val Xucapi                    = "xuc/api/1.0"
  val headers: List[(String, String)] = List(
    ("Content-Type", "application/json"),
    ("Accept", "application/json"),
    ("Peer", "xuc")
  )

  def isFromXuc(implicit request: RequestHeader): Boolean =
    request.headers.get("Peer") == Some("xuc")

  def requestBodyAsJson[A](implicit request: Request[A]): JsValue =
    request.body match {
      case AnyContentAsJson(json) => json
      case _                      => Json.obj()
    }

  def forwardRequest[A](
      domain: String,
      username: String,
      action: String,
      xucPeers: List[String]
  )(implicit request: Request[A], ws: WSClient): Future[Result] = {
    log.debug(
      s"forwarding request $request ${request.headers} ${request.body.getClass} $xucPeers"
    )

    def buildWs(peer: String) = {
      ws.url(s"http://$peer/$Xucapi/$action/$domain/$username/")
        .withHttpHeaders(headers*)
        .withRequestTimeout(WsTimeout)
        .post(requestBodyAsJson)
    }

    if (!isFromXuc) {
      log.debug(s"request $request ${request.headers}")
      val forwardWSs = xucPeers.map(peer => buildWs(peer))
      Future
        .sequence(forwardWSs)
        .map { success =>
          log.info(s"request forwarded on user not found ")
          Results.Accepted(
            Json.obj(
              "message" -> "request forwarded on user not found",
              "peers"   -> xucPeers
            )
          )
        }
        .recover { case ex =>
          log.error(s"unable to forward request $ex")
          Results.BadRequest(s"unable to forward request $ex")
        }
    } else {
      log.debug("request already coming from another xuc")
      Future(Results.NotFound("From xuc user not found"))
    }
  }

}
