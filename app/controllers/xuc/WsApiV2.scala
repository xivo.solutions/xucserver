package controllers.xuc

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.{AuthenticatedAction, RequestResultHelper}
import controllers.helpers.AuthenticatedAction.getApiUser
import services.config.{
  ConfigRepository,
  ConfigServerRequester,
  ExportTicketsCsv,
  ImportCsvCallback
}
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, JsValue}
import play.api.mvc.*
import services.request.{
  BaseRequest,
  DialFromQueue,
  DialToQueue,
  DndReq,
  HistoryDays,
  HistorySize
}
import services.{ActorIds, ActorIdsFactory}
import xivo.xuc.XucBaseConfig
import play.api.libs.json.*

import scala.concurrent.{ExecutionContext, Future}
import org.apache.pekko.actor.ActorSelection
import org.apache.pekko.pattern.AskTimeoutException
import models.XivoUserDao
import models.ws.{ErrorResult, ErrorType, JsonParsingError}
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import services.request.PhoneRequest.{Dial, DialByUsername}
import xivo.models.{PartialUserServices, UserForward}
import xivo.xuc.api.Requester

import scala.concurrent.ExecutionContext.Implicits.global

case class SuccessReturn(res: String = "success", data: Option[JsValue] = None)
object SuccessReturn {
  implicit val writes: Writes[SuccessReturn] = (
    (JsPath \ "res").write[String] and
      (JsPath \ "data").writeNullable[JsValue]
  )(o => (o.res, o.data))
}

trait WebserviceApiV2ErrorType extends ErrorType
case object JsonBodyNotFound extends WebserviceApiV2ErrorType {
  val error = "JsonBodyNotFound"
}
case object JsonErrorDecoding extends WebserviceApiV2ErrorType {
  val error = "JsonErrorDecoding"
}

case object UserNotFound extends WebserviceApiV2ErrorType {
  val error = "UserNotFound"
}

case object ExtensionNotFound extends WebserviceApiV2ErrorType {
  val error = "ExtensionNotFound"
}

case object HistoryExceptionError extends WebserviceApiV2ErrorType {
  val error = "HistoryExceptionError"
}
case class WebserviceApiV2Error(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case JsonBodyNotFound      => Results.BadRequest(body)
      case JsonErrorDecoding     => Results.BadRequest(body)
      case UserNotFound          => Results.BadRequest(body)
      case ExtensionNotFound     => Results.BadRequest(body)
      case HistoryExceptionError => Results.InternalServerError(body)
      case _                     => Results.InternalServerError(body)
    }
  }
}
object WebserviceApiV2Error {
  implicit val WebserviceApiV2Writes: Writes[WebserviceApiV2Error] =
    ((JsPath \ "error").write[ErrorType] and
      (JsPath \ "message").write[String])(o => (o.error, o.message))
}

class WsApiV2 @Inject() (
    ec: ExecutionContext,
    cfgRepo: ConfigRepository,
    @Named(ActorIds.QueueDispatcherId) queueDispatcher: ActorRef,
    @Named(ActorIds.CtiRouterFactoryId) ctiRouterFactory: ActorRef,
    @Named(ActorIds.CallHistoryManagerId) callHistoryManager: ActorRef,
    @Named(ActorIds.CallbackMgrInterfaceId) callbackManager: ActorRef,
    config: XucBaseConfig,
    parsers: PlayBodyParsers,
    actorIdFactory: ActorIdsFactory,
    configServerRequester: ConfigServerRequester,
    xivoUserDao: XivoUserDao,
    cc: ControllerComponents
)(implicit system: ActorSystem)
    extends AbstractController(cc)
    with RequestResultHelper {
  override val log: Logger                           = Logger(getClass.getName)
  val serviceName                                    = "WsApiV2"
  implicit val contentParser: BodyParser[AnyContent] = parsers.anyContent
  implicit val configRepository: ConfigRepository    = cfgRepo

  def logRequest(
      username: Option[String],
      requestAction: String,
      domain: String = "default"
  ): Unit = {
    log.info(
      s"""[${username.getOrElse(
          "unknown"
        )} @ domain $domain] Req: <$serviceName - $requestAction>"""
    )
  }

  private def getQueueDispatcherActor: ActorSelection = {
    system.actorSelection(
      actorIdFactory.queueDispatcherPath(queueDispatcher)
    )
  }

  private def setForward(
      username: String,
      partialUserServices: PartialUserServices
  ) = {
    xivoUserDao
      .getCtiUserByLogin(username)
      .map(user => {
        configServerRequester.setUserServices(
          user.id,
          partialUserServices
        )
        Ok(Json.toJson(SuccessReturn()))
      })
      .recoverWith({ case _ =>
        Future[Result](
          WebserviceApiV2Error(
            UserNotFound,
            s"Error setting forward status for user $username"
          ).toResult
        )
      })
  }

  def dialToQueue(): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      request =>
        DialToQueue.validate(request.body) match {
          case JsSuccess(dial, _) =>
            val user = getApiUser(request.user)
            logRequest(user.username, "dialToqueue", dial.domain)
            getQueueDispatcherActor ! dial
            Future[Result](Ok(Json.toJson(SuccessReturn())))
          case _ =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def dialNumber(username: String): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      request =>
        Dial.validate(request.body) match {
          case JsSuccess(dial, _) =>
            val user = getApiUser(request.user)
            logRequest(user.username, s"dial", dial.domain)
            system.actorSelection(
              actorIdFactory.ctiRouterPath(ctiRouterFactory, username)
            ) ! BaseRequest(system.deadLetters, dial)
            Future[Result](Ok(Json.toJson(SuccessReturn())))
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def dialUser(username: String): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      implicit request =>
        DialByUsername.validate(request.body) match {
          case JsSuccess(dialByUsername, _) =>
            val user = getApiUser(request.user)
            logRequest(
              user.username,
              s"dialByUsername",
              dialByUsername.domain
            )
            configRepository.phoneNumberForUser(
              dialByUsername.username
            ) match {
              case Some(number) =>
                system.actorSelection(
                  actorIdFactory.ctiRouterPath(ctiRouterFactory, username)
                ) ! BaseRequest(
                  system.deadLetters,
                  Dial(
                    number,
                    dialByUsername.variables,
                    dialByUsername.domain
                  )
                )
                Future[Result](Ok(Json.toJson(SuccessReturn())))
              case None =>
                Future(
                  WebserviceApiV2Error(
                    ExtensionNotFound,
                    s"Phone number for ${dialByUsername.username} not found"
                  ).toResult
                )
            }
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def dialFromQueue(username: String): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      implicit request =>
        DialFromQueue.validate(request.body) match {
          case JsSuccess(dialFromQueue, _) =>
            val user = getApiUser(request.user)
            logRequest(
              user.username,
              s"dialFromQueue",
              dialFromQueue.domain
            )
            getQueueDispatcherActor ! dialFromQueue
            Future[Result](Ok(Json.toJson(SuccessReturn())))
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def getUserCallHistory(
      username: String,
      size: Int
  ): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async {
      implicit request =>
        val user = getApiUser(request.user)
        logRequest(user.username, s"getUserCallHistory")
        Requester
          .richCallHistory(
            username,
            HistorySize(size),
            cfgRepo,
            callHistoryManager
          )
          .map(history => {
            val jsHistory = Json.toJson(history)
            Ok(Json.toJson(SuccessReturn(data = Some(jsHistory))))
          })
          .recover({
            case _: AskTimeoutException =>
              WebserviceApiV2Error(
                HistoryExceptionError,
                s"Error getting call history for $username: timed out waiting to find user"
              ).toResult

            case e: Exception =>
              WebserviceApiV2Error(
                HistoryExceptionError,
                s"Error getting call history for $username: $e"
              ).toResult
          })
    }

  def getUserCallHistoryByDays(
      username: String,
      days: Int
  ): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async {
      implicit request =>
        val user = getApiUser(request.user)
        logRequest(user.username, s"getUserCallHistoryByDays")
        Requester
          .richCallHistoryByDays(
            username,
            HistoryDays(days),
            cfgRepo,
            callHistoryManager
          )
          .map(history => {
            val jsHistory = Json.toJson(history)
            Ok(Json.toJson(SuccessReturn(data = Some(jsHistory))))
          })
          .recover({
            case _: AskTimeoutException =>
              WebserviceApiV2Error(
                HistoryExceptionError,
                s"Error getting call history for $username: timed out waiting to find user"
              ).toResult

            case e: Exception =>
              WebserviceApiV2Error(
                HistoryExceptionError,
                s"Error getting call history by days for $username: $e"
              ).toResult
          })
    }

  def dnd(username: String): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      implicit request =>
        DndReq.validate(request.body) match {
          case JsSuccess(dndRequest, _) =>
            val user = getApiUser(request.user)
            logRequest(
              user.username,
              s"dnd",
              dndRequest.domain
            )
            setForward(
              username,
              PartialUserServices(
                Some(dndRequest.state),
                None,
                None,
                None
              )
            )
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def uncForward(username: String): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      implicit request =>
        UserForward.validate(request.body) match {
          case JsSuccess(userForward, _) =>
            val user = getApiUser(request.user)
            logRequest(
              user.username,
              s"uncFoward",
              userForward.domain
            )
            setForward(
              username,
              PartialUserServices(
                None,
                None,
                None,
                Some(
                  UserForward(userForward.enabled, userForward.destination)
                )
              )
            )
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def naForward(username: String): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      implicit request =>
        UserForward.validate(request.body) match {
          case JsSuccess(userForward, _) =>
            val user = getApiUser(request.user)
            logRequest(
              user.username,
              s"naForward",
              userForward.domain
            )
            setForward(
              username,
              PartialUserServices(
                None,
                None,
                Some(
                  UserForward(userForward.enabled, userForward.destination)
                ),
                None
              )
            )
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def busyForward(username: String): Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      implicit request =>
        UserForward.validate(request.body) match {
          case JsSuccess(userForward, _) =>
            val user = getApiUser(request.user)
            logRequest(
              user.username,
              s"busyForward",
              userForward.domain
            )
            setForward(
              username,
              PartialUserServices(
                None,
                Some(
                  UserForward(userForward.enabled, userForward.destination)
                ),
                None,
                None
              )
            )
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def importRequests: Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      implicit request =>
        ImportCsvCallback.validate(request.body) match {
          case JsSuccess(importCsvCallback, _) =>
            val user = getApiUser(request.user)
            logRequest(user.username, s"importCsvCallbackRequests")
            Requester.importCsvCallback(
              callbackManager,
              importCsvCallback.listUuid,
              importCsvCallback.csv
            )
            Future.successful(Ok(Json.toJson(SuccessReturn())))
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }

  def exportTickets: Action[JsValue] =
    AuthenticatedAction(config.Authentication.secret).async(parsers.json) {
      implicit request =>
        ExportTicketsCsv.validate(request.body) match {
          case JsSuccess(exportTicketsCsv, _) =>
            val user = getApiUser(request.user)
            logRequest(user.username, s"exportTicketsCsv")
            processResult(
              Requester.exportTicketsCsv(
                callbackManager,
                exportTicketsCsv.listUuid
              )
            ).map(
              _.withHeaders(
                "Access-Control-Allow-Origin" -> "*",
                "Content-Type"                -> "text/csv",
                "Content-Disposition"         -> s"attachment;filename=${exportTicketsCsv.listUuid}.csv"
              )
            )
          case JsError(_) =>
            Future[Result](
              WebserviceApiV2Error(JsonParsingError, "Invalid JSON").toResult
            )
        }
    }
}
