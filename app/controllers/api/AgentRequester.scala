package controllers.api

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.pattern.ask
import org.apache.pekko.util.Timeout
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.{RequestResult, RequestSuccess}
import services.agent.{LoginProcessor, LogoutAgent, LogoutProcessor}
import services.config.ConfigRepository
import services.request.{AgentLoginRequest, AgentTogglePause, BaseRequest}
import services.{ActorIds, XucEventBus}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration.DurationInt

class AgentRequester @Inject() (
    configRepo: ConfigRepository,
    eventBus: XucEventBus,
    @Named(ActorIds.ConfigManagerId) configManager: ActorRef
)(implicit system: ActorSystem, ec: ExecutionContext) {

  implicit val timeout: Timeout = Timeout(3.seconds)

  def logout(phoneNumber: String): Future[RequestResult] = {
    val processor =
      system.actorOf(LogoutProcessor.props(eventBus, configManager))

    val result = processor ? LogoutAgent(phoneNumber)

    result.mapTo[RequestResult]
  }

  def login(agentLoginReq: AgentLoginRequest): Future[RequestResult] = {
    val processor =
      system.actorOf(LoginProcessor.props(eventBus, configManager))

    val result = processor ? agentLoginReq

    result.mapTo[RequestResult]
  }

  def togglePause(phoneNumber: String): Future[RequestResult] = {
    configManager ! BaseRequest(
      system.deadLetters,
      AgentTogglePause(phoneNumber)
    )
    Future(RequestSuccess(s"toggle Pause on $phoneNumber"))
  }

}
