package controllers.config

import com.google.inject.Inject
import controllers.helpers.AuthenticatedAction
import controllers.helpers.AuthenticatedAction.getApiUser
import play.api.Logger
import play.api.mvc._
import play.api.http.HttpEntity
import services.config.{ConfigRepository, ConfigServerRequester}
import scala.concurrent.ExecutionContext
import xivo.xuc.XucBaseConfig

class ForwardConfigController @Inject() (implicit
    configRequester: ConfigServerRequester,
    repo: ConfigRepository,
    config: XucBaseConfig,
    ec: ExecutionContext,
    parsers: PlayBodyParsers,
    cc: ControllerComponents
) extends AbstractController(cc) {
  val log: Logger                                    = Logger(getClass.getName)
  implicit val contentParser: BodyParser[AnyContent] = parsers.anyContent

  def forward(
      uri: String,
      apiVersion: String = "1.0"
  ): Action[AnyContent] =
    AuthenticatedAction(config.Authentication.secret).async { request =>
      val user = getApiUser(request.user)
      log.debug(
        s"forwarding request $uri to configmgt for ${user.username} with apiVersion $apiVersion and body : ${request.body}"
      )

      configRequester
        .forward(
          uri,
          request.method,
          request.body.asJson,
          apiVersion,
          user.username
        )
        .map { response =>
          val headers = response.headers.map { h =>
            (h._1, h._2.head)
          }
          Result(
            ResponseHeader(response.status, headers),
            HttpEntity.Strict(response.bodyAsBytes, None)
          )
        }
    }
}
