package controllers.security

import java.security.PrivilegedExceptionAction

import com.google.inject.{Inject, Singleton}
import javax.security.auth.Subject
import javax.security.auth.kerberos.KerberosPrincipal
import javax.security.auth.login.{LoginContext, LoginException}
import org.apache.xerces.impl.dv.util.Base64
import org.ietf.jgss._
import org.slf4j.LoggerFactory
import play.api.mvc._
import xivo.xuc.KerberosSsoConfig

import scala.jdk.CollectionConverters._
import org.slf4j.Logger

@Singleton
class KerberosAuthentication @Inject() (config: KerberosSsoConfig)
    extends Results {
  val securedLoginConfiguration      = new SecuredLoginConfiguration(config)
  val serverSubject: Option[Subject] = performServerLogin
  val spnegoOid                      = new Oid("1.3.6.1.5.5.2")
  val gssManager: GSSManager         = GSSManager.getInstance
  val logger: Logger                 = LoggerFactory.getLogger(getClass)

  private def performServerLogin: Option[Subject] = {
    if (
      securedLoginConfiguration
        .getPrincipal() == null || securedLoginConfiguration
        .getPrincipal()
        .length == 0
    ) {
      None
    } else {
      val principals = Set(
        new KerberosPrincipal(securedLoginConfiguration.getPrincipal())
      )
      val subject: Subject = new Subject(
        false,
        principals.asJava,
        Set[AnyRef]().asJava,
        Set[AnyRef]().asJava
      )
      val loginContext =
        new LoginContext("", subject, null, securedLoginConfiguration)
      try {
        loginContext.login()
        Some(loginContext.getSubject)
      } catch {
        case e: LoginException =>
          logger.error("Error during server Kerberos login", e)
          None
      }
    }
  }

  def performKerberosAuthentication(request: RequestHeader): Option[String] =
    request.headers.get("Authorization") match {
      case None => None
      case Some(headerValue) =>
        try {
          logger.debug("Header exists : perform login")
          performClientLogin(headerValue)
        } catch {
          case e: Exception =>
            logger.error(
              "Received exception when performing the Kerberos authentication",
              e
            )
            None
        }
    }

  private def performClientLogin(
      authorizationString: String
  ): Option[String] = {
    createClientContext(authorizationString) match {
      case Some(context) =>
        if (context.isEstablished) {
          logger.debug(
            "performClientLogin confState={}, integState={}, src={}, target={}",
            context.getConfState.toString,
            context.getIntegState.toString,
            Option(context.getSrcName).map(_.toString).getOrElse(""),
            Option(context.getTargName).map(_.toString).getOrElse("")
          )
          Some(context.getSrcName.toString.toLowerCase)
        } else None
      case None => None
    }
  }

  private def createClientContext(
      authorizationString: String
  ): Option[GSSContext] =
    serverSubject match {
      case Some(subject) =>
        Some(
          Subject.doAs(
            subject,
            new PrivilegedExceptionAction[GSSContext] {
              def run: GSSContext = {
                val serverCreds: GSSCredential = gssManager.createCredential(
                  null,
                  GSSCredential.DEFAULT_LIFETIME,
                  spnegoOid,
                  GSSCredential.ACCEPT_ONLY
                )
                val authorization = authorizationString.substring(
                  authorizationString.indexOf(" ") + 1
                )
                val authorizationBytes: Array[Byte] =
                  Base64.decode(authorization)
                val context: GSSContext = gssManager.createContext(serverCreds)
                context.acceptSecContext(
                  authorizationBytes,
                  0,
                  authorizationBytes.length
                )
                context
              }
            }
          )
        )
      case None => None
    }

}
