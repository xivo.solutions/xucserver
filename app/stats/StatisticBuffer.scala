package stats

import services.XucStatsEventBus.{
  AggregatedStatEvent,
  Stat,
  StatEvent,
  StatUpdate
}
import xivo.models.XivoObject.ObjectDefinition

import scala.collection.immutable.HashMap

class StatisticBuffer {

  type StatRepo = HashMap[ObjectDefinition, HashMap[String, Double]]

  var statBuffer: StatRepo = new StatRepo

  def isEmpty: Boolean = statBuffer.isEmpty

  def aggregate: List[AggregatedStatEvent] = {
    val aggs = statBuffer map { case (objectDefinition, statValues) =>
      val stats = statValues.map { case (statName, statValue) =>
        Stat(statName, statValue)
      }
      AggregatedStatEvent(objectDefinition, stats.toList)
    }
    aggs.toList
  }

  def addStat(statUpdate: StatEvent): StatisticBuffer = {
    statBuffer = addAll(statBuffer, statUpdate)
    this
  }

  private def addAll(statRepo: StatRepo, statUpdate: StatEvent): StatRepo = {
    statUpdate.stat match {
      case Nil => statRepo
      case x :: xs =>
        addAll(
          add(statRepo, statUpdate.xivoObject, Stat(x.statRef, x.value)),
          StatUpdate(statUpdate.xivoObject, xs)
        )
    }
  }

  private def add(
      buffer: StatRepo,
      objDef: ObjectDefinition,
      stat: Stat
  ): StatRepo = {
    buffer.get(objDef) match {
      case None =>
        addStatForNewObject(buffer, objDef, stat)
      case Some(list) =>
        buffer(objDef).get(stat.statRef) match {
          case None =>
            addStatToExistingObject(buffer, objDef, stat)
          case Some(d: Double) =>
            updateStatOfExistingObject(buffer, objDef, stat)
        }
    }
  }

  private def addStatForNewObject(
      buffer: StatRepo,
      objDef: ObjectDefinition,
      stat: Stat
  ): StatRepo = {
    buffer + (objDef -> HashMap[String, Double](stat.statRef -> stat.value))
  }

  private def addStatToExistingObject(
      buffer: StatRepo,
      objDef: ObjectDefinition,
      stat: Stat
  ): StatRepo = {
    buffer - objDef + (objDef ->
      (buffer(objDef) ++ HashMap[String, Double](stat.statRef -> stat.value)))
  }

  private def updateStatOfExistingObject(
      buffer: StatRepo,
      objDef: ObjectDefinition,
      stat: Stat
  ): StatRepo = {
    buffer - objDef +
      (objDef ->
        (buffer(objDef) - stat.statRef
          ++ HashMap[String, Double](stat.statRef -> stat.value)))
  }

}
