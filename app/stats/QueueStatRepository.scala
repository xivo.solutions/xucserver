package stats

import org.apache.pekko.actor.ActorRef
import org.apache.pekko.actor.ActorContext
import com.google.inject.Inject
import play.api.libs.concurrent.InjectedActorSupport

class QueueStatRepository @Inject() (
    queueStatManagerFactory: QueueStatManager.Factory
) extends InjectedActorSupport {
  protected[stats] var repo: Map[Int, ActorRef] = Map()

  def getQueueStat(queueId: Int): Option[ActorRef] = repo.get(queueId)
  def getAllStats: List[ActorRef]                  = repo.map(_._2).toList

  def createQueueStat(queueId: Int, context: ActorContext): ActorRef = {
    val actorRef = injectedChild(
      queueStatManagerFactory(queueId),
      s"${queueId}_QueueStatManager"
    )(context)
    repo += (queueId -> actorRef)
    actorRef
  }
}
