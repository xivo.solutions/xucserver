package stats

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef, Props}
import com.codahale.metrics.SharedMetricRegistries
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import com.google.inject.name.Named
import play.api.inject.Injector
import services.ActorIds
import stats.Statistic.{PublishSlidingStats, ResetStat, StatCalculator}
import us.theatr.pekko.quartz.AddCronSchedule
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType._
import xivo.xuc.XucConfig
import xivo.xucstats.XucBaseStatsConfig

object QueueStatManager {
  trait Factory {
    def apply(queueId: Int): Actor
  }
}

class QueueStatManager @Inject() (
    @Assisted queueId: Int,
    injector: Injector,
    @Named(ActorIds.QuartzActorId) scheduler: ActorRef,
    config: XucConfig,
    statConfig: XucBaseStatsConfig
) extends Actor
    with ActorLogging {

  private val metricRegistries =
    SharedMetricRegistries.getOrCreate(config.metricsRegistryName)

  val PublishSlidingSchedule = "0 * * * * ?"

  val objDef: ObjectDefinition = ObjectDefinition(Queue, Some(queueId))

  val aggregator: ActorRef = context.actorOf(
    Props(injector.instanceOf[StatsAggregator]),
    s"StatsAggregator-$queueId"
  )

  override def preStart(): Unit = {
    log.info(s"setting schedule to ${statConfig.resetSchedule}")
    scheduler ! AddCronSchedule(self, statConfig.resetSchedule, ResetStat)
    scheduler ! AddCronSchedule(
      self,
      PublishSlidingSchedule,
      PublishSlidingStats
    )
  }

  def start(ref: String, calc: StatCalculator): ActorRef =
    context.actorOf(
      Props(
        new Statistic(ref, objDef, calc, aggregator, metricRegistries)
          with HistoSize
      )
    )

  def startHistoValue(ref: String, calc: StatCalculator): ActorRef =
    context.actorOf(
      Props(
        new Statistic(ref, objDef, calc, aggregator, metricRegistries)
          with HistoValue
      )
    )

  val stats: List[ActorRef] =
    List(
      start("TotalNumberCallsEntered", Statistic.TotalNumberCallsEntered),
      start("TotalNumberCallsAbandonned", Statistic.TotalNumberCallsAbandonned),
      start("TotalNumberCallsAnswered", Statistic.TotalNumberCallsAnswered),
      start("TotalNumberCallsClosed", Statistic.TotalNumberCallsClosed),
      start("TotalNumberCallsTimeout", Statistic.TotalNumberCallsTimeout),
      start(
        "TotalNumberCallsNotAnswered",
        Statistic.TotalNumberCallsNotAnswered
      ),
      start("PercentageAnsweredTotal", Statistic.TotalNumberPercentageAnswered),
      start(
        "PercentageAbandonnedTotal",
        Statistic.TotalNumberPercentageAbandonned
      )
    ) ++
      statConfig.queuesStatTresholdsInSec.toList.flatMap { interval =>
        log.info(s"Building statistics for interval : $interval")
        List(
          start(
            s"TotalNumberCallsAbandonnedAfter$interval",
            Statistic.TotalNumberCallsAbandonnedAfter(interval)
          ),
          start(
            s"TotalNumberCallsAnsweredBefore$interval",
            Statistic.TotalNumberCallsAnsweredBefore(interval)
          ),
          startHistoValue(
            s"PercentageAnsweredBefore$interval",
            Statistic.RelativeNumberPercentageAnsweredBefore(interval)
          ),
          startHistoValue(
            s"PercentageAbandonnedAfter$interval",
            Statistic.RelativeNumberPercentageAbandonnedAfter(interval)
          )
        )
      }

  def receive: PartialFunction[Any, Unit] = { case event =>
    stats.foreach(_ ! event)
  }
}
