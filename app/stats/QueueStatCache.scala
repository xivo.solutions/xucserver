package stats

import org.apache.pekko.actor.{Actor, ActorLogging}
import com.google.inject.Inject
import services.XucStatsEventBus
import services.XucStatsEventBus.AggregatedStatEvent
import stats.Statistic.RequestStat
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType._

class QueueStatCache @Inject() (statsBus: XucStatsEventBus)
    extends Actor
    with ActorLogging {

  log.info(s"Queue stat cache starting $self")

  override def preStart(): Unit = statsBus.setStatCache(self)

  val statCache = new StatisticBuffer

  statsBus.subscribe(self, ObjectDefinition(Queue))

  def receive: Receive = {

    case stat: AggregatedStatEvent =>
      statCache.addStat(stat)

    case RequestStat(requester, _) =>
      log.debug(s"statistics requested ...$requester")
      statCache.aggregate.foreach(requester ! _)

    case unknown => // log.debug(s"unknown message received $unknown")
  }

}
