package helpers

import org.joda.time._

object DateUtil {
  def secondsSince(dateTime: DateTime): Long =
    Seconds.secondsBetween(dateTime, DateTime.now()).getSeconds
}
