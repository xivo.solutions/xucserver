package models

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class UsersQueueMembership(
    userIds: List[Long],
    membership: List[QueueMembership]
)

object UsersQueueMembership {
  import QueueMembership._

  implicit val usersQueueMembershipWrites: Writes[UsersQueueMembership] = (
    (JsPath \ "userIds").write[List[Long]] and
      (JsPath \ "membership").write[List[QueueMembership]]
  )(o => (o.userIds, o.membership))

  implicit val usersQueueMembershipReads: Reads[UsersQueueMembership] = (
    (JsPath \ "userIds").read[List[Long]] and
      (JsPath \ "membership").read[List[QueueMembership]]
  )(UsersQueueMembership.apply)
}
