package models

import play.api.libs.json.{JsValue, Json, Writes}
import xivo.xucami.models.QueueCall

case class QueueCallList(queueId: Long, calls: List[QueueCall])

object QueueCallList {
  implicit val writes: Writes[QueueCallList] = new Writes[QueueCallList] {
    def writes(queueCalls: QueueCallList): JsValue =
      Json.obj(
        "queueId" -> queueCalls.queueId,
        "calls"   -> queueCalls.calls
      )
  }
}
