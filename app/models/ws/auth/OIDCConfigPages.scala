package models.ws.auth

import play.api.libs.json.{Json, Reads}

sealed trait OIDCConfigPages
case class WellKnown(
    jwks_uri: String,
    authorization_endpoint: String
) extends OIDCConfigPages

case object WellKnown {
  implicit val reads: Reads[WellKnown] = Json.reads[WellKnown]
}
case class JsonWKS(
    kid: String,
    n: String,
    e: String,
    x5c: Array[String]
)
case object JsonWKS {
  implicit val reads: Reads[JsonWKS] = Json.reads[JsonWKS]

}
case class JwksUri(keys: List[JsonWKS]) extends OIDCConfigPages
case object JwksUri {
  implicit val reads: Reads[JwksUri] = Json.reads[JwksUri]
}
