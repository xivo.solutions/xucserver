package models.ws.auth

case class ApiUser(username: Option[String], userId: Long = 0)
