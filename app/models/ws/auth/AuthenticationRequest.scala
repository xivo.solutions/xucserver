package models.ws.auth

import models.ws.auth.AuthType.AuthType
import models.ws.auth.LineType.LineType
import models.ws.auth.SoftwareType.SoftwareType
import models.ws.auth.ApplicationType
import pdi.jwt.{JwtAlgorithm, JwtJson, JwtJsonImplicits}
import play.api.Logger
import play.api.libs.json.*
import play.api.mvc.{Result, Results}
import play.mvc.Http
import xivo.models.Line

import scala.util.{Failure, Success, Try}
import controllers.helpers.AuthenticatedAction
import models.ws.auth
import play.api.libs.functional.syntax.toFunctionalBuilderOps

case class ConnectionType(
    softwareType: SoftwareType,
    applicationType: ApplicationType,
    authType: AuthType,
    lineType: Option[LineType]
)

object AuthType extends Enumeration {
  type AuthType = Value

  val basic: Value    = Value("basic")
  val kerberos: Value = Value("kerberos")
  val cas: Value      = Value("cas")
  val oidc: Value     = Value("oidc")
}

object SoftwareType extends Enumeration {
  implicit val format: Format[SoftwareType] = Json.formatEnum(this)

  type SoftwareType = Value

  val electron: Value   = Value("electron")
  val webBrowser: Value = Value("webBrowser")
  val mobile: Value     = Value("mobile")
  val unknown: Value    = Value("unknown")

  def decode(s: Option[String]): Option[Value] = Try(
    withName(s.getOrElse(unknown.toString))
  ).toOption
}

enum ApplicationType(val value: String):
  case uc          extends ApplicationType("uc")
  case ccagent     extends ApplicationType("ccagent")
  case ccmanager   extends ApplicationType("ccmanager")
  case switchboard extends ApplicationType("switchboard")
  case unknown     extends ApplicationType("unknown")

object ApplicationType:
  implicit val format: Format[ApplicationType] = new Format[ApplicationType]:
    def reads(json: JsValue): JsResult[ApplicationType] =
      json.validate[String].flatMap { s =>
        ApplicationType.values.find(_.value == s) match
          case Some(appType) => JsSuccess(appType)
          case None          => JsError(s"Unknown ApplicationType: $s")
      }

    def writes(appType: ApplicationType): JsValue = JsString(appType.value)

  def decode(s: Option[String]): Option[ApplicationType] =
    s.flatMap(str => ApplicationType.values.find(_.value == str))
      .orElse(Some(unknown))

object AuthUserType extends Enumeration {
  type AuthUserType = Value

  val Cti: Value        = Value("cti")
  val Webservice: Value = Value("webservice")
  val Unknown: Value    = Value("unknown")

  def decode(s: String): Value =
    values.find(_.toString.toLowerCase() == s.toLowerCase()).getOrElse(Unknown)
}

object LineType extends Enumeration {

  implicit val format: Format[LineType] = Json.formatEnum(this)

  type LineType = Value

  val phone: Value  = Value("phone")
  val webRTC: Value = Value("webRTC")
  val ua: Value     = Value("ua")

  def lineToLineType(line: Line): LineType = {
    if (line.ua) LineType.ua
    else if (line.webRTC) LineType.webRTC
    else LineType.phone
  }
}

case class AuthenticationRequest(
    login: String,
    password: String,
    expiration: Option[Int],
    entity: Option[String],
    softwareType: Option[SoftwareType],
    applicationType: Option[ApplicationType]
)

object AuthenticationRequest {
  implicit val format: Format[AuthenticationRequest] =
    ((JsPath \ "login").format[String] and
      (JsPath \ "password").format[String] and
      (JsPath \ "expiration").formatNullable[Int] and
      (JsPath \ "entity").formatNullable[String] and
      (JsPath \ "softwareType").formatNullable[SoftwareType] and
      (JsPath \ "applicationType").formatNullable[ApplicationType])(
      AuthenticationRequest.apply,
      c =>
        (
          c.login,
          c.password,
          c.expiration,
          c.entity,
          c.softwareType,
          c.applicationType
        )
    )

  implicit def enumWrites: Writes[SoftwareType.SoftwareType] =
    (value: SoftwareType.SoftwareType) => JsString(value.toString)
}

sealed trait AuthenticationResult {
  val log: Logger = Logger("AuthenticationResult")
  def toResult: Result
}

object AuthenticationSuccess {
  implicit val format: Format[AuthenticationSuccess] =
    ((JsPath \ "login").format[String] and
      (JsPath \ "token").format[String] and
      (JsPath \ "TTL").format[Long])(
      AuthenticationSuccess.apply,
      c => (c.login, c.token, c.TTL)
    )
}

case class AuthenticationSuccess(login: String, token: String, TTL: Long)
    extends AuthenticationResult {
  def toResult: Result = {
    log.debug("Successful authentication")
    Results.Ok(Json.toJson(this))
  }
}

object AuthenticationError extends Enumeration {
  type AuthenticationError = Value
  val EmptyLogin: Value                  = Value
  val Forbidden: Value                   = Value
  val UserNotFound: Value                = Value
  val InvalidCredentials: Value          = Value
  val InvalidToken: Value                = Value
  val InvalidJson: Value                 = Value
  val BearerNotFound: Value              = Value
  val AuthorizationHeaderNotFound: Value = Value
  val TokenExpired: Value                = Value
  val UnhandledError: Value              = Value
  val SsoAuthenticationFailed: Value     = Value
  val CasServerUrlNotSet: Value          = Value
  val CasServerInvalidResponse: Value    = Value
  val CasServerInvalidParameter: Value   = Value
  val CasServerInvalidRequest: Value     = Value
  val CasServerInvalidTicketSpec: Value  = Value
  val CasServerUnauthorizedServiceProxy: Value =
    Value
  val CasServerInvalidProxyCallback: Value = Value
  val CasServerInvalidTicket: Value        = Value
  val CasServerInvalidService: Value       = Value
  val CasServerInternalError: Value        = Value
  val OidcNotEnabled: Value                = Value
  val OidcInvalidParameter: Value          = Value
  val OidcAuthenticationFailed: Value      = Value
  val OidcKeyNotFound: Value               = Value
  val OidcPubKeyInvalid: Value             = Value
  val WrongMobileSetup: Value              = Value
  val WrongRequester: Value                = Value
  val SignatureInvalid: Value              = Value

  implicit val format: Format[AuthenticationError] =
    new Format[AuthenticationError] {
      def writes(o: AuthenticationError): JsValue = JsString(o.toString)
      def reads(json: JsValue): JsResult[AuthenticationError] =
        JsSuccess(AuthenticationError.withName(json.as[String]))
    }
}

class AuthenticationException(
    val error: AuthenticationError.AuthenticationError,
    message: String
) extends Exception(message)

object AuthenticationFailure {
  implicit val format: Format[AuthenticationFailure] =
    ((JsPath \ "error").format[AuthenticationError.AuthenticationError] and
      (JsPath \ "message")
        .format[String])(
      AuthenticationFailure.apply,
      c => (c.error, c.message)
    )

  def from(t: Throwable): AuthenticationFailure =
    t match {
      case e: AuthenticationException =>
        AuthenticationFailure(e.error, e.getMessage)
      case _ =>
        AuthenticationFailure(
          AuthenticationError.UnhandledError,
          t.getMessage
        )
    }
}

case class AuthenticationFailure(
    error: AuthenticationError.AuthenticationError,
    message: String
) extends AuthenticationResult {
  import AuthenticationError._
  def toResult: Result = {
    log.warn(s"Failed authentication: $this")
    val body = Json.toJson(this)
    error match {
      case BearerNotFound | AuthorizationHeaderNotFound | InvalidJson |
          EmptyLogin | OidcInvalidParameter =>
        Results.BadRequest(body)
      case UnhandledError => Results.InternalServerError(body)
      case SsoAuthenticationFailed =>
        Results
          .Unauthorized(body)
          .withHeaders(Http.HeaderNames.WWW_AUTHENTICATE -> "Negotiate")
      case InvalidToken | InvalidCredentials => Results.Unauthorized(body)
      case _                                 => Results.Forbidden(body)
    }
  }
}

case class AuthenticationInformation(
    login: String,
    expiresAt: Long,
    issuedAt: Long,
    userType: String,
    acls: List[String],
    softwareType: Option[SoftwareType],
    applicationType: Option[ApplicationType]
) {
  def refresh(
      expires: Long,
      acls: List[String]
  ): AuthenticationInformation = {
    val now = AuthenticatedAction.now()
    this.copy(expiresAt = now + expires, issuedAt = now, acls = acls)
  }
  def encode(secret: String): String =
    AuthenticationInformation.encode(this, secret)
  def validate: Try[AuthenticationInformation] =
    if (expiresAt > AuthenticatedAction.now())
      Success(this)
    else
      Failure(
        new AuthenticationException(
          AuthenticationError.TokenExpired,
          "Token expired"
        )
      )

}

object AuthenticationInformation extends JwtJsonImplicits {
  implicit val format: Format[AuthenticationInformation] =
    ((JsPath \ "login").format[String] and
      (JsPath \ "expiresAt").format[Long] and
      (JsPath \ "issuedAt").format[Long] and
      (JsPath \ "userType").format[String] and
      (JsPath \ "acls").format[List[String]] and
      (JsPath \ "softwareType").formatNullable[SoftwareType] and
      (JsPath \ "applicationType").formatNullable[ApplicationType])(
      AuthenticationInformation.apply,
      c =>
        (
          c.login,
          c.expiresAt,
          c.issuedAt,
          c.userType,
          c.acls,
          c.softwareType,
          c.applicationType
        )
    )

  def encode(
      info: AuthenticationInformation,
      secret: String
  ): String =
    JwtJson.encode(Json.toJson(info).as[JsObject], secret, JwtAlgorithm.HS256)
  def decode(
      token: String,
      secret: String
  ): Try[AuthenticationInformation] =
    JwtJson
      .decodeJson(token, secret, Seq(JwtAlgorithm.HS256))
      .map(_.as[AuthenticationInformation])
      .recoverWith({ case t =>
        Failure(
          new AuthenticationException(
            AuthenticationError.InvalidToken,
            t.getMessage
          )
        )
      })

  def encodeFromLogin(
      login: String,
      secret: String,
      expires: Long,
      softwareType: Option[SoftwareType],
      applicationType: Option[ApplicationType],
      acls: List[String]
  ): String = {
    val now = AuthenticatedAction.now()
    encode(
      AuthenticationInformation(
        login,
        now + expires,
        now,
        AuthenticatedAction.ctiUserType,
        AuthenticatedAction.getAliasesFromAcls(acls),
        softwareType,
        applicationType
      ),
      secret
    )
  }
}
