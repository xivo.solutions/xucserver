package models.ws.sso

import play.api.libs.json.{JsValue, Json, Writes}

case class AuthenticationResult(success: Boolean, login: Option[String])

object AuthenticationResult {
  implicit def authenticationResultWrites: Writes[AuthenticationResult] =
    new Writes[AuthenticationResult] {
      override def writes(o: AuthenticationResult): JsValue =
        Json.obj(
          "success" -> o.success,
          "login"   -> o.login
        )
    }
}
