package models

import cats.Semigroup
import cats.derived.semiauto
import cats.syntax.all.*
import play.api.libs.json.Reads.*
import play.api.libs.functional.syntax.*
import play.api.libs.json.*

import scala.collection.immutable.ListSet

sealed trait XivoDird

/*
 * compatible with XiVO auth swagger specification v0.1
 */

case class DirSource(
    id: String,
    entry: Option[String]
)

case class DirSearchResult(
    columnHeaders: List[String],
    columnTypes: List[String],
    items: List[DirSearchItem]
)
object DirSearchResult {

  def parse(json: JsValue): DirSearchResult = {
    val columnHeaders = (json \ "column_headers").validate[List[String]].get
    val columnTypes   = (json \ "column_types").validate[List[String]].get
    val results =
      DirSearchItem.read(columnHeaders, columnTypes, (json \ "results").get)
    DirSearchResult(columnHeaders, columnTypes, results)
  }

}

case class DirSearchItem(
    item: ContactSheetDisplayView,
    relations: Relations,
    source: List[DirSource],
    rank: Int
)
object DirSearchItem {
  def read(
      columnHeaders: List[String],
      columnTypes: List[String],
      json: JsValue
  ): List[DirSearchItem] = {
    val list = json.validate[List[JsValue]].get
    val result = list.zipWithIndex.map((item, rank) => {
      val source: String = (item \ "source").validate[String].getOrElse("")
      val sourceEntryId: Option[String] =
        (item \ "relations" \ "source_entry_id").validate[String].asOpt
      val columnValues =
        ContactSheetDisplayView.read(
          columnHeaders,
          columnTypes,
          (item \ "column_values").get,
          source
        )
      val relations = (item \ "relations").validate[Relations].get
      DirSearchItem(
        item = columnValues,
        relations = relations,
        source = List(
          DirSource(
            id = source,
            entry = sourceEntryId
          )
        ),
        rank = rank
      )
    })
    result
  }
  implicit val semigroup: Semigroup[DirSearchItem] =
    (x: DirSearchItem, y: DirSearchItem) =>
      DirSearchItem(
        item = x.item |+| y.item,
        relations = x.relations |+| y.relations,
        source = x.source ++ y.source,
        rank = if x.rank < y.rank then x.rank else y.rank
      )
}

case class ContactSheetValueWithLabel(label: String, value: String)
object ContactSheetValueWithLabel {
  implicit val semigroup: Semigroup[ContactSheetValueWithLabel] =
    (x: ContactSheetValueWithLabel, y: ContactSheetValueWithLabel) =>
      ContactSheetValueWithLabel(
        label = if x.label.isBlank then y.label else x.label,
        value = if x.value.isBlank then y.value else x.value
      )
}
case class ContactSheetValueBoolean(value: Boolean)
object ContactSheetValueBoolean {
  implicit val semigroup: Semigroup[ContactSheetValueBoolean] =
    (x: ContactSheetValueBoolean, y: ContactSheetValueBoolean) =>
      ContactSheetValueBoolean(x.value || y.value)
}
case class ContactSheetValueString(value: String)
object ContactSheetValueString {
  implicit val semigroup: Semigroup[ContactSheetValueString] =
    (x: ContactSheetValueString, y: ContactSheetValueString) =>
      ContactSheetValueString(
        value = if x.value.isBlank then y.value else x.value
      )
}

case class ContactSheetDisplayView(
    name: ContactSheetValueString,
    subtitle1: ContactSheetValueString,
    subtitle2: ContactSheetValueString,
    title: ContactSheetValueWithLabel,
    service: ContactSheetValueWithLabel,
    phone: ContactSheetValueWithLabel,
    phonePro: ContactSheetValueWithLabel,
    phoneMobile: ContactSheetValueWithLabel,
    phoneHome: ContactSheetValueWithLabel,
    mail: ContactSheetValueWithLabel,
    fax: ContactSheetValueWithLabel,
    manager: ContactSheetValueWithLabel,
    company: ContactSheetValueWithLabel,
    website: ContactSheetValueWithLabel,
    office: ContactSheetValueWithLabel,
    location: ContactSheetValueWithLabel,
    favorite: ContactSheetValueBoolean,
    personal: ContactSheetValueBoolean,
    meetingroom: ContactSheetValueBoolean,
    picture: ContactSheetValueString
)
object ContactSheetDisplayView {
  def read(
      columnHeaders: List[String],
      columnTypes: List[String],
      json: JsValue,
      source: String
  ): ContactSheetDisplayView = {
    val index: Map[String, List[Int]] =
      columnTypes.zipWithIndex.groupBy(_._1).view.mapValues(_.map(_._2)).toMap

    def extractValues[T: Reads](colType: String, defaultValue: T): List[T] =
      index
        .getOrElse(colType, List.empty)
        .map(id => json(id).asOpt[T].getOrElse(defaultValue))

    def extractValue[T: Reads](colType: String, defaultValue: T): T =
      index.getOrElse(colType, List.empty).headOption match
        case None     => defaultValue
        case Some(id) => json(id).asOpt[T].getOrElse(defaultValue)

    def extractNaming(colType: String): List[String] =
      index.getOrElse(colType, List.empty).map(columnHeaders)

    def liftOrEmpty(opt: List[String], index: Int) =
      opt.lift(index).getOrElse("")

    val callablesNames: List[String]  = extractNaming("callable")
    val callablesValues: List[String] = extractValues[String]("callable", "")

    val infosNames: List[String]  = extractNaming("info")
    val infosValues: List[String] = extractValues[String]("info", "")

    ContactSheetDisplayView(
      ContactSheetValueString(extractValue[String]("name", "")),
      ContactSheetValueString(extractValue[String]("subtitle1", "")),
      ContactSheetValueString(extractValue[String]("subtitle2", "")),
      ContactSheetValueWithLabel(
        liftOrEmpty(infosNames, 0),
        liftOrEmpty(infosValues, 0)
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(infosNames, 1),
        liftOrEmpty(infosValues, 1)
      ),
      ContactSheetValueWithLabel(
        extractNaming("number").headOption.getOrElse(""),
        extractValues[String]("number", "").head
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(callablesNames, 0),
        liftOrEmpty(callablesValues, 0)
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(callablesNames, 1),
        liftOrEmpty(callablesValues, 1)
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(callablesNames, 2),
        liftOrEmpty(callablesValues, 2)
      ),
      ContactSheetValueWithLabel(
        extractNaming("email").headOption.getOrElse(""),
        extractValue[String]("email", "")
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(callablesNames, 3),
        liftOrEmpty(callablesValues, 3)
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(infosNames, 2),
        liftOrEmpty(infosValues, 2)
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(infosNames, 3),
        liftOrEmpty(infosValues, 3)
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(infosNames, 4),
        liftOrEmpty(infosValues, 4)
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(infosNames, 5),
        liftOrEmpty(infosValues, 5)
      ),
      ContactSheetValueWithLabel(
        liftOrEmpty(infosNames, 6),
        liftOrEmpty(infosValues, 6)
      ),
      ContactSheetValueBoolean(
        extractValue[Boolean]("favorite", false)
      ),
      ContactSheetValueBoolean(
        extractValue[Boolean]("personal", false)
      ),
      ContactSheetValueBoolean(source.equals("xivo_meetingroom")),
      ContactSheetValueString(
        extractValue[String]("picture", "")
      )
    )
  }

  implicit val semigroup: Semigroup[ContactSheetDisplayView] =
    semiauto.semigroup[ContactSheetDisplayView]

}

case class Relations(
    agentId: Option[Long],
    endpointId: Option[Long],
    userId: Option[Long],
    xivoId: Option[String]
)
object Relations {
  implicit val relationReads: Reads[Relations] = (
    (JsPath \ "agent_id").readNullable[Long] and
      (JsPath \ "endpoint_id").readNullable[Long] and
      (JsPath \ "user_id").readNullable[Long] and
      (JsPath \ "xivo_id").readNullable[String]
  )(Relations.apply)

  implicit val semigroup: Semigroup[Relations] = (x: Relations, y: Relations) =>
    Relations(
      agentId = x.agentId.orElse(y.agentId),
      endpointId = x.endpointId.orElse(y.endpointId),
      userId = x.userId.orElse(y.userId),
      xivoId = x.xivoId.orElse(y.xivoId)
    )

  type SourceId = String
  case class ResultGrouped(
      searchResults: List[DirSearchItem],
      internalSources: Map[SourceId, List[ContactSheetValueWithLabel]],
      directorySourcesPriority: ListSet[String],
      directoryInternalSourcesPriority: ListSet[String]
  ) {
    private val orderedResultsByPriority: List[DirSearchItem] =
      searchResults.sortBy(result =>
        directorySourcesPriority.toList.indexOf(
          result.source.headOption.map(_.id).getOrElse("")
        )
      )
    private val orderedInternalSourceByPriority
        : List[ContactSheetValueWithLabel] =
      internalSources.toList
        .sortBy((sourceId, _) =>
          directoryInternalSourcesPriority.toList.indexOf(
            sourceId
          )
        )
        .flatMap(_._2)

    def toDirSearchItem: Option[DirSearchItem] =
      DirSearchItem.semigroup
        .combineAllOption(orderedResultsByPriority)
        .map(mergeContact =>
          val mergedInternalSource = ContactSheetValueWithLabel.semigroup
            .combineAllOption(orderedInternalSourceByPriority)
            .getOrElse(mergeContact.item.phone)
          mergeContact.copy(item =
            mergeContact.item
              .copy(phone = mergedInternalSource)
          )
        )
  }

  object ResultGrouped {
    def apply(
        resultList: List[DirSearchItem],
        directorySourcesPriority: ListSet[String],
        directoryInternalSourcesPriority: ListSet[String]
    ): ResultGrouped =
      ResultGrouped(
        resultList,
        resultList
          .groupMap(_.source.headOption.map(_.id).getOrElse(""))(_.item.phone),
        directorySourcesPriority,
        directoryInternalSourcesPriority
      )
  }
}
