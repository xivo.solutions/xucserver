package models

import play.api.libs.json.{Format, JsPath, Json, Reads, Writes}

case class StunConfig(urls: List[String])

object StunConfig {

  val reads: Reads[StunConfig] =
    (JsPath \ "urls").read[List[String]].map { o => StunConfig(o) }

  val writes: Writes[StunConfig] =
    (JsPath \ "urls").write[List[String]].contramap(o => o.urls)

  implicit val format: Format[StunConfig] =
    Format(reads, writes)
}
