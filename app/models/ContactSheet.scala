package models

import org.xivo.cti.model.PhoneHintStatus
import play.api.libs.functional.syntax.*
import play.api.libs.json.*
import services.video.model.VideoEvents
import services.video.model.VideoEvents.Event

enum ResultType:
  case Research, Favorite

object ContactSheetDataType extends Enumeration {
  type Type = Value
  val PhoneNumber: ContactSheetDataType.Value = Value
  val Mail: ContactSheetDataType.Value        = Value
  val String: ContactSheetDataType.Value      = Value
  val Url: ContactSheetDataType.Value         = Value

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)

    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(ContactSheetDataType.withName(json.as[String]))
  }
}

case class ContactSheetAction(
    args: List[String],
    disable: Boolean
)

object ContactSheetAction {
  implicit val ContactSheetActionWrites: Writes[ContactSheetAction] =
    (action: ContactSheetAction) =>
      Json.obj(
        "args"    -> action.args,
        "disable" -> action.disable
      )

  implicit val ContactSheetActionMapWrites
      : Writes[Map[ContactSheetActionEnum.Action, ContactSheetAction]] =
    Writes.keyMapWrites[ContactSheetActionEnum.Action, ContactSheetAction, Map](
      using
      contactSheetActionEnum => contactSheetActionEnum.toString,
      Json.writes[ContactSheetAction]
    )

}

object ContactSheetActionEnum extends Enumeration {
  type Action = Value
  val Call: ContactSheetActionEnum.Value      = Value
  val Video: ContactSheetActionEnum.Value     = Value
  val Chat: ContactSheetActionEnum.Value      = Value
  val Mail: ContactSheetActionEnum.Value      = Value
  val ShareLink: ContactSheetActionEnum.Value = Value
  val Edit: ContactSheetActionEnum.Value      = Value
}

case class ContactSheetSources(
    name: String,
    id: String
)

object ContactSheetSources {

  implicit val ContactSheetSourcesReads: Reads[ContactSheetSources] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "id").read[String]
  )(ContactSheetSources.apply)

  implicit val ContactSheetSourcesWrites: Writes[ContactSheetSources] =
    (source: ContactSheetSources) =>
      Json.obj(
        "name" -> source.name,
        "id"   -> source.id
      )
}

case class ContactSheetStatus(
    phone: PhoneHintStatus,
    video: VideoEvents.Event
)

object ContactSheetStatus {

  implicit val contactSheetStatusReads: Reads[ContactSheetStatus] = (
    (JsPath \ "phone").read[Int].map(e => PhoneHintStatus.getHintStatus(e)) and
      (JsPath \ "video").read[VideoEvents.Event]
  )(ContactSheetStatus.apply)

  implicit val contactSheetStatusWrites: Writes[ContactSheetStatus] =
    (status: ContactSheetStatus) =>
      Json.obj(
        "phone" -> status.phone.getHintStatus,
        "video" -> status.video
      )
}

case class ContactSheetDetailField(
    name: String,
    data: String,
    dataType: ContactSheetDataType.Type
)

object ContactSheetDetailField {

  implicit val ContactSheetDetailFieldReads: Reads[ContactSheetDetailField] =
    (
      (JsPath \ "name").read[String] and
        (JsPath \ "data").read[String] and
        (JsPath \ "dataType").read[ContactSheetDataType.Type]
    )(ContactSheetDetailField.apply)

  implicit val ContactSheetDetailFieldWrites: Writes[ContactSheetDetailField] =
    (sheet: ContactSheetDetailField) =>
      Json.obj(
        "name"     -> sheet.name,
        "data"     -> sheet.data,
        "dataType" -> sheet.dataType
      )
}

case class ContactSheetDetailCategorie(
    name: String,
    fields: List[ContactSheetDetailField]
)

object ContactSheetDetailCategorie {

  implicit val ContactSheetDetailCategorieReads
      : Reads[ContactSheetDetailCategorie] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "fields").read[List[ContactSheetDetailField]]
  )(ContactSheetDetailCategorie.apply)

  implicit val ContactSheetDetailCategorieWrites
      : Writes[ContactSheetDetailCategorie] =
    (sheet: ContactSheetDetailCategorie) =>
      Json.obj(
        "name"   -> sheet.name,
        "fields" -> JsArray(sheet.fields.map(Json.toJson))
      )
}

case class ContactSheet(
    name: String,
    subtitle1: String,
    subtitle2: String,
    picture: String,
    isPersonal: Boolean,
    isFavorite: Boolean,
    isMeetingroom: Boolean,
    canBeFavorite: Boolean,
    status: ContactSheetStatus,
    actions: Map[ContactSheetActionEnum.Action, ContactSheetAction],
    sources: List[ContactSheetSources],
    details: List[ContactSheetDetailCategorie]
)

object ContactSheet {
  implicit val contactSheetWrites: Writes[ContactSheet] =
    (contact: ContactSheet) =>
      Json.obj(
        "name"          -> contact.name,
        "subtitle1"     -> contact.subtitle1,
        "subtitle2"     -> contact.subtitle2,
        "picture"       -> contact.picture,
        "isPersonal"    -> contact.isPersonal,
        "isFavorite"    -> contact.isFavorite,
        "isMeetingroom" -> contact.isMeetingroom,
        "canBeFavorite" -> contact.canBeFavorite,
        "status" -> Json.obj(
          "phone" -> contact.status.phone.getHintStatus,
          "video" -> contact.status.video
        ),
        "actions" -> Json.toJson(contact.actions),
        "sources" -> JsArray(contact.sources.map(Json.toJson)),
        "details" -> JsArray(contact.details.map(Json.toJson))
      )
}

case class ContactSheetWrapper(contactSheets: List[ContactSheet])

object ContactSheetWrapper {
  implicit val contactSheetWrapperWrites: Writes[ContactSheetWrapper] =
    (contactSheetWrapper: ContactSheetWrapper) =>
      Json.obj(
        "sheets" -> JsArray(contactSheetWrapper.contactSheets.map(Json.toJson))
      )
}
