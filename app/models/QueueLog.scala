package models

import anorm.SqlParser.get
import anorm._
import com.google.inject.{ImplementedBy, Inject}
import play.api.Logger
import play.api.db.Database
import xivo.xucstats.XucStatsConfig

object QueueLog {
  sealed trait ObjectEvent
  case object Unknown                                extends ObjectEvent
  case class EnterQueue(queueId: Int)                extends ObjectEvent
  case class Abandonned(queueId: Int, waitTime: Int) extends ObjectEvent
  case class Closed(queueId: Int)                    extends ObjectEvent
  case class Timeout(queueId: Int, waitTime: Int)    extends ObjectEvent
  case class LeaveEmpty(queueId: Int)                extends ObjectEvent
  case class ExitWithKey(queueId: Int)               extends ObjectEvent

  case class Complete(
      queueId: Int,
      agentNumber: String,
      waitTime: Int,
      callDuration: Int
  ) extends ObjectEvent
  case class Connect(
      queueId: Int,
      agentNumber: String,
      waitTime: Int,
      callId: String
  ) extends ObjectEvent

  case class QueueLogData(
      queuetime: String,
      agent: String,
      event: String,
      data1: Option[String] = None,
      data2: Option[String] = None,
      data3: Option[String] = None,
      queueid: Option[Int] = None
  )
}

@ImplementedBy(classOf[QueueLogImpl])
trait QueueLog {
  import QueueLog._
  def getAll(clause: String): List[QueueLogData]
  def defaultClause: String
}

class QueueLogImpl @Inject() (db: Database, xucStatsConfig: XucStatsConfig)
    extends QueueLog {
  import QueueLog._

  val log: Logger           = Logger(getClass.getName)
  val defaultClause1Mn      = "cast((now() - interval '1 minutes') as varchar)"
  val defaultClauseMidnight = "cast(current_date as varchar)"

  def defaultClause: String =
    if (xucStatsConfig.initFromMidnight) defaultClauseMidnight
    else defaultClause1Mn

  def query(clause: String): String = s"""
          select
              ql.time as queuetime,
              ql.agent,
              ql.event,
              ql.data1,
              ql.data2,
              ql.data3,
              qf.id as queueid,
              ql.queuename,
              qf.name
          from queue_log as ql
          left outer join queuefeatures as qf on (ql.queuename = qf.name)
          where ql.time > $clause
          order by ql.time
    """

  val simple: RowParser[QueueLogData] = {
    get[String]("queuetime") ~
      get[String]("agent") ~
      get[String]("event") ~
      get[Option[String]]("data1") ~
      get[Option[String]]("data2") ~
      get[Option[String]]("data3") ~
      get[Option[Int]]("queueid") map {
        case queuetime ~ agent ~ event ~ data1 ~ data2 ~ data3 ~ queueid =>
          QueueLogData(queuetime, agent, event, data1, data2, data3, queueid)
      }
  }

  def getAll(clause: String): List[QueueLogData] = {
    db.withConnection { implicit c =>
      log.debug(s"fetching $clause of queue log")
      SQL(query(clause)).as(simple.*)
    }
  }
}
