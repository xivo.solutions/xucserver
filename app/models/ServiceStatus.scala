package services

import org.apache.pekko.actor.ActorRef

object ServiceStatus {
  sealed trait ServiceStatusResponse
  case class ServiceStarting(name: String, serviceRef: ActorRef)
      extends ServiceStatusResponse
  case class ServiceReady(name: String, serviceRef: ActorRef)
      extends ServiceStatusResponse
  case class ServiceStopping(name: String, serviceRef: ActorRef)
      extends ServiceStatusResponse

  sealed trait ServiceStatusRequest
  case object GetServiceStatus       extends ServiceStatusRequest
  case object NotifyWhenServiceReady extends ServiceStatusRequest
}
