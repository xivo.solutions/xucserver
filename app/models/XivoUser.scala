package models

import anorm.SqlParser.get
import anorm.{~, SQL}
import com.google.inject.{ImplementedBy, Inject}
import play.api.db.Database
import play.api.libs.json.*
import xivo.network.XiVOWS
import anorm.{as, on}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, ExecutionContext, Future}
import anorm.RowParser
import play.api.libs.functional.syntax.toFunctionalBuilderOps

import scala.concurrent.duration.FiniteDuration

case class XivoUser(
    id: Long,
    agentId: Option[Long],
    voicemailid: Option[Long],
    firstname: String,
    lastname: Option[String],
    username: Option[String],
    password: Option[String],
    mobile_phone_number: Option[String],
    ctiProfileId: Option[Int]
) {
  def fullName: String =
    lastname
      .filterNot(_.isEmpty)
      .map(n => s"$firstname $n")
      .getOrElse(firstname)
}

case class XivoCtiProfile(
    user_id: Int,
    cti_profile_id: Option[Int],
    enabled: Boolean
)

object XivoCtiProfile {
  implicit val xivoReads: Reads[XivoCtiProfile] =
    (
      (JsPath \ "user_id").read[Int] and
        (JsPath \ "cti_profile_id").readNullable[Int] and
        (JsPath \ "enabled").read[Boolean]
    )(XivoCtiProfile.apply)
}

case class UserDisplayName(userName: String, displayName: String)
case object UserDisplayName {
  implicit val writes: OWrites[UserDisplayName] =
    (
      (JsPath \ "userName").write[String] and
        (JsPath \ "displayName").write[String]
    )(o => (o.userName, o.displayName))
}

@ImplementedBy(classOf[XivoUserDaoImpl])
trait XivoUserDao {
  def getCtiUsers(): Future[List[XivoUser]]
  def getCtiUser(userId: Long): Future[Option[XivoUser]]
  def getCtiUserByLogin(login: String): Future[XivoUser]
  def all(): List[XivoUser]
}

class XivoUserDaoImpl @Inject() (db: Database, ws: XiVOWS) extends XivoUserDao {
  val timeout: FiniteDuration = 10.seconds

  implicit val context: ExecutionContext =
    scala.concurrent.ExecutionContext.Implicits.global
  implicit val xivoReads: Reads[XivoUser] =
    (
      (JsPath \ "id").read[Long] and
        (JsPath \ "agentId").readNullable[Long] and
        (JsPath \ "voicemailid").readNullable[Long] and
        (JsPath \ "firstname").read[String] and
        (JsPath \ "lastname").readNullable[String] and
        (JsPath \ "username").readNullable[String] and
        (JsPath \ "password").readNullable[String] and
        (JsPath \ "mobile_phone_number").readNullable[String] and
        (JsPath \ "ctiProfileId").readNullable[Int]
    )(XivoUser.apply)

  val queryUsers =
    "select id, agentId, voicemailid, firstname, lastname, loginclient, passwdclient, mobilephonenumber, cti_profile_id from userfeatures"

  def queryUser(id: Long): String = s"""
    select id, agentId, voicemailid, firstname, lastname, loginclient, passwdclient, mobilephonenumber, cti_profile_id
    from userfeatures
    where id='$id'"""

  val simple: RowParser[XivoUser] = {
    get[Int]("id") ~
      get[Option[Long]]("agentId") ~
      get[Option[Long]]("voicemailid") ~
      get[String]("firstname") ~
      get[Option[String]]("lastname") ~
      get[Option[String]]("loginclient") ~
      get[Option[String]]("passwdclient") ~
      get[Option[String]]("mobilephonenumber") ~
      get[Option[Int]]("cti_profile_id") map {
        case id ~ agentId ~ voicemailid ~ firstname ~ lastname ~ loginclient ~ passwdclient ~ mobilephonenumber ~ ctiProfileId =>
          XivoUser(
            id,
            agentId,
            voicemailid,
            firstname,
            lastname,
            loginclient,
            passwdclient,
            mobilephonenumber filterNot { _.isEmpty },
            ctiProfileId
          )
      }
  }

  def all(): List[XivoUser] = Await.result(getCtiUsers(), timeout)

  def getCtiUser(userId: Long): Future[Option[XivoUser]] = {
    Future {
      db.withConnection { implicit c =>
        SQL(queryUser(userId)).as(simple.*).headOption
      }
    }
  }

  def getCtiUserByLogin(login: String): Future[XivoUser] = {
    Future {
      db.withConnection { implicit c =>
        SQL(queryUsers + " WHERE loginclient = {login}")
          .on("login" -> login)
          .as(simple.single)
      }
    }
  }

  def getCtiUsers(): Future[List[XivoUser]] = {
    Future {
      db.withConnection { implicit c =>
        SQL(queryUsers).as(simple.*)
      }
    }
  }

}
