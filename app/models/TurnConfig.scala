package models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Format, JsPath, Reads, Writes}

case class TurnConfig(
    urls: List[String],
    username: String,
    credential: String,
    ttl: Long
)

object TurnConfig {
  implicit val format: Format[TurnConfig] =
    (
      (JsPath \ "urls").format[List[String]] and
        (JsPath \ "username").format[String] and
        (JsPath \ "credential").format[String] and
        (JsPath \ "ttl").format[Long]
    )(TurnConfig.apply, o => (o.urls, o.username, o.credential, o.ttl))
}
