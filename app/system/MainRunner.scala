package system

import java.util.concurrent.TimeUnit

import org.apache.pekko.actor.{Actor, ActorLogging, Terminated}
import com.codahale.metrics.{
  Metric,
  MetricFilter,
  SharedMetricRegistries,
  Slf4jReporter
}
import com.codahale.metrics.jmx.JmxReporter
import com.codahale.metrics.jvm.{MemoryUsageGaugeSet, ThreadStatesGaugeSet}
import com.google.inject.Inject
import javax.crypto.Cipher
import models.XucUser
import play.api.Logger
import play.api.inject.Injector
import xivo.xuc.XucBaseConfig
import xivo.xucstats.XucBaseStatsConfig
import helpers.JmxActorSingletonMonitor
import java.time.LocalDateTime

object MainRunner {
  case class StartUser(user: XucUser)
}

class MainRunner @Inject() (
    xucConfig: XucBaseConfig,
    xucStatsConfig: XucBaseStatsConfig,
    injector: Injector
) extends Actor
    with ActorLogging
    with JmxActorSingletonMonitor {

  log.info(s"Xuc Main runner started ................$self")
  jmxBean.addString("version", xucserver.info.BuildInfo.version)
  jmxBean.addString("startTime", LocalDateTime.now().toString)

  logCipherKeyLength()

  if (log.isDebugEnabled) {
    logSystemProperties()
  }

  startStatistics()
  startTechMetrics()

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  def receive: PartialFunction[Any, Unit] = {
    case Terminated(actor) =>
      log.error(s"$actor stopped")
    case _ =>
  }

  private def startTechMetrics(): Unit = {
    val registry =
      SharedMetricRegistries.getOrCreate(xucConfig.metricsRegistryName)
    if (xucConfig.metricsRegistryJVM) {
      log.info("Activating JVM metrics")
      registry.removeMatching(new MetricFilter {
        override def matches(name: String, metric: Metric): Boolean =
          name.startsWith("jvm")
      })
      registry.register("jvm.memory", new MemoryUsageGaugeSet())
      registry.register("jvm.thread", new ThreadStatesGaugeSet())
    }

    if (xucConfig.metricsLogReporter) {
      log.info(
        s"Activating logReporter with period: ${xucConfig.metricsLogReporterPeriod} minutes"
      )
      val reporter = Slf4jReporter
        .forRegistry(registry)
        .outputTo(Logger(getClass.getName).logger)
        .convertRatesTo(TimeUnit.SECONDS)
        .convertDurationsTo(TimeUnit.MILLISECONDS)
        .build()
      reporter.start(xucConfig.metricsLogReporterPeriod, TimeUnit.MINUTES)
    }
    if (xucConfig.metricsJmxReporter) {
      log.info("Activating jmxReporter")
      val jmxReporter = JmxReporter
        .forRegistry(registry)
        .inDomain("Xuc")
        .createsObjectNamesWith(new JmxNameFactory)
        .build()
      jmxReporter.start()
    }
  }

  private def logCipherKeyLength(): Unit = {
    try {
      val allowedKeyLength = Cipher.getMaxAllowedKeyLength("AES")
      log.info("The allowed key length for AES is: " + allowedKeyLength)
    } catch {
      case e: Throwable =>
        log.error("Unable to get max cipher length")
        log.error(e.getStackTrace.mkString("\n"))
    }
  }

  private def logSystemProperties(): Unit = {
    import scala.jdk.CollectionConverters._
    val environmentVars = System.getenv.asScala
    for ((k, v) <- environmentVars) log.debug(s"envVar '$k': $v")

    val properties = System.getProperties
    properties.stringPropertyNames().asScala.iterator foreach (k =>
      log.debug("propsKey '{}': {}", k, properties.getProperty(k))
    )
  }

  private def startStatistics(): Unit = {
    log.info("Starting xuc stats")

    if (xucStatsConfig.statsLogReporter) {
      val reporter = Slf4jReporter
        .forRegistry(
          SharedMetricRegistries.getOrCreate(xucConfig.statsMetricsRegistryName)
        )
        .outputTo(Logger(getClass.getName).logger)
        .convertRatesTo(TimeUnit.SECONDS)
        .convertDurationsTo(TimeUnit.MILLISECONDS)
        .build()
      reporter.start(xucStatsConfig.statsLogReporterPeriod, TimeUnit.MINUTES)
    }

  }

}
