package system

import retry.*
import retry.RetryDetails.*
import cats.*
import cats.effect.IO
import cats.implicits.*
import scala.concurrent.duration.*
import retry.RetryPolicy
import retry.RetryPolicies.*
import cats.effect.unsafe.implicits.global

object ClusterAsync {

  var clusterRetries = 5

  def setCustomClusterRetries(retries: Int): Unit =
    clusterRetries = retries

  def withRetries[T, A](
      process: A => IO[T],
      arg: A,
      onRetry: () => Unit = () => {},
      onGiveUp: () => Unit = () => {}
  ): Unit =
    retryingOnAllErrors[T](
      policy = constantDelay[IO](1.second) |+| limitRetries[IO](clusterRetries),
      onError = customErrorHandling(
        () => {
          onRetry()
          IO.unit
        },
        () => {
          onGiveUp()
          IO.unit
        }
      )
    )(process(arg)).unsafeRunAndForget()

  private def customErrorHandling(
      onRetry: () => IO[Unit],
      onGiveup: () => IO[Unit]
  ): (err: Throwable, details: RetryDetails) => IO[Unit] = {
    def template(err: Throwable, details: RetryDetails): IO[Unit] =
      details match {
        case WillDelayAndRetry(
              nextDelay: FiniteDuration,
              retries: Int,
              cumulativeDelay: FiniteDuration
            ) =>
          onRetry()
        case GivingUp(totalRetries: Int, totalDelay: FiniteDuration) =>
          onGiveup()
      }

    template
  }

}
