package system

import org.joda.time.DateTime

import java.time.Clock

class TimeProvider {

  private val dateTime: DateTime = new DateTime()
  private val clock: Clock       = Clock.systemDefaultZone()

  def getJodaTime: Long   = dateTime.getMillis / 1000
  def getJavaClock: Clock = clock
}
