var DirectoryDisplay = {

    init : function(tableName) {
        this.tableName = tableName;
    },

    prepareTable : function() {
        $(this.tableName).empty();
    },

    genHeaders: function(contactExample) {
        return `
        <col>
        <colgroup span="2"></colgroup>
        <colgroup span="4"></colgroup>

        <tr>
            <th rowspan="3">Name</th>
            <th rowspan="3">canBeFavorite</th>
            <th rowspan="3">isFavorite</th>
            <th rowspan="3">isPersonal</th>
            <th rowspan="3">Picture</th>
            <th scope="colgroup" colspan="2">Subtitles</th>
            <th scope="colgroup" colspan="4">Actions</th>
            <th scope="colgroup" colspan="13">Details</th>
            <th scope="colgroup" colspan="2">Source</th>
            <th scope="colgroup" colspan="2">Status</th>
        </tr>

        <tr>
            <th rowspan="2" scope="col">1</th>
            <th rowspan="2" scope="col">2</th>
            <th rowspan="2" scope="col">Call</th>
            <th rowspan="2" scope="col">Chat</th>
            <th rowspan="2" scope="col">Mail</th>
            <th rowspan="2" scope="col">Video</th>
            <th scope="colgroup" colspan="6">Contacts</th>
            <th scope="colgroup" colspan="5">General</th>
            <th scope="colgroup" colspan="2">Workplace</th>
            <th rowspan="2" scope="col">Name</th>
            <th rowspan="2" scope="col">Id</th>
            <th rowspan="2" scope="col">Phone</th>
            <th rowspan="2" scope="col">Video</th>
        </tr>
        <tr>
        </tr>
        `
    },

    genContactActions: function(contact) {
        let actions = ""
        contact.actions.forEach(action => {
            actions += `<td>${action.args[0]}</td>`
        })
        return actions
    },

    genContactDetails: function(contact) {
        let details = ""
        contact.details.forEach(detail => {
            detail.fields.forEach(field => {
                details += `<td>${field.name}: ${field.data}</td>`
            })
        })
        return details
    },

    genContactSource: function(sources) {
        let sourcesData = ""
        sources.forEach(source => {
            sourcesData += `
                <td>${source.name}</td>
                <td>${source.id}</td>
            `
        })
        return sourcesData
    },

    genContactInfo: function(contact) {
        return  `
        <tr>
            <td><span>${contact.name}</span></td>
            <td><span>${contact.canBeFavorite}</span></td>
            <td><span>${contact.isFavorite}</span></td>
            <td><span>${contact.isPersonal}</span></td>
            <td><span>${contact.Picture}</span></td>
            <td><span>${contact.subtitle1}</span></td>
            <td><span>${contact.subtitle2}</span></td>
            ${this.genContactActions(contact)}
            ${this.genContactDetails(contact)}
            ${this.genContactSource(contact.sources)}
            <td>${contact.status.phone}</td>
            <td>${contact.status.video}</td>
        </tr>
        `
    },

    genArray: function(sheets) {
        let array = ""
        let headers = this.genHeaders(sheets[0])
        sheets.forEach(contact => {
            headers += this.genContactInfo(contact)
        });

        return headers
    },

    displayContacts : function(contacts) {
        $(this.tableName).append('<table class="table table-condensed table-bordered">' + this.genArray(contacts.sheets) + '</table>')
    },

    formatField : function(field) {
        if ($.isNumeric(field)) {
            return '<a href="#">'+field+'</a>';
        }
        else {
            return field;
        }
    }
};

function extractVariables(str) {
    var res = {};
    if(str) {
        var keyValues = str.split('&');
        for(var i=0; i<keyValues.length; i++) {
            var kv = keyValues[i].split('=');
            if(kv.length == 2) {
                var k = kv[0];
                var v = kv[1];
                res[k] = v;
            } else {
                console.log('Invalid key value pair');
                console.log(kv);
            }
        }
    }
    return res;
}

function makeAuthCall(type, url, data) {

    var req = {
        type: type,
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", 'Bearer ' + $('#xuc_token').val());
            },
        url: url
        };

    if (data) {
        req.data = JSON.stringify(data);
        req.dataType = 'json';
        req.contentType = 'application/json;charset=utf-8';
    }
    return $.ajax(req);
}

function makeAuthCsvUpload(url, csv_data) {
    return $.ajax({
        type : "POST",
        beforeSend: function(request) {
          request.setRequestHeader("Authorization", 'Bearer ' + $('#xuc_token').val());
        },
        contentType: "text/plain;charset=utf-8",
        data: csv_data,
        url : url,
        processData: false
    });
}

function extractErrorMessageFromResponse(response) {
    if (response.getResponseHeader('content-type').startsWith('application/json')) {
      return response.responseJSON.message + '(' + response.responseJSON.error + ')';
    } else {
      return response.status + ' ' + response.statusText;
    }
}
