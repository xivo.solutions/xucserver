var basePath = "/xuc/api/2.0/config/";

function membershipEventHandler(event) {
    console.log('UserQueueDefaultMembership Event handler ' + JSON.stringify(event));
    $('#xuc_membership_event').prepend('<li class="list-group-item event-item"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</code></pre></li>');
}

function queueDissuasionHandler(event) {
  console.log('Get queue dissuasion response' + JSON.stringify(event));
  $('#xuc_queue_dissuasion').prepend('<li class="list-group-item event-item"><pre><code>' + JSON.stringify(event) + '</code></pre></li>');
}

function queueStatisticsHandler(event) {
    console.log("queue statistics Handler " + JSON.stringify(event));
    $('#queue_stats').prepend(
            '<li class="list-group-item"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</code></pre></li>');
}
function queueConfigHandler(queueConfig) {
    $('#queue_config').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(queueConfig) + '</code></pre></li>');
}
function queueMemberHandler(queueMember) {
    $('#queue_member').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(queueMember) + '</code></pre></li>');
}
function queueCallsHandler(queueCalls) {
    $('#queue_calls').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(queueCalls) + '</code></pre></li>');
}

$('#xuc_get_queue_dissuasion').click(function () {
  var id = $('#xuc_queue_id_dissuasion').val();
  makeAuthCall('GET', basePath + 'queue/' + id + "/dissuasion")
    .then(function (data) {
      queueDissuasionHandler(data)
    })
    .fail(function (err) {
      try {
        JSON.parse(err.responseText);
        generateMessage(err.responseText, true);
      } catch (e) {
        generateMessage("Error when getting selected dissuasion sound file", true);
      }
      console.error(err)
    })
});

$('#xuc_get_queue_dissuasion_list').click(function () {
  var id = $('#xuc_queue_id_dissuasion').val();
  makeAuthCall('GET', basePath + 'queue/' + id + '/dissuasions')
    .then(function (data) {
      queueDissuasionHandler(data)
    })
    .fail(function (err) {
      try {
        JSON.parse(err.responseText);
        generateMessage(err.responseText, true);
      } catch (e) {
        generateMessage("Error when getting list of dissuasion sound files", true);
      }
      console.error(err);
    })
});

$('#set_dissuasion_sound_file').click(function () {
  var id = $('#xuc_queue_id_dissuasion').val();
  var data = {
    soundFile: $('#dissuasion_file_name').val()
  };

  makeAuthCall('PUT', basePath + 'queue/' + id + '/dissuasion/sound_file', data)
    .then(function (data) {
      queueDissuasionHandler(data)
    })
    .fail(function (err) {
      try {
        JSON.parse(err.responseText);
        generateMessage(err.responseText, true);
      } catch (e) {
        generateMessage("Error when set dissuasion to a sound file", true);
      }
      console.error(err);
    })
});

$('#set_dissuasion_queue').click(function () {
  var id = $('#xuc_queue_id_dissuasion').val();
  var data = {
    queueName: $('#dissuasion_queue_name').val()
  };

  makeAuthCall('PUT', basePath + 'queue/' + id + '/dissuasion/queue', data)
    .then(function (data) {
      queueDissuasionHandler(data)
    })
    .fail(function (err) {
      try {
        JSON.parse(err.responseText);
        generateMessage(err.responseText, true);
      } catch (e) {
        generateMessage("Error when set dissuasion to a queue", true);
      }
      console.error(err);
    })
});

$('#xuc_clean_queue_dissuasion').click(
  function () {
    $('#xuc_queue_dissuasion').empty()
  }
);

$('#xuc_subscribe_to_queue_stats_btn').click(function(event) {
    Cti.subscribeToQueueStats();
});

$('#xuc_get_config_queues').click(function(event) {
    $('#queue_config').html("");
    Cti.getList("queue");
});
$('#xuc_queues_recording_on').click(function(event) {
  makeAuthCall('POST', "/xuc/api/2.0/config/recording/status/on",{});
});
$('#xuc_queues_recording_off').click(function(event) {
  makeAuthCall('POST', "/xuc/api/2.0/config/recording/status/off",{});
});
$('#xuc_get_config_queuemembers').click(function(event) {
    Cti.getList("queuemember");
});
$('#xuc_subscribe_to_queue_calls').click(function(event) {
    Cti.subscribeToQueueCalls(parseInt($('#xuc_queue_id_calls').val()));
});
$('#xuc_unsubscribe_to_queue_calls').click(function(event) {
    Cti.unSubscribeToQueueCalls(parseInt($('#xuc_queue_id_calls').val()));
});
$('#xuc_clean_queue_stats').click(function(event) {
     $('#queue_stats').empty();
});

$('#xuc_clean_queue_config').click(function(event) {
     $('#queue_config').empty();
});

$('#xuc_clean_queue_member').click(function(event) {
     $('#queue_member').empty();
});
$('#xuc_clean_queue_calls').click(function(event) {
     $('#queue_calls').empty();
});

$('#xuc_dial_from_queue_btn').click(function(event) {
    var variables = extractVariables($("#xuc_dial_from_variables").val());
    Cti.dialFromQueue(
        $("#xuc_dial_from_queue_destination").val(),
        $("#xuc_dial_from_queue_id").val(),
        $("#xuc_dial_from_queue_caller_id_name").val(),
        $("#xuc_dial_from_queue_caller_id_number").val(),
        variables
    );
});

$('#xuc_membership_clean').click(function() {
    $('#xuc_membership_event').html("");
});

$('#xuc_membership_get').click(function() {
    Membership.getUserDefaultMembership(_currentUserId);
});

$('#xuc_membership_set').click(function() {
    var membership = $("#xuc_membership_data").val();
    Membership.setUserDefaultMembership(_currentUserId, JSON.parse(membership));
});

$('#xuc_membership_apply').click(function() {
    var userIds = $("#xuc_membership_users").val();
    Membership.applyUsersDefaultMembership(JSON.parse(userIds));
});

$('#xuc_retrieve_queuecall').click(function(event) {
    Cti.retrieveQueueCall(JSON.parse($("#xuc_queuecall").val()));
});

function initQueues() {
  Cti.setHandler(Cti.MessageType.QUEUECONFIG, queueConfigHandler);
  Cti.setHandler(Cti.MessageType.QUEUELIST, queueConfigHandler);
  Cti.setHandler(Cti.MessageType.QUEUEMEMBER, queueMemberHandler);
  Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, queueMemberHandler);
  Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, queueStatisticsHandler);
  Cti.setHandler(Cti.MessageType.QUEUECALLS, queueCallsHandler);
  Cti.setHandler(Membership.MessageType.USERQUEUEDEFAULTMEMBERSHIP, membershipEventHandler);
}