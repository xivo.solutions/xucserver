var basePath = "/xuc/api/2.0/config/";

function groupConfigHandler(groupConfig) {
    $('#group_config').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(groupConfig) + '</code></pre></li>');
}

$('#xuc_clean_group_config').click(function(event) {
     $('#group_config').empty();
});

$('#xuc_group_pause_btn').click(function(event) {
    Cti.pauseUserGroup(parseInt($('#groupid_to_pause').val()));
});
$('#xuc_group_unpause_btn').click(function(event) {
    Cti.unpauseUserGroup(parseInt($('#groupid_to_pause').val()));
});

function initGroups() {
  Cti.setHandler(Cti.MessageType.USERGROUPS, groupConfigHandler);
  Cti.setHandler(Cti.MessageType.USERGROUPUPDATE, groupConfigHandler);
}