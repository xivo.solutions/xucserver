var xucserver = window.location.hostname+":"+window.location.port;
var protocol = window.location.protocol == "http:" ? "ws:" : "wss:"
$('#OIDC_xuc_server').val(xucserver);

$('#OIDC_submit').click(function(event) {
    $.getJSON(window.location.protocol + "//" + $('#OIDC_xuc_server').val() + "/xuc/api/2.0/auth/oidc?token=" + $('#OIDC_token').val() , (data) => {
        if (data && data.login && data.token) {
            $('#xuc_token').val(data.token);
            xucToken = data.token;
            init(data.login, undefined, undefined, protocol + "//" + $('#OIDC_xuc_server').val() + "/xuc/api/2.0/cti?token=" + data.token);
        }
    }, "json")
    .fail((data) => {
        var error = "Something went wrong during the OIDC authentication -> " + data.responseJSON.message;
        generateMessage(error, true, true);
    })
});