$('#xuc_setUserPreference').click(function(event) {
  var key = $('#xuc_setUserPreferenceKey').val();
  var value = $('#xuc_setUserPreferenceValue').val();
  var valueType = $('#xuc_setUserPreferenceValueType').val();
  Cti.setUserPreference(key, value, valueType);
});

function userPreferenceHandler(event) {
    console.log('User preference Event handler' + JSON.stringify(event));
    $('#userPreference_events').prepend('<li class="list-group-item event-item"><pre><code>User Preference Event: ' + JSON.stringify(event) + '</code></pre></li>');
}

$('#clean_userPreference_events').click(function(event) {
     $('#userPreference_events').empty();
});