function agentStateEventHandler(agentState) {
    $('#agent_events').prepend(
            '<li class="list-group-item event-item"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(agentState) + '</code></pre></li>');
    $('#xuc_agentData').val("cause :  " + agentState.cause);
    if (agentState.name !== "AgentLoggedOut") {
        loginHandler(agentState);
    }
    switch (agentState.name) {
    case "AgentReady":
        readyHandler();
        break;
    case "AgentOnPause":
        onPauseHandler();
        break;
    case "AgentDialing":
        dialingHandler();
        break;
    case "AgentRinging":
        ringingHandler();
        break;
    case "AgentOnCall":
        onCallHandler(agentState);
        break;
    case "AgentOnWrapup":
        onWrapupHandler();
        break;
    case "AgentLoggedOut":
        logoutHandler();
        break;
    }
}
function loginHandler(event) {
    $('#xuc_agentStatus').val("Logged in");
    $('#xuc_agentPhoneNumber').val(event.phoneNb);
}

function logoutHandler() {
    $('#xuc_agentStatus').val("Logged out");
}

function readyHandler() {
    $('#xuc_agentStatus').val("Ready");
}
function dialingHandler() {
    $('#xuc_agentStatus').val("Dialing");
}
function ringingHandler() {
    $('#xuc_agentStatus').val("Ringing");
}

function onPauseHandler() {
    $('#xuc_agentStatus').val("Paused");
}
function onCallHandler(state) {
    $('#xuc_agentStatus').val("On Call");
    $('#xuc_agentData').val("Acd : " + state.acd + " dir : " + state.direction + " Type : " + state.callType);
    $('#xuc_agentMonitorState').val(state.monitorState);
}
function onWrapupHandler() {
    $('#xuc_agentStatus').val("Wrapup");
}

function agentConfigHandler(agentConfig) {
    console.log("agent config Handler " + JSON.stringify(agentConfig));
    $('#agent_config').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(agentConfig) + '</code></pre></li>');
}
function agentGroupConfigHandler(agentGroups) {
    console.log("agent group config Handler " + JSON.stringify(agentGroups));
    $('#agentgroup_config').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(agentGroups) + '</code></pre></li>');
}

function agentErrorHandler(error) {
    generateMessage(error, true);
}

function agentDirectoryHandler(agentDirectory) {
    console.log("agent directory Handler " + JSON.stringify(agentDirectory));
    $('#agent_directory').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(agentDirectory) + '</code></pre></li>');
}
function agentListenHandler(listenEvent) {
    $('#xuc_agentListenStatus').val(JSON.stringify(listenEvent));
}

function callbacksHandler(callbackLists) {
    console.log('Callback lists handler' + JSON.stringify(callbackLists));
    $('#callbacks').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(callbackLists) + '</code></pre></li>');
}

function agentStatisticsHandler(event) {
    console.log("agent statistics Handler " + JSON.stringify(event));
    $('#agent_stats').prepend(
            '<li class="list-group-item  event-item"><pre><code>' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</code></pre></li>');
}

$('#xuc_subscribe_to_agent_statistic_btn').click(function(event) {
    Cti.subscribeToAgentStats();
});

$('#xuc_clean_agent_statistic').click(function(event) {
    $('#agent_stats').empty();
});

$('#xuc_subscribe_to_agent_event_btn').click(function(event) {
    Cti.subscribeToAgentEvents();
});
$('#xuc_get_agentstates').click(function(event) {
    Cti.getAgentStates();
});
$('#xuc_clean_agent_events').click(function(event) {
     $('#agent_events').empty();
     $('#agentstates').empty();
});

$('#xuc_get_config_agents').click(function(event) {
    Cti.getList("agent");
});
$('#xuc_clean_agent_config').click(function(event) {
     $('#agent_config').empty();
});

$('#xuc_clean_agent_directory').click(function(event) {
     $('#agent_directory').empty();
});

$('#xuc_clean_agentgroup_config').click(function(event) {
     $('#agentgroup_config').empty();
});

$('#xuc_get_agent_directory').click(function(event) {
    Cti.getAgentDirectory();
});
$('#xuc_set_agent_queue_btn').click(function(event) {
    Cti.setAgentQueue($("#xuc_agentId_target").val(), $("#xuc_queueId").val(), $("#xuc_penalty").val());
});
$('#xuc_remove_agent_from_queue_btn').click(function(event) {
    Cti.removeAgentFromQueue($("#xuc_agentId_target").val(),$("#xuc_queueId").val());
});
$('#xuc_listen_agent_btn').click(function(event) {
    Cti.listenAgent(parseInt($("#xuc_agentId_target").val()));
});

$('#xuc_monitorpause_btn').click(function(event) {
    Cti.monitorPause(parseInt($('#xuc_agentId').text()));
});
$('#xuc_monitorunpause_btn').click(function(event) {
    Cti.monitorUnpause(parseInt($('#xuc_agentId').text()));
});
$('#xuc_login_btn').click(function(event) {
    Cti.loginAgent($('#xuc_agentPhoneNumber').val());
});
$('#xuc_logout_btn').click(function(event) {
    Cti.logoutAgent();
});
$('#xuc_pause_btn').click(function(event) {
    Cti.pauseAgent(undefined, $('#agent_pause_reason').val());
});
$('#xuc_unpause_btn').click(function(event) {
    Cti.unpauseAgent();
});
$('#xuc_get_config_agentgroups').click(function(event) {
    Cti.getList("agentgroup");
});
$('#xuc_clean_callbacks').click(function (){
    $('#callbacks').empty();
});
$('#xuc_get_callback_lists').click(function() {
    Callback.getCallbackLists();
});


function initAgents() {
  Cti.setHandler(Cti.MessageType.AGENTSTATISTICS, agentStatisticsHandler);
  Cti.setHandler(Cti.MessageType.AGENTCONFIG, agentConfigHandler);
  Cti.setHandler(Cti.MessageType.AGENTDIRECTORY, agentDirectoryHandler);
  Cti.setHandler(Cti.MessageType.AGENTERROR, agentErrorHandler);
  Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, agentGroupConfigHandler);
  Cti.setHandler(Cti.MessageType.AGENTLIST, agentConfigHandler);
  Cti.setHandler(Cti.MessageType.AGENTLISTEN, agentListenHandler);
  Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, agentStateEventHandler);
  Cti.setHandler(Callback.MessageType.CALLBACKLISTS, callbacksHandler);
}
