package services.video

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import com.google.inject.name.Named
import services.ActorIds
import services.request.UserBaseRequest
import services.video.model._

class VideoEventManager @Inject() (
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    @Named(VideoService.serviceName) videoService: ActorRef
) extends Actor
    with ActorLogging {

  override def receive: PartialFunction[Any, Unit] = {
    case ubr: UserBaseRequest =>
      ubr.request match {
        case e: VideoEvent =>
          configDispatcher ! UserVideoEvent(ubr.user.username, e.status)
        case vid: InviteToMeetingRoom =>
          videoService forward MeetingRoomInvite(
            vid.requestId,
            vid.token,
            vid.username,
            ubr.user.username,
            ubr.user.xivoUser.fullName
          )
        case ack: MeetingRoomInviteAck =>
          videoService ! MeetingRoomInviteAckReply(
            ack.requestId,
            ack.username,
            ubr.user.xivoUser.fullName,
            VideoInviteAck.ACK
          )
        case r: MeetingRoomInviteAccept =>
          videoService ! MeetingRoomInviteResponse(
            r.requestId,
            r.username,
            ubr.user.xivoUser.fullName,
            VideoInviteResponse.ACCEPT
          )
        case r: MeetingRoomInviteReject =>
          videoService ! MeetingRoomInviteResponse(
            r.requestId,
            r.username,
            ubr.user.xivoUser.fullName,
            VideoInviteResponse.REJECT
          )
      }
  }
}
