package services.video.model

import play.api.libs.json.*
import services.request.XucRequest
import play.api.libs.functional.syntax._

object VideoEvents extends Enumeration {
  type Event = Value
  val Available: VideoEvents.Value = Value
  val Busy: VideoEvents.Value      = Value

  implicit val format: Format[Event] = new Format[Event] {
    def writes(r: Event): JsValue = JsString(r.toString)

    def reads(json: JsValue): JsResult[Event] =
      JsSuccess(VideoEvents.withName(json.as[String]))
  }
}

object VideoInviteAck extends Enumeration {
  type Type = Value
  val ACK: VideoInviteAck.Value  = Value
  val NACK: VideoInviteAck.Value = Value

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(VideoInviteAck.withName(json.as[String]))
  }
}

object VideoInviteResponse extends Enumeration {
  type Type = Value
  val ACCEPT: Value = Value("Accept")
  val REJECT: Value = Value("Reject")

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(VideoInviteResponse.withName(json.as[String]))
  }
}

case class VideoEvent(status: String) extends XucRequest
case object VideoEvent {
  def validate(json: JsValue): JsResult[VideoEvent] = json.validate[VideoEvent]
  implicit val format: Format[VideoEvent] = (JsPath \ "status")
    .format[String]
    .inmap(e => VideoEvent(e), (o: VideoEvent) => o.status)
}

case class UserVideoEvent(fromUser: String, status: String) extends XucRequest
case object UserVideoEvent {
  def matchStatusToEvent(e: String): VideoEvents.Event = {
    e match {
      case "videoStart" => VideoEvents.Busy
      case "videoEnd"   => VideoEvents.Available
    }
  }
}

case class VideoStatusEvent(fromUser: String, status: VideoEvents.Event)
case object VideoStatusEvent {
  implicit val writes: Writes[VideoStatusEvent] = (
    (JsPath \ "fromUser").write[String] and
      (JsPath \ "status").write[VideoEvents.Event]
  )(o => (o.fromUser, o.status))
}

sealed trait VideoInvitationCommand extends XucRequest

case class InviteToMeetingRoom(requestId: Int, token: String, username: String)
    extends VideoInvitationCommand

case object InviteToMeetingRoom {
  implicit val reads: Reads[InviteToMeetingRoom] = (
    (JsPath \ "requestId").read[Int] and
      (JsPath \ "token").read[String] and
      (JsPath \ "username").read[String]
  )(InviteToMeetingRoom.apply)
  def validate(json: JsValue): JsResult[InviteToMeetingRoom] =
    json.validate[InviteToMeetingRoom]
}

case class MeetingRoomInviteAck(requestId: Int, username: String)
    extends VideoInvitationCommand

case object MeetingRoomInviteAck {
  implicit val reads: Reads[MeetingRoomInviteAck] = (
    (JsPath \ "requestId").read[Int] and
      (JsPath \ "username").read[String]
  )(MeetingRoomInviteAck.apply)
  def validate(json: JsValue): JsResult[MeetingRoomInviteAck] =
    json.validate[MeetingRoomInviteAck]
}

case class MeetingRoomInviteAccept(requestId: Int, username: String)
    extends VideoInvitationCommand

case object MeetingRoomInviteAccept {
  implicit val reads: Reads[MeetingRoomInviteAccept] = (
    (JsPath \ "requestId").read[Int] and
      (JsPath \ "username").read[String]
  )(MeetingRoomInviteAccept.apply)
  def validate(json: JsValue): JsResult[MeetingRoomInviteAccept] =
    json.validate[MeetingRoomInviteAccept]
}

case class MeetingRoomInviteReject(requestId: Int, username: String)
    extends VideoInvitationCommand

case object MeetingRoomInviteReject {
  implicit val reads: Reads[MeetingRoomInviteReject] = (
    (JsPath \ "requestId").read[Int] and
      (JsPath \ "username").read[String]
  )(MeetingRoomInviteReject.apply)
  def validate(json: JsValue): JsResult[MeetingRoomInviteReject] =
    json.validate[MeetingRoomInviteReject]
}

sealed trait VideoInvitationEvent extends XucRequest

case class MeetingRoomInvite(
    requestId: Int,
    token: String,
    destUsername: String,
    username: String,
    displayName: String
) extends VideoInvitationEvent

case object MeetingRoomInvite {
  implicit val reads: Reads[MeetingRoomInvite] = (
    (JsPath \ "requestId").read[Int] and
      (JsPath \ "token").read[String] and
      (JsPath \ "destUsername").read[String] and
      (JsPath \ "username").read[String] and
      (JsPath \ "displayName").read[String]
  )(MeetingRoomInvite.apply)

  implicit val writes: Writes[MeetingRoomInvite] =
    new Writes[MeetingRoomInvite] {
      def writes(mri: MeetingRoomInvite): JsValue =
        Json.obj(
          "requestId"   -> mri.requestId,
          "token"       -> mri.token,
          "username"    -> mri.username,
          "displayName" -> mri.displayName
        )
    }
}

case class MeetingRoomInviteAckReply(
    requestId: Int,
    destUsername: String,
    displayName: String,
    ackType: VideoInviteAck.Type
) extends VideoInvitationEvent

case object MeetingRoomInviteAckReply {

  implicit val reads: Reads[MeetingRoomInviteAckReply] = (
    (JsPath \ "requestId").read[Int] and
      (JsPath \ "destUsername").read[String] and
      (JsPath \ "displayName").read[String] and
      (JsPath \ "ackType").read[VideoInviteAck.Type]
  )(MeetingRoomInviteAckReply.apply)

  implicit val writes: Writes[MeetingRoomInviteAckReply] =
    new Writes[MeetingRoomInviteAckReply] {
      def writes(ack: MeetingRoomInviteAckReply): JsValue =
        Json.obj(
          "requestId"   -> ack.requestId,
          "displayName" -> ack.displayName,
          "ackType"     -> ack.ackType
        )
    }
}

case class MeetingRoomInviteResponse(
    requestId: Int,
    destUsername: String,
    displayName: String,
    responseType: VideoInviteResponse.Type
) extends VideoInvitationEvent

case object MeetingRoomInviteResponse {
  implicit val reads: Reads[MeetingRoomInviteResponse] = (
    (JsPath \ "requestId").read[Int] and
      (JsPath \ "destUsername").read[String] and
      (JsPath \ "displayName").read[String] and
      (JsPath \ "responseType").read[VideoInviteResponse.Type]
  )(MeetingRoomInviteResponse.apply)

  implicit val writes: Writes[MeetingRoomInviteResponse] =
    new Writes[MeetingRoomInviteResponse] {
      def writes(r: MeetingRoomInviteResponse): JsValue =
        Json.obj(
          "requestId"    -> r.requestId,
          "displayName"  -> r.displayName,
          "responseType" -> r.responseType.toString
        )
    }
}

case object VideoInvitationEvent {
  implicit val writes: Writes[VideoInvitationEvent] =
    new Writes[VideoInvitationEvent] {
      def writes(vie: VideoInvitationEvent): JsValue =
        vie match {
          case mi: MeetingRoomInvite           => Json.toJson(mi)
          case ack: MeetingRoomInviteAckReply  => Json.toJson(ack)
          case resp: MeetingRoomInviteResponse => Json.toJson(resp)
        }
    }
}
