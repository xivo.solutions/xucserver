package services.config

import org.apache.pekko.actor.{Actor, ActorLogging, Scheduler}
import org.apache.pekko.pattern.RetrySupport.retry
import com.google.inject.Inject
import controllers.helpers.{RequestError, RequestSuccess}
import services.config.ConfigInitializer.UpdateQueue
import services.config.ConfigServiceManager._
import xivo.events.cti.models.{
  CtiStatusLegacyMapWrapper,
  CtiStatusMapWrapper,
  CtiStatusTraits
}
import xivo.models.{
  AgentConfigUpdate,
  IceServer,
  MobileAppConfig,
  QueueConfigUpdate
}
import xivo.xuc.ConfigServerConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object ConfigServiceManager {
  case class ExportQualificationsCsv(
      queueId: Long,
      fromDate: String,
      toDate: String
  )
  case object GetQueueConfigAll
  case object GetAgentConfigAll
  case object GetIceServer
  case object GetCtiStatuses
  case object GetMobileConfig
}

class ConfigServiceManager @Inject() (
    configRequester: ConfigServerRequester,
    configServerConfig: ConfigServerConfig
) extends Actor
    with ActorLogging
    with CtiStatusTraits {
  implicit val scheduler: Scheduler = context.system.scheduler

  log.info(s"Starting ConfigServiceManager $self")

  def receive: PartialFunction[Any, Unit] = {
    case ExportQualificationsCsv(queueId, fromDate, toDate) =>
      val theSender = sender()
      configRequester
        .exportQualificationsCsv(queueId, fromDate, toDate)
        .onComplete({
          case Success(result) => theSender ! RequestSuccess(result)
          case Failure(e)      => theSender ! RequestError(e.getMessage)
        })

    case GetQueueConfigAll =>
      val theSender = sender()
      retry[List[QueueConfigUpdate]](
        () => configRequester.getQueueConfigAll,
        configServerConfig.retryCount,
        configServerConfig.retryDelay
      ).onComplete({
        case Success(result) =>
          result.foreach(theSender ! UpdateQueue(_))
        case Failure(e) =>
          log.error(
            s"Failed to retrieve all queue configuration from configuration server: ${e.getMessage}"
          )
      })

    case GetAgentConfigAll =>
      val theSender = sender()
      retry[List[AgentConfigUpdate]](
        () => configRequester.getAgentConfigAll,
        configServerConfig.retryCount,
        configServerConfig.retryDelay
      ).onComplete({
        case Success(agentConfigUpdate) =>
          agentConfigUpdate.foreach(acu => theSender ! acu)
        case Failure(e) =>
          log.error(
            s"Failed to retrieve all agent configuration from configuration server: ${e.getMessage}"
          )
      })

    case GetIceServer =>
      val theSender = sender()
      retry[IceServer](
        () => configRequester.getIceServer,
        configServerConfig.retryCount,
        configServerConfig.retryDelay
      ).onComplete({
        case Success(iceServer) =>
          theSender ! iceServer
        case Failure(e) =>
          log.error(
            s"Failed to retrieve ICE server from configuration server: ${e.getMessage}"
          )
      })

    case GetCtiStatuses =>
      val theSender = sender()
      retry[CtiStatusMap](
        () => configRequester.getCtiStatuses,
        configServerConfig.retryCount,
        configServerConfig.retryDelay
      ).onComplete({
        case Success(ctiStatuses) =>
          val ctiStatusWrapper = CtiStatusMapWrapper(ctiStatuses)
          theSender ! ctiStatusWrapper
        case Failure(e) =>
          log.error(
            s"Failed to retrieve ICE server from configuration server: ${e.getMessage}"
          )
      })
      retry[CtiStatusLegacyMap](
        () => configRequester.getCtiStatusesLegacy,
        configServerConfig.retryCount,
        configServerConfig.retryDelay
      ).onComplete({
        case Success(ctiStatusesLegacy) =>
          theSender ! CtiStatusLegacyMapWrapper(ctiStatusesLegacy)
        case Failure(e) =>
          log.error(
            s"Failed to retrieve ICE server from configuration server: ${e.getMessage}"
          )
      })

    case GetMobileConfig =>
      val theSender = sender()
      retry[MobileAppConfig](
        () => configRequester.checkValidMobileAppSetup,
        configServerConfig.retryCount,
        configServerConfig.retryDelay
      ).onComplete({
        case Success(mobileCfg) =>
          theSender ! mobileCfg
        case Failure(e) =>
          log.error(
            s"Failed to retrieve mobile App from configuration server: ${e.getMessage}"
          )
      })

    case unknown => log.info(s"Unknown message received: $unknown")
  }

}
