package services.config

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import org.apache.pekko.pattern.pipe
import com.google.inject.Inject
import com.google.inject.name.Named
import helpers.JmxActorSingletonMonitor
import models.{XivoUserDao, XucUser}
import org.xivo.cti.CtiMessage
import org.xivo.cti.message.{AgentStatusUpdate, IpbxCommandResponse}
import org.xivo.cti.model.Availability
import play.api.libs.concurrent.InjectedActorSupport
import services.agent.AgentAction
import services.request.{AgentActionRequest, BaseRequest}
import services.{ActorIds, Start}
import xivo.events.{AgentError, AgentLoginError, AgentQueues}
import xivo.network.CtiLinkKeepAlive.{StartKeepAlive, StopKeepALive}
import xivo.network.{CtiLink, CtiLinkKeepAlive, LoggedOn}
import xivo.websocket.{LinkState, LinkStatusUpdate}
import xivo.xuc.XucBaseConfig

import scala.concurrent.ExecutionContext.Implicits.global
import helpers.JmxStringMetric
import scala.util.Try

object ConfigManager {
  case object PublishUserPhoneStatuses
  case class InitConfig(ctiLink: ActorRef)
}

trait ServicesActors {
  private[config] val ctiLink: ActorRef
  private[config] val agentService: ActorRef

  def createServices: Unit
}

class ConfigManager @Inject() (
    agentActionFactory: AgentAction.Factory,
    @Named(ActorIds.AgentManagerId) val agentManager: ActorRef,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    @Named(ActorIds.StatusPublishId) statusPublish: ActorRef,
    xucConfig: XucBaseConfig,
    xivoUserDao: XivoUserDao
) extends Actor
    with ActorLogging
    with InjectedActorSupport
    with JmxActorSingletonMonitor {

  import ConfigManager.{InitConfig, PublishUserPhoneStatuses}

  val jmxState: Try[JmxStringMetric] = jmxBean.addString("State", "Loading")

  override def preStart(): Unit = {
    log.info(s"Starting ConfigManager $self")
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))

    context.become(initializing orElse ignoreUnknown("preStart"))
    jmxState.set("Initializing")
    xivoUserDao
      .getCtiUserByLogin(xucConfig.EventUser)
      .map(xivoUser => XucUser(xucConfig.EventUser, xivoUser))
      .pipeTo(self)
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  def create: IpbxCommandResponse => AgentLoginError = AgentError.create

  def receive: Receive = Actor.emptyBehavior

  def ignoreUnknown(state: String): Receive = { case unknown =>
    log.warning(s"Ignoring Message $unknown on context ConfigManager.$state")
  }

  def initializing: Receive = { case user: XucUser =>
    log.info(s"Starting CtiLink for $user")
    val ctiLink: ActorRef = context.actorOf(
      CtiLink.xucprops(user.username, xucConfig),
      ActorIds.ctiLinkActorName(user.username)
    )
    val agentActionService =
      injectedChild(agentActionFactory(ctiLink), "agentAction")
    val ctiKeepAlive: ActorRef =
      context.actorOf(
        CtiLinkKeepAlive.props(
          user.username,
          xucConfig.XivoCtiKeepAliveInterval
        )
      )
    context.become(
      ctiLinkDown(
        user,
        ctiLink,
        agentActionService,
        ctiKeepAlive
      ) orElse ignoreUnknown("initializing")
    )
    jmxState.set("CtiLinkDown")
  }

  def ctiLinkDown(
      user: XucUser,
      ctiLink: ActorRef,
      agentActionService: ActorRef,
      ctiKeepAlive: ActorRef
  ): Receive = { case LinkStatusUpdate(LinkState.up) =>
    log.info(s"CtiLink:LinkStatusUpdate(up) for $user")
    ctiLink ! Start(user)
    context.become(
      ctiLinkStarting(
        user,
        ctiLink,
        agentActionService,
        ctiKeepAlive
      ) orElse ignoreUnknown("down")
    )
    jmxState.set("CtiLinkStarting")
  }

  def ctiLinkStarting(
      user: XucUser,
      ctiLink: ActorRef,
      agentActionService: ActorRef,
      ctiKeepAlive: ActorRef
  ): Receive = {
    case ln: LoggedOn =>
      log.info(s"CtiLink:LoggedOn ${ln.user}")
      configDispatcher ! InitConfig(ctiLink)
      ctiKeepAlive ! StartKeepAlive(ln.userId, ctiLink)
      context.become(
        ready(
          user,
          ctiLink,
          agentActionService,
          ctiKeepAlive
        ) orElse ignoreUnknown("starting")
      )
      jmxState.set("Ready")
    case LinkStatusUpdate(LinkState.up) =>
      log.warning(
        s"Unexpected LinkStatusUpdate(up) received for $user, restarting keepAlive for Cti in context ConfigManager.ctiLinkStarting"
      )
      ctiKeepAlive ! StopKeepALive
      ctiKeepAlive ! StartKeepAlive(user.xivoUser.id.toString, ctiLink)
      context.become(
        ready(
          user,
          ctiLink,
          agentActionService,
          ctiKeepAlive
        ) orElse ignoreUnknown("starting")
      )
      jmxState.set("Ready")
  }

  def ready(
      user: XucUser,
      ctiLink: ActorRef,
      agentActionService: ActorRef,
      ctiKeepAlive: ActorRef
  ): Receive = {

    case agentStatus: AgentStatusUpdate
        if agentStatus.getStatus.getAvailability != Availability.LOGGED_OUT =>
      val agentQueues = AgentQueues(agentStatus)
      log.debug(s"$agentQueues")
      agentManager ! agentQueues

    case agentError: IpbxCommandResponse =>
      agentActionService ! create(agentError)

    case message: CtiMessage =>
      log.debug(s"CtiMessage $message")
      configDispatcher ! message

    case PublishUserPhoneStatuses =>
      log.debug("Request publish user phone statuses")
      configDispatcher ! PublishUserPhoneStatuses

    case userPhoneStatus: UserPhoneStatus =>
      log.debug("Received userPhoneStatus: " + userPhoneStatus)
      statusPublish ! userPhoneStatus

    case BaseRequest(r: ActorRef, a: AgentActionRequest) =>
      agentActionService ! BaseRequest(r, a)

    case LinkStatusUpdate(LinkState.down) =>
      log.info(s"LinkStatusUpdate(down) for $user")
      context.become(
        ctiLinkDown(
          user,
          ctiLink,
          agentActionService,
          ctiKeepAlive
        ) orElse ignoreUnknown("ready")
      )
      jmxState.set("CtiLinkDown")

    case unknown =>
      log.info(
        s"Uknown message received: $unknown  in context  ConfigManager.ready"
      )
  }

}
