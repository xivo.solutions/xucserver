package services.config

import org.apache.pekko.actor.{Actor, ActorRef}
import com.google.inject.Inject
import com.google.inject.name.Named
import helpers.FastLogging
import org.asteriskjava.manager.action.{
  ExtensionStateAction,
  ExtensionStateListAction
}
import org.asteriskjava.manager.event.ExtensionStatusEvent
import org.asteriskjava.manager.response.ExtensionStateResponse
import services.{ActorIds, XucAmiBus}
import services.XucAmiBus.{
  AmiAction,
  AmiConnected,
  AmiExtensionStatusEvent,
  AmiResponse,
  AmiType
}
import services.config.ExtensionManager.GetExtensionStatus

object ExtensionManager {
  case object RetryLoadExtensionState
  case class GetExtensionStatus(number: String)
}

class ExtensionManager @Inject() (
    xucAmiBus: XucAmiBus,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef
) extends Actor
    with FastLogging {

  def isValid(evt: ExtensionStateResponse): Boolean =
    Option(evt.getStatus).filter(_ >= 0).isDefined

  override def preStart(): Unit = {
    log.info(s"$self starting")
    xucAmiBus.subscribe(self, AmiType.AmiResponse)
    xucAmiBus.subscribe(self, AmiType.AmiService)
    context.become(receive)
  }

  override def postStop(): Unit = {
    xucAmiBus.unsubscribe(self)
  }

  override def receive: PartialFunction[Any, Unit] = {
    case AmiConnected(mds) =>
      xucAmiBus.publish(
        AmiAction(new ExtensionStateListAction(), None, Some(self), Some(mds))
      )

    case GetExtensionStatus(number: String) =>
      log.debug(s"Requesting status for $number")
      xucAmiBus.publish(
        AmiAction(new ExtensionStateAction(number, ""), None, Some(self))
      )

    case AmiResponse(
          (
            evt: ExtensionStateResponse,
            Some(AmiAction(_: ExtensionStateAction, _, _, _))
          )
        ) if isValid(evt) =>
      val exs: ExtensionStatusEvent = new ExtensionStatusEvent(this)
      exs.setHint(evt.getHint)
      exs.setStatus(evt.getStatus)
      exs.setStatustext(evt.getStatusText)
      exs.setExten(evt.getExten)
      log.debug(s"Received status ${exs.getStatus} for ${exs.getExten}")

      configDispatcher ! AmiExtensionStatusEvent(exs)
  }
}
