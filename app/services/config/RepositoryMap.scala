package services.config

import org.apache.pekko.actor.{Actor, ActorLogging}
import services.XucEventBus
import services.XucEventBus.{Topic, XucEvent}

import scala.concurrent.{ExecutionContext, Future}

sealed trait RepositoryMessage[+K, +V]

case class GetEntry[K, V](key: K)           extends RepositoryMessage[K, V]
case class SetEntry[K, V](key: K, value: V) extends RepositoryMessage[K, V]
case class DeleteEntry[K, V](key: K)        extends RepositoryMessage[K, V]
case object DeleteAllEntries                extends RepositoryMessage[Nothing, Nothing]
case object GetAllEntries                   extends RepositoryMessage[Nothing, Nothing]

case class Entry[K, V](key: K, value: Option[V])

object RepositoryMap {}

abstract class RepositoryMap[K, V] extends Actor with ActorLogging {

  def load(implicit ec: ExecutionContext): Future[Map[K, V]]
  def update(k: K, oldValue: Option[V], newValue: V): V = newValue
  def updated(k: K, v: Option[V]): Unit                 = {}
  def receiveCommand: Receive                           = Actor.emptyBehavior
  def mapResponse(e: Entry[K, V]): Any                  = e
  def mapResponse(m: Map[K, V]): Any                    = m

  protected var repository: Map[K, V] = Map.empty

  context.become(receive orElse receiveCommand)

  import context.dispatcher
  load.foreach(_.foreach(e => self ! SetEntry(e._1, e._2)))

  def handle(m: RepositoryMessage[K, V]): Unit =
    m match {
      case GetAllEntries =>
        log.debug(s"${sender()} ! AllEntries")
        sender() ! mapResponse(repository)
      case DeleteAllEntries =>
        log.debug(s"${sender()} ! DeleteAllEntries")
        repository = Map.empty
      case GetEntry(k) =>
        log.debug(s"${sender()} ! GetEntry($k)")
        sender() ! mapResponse(Entry(k, repository.get(k)))
      case SetEntry(k, v) =>
        log.debug(s"${sender()} ! SetEntry($k, $v)")
        if (!"".equals(k.toString)) {
          val newV = update(k, repository.get(k), v)
          repository = repository.updated(k, newV)
          updated(k, Some(newV))
        } else {
          log.warning(s"Entry with empty key was not added to cache!")
        }
      case DeleteEntry(k) =>
        log.debug(s"${sender()} ! DeleteEntry($k)")
        repository = repository.view.filterKeys(_ != k).toMap
        updated(k, None)
    }

  final override def receive: PartialFunction[Any, Unit] = {
    case msg: RepositoryMessage[K, V] => handle(msg)
  }

  def command(e: Entry[K, V])(implicit ec: ExecutionContext): Unit = {
    val msg = e.value match {
      case Some(v) => SetEntry(e.key, v)
      case None    => DeleteEntry(e.key)
    }
    self ! msg
  }
}

trait PublishToBus[K, V] {
  this: RepositoryMap[K, V] =>

  def eventBus: XucEventBus
  def topic: Topic

  override def updated(k: K, v: Option[V]): Unit =
    eventBus.publish(XucEvent(topic, mapResponse(Entry(k, v))))

}
