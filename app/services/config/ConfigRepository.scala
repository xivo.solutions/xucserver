package services.config

import com.google.inject.{ImplementedBy, Inject, Singleton}
import models.*
import org.joda.time.DateTime
import org.json.JSONObject
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.{AgentConfigUpdate as _, QueueConfigUpdate as _, *}
import org.xivo.cti.model.{Meetme, PhoneHintStatus}
import play.api.Logger
import play.api.libs.functional.syntax.*
import play.api.libs.json.*
import services.agent.{AgentStatistic, Statistic}
import services.config
import services.config.ConfigDispatcher.{
  DeviceType,
  LineConfigQueryById,
  LineConfigQueryByNb,
  TypeDefaultDevice,
  TypePhoneDevice
}
import services.config.ConfigRepository.{QueueCalls, WaitingCallsStatistics}
import services.video.model.{UserVideoEvent, VideoEvents, VideoStatusEvent}
import xivo.events.AgentState.{AgentLoggedOut, AgentReady}
import xivo.events.cti.models.{CtiStatus, CtiStatusLegacy, CtiStatusTraits}
import xivo.events.{AgentState, PhoneHintStatusEvent}
import xivo.models.Agent.*
import xivo.models.*
import xivo.models.MembershipStatus.{Available, Exited}
import xivo.xuc.{IceServerConfig, SipConfig}
import xivo.xucami.models.QueueCall

import java.time.Instant
import java.util.Base64
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.*
import scala.concurrent.{Await, Future}
import scala.jdk.CollectionConverters.*
import scala.util.Try

case class RichPhone(
    val phoneNumber: String,
    val currentStatus: PhoneHintStatus
)
case class AgentDirectoryEntry(agent: Agent, state: AgentState)
object AgentDirectoryEntry {
  implicit val agentDirectoryEntryWrites: Writes[AgentDirectoryEntry] =
    ((__ \ "agent").write[Agent] and
      (__ \ "agentState").write[AgentState])(ade =>
      (
        ade.agent,
        ade.state
      )
    )
}

trait AgentRepository {
  this: ConfigRepository & QueueMemberRepository =>

  private[config] var agents: Map[Agent.Id, Agent]                   = Map()
  private[config] var userAgentIndex: Map[Long, Agent.Id]            = Map()
  private[config] var agentStatistics: Map[Agent.Id, AgentStatistic] = Map()

  def addAgent(agent: Agent): Unit = {
    agents = agents + (agent.id                     -> agent)
    userAgentIndex = userAgentIndex + (agent.userId -> agent.id)
  }

  def loadAgent(id: Agent.Id): Option[Unit] = {
    agentFactory.getById(id).map(addAgent)
  }

  def getAgent(agentId: Agent.Id): Option[Agent] = agents.get(agentId)

  def getAgent(number: Agent.Number): Option[Agent] =
    agents.values.toList.find(agent => agent.number == number)

  def getAgentByUserId(userId: Long): Option[Agent] =
    userAgentIndex.get(userId).flatMap(getAgent)

  def getAgents(): List[Agent] = agents.values.toList

  def getAgents(groupId: Long, queueId: Long, penalty: Int): List[Agent] = {
    for {
      ag <- agents.values.toList.filter(_.groupId == groupId)
      qm <-
        agentQueueMembers.filter(_ == AgentQueueMember(ag.id, queueId, penalty))
    } yield ag
  }

  def getAgentsNotInQueue(groupId: Long, queueId: Long): List[Agent] = {
    for {
      agent <- agents.values.toList.filter(_.groupId == groupId)
      if agentNotInQueue(agent, queueId)
    } yield agent
  }

  def updateAgentStatistic(agStat: AgentStatistic): Unit = {
    def merge(
        stats1: List[Statistic],
        stats2: List[Statistic]
    ): List[Statistic] = {
      (stats1 ::: stats2)
        .foldLeft(Map[String, Statistic]()) {
          case (map, Statistic(name, value)) =>
            map.updated(name, Statistic(name, value))
        }
        .values
        .toList
    }
    agentStatistics.get(agStat.agentId) match {
      case Some(repoAgStat) =>
        merge(repoAgStat.statistics, agStat.statistics)
        agentStatistics = agentStatistics + (agStat.agentId -> AgentStatistic(
          agStat.agentId,
          merge(repoAgStat.statistics, agStat.statistics)
        ))
      case None =>
        agentStatistics = agentStatistics + (agStat.agentId -> agStat)
    }
  }

  def getAgentStatistics: List[AgentStatistic] = agentStatistics.values.toList

  def groupIdForAgent(id: Id): Option[Long] = agents.get(id).map(_.groupId)

  def convertAgentConfigUpdateToAgent(agentConfig: AgentConfigUpdate): Agent =
    Agent(
      agentConfig.id,
      agentConfig.firstname,
      agentConfig.lastname,
      agentConfig.number,
      agentConfig.context,
      agentConfig.numgroup,
      agentConfig.userid.getOrElse(0L)
    )
}

trait QueueMemberRepository {
  this: config.ConfigRepository =>

  private[config] var agentQueueMembers: List[AgentQueueMember] = List()

  def getAgentQueueMembers(): List[AgentQueueMember] = agentQueueMembers

  def agentNotInQueue(agent: Agent, queueId: Long): Boolean =
    !agentQueueMembers.exists { case AgentQueueMember(agentId, qId, _) =>
      agentId == agent.id && qId == queueId
    }

  def queueMemberExists(agqm: AgentQueueMember): Boolean =
    agentQueueMembers.contains(agqm)

  private def filterQueueMember(agentId: Agent.Id, queueId: Long) =
    agentQueueMembers.filter(agqm =>
      agqm.agentId != agentId || queueId != agqm.queueId
    )

  def filterOutQueueMember(agentId: Agent.Id): List[AgentQueueMember] =
    agentQueueMembers.filter(agqm => agqm.agentId == agentId)

  def getAgentQueueMembersToRemove(
      updatedAgentQueueMember: List[AgentQueueMember]
  ): List[AgentQueueMember] = {
    updatedAgentQueueMember
      .flatMap(agqm => filterOutQueueMember(agqm.agentId))
      .filterNot(updatedAgentQueueMember.toSet)
  }

  def updateOrAddQueueMembers(agentQueueMember: AgentQueueMember): Unit =
    agentQueueMembers = agentQueueMember :: filterQueueMember(
      agentQueueMember.agentId,
      agentQueueMember.queueId
    )

  def removeQueueMember(agentQueueMember: AgentQueueMember): Unit = {
    agentQueueMembers =
      filterQueueMember(agentQueueMember.agentId, agentQueueMember.queueId)
  }

  def getAgentQueueMember(
      qMconfig: QueueMemberConfigUpdate
  ): Option[AgentQueueMember] =
    for {
      agent       <- getAgent(qMconfig.getAgentNumber)
      queueConfig <- getQueue(qMconfig.getQueueName)
    } yield AgentQueueMember(agent.id, queueConfig.id, qMconfig.getPenalty)

  def getQueuesForAgent(agentId: Agent.Id): List[AgentQueueMember] =
    agentQueueMembers.filter(_.agentId == agentId)

  def convertAgentConfigUpdateToQueueMember(
      agentConfig: AgentConfigUpdate
  ): List[AgentQueueMember] =
    agentConfig.member.map(member =>
      AgentQueueMember(agentConfig.id, member.queue_id, member.penalty)
    )
}

trait GroupConfigsRepository {
  this: config.ConfigRepository =>

  private[config] var groupsConfig: List[GroupConfig] = List()

  def getGroups: List[GroupConfig] = groupsConfig.sortBy(_.groupId)

  def getUserGroups(userId: Long): List[UserGroup] =
    groupsConfig.flatMap { ug =>
      ug.users.find(_.userId == userId).map { user =>
        UserGroup(
          ug.groupId,
          ug.groupName,
          ug.groupNumber,
          user.membershipStatus
        )
      }
    }

  def updateGroupConfig(groupMember: GroupConfig): Unit =
    groupsConfig = (groupMember :: groupsConfig).distinctBy(_.groupId)

  def getGroupMembers(groupId: Long): List[UserGroupMember] =
    groupsConfig
      .find(_.groupId == groupId)
      .map(_.users)
      .getOrElse(List.empty)

  def getGroup(groupId: Long): Option[GroupConfig] =
    groupsConfig
      .find(_.groupId == groupId)

  def getGroupByName(name: String): Option[GroupConfig] =
    groupsConfig
      .find(_.groupName == name)

  def removeGroup(groupId: Long): Unit =
    groupsConfig = groupsConfig.filter(_.groupId != groupId)

  def addGroup(groupConfig: GroupConfig): Unit =
    groupsConfig = groupConfig :: groupsConfig

  def setUserStatus(
      userId: Long,
      groupId: Long,
      status: MembershipStatus
  ): Unit =
    groupsConfig = groupsConfig.map { group =>
      if (group.groupId == groupId) {
        val updatedUsers = group.users.collect {
          case user if user.userId == userId =>
            user.copy(membershipStatus = status)
          case user => user
        }
        group.copy(users = updatedUsers)
      } else group
    }

  def loadGroups(): Unit = groupsConfig = groupMemberFactory.all()
}

@ImplementedBy(classOf[ConfigRepository])
trait QueueRepository {
  this: config.ConfigRepository =>
  val OutBoundQueuePrefix                                    = "out"
  protected[config] var queues: Map[Long, QueueConfigUpdate] = Map()

  private def isAnOutboundQueue(qc: QueueConfigUpdate) =
    qc.name.startsWith(OutBoundQueuePrefix)

  def getQueues(): List[QueueConfigUpdate] = queues.values.toList

  def getQueue(queueId: Long): Option[QueueConfigUpdate] = queues.get(queueId)

  def getQueue(name: String): Option[QueueConfigUpdate] =
    queues.values.find(qcf => qcf.name == name)

  def updateQueueConfig(qConfig: QueueConfigUpdate): Unit =
    queues += (qConfig.id -> qConfig)

  def getOutboundQueue(queueIds: List[Long]): Option[QueueConfigUpdate] =
    queues.view
      .filterKeys(Set(queueIds*))
      .toMap
      .values
      .find(isAnOutboundQueue)
}

trait LineRepository {
  this: ConfigRepository =>
  var userLines: Map[Long, Line]           = Map()
  var agentLines: Map[Agent.Id, Line]      = Map()
  var linePhoneNbs: Map[Line.Id, String]   = Map()
  var linesUser: Map[Line.Id, Long]        = Map()
  var lineDevice: Map[Line.Id, DeviceType] = Map()

  def getLineForPhoneNb(phoneNb: String): Option[Id] =
    linePhoneNbs.filter(_._2 == phoneNb).keys.headOption

  def getPhoneNbfromInterface(iFace: String): Option[Number] =
    userLines
      .filter(_._2.interface == iFace)
      .values
      .flatMap(l => l.number)
      .headOption

  def getLineForUser(userId: Long): Option[Line] = userLines.get(userId)
  def getLineForUser(username: String): Option[Line] =
    getCtiUser(username).flatMap(user => userLines.get(user.id))
  def getLineForAgent(agentId: Agent.Id): Option[Line] = agentLines.get(agentId)
  def getLineConfig(lcr: LineConfigQueryByNb): Option[LineConfig] =
    getLineForPhoneNb(lcr.number) map (id =>
      LineConfig(
        id.toString,
        lcr.number,
        lineFactory.get(id.toInt),
        this.sipConfig.sipPort
      )
    )
  def getDeviceForLine(lineId: Line.Id): Option[DeviceType] =
    lineDevice.get(lineId)

  def getLineConfig(lcr: LineConfigQueryById): Option[LineConfig] = {
    val deviceType: DeviceType =
      getDeviceForLine(lcr.id).getOrElse(TypeDefaultDevice)
    linePhoneNbs
      .get(lcr.id)
      .map(nb =>
        LineConfig(
          lcr.id.toString,
          nb,
          lineFactory.get(lcr.id, deviceType),
          this.sipConfig.sipPort
        )
      )
  }

  def getLineUser(lineId: Line.Id): Option[XivoUser] = {
    log.debug(s"getLineUser: get user for lineId $lineId")
    val userId = linesUser.get(lineId)
    if (userId.isEmpty) {
      log.debug(
        s"getLineUser: no userId was found for lineId $lineId in map linesUser - current map contains ${linesUser.size} lines"
      )
    }
    val user = userId.flatMap(getCtiUser)
    if (user.isEmpty) {
      log.debug(
        s"getLineUser: no user was found for userId $userId in map users - current map contains ${ctiUsers.size} users"
      )
    }
    user
  }

  def getLineByEndpoint(endpoint: Endpoint): Option[Line] = {
    endpoint match {
      case e: SipEndpoint => lineFactory.get(e)
    }
  }

  protected def updateAgentLine(agentId: Agent.Id, line: Line): Unit =
    agentLines = agentLines + (agentId -> line)
  protected[config] def updateUserLine(userId: Long, line: Line): Unit =
    userLines = userLines + (userId -> line)

  protected[config] def updateLinePhoneNb(
      lineId: Line.Id,
      phoneNb: String
  ): Unit = {
    log.debug(s"updating phone $phoneNb with id $lineId")
    linePhoneNbs.filter(_._2 == phoneNb).keys.headOption.foreach { id =>
      {
        linePhoneNbs = linePhoneNbs - id
        log.info(s"phone $phoneNb already exists with id $lineId : removing")
      }
    }

    linePhoneNbs = linePhoneNbs + (lineId -> phoneNb)
  }
  protected[config] def updateLineUser(lineId: Line.Id, userId: Long): Unit =
    linesUser = linesUser + (lineId -> userId)
  protected[config] def updateLineDevice(
      lineId: Line.Id,
      deviceType: DeviceType
  ): Unit = {
    val previousDevice = lineDevice.get(lineId)
    if (previousDevice.isDefined && !previousDevice.contains(deviceType)) {
      getLineUser(lineId)
        .foreach(user =>
          configServerRequester.setUserPreference(
            user.id,
            UserPreferenceKey.PreferredDevice,
            deviceType.name,
            UserPreferenceType.StringType
          )
        )
    }
    lineDevice = lineDevice + (lineId -> deviceType)
  }

  def loadUserLine(userId: Long, lineId: Long): Option[Unit] = {
    lineFactory.get(lineId.toInt).map { line =>
      val futureDevType: Future[DeviceType] =
        if (line.ua)
          configServerRequester
            .getUserPreference(userId, UserPreferenceKey.PreferredDevice)
            .map(_.value match {
              case TypePhoneDevice.name   => TypePhoneDevice
              case TypeDefaultDevice.name => TypeDefaultDevice
              case _                      => TypeDefaultDevice
            })
            .recover { case t =>
              log.debug(
                s"Unable to get user $userId default device for line $lineId from database",
                t
              )
              TypeDefaultDevice
            }
        else if (line.webRTC)
          Future.successful(TypeDefaultDevice)
        else
          Future.successful(TypePhoneDevice)
      Try(Await.result(futureDevType, 5000.millis))
        .foreach(deviceType => updateLineDevice(lineId, deviceType))
      updateUserLine(userId, line)
    }
  }
}

trait CtiUserRepository {
  this: ConfigRepository =>
  protected[config] var userAgents: Map[Long, Agent.Id] = Map()
  protected[config] var ctiUsers: Map[Long, XivoUser]   = Map()

  def updateUser(user: XivoUser): Unit = {
    log.debug(
      s"updateUser: updating users map for user (id:${user.id} username:${user.username})"
    )
    ctiUsers.values
      .find(
        _.username == user.username && !user.username
          .map(_.trim)
          .forall(_.isEmpty)
      )
      .foreach(u =>
        ctiUsers = {
          log.debug(
            s"updateUser: remove user from map users (id:${u.id} username:${u.username})"
          )
          ctiUsers - u.id
        }
      )

    ctiUsers = ctiUsers + (user.id -> user)
  }

  def getCtiUser(id: Long): Option[XivoUser] = ctiUsers.get(id)
  def getCtiUser(username: String): Option[XivoUser] = {
    if (username.nonEmpty) {
      val user = ctiUsers.values.find(_.username.contains(username))
      if (user.isEmpty && username.contains('@')) {
        val userWithoutDomain = Some(username.split('@')(0))
        ctiUsers.values.find(_.username == userWithoutDomain)
      } else user
    } else {
      None
    }
  }
  def getCtiUser(username: String, password: String): Option[XivoUser] =
    ctiUsers
      .filter { case (id, user) =>
        user.username.contains(username) && user.password.contains(password)
      }
      .values
      .toList
      .headOption

  def getCtiUserDisplayName(username: String): Option[String] = {
    getCtiUser(username).map(user => user.fullName)
  }

  def getCtiUserByNumber(number: String): Option[XivoUser] =
    linePhoneNbs
      .find(_._2 == number)
      .flatMap(t => linesUser.get(t._1))
      .flatMap(userId => ctiUsers.get(userId))

  def userNameFromPhoneNb(nb: String): Option[String] =
    linePhoneNbs
      .find(t => t._2 == nb)
      .flatMap(t => linesUser.get(t._1))
      .flatMap(userId => ctiUsers.get(userId))
      .map(_.username.getOrElse(""))

  protected def updateUserAgent(userId: Long, agentId: Agent.Id): Unit =
    userAgents = userAgents + (userId -> agentId)
  protected def getCtiUserIdForAgent(agentId: Agent.Id): Option[Id] =
    userAgents.filter(_._2 == agentId).keys.headOption

  def getCtiUserByAgentId(id: Agent.Id): Option[XivoUser] =
    for {
      agent <- agents.get(id)
      user  <- ctiUsers.get(agent.userId)
    } yield user
}

trait WebServiceUserRepository {
  this: ConfigRepository =>
  protected[config] var webServiceUsers: Map[String, WebServiceUser] = Map()

  def updateWebServiceUser(user: WebServiceUser): Unit = {
    log.debug(
      s"updateWebServiceUser: updating web service users map for web service user (login: ${user.login} name: ${user.name})"
    )
    webServiceUsers.values
      .find(u => u.login == user.login)
      .foreach(u =>
        webServiceUsers = {
          log.debug(
            s"updateWebServiceUser: remove web service user from map web service users (login: ${u.login} name: ${u.name})"
          )
          webServiceUsers - u.login
        }
      )

    webServiceUsers = webServiceUsers + (user.login -> user)
  }

  def getWebServiceUser(login: String): Option[WebServiceUser] = {
    if (login.nonEmpty) {
      webServiceUsers.values.find(_.login == login)
    } else {
      None
    }
  }

  def getWebServiceUsers: List[WebServiceUser] = {
    log.debug(s"getWebServiceUsers: ${webServiceUsers.values.toList}")
    webServiceUsers.values.toList
  }
}

@ImplementedBy(classOf[ConfigRepository])
trait IceRepository {
  this: config.ConfigRepository =>
  protected[config] var address: Option[String] = None

  def updateIceConfig(iceServer: IceServer): Unit = {
    log.info(
      s"Update IceConfig repository with address: ${iceServer.stunAddress.getOrElse("none")}"
    )
    address = iceServer.stunAddress
  }

  def createPassword(username: String, secret: String): String = {
    val key = new SecretKeySpec(secret.getBytes, "HmacSHA1")
    val mac = Mac.getInstance("HmacSHA1")
    mac.init(key)
    Base64.getEncoder.encodeToString(mac.doFinal(username.getBytes))
  }

  def getStunConfig: Option[StunConfig] = {
    if (turnServerConfig.stunEnable) {
      address.map(a => {
        StunConfig(List(s"stun:$a"))
      })
    } else None
  }

  def getTurnConfig: Option[TurnConfig] = {
    if (turnServerConfig.turnEnable) {
      address.map(a => {
        val ttl = turnServerConfig.turnSecretTtl
        val username = Instant.now
          .plus(java.time.Duration.ofSeconds(ttl))
          .getEpochSecond
          .toString

        TurnConfig(
          List(s"turn:$a", s"turns:$a"),
          username,
          createPassword(username, turnServerConfig.turnSecret),
          ttl
        )
      })
    } else None
  }

  def getIceConfig: IceConfig = {
    address
      .map(_ => IceConfig(getStunConfig, getTurnConfig))
      .getOrElse(IceConfig(None, None))
  }
}

object ConfigRepository {
  type QueueCalls = Map[String, QueueCall]

  case class WaitingCallsStatistics(
      numberOfWaitingCalls: Int,
      oldestWaitingFrom: DateTime
  ) {
    require(numberOfWaitingCalls > 0)
  }
}

@Singleton
class ConfigRepository @Inject() (
    val messageFactory: MessageFactory,
    val lineFactory: LineFactory,
    val agentFactory: AgentFactory,
    val agentQueueMemberFactory: AgentQueueMemberFactory,
    val groupMemberFactory: GroupMemberFactory,
    val configServerRequester: ConfigServerRequester,
    val turnServerConfig: IceServerConfig,
    val sipConfig: SipConfig
) extends AgentRepository
    with QueueMemberRepository
    with QueueRepository
    with LineRepository
    with CtiUserRepository
    with WebServiceUserRepository
    with IceRepository
    with CtiStatusTraits
    with GroupConfigsRepository {

  val LoggerName: Number = getClass.getName
  val log: Logger        = Logger(LoggerName)

  var richPhones: Map[String, PhoneHintStatus]                = Map()
  var videoStatuses: Map[String, VideoEvents.Event]           = Map()
  var agentStatuses: Map[Agent.Id, AgentState]                = Map()
  var ctiStatuses: CtiStatusMap                               = Map()
  var ctiStatusesLegacy: CtiStatusLegacyMap                   = Map()
  var userStatuses: Map[Int, UserStatusUpdate]                = Map()
  var userServices: Map[Int, UserServices]                    = Map()
  var extensionPatterns: Map[ExtensionName, ExtensionPattern] = Map()
  var meetmes: List[Meetme]                                   = List()
  var queueCalls: Map[String, QueueCalls]                     = Map()
  var mobileConfigIsValid: Boolean                            = false

  def onMeetmeUpdate(meetmes: List[Meetme]): Unit = this.meetmes = meetmes

  def onPhoneIds(list: PhoneIdsList): List[JSONObject] =
    buildRequestsFromids(list.getIds, messageFactory.createGetPhoneConfig)

  def onQueueIds(list: QueueIds): List[JSONObject] =
    buildRequestsFromids(list.getIds, messageFactory.createGetQueueConfig)

  def requestStatusOnAgentIds(list: AgentIds): List[JSONObject] =
    buildRequestsFromids(list.getIds, messageFactory.createGetAgentStatus)

  def onQueueMemberIds(qmIds: QueueMemberIds): List[JSONObject] =
    buildRequestsFromids(
      qmIds.getMemberIds,
      messageFactory.createGetQueueMemberConfig
    )

  def updateMobileConfigStatus(isValid: Boolean): Unit = {
    log.info(s"Updating mobile app config, setup is valid: $isValid")
    this.mobileConfigIsValid = isValid
  }

  private def buildRequestsFromids[T](
      jids: java.util.List[T],
      factory: String => JSONObject
  ): List[JSONObject] = {
    val ids = jids.asScala.toList
    for (id <- ids) yield factory(id.toString)
  }

  def onUserServicesUpdated(userId: Int, services: UserServices): Unit = {
    userServices = userServices + (userId -> services)
  }

  def onPhoneConfigUpdate(phoneConfig: PhoneConfigUpdate): Unit = {

    Logger(LoggerName).debug(
      "Phone config updated for line [" + phoneConfig.getId + "]"
    )
    updatePhoneLine(
      phoneConfig.getId.toLong,
      phoneConfig.getNumber,
      phoneConfig.getUserId.toLong
    )
  }

  def updateExtensionPattern(exten: ExtensionPattern): Unit = {
    extensionPatterns = extensionPatterns + (exten.name -> exten)
  }

  def getExtensionPattern(name: ExtensionName): Option[ExtensionPattern] =
    extensionPatterns.get(name)

  def updatePhoneLine(
      lineId: Line.Id,
      lineNumber: String,
      userId: Long
  ): Unit = {
    updateLinePhoneNb(lineId, lineNumber)
    updateLineUser(lineId, userId)
  }
  def onUserConfigUpdate(userConfig: UserConfigUpdated): Unit = {
    userConfig.agentId
      .filter(id => id != 0)
      .foreach(agentId => updateUserAgent(userConfig.userId, agentId))
  }

  def getUserStatus(userId: Int): Option[UserStatusUpdate] =
    userStatuses.get(userId)

  def getUserVideoStatus(userName: String): Option[VideoEvents.Event] =
    videoStatuses.get(userName)

  def getUserServices(userId: Int): Option[UserServices] =
    userServices.get(userId)

  def getCtiStatusesLegacy(idCtiStatus: Int): Option[List[CtiStatusLegacy]] =
    ctiStatusesLegacy.get(idCtiStatus.toString)

  def updateCtiStatusesLegacy(_ctiStatusesLegacy: CtiStatusLegacyMap): Unit = {
    ctiStatusesLegacy = _ctiStatusesLegacy
  }

  def getCtiStatuses(idCtiStatus: Int): Option[List[CtiStatus]] =
    ctiStatuses.get(idCtiStatus.toString)

  def updateCtiStatuses(_ctiStatuses: CtiStatusMap): Unit = {
    ctiStatuses = _ctiStatuses
  }

  def updatePhoneStatus(phoneNumber: String, status: Int): Unit = {
    PhoneHintStatus.getHintStatus(status) match {
      case PhoneHintStatus.ERROR =>
        log.info(s"Cannot update phone status for line $phoneNumber")
      case validStatus =>
        log.info(
          s"Updating phone status for line $phoneNumber with status $validStatus"
        )
        richPhones = richPhones + (phoneNumber -> validStatus)
    }
  }

  def updateVideoStatus(e: UserVideoEvent): Unit = {
    log.info(s"Updating video status for user ${e.fromUser}")
    videoStatuses =
      videoStatuses + (e.fromUser -> UserVideoEvent.matchStatusToEvent(
        e.status
      ))
  }

  def getMeetmeList: List[Meetme] = meetmes

  def getUserPhoneStatus(
      phoneNumber: String,
      status: Int
  ): Option[UserPhoneStatus] = {
    for {
      (lineId, _) <- linePhoneNbs.find(t => t._2 == phoneNumber)
      userid      <- linesUser.get(lineId)
      user        <- ctiUsers.get(userid)
      if PhoneHintStatus.getHintStatus(status) != PhoneHintStatus.ERROR
    } yield UserPhoneStatus(
      user.username.getOrElse(""),
      PhoneHintStatus.getHintStatus(status)
    )
  }

  def getUserPhoneStatus(
      phoneStatus: PhoneStatusUpdate
  ): Option[UserPhoneStatus] = {
    for {
      userid <- linesUser.get(phoneStatus.getLineId.toLong)
      user   <- ctiUsers.get(userid)
      if phoneStatus.getHintStatus != ""
    } yield UserPhoneStatus(
      user.username.getOrElse(""),
      PhoneHintStatus.getHintStatus(Integer.valueOf(phoneStatus.getHintStatus))
    )
  }

  def getPhoneStatus(phoneId: Int): Option[PhoneStatusUpdate] = {
    for {
      phoneNumber <- linePhoneNbs.get(phoneId)
      hint        <- richPhones.get(phoneNumber)
    } yield {
      val ps = new PhoneStatusUpdate
      ps.setLineId(Integer.valueOf(phoneId))
      ps.setHintStatus(hint.getHintStatus.toString)
      ps
    }
  }

  def getPhoneHintStatusEventByNumber(
      number: String
  ): Option[PhoneHintStatusEvent] =
    richPhones.get(number).map(e => PhoneHintStatusEvent(number, e))

  def getPhoneHintStatusByNumber(number: String): Option[PhoneHintStatus] =
    richPhones.get(number)

  def getPhoneNumberHintStatus(phoneId: Int): Option[PhoneHintStatusEvent] =
    linePhoneNbs.get(phoneId).flatMap(getPhoneHintStatusEventByNumber)

  def getAllUserPhoneStatuses: List[UserPhoneStatus] = {
    (for {
      (lineId, userId) <- linesUser
      phoneNumber      <- linePhoneNbs.get(lineId.toLong)
      user             <- ctiUsers.get(userId)
      hint             <- richPhones.get(phoneNumber)
    } yield UserPhoneStatus(user.username.getOrElse(""), hint)).toList
  }

  def getVideoStatus(user: String): Option[VideoStatusEvent] =
    videoStatuses.get(user).map(status => VideoStatusEvent(user, status))

  def getAgentQueueMemberRequest(agentNumber: String): List[JSONObject] = {
    for {
      agent <- agents.values.toList.filter(agent => agent.number == agentNumber)
      agqm  <- agentQueueMembers.filter(_.agentId == agent.id)
      queue <- queues.values.filter(_.id == agqm.queueId)
    } yield messageFactory.createGetQueueMemberConfig(
      s"Agent/${agent.number},${queue.name}"
    )
  }

  def onAgentState(agentState: AgentState): Unit = {
    val previousState = agentStatuses.get(agentState.agentId)
    agentStatuses += (agentState.agentId -> agentState)
    (previousState, agentState) match {
      case (Some(_: AgentLoggedOut) | None, agentLogin: AgentReady) =>
        getLineForPhoneNb(agentLogin.phoneNb) map { lineId =>
          lineFactory.get(lineId) map { line =>
            updateAgentLine(agentLogin.id, line)
            getCtiUserIdForAgent(agentLogin.id) map (userId =>
              updateUserLine(userId, line)
            )
          }
        }
      case _ =>
    }
  }

  def getAgentLoggedOnPhoneNumber(phoneNumber: String): Option[Agent.Id] = {
    agentStatuses
      .filter { case (id, state) =>
        state match {
          case state: AgentLoggedOut  => false
          case _ if phoneNumber != "" => state.phoneNb == phoneNumber
          case _                      => false
        }
      }
      .keys
      .headOption
  }

  def getAgentDirectory: List[AgentDirectoryEntry] = {
    for {
      agent      <- agents.values.toList
      agentState <- agentStatuses.get(agent.id)
    } yield AgentDirectoryEntry(agent, agentState)
  }

  def getAgentStates: List[AgentState] = agentStatuses.values.toList

  def getAgentState(agentId: Agent.Id): Option[AgentState] =
    agentStatuses.get(agentId)

  def onQueueCallReceived(
      queue: String,
      uniqueId: String,
      queueCall: QueueCall
  ): Unit = {
    queueCalls.get(queue) match {
      case None =>
        queueCalls = queueCalls + (queue -> Map(uniqueId -> queueCall))
      case Some(calls) =>
        queueCalls = queueCalls + (queue -> (calls + (uniqueId -> queueCall)))
    }
    Logger(LoggerName).trace(
      s"QUEUECALLS ADD queueName=$queue callId=$uniqueId call=$queueCall queueAfter=${queueCalls.get(queue)}"
    )
  }

  def onQueueCallFinished(queue: String, uniqueId: String): Unit = {

    def oldestCallId(calls: QueueCalls): Option[String] =
      calls.toList
        .sortBy(_._2.queueTime.getMillis)
        .headOption
        .map(_._1)

    def getUniqueId(calls: QueueCalls) =
      if (calls.contains(uniqueId)) Some(uniqueId)
      else oldestCallId(calls)

    for {
      calls <- queueCalls.get(queue)
      uid   <- getUniqueId(calls)
    } {
      queueCalls = queueCalls + (queue -> (calls - uid))
      Logger(LoggerName).trace(
        s"QUEUECALLS DEL queueName=$queue callId=$uniqueId queueAfter=${queueCalls.get(queue)}"
      )
    }

  }

  /** @return names of updated queues */
  def onQueueCallTransferred(
      fromUniqueId: String,
      toUniqueId: String
  ): Iterable[String] =
    for {
      (queue, calls) <- queueCalls
      queueCall      <- calls.get(fromUniqueId)
    } yield {
      queueCalls =
        queueCalls + (queue -> (calls - fromUniqueId + (toUniqueId -> queueCall)))
      Logger(LoggerName).trace(
        s"QUEUECALLS TRANSFER queueName=$queue from=$fromUniqueId to=$toUniqueId queueAfter=${queueCalls
            .get(queue)}"
      )
      queue
    }

  def findWaitingCallsStatistics(
      queue: String
  ): Option[WaitingCallsStatistics] =
    queueCalls
      .get(queue)
      .filter(_.nonEmpty)
      .map { queueCalls =>
        import com.github.nscala_time.time.OrderingImplicits.DateTimeOrdering
        val oldestStartedAt = queueCalls.values.map(_.queueTime).min
        WaitingCallsStatistics(queueCalls.size, oldestStartedAt)
      }

  def removeQueueCallsFromMds(mdsName: String): Unit = {
    queueCalls = queueCalls.view
      .mapValues { calls =>
        calls.filterNot { case (id, call) => call.mdsName == mdsName }
      }
      .filterNot { case (q, calls) => calls.isEmpty }
      .toMap
  }

  def getQueueCalls(queueId: Long): QueueCallList =
    queues.get(queueId) match {
      case Some(queue) =>
        queueCalls.get(queue.name) match {
          case Some(calls) => QueueCallList(queueId, calls.values.toList)
          case None        => QueueCallList(queueId, List())
        }
      case None => QueueCallList(queueId, List())
    }

  def getQueueCalls(queueName: String): Option[QueueCallList] =
    queues.values
      .find(q => q.name.equals(queueName))
      .map(queue => getQueueCalls(queue.id.toInt))

  def agentFromUsername(username: String): Option[Agent] =
    getCtiUser(username) match {
      case None => None
      case Some(user) =>
        userAgents.get(user.id) match {
          case None          => None
          case Some(agentId) => agents.get(agentId)
        }
    }

  def updateQueueCallCallerId(
      queueName: String,
      channelName: String,
      uniqueId: String,
      callIdNum: String,
      callIdName: String
  ): Option[QueueCall] = {
    getQueueCalls(queueName)
      .flatMap(_.calls.find(qc => qc.channel == channelName))
      .map(oldQueueCall =>
        oldQueueCall.copy(number = callIdNum, name = Some(callIdName))
      )
      .map { updatedQueueCall =>
        onQueueCallReceived(queueName, uniqueId, updatedQueueCall)
        updatedQueueCall
      }
  }

  def interfaceFromUsername(username: String): Option[String] =
    getCtiUser(username).flatMap(user =>
      userLines
        .get(user.id)
        .map(l =>
          if (l.ua) l.interface takeWhile { !_.equals('_') }
          else l.interface
        )
    )

  def phoneNumberForUser(username: String): Option[String] =
    getCtiUser(username)
      .flatMap(u => getLineForUser(u.id))
      .flatMap(l => linePhoneNbs.get(l.id))

  def isGroupOrQueue(
      name: String
  ): Option[Either[QueueConfigUpdate, GroupConfig]] = {
    getQueue(name)
      .map(Left(_))
      .orElse(
        getGroupByName(name)
          .map(Right(_))
      )
  }
}
