package services.config

import java.util.UUID
import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import com.google.inject.name.Named
import controllers.helpers.{RequestError, RequestSuccess}
import models.*
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsResult, JsValue, Json, Reads}
import services.request.PhoneRequest.Dial
import services.request.*
import services.{ActorIds, XucEventBus}
import xivo.events.{PhoneEvent, PhoneEventType}
import xivo.network.RecordingWS
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebSocketEvent}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

case class ImportCsvCallback(listUuid: String, csv: String)
object ImportCsvCallback {
  def validate(json: JsValue): JsResult[ImportCsvCallback] =
    json.validate[ImportCsvCallback]
  implicit val ImportCsvCallbackRead: Reads[ImportCsvCallback] = (
    (JsPath \ "listUuid").read[String] and
      (JsPath \ "csv").read[String]
  )(ImportCsvCallback.apply)

}
case class ExportTicketsCsv(listUuid: String)
object ExportTicketsCsv {
  def validate(json: JsValue): JsResult[ExportTicketsCsv] =
    json.validate[ExportTicketsCsv]
  implicit val ExportTicketsCsvRead: Reads[ExportTicketsCsv] = (
    (JsPath \ "listUuid").read[String].map { o => ExportTicketsCsv(o) }
  )
}

class CallbackManager @Inject() (
    configRequester: ConfigServerRequester,
    recordingWS: RecordingWS,
    bus: XucEventBus,
    configRepository: ConfigRepository,
    @Named(ActorIds.CtiRouterFactoryId)
    ctiRouterFactory: ActorRef
) extends Actor
    with ActorLogging {

  bus.subscribe(self, XucEventBus.allPhoneEventsTopic)

  override def receive: Receive = {
    case BaseRequest(ref, GetCallbackLists) =>
      configRequester.getCallbackLists.onComplete({
        case Success(list) => ref ! CallbackLists(list)
        case Failure(exception) =>
          ref ! WsContent(
            WebSocketEvent.createError(WSMsgType.Error, exception.getMessage)
          )
      })

    case BaseRequest(ref, FindCallbackRequestWithId(i, f)) =>
      configRequester
        .findCallbackRequest(f)
        .onComplete({
          case Success(resp) => ref ! FindCallbackResponseWithId(i, resp)
          case Failure(exception) =>
            ref ! WsContent(
              WebSocketEvent.createError(WSMsgType.Error, exception.getMessage)
            )
        })

    case BaseRequest(ref, GetCallbackPreferredPeriods) =>
      configRequester
        .getPreferredCallbackPeriods()
        .onComplete({
          case Success(list) => ref ! PreferredCallbackPeriodList(list)
          case Failure(exception) =>
            ref ! WsContent(
              WebSocketEvent.createError(WSMsgType.Error, exception.getMessage)
            )
        })

    case ImportCsvCallback(uuid, csv) =>
      val theSender = sender()
      configRequester
        .importCsvCallback(uuid, csv)
        .onComplete({
          case Success(_) =>
            configRequester.getCallbackLists.foreach(bus.publish)
            theSender ! RequestSuccess("CSV successfully imported")
          case Failure(e) => theSender ! RequestError(e.getMessage)
        })

    case TakeCallbackWithAgent(uuid, userName) =>
      configRepository
        .agentFromUsername(userName)
        .foreach(agent =>
          configRequester
            .takeCallback(uuid, agent)
            .onComplete({
              case Success(_) =>
                bus.publish(CallbackTaken(UUID.fromString(uuid), agent.id))
              case Failure(e) =>
                log.error(
                  s"Error taking the callback request $uuid with agent $agent",
                  e
                )
            })
        )

    case ReleaseCallback(uuid) =>
      configRequester
        .releaseCallback(uuid)
        .onComplete({
          case Success(_) =>
            bus.publish(CallbackReleased(UUID.fromString(uuid)))
          case Failure(e) =>
            log.error(s"Error releasing the callback request $uuid", e)
        })

    case BaseRequest(ref, ReleaseAllCallbacks(agentId)) =>
      configRequester
        .getTakenCallbacks(agentId)
        .onComplete({
          case Success(list) =>
            list.foreach(u => {
              log.info(s"For agent $agentId force release callback $u")
              self ! ReleaseCallback(u.toString)
            })
          case Failure(e) =>
            log.error(
              s"Error releasing pending callback requests for agentId $agentId",
              e
            )
        })

    case BaseRequest(
          ref,
          StartCallbackWithUser(requestUuid, number, username)
        ) =>
      configRepository.agentFromUsername(username) match {
        case None =>
          log.error(s"$username has no agent, cannot take a callback")
        case Some(agent) =>
          configRequester
            .getCallbackRequest(requestUuid)
            .foreach(rq =>
              recordingWS
                .createCallbackTicket(
                  CallbackTicket.fromRequest(rq, agent.number, configRepository)
                )
                .onComplete({
                  case Success(ticket) =>
                    context.actorSelection(
                      ActorIds.ctiRouterPath(ctiRouterFactory, username)
                    ) ! BaseRequest(
                      ref,
                      Dial(
                        number,
                        Map("CALLBACK_TICKET_UUID" -> ticket.uuid.get.toString)
                      )
                    )
                    ref ! CallbackStarted(ticket.requestUuid, ticket.uuid.get)
                  case Failure(e) => log.error("exception" + e.getMessage)
                })
            )
      }

    case BaseRequest(
          ref,
          UpdateCallbackTicket(uuid, status, comment, dueDate, periodUuid)
        ) =>
      recordingWS.updateCallbackTicket(
        uuid,
        CallbackTicketPatch(status, comment, None)
      )
      status match {
        case Some(CallbackStatus.Callback) =>
          recordingWS
            .getCallbackTicket(uuid)
            .onComplete({
              case Success(t) =>
                if (dueDate.isDefined && periodUuid.isDefined) {
                  log.info(
                    s"Rescheduling callback ${t.requestUuid} to $dueDate"
                  )
                  configRequester
                    .rescheduleCallback(
                      t.requestUuid,
                      RescheduleCallback(dueDate.get, periodUuid.get)
                    )
                    .map(_ => {
                      configRequester
                        .getCallbackRequest(t.requestUuid.toString)
                        .map(cb => bus.publish(CallbackRequestUpdated(cb)))
                    })
                }
                configRequester.unclotureRequest(t.requestUuid)
              case Failure(e) => log.error(s"Cannot retrieve ticket $uuid", e)
            })
        case Some(_) =>
          recordingWS
            .getCallbackTicket(uuid)
            .onComplete({
              case Success(t) =>
                configRequester
                  .clotureRequest(t.requestUuid)
                  .onComplete({ case _ =>
                    bus.publish(CallbackClotured(t.requestUuid))
                    log.info(
                      s"Callback ${t.requestUuid} clotured (ticket $uuid updated)"
                    )
                  })
              case Failure(e) => log.error(s"Cannot retrieve ticket $uuid", e)
            })
        case _ =>
      }

    case ExportTicketsCsv(listUuid) =>
      val theSender = sender()
      recordingWS
        .exportTicketsCSV(listUuid)
        .onComplete({
          case Success(result) => theSender ! RequestSuccess(result)
          case Failure(e)      => theSender ! RequestError(e.getMessage)
        })

    case PhoneEvent(
          PhoneEventType.EventDialing,
          _,
          _,
          _,
          linkedId,
          _,
          _,
          variables,
          _,
          _
        ) if variables.contains("USR_CALLBACK_TICKET_UUID") =>
      val ticketUuid = variables("USR_CALLBACK_TICKET_UUID")
      recordingWS
        .updateCallbackTicket(
          ticketUuid,
          CallbackTicketPatch(None, None, Some(linkedId))
        )
        .failed
        .foreach(t =>
          log.error(
            t,
            s"Exception when setting the callid or ticket $ticketUuid"
          )
        )
  }
}
