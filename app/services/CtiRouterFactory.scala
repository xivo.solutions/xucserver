package services

import org.apache.pekko.actor.{Actor, ActorRef, Terminated}
import com.google.inject.Inject
import models.XucUser
import play.api.Logger
import play.api.libs.concurrent.InjectedActorSupport

sealed trait CtiRouterFactoryMessage

case class GetRouter(user: XucUser)             extends CtiRouterFactoryMessage
case class Router(user: XucUser, ref: ActorRef) extends CtiRouterFactoryMessage

class CtiRouterFactory @Inject() (ctiRouterActor: CtiRouter.Factory)
    extends Actor
    with InjectedActorSupport {
  var routers: Map[String, ActorRef] = Map()
  val log: Logger                    = Logger(getClass.getName)
  log.info("Building router factory")

  def receive: PartialFunction[Any, Unit] = {
    case GetRouter(user) =>
      routers.get(user.username) match {
        case Some(ref) =>
          log.info(s"Found existing router for ${user.username}")
          sender() ! Router(user, ref)
        case _ =>
          log.info(s"Creating new router for ${user.username}")
          val newRouter = injectedChild(
            ctiRouterActor(user),
            ActorIds.ctiRouterActorName(user.username)
          )
          context.watch(newRouter)
          routers = routers + (user.username -> newRouter)
          log.info(s"number of routers ${routers.size}")
          sender() ! Router(user, newRouter)
      }

    case t: Terminated =>
      val ref = t.actor
      log.warn(s"Actor $ref terminated removing from known routers")
      routers = routers.filter(p => p._2 != ref)
  }
}
