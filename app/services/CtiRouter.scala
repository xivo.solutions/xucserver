package services

import com.codahale.metrics.{Counter, MetricRegistry, SharedMetricRegistries}
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import com.google.inject.name.Named
import helpers.{
  JmxActorMonitor,
  JmxBooleanMetric,
  JmxLongMetric,
  JmxStringMetric
}
import models.{GetLineConfigInUse, LineConfig, XucUser}
import org.apache.pekko.Done
import org.apache.pekko.actor.{
  Actor,
  ActorRef,
  Cancellable,
  CoordinatedShutdown,
  PoisonPill,
  Terminated
}
import org.apache.pekko.util.Timeout
import org.json.JSONObject
import org.xivo.cti.message.{IpbxCommandResponse, QueueStatistics}
import org.xivo.cti.model.QueueStatRequest
import org.xivo.cti.{CtiMessage, MessageFactory}
import play.api.Logger
import play.api.libs.concurrent.InjectedActorSupport
import play.api.libs.json.JsValue
import services.XucEventBus.{Topic, TopicType}
import services.agent.AgentAction
import services.calltracking.DeviceConferenceAction.DeviceConferenceCommandErrorType
import services.chat.ChatService
import services.chat.model.*
import services.config.{ConfigRepository, ConfigServerRequester, ObjectType}
import services.group.GroupAction
import services.line.PhoneController
import services.request.*
import services.request.PhoneRequest.GetCurrentCallsPhoneEvents
import services.user.CtiUserAction
import services.video.VideoService
import services.video.VideoService.{ConnectVideoUser, DisconnectVideoUser}
import services.video.model.{
  VideoEvent,
  VideoInvitationCommand,
  VideoInvitationEvent,
  VideoStatusEvent
}
import services.voicemail.{
  SubscribeToVoiceMail,
  UnsubscribeFromVoiceMail,
  VoiceMailStatusUpdate
}
import xivo.directory.PersonalContactRepository
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType.*
import xivo.models.{Line, UserConfigUpdated}
import xivo.network.CtiLinkKeepAlive.StartKeepAlive
import xivo.network.{CtiLink, CtiLinkKeepAlive, LoggedOn}
import xivo.phonedevices.{RequestToAmi, RequestToMds}
import xivo.websocket.*
import xivo.websocket.WsActor.WsConnected
import xivo.websocket.WsBus.*
import xivo.xuc.XucBaseConfig

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

trait RouterLog {
  this: CtiRouter =>
  val logger: Logger = Logger(
    getClass.getPackage.getName + ".CtiRouter." + self.path.name
  )

  private def logMessage(msg: String): String = s"[${user.username}] $msg"

  object log {
    def debug(msg: String): Unit = logger.debug(logMessage(msg))

    def info(msg: String): Unit = logger.info(logMessage(msg))

    def error(msg: String): Unit = logger.error(logMessage(msg))

    def warn(msg: String): Unit = logger.warn(logMessage(msg))
  }

}

object CtiRouter {
  trait Factory {
    def apply(_user: XucUser): Actor
  }
}

class CtiRouter @Inject() (
    bus: WsBus,
    eventBus: XucEventBus,
    statEventBus: XucStatsEventBus,
    val messageFactory: MessageFactory,
    phoneControllerFactory: PhoneController.Factory,
    ctiFilterFactory: CtiFilter.Factory,
    personalContactRepoFactory: PersonalContactRepository.Factory,
    configRepository: ConfigRepository,
    agentActionFactory: AgentAction.Factory,
    groupActionFactory: GroupAction.Factory,
    configServerRequester: ConfigServerRequester,
    @Named(ActorIds.CallbackMgrInterfaceId) callbackManager: ActorRef,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    @Named(ActorIds.CallHistoryManagerId) callHistory: ActorRef,
    @Named(ActorIds.XivoDirectoryInterfaceId) xivoDirectory: ActorRef,
    @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef,
    @Named(ActorIds.QueueDispatcherId) queueDispatcher: ActorRef,
    @Named(ActorIds.AgentConfigId) agentConfig: ActorRef,
    @Named(ActorIds.VoiceMailManagerId) voiceMailManager: ActorRef,
    @Named(ChatService.serviceName) flashTextService: ActorRef,
    @Named(VideoService.serviceName) videoService: ActorRef,
    @Named(ActorIds.ClientLogger) clientLogger: ActorRef,
    @Named(ActorIds.VideoEventManager) videoEventManager: ActorRef,
    @Named(ActorIds.UserPreferenceService) userPreferenceService: ActorRef,
    xucConfig: XucBaseConfig,
    @Assisted _user: XucUser
)(implicit ec: ExecutionContext)
    extends Actor
    with RouterLog
    with JmxActorMonitor
    with InjectedActorSupport {

  protected[services] var user: XucUser = _user
  override lazy val jmxActorType        = "CtiRouter"
  val registry: MetricRegistry =
    SharedMetricRegistries.getOrCreate(xucConfig.metricsRegistryName)
  val nbOfClients: Counter =
    registry.counter(s"CtiRouter.${user.username}.nbOfClients")
  val totalClients: Counter = registry.counter("CtiRouter.totalClients")
  val jmxClientCount: Try[JmxLongMetric] =
    jmxBean.addLong("Clients", 0L, Some("Number of clients connected"))
  val jmxPhoneNumber: Try[JmxStringMetric] =
    jmxBean.addString("PhoneNumber", "Unavailable")
  val jmxLineProtocol: Try[JmxStringMetric] =
    jmxBean.addString("LineProtocol", "None")
  val jmxLineName: Try[JmxStringMetric] = jmxBean.addString("LineName", "None")
  val jmxDeviceIp: Try[JmxStringMetric] = jmxBean.addString("DeviceIp", "None")
  val jmxDeviceModel: Try[JmxStringMetric] =
    jmxBean.addString("DeviceModel", "None")
  val jmxCtiLoggedOn: Try[JmxBooleanMetric] =
    jmxBean.addBoolean("CtiEstablished", false)

  protected[services] var userLoggedOn                  = false
  protected[services] var loggedOnMsg: Option[LoggedOn] = None

  protected[services] var isReadyRequester: Set[ActorRef]            = Set.empty
  protected[services] var ctiLink: ActorRef                          = null
  protected[services] var ctiFilter: ActorRef                        = null
  protected[services] var agentActionService: ActorRef               = null
  protected[services] var groupActionService: ActorRef               = null
  protected[services] var ctiKeepAlive: ActorRef                     = null
  protected[services] var phoneController: Option[ActorRef]          = None
  protected[services] var ctiUserAction: CtiUserAction               = null
  protected[services] var currentLine: Option[Line]                  = None
  protected[services] var plannedAgentLoggedOut: Option[Cancellable] = None

  var wsActors: Set[ActorRef]   = Set.empty
  var nextPcId: Int             = 0
  implicit val timeout: Timeout = 5.seconds

  private def getNextPcId: Int = {
    nextPcId = nextPcId + 1
    nextPcId
  }

  def gracefulAgentLogout(agentId: Long): Future[Done] =
    Future {
      log.info(
        s"Logging out agent $agentId on user ${user.username} due to stopping JVM"
      )
      configRepository
        .getLineForAgent(agentId)
        .filter(_.webRTC)
        .foreach(_ => agentActionService ! AgentLogoutRequest(Some(agentId)))
      Done
    }

  private def SetOrReplaceAgentPlannedLogout(agentId: Long): Unit = {
    plannedAgentLoggedOut.foreach(_.cancel())
    plannedAgentLoggedOut = Some(
      CoordinatedShutdown(context.system).addCancellableTask(
        CoordinatedShutdown.PhaseBeforeServiceUnbind,
        s"${user.username}WebRTCAgentLogout"
      )(() => gracefulAgentLogout(agentId))
    )
  }

  log.info(s"Starting CtiRouter actor $ctiLink")

  def createPersonalContactRepo(xucUser: XucUser): ActorRef =
    injectedChild(
      personalContactRepoFactory(xucUser.xivoUser),
      "personalContactRepo"
    )

  def createCtiFilter(xucUser: XucUser): ActorRef =
    injectedChild(
      ctiFilterFactory(
        user,
        self,
        createPersonalContactRepo(xucUser),
        configRepository
      ),
      ActorIds.ctiFilterActorName(user.username)
    )

  def createCtiLink(username: String): ActorRef =
    context.actorOf(
      CtiLink.props(username, xucConfig),
      ActorIds.ctiLinkActorName(username)
    )

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => logger.error("Error while registering mbean", t))
    nbOfClients.dec(nbOfClients.getCount)
    ctiFilter = createCtiFilter(user)

    log.info(s"Creating link actor")
    ctiLink = createCtiLink(user.username)
    ctiUserAction =
      new CtiUserAction(ctiLink, messageFactory, configServerRequester)
    context.watch(ctiLink)
    log.info(s"Creating agent action service")
    agentActionService =
      injectedChild(agentActionFactory(ctiLink), "agentAction")
    log.info(s"Creating group action service")
    groupActionService =
      injectedChild(groupActionFactory(ctiLink), "groupAction")
    ctiKeepAlive = context.actorOf(
      CtiLinkKeepAlive.props(user.username, xucConfig.XivoCtiKeepAliveInterval)
    )
    log.debug(s"Actor $ctiLink created")
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
    plannedAgentLoggedOut.foreach { c =>
      log.debug("Cancel webRTC agent logout on JVM stop")
      c.cancel()
    }
  }

  private def onClientConnected(actor: ActorRef, updatedUser: XucUser): Unit = {
    if (!wsActors.contains(actor)) {
      jmxClientCount.inc()
      nbOfClients.inc()
      totalClients.inc()
      user = updatedUser
      if (wsActors.isEmpty) {
        voiceMailManager ! SubscribeToVoiceMail(user.xivoUser)
      }
      wsActors = wsActors + actor
    }

    log.info(
      s"Connected ${user.username} : ${nbOfClients.getCount} client(s) connected"
    )
  }

  private def onClientDisconnected(actor: ActorRef): Unit = {
    if (wsActors.contains(actor)) {
      wsActors = wsActors - actor
      jmxClientCount.dec()
      nbOfClients.dec()
      totalClients.dec()
      eventBus.unsubscribe(actor)

      if (wsActors.isEmpty) {
        Option(user).foreach(u => {
          flashTextService ! DisconnectFlashTextUser(u.xivoUser)
          videoService ! DisconnectVideoUser(u.xivoUser)
          voiceMailManager ! UnsubscribeFromVoiceMail(u.xivoUser)
          videoEventManager ! UserBaseRequest(actor, VideoEvent("videoEnd"), u)
          userPreferenceService ! UserPreferenceDisconnect(
            Some(user.xivoUser.id)
          )
        })

        if (ctiLink != null) {
          ctiLink ! PoisonPill
        }
      }
    }
    log.info(
      s"client disconnecting : ${nbOfClients.getCount} client(s) connected total: ${totalClients.getCount}"
    )
  }

  import services.config.ConfigDispatcher.*

  def receive: PartialFunction[Any, Unit] =
    mainReceive orElse processCallbackRequests orElse unknownMessage

  def mainReceive: Receive = {

    case IsReady =>
      phoneController match {
        case None    => isReadyRequester = isReadyRequester + sender()
        case Some(_) => sender() ! Started()
      }

    case WsConnected(wsActor, wsActorUser) =>
      onClientConnected(wsActor, wsActorUser)
      loggedOnMsg.foreach(ctiFilter ! ClientConnected(wsActor, _))

    case terminated: Terminated =>
      terminated.actor match {
        case actor: ActorRef if actor == ctiLink =>
          userLoggedOn = false
          jmxCtiLoggedOn.set(userLoggedOn)
          log.info(
            "Cti link is terminated ----------------" + terminated.actor.toString
          )

          flashTextService ! DisconnectFlashTextUser(user.xivoUser)
          videoService ! DisconnectVideoUser(user.xivoUser)
          ctiFilter ! PoisonPill
          context stop self

        case _ =>
          log.error(
            "Unknown actor terminated: " + terminated.actor.path.name
          )
      }

    case ln: LoggedOn =>
      log.info(s"loggedOn $ln")
      loggedOnMsg = Some(ln)
      userLoggedOn = true
      jmxCtiLoggedOn.set(userLoggedOn)
      ctiFilter ! ln
      ctiKeepAlive ! StartKeepAlive(ln.userId, ctiLink)
      flashTextService ! ConnectFlashTextUser(user.xivoUser)
      videoService ! ConnectVideoUser(user.xivoUser)

    case agentError: IpbxCommandResponse => agentActionService ! agentError

    case message: CtiMessage =>
      message match {
        case message: QueueStatistics =>
          log.debug(
            s"Deprecated, QueueStatistics shall not be sent by Ctid, blocked $message"
          )
        case _ => ctiFilter ! message
      }

    case ctiRequest: JSONObject =>
      if (userLoggedOn) {
        log.warn(
          s"Deprecated : please use high level message to send request: $ctiRequest"
        )
        ctiLink ! ctiRequest
        sender() ! CtiReqSuccess()
      } else {
        log.debug(s"not connected waiting ")
        rescheduleRequest(ctiRequest, sender())
      }

    case jsonMessage: JsValue =>
      log.debug(">>>to Browser " + jsonMessage.toString)
      bus.publish(
        WsMessageEvent(
          WsBus.browserTopic(user.username),
          WsContent(jsonMessage)
        )
      )

    case retry: CtiReqRetry =>
      if (userLoggedOn) {
        log.debug(
          s"processing request retry for sender : ${retry.requestSender}"
        )
        ctiLink ! retry.ctiRequest
        retry.requestSender ! CtiReqSuccess()
      } else {
        log.debug(s"unable to process request retry")
        retry.requestSender ! CtiReqError()
      }

    case LinkStatusUpdate(LinkState.up) => ctiLink ! Start(user)

    case LinkStatusUpdate(LinkState.down) =>
      loggedOnMsg = None
      bus.publish(
        WsMessageEvent(
          WsBus.browserTopic(user.username),
          WsContent(
            WebSocketEvent.createEvent(LinkStatusUpdate(LinkState.down))
          )
        )
      )

    case Leave(leaving) =>
      onClientDisconnected(leaving)

    case BaseRequest(sender, msg: AgentConfigRequest) =>
      agentConfig ! BaseRequest(sender, msg)

    case BaseRequest(_, AgentLoginRequest(None, Some(phoneNumber), None)) =>
      log.debug(s"Agent login request, None, $phoneNumber")
      ctiFilter ! AgentLoginRequest(None, Some(phoneNumber))

    case BaseRequest(_, AgentLogoutRequest(None)) =>
      log.debug(s"Agent logout request, None")
      ctiFilter ! AgentLogoutRequest(None)

    case BaseRequest(_, req @ AgentPauseRequest(None, _)) =>
      log.debug(s"Agent pause request without agent id")
      ctiFilter ! req

    case BaseRequest(_, req @ AgentUnPauseRequest(None)) =>
      log.debug(s"Agent unpause request without agent id")
      ctiFilter ! req

    case AgentListen(agentId, Some(userId)) =>
      log.debug(s"${AgentListen(agentId, Some(userId))}")
      agentActionService ! AgentListen(agentId, Some(userId))

    case BaseRequest(_, msg: AgentListen) =>
      log.debug(s"AgentListen $msg")
      ctiFilter ! msg

    case BaseRequest(_, msg: AgentActionRequest) =>
      log.debug(s"AgentActionRequest $msg")
      agentActionService ! msg

    case req @ BaseRequest(_, UserGroupPauseRequest(groupId, None)) =>
      log.debug(s"UserGroupPauseRequest")
      ctiFilter ! req

    case req @ BaseRequest(_, UserGroupUnpauseRequest(groupId, None)) =>
      log.debug(s"UserGroupUnpauseRequest")
      ctiFilter ! req

    case BaseRequest(wsActor, msg: UserGroupActionRequest) =>
      log.debug(s"UserGroupActionRequest")
      groupActionService ! BaseRequest(wsActor, msg)

    case BaseRequest(_, req @ UserStatusUpdateReq(None, _)) =>
      log.debug(s"User Status Update Request without user id")
      ctiFilter ! req

    case req @ BaseRequest(_, GetUserCallHistory(param)) =>
      logger.debug(s"Forwarding User call history request: param: $param")
      ctiFilter forward req

    case req @ BaseRequest(_, GetUserCallHistoryByDays(param)) =>
      logger.debug(
        s"Forwarding User call history by days request: param: $param"
      )
      ctiFilter forward req

    case BaseRequest(requester, GetConfig(objectType)) =>
      configDispatcher !
        RequestConfigForUsername(
          requester,
          GetConfig(objectType),
          Some(user.username)
        )
      eventBus.subscribe(requester, XucEventBus.configTopic(objectType))

    case BaseRequest(requester, GetList(objectType)) =>
      configDispatcher ! RequestConfig(requester, GetList(objectType))
      eventBus.subscribe(requester, XucEventBus.configTopic(objectType))

    case BaseRequest(requester, ToggleUniqueAccountDevice(_, deviceType)) =>
      configDispatcher ! RequestConfigForUsername(
        ctiFilter,
        ToggleUniqueAccountDevice(currentLine, deviceType),
        Some(user.username)
      )

    case BaseRequest(requester, SubscribeToAgentEvents) =>
      log.debug(s"$requester subscribing to agent events")
      eventBus.subscribe(
        requester,
        Topic(TopicType.Event, ObjectType.TypeAgent)
      )

    case BaseRequest(requester, SubscribeToQueueStats) =>
      statEventBus.subscribe(requester, ObjectDefinition(Queue))
      configDispatcher ! RequestConfig(requester, GetQueueStatistics)

    case BaseRequest(requester, SubscribeToAgentStats) =>
      log.debug("subscribing to agent stats")
      configDispatcher ! RequestConfig(requester, GetAgentStatistics)
      eventBus.subscribe(requester, Topic(TopicType.Stat, ObjectType.TypeAgent))

    case BaseRequest(requester, s: SubscribeToPhoneHints) =>
      log.debug(s"subscribing to phone hints for phone ${s.phoneNumbers}")
      configDispatcher ! MonitorPhoneHint(requester, s.phoneNumbers)

    case BaseRequest(requester, UnsubscribeFromAllPhoneHints) =>
      log.debug(s"unsubscribing $requester from all phone hints")
      configDispatcher ! MonitorPhoneHint(requester, List())

    case BaseRequest(requester, s: SubscribeToVideoStatus) =>
      log.debug(s"subscribing to video status for user ${s.usernames}")
      configDispatcher ! MonitorVideoStatus(requester, s.usernames)

    case BaseRequest(requester, UnsubscribeFromAllVideoStatus) =>
      log.debug(s"unsubscribing $requester from all video status")
      configDispatcher ! MonitorVideoStatus(requester, List())

    case BaseRequest(requester, SubscribeToQueueCalls(queueId)) =>
      log.debug(s"Subscribing to queue calls for queue $queueId")
      configDispatcher ! RequestConfig(requester, GetQueueCalls(queueId))
      eventBus.subscribe(requester, XucEventBus.queueCallsTopic(queueId))

    case BaseRequest(requester, UnSubscribeToQueueCalls(queueId)) =>
      log.debug(s"Unsubscribing to queue calls for queue $queueId")
      eventBus.unsubscribe(requester, XucEventBus.queueCallsTopic(queueId))

    case BaseRequest(requester, configQuery: ConfigQuery) =>
      configDispatcher ! RequestConfig(requester, configQuery)

    case BaseRequest(requester, monitorAction: MonitorActionRequest) =>
      phoneController.foreach(_ forward monitorAction)

    case BaseRequest(_, invite: InviteConferenceRoom) =>
      ctiLink ! messageFactory.createInviteConferenceRoom(invite.userId)

    case BaseRequest(_, req: QueueStatisticsReq) =>
      val requests = new java.util.ArrayList[QueueStatRequest]()
      requests.add(
        new QueueStatRequest(req.queueId.toString, req.window, req.xqos)
      )
      ctiLink ! messageFactory.createGetQueueStatistics(requests)
    case BaseRequest(_, userActionRequest: UserActionRequest) =>
      ctiUserAction.doUserAction(userActionRequest, user.xivoUser.id)

    case BaseRequest(ref, GetAgentCallHistory(size)) =>
      logger.debug(
        s"Forwarding agent call history request : size = $size to history manager"
      )
      callHistory ! BaseRequest(
        ref,
        AgentCallHistoryRequest(size, user.username)
      )

    case BaseRequest(ref, f: CustomerCallHistoryRequestWithId) =>
      logger.debug(
        s"Forwarding customer call history request to history manager"
      )
      callHistory ! BaseRequest(ref, f)

    case BaseRequest(ref, req: GetQueueCallHistory) =>
      logger.debug(s"Forwarding queue call history request to history manager")
      callHistory ! BaseRequest(ref, req)

    case BaseRequest(ref, dl: DirectoryLookUp) =>
      xivoDirectory ! UserBaseRequest(ref, dl, user)

    case BaseRequest(ref, dl: SearchContacts) =>
      xivoDirectory ! UserBaseRequest(ref, dl, user)

    case BaseRequest(ref, GetFavorites) =>
      xivoDirectory ! UserBaseRequest(ref, GetFavorites, user)

    case BaseRequest(ref, GetFavoriteContacts) =>
      xivoDirectory ! UserBaseRequest(ref, GetFavoriteContacts, user)

    case BaseRequest(ref, request.AddFavorite(contactId, directory)) =>
      xivoDirectory ! UserBaseRequest(
        ref,
        AddFavorite(contactId, directory),
        user
      )

    case BaseRequest(ref, request.RemoveFavorite(contactId, directory)) =>
      xivoDirectory ! UserBaseRequest(
        ref,
        RemoveFavorite(contactId, directory),
        user
      )

    case BaseRequest(ref, rq @ (_: PhoneRequest)) =>
      phoneController match {
        case Some(pc) => pc ! UserBaseRequest(ref, rq, user)
        case None =>
          log.warn(
            s"CtiRouter for $user has no phoneController, PhoneRequest $rq ignored"
          )
      }

    case BaseRequest(ref, ucr: UpdateConfigRequest) =>
      configDispatcher ! ConfigChangeRequest(ref, ucr)

    case BaseRequest(ref, dfq: DialFromQueue) => queueDispatcher ! dfq

    case BaseRequest(ref, pls: PushLogToServer) =>
      clientLogger ! UserBaseRequest(ref, pls, user)

    case BaseRequest(ref, vid: VideoInvitationCommand) =>
      videoEventManager ! UserBaseRequest(ref, vid, user)

    case vid: VideoInvitationEvent =>
      wsActors.foreach(_ ! vid)

    case BaseRequest(ref, videoEvent: VideoEvent) =>
      videoEventManager ! UserBaseRequest(ref, videoEvent, user)

    case BaseRequest(ref, unregisterMobileApp: UnregisterMobileApp) =>
      userPreferenceService ! unregisterMobileApp.copy(username =
        Some(user.username)
      )

    case vse: VideoStatusEvent =>
      wsActors.foreach(_ ! vse)

    case requestConfig: RequestConfig =>
      configDispatcher ! requestConfig

    case requestStatus: RequestStatus =>
      configDispatcher ! requestStatus

    case configChangeRequest: ConfigChangeRequest =>
      configDispatcher ! configChangeRequest

    case userConfigUpdated: UserConfigUpdated =>
      configDispatcher ! userConfigUpdated

    case requestToAmi: RequestToAmi =>
      amiBusConnector ! RequestToMds(
        requestToAmi,
        currentLine.flatMap(_.registrar)
      )

    case l @ LineConfig(_, phoneNb, Some(line), _) =>
      jmxPhoneNumber.set(phoneNb)
      jmxLineProtocol.set(line.protocol)
      jmxLineName.set(line.name)
      jmxDeviceIp.set(line.dev.flatMap(_.ip).getOrElse("None"))
      jmxDeviceModel.set(line.dev.map(_.vendor).getOrElse("None"))

      currentLine = Some(line)
      phoneController.foreach(_ ! PoisonPill)
      phoneController = Some(
        injectedChild(
          phoneControllerFactory(phoneNb, line, user.xivoUser),
          s"phonecontroller$getNextPcId"
        )
      )
      phoneController.foreach(p => {
        p ! PhoneController.Start
        p ! UserBaseRequest(self, GetCurrentCallsPhoneEvents, user)
      })
      isReadyRequester.map(_ ! Started())
      isReadyRequester = Set.empty
      wsActors.foreach(_ ! l)
      log.info(
        s"Created PhoneController for phone number $phoneNb with line $line"
      )

      if (line.webRTC) {
        user.xivoUser.agentId.foreach(SetOrReplaceAgentPlannedLogout)
      }

    case e: DeviceConferenceCommandErrorType =>
      val wsError = WebSocketEvent.createEvent(WsConferenceCommandError(e))
      bus.publish(
        WsMessageEvent(WsBus.browserTopic(user.username), WsContent(wsError))
      )

    case BaseRequest(_, BrowserRequestEnvelope(request)) =>
      val checkedRequest = request match {
        case BrowserSendDirectMessage(to, message, seq) =>
          SendDirectMessage(user.username, to, message, seq)
        case BrowserGetDirectMessageHistory(to, seq) =>
          GetDirectMessageHistory((user.username, to), seq)
        case BrowserMarkAsReadDirectChannel(to, seq) =>
          MarkDirectchannelAsRead((user.username, to), seq)
      }

      flashTextService ! checkedRequest
      videoService ! checkedRequest

    case msg: FlashTextEvent =>
      bus.publish(
        WsMessageEvent(
          WsBus.browserTopic(user.username),
          WsContent(WebSocketEvent.createEvent(BrowserEventEnvelope(msg)))
        )
      )

    case vm: VoiceMailStatusUpdate =>
      bus.publish(
        WsMessageEvent(
          WsBus.browserTopic(user.username),
          WsContent(WebSocketEvent.createEvent(vm))
        )
      )

    case BaseRequest(_, up: UserPreferenceRequest) =>
      log.debug(s"Set userPreference request $up")
      up.enrichWithUserId(user.xivoUser.id)
        .foreach(userPreferenceService ! _)

    case GetLineConfigInUse =>
      ctiFilter.forward(GetLineConfigInUse)
  }

  def processCallbackRequests: Receive = {

    case BaseRequest(ref, request.GetCallbackLists) =>
      callbackManager ! BaseRequest(ref, request.GetCallbackLists)
      eventBus.subscribe(ref, XucEventBus.callbackListsTopic())

    case BaseRequest(ref, request.TakeCallback(uuid)) =>
      callbackManager ! request.TakeCallbackWithAgent(uuid, user.username)

    case BaseRequest(ref, ReleaseCallback(uuid)) =>
      callbackManager ! request.ReleaseCallback(uuid)

    case BaseRequest(ref, request.StartCallback(uuid, number)) =>
      callbackManager ! BaseRequest(
        ref,
        request.StartCallbackWithUser(uuid, number, user.username)
      )

    case BaseRequest(ref, request: CallbackMgrRequest) =>
      callbackManager ! BaseRequest(ref, request)
  }

  def unknownMessage: Receive = { case unknown =>
    log.debug(s"Unknown message received: $unknown from: ${sender()}")
  }

  private def rescheduleRequest(
      ctiRequest: JSONObject,
      requestSender: ActorRef
  ) = {
    context.system.scheduler.scheduleOnce(800.milliseconds) {
      self ! CtiReqRetry(ctiRequest, requestSender)
    }
  }
}

case class Start(user: XucUser)

case class Leave(leaving: ActorRef)

case object IsReady

case class Started()

case class CtiReqRetry(ctiRequest: JSONObject, requestSender: ActorRef)

case class CtiReqSuccess()

case class CtiReqError()

case class ClientConnected(actor: ActorRef, ln: LoggedOn)
