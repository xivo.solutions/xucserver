package services.agent

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef, PoisonPill}
import services.agent.AgentInGroupAction.{AgentsDestination, RemoveAgents}
import services.config.ConfigDispatcher.{AgentList, GetAgents, RequestConfig}
import xivo.models.Agent
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.FiniteDuration

object AgentInGroupAction {
  case class AgentsDestination(queueId: Long, penalty: Int)
  case object RemoveAgents
}

abstract class AgentInGroupAction(
    groupId: Long,
    fromQueueId: Long,
    fromPenalty: Int,
    configDispatcher: ActorRef
) extends Actor
    with ActorLogging {

  val RequestTimeOut: FiniteDuration = 1.second

  context.system.scheduler.scheduleOnce(RequestTimeOut) {
    self ! PoisonPill
  }

  override def receive: Receive = {

    case AgentsDestination(toQueueId, toPenalty) =>
      configDispatcher ! RequestConfig(
        self,
        GetAgents(groupId, fromQueueId, fromPenalty)
      )
      context.become(forAllAgents(Some(toQueueId), Some(toPenalty)))

    case RemoveAgents =>
      configDispatcher ! RequestConfig(
        self,
        GetAgents(groupId, fromQueueId, fromPenalty)
      )
      context.become(forAllAgents())

    case unkown => log.debug(s"unkown $unkown message received")
  }

  def forAllAgents(
      toQueueId: Option[Long] = None,
      toPenalty: Option[Int] = None
  ): Receive = {

    case AgentList(agents) =>
      agents.foreach(actionOnAgent(_, toQueueId, toPenalty))
      self ! PoisonPill

    case unkown => log.debug(s" action unkown $unkown message received")

  }

  def actionOnAgent(
      agent: Agent,
      toQueueId: Option[Long],
      toPenalty: Option[Int]
  ): Unit

  override def postStop(): Unit =
    log.debug(s"Move in group $groupId $fromQueueId $fromPenalty stopped")
}
