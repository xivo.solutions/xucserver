package services.agent

import services.agent
import services.calltracking.DeviceCall
import xivo.events.CallDirection
import xivo.events.CallDirection.*
import xivo.xucami.models.{ChannelDirection, ChannelState, MonitorState}

object AgentCallState extends Enumeration {
  type AgentCallState = Value
  val OnCall: agent.AgentCallState.Value  = Value
  val Ringing: agent.AgentCallState.Value = Value
  val Dialing: agent.AgentCallState.Value = Value
}

import AgentCallState._

case class AgentCall(
    state: AgentCallState,
    acd: Boolean,
    direction: CallDirection,
    monitor: MonitorState.MonitorState = MonitorState.DISABLED
)

object AgentCall {

  def deviceCallToAgentCall(deviceCall: DeviceCall): Option[AgentCall] = {
    val maybeState = deviceCall.callState.flatMap(_ match {
      case ChannelState.RINGING     => Some(AgentCallState.Ringing)
      case ChannelState.DIALING     => Some(AgentCallState.Dialing)
      case ChannelState.ORIGINATING => Some(AgentCallState.Dialing)
      case ChannelState.UP          => Some(AgentCallState.OnCall)
      case ChannelState.HOLD        => Some(AgentCallState.OnCall)
      case _                        => None
    })
    val isAcd = deviceCall.queueName match {
      case None    => false
      case Some(_) => true
    }
    val maybeDirection = deviceCall.channel
      .flatMap(_.direction)
      .map(_ match {
        case ChannelDirection.INCOMING => CallDirection.Incoming
        case ChannelDirection.OUTGOING => CallDirection.Outgoing
      })

    for {
      state     <- maybeState
      direction <- maybeDirection
    } yield AgentCall(
      state,
      isAcd,
      direction,
      deviceCall.monitorState.getOrElse(MonitorState.DISABLED)
    )

  }

  def callListToAgentCallList(calls: List[DeviceCall]): List[AgentCall] = {
    for {
      call      <- calls
      agentCall <- deviceCallToAgentCall(call)
    } yield agentCall
  }

  def currentAgentCall(agentCalls: List[AgentCall]): Option[AgentCall] = {
    val sortedList = agentCalls.sortBy(c => {
      if (c.acd) {
        1
      } else {
        c.state match {
          case AgentCallState.OnCall  => 2
          case AgentCallState.Dialing => 3
          case AgentCallState.Ringing => 4
        }
      }
    })

    sortedList.headOption

  }

}
