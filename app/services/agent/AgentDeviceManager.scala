package services.agent

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import com.google.inject.name.Named
import services.request._
import services.{ActorIds, XucEventBus}
import xivo.ami.AmiBusConnector.{AgentSpyStart, AgentSpyStop}
import xivo.events.AgentState.{
  AgentLoggedOut,
  AgentOnPause,
  AgentOnWrapup,
  AgentReady
}

class AgentDeviceManager @Inject() (
    eventBus: XucEventBus,
    @Named(ActorIds.AmiBusConnectorId) amibusConnector: ActorRef
) extends Actor
    with ActorLogging {
  log.info(s"starting $self")

  eventBus.subscribe(self, XucEventBus.allAgentsEventsTopic)

  override def receive: Receive = {

    case AgentSpyStart(id, phoneNb) =>
      log.debug(
        s"Agent: $id spy started, turn on key listen light on phone $phoneNb."
      )
      amibusConnector ! TurnOnKeyLight(phoneNb, PhoneKey.Spied)

    case AgentSpyStop(id, phoneNb) =>
      log.debug(
        s"Agent: $id spy stopped, turn off key listen light on phone $phoneNb."
      )
      amibusConnector ! TurnOffKeyLight(phoneNb, PhoneKey.Spied)

    case AgentOnPause(id, _, phoneNb, _, _, _) =>
      log.debug(
        s"Agent: $id on pause, turn on key pause light on phone $phoneNb."
      )
      amibusConnector ! TurnOnKeyLight(phoneNb, PhoneKey.Pause)

    case AgentReady(id, _, phoneNb, _, _, agentNb) =>
      log.debug(
        s"Agent: $id ready, turn off key pause light turn on logon light on phone $phoneNb."
      )
      amibusConnector ! TurnOffKeyLight(phoneNb, PhoneKey.Pause)
      amibusConnector ! TurnOnKeyLight(phoneNb, PhoneKey.Logon)
      amibusConnector ! TurnOnKeyLightWithAgNum(
        phoneNb,
        PhoneKey.Logon,
        agentNb
      )
      amibusConnector ! TurnOffKeyLight(phoneNb, PhoneKey.Spied)

    case AgentLoggedOut(id, _, phoneNb, _, agentNb, _) if phoneNb != "" =>
      log.debug(
        s"Agent: $id (number: $agentNb) loggedout, turn off key pause light turn off logon light on phone $phoneNb."
      )
      amibusConnector ! TurnOffKeyLight(phoneNb, PhoneKey.Pause)
      amibusConnector ! TurnOffKeyLight(phoneNb, PhoneKey.Logon)
      amibusConnector ! TurnOffKeyLightWithAgNum(
        phoneNb,
        PhoneKey.Logon,
        agentNb
      )
      amibusConnector ! TurnOffKeyLight(phoneNb, PhoneKey.Spied)

    case AgentOnWrapup(id, _, phoneNb, _, _, _) =>
      log.debug(s"Agent: $id on wrapup, blink pause key on phone $phoneNb.")
      amibusConnector ! BlinkKeyLight(phoneNb, PhoneKey.Pause)

    case unknown =>
  }
}
