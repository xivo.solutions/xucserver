package services.agent

import org.joda.time.{DateTime, Interval}
import play.api.libs.json.Writes
import services.AgentStateFSM._
import xivo.events.AgentState
import xivo.events.AgentState._
import play.api.libs.json._

trait AgentStatCalculator {
  val name: String
  val collector: StatCollector

  def reset(): Unit
}

trait AgentStatCalculatorByEvent extends AgentStatCalculator {
  def processEvent(state: AgentState): Unit
}

trait AgentStatCalculatorByTransition extends AgentStatCalculator {
  def processTransition(t: AgentTransition): Unit
}

trait AgentStatCalculatorByStatValue extends AgentStatCalculator {
  def processStat(stats: AgentStatistic): Unit
}

sealed trait StatValue
object StatValue {
  implicit val baseWrites: Writes[StatValue] =
    new Writes[StatValue] {
      def writes(o: StatValue): JsValue =
        o match {
          case s: StatDateTime => StatDateTime.writes.writes(s)
          case s: StatPeriod   => StatPeriod.writes.writes(s)
          case s: StatTotal    => StatTotal.writes.writes(s)
          case s: StatAverage  => StatAverage.writes.writes(s)
        }
    }
}

case class StatDateTime(value: DateTime) extends StatValue
object StatDateTime {
  val writes: Writes[StatDateTime] = new Writes[StatDateTime] {
    def writes(c: StatDateTime): JsValue = { Json.toJson(c.value.toString()) }
  }
}

case class StatPeriod(value: Long) extends StatValue
object StatPeriod {
  val writes: Writes[StatPeriod] = new Writes[StatPeriod] {
    def writes(c: StatPeriod): JsValue = { Json.toJson(c.value) }
  }
}

case class StatTotal(value: Long) extends StatValue
object StatTotal {
  val writes: Writes[StatTotal] = new Writes[StatTotal] {
    def writes(c: StatTotal): JsValue = { Json.toJson(c.value) }
  }
}

case class StatAverage(value: Double) extends StatValue
object StatAverage {
  val writes: Writes[StatAverage] = new Writes[StatAverage] {
    def writes(c: StatAverage): JsValue = { Json.toJson(c.value) }
  }
}

object LoginDateTime { val name = "LoginDateTime" }
case class LoginDateTime(
    override val name: String = LoginDateTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition {
  override def processTransition(t: AgentTransition): Unit = {
    if (t.stateFrom == MAgentLoggedOut && t.stateTo != MAgentLoggedOut) {
      collector.onStatCalculated(name, StatDateTime(t.contextTo.lastChanged))
    }
  }

  override def reset(): Unit = {}
}

object LogoutDateTime { val name = "LogoutDateTime" }
case class LogoutDateTime(
    override val name: String = LogoutDateTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByEvent {

  override def processEvent(state: AgentState): Unit =
    state match {
      case AgentLoggedOut(_, dateTime, _, _, _, _) =>
        collector.onStatCalculated(name, StatDateTime(dateTime))
      case _ =>
    }
  override def reset(): Unit = {}
}

trait TimeProvider {
  def getTime: DateTime
}

trait JodaTimeProvider extends TimeProvider {
  def getTime = new DateTime
}

trait AgentTotalTime extends TimeProvider {
  this: AgentStatCalculator & TimeProvider =>

  var period: Long                        = 0
  private var startTime: Option[DateTime] = None

  override def reset(): Unit = {
    period = 0
    startTime = startTime.flatMap(_ => Some(getTime))
    collector.onStatCalculated(name, StatPeriod(0))
  }

  private[agent] def start(dateTime: DateTime): Unit =
    startTime match {
      case None => startTime = Some(dateTime)
      case _    =>
    }

  private[agent] def stopAndPublish(): Unit = {
    startTime match {
      case Some(time) =>
        collector.onStatCalculated(name, StatPeriod(accumulate()))
        startTime = None
      case None =>
    }
  }

  private[agent] def accumulate(): Long = {
    period = startTime match {
      case Some(atime) =>
        if (atime.isBefore(getTime))
          period + new Interval(atime, getTime).toDurationMillis / 1000
        else period
      case None => period
    }
    period
  }
}

object AgentReadyTotalTime { val name = "ReadyTime" }
case class AgentReadyTotalTime(
    override val name: String = AgentReadyTotalTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByEvent
    with AgentTotalTime
    with JodaTimeProvider {

  override def processEvent(state: AgentState): Unit =
    state match {
      case AgentReady(_, dateTime, _, _, _, _) => start(dateTime)
      case _                                   => stopAndPublish()
    }

}

object AgentPausedTotalTime { val name = "PausedTime" }
case class AgentPausedTotalTime(
    override val name: String = AgentPausedTotalTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByEvent
    with AgentTotalTime
    with JodaTimeProvider {

  override def processEvent(state: AgentState): Unit =
    state match {
      case AgentOnPause(_, dateTime, _, _, _, _) => start(dateTime)
      case _                                     => stopAndPublish()
    }

}

object AgentWrapupTotalTime { val name = "WrapupTime" }
case class AgentWrapupTotalTime(
    override val name: String = AgentWrapupTotalTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByEvent
    with AgentTotalTime
    with JodaTimeProvider {

  override def processEvent(state: AgentState): Unit =
    state match {
      case AgentOnWrapup(_, dateTime, _, _, _, _) => start(dateTime)
      case _                                      => stopAndPublish()
    }

}

object AgentPausedTotalTimeWithCause { val name = "PausedTimeWithCause" }
case class AgentPausedTotalTimeWithCause(
    override val name: String = AgentPausedTotalTimeWithCause.name,
    collector: StatCollector
) extends AgentStatCalculatorByEvent
    with AgentTotalTime
    with JodaTimeProvider {

  override def processEvent(state: AgentState): Unit =
    state match {
      case AgentOnPause(_, dateTime, _, _, Some(this.name), _) =>
        start(dateTime)
      case _ => stopAndPublish()
    }

}

trait AgentTotalCalls {
  this: AgentStatCalculator =>
  private[agent] var total: Long = 0

  override def reset(): Unit = {
    total = 0
    collector.onStatCalculated(name, StatTotal(total))
  }
  def accAndPublish(): Unit = {
    total = total + 1
    collector.onStatCalculated(name, StatTotal(total))
  }
}

object AgentInboundTotalCalls { val name = "InbCalls" }
case class AgentInboundTotalCalls(
    override val name: String = AgentInboundTotalCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition
    with AgentTotalCalls
    with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition): Unit = {
    if (isNewIncomingCall(t)) {
      accAndPublish()
    }
  }

}

object AgentInboundTotalAcdCalls { val name = "InbAcdCalls" }
case class AgentInboundTotalAcdCalls(
    override val name: String = AgentInboundTotalAcdCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition
    with AgentTotalCalls
    with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition): Unit = {
    if (
      (isNewIncomingCall(t) && isAcd(t.contextTo)) ||
      (isRinging(t.contextFrom) && !isAcd(t.contextFrom) && isRinging(
        t.contextTo
      ) && isAcd(t.contextTo))
    ) {
      accAndPublish()
    }
  }

}

trait AgentTransitionQualifier {

  def contextToCall(context: MContext): Option[AgentCall] = {
    context match {
      case AgentStateContext(_, _, currentCall, _, _) =>
        currentCall
      case _ => None
    }
  }

  def getCallState(context: MContext): Option[AgentCallState.AgentCallState] =
    contextToCall(context).map(_.state)

  def isRinging(context: MContext): Boolean =
    getCallState(context).contains(AgentCallState.Ringing)

  def isNewIncomingCall(t: AgentTransition): Boolean = {
    val callFrom = contextToCall(t.contextFrom)
    val callTo   = contextToCall(t.contextTo)

    (callFrom, callTo) match {
      case (
            None,
            Some(
              AgentCall(AgentCallState.Ringing | AgentCallState.OnCall, _, _, _)
            )
          ) =>
        true
      case (
            Some(
              AgentCall(AgentCallState.OnCall | AgentCallState.Dialing, _, _, _)
            ),
            Some(AgentCall(AgentCallState.Ringing, _, _, _))
          ) =>
        true
      case (_, _) =>
        false
    }
  }

  def isNewOutgoingCall(t: AgentTransition): Boolean = {
    val callFrom = contextToCall(t.contextFrom)
    val callTo   = contextToCall(t.contextTo)

    (callFrom, callTo) match {
      case (None, Some(AgentCall(AgentCallState.Dialing, _, _, _))) =>
        true
      case (
            Some(AgentCall(AgentCallState.OnCall, _, _, _)),
            Some(AgentCall(AgentCallState.Dialing, _, _, _))
          ) =>
        true
      case (_, _) =>
        false
    }
  }

  def isNewAnsweredIncomingCall(t: AgentTransition): Boolean = {
    val callFrom = contextToCall(t.contextFrom)
    val callTo   = contextToCall(t.contextTo)

    (callFrom, callTo) match {
      case (
            Some(AgentCall(AgentCallState.Ringing, _, _, _)),
            Some(AgentCall(AgentCallState.OnCall, _, _, _))
          ) =>
        true
      case (_, _) =>
        false
    }
  }

  def isNewAnsweredOutgoingCall(t: AgentTransition): Boolean = {
    val callFrom = contextToCall(t.contextFrom)
    val callTo   = contextToCall(t.contextTo)

    (callFrom, callTo) match {
      case (
            Some(AgentCall(AgentCallState.Dialing, _, _, _)),
            Some(AgentCall(AgentCallState.OnCall, _, _, _))
          ) =>
        true
      case (_, _) =>
        false
    }
  }

  def isHangup(t: AgentTransition): Boolean = {
    val callFrom = contextToCall(t.contextFrom)
    val callTo   = contextToCall(t.contextTo)

    (callFrom, callTo) match {
      case (Some(_), None) =>
        true
      case (_, _) =>
        false
    }
  }

  def isAcd(context: MContext): Boolean =
    contextToCall(context) match {
      case Some(AgentCall(_, true, _, _)) =>
        true
      case _ =>
        false
    }

}

object AgentInboundAnsweredCalls { val name = "InbAnsCalls" }
case class AgentInboundAnsweredCalls(
    override val name: String = AgentInboundAnsweredCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition
    with AgentTotalCalls
    with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition): Unit = {
    if (isNewAnsweredIncomingCall(t)) {
      accAndPublish()
    }
  }

}

object AgentInboundAnsweredAcdCalls { val name = "InbAnsAcdCalls" }
case class AgentInboundAnsweredAcdCalls(
    override val name: String = AgentInboundAnsweredAcdCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition
    with AgentTotalCalls
    with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition): Unit = {
    if (isNewAnsweredIncomingCall(t) && isAcd(t.contextTo)) {
      accAndPublish()
    }
  }

}

object AgentInboundTotalCallTime { val name = "InbCallTime" }
case class AgentInboundTotalCallTime(
    override val name: String = AgentInboundTotalCallTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition
    with AgentTotalTime
    with JodaTimeProvider
    with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition): Unit = {
    if (isNewAnsweredIncomingCall(t)) {
      start(getTime)
    }
    if (isHangup(t)) {
      stopAndPublish()
    }
  }
}

object AgentInboundTotalAcdCallTime { val name = "InbAcdCallTime" }
case class AgentInboundTotalAcdCallTime(
    override val name: String = AgentInboundTotalAcdCallTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition
    with AgentTotalTime
    with JodaTimeProvider
    with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition): Unit = {
    if (isNewAnsweredIncomingCall(t) && isAcd(t.contextTo)) {
      start(getTime)
    }
    if (isHangup(t) && isAcd(t.contextFrom)) {
      stopAndPublish()
    }
  }
}

object AgentOutboundTotalCalls { val name = "OutCalls" }
case class AgentOutboundTotalCalls(
    override val name: String = AgentOutboundTotalCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition
    with AgentTotalCalls
    with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition): Unit = {
    if (isNewOutgoingCall(t))
      accAndPublish()
  }
}

object AgentOutboundTotalCallTime { val name = "OutCallTime" }
case class AgentOutboundTotalCallTime(
    override val name: String = AgentOutboundTotalCallTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByTransition
    with AgentTotalTime
    with JodaTimeProvider
    with AgentTransitionQualifier {

  override def processTransition(t: AgentTransition): Unit = {
    if (isNewAnsweredOutgoingCall(t)) {
      start(getTime)
    }
    if (isHangup(t)) {
      stopAndPublish()
    }
  }
}

trait AgentAverageTime {
  this: AgentStatCalculator =>
  private[agent] var divisor: Option[Long] = None

  override def reset(): Unit = {
    divisor = None
    collector.onStatCalculated(name, StatTotal(0))
  }
  def storeDivisor(value: Long): Unit = {
    divisor = Some(value)
  }
  def averageAndPublish(value: Long): Unit = {
    divisor match {
      case Some(divisorValue) if divisorValue > 0 =>
        collector.onStatCalculated(name, StatTotal(value / divisorValue))
      case Some(0)        => collector.onStatCalculated(name, StatTotal(0))
      case None | Some(_) =>
    }
  }
}

object AgentInboundAverageCallTime { val name = "InbAverCallTime" }
case class AgentInboundAverageCallTime(
    override val name: String = AgentInboundAverageCallTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByStatValue
    with AgentAverageTime {

  override def processStat(stats: AgentStatistic): Unit = {
    stats match {
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundAnsweredCalls.name, StatTotal(value)))
          ) =>
        storeDivisor(value)
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundTotalCallTime.name, StatPeriod(value)))
          ) =>
        averageAndPublish(value)
      case any =>
    }

  }

}

object AgentInboundAverageAcdCallTime { val name = "InbAverAcdCallTime" }
case class AgentInboundAverageAcdCallTime(
    override val name: String = AgentInboundAverageAcdCallTime.name,
    collector: StatCollector
) extends AgentStatCalculatorByStatValue
    with AgentAverageTime {

  override def processStat(stats: AgentStatistic): Unit = {
    stats match {
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundAnsweredAcdCalls.name, StatTotal(value)))
          ) =>
        storeDivisor(value)
      case AgentStatistic(
            _,
            List(
              Statistic(AgentInboundTotalAcdCallTime.name, StatPeriod(value))
            )
          ) =>
        averageAndPublish(value)
      case any =>
    }

  }

}

object AgentInboundUnansweredCalls { val name = "InbUnansCalls" }
case class AgentInboundUnansweredCalls(
    override val name: String = AgentInboundUnansweredCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByStatValue {
  var total: Long    = 0
  var answered: Long = 0

  private def calc() = total - answered

  override def reset(): Unit = {
    total = 0
    answered = 0
    collector.onStatCalculated(name, StatTotal(calc()))
  }

  override def processStat(stats: AgentStatistic): Unit = {
    stats match {
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundTotalCalls.name, StatTotal(value)))
          ) =>
        total = value
        collector.onStatCalculated(name, StatTotal(calc()))
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundAnsweredCalls.name, StatTotal(value)))
          ) =>
        answered = value
        if (total >= value) collector.onStatCalculated(name, StatTotal(calc()))
      case any =>
    }
  }

}

object AgentInboundUnansweredAcdCalls { val name = "InbUnansAcdCalls" }
case class AgentInboundUnansweredAcdCalls(
    override val name: String = AgentInboundUnansweredAcdCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByStatValue {
  var total: Long    = 0
  var answered: Long = 0

  private def calc() = total - answered

  override def reset(): Unit = {
    total = 0
    answered = 0
    collector.onStatCalculated(name, StatTotal(calc()))
  }

  override def processStat(stats: AgentStatistic): Unit = {
    stats match {
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundTotalAcdCalls.name, StatTotal(value)))
          ) =>
        total = value
        collector.onStatCalculated(name, StatTotal(calc()))
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundAnsweredAcdCalls.name, StatTotal(value)))
          ) =>
        answered = value
        if (total >= value) collector.onStatCalculated(name, StatTotal(calc()))
      case any =>
    }
  }

}

object AgentInboundPercentUnansweredCalls { val name = "InbPercUnansCalls" }
case class AgentInboundPercentUnansweredCalls(
    name: String = AgentInboundPercentUnansweredCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByStatValue {

  var total: Long      = 0
  var unanswered: Long = 0

  private def calc: Double =
    if (total > 0 && total >= unanswered)
      (unanswered.toDouble / total.toDouble) * 100.0
    else {
      if (unanswered > 0) 100.0 else 0.0
    }

  override def reset(): Unit = {
    total = 0
    unanswered = 0
    collector.onStatCalculated(name, StatAverage(calc))
  }
  override def processStat(stats: AgentStatistic): Unit = {
    stats match {
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundTotalCalls.name, StatTotal(value)))
          ) =>
        total = value
        collector.onStatCalculated(name, StatAverage(calc))
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundUnansweredCalls.name, StatTotal(value)))
          ) =>
        unanswered = value
        collector.onStatCalculated(name, StatAverage(calc))
      case any =>
    }
  }
}

object AgentInboundPercentUnansweredAcdCalls {
  val name = "InbPercUnansAcdCalls"
}
case class AgentInboundPercentUnansweredAcdCalls(
    name: String = AgentInboundPercentUnansweredAcdCalls.name,
    collector: StatCollector
) extends AgentStatCalculatorByStatValue {

  var total: Long      = 0
  var unanswered: Long = 0

  private def calc: Double =
    if (total > 0 && total >= unanswered)
      (unanswered.toDouble / total.toDouble) * 100.0
    else {
      if (unanswered > 0) 100.0 else 0.0
    }

  override def reset(): Unit = {
    total = 0
    unanswered = 0
    collector.onStatCalculated(name, StatAverage(calc))
  }
  override def processStat(stats: AgentStatistic): Unit = {
    stats match {
      case AgentStatistic(
            _,
            List(Statistic(AgentInboundTotalAcdCalls.name, StatTotal(value)))
          ) =>
        total = value
        collector.onStatCalculated(name, StatAverage(calc))
      case AgentStatistic(
            _,
            List(
              Statistic(AgentInboundUnansweredAcdCalls.name, StatTotal(value))
            )
          ) =>
        unanswered = value
        collector.onStatCalculated(name, StatAverage(calc))
      case any =>
    }
  }
}
