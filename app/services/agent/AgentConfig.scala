package services.agent

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import org.apache.pekko.pattern.ask
import org.apache.pekko.util.Timeout
import com.google.inject.Inject
import com.google.inject.name.Named
import models.{QueueMembership, UserQueueDefaultMembership}
import services.ActorIds
import services.agent.AgentInGroupAction.{AgentsDestination, RemoveAgents}
import services.config.ConfigDispatcher.{
  ConfigChangeRequest,
  GetAgentByUserId,
  GetQueuesForAgent,
  QueuesForAgent,
  RequestConfig
}
import services.config.{ConfigServerRequester, GetEntry}
import services.request._
import xivo.models.{Agent, AgentQueueMember}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object AgentConfig {
  // Internal messages
  private[AgentConfig] sealed trait AgentConfigInternalRequest
  private[AgentConfig] case class ApplyDefaultMembership(
      membership: UserQueueDefaultMembership
  ) extends AgentConfigInternalRequest
  private[AgentConfig] case class ApplyAgentDefaultMembership(
      agentId: Agent.Id,
      membership: UserQueueDefaultMembership
  ) extends AgentConfigInternalRequest
}

class AgentConfig @Inject() (
    configRequester: ConfigServerRequester,
    @Named(
      ActorIds.DefaultQueueMembershipRepositoryIds
    ) defaultMembershipRepo: ActorRef,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    @Named(ActorIds.AgentGroupSetterId) agentGroupSetter: ActorRef
) extends Actor
    with ActorLogging {

  import AgentConfig._

  var pendingDefaultRequest: Map[Long, UserQueueDefaultMembership] = Map.empty
  var agentMap: Map[Agent.Id, Long]                                = Map.empty

  override def receive: Receive = {

    case BaseRequest(requester, rq: AgentConfigRequest) =>
      handleRequest(requester).apply(rq)

    case r: AgentConfig.AgentConfigInternalRequest => handleInternal(r)

    case a: Agent =>
      agentMap = agentMap + (a.id -> a.userId)
      configDispatcher ! RequestConfig(self, GetQueuesForAgent(a.id))

    case QueuesForAgent(activeQueues, agentId) =>
      agentMap
        .get(agentId)
        .flatMap(pendingDefaultRequest.get(_))
        .map(d =>
          applyDefaultConfiguration(agentId, activeQueues, d.membership)
        )

    case unknown => log.debug(s"unkown $unknown message received")
  }

  def handleInternal: PartialFunction[AgentConfigInternalRequest, Unit] = {
    case ApplyDefaultMembership(membership) =>
      pendingDefaultRequest =
        pendingDefaultRequest + (membership.userId -> membership)
      configDispatcher ! RequestConfig(
        self,
        GetAgentByUserId(membership.userId)
      )

    case ApplyAgentDefaultMembership(agentId, membership) =>
  }

  def handleRequest(
      requester: ActorRef
  ): PartialFunction[AgentConfigRequest, Unit] = {
    case MoveAgentInGroup(
          groupId,
          fromQueueId,
          fromPenalty,
          toQueueId,
          toPenalty
        ) =>
      context.actorOf(
        AgentInGroupMover.props(
          groupId,
          fromQueueId,
          fromPenalty,
          configDispatcher
        )
      ) ! AgentsDestination(toQueueId, toPenalty)

    case AddAgentInGroup(
          groupId,
          fromQueueId,
          fromPenalty,
          toQueueId,
          toPenalty
        ) =>
      context.actorOf(
        AgentInGroupAdder.props(
          groupId,
          fromQueueId,
          fromPenalty,
          configDispatcher
        )
      ) ! AgentsDestination(toQueueId, toPenalty)

    case AddAgentsNotInQueueFromGroupTo(groupId, toQueueId, toPenalty) =>
      context.actorOf(
        AgentFromGroupNotInQueueAdder.props(
          groupId,
          toQueueId,
          toPenalty,
          configDispatcher
        )
      ) ! AgentsDestination(toQueueId, toPenalty)

    case RemoveAgentGroupFromQueueGroup(groupId, queueId, penalty) =>
      context.actorOf(
        AgentGroupRemover.props(groupId, queueId, penalty, configDispatcher)
      ) ! RemoveAgents

    case SetAgentGroup(gId, aId) =>
      agentGroupSetter ! SetAgentGroup(gId, aId)

    case msg: SetUserDefaultMembership =>
      defaultMembershipRepo forward msg

    case msg: SetUsersDefaultMembership =>
      defaultMembershipRepo forward msg

    case GetUserDefaultMembership(userId) =>
      defaultMembershipRepo.tell(GetEntry(userId), requester)

    case ApplyUsersDefaultMembership(userIds) =>
      implicit val timeout: Timeout = Timeout(1.seconds)
      for {
        u <- userIds
        m <- (defaultMembershipRepo ? GetEntry(u))
          .mapTo[UserQueueDefaultMembership] if !m.membership.isEmpty
      } self ! ApplyDefaultMembership(m)
  }

  private def applyDefaultConfiguration(
      agentId: Agent.Id,
      active: List[AgentQueueMember],
      default: List[QueueMembership]
  ): Unit = {
    val activeMap: Map[Long, Int] =
      active.view.map(q => q.queueId -> q.penalty).toMap
    val defaultMap: Map[Long, Int] =
      default.view.map(q => q.queueId -> q.penalty).toMap

    for (q <- active if !defaultMap.contains(q.queueId)) {
      configDispatcher ! ConfigChangeRequest(
        self,
        RemoveAgentFromQueue(agentId, q.queueId)
      )
    }

    default.foreach(defQ => {
      activeMap.get(defQ.queueId) match {
        case Some(actPenalty) =>
          if (defQ.penalty != actPenalty)
            configDispatcher ! ConfigChangeRequest(
              self,
              SetAgentQueue(agentId, defQ.queueId, defQ.penalty)
            )

        case None =>
          configDispatcher ! ConfigChangeRequest(
            self,
            SetAgentQueue(agentId, defQ.queueId, defQ.penalty)
          )
      }
    })

  }
}
