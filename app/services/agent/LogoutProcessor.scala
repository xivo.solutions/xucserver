package services.agent

import org.apache.pekko.actor._
import controllers.helpers.{RequestSuccess, RequestTimeout}
import services.XucEventBus
import services.request.{AgentLogout, BaseRequest}
import xivo.events.AgentState.AgentLoggedOut

import scala.concurrent.duration._

object LogoutProcessor {
  def props(eventBus: XucEventBus, configManager: ActorRef): Props =
    Props(new LogoutProcessor(eventBus, configManager))
}

case class LogoutAgent(phoneNb: String)

class LogoutProcessor(eventBus: XucEventBus, configManager: ActorRef)
    extends Actor
    with ActorLogging {
  log.info(s"starting $self")

  private val timeout = 60.seconds

  context.setReceiveTimeout(timeout)

  override def receive: Receive = {
    case LogoutAgent(phoneNb) =>
      configManager ! BaseRequest(self, AgentLogout(phoneNb))
      context.setReceiveTimeout(1.seconds)
      context.become(expectingResult(sender(), phoneNb))

    case ReceiveTimeout =>
      log.error(s"LogoutProcessor started and not used, stopping itself")
      context.stop(self)

    case unknown =>
      log.error(s"Should not happen, in receive received: $unknown")
      context.stop(self)
  }

  def expectingResult(requester: ActorRef, phoneNb: String): Receive = {

    case AgentLoggedOut(id, _, phoneNb, _, _, _) =>
      log.debug(s"Agent id: $id logged out from $phoneNb")
      requester ! RequestSuccess(s"Agent id: $id logged out from $phoneNb")
      context.stop(self)

    case ReceiveTimeout =>
      log.error(s"Agent logout from phone: $phoneNb timed out")
      requester ! RequestTimeout(
        s"Agent logout from phoneNb: $phoneNb timed out"
      )
      context.stop(self)

    case unknown =>
      log.debug(s"Dropped, in expectingResult received: $unknown")
  }

}
