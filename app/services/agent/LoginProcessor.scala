package services.agent

import org.apache.pekko.actor._
import controllers.helpers.{
  RequestError,
  RequestResult,
  RequestSuccess,
  RequestTimeout
}
import services.XucEventBus
import services.request.{AgentLoginRequest, BaseRequest}
import xivo.events.AgentLoginError
import xivo.events.AgentState.{AgentOnPause, AgentReady}

import scala.concurrent.duration._

object LoginProcessor {
  def props(eventBus: XucEventBus, configManager: ActorRef): Props =
    Props(new LoginProcessor(eventBus, configManager))
}

class LoginProcessor(eventBus: XucEventBus, configManager: ActorRef)
    extends Actor
    with ActorLogging {

  log.info(s"starting $self")

  private val timeout = 60.seconds

  context.setReceiveTimeout(timeout)

  override def receive: Receive = {
    case req: AgentLoginRequest =>
      configManager ! BaseRequest(self, req)
      context.setReceiveTimeout(1.seconds)
      context.become(expectingResult(sender(), req))

    case ReceiveTimeout =>
      log.error(s"LoginProcessor started and not used, stopping itself")
      context.stop(self)

    case unknown =>
      log.error(s"Should not happen, in receive received: $unknown")
      context.stop(self)
  }

  def expectingResult(requester: ActorRef, req: AgentLoginRequest): Receive = {

    case AgentReady(id, _, phoneNb, _, _, _) =>
      log.debug(s"Agent id: $id logged on $phoneNb")
      requester ! RequestSuccess(s"Agent id: $id logged on $phoneNb")
      context.stop(self)

    case AgentOnPause(id, _, phoneNb, _, _, _) =>
      log.debug(s"Agent id: $id logged on $phoneNb")
      requester ! RequestSuccess(
        s"Agent id: $id logged on $phoneNb in pause state"
      )
      context.stop(self)

    case AgentLoginError(error) =>
      requester ! RequestError(s"AgentLogin error: $error")
      context.stop(self)

    case r: RequestResult =>
      requester ! r
      context.stop(self)

    case ReceiveTimeout =>
      log.error(
        s"Agent id: ${req.id.getOrElse("UNKNOWN")} login on phone: ${req.phoneNumber.getOrElse("UNKNOWN")} timed out"
      )
      requester ! RequestTimeout(
        s"Agent id: ${req.id.getOrElse("UNKNOWN")} login on phone: ${req.phoneNumber.getOrElse("UNKNOWN")} timed out"
      )
      context.stop(self)

    case unknown =>
      log.debug(s"Dropped, in expectingResult received: $unknown")
  }

}
