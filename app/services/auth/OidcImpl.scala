package services.auth

import org.apache.pekko.actor.ActorRef
import cats.data.Validated._
import cats.data.ValidatedNec
import cats.implicits._
import com.google.inject.Inject
import controllers.helpers.AuthenticatedAction
import controllers.helpers.XivoAuthenticationHelper.getXivoAuthToken
import helpers.OptionToTry.OptionToTry
import models.XivoUser
import models.ws.auth.SoftwareType.SoftwareType
import models.ws.auth.ApplicationType
import models.ws.auth._
import pdi.jwt.{JwtJson, JwtOptions}
import play.api.libs.json._
import play.api.libs.ws.WSClient
import services.config.ConfigRepository
import system.TimeProvider
import xivo.xuc.{OidcSsoConfig, XucBaseConfig}

import java.io.ByteArrayInputStream
import java.security.cert.{Certificate, CertificateFactory}
import java.security.interfaces.RSAPublicKey
import scala.annotation.unused
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.concurrent.{Await, Future}
import scala.util.chaining._
import scala.util.{Failure, Success, Try}

class OidcImpl @Inject() (implicit timeProvider: TimeProvider) {

  type Token                   = String
  private type TokenInfo       = (JsonWKS, String)
  private type OidcErrorsOr[A] = ValidatedNec[AuthenticationFailure, A]

  val waitResult: FiniteDuration      = 10.seconds
  private val jwtJsonService: JwtJson = JwtJson(timeProvider.getJavaClock)
  private def fetchFromUrl[T <: OIDCConfigPages](
      ws: WSClient,
      url: String
  )(implicit reads: Reads[T]): Future[T] =
    ws.url(url)
      .get()
      .map(_.json.as[T])
      .recoverWith { case e =>
        Future.failed(
          new AuthenticationException(
            AuthenticationError.OidcKeyNotFound,
            e.getMessage
          )
        )
      }

  private def getKidFromTokenHeader(token: Token): Future[String] =
    jwtJsonService
      .decodeJsonAll(token, JwtOptions(signature = false))
      .toOption
      .flatMap(decodedToken => (decodedToken._1 \ "kid").asOpt[String])
      .toTry(
        new AuthenticationException(
          AuthenticationError.OidcKeyNotFound,
          s"Unable to retrieve the kid of token : $token"
        )
      )
      .pipe(Future.fromTry)

  private def parseX509Certificate(x5c: String): Try[Certificate] = Try {
    val cf: CertificateFactory = CertificateFactory.getInstance("X.509")
    val certBytes: Array[Byte] = pdi.jwt.JwtBase64.decodeNonSafe(x5c)
    cf
      .generateCertificate(
        new ByteArrayInputStream(certBytes)
      )
  }

  private def getPubKey(jwks: JsonWKS): Try[RSAPublicKey] = {

    val modulus  = jwks.n
    val exponent = jwks.e

    Try {

      val modulusBytes  = java.util.Base64.getUrlDecoder.decode(modulus)
      val exponentBytes = java.util.Base64.getUrlDecoder.decode(exponent)

      val keyFactory = java.security.KeyFactory.getInstance(
        "RSA"
      )
      val publicKeySpec = new java.security.spec.RSAPublicKeySpec(
        new java.math.BigInteger(1, modulusBytes),
        new java.math.BigInteger(1, exponentBytes)
      )

      keyFactory
        .generatePublic(publicKeySpec)
        .asInstanceOf[RSAPublicKey]
    }
  }

  private def verifyTokenSignature(
      jsonWKS: JsonWKS,
      pem: String
  ): Try[String] =
    getPubKey(jsonWKS)
      .flatMap(pubKey =>
        Try(
          parseX509Certificate(pem)
            .map { parsedCertificate =>
              parsedCertificate.verify(pubKey)
              pubKey
            }
        )
      )
      .recoverWith({ case t =>
        Failure(
          new AuthenticationException(
            AuthenticationError.OidcPubKeyInvalid,
            s"unable to generate pubkey with key set : ${t.getMessage}"
          )
        )
      })
      .map(_.toString)

  private def checkForMainOIDCConfig(
      config: Option[String],
      error: String
  ): Try[String] = {
    config
      .map(Success(_))
      .getOrElse(
        Failure(
          new AuthenticationException(
            AuthenticationError.OidcAuthenticationFailed,
            error
          )
        )
      )
  }

  private def tokenError[E](s: String)(
      @unused e: E
  ): ValidatedNec[AuthenticationFailure, Nothing] =
    AuthenticationFailure(AuthenticationError.InvalidToken, s).invalidNec

  private def validateField[T](field: String, content: JsObject)(implicit
      rds: Reads[T]
  ): ValidatedNec[AuthenticationFailure, T] = {
    (content \ field)
      .validate[T](rds)
      .fold(
        invalid => tokenError(s"Unable to get $field from token")(invalid),
        valid => valid.validNec
      )
  }

  private def extractTokenInfo(
      token: String,
      ws: WSClient,
      wsUrl: String
  ): Try[TokenInfo] = {
    val maybeWellKnown = fetchFromUrl[WellKnown](
      ws,
      wsUrl
    )

    def maybeJWKS(jwksUri: String) =
      fetchFromUrl[JwksUri](ws, jwksUri)
        .map(_.keys)

    val f = (for {
      targetKid <- getKidFromTokenHeader(token)
      wellKnown <- maybeWellKnown
      keysSet   <- maybeJWKS(wellKnown.jwks_uri)
    } yield {
      keysSet
        .find(_.kid == targetKid)
        .flatMap { keySet =>
          keySet.x5c.headOption.map(pem => (keySet, pem))
        }
        .toTry(
          new AuthenticationException(
            AuthenticationError.OidcKeyNotFound,
            s"x5c key is not found from jwks_uri page : ${wellKnown.jwks_uri}"
          )
        )
        .map { tuple =>
          val (keySet, pem) = tuple
          (keySet, pem)
        }
        .pipe(Future.fromTry)
    }).flatten

    Try(Await.result(f, waitResult))
  }

  private def validateIssuerSignature(
      token: String,
      ws: WSClient,
      issuerUrl: String
  ): OidcErrorsOr[String] = {
    extractTokenInfo(
      token,
      ws,
      s"$issuerUrl/.well-known/openid-configuration"
    ).flatMap(tokenInfo =>
      verifyTokenSignature(tokenInfo._1, tokenInfo._2)
    ) match {
      case Failure(exception) =>
        AuthenticationFailure(
          AuthenticationError.SignatureInvalid,
          s"Unable to validate the signature for this token: ${exception.getMessage}"
        ).invalidNec
      case Success(pubKey) => Valid(pubKey)
    }
  }

  private def getLoginFromToken(
      token: String,
      config: OidcSsoConfig,
      oidcMainClientID: String,
      oidcMainURL: String,
      ws: WSClient
  ): Try[String] = {

    def validate(content: JsObject): Try[JsObject] = {
      val getIssuer: OidcErrorsOr[String] =
        validateField[String]("iss", content)
      val getAudience: OidcErrorsOr[List[String]] =
        validateField[String]("aud", content)
          .map(List(_)) orElse validateField[List[String]]("aud", content)
      val getExpiration: OidcErrorsOr[Long] =
        validateField[Long]("exp", content)
      val getClientId: OidcErrorsOr[String] =
        validateField[String]("azp", content) orElse validateField[String](
          "appid",
          content
        )

      def validateIssuer(tokenIssuer: String): OidcErrorsOr[String] = {

        val issuers        = config.oidcAdditionalTrustedServersUrl :+ oidcMainURL
        val matchingIssuer = issuers.find(_.equals(tokenIssuer))

        if (matchingIssuer.nonEmpty)
          Valid(tokenIssuer)
        else
          AuthenticationFailure(
            AuthenticationError.InvalidToken,
            s"Issuer $tokenIssuer not present in issuers list ${issuers.toString()}"
          ).invalidNec
      }

      def validateClientId(clientId: String): OidcErrorsOr[String] = {

        val clientIDs =
          config.oidcAdditionalTrustedClientIDs :+ oidcMainClientID
        val matchingClientID = clientIDs.find(_.equals(clientId))

        if (matchingClientID.nonEmpty)
          Valid(clientId)
        else
          AuthenticationFailure(
            AuthenticationError.InvalidToken,
            s"Client id $clientId not present in client IDs list ${clientIDs.toString()}"
          ).invalidNec
      }

      def validateAudience(audience: List[String]): OidcErrorsOr[List[String]] =
        if (config.oidcAudience.intersect(audience).nonEmpty)
          Valid(audience)
        else
          AuthenticationFailure(
            AuthenticationError.InvalidToken,
            s"The list of audience $audience from the token does not contain any authorized audience from configured audiences ${config.oidcAudience}"
          ).invalidNec

      def validateExpiration(expiration: Long): OidcErrorsOr[Long] =
        if (timeProvider.getJodaTime <= expiration * 1000)
          Valid(expiration)
        else
          AuthenticationFailure(
            AuthenticationError.InvalidToken,
            s"Token expired at $expiration"
          ).invalidNec

      getIssuer
        .andThen(validateIssuer)
        .andThen(issuer => validateIssuerSignature(token, ws, issuer)) *>
        getClientId.andThen(validateClientId) *>
        getAudience.andThen(validateAudience) *>
        getExpiration.andThen(validateExpiration) match {
        case Valid(_) => Success(content)
        case Invalid(errors) =>
          Failure(
            new AuthenticationException(
              AuthenticationError.OidcAuthenticationFailed,
              errors.map(_.message).mkString_(", ")
            )
          )
      }
    }

    jwtJsonService
      .decodeJson(token, JwtOptions(signature = false))
      .flatMap(validate)
      .map(content =>
        (content \ config.oidcUsernameField.getOrElse("preferred_username"))
          .as[String]
      )
      .recoverWith({ case t =>
        Failure(
          new AuthenticationException(
            AuthenticationError.InvalidToken,
            t.getMessage
          )
        )
      })
  }

  def authenticate(
      token: Option[String],
      config: XucBaseConfig,
      repo: ConfigRepository,
      ws: WSClient,
      softwareType: Option[SoftwareType],
      applicationType: Option[ApplicationType]
  )(implicit
      xivoAuthentication: ActorRef
  ): Try[(AuthenticationInformation, XivoUser)] = {

    if (!config.oidcEnable) {
      Failure(
        new AuthenticationException(
          AuthenticationError.OidcNotEnabled,
          "OpenId Connect authentication is not enabled"
        )
      )
    } else {
      token
        .map(t => {
          val now = timeProvider.getJodaTime
          for {
            oidcMainURL <- checkForMainOIDCConfig(
              config.oidcServerUrl,
              "OIDC server main URL is not set"
            )
            oidcMainClientID <- checkForMainOIDCConfig(
              config.oidcClientId,
              "OIDC server main client ID is not set"
            )
            login <- getLoginFromToken(
              t,
              config,
              oidcMainClientID,
              oidcMainURL,
              ws
            )
            xivoUser <-
              repo
                .getCtiUser(login)
                .toTry(
                  new AuthenticationException(
                    AuthenticationError.UserNotFound,
                    s"Matching XiVO User not found : $login"
                  )
                )
            token <- getXivoAuthToken(xivoUser.id, config).toTry(
              new AuthenticationException(
                AuthenticationError.UserNotFound,
                s"XiVO auth token cannot be retrieved for userId ${xivoUser.id}"
              )
            )
          } yield (
            AuthenticationInformation(
              login,
              now + config.Authentication.expires,
              now,
              AuthenticatedAction.ctiUserType,
              AuthenticatedAction.getAliasesFromAcls(token.acls),
              softwareType,
              applicationType
            ),
            xivoUser
          )
        })
        .getOrElse {
          Failure(
            new AuthenticationException(
              AuthenticationError.OidcInvalidParameter,
              "Required access token parameter is not set"
            )
          )
        }
    }
  }
}
