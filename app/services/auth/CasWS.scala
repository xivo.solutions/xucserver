package services.auth

import models.ws.auth.{AuthenticationError, AuthenticationException}
import models.ws.sso._
import play.api.http.Status
import xivo.network.XiVOWS
import xivo.xuc.XucBaseConfig

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait CasWS {

  def validate(
      service: String,
      ticket: String,
      xiVOWS: XiVOWS,
      config: XucBaseConfig
  )(implicit ec: ExecutionContext): Future[CasAuthenticationSuccess] = {
    config.casServerUrl match {
      case Some(url) =>
        val rq = xiVOWS.WS
          .url(url + "/serviceValidate")
          .withQueryStringParameters("service" -> service, "ticket" -> ticket)
          .get()
        rq.flatMap(resp => {
          if (resp.status != Status.OK) {
            Future.failed(
              new AuthenticationException(
                AuthenticationError.CasServerInvalidResponse,
                s"CAS server responded with status code ${resp.status}"
              )
            )
          } else {
            CasServiceResponse.decode(resp.body) match {
              case Success(casResp) =>
                casResp.response match {
                  case auth: CasAuthenticationSuccess => Future.successful(auth)
                  case CasAuthenticationFailure(code, description) =>
                    val authError =
                      CasAuthenticationError.toAuthenticationError(code)
                    Future.failed(
                      new AuthenticationException(
                        authError,
                        description.getOrElse("")
                      )
                    )
                }
              case Failure(t) =>
                Future.failed(
                  new AuthenticationException(
                    AuthenticationError.CasServerInvalidResponse,
                    s"CAS server responded with ${t.getMessage}"
                  )
                )
            }
          }
        })
      case None =>
        Future.failed(
          new AuthenticationException(
            AuthenticationError.CasServerUrlNotSet,
            "CAS server url is not set in configuration"
          )
        )
    }

  }
}

object CasWSImpl extends CasWS {}
