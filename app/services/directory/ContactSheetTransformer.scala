package services.directory

import com.google.inject.Inject
import models.{
  ContactSheet,
  ContactSheetAction,
  ContactSheetActionEnum,
  ContactSheetDataType,
  ContactSheetDetailCategorie,
  ContactSheetDetailField,
  ContactSheetDisplayView,
  ContactSheetSources,
  ContactSheetStatus,
  ContactSheetValueWithLabel,
  ContactSheetWrapper,
  DirSearchItem
}
import org.xivo.cti.model.PhoneHintStatus
import services.MeetingroomLinks
import services.config.{ConfigRepository, ConfigServerRequester}
import services.video.model.VideoEvents
import xivo.xuc.XucConfig

class ContactSheetTransformer @Inject() (
    repo: ConfigRepository,
    configRequester: ConfigServerRequester,
    config: XucConfig,
    meetingroomLinks: MeetingroomLinks
) {

  def formatSingleStringOption(
      option: Option[String],
      disableChat: Boolean = false
  ): ContactSheetAction =
    if disableChat then {
      ContactSheetAction(List.empty, true)
    } else {
      option match
        case Some(optionDefined) if optionDefined != "" =>
          ContactSheetAction(List(optionDefined), false)
        case _ => ContactSheetAction(List.empty, true)
    }
  def getCallAction(
      item: ContactSheetDisplayView
  ): ContactSheetAction =
    val mainNumberList: Option[String] =
      List(item.phone, item.phonePro, item.phoneMobile, item.phoneHome)
        .find(!_.value.isBlank)
        .map(_.value)
    formatSingleStringOption(mainNumberList)

  private def generateActions(
      username: Option[String],
      entry: DirSearchItem
  ): Map[ContactSheetActionEnum.Action, ContactSheetAction] = {
    if entry.item.meetingroom.value then generateMeetingroomsActions(entry)
    else generateContactActions(username, entry)
  }

  private def generateContactActions(
      username: Option[String],
      entry: DirSearchItem
  ): Map[ContactSheetActionEnum.Action, ContactSheetAction] = {

    Map(
      ContactSheetActionEnum.Call  -> getCallAction(entry.item),
      ContactSheetActionEnum.Video -> formatSingleStringOption(username),
      ContactSheetActionEnum.Chat -> formatSingleStringOption(
        username,
        config.chatXucMgt
      ),
      ContactSheetActionEnum.Mail -> formatSingleStringOption(
        Some(entry.item.mail.value)
      )
    ) ++ getEditMap(entry)
  }

  def generateMeetingroomsActions(
      entry: DirSearchItem
  ): Map[ContactSheetActionEnum.Action, ContactSheetAction] = {
    Map(
      ContactSheetActionEnum.Call -> getCallAction(entry.item),
      ContactSheetActionEnum.Video -> formatSingleStringOption(
        entry.source
          .find(dirSource => dirSource.id == "xivo_meetingroom")
          .flatMap(dirSource => dirSource.entry)
      ),
      ContactSheetActionEnum.ShareLink -> formatSingleStringOption(
        meetingroomLinks.getSharingLink(entry)
      )
    ) ++ getEditMap(entry)
  }

  def getEditAction(entry: DirSearchItem): Option[ContactSheetAction] =
    if entry.item.personal.value then
      entry.source.headOption
        .flatMap(source => source.entry)
        .map(entrySourceId => ContactSheetAction(List(entrySourceId), false))
    else None

  def getEditMap(
      entry: DirSearchItem
  ): Map[ContactSheetActionEnum.Action, ContactSheetAction] =
    getEditAction(entry) match
      case Some(value) => Map(ContactSheetActionEnum.Edit -> value)
      case None        => Map.empty

  private def mapToContactSheet(
      entry: DirSearchItem
  ): ContactSheet = {

    val username: Option[String] =
      entry.relations.userId.flatMap(id =>
        repo.getCtiUser(id).flatMap(ctiUser => ctiUser.username)
      )
    ContactSheet(
      name = entry.item.name.value,
      subtitle1 = entry.item.subtitle1.value,
      subtitle2 = entry.item.subtitle2.value,
      picture = entry.item.picture.value,
      isPersonal = entry.item.personal.value,
      isFavorite = entry.item.favorite.value,
      isMeetingroom = entry.item.meetingroom.value,
      canBeFavorite =
        entry.source.exists(dirSource => dirSource.entry.isDefined),
      status = ContactSheetStatus(
        phone =
          if entry.item.phone.value.isBlank then PhoneHintStatus.UNEXISTING
          else
            repo.richPhones
              .getOrElse(entry.item.phone.value, PhoneHintStatus.UNEXISTING)
        ,
        video = username
          .flatMap(u => repo.getUserVideoStatus(u))
          .getOrElse(VideoEvents.Available)
      ),
      actions = generateActions(username, entry),
      sources = entry.source.map(dirSource =>
        ContactSheetSources(dirSource.id, dirSource.entry.getOrElse(""))
      ),
      details = List(
        ContactSheetDetailCategorie(
          "Contacts",
          List(
            ContactSheetDetailField(
              entry.item.phone.label,
              entry.item.phone.value,
              ContactSheetDataType.PhoneNumber
            ),
            ContactSheetDetailField(
              entry.item.phonePro.label,
              entry.item.phonePro.value,
              ContactSheetDataType.PhoneNumber
            ),
            ContactSheetDetailField(
              entry.item.phoneMobile.label,
              entry.item.phoneMobile.value,
              ContactSheetDataType.PhoneNumber
            ),
            ContactSheetDetailField(
              entry.item.phoneHome.label,
              entry.item.phoneHome.value,
              ContactSheetDataType.PhoneNumber
            ),
            ContactSheetDetailField(
              entry.item.mail.label,
              entry.item.mail.value,
              ContactSheetDataType.Mail
            ),
            ContactSheetDetailField(
              entry.item.fax.label,
              entry.item.fax.value,
              ContactSheetDataType.PhoneNumber
            )
          )
        ),
        ContactSheetDetailCategorie(
          "Général",
          List(
            ContactSheetDetailField(
              entry.item.title.label,
              entry.item.title.value,
              ContactSheetDataType.String
            ),
            ContactSheetDetailField(
              entry.item.service.label,
              entry.item.service.value,
              ContactSheetDataType.String
            ),
            ContactSheetDetailField(
              entry.item.manager.label,
              entry.item.manager.value,
              ContactSheetDataType.String
            ),
            ContactSheetDetailField(
              entry.item.company.label,
              entry.item.company.value,
              ContactSheetDataType.String
            ),
            ContactSheetDetailField(
              entry.item.website.label,
              entry.item.website.value,
              ContactSheetDataType.Url
            )
          )
        ),
        ContactSheetDetailCategorie(
          "Lieu d'affectation",
          List(
            ContactSheetDetailField(
              entry.item.office.label,
              entry.item.office.value,
              ContactSheetDataType.String
            ),
            ContactSheetDetailField(
              entry.item.location.label,
              entry.item.location.value,
              ContactSheetDataType.String
            )
          )
        )
      )
    )
  }

  def mapDirSearchResultToContactSheet(
      results: List[DirSearchItem],
      requesterUserId: Long
  ): ContactSheetWrapper =
    new ContactSheetWrapper(
      results.map(result => mapToContactSheet(result))
    );
}
