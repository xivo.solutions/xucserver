package services.directory

import com.google.inject.Inject
import com.google.inject.name.Named
import models.*
import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import services.ActorIds
import services.config.ConfigDispatcher.{MonitorPhoneHint, MonitorVideoStatus}
import services.directory.DirectoryTransformer.{
  EnrichDirectoryResult,
  RawDirectoryResult
}
import xivo.services.XivoDirectory.*
import xivo.websocket.WebSocketEvent
import xivo.websocket.WsBus.WsContent
import models.RichDirectoryEntries.defaultHeaders

object DirectoryTransformer {
  final val serviceName = "DirectoryTransformer"
  case class RawDirectoryResult(
      requester: ActorRef,
      result: XivoDirectoryMsg,
      requesterUserId: Long
  )
  case class EnrichDirectoryResult(
      dirResult: DirSearchResult,
      requesterUserId: Long
  )
}

class DirectoryTransformer @Inject() (
    transformer: DirectoryTransformerLogic,
    contactSheetTransformer: ContactSheetTransformer,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef
) extends Actor
    with ActorLogging {

  private def processResult(
      res: DirectoryResult,
      requesterUserId: Long,
      contactSheetTransformer: ContactSheetTransformer
  ): ContactSheetWrapper = {
    contactSheetTransformer.mapDirSearchResultToContactSheet(
      res.result.items,
      requesterUserId
    )
  }

  private def propagateStatus(
      ref: ActorRef,
      result: DirectoryResult,
      richResults: List[RichEntry]
  ): Unit = {
    val phoneNumbers = transformer.getPhoneNumbers(result.result.items)
    val userNames    = transformer.getUsernames(richResults)
    configDispatcher ! MonitorPhoneHint(
      ref,
      phoneNumbers
    )
    configDispatcher ! MonitorVideoStatus(
      ref,
      userNames
    )
  }

  private def legacyTransform(
      ref: ActorRef,
      result: DirectoryResult,
      requesterUserId: Long,
      richResults: List[RichEntry]
  ): Unit = {
    val obj = RichResult(defaultHeaders)
    obj.entries = richResults
    result match
      case Favorites(_, _) =>
        ref ! WsContent(WebSocketEvent.createEvent(obj, ResultType.Favorite))
      case DirLookupResult(_, _) =>
        ref ! WsContent(WebSocketEvent.createEvent(obj, ResultType.Research))
  }

  private def transform(
      ref: ActorRef,
      result: DirectoryResult,
      requesterUserId: Long
  ): Unit = {
    val event = processResult(
      result,
      requesterUserId,
      contactSheetTransformer
    )
    result match
      case Favorites(_, _) =>
        ref ! WsContent(WebSocketEvent.createEvent(event, ResultType.Favorite))
      case DirLookupResult(_, _) =>
        ref ! WsContent(WebSocketEvent.createEvent(event, ResultType.Research))
  }

  def receive: Receive = {

    case RawDirectoryResult(ref, result: DirectoryResult, requesterUserId) =>
      val mergedResult: DirectoryResult = transformer.mergeContacts(result)
      val richResults =
        transformer.getEnrichResult(mergedResult.result, requesterUserId)
      propagateStatus(ref, mergedResult, richResults)
      if (mergedResult.toContactSheet)
        transform(ref, mergedResult, requesterUserId)
      else
        legacyTransform(
          ref,
          mergedResult,
          requesterUserId,
          richResults
        )

    case RawDirectoryResult(ref, result: FavoriteUpdated, _) =>
      log.debug(s"XivoDirectoryResult: $result")
      ref ! WsContent(WebSocketEvent.createEvent(result))

    case EnrichDirectoryResult(result: DirSearchResult, requesterUserId) =>
      val obj = RichResult(RichDirectoryEntries.defaultHeaders)
      obj.entries = transformer.getEnrichResult(result, requesterUserId)
      log.debug(
        s"Enrich RichDirectoryResult: ${obj.headers} ${obj.entries}"
      )
      sender() ! obj

    case any =>
      log.debug(s"Received unprocessed message: $any")
  }
}
