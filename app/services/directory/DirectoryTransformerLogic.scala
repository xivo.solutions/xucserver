package services.directory

import com.google.inject.Inject
import models.*
import models.Relations.{ResultGrouped, SourceId}
import org.slf4j.{Logger, LoggerFactory}
import org.xivo.cti.model.PhoneHintStatus
import services.MeetingroomLinks
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.services.XivoDirectory.{DirLookupResult, DirectoryResult, Favorites}
import xivo.xuc.XucConfig

import scala.collection.mutable

class DirectoryTransformerLogic @Inject() (
    repo: ConfigRepository,
    configRequester: ConfigServerRequester,
    config: XucConfig,
    meetingroomLinks: MeetingroomLinks
) {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  def enrich(
      entry: DirSearchItem,
      requestedUserId: Long
  ): RichEntry = {
    val lineNumber = entry.item.phone
    val phoneStatus =
      if (lineNumber.value `equals` "") PhoneHintStatus.UNEXISTING
      else
        repo.richPhones.getOrElse(lineNumber.value, PhoneHintStatus.UNEXISTING)
    val item = entry.item
    val username =
      entry.relations.userId.flatMap(repo.getCtiUser(_).flatMap(_.username))

    RichEntry(
      status = phoneStatus,
      fields = mutable.Buffer(
        item.name.value,
        item.phone.value,
        item.phonePro.value,
        item.phoneMobile.value,
        item.favorite.value,
        item.mail.value
      ),
      videoStatus = username.flatMap(u => repo.getUserVideoStatus(u)),
      contactId = entry.source.headOption.flatMap(_.entry),
      source = entry.source.map(_.id),
      favorite = Some(item.favorite.value),
      username = username,
      url = meetingroomLinks.getSharingLink(entry),
      personal = item.personal.value
    )
  }

  def getEnrichResult(
      result: DirSearchResult,
      requesterUserId: Long
  ): List[RichEntry] = {
    result.items.map(entry => {
      enrich(entry, requesterUserId)
    })
  }

  def getPhoneNumbers(result: List[DirSearchItem]): List[String] = {
    result
      .flatMap(_.relations.endpointId)
      .flatMap(x => repo.linePhoneNbs.get(x))
  }

  def getUsernames(results: List[RichEntry]): List[String] = {
    results.flatMap(r => r.username)
  }

  def mergeContacts(directoryResult: DirectoryResult): DirectoryResult =
    if config.directorySourcesPriority.size <= 1 then return directoryResult
    val mergedDirSearchResult: DirSearchResult = mergeDirSearchResult(
      directoryResult.result
    )
    directoryResult match
      case Favorites(_, _) =>
        Favorites(
          result = mergedDirSearchResult,
          toContactSheet = directoryResult.toContactSheet
        )
      case DirLookupResult(_, _) =>
        DirLookupResult(
          result = mergedDirSearchResult,
          toContactSheet = directoryResult.toContactSheet
        )

  private def separateUnmergeSources(
      groupedDirSearchItem: List[List[DirSearchItem]],
      dirSearchItemsByEmail: (String, List[DirSearchItem])
  ): List[List[DirSearchItem]] =
    val (email, dirSearchItems) = dirSearchItemsByEmail

    def containsNoMergeableSource(resultSourceId: String) =
      !config.directorySourcesPriority.contains(resultSourceId)

    def hasSeveralOccurrencesForSource(resultSourceId: String) =
      val nbOccurrencePerSource: Map[SourceId, Int] =
        dirSearchItems
          .flatMap(_.source.map(_.id))
          .groupBy(identity)
          .map((key, result) => (key, result.size))
      nbOccurrencePerSource.getOrElse(resultSourceId, 0) > 1

    def hasNoEmail(result: DirSearchItem) = result.item.mail.value.isBlank

    val (toNotMerge, toMerge) =
      dirSearchItems.partition(result =>
        result.source
          .map(_.id)
          .forall(resultSourceId =>
            containsNoMergeableSource(resultSourceId)
              || hasSeveralOccurrencesForSource(resultSourceId) || hasNoEmail(
                result
              )
          )
      )
    if groupedDirSearchItem.isEmpty then
      List(toMerge) ::: toNotMerge.map(List(_))
    else groupedDirSearchItem ::: List(toMerge) ::: toNotMerge.map(List(_))

  def mergeDirSearchResult(dirSearchResult: DirSearchResult): DirSearchResult =
    val dirSearchItemsByEmail: Map[String, List[DirSearchItem]] =
      dirSearchResult.items.groupBy(r => r.item.mail.value)
    val groupedResults: List[ResultGrouped] = dirSearchItemsByEmail
      .foldLeft(List.empty)(separateUnmergeSources)
      .map(
        ResultGrouped
          .apply(
            _,
            config.directorySourcesPriority,
            config.directoryInternalSource
          )
      )
    val mergedDirSearchItems: List[DirSearchItem] =
      groupedResults.flatMap(_.toDirSearchItem).sortWith(_.rank < _.rank)
    dirSearchResult.copy(items = mergedDirSearchItems)
}
