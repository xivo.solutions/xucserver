package services.request

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsValue, Reads}
import play.api.libs.json.*

case class GetQueueCallHistory(queue: String, size: Int) extends XucRequest

object GetQueueCallHistory {
  def validate(json: JsValue): JsResult[GetQueueCallHistory] =
    json.validate[GetQueueCallHistory]

  implicit val reads: Reads[GetQueueCallHistory] = (
    (JsPath \ "queue").read[String] and
      (JsPath \ "size").read[Int]
  )(GetQueueCallHistory.apply)
}
