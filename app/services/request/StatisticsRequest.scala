package services.request

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsResult, JsValue, Json, Reads}

class StatisticsRequest extends XucRequest

case class QueueStatisticsReq(queueId: Long, window: Int, xqos: Int)
    extends StatisticsRequest

object QueueStatisticsReq {
  def validate(json: JsValue): JsResult[QueueStatisticsReq] =
    json.validate[QueueStatisticsReq]
  implicit val QueueStatisticsReqRead: Reads[QueueStatisticsReq] = (
    (JsPath \ "queueId").read[Long] and
      (JsPath \ "window").read[Int] and
      (JsPath \ "xqos").read[Int]
  )(QueueStatisticsReq.apply)
}
