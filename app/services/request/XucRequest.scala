package services.request

import org.apache.pekko.actor.ActorRef
import models.XucUser

class XucRequest
object XucRequest {
  val ClassProp = "claz"
  val PingClass = "ping"
  val WebClass  = "web"
  val Cmd       = "command"

  object errors {
    val invalid              = "Invalid request"
    val invalidNoCmd: String = s"$invalid unknown cmd field"
    val validation           = "Validation error"
  }
}

object Ping extends XucRequest

case class InvalidRequest(reason: String, request: String) extends XucRequest

case class BaseRequest(requester: ActorRef, request: XucRequest)

case class UserBaseRequest(
    requester: ActorRef,
    request: XucRequest,
    user: XucUser
)
