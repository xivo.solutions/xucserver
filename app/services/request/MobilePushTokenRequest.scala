package services.request

sealed trait MobilePushTokenChange
case class MobilePushTokenAdded(userId: Long)   extends MobilePushTokenChange
case class MobilePushTokenDeleted(userId: Long) extends MobilePushTokenChange
