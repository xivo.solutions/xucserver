package services.request

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, JsResult, JsValue, Json, Reads}

case class PushLogToServer(level: String, log: String) extends XucRequest

object PushLogToServer {
  def validate(json: JsValue): JsResult[PushLogToServer] =
    json.validate[PushLogToServer]
  implicit val PushLogToServerRead: Reads[PushLogToServer] = (
    (JsPath \ "level").read[String] and
      (JsPath \ "log").read[String]
  )(PushLogToServer.apply)
}
