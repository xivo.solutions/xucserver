package services.request

import play.api.libs.json.*

trait UserGroupActionRequest extends XucRequest {
  val groupId: Long
  val userId: Option[Long]
}

trait UserGroupRequestCompanion[T <: UserGroupActionRequest] {
  def apply(groupId: Long, userId: Option[Long]): T

  implicit val userGroupRequestReads: Reads[T] =
    (JsPath \ "groupId").read[Long].map(groupId => apply(groupId, None))

  def validate(json: JsValue): JsResult[T] = json.validate[T]
}

case class UserGroupPauseRequest(groupId: Long, userId: Option[Long])
    extends UserGroupActionRequest
object UserGroupPauseRequest
    extends UserGroupRequestCompanion[UserGroupPauseRequest]

case class UserGroupUnpauseRequest(groupId: Long, userId: Option[Long])
    extends UserGroupActionRequest
object UserGroupUnpauseRequest
    extends UserGroupRequestCompanion[UserGroupUnpauseRequest]
