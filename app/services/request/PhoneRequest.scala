package services.request

import play.api.libs.json.*
import play.api.libs.json.Reads.*
import play.api.libs.functional.syntax.*
import xivo.phonedevices.WebRTCDeviceBearer
import xivo.xucami.models.QueueCall
import xivo.websocket.{
  WsConferenceEvent,
  WsConferenceParticipantOrganizerRole,
  WsConferenceParticipantRole,
  WsConferenceParticipantUserRole
}

abstract class PhoneRequest extends XucRequest

object PhoneRequest {
  case class Dial(
      destination: String,
      variables: Map[String, String] = Map(),
      domain: String = "default"
  ) extends PhoneRequest
  object Dial {
    implicit val reads: Reads[Dial] = ((JsPath \ "destination").read[String] and
      (JsPath \ "variables")
        .readNullable[Map[String, String]]
        .map(_.getOrElse(Map())) and
      (JsPath \ "domain")
        .readNullable[String]
        .map(_.getOrElse("default")))(Dial.apply)
    def validate(json: JsValue): JsResult[Dial] = json.validate[Dial]
  }

  case class DialFromMobile(
      destination: String,
      variables: Map[String, String] = Map()
  ) extends PhoneRequest
  object DialFromMobile {
    implicit val reads: Reads[DialFromMobile] =
      ((JsPath \ "destination").read[String] and
        (JsPath \ "variables")
          .readNullable[Map[String, String]]
          .map(_.getOrElse(Map())))(DialFromMobile.apply)
    def validate(json: JsValue): JsResult[DialFromMobile] =
      json.validate[DialFromMobile]
  }

  case class DialByUsername(
      username: String,
      variables: Map[String, String] = Map(),
      domain: String
  ) extends PhoneRequest
  object DialByUsername {
    implicit val reads: Reads[DialByUsername] =
      ((JsPath \ "username").read[String] and
        (JsPath \ "variables")
          .readNullable[Map[String, String]]
          .map(_.getOrElse(Map())) and
        (JsPath \ "domain")
          .readNullable[String]
          .map(_.getOrElse("default")))(DialByUsername.apply)
    def validate(json: JsValue): JsResult[DialByUsername] =
      json.validate[DialByUsername]
  }

  case class ListenCallbackMessage(
      voiceMsgRef: String,
      variables: Map[String, String] = Map()
  ) extends PhoneRequest
  object ListenCallbackMessage {
    implicit val reads: Reads[ListenCallbackMessage] =
      ((JsPath \ "voiceMessageRef").read[String] and
        (JsPath \ "variables")
          .readNullable[Map[String, String]]
          .map(_.getOrElse(Map())))(ListenCallbackMessage.apply)
    def validate(json: JsValue): JsResult[ListenCallbackMessage] =
      json.validate[ListenCallbackMessage]
  }

  case class Answer(uniqueId: Option[String]) extends PhoneRequest
  object Answer {
    implicit val reads: Reads[Answer] =
      (JsPath \ "uniqueId").readNullable[String].map(o => Answer(o))
    def validate(json: JsValue): JsResult[Answer] = json.validate[Answer]
  }

  case class Hangup(uniqueId: Option[String]) extends PhoneRequest
  object Hangup {
    implicit val reads: Reads[Hangup] =
      (JsPath \ "uniqueId").readNullable[String].map(o => Hangup(o))
    def validate(json: JsValue): JsResult[Hangup] = json.validate[Hangup]
  }

  case class ToggleMicrophone(uniqueId: Option[String]) extends PhoneRequest
  object ToggleMicrophone {
    implicit val reads: Reads[ToggleMicrophone] =
      (JsPath \ "uniqueId").readNullable[String].map(o => ToggleMicrophone(o))
    def validate(json: JsValue): JsResult[ToggleMicrophone] =
      json.validate[ToggleMicrophone]
  }

  case class AttendedTransfer(
      destination: String,
      device: Option[WebRTCDeviceBearer]
  ) extends PhoneRequest
  object AttendedTransfer {
    implicit val reads: Reads[AttendedTransfer] = (
      (JsPath \ "destination").read[String] and
        (JsPath \ "device").readNullable[WebRTCDeviceBearer]
    )(AttendedTransfer.apply)
    def validate(json: JsValue): JsResult[AttendedTransfer] =
      json.validate[AttendedTransfer]
  }

  case object CompleteTransfer extends PhoneRequest {
    def validate(json: JsValue): JsSuccess[CompleteTransfer.type] = JsSuccess(
      CompleteTransfer
    )
  }

  case object CancelTransfer extends PhoneRequest {
    def validate(json: JsValue): JsSuccess[CancelTransfer.type] = JsSuccess(
      CancelTransfer
    )
  }

  case class DirectTransfer(destination: String) extends PhoneRequest
  object DirectTransfer {
    implicit val reads: Reads[DirectTransfer] =
      (JsPath \ "destination").read[String].map(o => DirectTransfer(o))
    def validate(json: JsValue): JsResult[DirectTransfer] =
      json.validate[DirectTransfer]
  }

  case class IncludeToConference(
      role: WsConferenceParticipantRole,
      marked: Boolean = false,
      leaveWhenLastMarkedLeave: Boolean = false,
      callerId: Option[String] = None
  ) extends PhoneRequest {
    def isAdmin: Boolean = role match
      case WsConferenceParticipantOrganizerRole => true
      case WsConferenceParticipantUserRole      => false
  }
  object IncludeToConference {
    import WsConferenceEvent.wsConferenceParticipantRoleReads
    implicit val reads: Reads[IncludeToConference] =
      ((JsPath \ "role")
        .readNullable[WsConferenceParticipantRole]
        .map(_.getOrElse(WsConferenceParticipantUserRole)) and
        (JsPath \ "marked")
          .readNullable[Boolean]
          .map(_.getOrElse(false)) and
        (JsPath \ "leaveWhenLastMarkedLeave")
          .readNullable[Boolean]
          .map(_.getOrElse(false)) and
        (JsPath \ "callerId")
          .readNullable[String])(IncludeToConference.apply)

    def validate(json: JsValue): JsResult[IncludeToConference] =
      json.validate[IncludeToConference]
  }

  case object Conference extends PhoneRequest {
    def validate(json: JsValue): JsSuccess[Conference.type] = JsSuccess(
      Conference
    )
  }

  case class ConferenceInvite(
      numConf: String,
      exten: String,
      role: WsConferenceParticipantRole,
      earlyJoin: Boolean,
      variables: Map[String, String] = Map(),
      marked: Boolean = false,
      leaveWhenLastMarkedLeave: Boolean = false,
      callerId: Option[String] = None
  ) extends PhoneRequest
  object ConferenceInvite {
    import WsConferenceEvent.wsConferenceParticipantRoleReads

    implicit val reads: Reads[ConferenceInvite] =
      ((JsPath \ "numConf").read[String] and
        (JsPath \ "exten").read[String] and
        (JsPath \ "role").read[WsConferenceParticipantRole] and
        (JsPath \ "earlyJoin").read[Boolean] and
        (JsPath \ "variables")
          .readNullable[Map[String, String]]
          .map(_.getOrElse(Map())) and
        (JsPath \ "marked")
          .readNullable[Boolean]
          .map(_.getOrElse(false)) and
        (JsPath \ "leaveWhenLastMarkedLeave")
          .readNullable[Boolean]
          .map(_.getOrElse(false)) and
        (JsPath \ "callerId")
          .readNullable[String])(ConferenceInvite.apply)
    def validate(json: JsValue): JsResult[ConferenceInvite] =
      json.validate[ConferenceInvite]
  }

  case class ConferenceMute(numConf: String, index: Int) extends PhoneRequest
  object ConferenceMute {
    implicit val reads: Reads[ConferenceMute] = (
      (JsPath \ "numConf").read[String] and
        (JsPath \ "index").read[Int]
    )(ConferenceMute.apply)
    def validate(json: JsValue): JsResult[ConferenceMute] =
      json.validate[ConferenceMute]
  }

  case class ConferenceUnmute(numConf: String, index: Int) extends PhoneRequest
  object ConferenceUnmute {
    implicit val reads: Reads[ConferenceUnmute] = (
      (JsPath \ "numConf").read[String] and
        (JsPath \ "index").read[Int]
    )(ConferenceUnmute.apply)
    def validate(json: JsValue): JsResult[ConferenceUnmute] =
      json.validate[ConferenceUnmute]
  }

  case class ConferenceMuteAll(numConf: String) extends PhoneRequest
  object ConferenceMuteAll {
    implicit val reads: Reads[ConferenceMuteAll] =
      (JsPath \ "numConf").read[String].map(o => ConferenceMuteAll(o))
    def validate(json: JsValue): JsResult[ConferenceMuteAll] =
      json.validate[ConferenceMuteAll]
  }

  case class ConferenceUnmuteAll(numConf: String) extends PhoneRequest
  object ConferenceUnmuteAll {
    implicit val reads: Reads[ConferenceUnmuteAll] =
      (JsPath \ "numConf").read[String].map(o => ConferenceUnmuteAll(o))
    def validate(json: JsValue): JsResult[ConferenceUnmuteAll] =
      json.validate[ConferenceUnmuteAll]
  }

  case class ConferenceMuteMe(numConf: String) extends PhoneRequest
  object ConferenceMuteMe {
    implicit val reads: Reads[ConferenceMuteMe] =
      (JsPath \ "numConf").read[String].map(o => ConferenceMuteMe(o))
    def validate(json: JsValue): JsResult[ConferenceMuteMe] =
      json.validate[ConferenceMuteMe]
  }

  case class ConferenceUnmuteMe(numConf: String) extends PhoneRequest
  object ConferenceUnmuteMe {
    implicit val reads: Reads[ConferenceUnmuteMe] =
      (JsPath \ "numConf").read[String].map(o => ConferenceUnmuteMe(o))
    def validate(json: JsValue): JsResult[ConferenceUnmuteMe] =
      json.validate[ConferenceUnmuteMe]
  }

  case class ConferenceKick(numConf: String, index: Int) extends PhoneRequest
  object ConferenceKick {
    implicit val reads: Reads[ConferenceKick] = (
      (JsPath \ "numConf").read[String] and
        (JsPath \ "index").read[Int]
    )(ConferenceKick.apply)
    def validate(json: JsValue): JsResult[ConferenceKick] =
      json.validate[ConferenceKick]
  }

  case class ConferenceClose(numConf: String) extends PhoneRequest
  object ConferenceClose {
    implicit val reads: Reads[ConferenceClose] =
      (JsPath \ "numConf").read[String].map(o => ConferenceClose(o))
    def validate(json: JsValue): JsResult[ConferenceClose] =
      json.validate[ConferenceClose]
  }

  case class ConferenceDeafen(numConf: String, index: Int) extends PhoneRequest
  object ConferenceDeafen {
    implicit val reads: Reads[ConferenceDeafen] = (
      (JsPath \ "numConf").read[String] and
        (JsPath \ "index").read[Int]
    )(ConferenceDeafen.apply)
    def validate(json: JsValue): JsResult[ConferenceDeafen] =
      json.validate[ConferenceDeafen]
  }

  case class ConferenceUndeafen(numConf: String, index: Int)
      extends PhoneRequest
  object ConferenceUndeafen {
    implicit val reads: Reads[ConferenceUndeafen] = (
      (JsPath \ "numConf").read[String] and
        (JsPath \ "index").read[Int]
    )(ConferenceUndeafen.apply)
    def validate(json: JsValue): JsResult[ConferenceUndeafen] =
      json.validate[ConferenceUndeafen]
  }

  case class ConferenceReset(numConf: String) extends PhoneRequest
  object ConferenceReset {
    implicit val reads: Reads[ConferenceReset] =
      (JsPath \ "numConf").read[String].map(o => ConferenceReset(o))
    def validate(json: JsValue): JsResult[ConferenceReset] =
      json.validate[ConferenceReset]
  }

  case class Hold(uniqueId: Option[String]) extends PhoneRequest
  object Hold {
    implicit val reads: Reads[Hold] =
      (JsPath \ "uniqueId").readNullable[String].map(o => Hold(o))
    def validate(json: JsValue): JsResult[Hold] = json.validate[Hold]
  }

  case class SetData(variables: Map[String, String]) extends PhoneRequest
  object SetData {
    implicit val reads: Reads[SetData] =
      (JsPath \ "variables").read[Map[String, String]].map(o => SetData(o))
    def validate(json: JsValue): JsResult[SetData] = json.validate[SetData]
  }

  case object GetCurrentCallsPhoneEvents extends PhoneRequest {
    def validate(json: JsValue): JsSuccess[GetCurrentCallsPhoneEvents.type] =
      JsSuccess(GetCurrentCallsPhoneEvents)
  }

  case class SendDtmfRequest(key: Char) extends PhoneRequest
  object SendDtmfRequest {
    implicit val sendDtmfRead: Reads[SendDtmfRequest] = (JsPath \ "key")
      .read[String](minLength[String](1) keepAnd maxLength[String](1))
      .map(s => SendDtmfRequest(s.charAt(0)))
    def validate(json: JsValue): JsResult[SendDtmfRequest] =
      json.validate[SendDtmfRequest]
  }

  case class RetrieveQueueCall(
      queueCall: QueueCall,
      variables: Map[String, String] = Map()
  ) extends PhoneRequest
  object RetrieveQueueCall {
    implicit val reads: Reads[RetrieveQueueCall] =
      ((JsPath \ "queueCall").read[QueueCall] and
        (JsPath \ "variables")
          .readNullable[Map[String, String]]
          .map(_.getOrElse(Map())))(RetrieveQueueCall.apply)
    def validate(json: JsValue): JsResult[RetrieveQueueCall] =
      json.validate[RetrieveQueueCall]
  }
}
