package services.request

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, JsValue, Reads}
import services.config.ConfigDispatcher.{DeviceType, LineConfigQueryById}
import xivo.models.{Agent, Line}
import play.api.libs.json.JsResult

class UpdateConfigRequest extends XucRequest

case class SetAgentQueue(agentId: Agent.Id, queueId: Long, penalty: Int)
    extends UpdateConfigRequest
object SetAgentQueue {
  def validate(json: JsValue): JsResult[SetAgentQueue] =
    json.validate[SetAgentQueue]
  implicit val SetAgentQueueReads: Reads[SetAgentQueue] = (
    (JsPath \ "agentId").read[Agent.Id] and
      (JsPath \ "queueId").read[Long] and
      (JsPath \ "penalty").read[Int]
  )(SetAgentQueue.apply)
}

case class RemoveAgentFromQueue(agentId: Agent.Id, queueId: Long)
    extends UpdateConfigRequest
object RemoveAgentFromQueue {
  def validate(json: JsValue): JsResult[RemoveAgentFromQueue] =
    json.validate[RemoveAgentFromQueue]
  implicit val SetAgentQueueReads: Reads[RemoveAgentFromQueue] = (
    (JsPath \ "agentId").read[Agent.Id] and
      (JsPath \ "queueId").read[Long]
  )(RemoveAgentFromQueue.apply)
}

case class UpdateLineForUser(userId: Long) extends UpdateConfigRequest

case class ChangeDeviceForUser(
    lineConfigDeviceChangeQuery: LineConfigQueryById,
    currentLine: Option[Line],
    deviceType: DeviceType
) extends UpdateConfigRequest
