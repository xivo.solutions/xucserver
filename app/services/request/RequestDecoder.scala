package services.request

import play.api.libs.json._
import services.config.ConfigDispatcher._
import services.chat.model.BrowserRequestEnvelope
import services.request.PhoneRequest._
import services.video.model.{
  InviteToMeetingRoom,
  MeetingRoomInviteAccept,
  MeetingRoomInviteAck,
  MeetingRoomInviteReject,
  VideoEvent
}

class RequestDecoder {

  val decoders: Map[String, JsValue => JsResult[XucRequest]] = Map(
    "addAgentsInGroup"               -> AddAgentInGroup.validate,
    "addAgentsNotInQueueFromGroupTo" -> AddAgentsNotInQueueFromGroupTo.validate,
    "addFavorite"                    -> AddFavorite.validate,
    "agentLogin"                     -> AgentLoginRequest.validate,
    "agentLogout"                    -> AgentLogoutRequest.validate,
    "answer"                         -> Answer.validate,
    "applyUsersDefaultMembership"    -> ApplyUsersDefaultMembership.validate,
    "attendedTransfer"               -> AttendedTransfer.validate,
    "busyFwd"                        -> BusyForward.validate,
    "cancelTransfer"                 -> CancelTransfer.validate,
    "flashTextBrowserRequest"        -> BrowserRequestEnvelope.validate,
    "completeTransfer"               -> CompleteTransfer.validate,
    "conference"                     -> Conference.validate,
    "conferenceInvite"               -> ConferenceInvite.validate,
    "conferenceMute"                 -> ConferenceMute.validate,
    "conferenceUnmute"               -> ConferenceUnmute.validate,
    "conferenceMuteAll"              -> ConferenceMuteAll.validate,
    "conferenceUnmuteAll"            -> ConferenceUnmuteAll.validate,
    "conferenceMuteMe"               -> ConferenceMuteMe.validate,
    "conferenceUnmuteMe"             -> ConferenceUnmuteMe.validate,
    "conferenceKick"                 -> ConferenceKick.validate,
    "conferenceClose"                -> ConferenceClose.validate,
    "conferenceDeafen"               -> ConferenceDeafen.validate,
    "conferenceUndeafen"             -> ConferenceUndeafen.validate,
    "conferenceReset"                -> ConferenceReset.validate,
    "includeToConference"            -> IncludeToConference.validate,
    "dial"                           -> Dial.validate,
    "dialByUsername"                 -> DialByUsername.validate,
    "dialFromMobile"                 -> DialFromMobile.validate,
    "directoryLookUp"                -> DirectoryLookUp.validate,
    "searchContacts"                 -> SearchContacts.validate,
    "directTransfer"                 -> DirectTransfer.validate,
    "displayNameLookup"              -> DisplayNameLookup.validate,
    "dnd"                            -> DndReq.validate,
    "getAgentDirectory"              -> GetAgentDirectory.validate,
    "getAgentCallHistory"            -> GetAgentCallHistory.validate,
    "getAgentStates"                 -> GetAgentStates.validate,
    "getCallbackLists"               -> GetCallbackLists.validate,
    "findCallbackRequest"            -> FindCallbackRequestWithId.validate,
    "findCustomerCallHistory"        -> CustomerCallHistoryRequestWithId.validate,
    "getCallbackPreferredPeriods"    -> GetCallbackPreferredPeriods.validate,
    "getConfig"                      -> GetConfig.validate,
    "getIceConfig"                   -> GetIceConfig.validate,
    "getFavorites"                   -> GetFavorites.validate,
    "getFavoriteContacts"            -> GetFavoriteContacts.validate,
    "getQueueCallHistory"            -> GetQueueCallHistory.validate,
    "getList"                        -> GetList.validate,
    "getQueueStatistics"             -> QueueStatisticsReq.validate,
    "getUserCallHistory"             -> GetUserCallHistory.validate,
    "getUserCallHistoryByDays"       -> GetUserCallHistoryByDays.validate,
    "getUserDefaultMembership"       -> GetUserDefaultMembership.validate,
    "hangup"                         -> Hangup.validate,
    "hold"                           -> Hold.validate,
    "includeToConference"            -> IncludeToConference.validate,
    "inviteConferenceRoom"           -> InviteConferenceRoom.validate,
    "inviteToMeetingRoom"            -> InviteToMeetingRoom.validate,
    "listenAgent"                    -> AgentListen.validate,
    "listenCallbackMessage"          -> ListenCallbackMessage.validate,
    "meetingRoomInviteAccept"        -> MeetingRoomInviteAccept.validate,
    "meetingRoomInviteAck"           -> MeetingRoomInviteAck.validate,
    "meetingRoomInviteReject"        -> MeetingRoomInviteReject.validate,
    "monitorPause"                   -> MonitorPause.validate,
    "monitorUnpause"                 -> MonitorUnpause.validate,
    "moveAgentsInGroup"              -> MoveAgentInGroup.validate,
    "toggleMicrophone"               -> ToggleMicrophone.validate,
    "naFwd"                          -> NaForward.validate,
    "pauseAgent"                     -> AgentPauseRequest.validate,
    "pauseUserGroup"                 -> UserGroupPauseRequest.validate,
    "releaseCallback"                -> ReleaseCallback.validate,
    "removeAgentFromQueue"           -> RemoveAgentFromQueue.validate,
    "removeAgentGroupFromQueueGroup" -> RemoveAgentGroupFromQueueGroup.validate,
    "removeFavorite"                 -> RemoveFavorite.validate,
    "retrieveQueueCall"              -> RetrieveQueueCall.validate,
    "setAgentGroup"                  -> SetAgentGroup.validate,
    "setAgentQueue"                  -> SetAgentQueue.validate,
    "setUserDefaultMembership"       -> SetUserDefaultMembership.validate,
    "setUsersDefaultMembership"      -> SetUsersDefaultMembership.validate,
    "startCallback"                  -> StartCallback.validate,
    "subscribeToAgentEvents"         -> SubscribeToAgentEvents.validate,
    "subscribeToAgentStats"          -> SubscribeToAgentStats.validate,
    "subscribeToPhoneHints"          -> SubscribeToPhoneHints.validate,
    "unsubscribeFromAllPhoneHints"   -> UnsubscribeFromAllPhoneHints.validate,
    "subscribeToQueueCalls"          -> SubscribeToQueueCalls.validate,
    "unSubscribeToQueueCalls"        -> UnSubscribeToQueueCalls.validate,
    "subscribeToQueueStats"          -> SubscribeToQueueStats.validate,
    "takeCallback"                   -> TakeCallback.validate,
    "toggleUniqueAccountDevice"      -> ToggleUniqueAccountDevice.validate,
    "uncFwd"                         -> UncForward.validate,
    "updateCallbackTicket"           -> UpdateCallbackTicket.validate,
    "unpauseAgent"                   -> AgentUnPauseRequest.validate,
    "unpauseUserGroup"               -> UserGroupUnpauseRequest.validate,
    "userStatusUpdate"               -> UserStatusUpdateReq.validate,
    "setData"                        -> SetData.validate,
    "dialFromQueue"                  -> DialFromQueue.validate,
    "getCurrentCallsPhoneEvents"     -> GetCurrentCallsPhoneEvents.validate,
    "sendDtmf"                       -> SendDtmfRequest.validate,
    "pushLogToServer"                -> PushLogToServer.validate,
    "videoEvent"                     -> VideoEvent.validate,
    "subscribeToVideoStatus"         -> SubscribeToVideoStatus.validate,
    "unsubscribeFromAllVideoStatus"  -> UnsubscribeFromAllVideoStatus.validate,
    "setUserPreference"              -> SetUserPreferenceRequest.validate,
    "unregisterMobileApp"            -> UnregisterMobileApp.validate
  )

  def decodeCmd(message: JsValue): XucRequest = {
    (message \ XucRequest.Cmd).getOrElse(JsNull) match {
      case JsString(cmd) if decoders.contains(cmd) =>
        decoders(cmd)(message) match {
          case s: JsSuccess[XucRequest] => s.get
          case e: JsError =>
            InvalidRequest(
              s"${XucRequest.errors.validation} $e",
              message.toString()
            )
        }
      case _ =>
        InvalidRequest(XucRequest.errors.invalidNoCmd, message.toString())
    }
  }

  def decode(message: JsValue): XucRequest = {
    (message \ XucRequest.ClassProp).getOrElse(JsNull) match {
      case JsString(XucRequest.PingClass) => Ping
      case JsString(XucRequest.WebClass)  => decodeCmd(message)
      case _                              => InvalidRequest(XucRequest.errors.invalid, message.toString())
    }
  }

}
