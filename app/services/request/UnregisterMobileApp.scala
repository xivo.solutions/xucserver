package services.request

import play.api.libs.json.{JsPath, JsResult, JsValue, Json, Reads}

case class UnregisterMobileApp(username: Option[String]) extends XucRequest

object UnregisterMobileApp {
  implicit val reads: Reads[UnregisterMobileApp] = (
    (JsPath \ "username").readNullable[String].map(o => UnregisterMobileApp(o))
  )

  def validate(json: JsValue): JsResult[UnregisterMobileApp] =
    json.validate[UnregisterMobileApp]
}
