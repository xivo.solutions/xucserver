package services.chat.model

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Json, Writes}
import services.VideoChat.VideoChatUser

case class ChatUser(
    username: String,
    phoneNumber: Option[String],
    displayName: Option[String],
    guid: Option[String]
) extends VideoChatUser

object ChatUser {
  implicit val chatUserWriter: Writes[ChatUser] = (
    (JsPath \ "username").write[String] and
      (JsPath \ "phoneNumber").writeNullable[String] and
      (JsPath \ "displayName").writeNullable[String] and
      (JsPath \ "guid").writeNullable[String]
  )(o => (o.username, o.phoneNumber, o.displayName, o.guid))
}
