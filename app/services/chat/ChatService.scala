package services.chat

import org.apache.pekko.actor._
import com.google.inject.Inject
import helpers.JmxActorSingletonMonitor
import models.XivoUser
import services.{ActorIds, VideoChat}
import services.chat.ChatService.Username
import services.chat.model._
import services.config.ConfigRepository
import services.VideoChat.{ChatStatus, VideoChatStates}
import xivo.models._
import xivo.network.ChatLink.{RetrieveDirectChannels, StopChatLink}
import xivo.network._
import xivo.xuc.ChatConfig

import java.time.OffsetDateTime
import java.util.NoSuchElementException
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import helpers.JmxLongMetric
import scala.util.Try

case class ChatUserState(
    user: ChatUser,
    status: ChatStatus,
    ctiRouterRef: Option[ActorRef],
    chatLinkRef: Option[ActorRef],
    xivoUser: XivoUser
) extends VideoChatStates[ChatUserState, ChatStatus, ChatUser] {

  def updateGuid(newGuid: Option[String]): ChatUserState = {
    val userWGuid = this.user.copy(guid = newGuid)
    this.copy(user = userWGuid)
  }

  def updateChatLinkRef(
      newChatLinkRef: Option[ActorRef]
  ): ChatUserState = {
    this.copy(chatLinkRef = newChatLinkRef)
  }

  def stopChatLink: Unit = chatLinkRef.foreach(_ ! StopChatLink)

  override def disconnect: ChatUserState = {
    stopChatLink
    updateChatLinkRef(None)
      .updateGuid(None)
  }

  override def withStatus(status: ChatStatus): ChatUserState =
    this.copy(status = status)
}

sealed trait FlashTextServiceActorMessage

case class UpdateChatLinkRef(ChatLinkRef: ActorRef, username: String)
    extends FlashTextServiceActorMessage

class ChatService @Inject() (
    config: ChatConfig,
    chatBackendWS: ChatBackendWS,
    configRepo: ConfigRepository,
    flashTextUtil: ChatUtil
) extends Actor
    with ActorLogging
    with VideoChat[ChatUserState, ChatStatus, ChatUser]
    with JmxActorSingletonMonitor {

  var users: Map[Username, VideoChatStates[
    ChatUserState,
    ChatStatus,
    ChatUser
  ]] = Map.empty
  val jmxUsersCount: Try[JmxLongMetric] =
    jmxBean.addLong("Users", 0L, Some("Number of flash text users"))

  def getDate: OffsetDateTime = OffsetDateTime.now()

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))

    if (config.chatEnable) {
      context.become(
        processMattermostRequest orElse processMattermostResponse orElse processServiceRequest
      )
    } else {
      context.become(processServiceRequest)
    }
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  def processServiceRequest: Receive = {
    case _: FlashTextRequest if !config.chatEnable =>
      log.info("Chat disabled, request won't be processed.")
    case other => log.warning(s"Unexpected request recieved, $other")
  }

  def processMattermostRequest: Receive = {
    case ConnectFlashTextUser(xivoUser) =>
      val ctiRouterRef = sender()
      log.debug(s"Connect Flash Text User : $xivoUser, $ctiRouterRef")
      xivoUser.username.foreach(username =>
        getOrCreateMattermostUser(xivoUser, username)
          .map(guid => self ! UpdateUserWithGuid(guid))
          .recover(handleErrors(s"Unable to get user.", username, "", 0))
      )

      users = disconnectXivoUser(xivoUser, ChatStatus.Unavailable)(users)
      users = connectXivoUser(
        xivoUser,
        newState(xivoUser, ctiRouterRef, ChatStatus.Available)
      )(users)
      jmxUsersCount.set(users.size)

    case DisconnectFlashTextUser(xivoUser) =>
      log.debug(s"Disconnect Flash Text User : $xivoUser")
      users = disconnectXivoUser(xivoUser, ChatStatus.Unavailable)(users)

      jmxUsersCount.set(users.size)

    case SendDirectMessage(from, to, message, seq) =>
      val date: OffsetDateTime = getDate
      log.debug(
        s"Send message from : $from, to : $to, seq : $seq, date: $date, message : $message"
      )

      getParties(from, to)
        .map(p => persistToMattermost(p, message, seq))
        .recover { case e =>
          handleErrors(
            "Unable to send message",
            from,
            to,
            seq
          )(MessagePersistError(e.getMessage))
        }

    case GetDirectMessageHistory((from, to), seq) =>
      val date: OffsetDateTime = getDate
      log.debug(
        s"Get history of $from, with : $to, seq : $seq, date: $date"
      )

      getParties(from, to)
        .map(p => getHistoryFromMattermost(p, seq))
        .recover { case e =>
          handleErrors("Unable to fetch chat history", from, to, seq)(
            MessageHistoryError(e.getMessage)
          )
        }

    case MarkDirectchannelAsRead((from, to), seq) =>
      val date: OffsetDateTime = getDate
      log.debug(
        s"Mark messages as read of $from, with : $to, seq : $seq, date: $date"
      )

      getParties(from, to)
        .map(p => markChannelAsRead(p, seq))
        .recover { case e =>
          handleErrors(s"Unable to mark channel as read", from, to, seq)
        }
  }

  def processMattermostResponse: Receive = {
    case m: MattermostDirectMessageAck =>
      log.debug(s"Message successfully sent to mattermost backend")
      sendAckToCtiRouter(m)
      sendMessageToCtiRouter(m)

    case UpdateUserWithGuid(m: MattermostGuid) =>
      log.debug(s"Update guid for ${m.username} : $m")
      users
        .get(m.username)
        .collect { case s: ChatUserState => s }
        .map(_.updateGuid(Some(m.guid)))
        .foreach { userState =>
          createChatLink(userState, m.guid)
          users = users.updated(m.username, userState)
        }

    case UpdateChatLinkRef(chatLinkRef: ActorRef, username: String) =>
      log.debug(s"Chatlink actor created for $username : $chatLinkRef")
      users
        .get(username)
        .collect { case s: ChatUserState => s }
        .map(_.updateChatLinkRef(Some(chatLinkRef)))
        .foreach(st => users = users.updated(username, st))

      chatLinkRef ! RetrieveDirectChannels
  }

  def receive: Receive = Actor.emptyBehavior

  private def newState(
      xivoUser: XivoUser,
      cti: ActorRef,
      status: ChatStatus = ChatStatus.Available
  )(username: String) =
    ChatUserState(
      ChatUser(
        username,
        flashTextUtil.getPhoneNumber(username),
        Some(xivoUser.fullName),
        None
      ),
      status,
      Some(cti),
      None,
      xivoUser
    )

  def persistToMattermost(
      p: MattermostParties,
      message: String,
      seq: Long
  ): Unit = {
    getChatlink(p.from.username) match {
      case Some(ref) =>
        ref.forward(SendDirectMattermostMessage(p.from, p.to, message, seq))
      case None =>
        throw new Exception(
          s"Chat link actor not found for user ${p.from.username}"
        )
    }
  }

  private def getHistoryFromMattermost(
      p: MattermostParties,
      seq: Long
  ): Unit = {
    getChatlink(p.from.username) match {
      case Some(ref) =>
        ref.forward(GetDirectMattermostMessagesHistory(p.from, p.to, seq))
      case None =>
        throw new Exception(
          s"Chat link actor not found for user ${p.from.username}"
        )
    }
  }

  private def markChannelAsRead(p: MattermostParties, seq: Long): Unit = {
    getChatlink(p.from.username) match {
      case Some(ref) => ref ! MarkMattermostMessagesAsRead(p.from, p.to, seq)
      case None =>
        throw new Exception(
          s"Chat link actor not found for user ${p.from.username}"
        )
    }
  }

  def getOrCreateMattermostUser(
      xivoUser: XivoUser,
      username: String
  ): Future[MattermostGuid] = {
    chatBackendWS
      .getCtiUser(xivoUser.id)
      .flatMap(usersList =>
        if (usersList.isEmpty) {
          log.debug(s"Creating a user for ${xivoUser.username} in chat server")
          chatBackendWS
            .createUser(
              xivoUser.id,
              xivoUser.firstname,
              xivoUser.lastname.getOrElse("")
            )
            .map { mattermostUser =>
              log.debug(s"User created in mattermost : $mattermostUser")
              MattermostGuid(
                mattermostUser.id,
                username,
                flashTextUtil.getDisplayName(xivoUser)
              )
            }
        } else {
          log.debug(
            s"Mattermost user retrieved : ${usersList.head.id}, id: ${xivoUser.id}"
          )
          Future.successful(
            MattermostGuid(
              usersList.head.id,
              username,
              flashTextUtil.getDisplayName(xivoUser)
            )
          )
        }
      )
  }

  def createChatLink(
      flashTextUserState: ChatUserState,
      guid: String
  ): Unit =
    chatBackendWS
      .getUserToken(flashTextUserState.xivoUser.id)
      .foreach { token =>
        log.debug(
          s"Retrieve token for ${flashTextUserState.user.username} : $token"
        )
        createChatLinkActor(flashTextUserState, token, guid)
      }

  def createChatLinkActor(
      flashTextUserState: ChatUserState,
      token: String,
      guid: String
  ): ActorRef = {
    context.actorOf(
      ChatLink.props(
        flashTextUserState,
        token,
        self,
        chatBackendWS,
        guid,
        flashTextUtil
      ),
      ActorIds.chatLinkActorName(flashTextUserState.user.username)
    )
  }

  private def sendAckToCtiRouter(m: MattermostDirectMessageAck): Unit = {
    users
      .get(m.usernameTo)
      .map(_.status)
      .collect {
        case s if s == ChatStatus.Unavailable =>
          getCtiRouter(m.usernameFrom).foreach(
            _ ! RequestAck(m.seq, offline = true, m.offsetDateTime)
          )
        case _ =>
          getCtiRouter(m.usernameFrom).foreach(
            _ ! RequestAck(m.seq, date = m.offsetDateTime)
          )
      }
      .getOrElse(
        getCtiRouter(m.usernameFrom)
          .foreach(_ ! RequestAck(m.seq, offline = true, m.offsetDateTime))
      )
  }

  private def sendMessageToCtiRouter(m: MattermostDirectMessageAck) = {
    for {
      userFrom    <- users.get(m.usernameFrom)
      userTo      <- users.get(m.usernameTo)
      ctiRouterTo <- userTo.ctiRouterRef
    } yield ctiRouterTo ! Message(
      userFrom.user,
      userTo.user,
      m.message,
      m.offsetDateTime,
      m.seq
    )
  }

  private def getLocalMattermostUser(
      recipient: String
  ): Option[MattermostGuid] = {
    for {
      userFrom <- users.get(recipient).map(_.user)
      guidFrom <- userFrom.guid
    } yield MattermostGuid(guidFrom, userFrom.username, userFrom.displayName)
  }

  private def getParties(
      from: String,
      to: String
  ): Future[MattermostParties] = {
    for {
      fromUser <- getMattermostUser(from)
      toUser   <- getMattermostUser(to)
    } yield MattermostParties(fromUser, toUser)
  }

  private def getMattermostUser(username: String): Future[MattermostGuid] = {
    getLocalMattermostUser(username)
      .map(Future.successful)
      .getOrElse {
        configRepo
          .getCtiUser(username)
          .flatMap(xivoUser =>
            xivoUser.username.map(u => getOrCreateMattermostUser(xivoUser, u))
          )
          .getOrElse(
            Future.failed(
              new NoSuchElementException(
                s"User $username's guid not available."
              )
            )
          )
      }
  }

  def handleErrors(
      message: String,
      from: String,
      to: String,
      seq: Long
  ): PartialFunction[Throwable, Unit] = { case e =>
    log.error(s"$message. ${e.getMessage}")
    e match {
      case _: MessageHistoryError =>
        getCtiRouter(from).foreach(
          _ ! MessageHistory(
            (
              ChatUser(from, None, None, None),
              ChatUser(to, None, None, None)
            ),
            List(),
            0
          )
        )
      case _: MessagePersistError =>
        getCtiRouter(from).foreach(_ ! RequestNack(seq))
      case _: Exception => getCtiRouter(from).foreach(_ ! RequestNack(seq))
    }
  }

  private def getCtiRouter(username: String): Option[ActorRef] =
    users
      .get(username)
      .collect { case s: ChatUserState => s }
      .flatMap(_.ctiRouterRef)

  private def getChatlink(username: String): Option[ActorRef] =
    users
      .get(username)
      .collect { case s: ChatUserState => s }
      .flatMap(_.chatLinkRef)
}

object ChatService {
  final val serviceName = "ChatService"
  type Username = String

  def props(
      config: ChatConfig,
      chatBackendWS: ChatBackendWS,
      configRepo: ConfigRepository,
      flashTextUtil: ChatUtil
  ): Props =
    Props(
      new ChatService(config, chatBackendWS, configRepo, flashTextUtil)
    )
}
