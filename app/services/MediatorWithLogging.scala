package services

import helpers.{FastLogging, JmxActorSingletonMonitor}
import org.apache.pekko.actor.{Actor, ActorRef, ActorSystem}
import org.apache.pekko.cluster.pubsub.DistributedPubSub
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.{
  Publish,
  Subscribe,
  Unsubscribe
}

class MediatorWithLogging
    extends Actor
    with FastLogging
    with JmxActorSingletonMonitor {
  val mediator: ActorRef = DistributedPubSub(context.system).mediator

  private def newMediatorLog(msg: String): Unit = log.debug(s"Mediator - $msg")

  override def receive: Receive = {
    case pub: Publish =>
      newMediatorLog(s"Sending ${pub.msg} in ${pub.topic} topic")
      mediator ! pub
    case sub: Subscribe =>
      newMediatorLog(s"Subscribing ${sub.ref} on topic ${sub.topic}")
      mediator ! sub
    case unsub: Unsubscribe =>
      newMediatorLog(s"Unsubscribing ${unsub.ref} from topic ${unsub.topic}")
      mediator ! unsub
    case other: Any => mediator ! other
  }

}
