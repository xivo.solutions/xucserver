package services

import com.google.inject.Inject
import models.DirSearchItem
import services.config.ConfigServerRequester
import xivo.xuc.XucConfig

import scala.concurrent.Await
import scala.util.{Success, Try}

class MeetingroomLinks @Inject() (
    configRequester: ConfigServerRequester,
    config: XucConfig
) {

  def getMeetingRoomAlias(
      roomId: String
  ): Option[String] =
    Try(
      Await.result(
        configRequester.getMeetingRoomAlias(roomId),
        config.defaultWSTimeout
      )
    ) match {
      case Success(room) if room.alias.isDefined =>
        Some(s"/meet?id=${room.alias.get}")
      case _ => None
    }

  def getSharingLink(
      result: DirSearchItem
  ): Option[String] = {
    if (
      result.source.map(dirSource => dirSource.id).contains("xivo_meetingroom")
    ) {
      getMeetingRoomAlias(
        result.source
          .filter(dirSource => dirSource.id == "xivo_meetingroom")
          .head
          .entry
          .getOrElse("")
      )
    } else {
      None
    }
  }
}
