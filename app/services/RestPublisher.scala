package services

import com.google.inject.Inject
import helpers.LogFutureFailure
import play.api.Logger
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSClient
import xivo.xuc.XucBaseConfig
import play.api.libs.ws.WSBodyWritables.writeableOf_JsValue

import scala.concurrent.ExecutionContext.Implicits.global

class RestPublisher @Inject() (ws: WSClient, config: XucBaseConfig) {
  implicit val log: Logger = Logger(getClass.getName)
  val headers: List[(String, String)] =
    List(("Content-Type", "application/json"), ("Accept", "application/json"))

  def publish(username: String, jsonMessage: JsValue): Unit = {
    val url = config.EVENT_URL

    log.debug(s"publishing message $buildEvent to $url")

    if (url != "") {
      try {
        ws.url(url)
          .withHttpHeaders(headers*)
          .post(buildEvent)
          .recoverWith(
            LogFutureFailure.logFailure(s"Unable to publish event to $url")
          )
      } catch {
        case e: Throwable => log.error("Unable to send event " + e.getMessage)
      }
    } else {
      log.debug("no Url configured to publish user configuration")
    }

    def buildEvent = Json.obj("username" -> username, "message" -> jsonMessage)
  }

}
