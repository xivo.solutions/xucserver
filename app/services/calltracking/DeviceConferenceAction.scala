package services.calltracking

import models.XivoUser
import services.XucAmiBus.*
import xivo.websocket.{
  WsConferenceParticipantOrganizerRole,
  WsConferenceParticipantRole
}
import xivo.xucami.models.ChannelOptions

object DeviceConferenceAction {
  sealed trait DeviceConferenceCommand {
    val numConf: String
  }
  case class Mute(numConf: String, index: Int)   extends DeviceConferenceCommand
  case class Unmute(numConf: String, index: Int) extends DeviceConferenceCommand
  case class MuteAll(numConf: String)            extends DeviceConferenceCommand
  case class UnmuteAll(numConf: String)          extends DeviceConferenceCommand
  case class MuteMe(numConf: String)             extends DeviceConferenceCommand
  case class UnmuteMe(numConf: String)           extends DeviceConferenceCommand
  case class Kick(numConf: String, index: Int)   extends DeviceConferenceCommand
  case class Close(numConf: String)              extends DeviceConferenceCommand
  case class Invite(
      numConf: String,
      exten: String,
      role: WsConferenceParticipantRole,
      earlyJoin: Boolean,
      variables: Map[String, String],
      marked: Boolean,
      leaveWhenLastMarkedLeave: Boolean,
      callerId: Option[String],
      xivoUser: Option[XivoUser]
  ) extends DeviceConferenceCommand
  case class Deafen(numConf: String, index: Int) extends DeviceConferenceCommand
  case class Undeafen(numConf: String, index: Int)
      extends DeviceConferenceCommand
  case class Reset(numConf: String) extends DeviceConferenceCommand

  sealed trait DeviceConferenceCommandErrorType
  case object NotAMember          extends DeviceConferenceCommandErrorType
  case object NotOrganizer        extends DeviceConferenceCommandErrorType
  case object CannotKickOrganizer extends DeviceConferenceCommandErrorType
  case object ParticipantNotFound extends DeviceConferenceCommandErrorType
  case object ConferenceNotFound  extends DeviceConferenceCommandErrorType
  case object OrganizerStillThere extends DeviceConferenceCommandErrorType

  def toAmi(
      conferences: Map[String, ConferenceWithChannels],
      action: DeviceConferenceCommand,
      context: String
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    action match {
      case a: Invite    => invite(conferences, a, context, a.variables)
      case a: Mute      => mute(conferences, a)
      case a: Unmute    => unmute(conferences, a)
      case a: MuteAll   => muteAll(conferences, a)
      case a: UnmuteAll => unmuteAll(conferences, a)
      case a: MuteMe    => muteMe(conferences, a)
      case a: UnmuteMe  => unmuteMe(conferences, a)
      case a: Kick      => kick(conferences, a)
      case a: Close     => close(conferences, a)
      case a: Deafen    => deafen(conferences, a)
      case a: Undeafen  => undeafen(conferences, a)
      case a: Reset     => reset(conferences, a)
    }

  private def findConference(
      conferences: Map[String, ConferenceWithChannels],
      numConf: String
  ): Either[DeviceConferenceCommandErrorType, ConferenceWithChannels] =
    conferences
      .get(numConf)
      .map(Right(_))
      .getOrElse(Left(NotAMember))

  private def findMeIn(
      conferences: Map[String, ConferenceWithChannels],
      numConf: String
  ): Either[DeviceConferenceCommandErrorType, List[ConferenceParticipant]] =
    conferences
      .get(numConf)
      .map(_.findMe())
      .filter(_.nonEmpty)
      .map(Right(_))
      .getOrElse(Left(NotAMember))

  private def findParticipant(
      conferences: Map[String, ConferenceWithChannels],
      numConf: String,
      index: Int
  ): Either[DeviceConferenceCommandErrorType, ConferenceParticipant] =
    conferences
      .get(numConf)
      .flatMap(_.conference.getParticipant(index))
      .map(Right(_))
      .getOrElse(Left(ParticipantNotFound))

  private def findOthers(
      conferences: Map[String, ConferenceWithChannels],
      numConf: String
  ): List[ConferenceParticipant] = {
    val meIndices = findMeIn(conferences, numConf) match {
      case Left(_)  => List.empty
      case Right(l) => l.map(_.index)
    }

    conferences
      .get(numConf)
      .map(_.conference.participants)
      .getOrElse(List.empty)
      .filterNot(p => meIndices.contains(p.index))
  }

  private def organizerCountLesserThanOrEquals(
      confwchan: ConferenceWithChannels,
      maxOrg: Int
  ): Either[DeviceConferenceCommandErrorType, ConferenceWithChannels] = {
    val orgCount = confwchan.conference.participants
      .filter(_.role == OrganizerRole)
      .length
    if (orgCount <= maxOrg) {
      Right(confwchan)
    } else {
      Left(OrganizerStillThere)
    }
  }

  private def isOrganizer(
      conferences: Map[String, ConferenceWithChannels],
      numConf: String
  ): Boolean =
    findMeIn(conferences, numConf) match {
      case Left(_)  => false
      case Right(l) => l.map(_.role).contains(OrganizerRole)
    }

  private def checkIAmAnOrganizer(
      conference: ConferenceWithChannels
  ): Either[DeviceConferenceCommandErrorType, ConferenceWithChannels] = {
    val hasRole = conference
      .findMe()
      .map(_.role)
      .filter(_ == OrganizerRole)

    if (hasRole.nonEmpty) Right(conference)
    else Left(NotOrganizer)
  }

  def getMeetmeOptions(
      isAdmin: Boolean,
      quietEnterAndLeave: Boolean,
      marked: Boolean,
      leaveWhenLastMarkedLeave: Boolean
  ): String =
    val com = ChannelOptions.MeetMe
    com.Defaults
      + (if (quietEnterAndLeave) com.QuietEnterAndLeave else "")
      + (if (isAdmin) com.Admin else "")
      + (if (marked) com.Marked else "")
      + (if (leaveWhenLastMarkedLeave) com.MarkedLeave else "")
      + com.WaitingMusic

  private def invite(
      conferences: Map[String, ConferenceWithChannels],
      action: Invite,
      context: String,
      variables: Map[String, String]
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] = {
    for {
      confWithChannel <- findConference(conferences, action.numConf)
      _               <- checkIAmAnOrganizer(confWithChannel)
      conf    = confWithChannel.conference
      isAdmin = action.role == WsConferenceParticipantOrganizerRole
      pin     = if (isAdmin) conf.adminPin else conf.userPin
    } yield List(
      InviteInConferenceActionRequest(
        action.exten,
        action.numConf,
        pin,
        isAdmin,
        action.earlyJoin,
        context,
        variables,
        action.callerId,
        action.xivoUser,
        getMeetmeOptions(
          isAdmin,
          true,
          action.marked,
          action.leaveWhenLastMarkedLeave
        )
      )
    )
  }

  private def muteMe(
      conferences: Map[String, ConferenceWithChannels],
      action: MuteMe
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    findMeIn(conferences, action.numConf)
      .map(_.map(p => MeetMeMuteRequest(action.numConf, p.index)))

  private def unmuteMe(
      conferences: Map[String, ConferenceWithChannels],
      action: UnmuteMe
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    findMeIn(conferences, action.numConf)
      .map(_.map(p => MeetMeUnmuteRequest(action.numConf, p.index)))

  private def mute(
      conferences: Map[String, ConferenceWithChannels],
      action: Mute
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    if (isOrganizer(conferences, action.numConf))
      findParticipant(conferences, action.numConf, action.index)
        .map(_ => List(MeetMeMuteRequest(action.numConf, action.index)))
    else
      Left(NotOrganizer)

  private def unmute(
      conferences: Map[String, ConferenceWithChannels],
      action: Unmute
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    if (isOrganizer(conferences, action.numConf))
      findParticipant(conferences, action.numConf, action.index)
        .map(_ => List(MeetMeUnmuteRequest(action.numConf, action.index)))
    else
      Left(NotOrganizer)

  private def muteAll(
      conferences: Map[String, ConferenceWithChannels],
      action: MuteAll
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    if (isOrganizer(conferences, action.numConf))
      Right(
        findOthers(conferences, action.numConf)
          .map(p => MeetMeMuteRequest(action.numConf, p.index))
      )
    else
      Left(NotOrganizer)

  private def unmuteAll(
      conferences: Map[String, ConferenceWithChannels],
      action: UnmuteAll
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    if (isOrganizer(conferences, action.numConf))
      Right(
        findOthers(conferences, action.numConf)
          .map(p => MeetMeUnmuteRequest(action.numConf, p.index))
      )
    else
      Left(NotOrganizer)

  private def kick(
      conferences: Map[String, ConferenceWithChannels],
      action: Kick
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    if (isOrganizer(conferences, action.numConf)) {
      val targetRole = conferences
        .get(action.numConf)
        .flatMap(_.conference.getParticipant(action.index))
        .map(_.role)

      if (targetRole.contains(OrganizerRole))
        Left(CannotKickOrganizer)
      else
        findParticipant(conferences, action.numConf, action.index)
          .map(_ => List(MeetMeKickRequest(action.numConf, action.index)))
    } else
      Left(NotOrganizer)

  private def close(
      conferences: Map[String, ConferenceWithChannels],
      action: Close
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    for {
      conf <- findConference(conferences, action.numConf)
      _    <- checkIAmAnOrganizer(conf)
    } yield conf.conference.participants.map(p =>
      HangupActionRequest(p.channelName)
    )

  private def deafen(
      conferences: Map[String, ConferenceWithChannels],
      action: Deafen
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    if (isOrganizer(conferences, action.numConf))
      findParticipant(conferences, action.numConf, action.index)
        .map(p => List(MeetMeDeafenRequest(p.channelName, action.index)))
    else
      Left(NotOrganizer)

  private def undeafen(
      conferences: Map[String, ConferenceWithChannels],
      action: Undeafen
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] =
    if (isOrganizer(conferences, action.numConf))
      findParticipant(conferences, action.numConf, action.index)
        .map(p => List(MeetMeUndeafenRequest(p.channelName, action.index)))
    else
      Left(NotOrganizer)

  private def reset(
      conferences: Map[String, ConferenceWithChannels],
      action: Reset
  ): Either[DeviceConferenceCommandErrorType, List[AmiActionRequest]] = {
    val unmuteRq = findOthers(conferences, action.numConf)
      .filter(_.isMuted)
      .map(p => MeetMeUnmuteRequest(action.numConf, p.index))
    val undeafRq = findOthers(conferences, action.numConf)
      .filter(_.isDeaf)
      .map(p => MeetMeUndeafenRequest(p.channelName, p.index))
    for {
      conf <- findConference(conferences, action.numConf)
      _    <- checkIAmAnOrganizer(conf)
      _    <- organizerCountLesserThanOrEquals(conf, 1)
    } yield unmuteRq ++ undeafRq
  }
}
