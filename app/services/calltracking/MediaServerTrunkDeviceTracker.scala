package services.calltracking

import org.apache.pekko.actor.*
import services.{ActorIds, XucAmiBus}
import services.calltracking.DeviceMessage.*
import xivo.xuc.DeviceTrackerConfig

class MediaServerTrunkDeviceTracker(
    trunkInterface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig,
    mediator: ActorRef
) extends UnknownDeviceTracker(
      trunkInterface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig,
      mediator
    ) {

  override val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.MediaServerTrunkDeviceTrackerType

}
