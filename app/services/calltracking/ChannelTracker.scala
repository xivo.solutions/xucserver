package services.calltracking

import org.apache.pekko.actor.*
import com.google.inject.Inject
import helpers.FastLogging
import org.asteriskjava.manager.action.{
  CoreShowChannelsAction,
  GetVarAction,
  PauseMixMonitorAction
}
import org.asteriskjava.manager.event.*
import org.asteriskjava.manager.response.{GetVarResponse, ManagerResponse}
import services.ServiceStatus.*
import services.XucAmiBus
import services.XucAmiBus.*
import services.calltracking.AsteriskGraphTracker.LoopDetected
import xivo.xuc.ChannelTrackerConfig
import xivo.xucami.models.*

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.*
import helpers.JmxLongMetric
import xivo.xucami.userevents.UserEventRecordOnDemand

import scala.util.Try

object ChannelTracker {
  final val serviceName = "ChannelTracker"
  case object FlushPendingNotification
  case class GetChannel(name: String)
  case class NoSuchChannel(name: String)
  case object RetryGetCurrentChannels
}

class ChannelTracker @Inject() (
    xucAmiBus: XucAmiBus,
    config: ChannelTrackerConfig
) extends Actor
    with FastLogging
    with BaseTracker[Channel] {

  import ChannelTracker._

  var readyRequester: List[ActorRef]    = List.empty[ActorRef]
  var channels: Map[String, Channel]    = Map.empty[String, Channel]
  var pendingNotifications: Set[String] = Set.empty
  var tickSchedule: Option[Cancellable] = None

  val jmxNewChannelCount: Try[JmxLongMetric] = jmxBean.addLong(
    "NewChannelEvents",
    0L,
    Some("Number of NewChannelEvent received")
  )
  val jmxHangupChannelCount: Try[JmxLongMetric] =
    jmxBean.addLong("HangupEvents", 0L, Some("Number of HangupEvent received"))

  def scheduler: Scheduler = context.system.scheduler

  private def sendCoreShowChannels = {
    xucAmiBus.publish(AmiAction(new CoreShowChannelsAction(), None, Some(self)))
    scheduler.scheduleOnce(
      config.retryGetCurrentChannelsDelay.seconds,
      self,
      RetryGetCurrentChannels
    )

  }

  override def preStart(): Unit = {
    log.info(s"$self starting")
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
    xucAmiBus.subscribe(self, AmiType.AmiEvent)
    xucAmiBus.subscribe(self, AmiType.QueueEvent)
    xucAmiBus.subscribe(self, AmiType.AmiResponse)
    xucAmiBus.subscribe(self, AmiType.AmiService)
    sendCoreShowChannels
    context.become(handleWatchers orElse receive)
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
    tickSchedule.foreach(_.cancel())
    xucAmiBus.unsubscribe(self)
  }

  override def receive: PartialFunction[Any, Unit] = {
    case GetServiceStatus =>
      sender() ! ServiceStarting(serviceName, self)

    case NotifyWhenServiceReady =>
      readyRequester = sender() :: readyRequester

    case RetryGetCurrentChannels =>
      log.warning("Unable to get current channels, retrying core show channels")
      sendCoreShowChannels

    case amievt: AmiEvent =>
      amievt.message match {
        case evt: CoreShowChannelEvent =>
          log.debug("CoreShowChannelEvent {}", evt)
          val c = Channel(evt, amievt.sourceMds)
          xucAmiBus.publish(
            AmiAction(new GetVarAction(c.name, "DIALSTATUS"), Some(c.id))
          )
          channels += (evt.getChannel -> c)
          immediateNotify(c)

        case c: CoreShowChannelsCompleteEvent =>
          readyRequester.foreach(_ ! ServiceReady(serviceName, self))
          readyRequester = List.empty
          log.debug("Service is ready")
          val tickDelay = config.channelTrackerThrottleTimeoutMs.millis
          tickSchedule = Some(
            scheduler.scheduleAtFixedRate(
              tickDelay,
              tickDelay,
              self,
              FlushPendingNotification
            )
          )
          context.become(handleWatchers orElse ready)
        case _ =>
          log.warning("Ignoring initial event {}", amievt)
      }
  }

  def ready: Receive = {
    case GetServiceStatus | NotifyWhenServiceReady =>
      sender() ! ServiceReady(serviceName, self)

    case AmiFailure(message, mdsName) =>
      log.warning("AmiFailure detected ({}), clear all channels", message)
      val toRemove = channels.values
        .filter(_.mdsName == mdsName)
        .map(c => {
          updateChannel(c.name, _.withChannelState(ChannelState.HUNGUP), true)
          c.name
        })

      channels = channels -- toRemove

    case AmiEvent(evt: NewChannelEvent, mdsName) =>
      log.debug("Received {}", evt)
      jmxNewChannelCount.inc()
      val c = Channel(evt, mdsName)
      channels += (evt.getChannel -> c)
      immediateNotify(c)

    case AmiEvent(evt: NewStateEvent, _) =>
      log.debug("Received {}", evt)
      updateChannel(evt.getChannel, _.withState(evt), true)

    case AmiEvent(evt: AbstractHoldEvent, _) =>
      log.debug("Received {}", evt)
      val state = if (evt.isHold) {
        ChannelState.HOLD
      } else {
        ChannelState.UP
      }

      updateChannel(evt.getChannel, _.withChannelState(state))

    case AmiEvent(evt: HangupEvent, _) =>
      jmxHangupChannelCount.inc()
      updateChannel(
        evt.getChannel,
        _.withChannelState(ChannelState.HUNGUP),
        true
      )
      channels -= evt.getChannel

    case AmiEvent(evt: VarSetEvent, _) =>
      updateChannel(
        evt.getChannel,
        c => {
          val c2 = c.updateVariable(evt.getVariable, evt.getValue)
          val isMonitorPaused = Option(evt.getVariable)
            .map(_.dropWhile(_ == '_'))
            .contains("MONITOR_PAUSED")
          if (c2.monitored == MonitorState.ACTIVE && isMonitorPaused)
            c2.copy(monitored = MonitorState.PAUSED)
          else
            c2
        }
      )

    case AmiEvent(evt: NewCallerIdEvent, _) =>
      updateChannel(
        evt.getChannel,
        _.withCallerId(CallerId(evt.getCallerIdName, evt.getCallerIdNum))
      )

    case AmiEvent(evt: NewConnectedLineEvent, _) =>
      updateChannel(
        evt.getChannel,
        _.withConnectedLine(evt.getConnectedLineNum, evt.getConnectedLineName)
      )

    case AmiEvent(evt: MixMonitorStartEvent, _) =>
      log.debug(s"Received $evt")
      updateChannel(evt.getChannel, _.copy(monitored = MonitorState.ACTIVE))

    case AmiEvent(evt: MixMonitorStopEvent, _) =>
      log.debug(s"Received $evt")
      updateChannel(evt.getChannel, _.copy(monitored = MonitorState.DISABLED))

    case AmiEvent(evt: UserEventRecordOnDemand, _) =>
      log.debug(s"Received $evt")
      // TODO: the copy shall be done with MonitorState.PAUSED, but then the AmiEvent(_:VarSetEvent,_) listener shall also be refactored
      updateChannel(evt.getChannel, _.copy(monitored = MonitorState.ACTIVE))

    case AmiResponse(
          (
            evt: ManagerResponse,
            Some(AmiAction(action: PauseMixMonitorAction, _, _, _))
          )
        ) =>
      if (Option(evt.getResponse).contains("Success")) {
        updateChannel(
          action.getChannel,
          _.copy(monitored =
            if (action.getState == 0) MonitorState.ACTIVE
            else MonitorState.PAUSED
          )
        )
      }

    case AmiResponse(
          (evt: GetVarResponse, Some(AmiAction(action: GetVarAction, _, _, _)))
        ) =>
      if (evt.getResponse == "Success") {
        log.debug("Received {}", evt)
        evt.getVariable match {
          case "DIALSTATUS" =>
            val direction =
              channels
                .get(action.getChannel)
                .flatMap(_.getDirection(evt.getValue))
            updateChannel(action.getChannel, _.copy(direction = direction))
          case "CHANNEL(pjsip,call-id)" =>
            log.debug(
              s"Got SIP CallId ${evt.getValue} for channel ${action.getChannel}"
            )
            updateChannel(
              action.getChannel,
              _.updateVariable("SIPCALLID", evt.getValue)
            )
          case _ =>
        }
      }

    case LoopDetected(nodes) =>
      log.warning("Received a loop notification, hanging up channels")
      nodes
        .filter(n => channels.contains(n.name))
        .foreach(n => {
          log.warning(s"- hanging up ${n.name}")
          updateChannel(n.name, _.withChannelState(ChannelState.HUNGUP), true)
          channels -= n.name
        })

    case evt: QueueHistoryEvent =>
      log.debug("Received {}", evt)
      updateChannel(evt.channel, _.withQueueHistory(evt))

    case AmiEvent(evt: PickupEvent, _) =>
      updateChannel(evt.getChannel, _.withInterception)

    case FlushPendingNotification =>
      for {
        channelName <- pendingNotifications
        channel     <- channels.get(channelName)
      } immediateNotify(channel)

      pendingNotifications = Set.empty

    case GetChannel(name) =>
      channels.get(name) match {
        case Some(c) => sender() ! c
        case None    => sender() ! NoSuchChannel(name)
      }
  }

  def updateChannel(
      name: String,
      f: Channel => Channel,
      immediateNotification: Boolean = false
  ): Unit = {
    Option(name)
      .flatMap(channels.get(_))
      .map(f)
      .foreach(c => {
        channels = channels.updated(c.name, c)
        if (immediateNotification || !config.enableChannelTrackerThrottling) {
          immediateNotify(c)
        } else {
          pendingNotifications = pendingNotifications + c.name
        }
      })
  }

  def immediateNotify(c: Channel): Unit = {
    if (pendingNotifications.contains(c.name)) {
      pendingNotifications = pendingNotifications - c.name
    }
    notify(c.name, c)
  }

  override def onNewWatcher(channelName: String, actor: ActorRef): Unit = {
    channels.view
      .filterKeys(_.startsWith(channelName))
      .toMap
      .values
      .foreach(actor ! _)
  }
}
