package services.calltracking

import cats.effect.IO
import org.apache.pekko.actor.*
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.Publish
import services.{ActorIds, XucAmiBus}
import services.calltracking.ConferenceTracker.*
import services.calltracking.DeviceMessage.*
import services.calltracking.SingleDeviceTracker.PartyInformation
import system.ClusterAsync
import xivo.xuc.DeviceTrackerConfig

class UnknownDeviceTracker(
    interface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig,
    mediator: ActorRef
) extends SingleDeviceTracker(
      interface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig
    ) {

  override def customBehavior: Receive =
    redirectConferenceEvent orElse super.customBehavior

  private def forwardToDevicesTracker(msg: DeviceMessage): Unit =
    context.parent.forward(msg)
  private def forwardToMediator(msg: DeviceMessage): Unit =
    log.debug(s"[HA] Publishing $msg on topic $conferenceEventTopic")
    mediator ! Publish(conferenceEventTopic, msg)

  private def retrieveSipCallID(channel: String): Option[String] = {
    tracker.calls.get(channel).flatMap(_.variables.get("XIVO_SIPCALLID"))
  }

  private val routeDeviceJoinConfNoCallID: DeviceJoinConference => IO[Unit] = {
    event =>
      getRemoteChannelName(event.channel) match {
        case Some(chan) =>
          log.debug(
            s"${event.channel} - Found remote channel name, forward event to DevicesTracker"
          )
          forwardToDevicesTracker(
            event.copy(
              channel = chan,
              localChannel = Some(event.localChannel.getOrElse(event.channel))
            )
          )
          IO.unit
        case None =>
          retrieveSipCallID(event.channel) match
            case Some(sipCallId) =>
              log.debug(
                s"${event.channel} - Found sip call id $sipCallId, forward event to Mediator"
              )
              forwardToMediator(
                event.copy(
                  sipCallId = Some(sipCallId),
                  localChannel =
                    Some(event.localChannel.getOrElse(event.channel))
                )
              )
              IO.unit
            case None =>
              IO.raiseError(
                new Exception(s"Could not route ${event.getClass} event")
              )
      }
  }

  private val routeDeviceJoinConfWithCallID
      : ((DeviceJoinConference, String)) => IO[Unit] = { (event, sipCallId) =>
    tracker.calls.values
      .find(
        _.variables.exists(variable =>
          variable._1 == "XIVO_SIPCALLID" && variable._2 == sipCallId
        )
      )
      .flatMap(dc => getRemoteChannelName(dc.channelName)) match
      case Some(remoteChannelName) =>
        if (!remoteChannelName.contains("anonymous")) {
          forwardToDevicesTracker(event.copy(channel = remoteChannelName))
          IO.unit
        } else
          IO.raiseError(
            new Exception(s"Could not route ${event.getClass} event")
          )
      case None =>
        IO.raiseError(
          new Exception(s"Could not route ${event.getClass} event")
        )
  }

  private def redirectConferenceEvent: Receive = {
    case event @ DeviceJoinConference(_, channel, _, None, _) =>
      log.debug(s"$channel - Receive join event without sip call id")
      ClusterAsync.withRetries[Unit, DeviceJoinConference](
        process = routeDeviceJoinConfNoCallID,
        arg = event,
        onGiveUp = () =>
          log.debug(
            "Giving up on DeviceJoinConference without sipcallid distribution"
          )
      )

    case event @ DeviceJoinConference(_, _, _, Some(sipCallId), _) =>
      log.debug(s"Receive join event with sip call id")
      ClusterAsync.withRetries[Unit, (DeviceJoinConference, String)](
        process = routeDeviceJoinConfWithCallID,
        arg = (event, sipCallId),
        onGiveUp = () =>
          log.debug(
            "Giving up on DeviceJoinConference with sipcallid distribution"
          )
      )

    case event @ DeviceLeaveConference(_, _, _, Some(sipCallId)) =>
      log.debug(s"Receive leave event with sip call id")
      tracker.calls.values
        .find(
          _.variables.exists(variable =>
            variable._1 == "XIVO_SIPCALLID" && variable._2 == sipCallId
          )
        )
        .flatMap(dc => getRemoteChannelName(dc.channelName))
        .filter(c => !c.contains("anonymous"))
        .map(c => event.copy(channel = c))
        .foreach(forwardToDevicesTracker)

    case event @ DeviceLeaveConference(_, channel, _, _) =>
      log.debug(s"Receive join event without sip call id")
      getRemoteChannelName(channel) match {
        case Some(remoteChannel) =>
          log.debug(
            s"Forward event to DevicesTracker for channel $remoteChannel"
          )
          forwardToDevicesTracker(event.copy(channel = remoteChannel))
        case None =>
          retrieveSipCallID(channel).map(sipCallId => {
            log.debug(s"Forward event to Mediator with sip call id $sipCallId")
            forwardToMediator(
              event.copy(sipCallId = Some(sipCallId))
            )
          })
      }
  }

  private def getRemoteChannelName(channel: String): Option[String] =
    tracker.calls
      .get(channel)
      .map(call => AsteriskGraphTracker.pathsEndChannels(call.paths))
      .flatMap(_.headOption)
      .map(_.name)

  val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.UnknownDeviceTrackerType
  def notify(call: DeviceCall, redoLast: Boolean = false): Unit = {}
  def onPartyInformation(evt: PartyInformation): Unit           = {}
}
