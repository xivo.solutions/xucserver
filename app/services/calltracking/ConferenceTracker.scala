package services.calltracking

import com.google.inject.Inject
import com.google.inject.name.Named
import helpers.{FastLogging, JmxActorSingletonMonitor, JmxLongMetric}
import org.apache.pekko.actor.*
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.{
  Publish,
  Subscribe
}
import org.asteriskjava.manager.action.MuteAudioAction
import org.asteriskjava.manager.event.{
  MeetMeJoinEvent,
  MeetMeLeaveEvent,
  MeetMeMuteEvent,
  MeetMeTalkingEvent
}
import org.asteriskjava.manager.response.ManagerResponse
import services.XucAmiBus.*
import services.calltracking.BaseTracker.*
import services.calltracking.ConferenceTracker.*
import services.calltracking.DeviceMessage.DeviceInterface
import services.request.PhoneRequest.IncludeToConference
import services.{ActorIds, XucAmiBus}
import xivo.models.{ConferenceFactory, MeetMeConference}
import xivo.xuc.XucConfig
import xivo.xucami.models.Channel

import scala.concurrent.duration.*
import scala.language.postfixOps
import scala.util.Try

object ConferenceTracker {

  def serviceName: String = "ConferenceTracker"

  final case class GetConference(numConf: String)
  case object RefreshConference
  final case class SubscribeToConference(numConf: String)
  final case class SubscribeToConferenceStatus(numConf: String)
  final case class UnsubscribeConference(numConf: String)
  final case class UnsubscribeConferenceStatus(numConf: String)
  final case class ConferenceSubscriptionAck(conference: ConferenceRoom)

  final val conferenceEventTopic = "ConferenceEventTopic"

  final def conferenceParticipantEventTopic(
      conferenceName: String,
      nodeHost: String
  ) = s"${nodeHost}:ConferenceParticipantEventTopic:$conferenceName"

  final def getConfNameFromTopic(topic: String): Option[String] = {
    topic
      .split(":ConferenceParticipantEventTopic:")
      .lastOption
  }
  final def ConfParticipantResultTopic(
      nodeHost: String,
      interface: DeviceInterface
  ) = s"${nodeHost}:ConfParticipantResultTopic:$interface"

  final def conferenceActionTopic(nodeHost: String) =
    s"ConferenceActionTopic:${nodeHost}"

  case class ConfParticipantResult(
      numConf: String,
      participants: List[ConferenceParticipant]
  )
  case class GetConferenceParticipants(numConf: String, responseTopic: String)

  sealed trait ConferenceParticipantChange {
    def numConf: String
    def participant: ConferenceParticipant
  }

  final case class ParticipantJoinConference(
      numConf: String,
      participant: ConferenceParticipant
  ) extends ConferenceParticipantChange

  final case class ParticipantLeaveConference(
      numConf: String,
      participant: ConferenceParticipant
  ) extends ConferenceParticipantChange

  final case class ParticipantUpdated(
      numConf: String,
      participant: ConferenceParticipant
  ) extends ConferenceParticipantChange

  final case class ConferenceStatusChange(
      numConf: String,
      status: ConferenceStatus
  )

  final case class NoConferenceRoom(numConf: String = "")

  private final case class RemoveConferenceRoom(numConf: String)

  final case class StopConference(numConf: String)

  sealed trait DeviceConference {
    val topic: String
    val sipCallId: Option[String]
    def channel: String
    def conference: ConferenceRoom
    def isFor(interface: DeviceInterface): Boolean =
      channel.startsWith(interface)
  }
  final case class DeviceJoinConference(
      conference: ConferenceRoom,
      channel: String,
      topic: String,
      sipCallId: Option[String] = None,
      localChannel: Option[String] = None
  ) extends DeviceMessage
      with DeviceConference
  final case class DeviceLeaveConference(
      conference: ConferenceRoom,
      channel: String,
      topic: String,
      sipCallId: Option[String] = None
  ) extends DeviceMessage
      with DeviceConference
}

class ConferenceTracker @Inject() (
    amiBus: XucAmiBus,
    conferenceFactory: ConferenceFactory,
    @Named(ActorIds.ChannelTrackerId) channelTracker: ActorRef,
    configuration: XucConfig,
    @Named(ActorIds.MediatorWithLoggingId) mediator: ActorRef
) extends Actor
    with FastLogging
    with JmxActorSingletonMonitor {

  type ConferenceMap         = Map[String, ConferenceRoom]
  type ChannelParticipantMap = Map[String, ConferenceParticipant]
  private type SubscriberMap = Map[String, Set[ActorRef]]

  private val jmxConferenceCount: Try[JmxLongMetric] =
    jmxBean.addLong("Conferences", 0L, Some("Number of Conferences"))
  private val jmxParticipantCount: Try[JmxLongMetric] =
    jmxBean.addLong("Participants", 0L, Some("Number of Participants"))
  val nodeHost: String = configuration.getNodeHostAndPort

  def scheduler: Scheduler = context.system.scheduler

  def receive: Receive =
    processMessage(ConferenceRoomRepository.empty, Map.empty)

  def processMessage(
      conferences: ConferenceRoomRepository,
      statusSubscribers: SubscriberMap
  ): Receive = {

    case conf: ConferenceRoom =>
      log.debug("Loading Conference {}", conf)
      val newRepo = conferences.updateConference(conf)
      jmxConferenceCount.set(newRepo.conferences.size)
      context.become(
        processMessage(newRepo, statusSubscribers)
      )

    case GetConferenceParticipants(numConf, topic) =>
      conferences
        .getConference(numConf)
        .foreach(conf => {
          mediator ! Publish(
            topic,
            ConfParticipantResult(numConf, conf.participants)
          )
        })

    case RemoveConferenceRoom(numConf: String) =>
      log.debug("Remove Conference {}", numConf)
      val newRepo = conferences.removeConference(numConf)
      jmxConferenceCount.set(newRepo.conferences.size)
      context.become(
        processMessage(newRepo, statusSubscribers)
      )

    case GetConference(numConf) =>
      sender() ! conferences
        .getConference(numConf)
        .getOrElse(NoConferenceRoom(numConf))

    case AmiEvent(evt: MeetMeJoinEvent, mds) =>
      log.debug("AmiEvent({})", evt)
      val p       = ConferenceParticipant(evt)
      val newRepo = conferences.addParticipant(p, mds, nodeHost)
      val numConf = evt.getMeetMe
      jmxParticipantCount.set(newRepo.channelMap.size)
      channelTracker ! WatchChannelStartingWith(evt.getChannel)
      for {
        oldConf <-
          conferences
            .getConference(numConf)
            .orElse(Some(ConferenceRoom.empty(numConf, mds, nodeHost)))
        newConf <- newRepo.getConference(numConf)
        newPart <- newConf.getParticipant(p.index)
      } {
        notifyChange(
          ParticipantJoinConference(newConf.number, newPart)
        )
        notifyStatusChange(statusSubscribers, oldConf, newConf)
        val evt = DeviceJoinConference(
          newConf,
          newPart.channelName,
          ConferenceTracker.conferenceParticipantEventTopic(
            numConf,
            nodeHost
          )
        )
        log.debug(
          s"[HA] Publishing $evt on topic ${ConferenceTracker.conferenceEventTopic}"
        )
        mediator ! Publish(
          ConferenceTracker.conferenceEventTopic,
          evt
        )
      }

      context.become(
        processMessage(newRepo, statusSubscribers)
      )

    case AmiEvent(evt: MeetMeLeaveEvent, mds) =>
      log.debug("AmiEvent({})", evt)
      val p       = ConferenceParticipant(evt)
      val newRepo = conferences.removeParticipant(p, mds, nodeHost)
      val numConf = evt.getMeetMe
      jmxParticipantCount.set(newRepo.channelMap.size)

      for {
        oldConf <-
          conferences
            .getConference(numConf)
            .orElse(Some(ConferenceRoom.empty(numConf, mds, nodeHost)))
        newConf <- newRepo.getConference(numConf)
        oldPart <- oldConf.getParticipant(p.index)
      } {
        notifyChange(
          ParticipantLeaveConference(newConf.number, oldPart)
        )
        notifyStatusChange(statusSubscribers, oldConf, newConf)
        channelTracker ! UnWatchChannelStartingWith(oldPart.channelName)
        val evt = DeviceLeaveConference(
          newConf,
          oldPart.channelName,
          ConferenceTracker.conferenceParticipantEventTopic(
            newConf.number,
            nodeHost
          ),
          None
        )
        log.debug(
          s"[HA] Publishing $evt on topic ${ConferenceTracker.conferenceEventTopic}"
        )
        mediator ! Publish(
          ConferenceTracker.conferenceEventTopic,
          evt
        )
      }

      context.become(
        processMessage(newRepo, statusSubscribers)
      )

    case AmiEvent(evt: MeetMeTalkingEvent, _) =>
      updateParticipant(
        conferences,
        statusSubscribers,
        evt.getMeetMe,
        evt.getUser
      )(_.copy(isTalking = evt.getStatus))

    case AmiEvent(evt: MeetMeMuteEvent, _) =>
      updateParticipant(
        conferences,
        statusSubscribers,
        evt.getMeetMe,
        evt.getUser
      )(_.copy(isMuted = evt.getStatus))

    case AmiResponse(
          (
            evt: ManagerResponse,
            Some(AmiAction(action: MuteAudioAction, _, _, _))
          )
        ) =>
      if (Option(evt.getResponse).contains("Success")) {
        def setDeafenState() = {
          action.getState match {
            case MuteAudioAction.State.MUTE   => true
            case MuteAudioAction.State.UNMUTE => false
          }
        }
        for {
          p <- conferences.getParticipantByChannel(action.getChannel)
        } updateParticipant(
          conferences,
          statusSubscribers,
          p.numConf,
          p.index
        )(_.copy(isDeaf = setDeafenState()))
      }

    case c: Channel =>
      val roleOpt = c.variables
        .get(Channel.VarNames.ConferenceRoleVariable)
        .flatMap(ConferenceParticipantRole.fromString)

      for {
        role <- roleOpt
        p    <- conferences.getParticipantByChannel(c.name)
      } updateParticipant(
        conferences,
        statusSubscribers,
        p.numConf,
        p.index
      )(_.copy(role = role))

    case SubscribeToConferenceStatus(numConf) =>
      val list                 = statusSubscribers.getOrElse(numConf, Set.empty) + sender()
      val newStatusSubscribers = statusSubscribers + (numConf -> list)
      context.become(
        processMessage(conferences, newStatusSubscribers)
      )

    case UnsubscribeConferenceStatus(numConf) =>
      statusSubscribers
        .get(numConf)
        .map(_.filterNot(_.equals(sender())))
        .map(statusSubscribers.updated(numConf, _))
        .foreach(s => context.become(processMessage(conferences, s)))

    case RefreshConference =>
      import context.dispatcher

      for {
        dbConferences <- conferenceFactory.all()
        confNumberToDelete = conferences.numbers.filterNot(confNumber =>
          dbConferences.map(_.confno).contains(confNumber)
        )
        confNumberToCreateOrUpdate = dbConferences.filterNot(conf =>
          conferences.conferences.values.toList
            .exists(_.equalsMeetMeConference(conf))
        )
      } {
        confNumberToDelete.foreach(numConf =>
          self ! RemoveConferenceRoom(numConf)
        )
        confNumberToCreateOrUpdate.foreach(conf =>
          self ! ConferenceRoom(conf, nodeHost)
        )
      }

    case AmiRequest(msg, mds) =>
      log.info(s"Received amiEvent $msg for mediaserver $mds")
      amiBus.publish(
        AmiAction(
          msg.buildAction(),
          targetMds = mds
        )
      )
  }

  override def preStart(): Unit = {
    import context.dispatcher
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
    amiBus.subscribe(self, AmiType.AmiEvent)
    amiBus.subscribe(self, AmiType.AmiResponse)
    mediator ! Subscribe(ConferenceTracker.conferenceEventTopic, self)
    val topic = ConferenceTracker.conferenceActionTopic(nodeHost)
    log.debug(
      s"[HA] Subscribing to topic $topic"
    )
    mediator ! Subscribe(
      topic,
      self
    )
    for {
      list       <- conferenceFactory.all()
      meetmeconf <- list
    } {
      self ! ConferenceRoom(meetmeconf, nodeHost)
    }

    scheduler.scheduleWithFixedDelay(
      configuration.conferenceRefreshTimerSeconds second,
      configuration.conferenceRefreshTimerSeconds second,
      self,
      RefreshConference
    )
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
  }

  private def notifyStatusChange(
      subscribers: SubscriberMap,
      oldConf: ConferenceRoom,
      newConf: ConferenceRoom
  ): Unit = {
    if (newConf.status != oldConf.status) {
      subscribers
        .get(newConf.number)
        .foreach(
          _.foreach(_ ! ConferenceStatusChange(newConf.number, newConf.status))
        )
    }
  }

  private def notifyChange(
      conferenceParticipantChange: ConferenceParticipantChange
  ): Unit = {
    val topic = ConferenceTracker.conferenceParticipantEventTopic(
      conferenceParticipantChange.numConf,
      nodeHost
    )
    log.debug(
      s"[HA] Publishing $conferenceParticipantChange on topic $topic"
    )
    mediator ! Publish(topic, conferenceParticipantChange)
  }

  def updateParticipant(
      conferences: ConferenceRoomRepository,
      statusSubscribers: SubscriberMap,
      numConf: String,
      index: Int
  )(fn: ConferenceParticipant => ConferenceParticipant): Unit = {
    val newRepo = conferences.updateParticipant(numConf, index)(fn)

    for {
      oldP <- conferences.getParticipant(numConf, index)
      newP <- newRepo.getParticipant(numConf, index) if oldP != newP
    } notifyChange(ParticipantUpdated(numConf, newP))

    context.become(
      processMessage(newRepo, statusSubscribers)
    )
  }
}
