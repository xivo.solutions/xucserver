package services.calltracking

import org.apache.pekko.actor.*
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.{
  Publish,
  Subscribe,
  SubscribeAck,
  Unsubscribe
}

import services.XucAmiBus.AmiRequest
import services.calltracking.ConferenceTracker.*
import services.calltracking.DeviceConferenceAction.*
import services.calltracking.DeviceMessage.*
import services.calltracking.SingleDeviceTracker.{
  IncludeToConfGetCalls,
  PartyInformation
}
import services.calltracking.graph.{NodeChannelLike, NodeLocalChannel}
import services.line.PhoneController.{
  IncludeToConfResponse,
  IncludeToConfWrongPreconditions
}
import services.{XucAmiBus, XucEventBus}
import xivo.events.{CurrentCallsPhoneEvents, PhoneEvent}
import xivo.models.Line
import xivo.websocket.*
import xivo.xuc.{DeviceTrackerConfig, XucConfig}
import xivo.xucami.models.Channel

object SipDeviceTracker {
  case class WatchOutboundCallTo(interface: DeviceInterface, number: String)
      extends SingleDeviceTracker.DeviceMessageWithInterface
  case class UnWatchOutboundCallTo(interface: DeviceInterface, number: String)
      extends SingleDeviceTracker.DeviceMessageWithInterface
}

object SipDriver extends Enumeration {
  type SipDriver = Value
  val SIP, PJSIP                       = Value
  def toEnum(d: String): Option[Value] = values.find(_.toString == d)
}

class SipDeviceTracker(
    line: Line,
    eventBus: XucEventBus,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig,
    configDispatcher: ActorRef,
    mediator: ActorRef,
    configuration: XucConfig
) extends SingleDeviceTracker(
      line.trackingInterface,
      channelTracker,
      graphTracker,
      xucAmiBus,
      deviceTrackerConfig
    ) {

  import BaseTracker.*
  import SingleDeviceTracker.{
    DeviceConferenceMessage,
    SendCurrentCallsPhoneEvents
  }

  val deviceTrackerType: SingleDeviceTracker.DeviceTrackerType =
    SingleDeviceTracker.SipDeviceTrackerType
  var lastPhoneEvent: Option[PhoneEvent] = None
  var mobileIface: Option[String]        = None

  override def customBehavior: Receive =
    sipDeviceBehavior orElse super.customBehavior

  override def postStop(): Unit = {
    clearMobileRegistration()
    super.postStop()
  }

  override def preStart(): Unit = {
    super.preStart()
    mediator ! Subscribe(
      ConferenceTracker
        .ConfParticipantResultTopic(
          configuration.getNodeHostAndPort,
          line.interface
        ),
      self
    )
  }
  def sipDeviceBehavior: Receive = {
    case SendCurrentCallsPhoneEvents(_) =>
      sendCurrentCallsPhoneEvents(getCalls().toList)

    case evt: DeviceConference =>
      evt match {
        case joinEvent: DeviceJoinConference =>
          log.debug(s"[HA] Subscribing to topic ${joinEvent.topic}")
          mediator ! Subscribe(
            joinEvent.topic,
            self
          )
          tracker = tracker.withConferenceEvent(evt)
          sendConferenceEvents(evt.conference, evt.topic)
        case leaveEvent: DeviceLeaveConference =>
          onDeviceConferenceEvent(evt)
          log.debug(s"[HA] Unsubscribing to topic ${leaveEvent.topic}")
          mediator ! Unsubscribe(
            leaveEvent.topic,
            self
          )
          tracker = tracker.withConferenceEvent(evt)
      }

    case SubscribeAck(Subscribe(topic: String, _, _)) =>
      ConferenceTracker.getConfNameFromTopic(topic) match {
        case Some(confName) =>
          val getConferenceParticipant = GetConferenceParticipants(
            confName: String,
            ConferenceTracker.ConfParticipantResultTopic(
              configuration.getNodeHostAndPort,
              line.interface
            )
          )
          mediator ! Publish(
            ConferenceTracker.conferenceEventTopic,
            getConferenceParticipant
          )
        case None =>
          log.warning(s"Unable to retrieve conference name from topic $topic")
      }

    case evt: ConferenceParticipantChange =>
      if (evt.isInstanceOf[ParticipantLeaveConference] && isMe(evt)) {
        tracker.conferences.get(evt.numConf) match
          case None => log.info(s"No conference ${evt.numConf} found")
          case Some(conference) =>
            self forward DeviceLeaveConference(
              conference.conference,
              evt.participant.channelName,
              conference.topic,
              None
            )
            toAmiRequests(
              sender(),
              Reset(evt.numConf),
              conference.conference.mds
            )
              .foreach(r =>
                val topic = ConferenceTracker.conferenceActionTopic(
                  conference.conference.nodeHost
                )
                log.debug(
                  s"[HA] Publishing AmiRequest $r on topic $topic"
                )
                mediator ! Publish(topic, r)
              )
      }
      tracker = tracker.withConferenceParticipantEvent(evt)
      onConferenceParticipantChange(evt)

    case IncludeToConfGetCalls(_, replyTo, evt, ctiRouterRef) =>
      val callIds       = tracker.calls.keys.toList
      val conferenceIds = tracker.conferences.keys.toList
      if (tracker.conferences.size != 1 || tracker.calls.size != 2) {
        replyTo ! IncludeToConfWrongPreconditions(callIds, conferenceIds)
      } else {
        val confWithChannels = tracker.conferences.values.head
        val confChannels     = confWithChannels.channelNames.flatten
        val maybeNotConfCalls =
          tracker.calls
            .filterNot((callChannel, deviceCall) =>
              confChannels.contains(callChannel)
            )
        val maybeConfCalls = tracker.calls
          .filter((callChannel, deviceCall) =>
            confChannels.contains(callChannel)
          )
          .values

        if (maybeNotConfCalls.size != 1 || maybeConfCalls.size != 1) {
          replyTo ! IncludeToConfWrongPreconditions(callIds, conferenceIds)
        } else {
          val (confCalls, notConfCalls) =
            (maybeConfCalls.head, maybeNotConfCalls.head)
          val (remoteChanName, remoteChanSourceMds) = AsteriskGraphTracker
            .pathsEndpoints(notConfCalls._2.paths)
            .headOption
            .map(rc => (rc.name, rc.sourceMds))
            .getOrElse(
              (
                notConfCalls._1,
                notConfCalls._2.channel
                  .map(_.mdsName)
                  .getOrElse("default")
              )
            )

          replyTo ! IncludeToConfResponse(
            confWithChannels.conference,
            remoteChanName,
            remoteChanSourceMds,
            confCalls,
            evt,
            ctiRouterRef
          )
        }
      }

    case DeviceConferenceMessage(_, command) =>
      log.info(s"Received conference command $command")
      tracker.conferences.get(command.numConf) match
        case Some(conference) =>
          toAmiRequests(sender(), command, conference.conference.mds).foreach(
            r =>
              val topic = ConferenceTracker.conferenceActionTopic(
                conference.conference.nodeHost
              )
              log.debug(
                s"[HA] Publishing AmiRequest on topic $topic"
              )
              mediator ! Publish(topic, r)
          )
        case None =>
          log.info(s"No conference ${command.numConf} found")

    case SipDeviceTracker.WatchOutboundCallTo(_, mobile) =>
      val iface = s"Local/$mobile@"
      if (!mobileIface.contains(iface)) {
        clearMobileRegistration()
        channelTracker ! WatchChannelStartingWith(iface)
        graphTracker ! WatchChannelStartingWith(iface)
        context.parent ! DevicesTracker.RegisterActor(iface, self)
        mobileIface = Some(iface)
      }

    case ConfParticipantResult(numConf, participants) =>
      tracker.conferences
        .get(numConf)
        .map(conf => {
          participants.foreach(participant =>
            self ! ParticipantUpdated(conf.conference.number, participant)
          )
        })

    case SipDeviceTracker.UnWatchOutboundCallTo(_, mobile) =>
      clearMobileRegistration()
  }

  private def sendConferenceEvents(
      conf: ConferenceRoom,
      topic: String
  ): Unit = {
    tracker = tracker.updateConference(conf)
    val joinEvents = tracker.conferences
      .get(conf.number)
      .map(c =>
        c.channelNames.flatten.map(ch =>
          DeviceJoinConference(c.conference, ch, topic)
        )
      )
      .getOrElse(List.empty)

    joinEvents.foreach(e => onDeviceConferenceEvent(e))
  }

  def clearMobileRegistration(): Unit = {
    mobileIface.foreach(iface => {
      channelTracker ! UnWatchChannelStartingWith(iface)
      graphTracker ! UnWatchChannelStartingWith(iface)
      context.parent ! DevicesTracker.UnRegisterActor(iface, self)

      tracker.calls.keys
        .filter(_.startsWith(iface))
        .foreach(self ! RemoveChannel(_))
    })
    mobileIface = None
  }

  def notify(call: DeviceCall, redoLast: Boolean = false): Unit = {
    DeviceCallToPhoneEvent(call, line)
      .filterNot(event =>
        if (redoLast) { false }
        else { lastPhoneEvent.contains(event) }
      )
      .foreach(e => {
        lastPhoneEvent = Some(e)
        configDispatcher ! e
      })
  }

  def sendCurrentCallsPhoneEvents(calls: List[DeviceCall]): Unit = {
    configDispatcher ! CurrentCallsPhoneEvents(
      line.number.get,
      calls.flatMap(DeviceCallToPhoneEvent(_, line))
    )
    for {
      c <- tracker.conferences.values
      conference = c.conference
      channels   = c.channelNames
      channelNameSet <- channels
      channelName    <- channelNameSet
      call           <- findCallByChannel(channelName)
      channel        <- call.channel
      number         <- line.number
    } {
      val me       = c.findMe().map(_.index)
      val joinConf = DeviceJoinConference(conference, channelName, c.topic)
      val wsmsg    = WsConferenceEvent.from(joinConf, channel.id, number, me)
      log.debug(
        s"publishing WsConferenceEvent for current calls from channel ${channel.id}"
      )
      configDispatcher ! wsmsg
    }
  }

  def onDeviceConferenceEvent(
      evt: DeviceConference
  ): Unit = {
    val me = tracker.conferences
      .get(evt.conference.number)
      .map(_.findMe().map(_.index))
      .getOrElse(List.empty)

    for {
      call    <- findCallByChannel(evt.channel)
      channel <- call.channel
      number  <- line.number
    } {
      val wsmsg = WsConferenceEvent.from(evt, channel.id, number, me)
      log.debug(
        s"publishing WsConferenceEvent for device conference from channel ${channel.id}"
      )
      configDispatcher ! wsmsg
    }
  }

  def onConferenceParticipantChange(evt: ConferenceParticipantChange): Unit = {
    for {
      c <- tracker.conferences.get(evt.numConf)
      conf     = c.conference
      channels = c.channelNames
      channelNameSet <- channels
      channelName    <- channelNameSet
      call           <- findCallByChannel(channelName)
      channel        <- call.channel
      number         <- line.number
    } {
      val me    = c.findMe().map(_.index)
      val wsmsg = WsConferenceParticipantEvent.from(evt, channel.id, number, me)
      log.debug(
        s"publishing WsConferenceParticipantEvent for conference participant change from channel ${channel.id}"
      )
      configDispatcher ! wsmsg
    }
  }

  def onPartyInformation(evt: PartyInformation): Unit = {}

  def findCallByChannel(channel: String): Option[DeviceCall] =
    tracker.calls.get(channel)

  def isMe(evt: ConferenceParticipantChange): Boolean = {
    tracker.conferences
      .get(evt.numConf)
      .exists(_.channelNames.flatten.contains(evt.participant.channelName))
  }

  def toAmiRequests(
      sender: ActorRef,
      command: DeviceConferenceCommand,
      mds: String
  ): List[XucAmiBus.AmiRequest] =
    DeviceConferenceAction.toAmi(
      tracker.conferences,
      command,
      line.context
    ) match
      case Left(value) =>
        sender ! value
        List.empty
      case Right(value) =>
        value.map(AmiRequest(_, Some(mds)))

  override def filterChannel(c: Channel): Boolean =
    !(c.isLocal && c.name.endsWith(";2"))
  override def filterNodeChannel(c: NodeChannelLike): Boolean =
    c match {
      case NodeLocalChannel(name, _) if c.name.endsWith(";2") => false
      case _                                                  => true
    }
}
