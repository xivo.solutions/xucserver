package services.calltracking

import org.asteriskjava.manager.event._
import services.calltracking.graph.GraphBuilder._
import services.calltracking.graph.NodeBridge.BridgeCreator
import services.calltracking.graph._

object AmiEventToAsteriskEdge {

  def impactedChannels(
      graph: AsteriskGraph,
      o: AsteriskObject
  ): Set[NodeChannelLike] = {
    val set = o match {
      case c: NodeChannelLike => Set[NodeChannelLike](c)
      case _                  => Set.empty[NodeChannelLike]
    }
    set ++ graph.remoteChannels(o)
  }

  def apply(
      graph: AsteriskGraph,
      event: ManagerEvent,
      sourceMds: String
  ): (AsteriskGraph, Set[NodeChannelLike]) =
    applyToOption(graph, event, sourceMds).getOrElse((graph, Set.empty))

  def applyToOption(
      graph: AsteriskGraph,
      event: ManagerEvent,
      sourceMds: String
  ): Option[(AsteriskGraph, Set[NodeChannelLike])] =
    event match {

      case evt: HangupEvent =>
        NodeChannel
          .from(evt.getChannel, sourceMds)
          .map(c => {
            val impacted = impactedChannels(graph, c)
            (graph.del(c), impacted)
          })

      case evt: DialBeginEvent =>
        for {
          c1 <- NodeChannel.from(evt.getChannel, sourceMds)
          c2 <- NodeChannel.from(evt.getDestination, sourceMds)
        } yield {
          val g        = AsteriskGraph.add(graph, c1 ~ NodeDial(c1, c2) ~ c2)
          val impacted = impactedChannels(g, c1) ++ impactedChannels(g, c2)

          (g, impacted)
        }

      case evt: DialEndEvent =>
        for {
          c1 <- NodeChannel.from(evt.getChannel, sourceMds)
          c2 <- NodeChannel.from(evt.getDestination, sourceMds)
        } yield {
          val impacted =
            impactedChannels(graph, c1) ++ impactedChannels(graph, c2)

          (graph.del(NodeDial(c1, c2)), impacted)
        }

      case evt: LocalBridgeEvent =>
        val lc1      = NodeLocalChannel(evt.getLocalOneChannel, sourceMds)
        val lc2      = NodeLocalChannel(evt.getLocalTwoChannel, sourceMds)
        val g        = AsteriskGraph.add(graph, lc1 ~ NodeLocalBridge(lc1, lc2) ~ lc2)
        val impacted = g.remoteChannels(lc1) ++ g.remoteChannels(lc2)
        Some((g, impacted))

      case evt: BridgeEnterEvent =>
        NodeChannel
          .from(evt.getChannel, sourceMds)
          .map(c => {
            val bridgeCreator =
              Option(evt.getBridgeCreator).map(new BridgeCreator(_))
            val g = AsteriskGraph.add(
              graph,
              c ~ NodeBridge(evt.getBridgeUniqueId, sourceMds, bridgeCreator)
            )
            (g, impactedChannels(g, c))
          })

      case evt: BridgeLeaveEvent =>
        NodeChannel
          .from(evt.getChannel, sourceMds)
          .map(c => {
            val impacted = impactedChannels(graph, c)
            val bridgeCreator =
              Option(evt.getBridgeCreator).map(new BridgeCreator(_))
            val g = AsteriskGraph.del(
              graph,
              c ~ NodeBridge(evt.getBridgeUniqueId, sourceMds, bridgeCreator)
            )
            (g, impacted)
          })

      case evt: LocalOptimizationBeginEvent =>
        for {
          c1 <- NodeChannel.from(evt.getLocalOneChannel, sourceMds)
          c2 <- NodeChannel.from(evt.getLocalTwoChannel, sourceMds)
        } yield {
          val g        = AsteriskGraph.add(graph, c1 ~ NodeOptimize(c1, c2) ~ c2)
          val impacted = impactedChannels(g, c1) ++ impactedChannels(g, c2)
          (g, impacted)
        }

      case evt: LocalOptimizationEndEvent =>
        for {
          c1 <- NodeChannel.from(evt.getLocalOneChannel, sourceMds)
          c2 <- NodeChannel.from(evt.getLocalTwoChannel, sourceMds)
        } yield {
          val impacted =
            impactedChannels(graph, c1) ++ impactedChannels(graph, c2)

          (graph.del(NodeOptimize(c1, c2)), impacted)
        }

      case evt: CoreShowChannelEvent =>
        for {
          bridgeId <- Option(evt.getBridgeid) if !bridgeId.isEmpty
          channel  <- NodeChannel.from(evt.getChannel, sourceMds)
        } yield {
          val g2 = AsteriskGraph.add(
            graph,
            channel ~ NodeBridge(
              evt.getBridgeid,
              sourceMds,
              Some(new BridgeCreator("CoreShowChannels"))
            )
          )
          channel match {
            case NodeLocalChannel(name, mdsName) =>
              // Assume a local bridge exists if a local channel exists
              val baseName = name.dropRight(1)
              val lb = NodeLocalBridge(
                NodeLocalChannel(baseName + "1", sourceMds),
                NodeLocalChannel(baseName + "2", sourceMds)
              )
              (AsteriskGraph.add(g2, channel ~ lb), Set.empty[NodeChannelLike])
            case _ => (g2, Set.empty[NodeChannelLike])
          }
        }

      case evt: VarSetEvent =>
        for {
          varName <- Option(evt.getVariable)
          if varName == "SIPCALLID" || varName == "XIVO_SIPCALLID"
          callId  <- Option(evt.getValue)
          channel <- NodeChannel.from(evt.getChannel, sourceMds)
        } yield {
          val g = AsteriskGraph.add(graph, channel ~ NodeMdsTrunkBridge(callId))
          (g, impactedChannels(g, channel))
        }
      case _ => None
    }
}
