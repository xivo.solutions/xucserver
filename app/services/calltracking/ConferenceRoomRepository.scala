package services.calltracking

import play.api.Logger
object ConferenceRoomRepository {
  type ConferenceMap         = Map[String, ConferenceRoom]
  type ChannelParticipantMap = Map[String, ConferenceParticipant]
  val logger: Logger = Logger(getClass.getName)

  val empty: ConferenceRoomRepository =
    ConferenceRoomRepository(Map.empty, Map.empty, logger)
}

import ConferenceRoomRepository.*

case class ConferenceRoomRepository(
    conferences: ConferenceMap,
    channelMap: ChannelParticipantMap,
    logger: Logger
) {

  def numbers: Set[String] = conferences.keySet

  def updateConference(c: ConferenceRoom): ConferenceRoomRepository = {
    logger.debug(s"Updating conference ${c.number}")
    val updatedConf = conferences
      .get(c.number)
      .map(_.copy(name = c.name, userPin = c.userPin, adminPin = c.adminPin))
      .getOrElse(c)

    ConferenceRoomRepository(
      conferences.updated(c.number, updatedConf),
      channelMap,
      logger
    )
  }

  def removeConference(numConf: String): ConferenceRoomRepository = {
    logger.debug(s"Removing conference $numConf if empty")
    if (conferences.get(numConf).exists(_.participants.isEmpty)) {
      val removedChannels = conferences
        .get(numConf)
        .map(_.participants.map(_.channelName))
        .getOrElse(List.empty)

      ConferenceRoomRepository(
        conferences - numConf,
        channelMap -- removedChannels,
        logger
      )
    } else { this }
  }

  def getConference(numConf: String): Option[ConferenceRoom] =
    conferences.get(numConf)

  def addParticipant(
      p: ConferenceParticipant,
      mds: String,
      nodeHost: String
  ): ConferenceRoomRepository = {
    logger.debug(s"Adding participant $p in conference ${p.numConf}")
    val c = conferences
      .getOrElse(p.numConf, ConferenceRoom.empty(p.numConf, mds, nodeHost))
      .addParticipant(p)
    val channels = channelMap.updated(p.channelName, p)
    ConferenceRoomRepository(
      conferences.updated(c.number, c),
      channels,
      logger
    )
  }

  def removeParticipant(
      p: ConferenceParticipant,
      mds: String,
      nodeHost: String
  ): ConferenceRoomRepository = {
    logger.debug(s"Removing participant $p from conference ${p.numConf}")
    val c = conferences
      .getOrElse(p.numConf, ConferenceRoom.empty(p.numConf, mds, nodeHost))
      .removeParticipant(p)
    val channels = channelMap.updated(p.channelName, p)
    ConferenceRoomRepository(
      conferences.updated(c.number, c),
      channels,
      logger
    )
  }

  def updateParticipant(numConf: String, index: Int)(
      f: ConferenceParticipant => ConferenceParticipant
  ): ConferenceRoomRepository = {
    logger.debug(s"updating user by index $index in conference $numConf")
    conferences
      .get(numConf)
      .map(_.updateParticipant(index)(f))
      .map(c => {
        ConferenceRoomRepository(
          conferences.updated(c.number, c),
          channelMap,
          logger
        )
      })
      .getOrElse(this)
  }

  def getParticipant(
      numConf: String,
      index: Int
  ): Option[ConferenceParticipant] =
    logger.debug(s"Getting user by index $index in conference $numConf")
    conferences.get(numConf).flatMap(_.getParticipant(index))

  def getParticipantByChannel(c: String): Option[ConferenceParticipant] =
    logger.debug(s"Getting user by channel $c")
    channelMap.get(c)
}
