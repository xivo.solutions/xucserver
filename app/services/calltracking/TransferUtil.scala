package services.calltracking

import org.apache.pekko.actor.{ActorContext, ActorRef}
import org.apache.pekko.util.Timeout
import com.google.inject.name.Named
import com.google.inject.{ImplementedBy, Inject}
import models.XivoUser

import scala.util.{Failure, Success}
import services.ActorIds
import services.calltracking.graph.{
  NodeChannel,
  NodeChannelLike,
  NodeLocalChannel
}
import xivo.models.Line
import xivo.phonedevices.{
  DeviceAdapter,
  DialWithLocalChannelCommand,
  HangupChannelCommand,
  LocalOptimizeAwayCommand,
  MultipleRedirectCommand,
  QueueLogEventCommand,
  QueueStatusByMemberCommand,
  RedirectCommand,
  SetVarCommand,
  WebRTCDeviceBearer
}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.concurrent.duration._
import xivo.xucami.models.ChannelState
import helpers.OptionToTry._
import play.api.Logger

case class CallChannelsForTransfer(local: NodeChannel, remote: NodeChannelLike)
sealed trait TransferEvent
case class QueueTransferEvent(
    queue: String,
    uniqueId: String,
    originalUniqueId: String
) extends TransferEvent
case class NoQueueTransferEvent(
    channel: NodeLocalChannel,
    uniqueId: String,
    originalUniqueId: String
) extends TransferEvent
case class CallsForTransfer(
    call1: CallChannelsForTransfer,
    call2: CallChannelsForTransfer,
    remoteId: Option[String] = None,
    queueEvent: Option[TransferEvent] = None
)

@ImplementedBy(classOf[TransferUtilImpl])
trait TransferUtil {
  def getChannelsForTransfer(
      calls: Map[String, DeviceCall]
  ): Option[CallsForTransfer]
  def completeTransfer(
      calls: CallsForTransfer,
      timeout: FiniteDuration = 3.minutes,
      agentNum: Option[String] = None
  )(implicit context: ActorContext): Future[(NodeChannelLike, NodeChannelLike)]
  def attendedTransfer(
      calls: Map[String, DeviceCall],
      destination: String,
      device: Option[WebRTCDeviceBearer],
      line: Line,
      xivoUser: XivoUser,
      deviceAdapter: DeviceAdapter,
      sender: ActorRef
  )(implicit context: ActorContext): Future[Boolean]
}

class TransferUtilImpl @Inject() (
    @Named(ActorIds.ChannelTrackerId) channelTracker: ActorRef,
    @Named(ActorIds.AsteriskGraphTrackerId) graphTracker: ActorRef,
    @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef
) extends TransferUtil {

  val log: Logger = Logger(getClass.getName)

  private def fixNodeChannel(channel: NodeChannelLike): NodeChannelLike =
    channel match {
      case NodeChannel(_, _) => channel
      case NodeLocalChannel(name, mdsName) =>
        NodeLocalChannel(name.dropRight(1) + "1", mdsName)
    }

  private def getQueueTransferEvent(
      calls: Map[String, DeviceCall]
  ): Option[TransferEvent] = {
    calls.values.toList match {
      case call1 :: call2 :: nil =>
        for {
          rch1        <- AsteriskGraphTracker.pathsEndChannels(call1.paths).headOption
          rch1Channel <- call1.remoteChannels.get(rch1.name)
          rch2 <-
            call2.paths
              .flatMap(_.collect({
                case c: NodeLocalChannel if c.name.endsWith(";2") => c
              }).headOption)
              .headOption
          rch2Channel <- call2.remoteChannels.get(rch2.name)
          queue = rch2Channel.lastQueueName
        } yield queue match {
          case Some(queueName) =>
            QueueTransferEvent(queueName, rch2Channel.id, rch1Channel.id)
          case None =>
            NoQueueTransferEvent(rch2, rch2Channel.id, rch1Channel.id)
        }

      case _ => None
    }
  }

  def getChannelsForTransfer(
      calls: Map[String, DeviceCall]
  ): Option[CallsForTransfer] =
    calls.values.toList match {
      case call1 :: call2 :: nil =>
        log.info(s"Transferring following calls: $calls")
        for {
          rch1 <-
            AsteriskGraphTracker.pathsFirstChannels(call1.paths).headOption
          rch2 <-
            AsteriskGraphTracker.pathsFirstChannels(call2.paths).headOption
          queueEvent = getQueueTransferEvent(calls)
        } yield CallsForTransfer(
          CallChannelsForTransfer(
            NodeChannel(call1.channelName, rch1.sourceMds),
            rch1
          ),
          CallChannelsForTransfer(
            NodeChannel(call2.channelName, rch2.sourceMds),
            fixNodeChannel(rch2)
          ),
          call2.channel.map(_.linkedChannelId),
          queueEvent
        )
      case _ => None
    }

  def completeTransfer(
      calls: CallsForTransfer,
      timeout: FiniteDuration = 3.minutes,
      agentNum: Option[String]
  )(implicit
      context: ActorContext
  ): Future[(NodeChannelLike, NodeChannelLike)] = {
    implicit val ec: ExecutionContextExecutor = context.dispatcher

    amiBusConnector ! SetVarCommand(
      calls.call2.local.name,
      "CHANNEL(hangupsource)",
      calls.call2.local.name
    )

    WaitForChannelInBridgeWait(channelTracker, graphTracker, calls.call1.local)
      .foreach(_ =>
        amiBusConnector ! HangupChannelCommand(calls.call1.local.name)
      )

    WaitForChannelInBridgeWait(channelTracker, graphTracker, calls.call2.local)
      .foreach(_ =>
        amiBusConnector ! HangupChannelCommand(calls.call2.local.name)
      )

    val step1 = waitForBothChannelBridged(
      (calls.call1.remote, calls.call2.remote)
    )

    setChannelLinkedIdVar(calls.call1.local.name, calls.remoteId)

    amiBusConnector ! MultipleRedirectCommand(
      calls.call1.remote.name,
      "xuc_attended_xfer_wait",
      "s",
      1,
      calls.call1.local.name,
      "xuc_attended_xfer_wait",
      "s",
      1
    )

    amiBusConnector ! MultipleRedirectCommand(
      calls.call2.remote.name,
      "xuc_attended_xfer_wait",
      "s",
      1,
      calls.call2.local.name,
      "xuc_attended_xfer_wait",
      "s",
      1
    )

    def setChannelLinkedIdVar(
        channel: String,
        remoteId: Option[String]
    ): Unit = {
      remoteId match {
        case Some(linkedId) =>
          amiBusConnector ! SetVarCommand(
            channel,
            "XIVO_CHANNEL2_LINKEDID",
            linkedId
          )
        case None =>
          log.error(
            s"Unable to set variable for transfer user event for call with empty linked channel id"
          )
      }
    }

    step1
      .flatMap(channels => {
        val waitrch1 = WaitForChannelState(
          channelTracker,
          channels._1,
          ChannelState.HUNGUP,
          Timeout(timeout)
        )
        waitrch1.foreach(_ =>
          amiBusConnector ! HangupChannelCommand(channels._2.name)
        )
        val waitrch2 = WaitForChannelState(
          channelTracker,
          channels._2,
          ChannelState.UP,
          Timeout(timeout)
        )
        waitrch2.failed.foreach(_ => {
          waitrch1.cancel()
          amiBusConnector ! HangupChannelCommand(channels._2.name)
          amiBusConnector ! RedirectCommand(
            channels._1.name,
            "xuc_attended_xfer_timeout",
            "s",
            1
          )
        })
        waitrch2.foreach(_ => waitrch1.cancel())

        waitrch2.map(_ => channels)
      })
      .map(channels => {
        amiBusConnector ! SetVarCommand(
          channels._1.name,
          "XIVO_CHAN_TO_BRIDGE",
          channels._2.name
        )
        setChannelLinkedIdVar(channels._1.name, calls.remoteId)

        log.debug(
          s"Start redirect command for ${channels._1} and ${channels._2}"
        )

        amiBusConnector ! RedirectCommand(
          channels._1.name,
          "xuc_attended_xfer_end",
          "s",
          1
        )
        amiBusConnector ! LocalOptimizeAwayCommand(channels._2.name)

        agentNum.map(num =>
          context.system.scheduler.scheduleOnce(
            2.seconds,
            amiBusConnector,
            QueueStatusByMemberCommand(s"Agent/$num")
          )
        )

        log.debug(s"QueueEvent: ${calls.queueEvent}")
        calls.queueEvent.foreach {
          case QueueTransferEvent(queue, uniqueId, originalUniqueId) =>
            amiBusConnector ! QueueLogEventCommand(
              queue,
              QueueLogEventCommand.QueueLogTransferEvent,
              uniqueId,
              originalUniqueId
            )
          case NoQueueTransferEvent(nodeChannel, _, originalUniqueId) =>
            context.actorOf(
              MonitorTransferredChannel.props(
                channelTracker,
                amiBusConnector,
                nodeChannel,
                originalUniqueId
              )
            )
        }
        channels
      })
  }

  def waitForBothChannelBridged(channels: (NodeChannelLike, NodeChannelLike))(
      implicit context: ActorContext
  ): Future[(NodeChannelLike, NodeChannelLike)] = {
    implicit val ec: ExecutionContextExecutor = context.dispatcher

    val waitForRemoteChan1 =
      WaitForChannelInBridgeWait(channelTracker, graphTracker, channels._1)
    waitForRemoteChan1.failed.foreach(_ =>
      amiBusConnector ! HangupChannelCommand(channels._2.name)
    )

    val waitForRemoteChan2 =
      WaitForChannelInBridgeWait(channelTracker, graphTracker, channels._2)
    waitForRemoteChan2.failed.foreach(_ =>
      amiBusConnector ! HangupChannelCommand(channels._1.name)
    )

    for {
      rch1Bridged <- waitForRemoteChan1
      rch2Bridged <- waitForRemoteChan2
    } yield channels
  }

  def attendedTransfer(
      calls: Map[String, DeviceCall],
      destination: String,
      device: Option[WebRTCDeviceBearer],
      line: Line,
      xivoUser: XivoUser,
      deviceAdapter: DeviceAdapter,
      sender: ActorRef
  )(implicit context: ActorContext): Future[Boolean] = {
    implicit val ec: ExecutionContextExecutor = context.dispatcher

    val tCall =
      calls.values
        .find(_ => calls.size == 1)
        .toTry(
          new Exception(
            s"Expecting only one call for transfer, got ${calls.size} : $calls"
          )
        )

    val tCmd =
      for {
        number <- line.number.toTry(new Exception("Line with no number"))
        call   <- tCall
      } yield DialWithLocalChannelCommand(
        line.interface,
        xivoUser.fullName,
        number,
        destination,
        device,
        line.xivoIp,
        xivoUser.id,
        line.context,
        call.variables,
        line.driver,
        line.dev.map(_.vendor)
      )

    val tChannel = tCall
      .flatMap(c =>
        c.channel
          .toTry(new Exception("Unable to transfer call with no channel"))
      )
      .flatMap(c =>
        c.state match {
          case ChannelState.HOLD | ChannelState.UP =>
            Success(c)
          case _ =>
            Failure(
              new Exception(
                s"Unable to transfer call, channel has invalid state: ${c.state}"
              )
            )
        }
      )

    val result = for {
      call    <- tCall
      channel <- tChannel
      cmd     <- tCmd
    } yield channel.state match {
      case ChannelState.HOLD =>
        amiBusConnector ! cmd
        Future.successful(true)
      case ChannelState.UP =>
        deviceAdapter.hold(Some(call), sender)
        WaitForChannelState(
          channelTracker,
          NodeChannel(call.channelName, channel.mdsName),
          ChannelState.HOLD,
          Timeout(2.second)
        )
          .map(_ => { amiBusConnector ! cmd })
          .map(_ => true)
    }

    result match {
      case Success(f) => f
      case Failure(f) => Future.failed(f)
    }

  }
}
