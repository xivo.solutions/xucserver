package services.calltracking

import org.apache.pekko.actor._
import services.calltracking.BaseTracker._
import services.calltracking.graph.NodeChannelLike
import xivo.xucami.models.{Channel, ChannelState}
import xivo.phonedevices.QueueLogEventCommand
import scala.concurrent.duration._

class MonitorTransferredChannel(
    channelTracker: ActorRef,
    amiBusConnectorURI: ActorRef,
    channel: NodeChannelLike,
    transferredChannelId: String
) extends Actor
    with ActorLogging {

  def timeout: FiniteDuration = 3.minute

  def scheduler: Scheduler = context.system.scheduler

  override def preStart(): Unit = {
    channelTracker ! WatchChannelStartingWith(channel.name)
    scheduler.scheduleOnce(
      timeout,
      self,
      MonitorTransferredChannel.MonitorTimeout
    )(context.dispatcher)
  }

  override def postStop(): Unit = {
    channelTracker ! UnWatchChannelStartingWith(channel.name)
  }

  override def receive: PartialFunction[Any, Unit] = {
    case MonitorTransferredChannel.MonitorTimeout =>
      log.debug("Timeout before entering a queue")
      context.stop(self)

    case c: Channel =>
      if (c.lastQueueName.isDefined) {
        c.lastQueueName.foreach(queue =>
          amiBusConnectorURI ! QueueLogEventCommand(
            queue,
            QueueLogEventCommand.QueueLogTransferEvent,
            c.id,
            transferredChannelId
          )
        )
        context.stop(self)
      } else if (c.state == ChannelState.HUNGUP) {
        log.debug(s"$channel is hung up")
        context.stop(self)
      }
  }
}

object MonitorTransferredChannel {
  case object MonitorTimeout

  def props(
      channelTracker: ActorRef,
      amiBusConnectorURI: ActorRef,
      channel: NodeChannelLike,
      transferredChannelId: String
  ): Props =
    Props(
      new MonitorTransferredChannel(
        channelTracker,
        amiBusConnectorURI,
        channel,
        transferredChannelId
      )
    )
}
