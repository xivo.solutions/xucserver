package services.calltracking.graph

case class Edge[Node](n1: Node, n2: Node)

object BaseGraph {
  type Links[Node] = Map[Node, Set[Node]]
  type Path[Node]  = List[Node]
  def Path[Node](xs: Node*): List[Node] = List(xs*)
}

import BaseGraph._
abstract class BaseGraph[Node, +This <: BaseGraph[Node, This]](
    links: Links[Node]
)(implicit newBuilder: Links[Node] => This) {
  self: This =>

  override def toString(): String = s"BaseGraph($links)"

  def add(e: Edge[Node]): This =
    newBuilder(
      links
        + (e.n1 -> (neighbors(e.n1) + e.n2))
        + (e.n2 -> (neighbors(e.n2) + e.n1))
    )

  def add(edges: List[Edge[Node]]): This =
    edges.foldRight(this)((edge, graph) => graph.add(edge))

  def del(e: Edge[Node]): This =
    newBuilder({
      val neighborsOf1 = neighbors(e.n1) - e.n2
      val neighborsOf2 = neighbors(e.n2) - e.n1

      val newLinks = if (neighborsOf1.isEmpty) {
        links - e.n1
      } else {
        links.updated(e.n1, neighbors(e.n1) - e.n2)
      }

      if (neighborsOf2.isEmpty) {
        newLinks - e.n2
      } else {
        newLinks.updated(e.n2, neighbors(e.n2) - e.n1)
      }
    })

  def del(edges: List[Edge[Node]]): This =
    edges.foldRight(this)((edge, graph) => graph.del(edge))

  def del(n: Node): This = {
    neighbors(n)
      .map(Edge(n, _))
      .foldLeft(this)((graph, edge) => graph.del(edge))
  }

  def neighbors(n: Node): Set[Node] =
    links.getOrElse(n, Set.empty)

  def connectionsOf(n: Node): Set[Node] = {
    def connectionsOfAcc(n: Node, connections: Set[Node]): Set[Node] = {
      val nexts = neighbors(n) -- connections

      if (nexts.isEmpty) connections
      else nexts.flatMap(i => connectionsOfAcc(i, connections + i))
    }

    connectionsOfAcc(n, Set(n)) - n
  }

  def endpointsOf(n: Node): Set[Node] = {
    def endpointsOfAcc(n: Node, visited: Set[Node]): Set[Node] = {
      val nexts = neighbors(n) -- visited
      if (nexts.isEmpty) Set(n)
      else nexts.flatMap(i => endpointsOfAcc(i, visited + i))
    }
    endpointsOfAcc(n, Set.empty) - n
  }

  def pathsFrom(n: Node): Set[Path[Node]] = {
    def pathsFromAcc(
        n: Node,
        visited: Set[Node],
        currentPath: Path[Node]
    ): Set[Path[Node]] = {
      val nexts = neighbors(n) -- visited
      if (nexts.isEmpty) Set(currentPath)
      else nexts.flatMap(i => pathsFromAcc(i, visited + i, currentPath :+ i))
    }

    pathsFromAcc(n, Set(n), List.empty)
  }

}
