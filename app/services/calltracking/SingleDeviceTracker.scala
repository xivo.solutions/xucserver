package services.calltracking

import org.apache.pekko.actor.*
import helpers.{FastLogging, JmxActorMonitor}
import org.asteriskjava.manager.action.StopMixMonitorAction
import services.XucAmiBus
import services.XucAmiBus.AmiAction
import services.calltracking.DeviceConferenceAction.*
import services.calltracking.DeviceMessage.*
import services.calltracking.graph.NodeChannelLike
import xivo.xuc.DeviceTrackerConfig
import xivo.xucami.models.{Channel, ChannelState, MonitorState}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.*
import helpers.JmxLongMetric
import services.channel.ActionBuilder
import services.request.PhoneRequest.IncludeToConference
import xivo.xucami.models.Channel.VarNames.XucRecordUnpaused
import xivo.xucami.models.MonitorState.MonitorState

import scala.util.Try

object SingleDeviceTracker {

  trait DeviceMessageWithInterface extends DeviceMessage {
    def interface: DeviceInterface
    override def isFor(dstInterface: DeviceInterface): Boolean =
      interface.stripSuffix("-") == dstInterface.stripSuffix("-")
  }

  case object GetCalls
  case class Calls(calls: List[DeviceCall])
  case class PartyInformation(
      partyChannelName: DeviceInterface,
      channel: Channel,
      sourceTrackerType: DeviceTrackerType
  ) extends DeviceMessage {

    override def isFor(interface: DeviceInterface): Boolean =
      partyChannelName.startsWith(interface)
  }

  case class MonitorCalls(interface: DeviceInterface)
      extends DeviceMessageWithInterface
  case class UnMonitorCalls(interface: DeviceInterface)
      extends DeviceMessageWithInterface
  case class DeviceConferenceMessage(
      interface: DeviceInterface,
      command: DeviceConferenceCommand
  ) extends DeviceMessageWithInterface

  case class SendCurrentCallsPhoneEvents(interface: DeviceInterface)
      extends DeviceMessageWithInterface

  final case class IncludeToConfGetCalls(
      phoneInterface: String,
      replyTo: ActorRef,
      evt: IncludeToConference,
      ctiRouterRef: ActorRef
  ) extends DeviceMessage {
    def isFor(interface: DeviceInterface): Boolean =
      interface.contains(phoneInterface)
  }

  sealed trait DeviceTrackerType
  case object SipDeviceTrackerType              extends DeviceTrackerType
  case object CustomDeviceTrackerType           extends DeviceTrackerType
  case object TrunkDeviceTrackerType            extends DeviceTrackerType
  case object MediaServerTrunkDeviceTrackerType extends DeviceTrackerType
  case object UnknownDeviceTrackerType          extends DeviceTrackerType

}

abstract class SingleDeviceTracker(
    interface: DeviceInterface,
    channelTracker: ActorRef,
    graphTracker: ActorRef,
    xucAmiBus: XucAmiBus,
    deviceTrackerConfig: DeviceTrackerConfig
) extends Actor
    with FastLogging
    with JmxActorMonitor
    with ActionBuilder {

  import AsteriskGraphTracker._
  import BaseTracker._
  import SingleDeviceTracker._

  case class RemoveChannel(name: String)
  case class RemoveLoopChannel(name: String)

  val jmxCallCount: Try[JmxLongMetric] =
    jmxBean.addLong("Calls", 0L, Some("Current number of call"))
  val jmxPartyInformationCount: Try[JmxLongMetric] = jmxBean.addLong(
    "PartyInformations",
    0L,
    Some("Total number of PartyInformation received")
  )
  val jmxPathsCount: Try[JmxLongMetric] = jmxBean.addLong(
    "PathsFromChannel",
    0L,
    Some("Total number of PathsFromChannel received")
  )
  val jmxChannelsCount: Try[JmxLongMetric] = jmxBean.addLong(
    "ChannelEvent",
    0L,
    Some("Total number of Channel message received")
  )
  jmxBean.registerOperation(
    "stop",
    stopme,
    Some("Stop actor. Warning, it will not be restarted automatically !")
  )
  jmxBean.registerOperation(
    "clearCalls",
    clearcalls,
    Some("Clear list of calls from this tracker but does not hangup them.")
  )

  var tracker: CallTracker        = CallTracker()
  var callWatchers: Set[ActorRef] = Set.empty
  var loopedChannels: Set[String] = Set.empty

  var lastNotification: Option[DeviceCall] = None

  val deviceTrackerType: DeviceTrackerType

  val channelStartsWith: DeviceInterface = interface

  override def preStart(): Unit = {
    log.debug(s"Starting DeviceTracker for $interface ($channelStartsWith)")
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
    channelTracker ! WatchChannelStartingWith(channelStartsWith)
    graphTracker ! WatchChannelStartingWith(channelStartsWith)
    context.parent ! DevicesTracker.RegisterActor(channelStartsWith, self)
    context.become(customBehavior)
  }

  override def postStop(): Unit = {
    log.debug(s"Stopping DeviceTracker for $interface ($channelStartsWith)")
    channelTracker ! UnWatchChannelStartingWith(channelStartsWith)
    graphTracker ! UnWatchChannelStartingWith(channelStartsWith)
    context.parent ! DevicesTracker.UnRegisterActor(channelStartsWith, self)
    jmxBean.unregister()
  }

  def stopme(): Unit = self ! PoisonPill

  def clearcalls(): Unit = {
    tracker.calls.keys.foreach(self ! RemoveChannel(_))
  }

  def customBehavior: Receive = receive

  override def receive: PartialFunction[Any, Unit] = {
    case PathsFromChannel(c: NodeChannelLike, paths) =>
      jmxPathsCount.inc()
      if (!loopedChannels.contains(c.name) && filterNodeChannel(c)) {
        for {
          call    <- tracker.calls.get(c.name)
          channel <- call.channel
          newConnection <-
            pathsEndChannels(paths) -- pathsEndChannels(call.paths)
        } context.parent ! PartyInformation(
          newConnection.name,
          channel,
          deviceTrackerType
        )

        tracker = tracker.withPaths(c, paths)
        notify(c.name)
      }

    case c: Channel =>
      jmxChannelsCount.inc()
      if (!loopedChannels.contains(c.name) && filterChannel(c)) {
        tracker = tracker.withChannel(c)
        jmxCallCount.set(tracker.calls.size)
        if (c.state == ChannelState.HUNGUP) {
          scheduler.scheduleOnce(2.seconds, self, RemoveChannel(c.name))
        }
        for {
          call   <- tracker.calls.get(c.name)
          remote <- pathsEndChannels(call.paths)
        } context.parent ! PartyInformation(remote.name, c, deviceTrackerType)
        notify(c.name)
      }

    case RemoveChannel(name) =>
      tracker = tracker.removeChannel(name)
      jmxCallCount.set(tracker.calls.size)

    case RemoveLoopChannel(name) =>
      loopedChannels = loopedChannels - name

    case PartyInformation(partyChannelName, channel, sourceTrackerType) =>
      log.debug(
        s"PartyInformation with partyChannelName $partyChannelName sourceTrackerType $sourceTrackerType and channel ${channel.name} ${channel.state} ${channel.monitored}"
      )
      jmxPartyInformationCount.inc()
      tracker = tracker.withPartyInformation(partyChannelName, channel)
      onPartyInformation(
        PartyInformation(partyChannelName, channel, sourceTrackerType)
      )
      toggleRecording(
        PartyInformation(partyChannelName, channel, sourceTrackerType)
      )
      notify(partyChannelName)

    case GetCalls =>
      val calls = getCalls()
      sender() ! Calls(calls.toList)

    case MonitorCalls(_) =>
      callWatchers = callWatchers + sender()
      sender() ! DeviceCalls(interface, tracker.calls)

    case UnMonitorCalls(_) =>
      callWatchers = callWatchers - sender()

    case LoopDetected(channels) =>
      val channelNames = channels.map(_.name)
      loopedChannels = loopedChannels ++ channelNames
      channelNames.foreach(name => {
        tracker = tracker.removeChannel(name)
        scheduler.scheduleOnce(30.seconds, self, RemoveLoopChannel(name))
      })
      jmxCallCount.set(tracker.calls.size)
  }

  protected def getCalls(): Iterable[DeviceCall] =
    tracker.calls.values.filter(
      _.channel.forall(_.state != ChannelState.HUNGUP)
    )

  def scheduler: Scheduler = context.system.scheduler

  def notify(channelName: String): Unit = {
    callWatchers.foreach(
      _ ! DeviceCalls(
        interface,
        tracker.calls.filter(
          _._2.channel.forall(_.state != ChannelState.HUNGUP)
        )
      )
    )
    tracker.calls
      .get(channelName)
      .filterNot(lastNotification.contains)
      .foreach(c => {
        lastNotification = Some(c)
        notify(c)
      })
  }

  def notify(call: DeviceCall, redoLast: Boolean = false): Unit

  def onPartyInformation(evt: PartyInformation): Unit

  def toggleRecording(evt: PartyInformation): Unit = {

    def logAmiAction(
        act: XucAmiBus.AmiAction,
        channel: Option[Channel],
        channelName: String,
        call: DeviceCall,
        additionalMessage: String
    ): Unit = {
      log.info(
        s"${act.message.getAction} " +
          s"on channel $channelName " +
          s"for recordingid:${channel
              .foreach(_.variables.getOrElse("recordingid", ""))} " +
          s"(channel ${evt.partyChannelName} " +
          additionalMessage
      )
    }

    def startRecording(
        incomingChannel: Channel
    ): List[AmiAction] = {
      val recordingId: String =
        incomingChannel.variables.getOrElse("recordingid", "")
      if !recordingId.isBlank then
        log.warning(
          s"Activating Recording Preemptively for channel ${incomingChannel.name} with recordingid $recordingId"
        )
        val varAction: AmiAction = setVarAction(
          channelName = incomingChannel.name,
          key = XucRecordUnpaused,
          value = "true",
          channelId = incomingChannel.id
        )
        val mixMonitorAction: AmiAction = startMixMonitorAction(
          channelName = incomingChannel.name,
          channelId = incomingChannel.id,
          recordingId = recordingId
        )
        List(varAction, mixMonitorAction)
      else
        log.debug(
          s"recordingid not found in channel, cannot enable recording preemptively for ${incomingChannel.name}"
        )
        List.empty
    }

    def isRodChannelWithRodNotStarted(channel: Channel): Boolean =
      // Given channel has the recording mode recordedondemand (rod) but it has never been started by the agent
      // Thus its monitor state is MonitorState.PAUSED
      channel.variables
        .getOrElse(
          "__MONITOR_PAUSED",
          ""
        ) == "true" && channel.variables
        .getOrElse("XUC_RECORD_UNPAUSED", "") != "true"

    def getRecordingActions(
        incomingChannel: Channel,
        bridgedChannel: Channel
    ): List[AmiAction] =
      def getAmiActionsByMonitorStates
          : PartialFunction[(MonitorState, String, MonitorState), List[
            AmiAction
          ]] = {
        case (MonitorState.ACTIVE, "notrecorded", _) =>
          List(stopMixMonitorAction(incomingChannel.name))
        case (MonitorState.PAUSED, "recorded", _) =>
          if isRodChannelWithRodNotStarted(incomingChannel)
          then
            startRecording(
              incomingChannel
            )
          else
            List(
              unpauseMixMonitorAction(
                incomingChannel.name,
                incomingChannel.id
              )
            )
        case (MonitorState.PAUSED, "notrecorded", _) =>
          List(stopMixMonitorAction(incomingChannel.name))
        case (MonitorState.DISABLED, "notrecorded", MonitorState.ACTIVE) =>
          List(stopMixMonitorAction(incomingChannel.name))
        case (MonitorState.DISABLED, "notrecorded", MonitorState.PAUSED) =>
          List(stopMixMonitorAction(incomingChannel.name))
      }

      getAmiActionsByMonitorStates.applyOrElse(
        (
          incomingChannel.monitored,
          bridgedChannel.variables
            .getOrElse(Channel.VarNames.RecordingMode, ""),
          bridgedChannel.monitored
        ),
        _ =>
          log.debug(s"No action taken for ami event $evt")
          List.empty
      )

    def publishRecordActionToAmi(
        call: DeviceCall
    ): Unit = {
      val action: List[AmiAction] = call.channel match
        case Some(incomingChannel: Channel) =>
          getRecordingActions(
            incomingChannel = incomingChannel,
            bridgedChannel = evt.channel
          )
        case _ =>
          log.debug(s"No action taken for ami event $evt")
          List.empty

      action.foreach(act => {
        xucAmiBus.publish(act)
        logAmiAction(
          act,
          call.channel,
          evt.partyChannelName,
          call,
          s" was bridged with channel ${evt.channel.name} " +
            s"which is in ${evt.channel.variables.getOrElse(Channel.VarNames.RecordingMode, "")} mode)"
        )
      })
    }

    def publishExplicitStopRecordActionToAmi(call: DeviceCall): Unit = {
      // This part is run only to stop monitor for monitored channels in path which are not the end channel
      // It purposely does not uses the setRecordingActions method because it acts the other way round :
      // here it stop recording according to myMonitorState AND myRecMode AND otherPartyMonitorState
      // (setRecordingActions stops/starts recording according to myMonitorState AND otherPartyRecMode)
      val myRecMode = call.channel.map(
        _.variables.getOrElse(Channel.VarNames.RecordingMode, "")
      )
      if (myRecMode.contains("notrecorded")) {
        if (
          evt.channel.monitored == MonitorState.ACTIVE || evt.channel.monitored == MonitorState.PAUSED
        ) {
          val action = new StopMixMonitorAction(evt.channel.name)
          xucAmiBus.publish(AmiAction(action))
          logAmiAction(
            AmiAction(action),
            Some(evt.channel),
            evt.channel.name,
            call,
            s"in ${myRecMode.getOrElse("")} mode " +
              s"was bridged with channel ${evt.channel.name} which was being recorded)"
          )
        }
      }
    }

    if (deviceTrackerConfig.enableRecordingRules) {
      tracker.calls
        .get(evt.partyChannelName)
        .foreach(call => {
          val myMonitorState = call.channel.map(_.monitored)
          if (pathsEndChannels(call.paths).exists(_.name == evt.channel.name)) {
            publishRecordActionToAmi(call)
          } else if (myMonitorState.contains(MonitorState.DISABLED)) {
            publishExplicitStopRecordActionToAmi(call)
          }
        })
    }
  }

  def filterChannel(c: Channel): Boolean             = true
  def filterNodeChannel(c: NodeChannelLike): Boolean = true
}
