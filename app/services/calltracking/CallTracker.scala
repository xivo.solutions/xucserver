package services.calltracking

import services.calltracking.AsteriskGraphTracker.AsteriskPath
import services.calltracking.CallTracker.Channels
import services.calltracking.graph.{NodeChannelLike, NodeDial}
import services.calltracking.ConferenceTracker._
import xivo.xucami.models.{
  Channel,
  ChannelDirection,
  ChannelState,
  MonitorState
}
import xivo.events.UserData

object CallTracker {
  type Channels = Set[String]
}

case class DeviceCall(
    channelName: String,
    channel: Option[Channel],
    paths: Set[AsteriskPath],
    remoteChannels: Map[String, Channel]
) {

  def queueName: Option[String] = {
    if (channel.exists(_.isOriginate)) None
    else
      remoteChannels.values.toList
        .sortBy(_.id)
        .flatMap(_.variables.get(UserData.QueueNameKey))
        .lastOption
  }

  def nonLocalRemoteChannels: List[Channel] =
    remoteChannels.values.filterNot(_.isLocal).toList

  def nonGroupRemoteChannel: Channel => Boolean = { (channel: Channel) =>
    channel.state match {
      case ChannelState.DOWN => false
      case _ =>
        !channel.variables
          .get(Channel.VarNames.XiVOFwdReferer)
          .exists(_.startsWith("group"))
    }
  }

  def remoteLineNumber: Option[String] = {
    channel
      .flatMap(_.connectedLineNb)
      .orElse(
        nonLocalRemoteChannels
          .filter(nonGroupRemoteChannel)
          .filter(_.direction.contains(ChannelDirection.OUTGOING))
          .flatMap(_.callerId.numberOption)
          .headOption
      )
      .orElse(channel.flatMap(_.exten))
  }

  def remoteLineName: Option[String] = {
    channel
      .flatMap(_.connectedLineName)
      .orElse(
        nonLocalRemoteChannels
          .filter(nonGroupRemoteChannel)
          .filter(_.direction.contains(ChannelDirection.OUTGOING))
          .flatMap(_.callerId.nameOption)
          .headOption
      )
  }

  def variables: Map[String, String] = {
    val remoteVars = remoteChannels.values.flatMap(
      _.variables.view.filterKeys(_.startsWith(Channel.UserDataPrefix)).toMap
    )
    val localVars = channel.map(_.variables).getOrElse(Map.empty)

    localVars ++ remoteVars
  }

  def monitoredChannels: List[Channel] = {
    val deviceMonitoredChannel =
      channel.filter(_.monitored != MonitorState.DISABLED).toList
    val remoteMonitoredChannel =
      remoteChannels.values.filter(_.monitored != MonitorState.DISABLED)

    deviceMonitoredChannel ++ remoteMonitoredChannel
  }

  def monitorState: Option[MonitorState.MonitorState] = {
    channel
      .map(_.monitored)
      .filter(_ != MonitorState.DISABLED)
      .orElse(
        remoteChannels.values.map(_.monitored).find(_ != MonitorState.DISABLED)
      )
  }

  private def endChannel: Option[Channel] =
    AsteriskGraphTracker
      .pathsEndChannels(paths)
      .map(_.name)
      .flatMap(remoteChannels.get)
      .filterNot(_.isLocal)
      .headOption

  private def localChannelHasNoNodeDial: Boolean = {
    paths.flatten.collect({ case n: NodeDial => n }).isEmpty
  }

  private def remoteChannelIsAnswered: Boolean = {
    remoteChannels.values.exists(c => c.state == ChannelState.UP)
  }

  private def remoteChannelIsInHold: Boolean = {
    remoteChannels.values.exists(c => c.state == ChannelState.HOLD)
  }

  def callState: Option[ChannelState.ChannelState] =
    channel.flatMap(c =>
      c.state match {
        case ChannelState.RINGING if c.isOriginate || c.isOutboundOriginate =>
          Some(ChannelState.ORIGINATING)

        case ChannelState.RINGING if !c.isAgentCallback =>
          Some(ChannelState.RINGING)

        case ChannelState.UP if c.isOriginate =>
          if (
            (remoteChannelIsAnswered && localChannelHasNoNodeDial)
            || remoteChannelIsInHold
            || c.isSetAsAnsweredByAsterisk
            || c.isSetAsAnsweredByXiVOPickUp
            || c.isPickupGroupCallRetrieval
            || c.isWaitingInQueue
            || c.isSwitchboardOriginate
          )
            Some(ChannelState.UP)
          else
            Some(ChannelState.ORIGINATING)

        case ChannelState.UP if c.isOutboundOriginate =>
          if (
            endChannel
              .map(_.state)
              .contains(ChannelState.UP) && localChannelHasNoNodeDial
          )
            Some(ChannelState.UP)
          else
            Some(ChannelState.ORIGINATING)

        case ChannelState.UP if !c.isAgentCallback => Some(ChannelState.UP)

        case ChannelState.HOLD => Some(ChannelState.HOLD)

        case ChannelState.HUNGUP => Some(ChannelState.HUNGUP)

        case ChannelState.ORIGINATING => Some(ChannelState.ORIGINATING)

        case ChannelState.DIALING => Some(ChannelState.DIALING)

        case _ => None
      }
    )
}

case class DeviceCalls(interface: String, calls: Map[String, DeviceCall])
case class ConferenceWithChannels(
    conference: ConferenceRoom,
    channelNames: Set[Channels] = Set.empty,
    topic: String
) {
  def withChannels(newChannelNames: Channels): ConferenceWithChannels =
    this.copy(channelNames = channelNames + newChannelNames)
  def removeChannel(channelName: String): ConferenceWithChannels =
    this.copy(channelNames = channelNames.filterNot(_.contains(channelName)))
  def withParticipant(p: ConferenceParticipant): ConferenceWithChannels =
    this.copy(conference = conference.addParticipant(p))
  def removeParticipant(p: ConferenceParticipant): ConferenceWithChannels =
    this.copy(conference = conference.removeParticipant(p))
  def updateParticipant(p: ConferenceParticipant): ConferenceWithChannels =
    this.copy(conference = conference.updateParticipant(p.index)(_ => p))
  def findMe(): List[ConferenceParticipant] = {
    conference.participants
      .filter(p => channelNames.flatten.contains(p.channelName))
  }
}

case class CallTracker(
    calls: Map[String, DeviceCall] = Map.empty,
    conferences: Map[String, ConferenceWithChannels] = Map.empty
) {

  def withPaths(c: NodeChannelLike, paths: Set[AsteriskPath]): CallTracker = {
    val call = calls.get(c.name) match {
      case Some(DeviceCall(name, channel, oldPath, remoteChannels)) =>
        val pathDiff = oldPath.flatten.diff(paths.flatten)
        val remoteChannelsToDel =
          pathDiff.collect({ case c: NodeChannelLike => c }).map(_.name)
        val newRemoteChannels = remoteChannels -- remoteChannelsToDel
        DeviceCall(name, channel, paths, newRemoteChannels)

      case _ => DeviceCall(c.name, None, paths, Map.empty)
    }
    if (call.channel.isEmpty && call.paths.flatten.isEmpty)
      this
    else
      this.copy(calls = calls.updated(c.name, call))
  }

  def withChannel(c: Channel): CallTracker = {
    val call = calls.get(c.name) match {
      case Some(DeviceCall(name, _, paths, remoteChannels)) =>
        DeviceCall(name, Some(c), paths, remoteChannels)
      case _ => DeviceCall(c.name, Some(c), Set.empty, Map.empty)
    }
    this.copy(calls = calls.updated(c.name, call))
  }

  def withPartyInformation(
      partyChannelName: String,
      channel: Channel
  ): CallTracker = {
    calls
      .get(partyChannelName)
      .map(c =>
        DeviceCall(
          c.channelName,
          c.channel,
          c.paths,
          c.remoteChannels.updated(channel.name, channel)
        )
      ) match {
      case Some(c) => this.copy(calls = calls.updated(c.channelName, c))
      case None    => this
    }

  }

  def removeChannel(name: String): CallTracker = {
    this.copy(calls = calls - name)
  }

  def withConferenceEvent(evt: DeviceConference): CallTracker = {
    evt match {
      case DeviceJoinConference(
            conference,
            channel,
            _,
            _,
            maybeRemoteChannel
          ) =>
        val remoteChannels = calls
          .get(channel)
          .map(_.paths)
          .map(AsteriskGraphTracker.pathsEndChannels(_).map(_.name))
          .getOrElse(Set.empty[String])
          .concat(maybeRemoteChannel)

        val conf = conferences
          .get(evt.conference.number)
          .map(_.withChannels(Set(evt.channel) ++ remoteChannels))
          .getOrElse(
            ConferenceWithChannels(
              conference,
              Set(Set(evt.channel) ++ remoteChannels),
              evt.topic
            )
          )

        this.copy(conferences = conferences.updated(conference.number, conf))

      case DeviceLeaveConference(conference, _, _, _) =>
        conferences
          .get(evt.conference.number)
          .map(_.removeChannel(evt.channel))
          .filterNot(_.channelNames.isEmpty) match {
          case Some(c) =>
            this.copy(conferences = conferences.updated(conference.number, c))
          case _ => this.copy(conferences = conferences - conference.number)
        }
    }
  }

  def updateConference(conf: ConferenceRoom): CallTracker = {
    conferences
      .get(conf.number)
      .map(_.copy(conference = conf))
      .map(c => this.copy(conferences = conferences.updated(conf.number, c)))
      .getOrElse(this)
  }

  def withConferenceParticipantEvent(
      evt: ConferenceParticipantChange
  ): CallTracker = {
    evt match {
      case ParticipantJoinConference(numConf, part) =>
        conferences
          .get(numConf)
          .map(_.withParticipant(part)) match {
          case Some(c) =>
            this.copy(conferences = conferences.updated(numConf, c))
          case _ => this
        }

      case ParticipantLeaveConference(numConf, part) =>
        conferences
          .get(numConf)
          .map(_.removeParticipant(part)) match {
          case Some(c) =>
            this.copy(conferences = conferences.updated(numConf, c))
          case _ => this
        }

      case ParticipantUpdated(numConf, part) =>
        conferences
          .get(numConf)
          .map(_.updateParticipant(part)) match {
          case Some(c) =>
            this.copy(conferences = conferences.updated(numConf, c))
          case _ => this
        }
    }
  }
}
