package services.callhistory

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import play.api.libs.json.{JsError, JsSuccess}
import services.callhistory.CallHistoryManager.{
  QueueCallSearchF,
  SearchFunction
}
import services.config.ConfigRepository
import services.request._
import xivo.models.CallHistory
import xivo.network._
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebSocketEvent}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object CallHistoryManager {
  type SearchFunction = (HistoryParam, String) => Future[HistoryServerResponse]
  type CustomerSearchFunction =
    (Option[String], Option[String], Int) => Future[HistoryServerResponse]
  type QueueCallSearchF = (String, Int) => Future[HistoryServerResponse]
}

class CallHistoryManager @Inject() (
    ws: RecordingWS,
    configRepository: ConfigRepository
) extends Actor
    with ActorLogging {

  override def receive: Receive = {
    case BaseRequest(ref, AgentCallHistoryRequest(size, ctiUserName)) =>
      log.debug(s"Received agent call history request for user $ctiUserName")
      configRepository
        .agentFromUsername(ctiUserName)
        .foreach(agent =>
          getCallHistory(
            ref,
            HistorySize(size),
            agent.number,
            ws.getAgentCallHistory
          )
        )

    case BaseRequest(ref, CustomerCallHistoryRequestWithId(i, f)) =>
      log.debug(
        s"Received customer call history request with id $i function $f"
      )
      ws.findCustomerCallHistory(f)
        .onComplete({
          case Success(resp) => ref ! CustomerCallHistoryResponseWithId(i, resp)
          case Failure(exception) =>
            ref ! WsContent(
              WebSocketEvent.createError(WSMsgType.Error, exception.getMessage)
            )
        })

    case BaseRequest(ref, UserCallHistoryRequest(param, ctiUserName)) =>
      log.debug(s"Received user call history request for user $ctiUserName")
      configRepository
        .interfaceFromUsername(ctiUserName)
        .foreach(interface =>
          getCallHistory(ref, param, interface, ws.getUserCallHistory)
        )

    case BaseRequest(ref, UserCallHistoryRequestByDays(param, ctiUserName)) =>
      log.debug(
        s"Received user call history request by days for user $ctiUserName"
      )
      configRepository
        .interfaceFromUsername(ctiUserName)
        .foreach(interface =>
          getCallHistory(ref, param, interface, ws.getUserCallHistory)
        )

    case BaseRequest(ref, GetQueueCallHistory(queue, size)) =>
      getQueueCallHistory(ref, queue, size, ws.getQueueCallHistory)
  }

  private def getCallHistory(
      ref: ActorRef,
      param: HistoryParam,
      term: String,
      search: SearchFunction
  ): Unit = processResp(ref, search(param, term))

  private def getQueueCallHistory(
      ref: ActorRef,
      queue: String,
      size: Int,
      search: QueueCallSearchF
  ): Unit = processResp(ref, search(queue, size))

  private def processResp(
      ref: ActorRef,
      f: Future[HistoryServerResponse]
  ): Unit = {
    f.map { case HistoryServerResponse(json) =>
      CallHistory.validate(json) match {
        case JsSuccess(history, _) => ref ! history
        case JsError(e)            => log.error(s"Error decoding the json: $e")
      }
    }.failed
      .foreach { t =>
        fail(
          ref,
          s"Connection to history server failed with message ${t.getMessage}"
        )
      }
  }

  private def fail(ref: ActorRef, message: String): Unit = {
    log.error(message)
    ref ! WsContent(WebSocketEvent.createError(WSMsgType.Error, message))
  }
}
