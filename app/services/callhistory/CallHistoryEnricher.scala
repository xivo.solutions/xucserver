package services.callhistory

import org.apache.pekko.actor._
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import com.google.inject.name.Named
import org.xivo.cti.model.PhoneHintStatus
import services.ActorIds
import services.callhistory.CallHistoryEnricher.Stop
import services.config.{ConfigRepository, GetAllEntries}
import services.config.ConfigDispatcher.{MonitorPhoneHint, MonitorVideoStatus}
import services.request._
import services.video.model.VideoEvents
import services.video.model.VideoEvents.Event
import xivo.directory.PersonalContactRepository.{
  PersonalContactMap,
  PersonalContacts
}
import xivo.models.{CallDetail, CallHistory, CallStatus, RichCallHistory}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object CallHistoryEnricher {
  case object Stop

  trait Factory {
    def apply(
        @Assisted("respondTo") respondTo: ActorRef,
        @Assisted("pcRepository") pcRepository: ActorRef,
        param: HistoryParam,
        ctiUserName: String
    ): Actor
  }
}

class CallHistoryEnricher @Inject() (
    @Named(ActorIds.CallHistoryManagerId) callHistoryManager: ActorRef,
    @Assisted("respondTo") respondTo: ActorRef,
    @Assisted("pcRepository") pcRepository: ActorRef,
    @Assisted param: HistoryParam,
    @Assisted ctiUsername: String,
    configRepository: ConfigRepository,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef
) extends Actor
    with ActorLogging {

  override def preStart(): Unit = {
    param match {
      case p: HistorySize =>
        callHistoryManager ! BaseRequest(
          self,
          UserCallHistoryRequest(p, ctiUsername)
        )
      case p: HistoryDays =>
        callHistoryManager ! BaseRequest(
          self,
          UserCallHistoryRequestByDays(p, ctiUsername)
        )
    }

    pcRepository.tell(GetAllEntries, self)
  }

  override def receive: Receive = waitingForResponses() orElse timeout

  def waitingForPersonalContacts(history: CallHistory): Receive = {
    case pcMap: PersonalContacts =>
      sendResponse(history, pcMap.map)
  }

  def waitingForCallHistory(pcMap: PersonalContactMap): Receive = {
    case history: CallHistory =>
      sendResponse(history, pcMap)
  }

  def waitingForResponses(): Receive = {
    case pcMap: PersonalContacts =>
      context.become(waitingForCallHistory(pcMap.map) orElse timeout)
    case history: CallHistory =>
      context.become(waitingForPersonalContacts(history) orElse timeout)
  }

  def timeout: Receive = { case Stop =>
    shutdown()
  }

  def shutdown(): Unit = {
    context.stop(self)
  }

  def sendResponse(history: CallHistory, pcMap: PersonalContactMap): Unit = {
    timeoutTasks.cancel()
    respondTo ! enrichContact(history, pcMap)
    shutdown()
  }

  def phoneNbToUserName(repo: ConfigRepository, s: String): String =
    repo.userNameFromPhoneNb(s).getOrElse("")

  def phoneNbToPhoneStatus(repo: ConfigRepository, s: String): PhoneHintStatus =
    repo.richPhones.getOrElse(s, PhoneHintStatus.UNEXISTING)

  def phoneNbToVideoStatus(repo: ConfigRepository, s: String): Event =
    repo
      .getUserVideoStatus(repo.userNameFromPhoneNb(s).getOrElse(""))
      .getOrElse(VideoEvents.Available)

  def enrichContact(
      callHistory: CallHistory,
      pcMap: PersonalContactMap
  ): RichCallHistory = {

    def updateName(call: CallDetail, outgoing: Boolean = false): CallDetail = {
      val num = if (outgoing) call.dstNum else Some(call.srcNum)
      if (num.isDefined) {
        pcMap
          .get(num.get)
          .map(pc =>
            if (outgoing)
              call.copy(dstFirstName = pc.firstname, dstLastName = pc.lastname)
            else
              call.copy(srcFirstName = pc.firstname, srcLastName = pc.lastname)
          )
          .getOrElse(call)
      } else call
    }

    val retCallHistory = CallHistory(callHistory.calls.map(call => {
      call.status match {
        case CallStatus.Emitted => updateName(call, outgoing = true)
        case _                  => updateName(call)
      }
    }))

    val phoneNbtoMonitor = callHistory.calls
      .map(call => {
        call.status match {
          case CallStatus.Emitted => call.dstNum.getOrElse("")
          case _                  => call.srcNum
        }
      })
      .distinct
    configDispatcher ! MonitorPhoneHint(
      respondTo,
      phoneNbtoMonitor
    )
    configDispatcher ! MonitorVideoStatus(
      respondTo,
      phoneNbtoMonitor
    )

    RichCallHistory.apply(
      retCallHistory,
      phoneNbToUserName(configRepository, _),
      phoneNbToPhoneStatus(configRepository, _),
      phoneNbToVideoStatus(configRepository, _)
    )
  }

  def scheduler: Scheduler      = context.system.scheduler
  val timeoutTasks: Cancellable = scheduler.scheduleOnce(3.seconds, self, Stop)

}
