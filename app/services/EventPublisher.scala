package services

import org.apache.pekko.actor.{Actor, ActorLogging}
import com.google.inject.Inject
import services.config.ConfigRepository
import services.video.model.VideoStatusEvent
import xivo.events.PhoneEvent
import xivo.websocket.WebSocketEvent

class EventPublisher @Inject() (
    restPublisher: RestPublisher,
    eventBus: XucEventBus,
    userRepo: ConfigRepository
) extends Actor
    with ActorLogging {

  log.info("EventPublisher started " + self)

  override def preStart(): Unit =
    eventBus.subscribe(self, XucEventBus.allPhoneEventsTopic)
  eventBus.subscribe(self, XucEventBus.allVideoStatusTopic)

  def receive: PartialFunction[Any, Unit] = {

    case phoneEvent: PhoneEvent =>
      userRepo.userNameFromPhoneNb(phoneEvent.DN) foreach (restPublisher
        .publish(_, WebSocketEvent.createEvent(phoneEvent)))

    case videoEvent: VideoStatusEvent =>
      restPublisher.publish(
        videoEvent.fromUser,
        WebSocketEvent.createEvent(videoEvent)
      )

    case unknown => log.debug("Uknown message received: ")

  }
}
