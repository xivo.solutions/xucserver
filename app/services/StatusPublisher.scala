package services

import com.google.inject.Inject
import play.api.libs.ws.WSClient
import helpers.LogFutureFailure
import play.api.Logger
import play.api.libs.json.Json
import scala.concurrent.ExecutionContext
import services.config.UserPhoneStatus
import xivo.xuc.XucBaseConfig
import play.api.libs.ws.WSBodyWritables.writeableOf_JsValue

class StatusPublisher @Inject() (xucConfig: XucBaseConfig, ws: WSClient)(
    implicit ec: ExecutionContext
) {
  implicit val log: Logger = Logger(getClass.getName)

  def publishStatus(status: UserPhoneStatus): Unit = {
    val url = xucConfig.EVENT_URL

    log.debug(
      "publishing status " + status.username + " status " + status.status.getHintStatus + " to " + url
    )
    if (url != "") {
      try {
        val data = Json.obj(
          "username" -> status.username,
          "status"   -> status.status.getHintStatus()
        )
        ws.url(url)
          .post(data)
          .recoverWith(LogFutureFailure.logFailure("Unable to publish status"))
      } catch {
        case e: Throwable => log.error("Unable to send status " + e.getMessage)
      }
    } else {
      log.debug("no Url configured to publish user configuration")
    }
  }

}
