package services

import org.apache.pekko.actor.{Actor, ActorLogging, Props}
import com.google.inject.Inject
import services.XucAmiBus.{
  AmiAction,
  DialFromQueueActionRequest,
  DialToQueueActionRequest
}
import services.config.QueueRepository
import services.request.DialFromQueue
import services.request.DialToQueue
import xivo.xuc.XucBaseConfig

object QueueDispatcher {
  def props(
      queueRepository: QueueRepository,
      amiBus: XucAmiBus,
      config: XucBaseConfig
  ): Props =
    Props(new QueueDispatcher(queueRepository, amiBus, config))
}

class QueueDispatcher @Inject() (
    queueRepository: QueueRepository,
    amiBus: XucAmiBus,
    config: XucBaseConfig
) extends Actor
    with ActorLogging {
  override def receive: Receive = {

    case DialFromQueue(
          destination,
          queueId,
          callerIdName,
          callerIdNumber,
          variables,
          domain
        ) =>
      queueRepository.getQueue(queueId) match {
        case None =>
          log.warning(
            s"Ignoring DialFromQueue request for unknown queue.id=$queueId"
          )
        case Some(queueConfigUpdate) =>
          log.debug(
            s"Processing DialFromQueue($destination,$queueId,$callerIdName,$callerIdNumber,$variables,$domain) for $queueConfigUpdate"
          )
          val callerId = DialFromQueue.callerId(callerIdName, callerIdNumber)
          val msg = DialFromQueueActionRequest(
            destination,
            queueConfigUpdate.number,
            callerId,
            variables,
            config.xivoHost
          )
          amiBus.publish(
            AmiAction(msg.buildAction(), targetMds = Some("default"))
          )
      }

    case DialToQueue(
          destination,
          queueName,
          callerIdNumber,
          variables,
          _
        ) =>
      queueRepository.getQueue(queueName) match {
        case None =>
          log.warning(
            s"Ignoring DialToQueue request for unknown queue.name=$queueName"
          )
        case Some(queueConfigUpdate) =>
          log.debug(
            s"Processing DialToQueue($destination,$queueName,$callerIdNumber,$variables) for $queueConfigUpdate"
          )
          val callerId = DialToQueue.callerId(callerIdNumber)
          val msg = DialToQueueActionRequest(
            destination,
            queueConfigUpdate.number,
            callerId,
            variables,
            config.xivoHost
          )
          amiBus.publish(
            AmiAction(msg.buildAction(), targetMds = Some("default"))
          )
      }

    case other => log.warning(s"Received unknown message: $other")
  }
}
