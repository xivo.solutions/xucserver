package services.group

import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import com.google.inject.name.Named
import controllers.helpers.RequestError
import org.apache.pekko.actor.*
import org.xivo.cti.MessageFactory
import services.XucAmiBus.*
import services.config.ConfigRepository
import services.request.*
import services.{ActorIds, XucEventBus}

object GroupAction {
  trait Factory {
    def apply(ctiLink: ActorRef): Actor
  }
}

class GroupAction @Inject() (
    @Assisted ctiLink: ActorRef,
    messageFactory: MessageFactory,
    configRepo: ConfigRepository,
    eventBus: XucEventBus,
    @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef
) extends Actor
    with ActorLogging {

  def receive: PartialFunction[Any, Unit] = {
    case BaseRequest(requester, request: UserGroupActionRequest) =>
      request match
        case UserGroupPauseRequest(groupId, Some(userId)) =>
          configRepo.getGroup(groupId) match
            case Some(group) =>
              val iface = s"Local/id-$userId@usercallback"

              amiBusConnector ! GroupPauseRequest(
                UserGroupPauseActionRequest(iface, group.groupName)
              )
            case None =>
              requester ! RequestError(
                s"Group with id: ${groupId} does not exist"
              )

        case UserGroupUnpauseRequest(groupId, Some(userId)) =>
          configRepo.getGroup(groupId) match
            case Some(group) =>
              val iface = s"Local/id-$userId@usercallback"
              amiBusConnector ! GroupUnpauseRequest(
                UserGroupUnpauseActionRequest(iface, group.groupName)
              )
            case None =>
              requester ! RequestError(
                s"Group with id: ${groupId} does not exist"
              )

        case unknown => log.debug(s"unknown $unknown message received")
  }
}
