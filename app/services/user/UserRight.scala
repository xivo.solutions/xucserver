package services.user

import org.slf4j.LoggerFactory
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.*
import services.XucStatsEventBus.AggregatedStatEvent
import services.agent.AgentStatistic
import services.config.ConfigDispatcher.{
  AgentGroupList,
  AgentList,
  AgentQueueMemberList,
  QueueList
}
import services.config.{ConfigRepository, ConfigServerRequester}
import services.user.UserRight.toJson
import xivo.events.AgentState
import xivo.models.XivoObject.ObjectType
import xivo.xuc.ConfigServerConfig

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Profile {
  val Administrator = "Admin"
  val Supervisor    = "Supervisor"
  val Other         = "NoRightForUser"
}

trait Right {
  val name: String
}

case object RecordingAccess extends Right {
  override val name: String = "recording"
}

case object DissuasionAccess extends Right {
  override val name: String = "dissuasion"
}

abstract class UserRight {
  def filter(o: Any, configRepository: ConfigRepository): Option[Any]
  def profileName: String
  def rights: List[Right]
}

case class AdminRight() extends UserRight {
  override def filter(o: Any, configRepository: ConfigRepository): Option[Any] =
    Some(o)
  override def rights: List[Right] = List(RecordingAccess)
  override val profileName: String = Profile.Administrator
}

object AdminRight {
  implicit val adminRightWrites: Writes[AdminRight] = (right: AdminRight) =>
    toJson(right)
}

case class NoRightForUser() extends UserRight {
  override def filter(o: Any, configRepository: ConfigRepository): Option[Any] =
    Some(o)
  override def rights: List[Right] = List()
  override val profileName: String = Profile.Other
}

object NoRightForUser {
  implicit val noRightWrites: Writes[NoRightForUser] =
    (right: NoRightForUser) => toJson(right)
}

case class SupervisorRight(
    queueIds: List[Long],
    groupIds: List[Long],
    recordingAccess: Boolean,
    dissuasionAccess: Boolean
) extends UserRight {
  override def rights: List[Right] = {
    List(RecordingAccess).filter(_ => recordingAccess) ++
      List(DissuasionAccess).filter(_ => dissuasionAccess)
  }

  override def filter(o: Any, configRepository: ConfigRepository): Option[Any] =
    o match {
      case ql: QueueList                => filter(ql)
      case qmList: AgentQueueMemberList => filter(qmList, configRepository)
      case stat: AggregatedStatEvent    => filter(stat)
      case state: AgentState            => filter(state, configRepository)
      case stat: AgentStatistic         => filter(stat, configRepository)
      case list: AgentGroupList         => filter(list)
      case list: AgentList              => filter(list)
      case o                            => Some(o)
    }

  override val profileName: String = Profile.Supervisor

  def filter(queueList: QueueList): Option[QueueList] =
    Some(QueueList(queueList.queues.filter(q => queueIds.contains(q.id))))

  def filter(
      qmList: AgentQueueMemberList,
      configRepository: ConfigRepository
  ): Option[AgentQueueMemberList] =
    Some(
      AgentQueueMemberList(
        qmList.agentQueueMembers
          .filter(qm => queueIds.contains(qm.queueId))
          .filter(qm => hasRightsOnAgent(qm.agentId, configRepository))
      )
    )

  def filter(stat: AggregatedStatEvent): Option[AggregatedStatEvent] =
    stat.xivoObject.objectType match {
      case ObjectType.Queue =>
        if (
          stat.xivoObject.objectRef.isDefined && queueIds.contains(
            stat.xivoObject.objectRef.get
          )
        ) Some(stat)
        else None
      case _ => Some(stat)
    }

  private def hasRightsOnAgent(
      agentId: Long,
      configRepository: ConfigRepository
  ): Boolean = {
    val groupId = configRepository.groupIdForAgent(agentId)
    groupId.isDefined && groupIds.contains(groupId.get)
  }

  def filter(
      state: AgentState,
      configRepository: ConfigRepository
  ): Option[AgentState] = {
    if (hasRightsOnAgent(state.agentId, configRepository)) Some(state)
    else None
  }

  def filter(
      stat: AgentStatistic,
      configRepository: ConfigRepository
  ): Option[AgentStatistic] =
    if (hasRightsOnAgent(stat.agentId, configRepository)) Some(stat) else None

  def filter(list: AgentGroupList): Option[AgentGroupList] =
    Some(
      AgentGroupList(list.agentGroups.filter(g => groupIds.contains(g.id.get)))
    )

  def filter(list: AgentList): Option[AgentList] =
    Some(AgentList(list.agents.filter(a => groupIds.contains(a.groupId))))
}

object SupervisorRight {
  implicit val supervisorRightWrites: Writes[SupervisorRight] =
    (right: SupervisorRight) => toJson(right)
}

object UserRight {

  val logger: org.slf4j.Logger = LoggerFactory.getLogger(getClass)

  def validate(json: JsValue): JsResult[UserRight] =
    (json \ "type").asOpt[String] match {
      case None               => JsError("Missing attribute 'type'")
      case Some("admin")      => JsSuccess(AdminRight())
      case Some("supervisor") => (json \ "data").validate[SupervisorRight]
      case Some(s)            => JsError(s"Unknown right $s")
    }

  def getRights(
      username: String,
      configRequester: ConfigServerRequester,
      configServerConfig: ConfigServerConfig
  ): Future[UserRight] = {
    if (
      configServerConfig.configHost == "" || configServerConfig.configPort == 0
    ) {
      logger.info(
        s"No right server configured, giving admin rights to user $username"
      )
      Future.successful(AdminRight())
    } else
      configRequester
        .getRights(username)
        .map(response =>
          response.status match {
            case 200 =>
              UserRight.validate(response.json) match {
                case JsSuccess(right, _) =>
                  logger.info(s"Giving right $right to user $username")
                  right
                case JsError(e) =>
                  logger.error(
                    s"Unable to decode right management server answer",
                    e
                  )
                  NoRightForUser()
              }
            case 404 =>
              logger.info(
                s"User $username not found on rights server, giving NoUserRights"
              )
              NoRightForUser()
            case s =>
              logger.error(
                s"Error status $s received from right server with message ${response.body}"
              )
              NoRightForUser()
          }
        )
        .recover({ case e: Exception =>
          logger.error(s"Unable to contact right management server", e)
          NoRightForUser()
        })
  }

  implicit val supervisorReads: Reads[SupervisorRight] = (
    (JsPath \ "queueIds").read[List[Long]] and
      (JsPath \ "groupIds").read[List[Long]] and
      (JsPath \ "recordingAccess").read[Boolean] and
      (JsPath \ "dissuasionAccess").read[Boolean]
  )(SupervisorRight.apply)

  implicit val rightWrites: Writes[UserRight] =
    (o: UserRight) =>
      Json.obj("profile" -> o.profileName, "rights" -> o.rights.map(_.name))
  def toJson(right: UserRight): JsValue = Json.toJson(right)
}
