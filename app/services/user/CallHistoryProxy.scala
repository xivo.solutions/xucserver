package services.user

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef, Props}
import services.request.{
  BaseRequest,
  UserCallHistoryRequest,
  UserCallHistoryRequestByDays
}
import xivo.models.CallHistory

object CallHistoryProxy {
  def props(callHistoryManager: ActorRef): Props =
    Props(new CallHistoryProxy(callHistoryManager))
}

class CallHistoryProxy(callHistoryManager: ActorRef)
    extends Actor
    with ActorLogging {
  var requester: Option[ActorRef] = None

  override def receive: Receive = {
    case rq: UserCallHistoryRequest =>
      requester = Some(sender())
      callHistoryManager ! BaseRequest(self, rq)
    case rq: UserCallHistoryRequestByDays =>
      requester = Some(sender())
      callHistoryManager ! BaseRequest(self, rq)
    case history: CallHistory => requester.foreach(_ ! history)
  }
}
