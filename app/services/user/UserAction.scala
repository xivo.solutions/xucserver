package services.user

import org.apache.pekko.actor.ActorRef
import org.xivo.cti.MessageFactory
import play.api.Logger
import services.config.ConfigServerRequester
import services.request._
import xivo.models.{PartialUserServices, UserForward}

class CtiUserAction(
    ctiLink: ActorRef,
    messageFactory: MessageFactory = new MessageFactory(),
    configServerRequester: ConfigServerRequester
) {
  val log: Logger = Logger(getClass.getName)

  private def forward(request: ForwardRequest, userId: Long) = {
    request match {
      case UncForward(destination, state) =>
        configServerRequester.setUserServices(
          userId,
          PartialUserServices(
            None,
            None,
            None,
            Some(UserForward(state, destination))
          )
        )
      case NaForward(destination, state) =>
        configServerRequester.setUserServices(
          userId,
          PartialUserServices(
            None,
            None,
            Some(UserForward(state, destination)),
            None
          )
        )
      case BusyForward(destination, state) =>
        configServerRequester.setUserServices(
          userId,
          PartialUserServices(
            None,
            Some(UserForward(state, destination)),
            None,
            None
          )
        )
      case ukn => log.error(s"Invalid forward action $ukn received")
    }

  }

  def doUserAction(request: UserActionRequest, userId: Long): Any =
    request match {

      case request: ForwardRequest => forward(request, userId)

      case UserStatusUpdateReq(Some(userid), status) =>
        ctiLink ! messageFactory.createUserAvailState(userid.toString, status)

      case DndReq(state, _) =>
        configServerRequester.setUserServices(
          userId,
          PartialUserServices(Some(state), None, None, None)
        )

      case ukn => log.error(s"Invalid user action $ukn received")

    }
}
