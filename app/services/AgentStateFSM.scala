package services

import org.apache.pekko.actor._
import org.joda.time.DateTime
import services.AgentStateFSM._
import services.agent.{AgentCall, AgentStateProdTHandler, AgentStateTHandler}
import services.calltracking.DeviceCalls
import services.config.ConfigRepository
import xivo.events.AgentState
import xivo.models.Agent

object AgentStateFSM {
  def props(
      agentId: Agent.Id,
      agentNumber: Agent.Number,
      configRepository: ConfigRepository,
      devicesTracker: ActorRef,
      xucEventBus: XucEventBus
  ): Props =
    Props(
      new AgentStateFSM(
        agentId,
        agentNumber: Agent.Number,
        xucEventBus,
        configRepository,
        devicesTracker
      ) with AgentStateProdTHandler
    )

  case class GetLastReceivedState(requester: ActorRef)

  sealed trait MContext {
    def lastChanged: DateTime
    def refreshTimestamp: MContext
  }

  case class AgentStateEmptyContext(lastChanged: DateTime = DateTime.now())
      extends MContext {
    def refreshTimestamp: AgentStateEmptyContext = {
      this.copy(lastChanged = DateTime.now)
    }
  }
  case class AgentStateContext(
      pauseReason: Option[String] = None,
      phoneNumber: Option[String],
      currentCall: Option[AgentCall] = None,
      queues: List[Int] = List.empty,
      lastChanged: DateTime = DateTime.now()
  ) extends MContext {

    def refreshTimestamp: AgentStateContext = {
      this.copy(lastChanged = DateTime.now)
    }
  }

  sealed trait MAgentStates {
    def isPause: Boolean = false
  }
  case object MAgentReady extends MAgentStates
  case object MAgentPaused extends MAgentStates {
    override def isPause = true
  }
  case object MAgentLoggedOut extends MAgentStates
  case object MAgentOnWrapup extends MAgentStates {
    override def isPause = true
  }

}
trait AgentStateCanGo extends FSM[MAgentStates, MContext] {
  this: AgentStateTHandler =>
  import xivo.events._

  def agentNumber: Agent.Number

  def canGoToReady: StateFunction = {
    case Event(e: EventAgentLogin, _) =>
      goto(MAgentReady) using AgentStateContext(None, Some(e.phoneNb))
    case Event(e: EventAgentUnPause, context) =>
      goto(MAgentReady) using context.refreshTimestamp
  }

  def canGoToPause: StateFunction = {
    case Event(e: EventAgentPause, context: AgentStateContext) =>
      val newContext = context.copy(pauseReason = e.reason)
      goto(MAgentPaused) using newContext.refreshTimestamp
  }

  def canGoToLoggout: StateFunction = {
    case Event(
          EventAgentLogout(agentId, agentNb),
          context: AgentStateContext
        ) =>
      log.debug("Logging out agent (id: $agentId, nb: $agentNb) from $context")
      goto(MAgentLoggedOut) using AgentStateContext(
        None,
        context.phoneNumber,
        None,
        List.empty,
        DateTime.now()
      )
    case Event(
          EventAgentLogout(agentId, agentNb),
          _
        ) =>
      log.warning(
        "Logging out agent (id: $agentId, nb: $agentNb) from strange $context"
      )
      goto(MAgentLoggedOut) using AgentStateContext(
        None,
        Some(agentNb),
        None,
        List.empty,
        DateTime.now()
      )

  }

  def canGoToWrapup: StateFunction = {
    case Event(e: EventAgentWrapup, context) =>
      goto(MAgentOnWrapup) using context.refreshTimestamp
  }

  def processContextEvent: StateFunction = {

    case Event(DeviceCalls(_, newCalls), context: AgentStateContext) =>
      val agentCall = AgentCall
        .currentAgentCall(
          AgentCall.callListToAgentCallList(newCalls.values.toList)
        )
        .map(call => {
          if (context.currentCall.map(_.acd).contains(true) && !call.acd) {
            call.copy(acd = true)
          } else {
            call
          }
        })
      val newContext = context.copy(currentCall = agentCall)
      if (newContext != context) {
        val newContextWithTs = newContext.refreshTimestamp
        publishTransition(stateName, context, stateName, newContextWithTs)
        publishState(stateName, newContextWithTs)
        stay() using newContextWithTs
      } else {
        stay() using newContext
      }

    case Event(AgentQueues(_, newQueues), context: AgentStateContext) =>
      val newContext = context.copy(queues = newQueues)
      if (newContext != context) {
        publishTransition(stateName, context, stateName, newContext)
        publishState(stateName, newContext)
      }
      stay() using newContext
  }

}

class AgentStateFSM(
    val agentId: Agent.Id,
    val agentNumber: Agent.Number,
    val eventBus: XucEventBus,
    val configRepository: ConfigRepository,
    val devicesTracker: ActorRef
) extends Actor
    with AgentStateCanGo
    with LoggingFSM[MAgentStates, MContext] {
  this: AgentStateTHandler =>

  var previousAgentStateEvent: Option[AgentState] = None

  log.info(s"Starting AgentStateFSM $self")

  startWith(MAgentLoggedOut, AgentStateEmptyContext())
  publishState(MAgentLoggedOut, AgentStateEmptyContext())

  when(MAgentLoggedOut)(processContextEvent orElse canGoToReady)

  when(MAgentReady)(
    processContextEvent orElse canGoToLoggout orElse canGoToPause orElse canGoToWrapup
  )

  when(MAgentPaused)(
    processContextEvent orElse canGoToLoggout orElse canGoToReady orElse canGoToPause orElse canGoToWrapup
  )

  when(MAgentOnWrapup)(
    processContextEvent orElse canGoToLoggout orElse canGoToReady orElse canGoToPause
  )

  onTransition {
    case MAgentLoggedOut -> MAgentReady => whenLoggingIn(nextStateData)
    case _ -> MAgentLoggedOut           => whenLoggingOut(stateData)
  }

  onTransition { case from -> to =>
    publishState(to, nextStateData)
    publishTransition(from, stateData, to, nextStateData)
  }

  initialize()

}
