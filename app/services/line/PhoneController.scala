package services.line

import org.apache.pekko.actor.*
import com.google.inject.Inject
import com.google.inject.assistedinject.Assisted
import com.google.inject.name.Named
import models.XivoUser
import org.apache.pekko.util.Timeout
import org.asteriskjava.live.HangupCause
import org.asteriskjava.manager.action.{HangupAction, SetVarAction}
import services.XucAmiBus.AmiAction
import services.calltracking.DeviceConferenceAction.*
import services.calltracking.RetrieveUtil.RetrieveQueueCallContext
import services.calltracking.SingleDeviceTracker.{
  DeviceConferenceMessage,
  IncludeToConfGetCalls,
  SendCurrentCallsPhoneEvents
}
import services.calltracking.{DeviceCall, DeviceCalls, SingleDeviceTracker, *}
import services.config.ConfigDispatcher.*
import services.config.{
  AgentOnPhone,
  ConfigRepository,
  ObjectType,
  OutboundQueue
}
import services.request.PhoneRequest.*
import services.request.{
  MonitorPause,
  MonitorUnpause,
  PhoneRequest,
  UserBaseRequest
}
import services.{ActorIds, PhoneNumberSanitizer, XucAmiBus, XucEventBus}
import xivo.events.AgentState
import xivo.events.AgentState.{AgentLoggedOut, AgentReady}
import xivo.models.{AgentQueueMember, Line, QueueConfigUpdate}
import xivo.phonedevices.{
  DeviceAdapter,
  DeviceAdapterFactory,
  IncludeToConfCommand,
  SetDataCommand,
  SetVarCommand,
  SetVars
}
import xivo.xuc.{RemoteConferenceExtension, TransferConfig, XucConfig}
import xivo.xucami.models.{Channel, ChannelState}

import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global
import org.apache.pekko.pattern.ask
import services.line.PhoneController.{
  IncludeToConfResponse,
  IncludeToConfWrongPreconditions
}

import scala.concurrent.{ExecutionContextExecutor, Future}

object PhoneController {
  case object Start

  final case class IncludeToConfResponse(
      conf: ConferenceRoom,
      chanToInclude: String,
      remoteChannelMds: String,
      callToHold: DeviceCall,
      evt: IncludeToConference,
      ctiRouterRef: ActorRef
  )

  case class IncludeToConfWrongPreconditions(
      calls: List[String],
      conferences: List[String]
  ) {
    private def listEntities[T](
        name: String,
        entities: List[String]
    ): String = {
      if (entities.isEmpty) s"No ${name}s found."
      else
        s"${name}s:\n${entities
            .map { id => s" - $name ID: $id" }
            .mkString("\n")}"
    }

    def detailedPreconditionsMessage: String = {
      List(
        "Call tracker state:",
        s"Number of conferences: ${conferences.size}",
        s"Number of calls: ${calls.size}",
        "",
        listEntities("Conference", conferences),
        "",
        listEntities("Call", calls)
      ).mkString("\n")
    }
  }

  trait Factory {
    def apply(phoneNb: String, line: Line, xivoUser: XivoUser): Actor
  }
}

class PhoneController @Inject() (
    configRepository: ConfigRepository,
    bus: XucEventBus,
    amiBus: XucAmiBus,
    transferUtil: TransferUtil,
    recordingActionController: RecordingActionController,
    config: XucConfig,
    deviceAdapterFactory: DeviceAdapterFactory,
    retrieveUtil: RetrieveUtil,
    @Named(ActorIds.DevicesTrackerId) devicesTracker: ActorRef,
    @Named(ActorIds.ChannelTrackerId) channelTracker: ActorRef,
    @Named(ActorIds.AsteriskGraphTrackerId) asteriskGraphTracker: ActorRef,
    @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    @Assisted phoneNb: String,
    @Assisted line: Line,
    @Assisted xivoUser: XivoUser
) extends Actor
    with ActorLogging {

  val deviceAdapter: DeviceAdapter =
    deviceAdapterFactory.getAdapter(line, phoneNb)
  var agentId: Option[Long]            = None
  var agentNum: Option[String]         = None
  var queue: Option[QueueConfigUpdate] = None
  var enableOutboundDial               = false

  var calls: Map[String, DeviceCall] = Map.empty
  private var onCallsCallbacks: List[Map[String, DeviceCall] => Unit] =
    List.empty
  def addOnCallsCallbacks(
      callback: Map[String, DeviceCall] => Unit
  ): Unit =
    log.debug(
      s"Pushing one callback to callback map of ${onCallsCallbacks.length} element(s)"
    )
    onCallsCallbacks = onCallsCallbacks :+ callback

  override def preStart(): Unit = {
    devicesTracker ! SingleDeviceTracker.MonitorCalls(line.interface)
    xivoUser.mobile_phone_number.foreach(mobile =>
      devicesTracker ! SipDeviceTracker.WatchOutboundCallTo(
        line.interface,
        mobile
      )
    )
  }

  override def postStop(): Unit = {
    bus.unsubscribe(self)
    devicesTracker ! SingleDeviceTracker.UnMonitorCalls(line.interface)
  }

  private def chanVars(
      chan: String,
      vars: List[(String, String)]
  ): List[SetVarCommand] =
    vars.map(variable => SetVarCommand(chan, variable._1, variable._2))

  private def getCallFromUniqueId(uniqueId: String): Option[DeviceCall] = {
    calls.values.find(call =>
      call.channel.exists(channel => channel.id == uniqueId)
    )
  }

  private def buildHangupActions(uniqueId: String): List[AmiAction] = {
    val maybeCall = getCallFromUniqueId(uniqueId)

    maybeCall
      .map(call => {
        val setVar = new SetVarAction()
        setVar.setChannel(call.channelName)
        setVar.setVariable("CHANNEL(hangupsource)")
        setVar.setValue(call.channelName)
        val action = new HangupAction()
        action.setChannel(call.channelName)
        action.setCause(HangupCause.AST_CAUSE_NORMAL_CLEARING.getCode)
        val mds = call.channel map { _.mdsName }
        List(
          AmiAction(setVar, Some(uniqueId), targetMds = mds),
          AmiAction(action, Some(uniqueId), targetMds = mds)
        )
      })
      .getOrElse(List())
  }

  private def printCallsPathsAndRemoteChannels(): Unit = {
    calls.foreach((chanName, deviceCall) =>
      log.debug(
        s"""
           |Call $chanName contains nodes :
           |
           |${deviceCall.paths.mkString("\n")}
           |
           |And remote Channels : ${deviceCall.remoteChannels.keys}"
           """.stripMargin
      )
    )
  }

  private def includeChannelInConference(
      conference: ConferenceRoom,
      channelName: String,
      channelMds: String,
      evt: IncludeToConference
  ): Unit = {

    def setCallerId(callerId: String): Unit = {
      SetVars(
        amiBusConnector,
        chanVars(
          channelName,
          List(
            ("XIVO_ORIG_CID_NAME", callerId)
          )
        )
      ).send()
    }

    def includeLocalChannelToConf(
        confOptions: String,
        confPin: String,
        confRole: String
    ): Unit = {
      log.debug(
        s"Conference ${conference.name} and channel $channelName are on the same host, sending include command directly on the channel"
      )

      SetVars(
        amiBusConnector,
        chanVars(
          channelName,
          List(
            ("XIVO_MEETMEOPTIONS", confOptions),
            (Channel.VarNames.ConferencePinVariable, confPin),
            (Channel.VarNames.ConferenceRoleVariable, confRole)
          )
        )
      ).send()

      amiBusConnector ! IncludeToConfCommand(
        channelName,
        "xivo-join-conf",
        s"conf-${conference.number}",
        1
      )
    }

    def includeXdsChannelToConf(
        confOptions: String,
        confPin: String,
        confAdmin: String
    ): Unit = {
      log.debug(
        s"Sending includeToConf command to mds ${conference.mds}"
      )
      SetVars(
        amiBusConnector,
        chanVars(
          channelName,
          List(
            ("CHANVAR_ALREADY_SET", "TRUE"),
            ("XIVO_PRESUBR_GLOBAL_NAME", "XDS"),
            ("XDS_PEER", conference.mds),
            ("XDS_CONTEXT", "xivo-join-conf"),
            ("XDS_EXTEN", s"conf-${conference.number}"),
            ("XDS_SUBR_ARG1", confPin),
            ("XDS_SUBR_ARG2", confOptions),
            ("XDS_SUBR_ARG3", confAdmin)
          )
        )
      ).send()

      amiBusConnector ! IncludeToConfCommand(
        channelName,
        "xds-to-mds",
        s"routing",
        1
      )
    }

    def includeClusterChannelToConf(
        remoteExtension: RemoteConferenceExtension,
        confOptions: String,
        confPin: String,
        confRole: String
    ): Unit = {
      remoteExtension.SipDomain.foreach(sipDomain =>
        amiBusConnector ! SetVarCommand(
          channelName,
          "XIVO_SIPDOMAIN",
          sipDomain
        )
      )

      SetVars(
        amiBusConnector,
        chanVars(
          channelName,
          List(
            ("PUSH(_XIVO_HEADERS_ARR)", s"X_MEETME_NAME|${conference.number}"),
            ("PUSH(_XIVO_HEADERS_ARR)", s"X_MEETME_OPTIONS|$confOptions"),
            ("PUSH(_XIVO_HEADERS_ARR)", s"X_MEETME_PIN|$confPin"),
            ("PUSH(_XIVO_HEADERS_ARR)", s"X_MEETME_ROLE|$confRole")
          )
        )
      ).send()

      amiBusConnector ! IncludeToConfCommand(
        channelName,
        remoteExtension.context,
        remoteExtension.extension,
        1
      )
    }

    log.debug(
      s"Including channel $channelName on $channelMds in conference ${conference.number} on ${conference.mds}"
    )
    val pin = (if (evt.isAdmin) conference.adminPin else conference.userPin)
      .getOrElse("")
    val role = if (evt.isAdmin) "ADMIN" else "USER"
    val options = getMeetmeOptions(
      evt.isAdmin,
      true,
      evt.marked,
      evt.leaveWhenLastMarkedLeave
    )

    setCallerId(evt.callerId.getOrElse(""))
    if (config.getNodeHostAndPort != conference.nodeHost) {
      val maybeRemoteConfExten =
        config.getConfExtensionPerRemoteNode
          .find(
            _._1.contains(conference.nodeHost)
          )
          .map(_._2)
      maybeRemoteConfExten.foreach(rmtConfExt =>
        includeClusterChannelToConf(
          rmtConfExt,
          options,
          pin,
          role
        )
      )
    } else if (channelMds.equals(conference.mds)) {
      includeLocalChannelToConf(options, pin, role)
    } else {
      includeXdsChannelToConf(options, pin, role)
    }
  }

  private def unholdConference(
      sender: ActorRef,
      conferenceCall: DeviceCall
  ): Unit = {
    conferenceCall.callState.foreach(state =>
      if (state == ChannelState.HOLD) {
        log.debug(s"Unholding conference ${conferenceCall.channelName}")
        deviceAdapter.hold(Some(conferenceCall), sender)
      }
    )
  }

  private def setIncludeCallback(
      senderRef: ActorRef,
      conferenceCall: DeviceCall
  ): Unit = {
    addOnCallsCallbacks((calls: Map[String, DeviceCall]) =>
      unholdConference(senderRef, conferenceCall)
    )
  }

  override def receive: Receive = mainReceive orElse doRecordingAction

  def mainReceive: Receive = {
    case PhoneController.Start =>
      bus.subscribe(self, XucEventBus.allAgentsEventsTopic)
      configDispatcher ! RequestConfig(self, GetAgentOnPhone(phoneNb))

    case DeviceCalls(_, newCalls) =>
      calls = newCalls
      if (onCallsCallbacks.nonEmpty) {
        log.debug(
          s"Executing ${onCallsCallbacks.size} callback(s) on DeviceCalls updated"
        )
        onCallsCallbacks.foreach(callback => callback(calls))
        onCallsCallbacks = List.empty
        log.debug(s"Emptying callback map")
      }

    case AgentOnPhone(agent, _) =>
      agentId = Some(agent)
      agentNum = agentId.flatMap(configRepository.getAgent).map(_.number)
      configDispatcher ! RequestConfig(self, GetQueuesForAgent(agent))
      configDispatcher ! RequestStatus(self, agent.toInt, ObjectType.TypeAgent)

    case QueuesForAgent(queues, _) =>
      configDispatcher ! RequestConfig(
        self,
        OutboundQueueQuery(queues.map(_.queueId))
      )

    case OutboundQueue(q) =>
      log.info(s"Using queue ${q.name} (${q.number} for outbound dialing")
      queue = Some(q)

    case AgentLoggedOut(_, _, number, _, _, _) if number == phoneNb =>
      agentId = None
      queue = None
      bus.unsubscribe(self, XucEventBus.configTopic(ObjectType.TypeQueueMember))

    case AgentReady(id, _, number, queueIds, _, _) if number == phoneNb =>
      agentId = Some(id)
      enableOutboundDial = true
      bus.subscribe(self, XucEventBus.configTopic(ObjectType.TypeQueueMember))
      configDispatcher ! RequestConfig(
        self,
        OutboundQueueQuery(queueIds.map(_.toLong))
      )

    case s: AgentState if s.phoneNb == phoneNb => enableOutboundDial = false

    case AgentQueueMember(agId, qId, penalty) if agentId.contains(agId) =>
      if (queue.isEmpty && penalty >= 0) {
        configDispatcher ! RequestConfig(
          self,
          OutboundQueueQuery(List(qId.toLong))
        )
      } else if (queue.map(_.id).contains(qId) && penalty < 0) {
        queue = None
      }

    case UserBaseRequest(_, rq: PhoneRequest, xucUser) =>
      rq match {
        case Dial(destination, variables, domain) =>
          log.debug("websocket command Dial {}", destination)
          val sanitizedNumber = PhoneNumberSanitizer.sanitize(destination)
          log.debug(
            s"Dial: destination number sanitized from {} to {}",
            destination,
            sanitizedNumber
          )
          if (agentId.isDefined && queue.isDefined && enableOutboundDial)
            configRepository
              .getLineUser(line.id)
              .map(_.id)
              .foreach(xivoUserId =>
                deviceAdapter.odial(
                  sanitizedNumber,
                  agentId.get,
                  queue.map(_.number).get,
                  variables,
                  xivoUserId,
                  sender(),
                  line.driver,
                  line.dev.map(_.vendor)
                )
              )
          else
            deviceAdapter.dial(sanitizedNumber, variables, sender())

        case DialFromMobile(destination, variables) =>
          log.debug("websocket command DialFromMobile {}", destination)
          xivoUser.mobile_phone_number match {
            case None | Some("") =>
              log.info(
                "{} has empty mobile number, aborting DialFromMobile",
                xucUser
              )
            case Some(mobileNumber) =>
              val destinationSanitized =
                PhoneNumberSanitizer.sanitize(destination)
              val mobileNumberSanitized =
                PhoneNumberSanitizer.sanitize(mobileNumber)
              log.debug(
                s"DialFromMobile sanitization: destination {} -> {}, mobile {} -> {}",
                destination,
                destinationSanitized,
                mobileNumber,
                mobileNumberSanitized
              )
              val callerName = xivoUser.lastname
                .map(xivoUser.firstname + " " + _)
                .getOrElse(xivoUser.firstname)
              deviceAdapter.dialFromMobile(
                mobileNumberSanitized,
                destinationSanitized,
                phoneNb,
                callerName,
                variables,
                sender(),
                xivoUser.id
              )
          }

        case DialByUsername(username, variables, domain) =>
          configRepository.phoneNumberForUser(username) match {
            case Some(number) =>
              self.forward(
                UserBaseRequest(sender(), Dial(number, variables), xucUser)
              )
            case None =>
              log.warning(
                s"Dial number not found for username $username, aborting DialByUsername"
              )
          }

        case ListenCallbackMessage(voiceMsgRef, variables) =>
          log.debug("websocket command ListenCallbackMessage {}", voiceMsgRef)
          deviceAdapter.listenCallbackMessage(voiceMsgRef, variables, sender())

        case Hangup(uniqueId: Option[String]) =>
          uniqueId.fold(deviceAdapter.hangup(sender()))(id => {
            val actions = buildHangupActions(id)
            if (actions.isEmpty) {
              log.warning(
                s"No channel $uniqueId found to hangup, call legacy implementation"
              )
              deviceAdapter.hangup(sender())
            } else {
              actions.map(amiBus.publish)
            }
          })

        case ToggleMicrophone(uniqueId: Option[String]) =>
          deviceAdapter.toggleMicrophone(
            uniqueId.flatMap(getCallFromUniqueId),
            sender()
          )

        case AttendedTransfer(destination, device) =>
          if (config.enableAmiTransfer) {
            log.debug(
              s"AttendedTransfer (amitransfer) starting attended transfer to $destination: trying to find user for line name:${line.name} id:${line.id}"
            )
            configRepository
              .getLineUser(line.id)
              .foreach(xivoUser => {
                implicit val ec: ExecutionContextExecutor = context.dispatcher
                transferUtil
                  .attendedTransfer(
                    calls,
                    destination,
                    device,
                    line,
                    xivoUser,
                    deviceAdapter,
                    sender()
                  )(context)
                  .failed
                  .foreach(t => log.error(t, "error while transferring"))
              })
          } else {
            log.debug(
              s"AttendedTransfer (deviceadapter) starting attended transfer to $destination: from line name:${line.name} id:${line.id}"
            )
            deviceAdapter.attendedTransfer(destination, sender())
          }

        case CompleteTransfer =>
          if (config.enableAmiTransfer) {
            transferUtil
              .getChannelsForTransfer(calls)
              .foreach(c => {
                log.debug(s"CompleteTransfer for $c")
                implicit val ec: ExecutionContextExecutor = context.dispatcher
                transferUtil
                  .completeTransfer(c, agentNum = agentNum)(context)
                  .failed
                  .foreach(t =>
                    log.error(t, s"Got error while transferring $c")
                  )
              })
          } else {
            deviceAdapter.completeTransfer(sender())
          }

        case CancelTransfer => deviceAdapter.cancelTransfer(sender())

        case DirectTransfer(destination) =>
          deviceAdapter.directTransfer(destination, sender())

        case Conference => deviceAdapter.conference(sender())

        case evt: IncludeToConference =>
          val ctiRouterRef = sender()
          devicesTracker ! IncludeToConfGetCalls(
            line.interface,
            self,
            evt,
            ctiRouterRef
          )

        case ConferenceInvite(
              numConf,
              exten,
              role,
              earlyJoin,
              variables,
              marked,
              leaveWhenLastMarkedLeave,
              callerId
            ) =>
          val maybeUser = configRepository
            .getLineForPhoneNb(exten)
            .flatMap(lineId => configRepository.getLineUser(lineId))

          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Invite(
              numConf,
              exten,
              role,
              earlyJoin,
              variables,
              marked,
              leaveWhenLastMarkedLeave,
              callerId,
              maybeUser
            )
          )

        case ConferenceMute(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Mute(numConf, index)
          )

        case ConferenceUnmute(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Unmute(numConf, index)
          )

        case ConferenceMuteAll(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            MuteAll(numConf)
          )

        case ConferenceUnmuteAll(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            UnmuteAll(numConf)
          )

        case ConferenceMuteMe(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            MuteMe(numConf)
          )

        case ConferenceUnmuteMe(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            UnmuteMe(numConf)
          )

        case ConferenceKick(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Kick(numConf, index)
          )

        case ConferenceClose(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Close(numConf)
          )

        case ConferenceDeafen(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Deafen(numConf, index)
          )

        case ConferenceUndeafen(numConf, index) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Undeafen(numConf, index)
          )

        case ConferenceReset(numConf) =>
          devicesTracker ! DeviceConferenceMessage(
            line.interface,
            Reset(numConf)
          )

        case Hold(uniqueId: Option[String]) =>
          deviceAdapter.hold(uniqueId.flatMap(getCallFromUniqueId), sender())

        case Answer(uniqueId: Option[String]) =>
          deviceAdapter.answer(uniqueId.flatMap(getCallFromUniqueId), sender())

        case SetData(variables) => sender() ! SetDataCommand(phoneNb, variables)

        case GetCurrentCallsPhoneEvents =>
          devicesTracker ! SendCurrentCallsPhoneEvents(line.interface)

        case s: SendDtmfRequest => deviceAdapter.sendDtmf(s.key, sender())

        case RetrieveQueueCall(queueCall, variables) =>
          log.debug("websocket command RetrieveQueueCall {}", queueCall.channel)
          implicit val ec: ExecutionContextExecutor = context.dispatcher
          val agentNum =
            agentId.flatMap(configRepository.getAgent).map(_.number)
          val agentName = xivoUser.lastname
            .map(xivoUser.firstname + " " + _)
            .getOrElse(xivoUser.firstname)

          retrieveUtil.processRetrieve(
            calls.values.toList,
            RetrieveQueueCallContext(
              line,
              queueCall,
              variables,
              agentNum,
              agentName
            ),
            deviceAdapter,
            sender()
          )

        case _ => log.error("Unsupported phone request received {}", rq)
      }

    case IncludeToConfResponse(
          conf,
          chanToInclude,
          remoteChannelMds,
          callToHold,
          evt,
          ctiRouterRef
        ) =>
      setIncludeCallback(ctiRouterRef, callToHold)
      includeChannelInConference(
        conf,
        chanToInclude,
        remoteChannelMds,
        evt
      )

    case a: IncludeToConfWrongPreconditions =>
      log.error(
        s"Include to conference failed due to wrong preconditions:\n${a.detailedPreconditionsMessage}"
      )

    case e: DeviceConferenceCommandErrorType =>
      context.parent ! e
  }

  def doRecordingAction: Receive = {

    case mp: MonitorPause =>
      log.debug("Received monitor pause {}", calls)
      recordingActionController
        .getMonitoredChannel(calls)
        .foreach(channel => {
          log.debug("Found channel to pause recording {}", channel)
          recordingActionController.pauseRecording(channel)
        })

    case mup: MonitorUnpause =>
      log.debug("Received monitor pause {}", calls)
      recordingActionController
        .getMonitoredChannelOnPause(calls)
        .foreach(channel => {
          log.debug("Found channel to un pause recording {}", channel)
          recordingActionController.unPauseRecording(channel)
        })

  }
}
