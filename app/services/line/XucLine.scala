package services.line

object LineState extends Enumeration(-2) {
  type LineState = Value
  val UNITIALIZED, HUNGUP, DOWN, RSRVD, OFFHOOK, DIALING, ORIGINATING, RINGING,
      UP, BUSY, DIALING_OFFHOOK, PRERING, HOLD = Value
}

object CallLegDirection extends Enumeration {
  type CallLegDirection = Value
  val UNKNOWN, ORIGINATING, TERMINATING = Value
}
