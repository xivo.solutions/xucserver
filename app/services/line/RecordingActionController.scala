package services.line

import com.google.inject.{ImplementedBy, Inject}
import play.api.Logger
import services.XucAmiBus
import services.calltracking.DeviceCall
import services.channel.ActionBuilder
import xivo.xucami.models.Channel.VarNames.XucRecordUnpaused
import xivo.xucami.models.MonitorState.MonitorState
import xivo.xucami.models.{Channel, MonitorState}

@ImplementedBy(classOf[RecordingActionControllerImpl])
trait RecordingActionController {
  def getMonitoredChannel(calls: Map[String, DeviceCall]): Option[Channel]

  def getMonitoredChannelOnPause(
      calls: Map[String, DeviceCall]
  ): Option[Channel]

  def unPauseRecording(recordedChannel: Channel): Unit

  def pauseRecording(recordedChannel: Channel): Unit

}

class RecordingActionControllerImpl @Inject() (amiBus: XucAmiBus)
    extends RecordingActionController
    with ActionBuilder {
  val log: Logger = Logger(getClass.getName)

  def findChannel(
      calls: Map[String, DeviceCall],
      mstate: MonitorState
  ): Option[Channel] =
    calls.values.flatMap(_.monitoredChannels).find(_.monitored == mstate)

  override def getMonitoredChannel(
      calls: Map[String, DeviceCall]
  ): Option[Channel] = findChannel(calls, MonitorState.ACTIVE)

  override def getMonitoredChannelOnPause(
      calls: Map[String, DeviceCall]
  ): Option[Channel] = findChannel(calls, MonitorState.PAUSED)

  override def pauseRecording(recordedChannel: Channel): Unit =
    log.debug(s"pause recording for channel $recordedChannel")
    amiBus.publish(
      pauseMixMonitorAction(recordedChannel.name, recordedChannel.id)
    )

  override def unPauseRecording(recordedChannel: Channel): Unit =
    log.debug(s"unpause recording for channel $recordedChannel")
    recordedChannel.variables.getOrElse("__MONITOR_PAUSED", "") match
      case "true" =>
        recordedChannel.variables.getOrElse("XUC_RECORD_UNPAUSED", "") match
          case "true" =>
            amiBus.publish(
              unpauseMixMonitorAction(recordedChannel.name, recordedChannel.id)
            )
          case _ =>
            recordedChannel.variables.getOrElse("recordingid", "") match
              case "" =>
                log.error(
                  s"recordingid not found in channel, cannot enable recording for $recordedChannel"
                )
              case recordingId: String =>
                amiBus.publish(
                  setVarAction(
                    channelName = recordedChannel.name,
                    key = XucRecordUnpaused,
                    value = "true",
                    channelId = recordedChannel.id
                  )
                )
                amiBus.publish(
                  startMixMonitorAction(
                    channelName = recordedChannel.name,
                    channelId = recordedChannel.id,
                    recordingId = recordingId
                  )
                )
      case _ =>
        amiBus.publish(
          unpauseMixMonitorAction(recordedChannel.name, recordedChannel.id)
        )
}
