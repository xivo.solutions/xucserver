package xivo.events

import org.joda.time.{DateTime, Seconds}
import play.api.libs.json._
import xivo.models.Agent
import xivo.models.Agent.Id
import xivo.xucami.models.MonitorState
import xivo.xucami.models.MonitorState.MonitorState

abstract class AgentState(
    val name: String,
    val agentId: Agent.Id,
    val changed: DateTime,
    val phoneNb: String,
    val queues: List[Int],
    val cause: Option[String] = Some(""),
    val agentNb: Agent.Number
) {
  def getSince: Long =
    Seconds.secondsBetween(changed, new DateTime()).getSeconds

}

object AgentState {

  private def commonJson(agentState: AgentState): JsObject = {
    val json = Json.obj(
      "name"    -> agentState.name,
      "agentId" -> agentState.agentId,
      "phoneNb" -> agentState.phoneNb,
      "since"   -> agentState.getSince,
      "queues"  -> agentState.queues
    )

    agentState.cause match {
      case Some(cause) =>
        json.++(Json.obj("cause" -> cause))
      case None =>
        json
    }
  }

  val agentOnCallWrites: Writes[AgentOnCall] = new Writes[AgentOnCall] {
    def writes(c: AgentOnCall): JsValue = {
      commonJson(c).++(
        Json.obj(
          "acd"          -> c.acd,
          "direction"    -> c.direction.toString,
          "monitorState" -> c.monitorState.toString
        )
      )
    }
  }

  val agentStateCommonWrites: Writes[AgentState] = new Writes[AgentState] {
    def writes(c: AgentState): JsValue = {
      commonJson(c)
    }
  }

  implicit val agentStateWrites: Writes[AgentState] =
    new Writes[AgentState] {
      def writes(c: AgentState): JsValue =
        c match {
          case aoc: AgentOnCall => agentOnCallWrites.writes(aoc)
          case other            => agentStateCommonWrites.writes(other)
        }

    }

  import xivo.events.CallDirection._

  case class AgentReady(
      id: Agent.Id,
      override val changed: DateTime,
      override val phoneNb: String,
      override val queues: List[Int],
      override val cause: Option[String] = Some(""),
      override val agentNb: Agent.Number
  ) extends AgentState(
        "AgentReady",
        id,
        changed,
        phoneNb,
        queues,
        cause,
        agentNb
      )

  case class AgentDialing(
      id: Agent.Id,
      override val changed: DateTime,
      override val phoneNb: String,
      override val queues: List[Int],
      override val cause: Option[String] = Some(""),
      override val agentNb: Agent.Number
  ) extends AgentState(
        "AgentDialing",
        id,
        changed,
        phoneNb,
        queues,
        cause,
        agentNb
      )

  case class AgentOnCall(
      id: Agent.Id,
      override val changed: DateTime,
      acd: Boolean,
      val direction: CallDirection,
      override val phoneNb: String,
      override val queues: List[Int],
      val onPause: Boolean,
      override val cause: Option[String] = Some(""),
      val monitorState: MonitorState = MonitorState.UNKNOWN,
      override val agentNb: Agent.Number
  ) extends AgentState(
        "AgentOnCall",
        id,
        changed,
        phoneNb,
        queues,
        cause,
        agentNb
      )

  case class AgentOnPause(
      id: Agent.Id,
      override val changed: DateTime,
      override val phoneNb: String,
      override val queues: List[Int],
      override val cause: Option[String] = Some(""),
      override val agentNb: Agent.Number
  ) extends AgentState(
        "AgentOnPause",
        id,
        changed,
        phoneNb,
        queues,
        cause,
        agentNb
      )

  case class AgentLoggedOut(
      id: Agent.Id,
      override val changed: DateTime,
      override val phoneNb: String,
      override val queues: List[Int],
      override val agentNb: Agent.Number,
      override val cause: Option[String] = Some("")
  ) extends AgentState(
        "AgentLoggedOut",
        id,
        changed,
        phoneNb,
        queues,
        cause,
        agentNb
      )

  case class AgentOnWrapup(
      id: Agent.Id,
      override val changed: DateTime,
      override val phoneNb: String,
      override val queues: List[Int],
      override val cause: Option[String] = Some(""),
      override val agentNb: Agent.Number
  ) extends AgentState(
        "AgentOnWrapup",
        id,
        changed,
        phoneNb,
        queues,
        cause,
        agentNb
      )

  case class AgentRinging(
      id: Id,
      override val changed: DateTime,
      acd: Boolean,
      override val phoneNb: String,
      override val queues: List[Int],
      override val cause: Option[String] = Some(""),
      override val agentNb: Agent.Number
  ) extends AgentState(
        "AgentRinging",
        id,
        new DateTime,
        phoneNb,
        queues,
        cause,
        agentNb
      )

  def toJson(agentState: AgentState): JsValue = {
    Json.toJson(agentState)
  }
}
