package xivo.events

import xivo.models.Agent

trait EventAgent {
  def agentId: Agent.Id
}
