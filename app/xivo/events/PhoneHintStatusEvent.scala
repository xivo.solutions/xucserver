package xivo.events

import org.xivo.cti.model.PhoneHintStatus
import play.api.libs.json.{JsValue, Json, Writes}

object PhoneHintStatusEvent {
  implicit val writes: Writes[PhoneHintStatusEvent] =
    new Writes[PhoneHintStatusEvent] {
      override def writes(o: PhoneHintStatusEvent): JsValue =
        Json.obj(
          "number" -> o.number,
          "status" -> o.status.getHintStatus()
        )
    }
}
case class PhoneHintStatusEvent(number: String, status: PhoneHintStatus)
