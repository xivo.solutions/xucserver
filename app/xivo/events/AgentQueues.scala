package xivo.events

import org.xivo.cti.message.AgentStatusUpdate
import scala.jdk.CollectionConverters._
import xivo.models.Agent

case class AgentQueues(agentId: Agent.Id, queues: List[Int])

object AgentQueues {
  def apply(agentStatus: AgentStatusUpdate): AgentQueues = {
    AgentQueues(
      agentStatus.getAgentId,
      agentStatus.getStatus.getQueues.asScala.toList.map(Int.unbox(_))
    )
  }
}
