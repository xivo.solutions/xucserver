package xivo.network

import java.net.ConnectException

import org.apache.pekko.actor.{Actor, ActorRef, Props}
import org.apache.pekko.pattern.pipe
import play.api.Logger
import services.chat.model._
import services.chat.{ChatUserState, ChatUtil, UpdateChatLinkRef}
import xivo.models.{MattermostNoUnreadMessages, _}
import xivo.network.ChatLink._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait ChatLinkLog {
  this: ChatLink =>

  val logger: Logger = Logger(
    getClass.getPackage.getName + ".ChatLink." + self.path.name
  )
  var directChannels: Map[OtherPartyGuid, MattermostDirectChannel] = Map.empty

  private def logMessage(msg: String): String =
    s"[${chatUserState.user.username}] $msg"

  object log {
    def debug(msg: String): Unit = logger.debug(logMessage(msg))
    def info(msg: String): Unit  = logger.info(logMessage(msg))
    def error(msg: String): Unit = logger.error(logMessage(msg))
    def warn(msg: String): Unit  = logger.warn(logMessage(msg))
  }
}

object ChatLink {
  def props(
      flashTextUserState: ChatUserState,
      token: String,
      flashTextService: ActorRef,
      chatBackendWS: ChatBackendWS,
      currentGuid: String,
      flashTextUtil: ChatUtil
  ): Props =
    Props(
      new ChatLink(
        flashTextUserState,
        token,
        flashTextService,
        chatBackendWS,
        currentGuid,
        flashTextUtil
      )
    )

  type OtherPartyGuid = String

  case object StopChatLink
  case object RetrieveDirectChannels
  case class UpdateDirectChannels(
      channel: MattermostDirectChannel,
      members: List[MattermostChannelMember]
  )
  case class CheckUnreadMessages(channel: MattermostDirectChannel)
}

class ChatLink(
    val chatUserState: ChatUserState,
    token: String,
    chatService: ActorRef,
    chatBackendWS: ChatBackendWS,
    guid: String,
    chatTextUtil: ChatUtil
) extends Actor
    with ChatLinkLog {

  override def preStart(): Unit = {
    log.info(s"Starting ChatLink actor for user")
    chatService ! UpdateChatLinkRef(self, chatUserState.user.username)
  }

  override def receive: Receive = {
    case RetrieveDirectChannels =>
      withRecovery("Unable to retrieve direct channels", 0) {
        chatBackendWS
          .getChannelsForUser(guid)
          .map(getChannelMembers)
      }

    case SendDirectMattermostMessage(userFrom, userTo, message, seq) =>
      log.debug(
        s"Send new message from ${userFrom.username}(guid:${userFrom.guid}) to ${userTo.username}(guid:${userTo.guid}) : $message"
      )
      withRecovery("Unable to post a message", seq) {
        getOrCreateChannel(userFrom.guid, userTo.guid)
          .flatMap { channel =>
            chatBackendWS
              .createPostInChannel(
                NewMattermostChannelPost(
                  channel.id,
                  message,
                  seq,
                  userFrom.username,
                  userTo.username
                ),
                token
              )
              .pipeTo(chatService)
          }
      }

    case UpdateDirectChannels(channel, members) =>
      log.debug(s"Update direct channel ${channel.name}")
      setDirectChannelMember(channel, members)
      self ! CheckUnreadMessages(channel)

    case GetDirectMattermostMessagesHistory(userFrom, userTo, seq) =>
      log.debug(s"Get direct messages history with ${userTo.username}")
      withRecovery("Unable to get history from channel", 0) {
        getOrCreateChannel(userFrom.guid, userTo.guid)
          .flatMap { channel =>
            chatBackendWS
              .getPostsForChannel(channel.id, token)
              .map(chatTextUtil.mattermostOrderRecipients(userFrom, userTo))
              .map(processHistoryMessages(userFrom, userTo, seq))
          }
      }

    case CheckUnreadMessages(channel) =>
      log.debug(s"Check unread messages in channel ${channel.id}")
      withRecovery("Unable to check unread messages", 0) {
        getUnreadMessagesCounterByChannel(channel)
          .flatMap { _ =>
            getUserByChannel(channel)
              .map(chatBackendWS.getCtiUser)
              .getOrElse(
                Future.failed(
                  new NoSuchElementException(
                    "Recipient not found in the list of direct channels."
                  )
                )
              )
          }
          .map(u => ChatBackendWS.getIdFromUser(u.username))
          .map(id =>
            getLatestPostFromChannel(channel.id)(id).map(processUnreadMessages)
          )
      }

    case MarkMattermostMessagesAsRead(userFrom, userTo, seq) =>
      log.debug(s"Mark messages as read with ${userTo.username}")
      withRecovery("Unable to mark messages as read", seq) {
        getOrCreateChannel(userFrom.guid, userTo.guid)
          .flatMap { channel =>
            chatBackendWS
              .setActiveChannel(channel.id, guid, token)
              .map(r => processMarkedAsRead(userTo.username, r.status, seq))
          }
      }

    case StopChatLink =>
      log.debug(s"Stopping actor chatLink")
      context.stop(self)
  }

  private def getOrCreateChannel(
      from: String,
      to: String
  ): Future[MattermostDirectChannel] = {
    directChannels
      .get(to)
      .map(ch => Future.successful(ch))
      .getOrElse {
        chatBackendWS
          .createDirectMessageChannel(from, to, token)
          .map { channel =>
            directChannels = directChannels.updated(to, channel)
            channel
          }
      }
  }

  private def getChannelMembers(channels: List[MattermostDirectChannel]) = {
    channels.map { channel =>
      for {
        members <- chatBackendWS.getChannelMembers(
          channel.id,
          token,
          chatUserState.user.guid,
          filtered = true
        )
      } yield self ! UpdateDirectChannels(channel, members)
    }
  }

  private def getUserByChannel(
      channel: MattermostDirectChannel
  ): Option[OtherPartyGuid] = {
    directChannels.find(_._2.id == channel.id).map(_._1)
  }

  private def getUnreadMessagesCounterByChannel(
      channel: MattermostDirectChannel
  ): Future[MattermostUnreadCounter] = {
    chatBackendWS
      .getUnreadMessagessCounterByChannel(channel.id, guid, token)
      .flatMap {
        case m if m.msg_count > 0 => Future.successful(m)
        case _                    => Future.failed(MattermostNoUnreadMessages(channel))
      }
  }

  private def getLatestPostFromChannel(
      channelId: String
  ): String => Future[List[Message]] = { (userId: String) =>
    chatBackendWS
      .getPostsAroundLastUnread(channelId, guid, token)
      .map(m => m.filterNot(_.user_id == guid))
      .filter(_.nonEmpty)
      .flatMap { messages =>
        Future.sequence(
          messages
            .map(setMessageDirection(userId))
            .map(chatTextUtil.mattermostOrderRecipients)
        )
      }
  }

  private def setDirectChannelMember(
      channel: MattermostDirectChannel,
      members: List[MattermostChannelMember]
  ) = {
    members.map(m =>
      directChannels = directChannels.updated(m.user_id, channel)
    )
  }

  private def setMessageDirection(userId: String) = {
    (m: MattermostDirectMessage) =>
      val direction = {
        if (m.user_id == guid) DirectionIsFromUser
        else DirectionIsToUser
      }
      MattermostUnreadNotification(
        direction,
        chatUserState.user.username,
        userId.toLong,
        m,
        m.offsetDateTime,
        0
      )
  }

  private def processUnreadMessages(msg: List[Message]): Unit = {
    chatUserState.ctiRouterRef
      .foreach(_ ! MessageUnreadNotification(msg, 0))
  }

  private def processHistoryMessages(
      userFrom: MattermostGuid,
      userTo: MattermostGuid,
      sequence: Long
  ): List[Message] => Unit = { (posts: List[Message]) =>
    val (from, to) = (
      ChatUser(
        userFrom.username,
        chatTextUtil.getPhoneNumber(userFrom.username),
        userFrom.displayName,
        Some(userFrom.guid)
      ),
      ChatUser(
        userTo.username,
        chatTextUtil.getPhoneNumber(userTo.username),
        userTo.displayName,
        Some(userTo.guid)
      )
    )
    chatUserState.ctiRouterRef
      .foreach(_ ! MessageHistory((from, to), posts, sequence))
  }

  private def processMarkedAsRead(
      usernameTo: String,
      status: String,
      seq: Long
  ): Unit = {
    chatUserState.ctiRouterRef
      .foreach(_ ! MessageMarkAsRead(usernameTo, status, seq))
  }

  private def withRecovery[T](message: String, seq: Long)(
      f: Future[T]
  ): Future[Any] = {
    f.recover(handleErrors(message, seq))
  }

  private def handleErrors(
      message: String,
      seq: Long
  ): PartialFunction[Throwable, Unit] = {
    case MattermostNoUnreadMessages(channel) =>
      log.debug(
        s"No unread message for user ${chatUserState.user.username} and channel ${channel.name}"
      )
    case e: Exception =>
      log.error(s"$message. ${e.getMessage}")
      e match {
        case _: WebServiceException =>
          chatUserState.ctiRouterRef.foreach(_ ! RequestNack(seq))
        case _: ConnectException =>
          chatUserState.ctiRouterRef.foreach(_ ! RequestNack(seq))
        case e: NoSuchElementException => log.warn(s"${e.getMessage}")
        case _: Exception              => log.error(s"Unhandled error. ${e.getMessage}")
      }
  }
}
