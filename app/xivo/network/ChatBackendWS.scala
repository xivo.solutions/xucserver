package xivo.network

import java.security.{InvalidParameterException, MessageDigest}

import com.google.inject.Inject
import helpers.FutureFailureLogger
import play.api.Logger
import play.api.http.Status
import play.api.libs.json._
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}
import xivo.models._
import xivo.xuc.ChatConfig
import play.api.libs.ws.WSBodyWritables.writeableOf_JsValue

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ChatBackendWS @Inject() (config: ChatConfig, val ws: WSClient)
    extends FutureFailureLogger {

  implicit val logger: Logger = Logger(getClass.getName)

  final val team: String = "xivo"
  var teamId: String     = scala.compiletime.uninitialized

  def withSecret[T](f: String => Future[T]): Future[T] =
    config.secret match {
      case Some(s) => f(s)
      case None    => Future.failed(new WebServiceException("No secret defined"))
    }

  def withTeamId[T](f: String => Future[T]): Future[T] =
    teamId match {
      case null => getTeamId.flatMap(f)
      case t    => f(t)
    }

  def checkResponseStatus(response: WSResponse): Future[WSResponse] = {
    if (Status.isSuccessful(response.status)) {
      Future.successful(response)
    } else if (Status.isClientError(response.status)) {
      logger.warn(
        s"Error response with status ${response.status} and body ${response.body}"
      )
      Future.successful(response)
    } else {
      Future.failed(
        new WebServiceException(
          s"Error response with status ${response.status} and body : ${response.body}"
        )
      )
    }
  }

  def handleParsingError[T](
      response: WSResponse
  )(implicit rds: Reads[T]): Future[T] =
    response.json.validate[T] match {
      case JsSuccess(l, _) => Future.successful(l)
      case e: JsError =>
        Future.failed(new WebServiceException(s"Failed to parse json : $e"))
    }

  def getWsUrl(resource: String): WSRequest =
    ws.url(s"http://${config.chatHost}:${config.chatPort}/api/v4/$resource")

  def requestWithAdminToken(resource: String): WSRequest =
    getWsUrl(resource).withHttpHeaders(
      ("Authorization", s"Bearer ${config.chatAdminToken}")
    )

  def requestWithUserToken(resource: String, token: String): WSRequest = {
    getWsUrl(resource).withHttpHeaders(("Authorization", s"Bearer $token"))
  }

  def postWithAdminToken[I, O](resource: String, payload: I)(implicit
      rds: Reads[O],
      tjs: Writes[I]
  ): Future[O] = {
    requestWithAdminToken(resource)
      .post(Json.toJson(payload))
      .flatMap(checkResponseStatus)
      .flatMap(handleParsingError[O])
  }

  def postWithUserToken[I, O](
      resource: String,
      payload: I,
      token: String
  )(implicit rds: Reads[O], tjs: Writes[I]): Future[O] = {
    requestWithUserToken(resource, token)
      .post(Json.toJson(payload))
      .flatMap(checkResponseStatus)
      .flatMap(handleParsingError[O])
  }

  def getWithAdminToken[O](
      resource: String
  )(implicit rds: Reads[O]): Future[O] = {
    requestWithAdminToken(resource)
      .get()
      .flatMap(checkResponseStatus)
      .flatMap(handleParsingError[O])
  }

  def getWithUserToken[O](resource: String, token: String)(implicit
      rds: Reads[O]
  ): Future[O] = {
    requestWithUserToken(resource, token)
      .get()
      .flatMap(checkResponseStatus)
      .flatMap(handleParsingError[O])
  }

  def createUser(
      id: Long,
      firstName: String,
      lastName: String
  ): Future[MattermostUser] =
    logFailure(s"Error while creating user $id $firstName $lastName") {
      withSecret { secret =>
        val user = NewMattermostUser(id, secret, firstName, lastName)
        postWithAdminToken[NewMattermostUser, MattermostUser]("users", user)
      }
    }

  def createDirectMessageChannel(
      fromGuid: String,
      toGuid: String,
      token: String
  ): Future[MattermostDirectChannel] = {
    logFailure(
      s"Error while creating direct message channel between $fromGuid and $toGuid"
    ) {
      postWithUserToken[List[String], MattermostDirectChannel](
        "channels/direct",
        List(fromGuid, toGuid),
        token
      )
    }
  }

  def createPostInChannel(
      message: NewMattermostChannelPost,
      token: String
  ): Future[MattermostDirectMessageAck] = {
    logFailure(
      s"Error while posting message to channel ${message.channel_id}"
    ) {
      postWithUserToken[NewMattermostChannelPost, MattermostDirectMessage](
        "posts",
        message,
        token
      )
        .map(m =>
          MattermostDirectMessageAck(
            m.id,
            m.channel_id,
            m.message,
            m.create_at,
            m.user_id,
            message.seq,
            message.from,
            message.to
          )
        )
    }
  }

  def getCtiUser(id: Long): Future[List[MattermostUser]] =
    logFailure(s"Error while getting user $id") {
      postWithAdminToken[List[String], List[MattermostUser]](
        "users/usernames",
        List(ChatBackendWS.getUsernameFromId(id))
      )
    }

  def getCtiUser(guid: String): Future[MattermostUser] =
    logFailure(s"Error while getting user $guid") {
      getWithAdminToken[MattermostUser](s"users/$guid")
    }

  def getUserToken(id: Long): Future[String] = {
    config.secret match {
      case Some(secret) =>
        logFailure(s"Error while getting user $id token ") {
          val json = Json.obj(
            "login_id" -> ChatBackendWS.getUsernameFromId(id),
            "password" -> ChatBackendWS.getPasswordFromId(id, secret)
          )

          getWsUrl("users/login")
            .post(json)
            .map(_.header("Token"))
            .flatMap {
              case Some(token) => Future.successful(token)
              case None =>
                Future.failed(
                  new WebServiceException(
                    s"Error when getting user $id token, Token header is not set"
                  )
                )
            }
        }
      case None =>
        Future.failed(new WebServiceException("Cannot retrieve configuration"))
    }
  }

  def getTeamId: Future[String] = {
    logFailure(s"Error while getting team: $team id") {
      getWithAdminToken[MattermostTeam](s"teams/name/$team")
        .map { team =>
          teamId = team.id; teamId
        }
    }
  }

  def getChannelsForUser(
      userGuid: String
  ): Future[List[MattermostDirectChannel]] = {
    logFailure(s"Error while getting channels for user $userGuid") {
      withTeamId { teamId =>
        requestWithAdminToken(s"users/$userGuid/teams/$teamId/channels")
          .get()
          .flatMap(checkResponseStatus)
          .flatMap { response =>
            handleParsingError[List[MattermostDirectChannel]](response)
              .recoverWith { case _ =>
                handleParsingError[NoChannelsFoundError](response)
                  .map(_ => List())
              }
          }
      }
    }
  }

  def getChannelMembers(
      channelId: String,
      token: String,
      userGuid: Option[String] = None,
      filtered: Boolean = true
  ): Future[List[MattermostChannelMember]] = {
    logFailure(s"Error while getting members for channel $channelId") {
      getWithUserToken[List[MattermostChannelMember]](
        s"channels/$channelId/members",
        token
      )
        .collect {
          case m if filtered && userGuid.isDefined =>
            m.filterNot(_.user_id == userGuid.get)
          case m => m
        }
    }
  }

  def getPostsForChannel(
      channelId: String,
      token: String,
      page: String = "0",
      perPage: String = "60"
  ): Future[List[MattermostDirectMessage]] = {
    logFailure(s"Error while getting members for channel $channelId") {
      getWithUserToken[MattermostChannelPosts](
        s"channels/$channelId/posts?page=$page&per_page=$perPage",
        token
      )
        .map(sortDirectChannelPosts)
    }
  }

  def getUnreadMessagessCounterByChannel(
      channelId: String,
      userGuid: String,
      token: String
  ): Future[MattermostUnreadCounter] = {
    logFailure(
      s"Error while getting unread messages for channel $channelId and user $userGuid"
    ) {
      getWithUserToken[MattermostUnreadCounter](
        s"users/$userGuid/channels/$channelId/unread",
        token
      )
    }
  }

  def getPostsAroundLastUnread(
      channelId: String,
      userGuid: String,
      token: String
  ): Future[List[MattermostDirectMessage]] = {
    logFailure(s"Error while getting members for channel $channelId") {
      getWithUserToken[MattermostChannelPosts](
        s"users/$userGuid/channels/$channelId/posts/unread?limit_before=0&limit_after=200",
        token
      )
        .map(sortDirectChannelPosts)
        .filter(_.nonEmpty)
        .recoverWith { case _ => getPostsForChannel(channelId, token) }
    }
  }

  def setActiveChannel(
      channelId: String,
      userGuid: String,
      token: String
  ): Future[MattermostViewChannelResponse] = {
    logFailure(s"Error while updating the active channel") {
      postWithUserToken[MattermostViewChannel, MattermostViewChannelResponse](
        s"channels/members/$userGuid/view",
        MattermostViewChannel(channelId),
        token
      )
    }
  }

  private def sortDirectChannelPosts
      : MattermostChannelPosts => List[MattermostDirectMessage] = {
    (channelData: MattermostChannelPosts) =>
      val unsortedMessages = channelData.posts.map(p => p.id -> p).toMap
      channelData.order.reverse.collect(unsortedMessages)
  }
}

object ChatBackendWS {

  def getUsernameFromId(id: Long): String = s"user_$id"

  def getIdFromUser(username: String): String = {
    username match {
      case s if s.startsWith("user_") => username.stripPrefix("user_").trim
      case _ =>
        throw new InvalidParameterException(
          s"Username is not in expected format. Expecting user_id, found $username."
        )
    }
  }

  def getEmailFromId(id: Long): String = s"${getUsernameFromId(id)}@xivopbx"

  def getPasswordFromId(id: Long, secret: String): String = {
    val alphabet =
      "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
    val keys = MessageDigest
      .getInstance("SHA-256")
      .digest(s"${id}_$secret".getBytes)
      .map(_.toInt)
    val sb = new StringBuilder(keys.length)
    keys.foreach(key => sb.append(alphabet(Math.abs(key) % alphabet.size)))
    sb.toString
  }
}
