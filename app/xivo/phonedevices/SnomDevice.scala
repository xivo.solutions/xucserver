package xivo.phonedevices

import org.apache.pekko.actor.ActorRef
import com.google.inject.Inject
import helpers.LogFutureFailure
import play.api.Logger
import play.api.libs.ws.{WSAuthScheme, WSClient}
import services.calltracking.DeviceCall
import xivo.models.Line
import xivo.xuc.XucConfig

import scala.concurrent.ExecutionContext.Implicits.global

class SnomDevice @Inject() (
    ip: String,
    line: Line,
    lineNumber: String,
    val ws: WSClient,
    val config: XucConfig
) extends CtiDevice(line, lineNumber, config) {
  override implicit val logger: Logger = Logger(getClass.getName)

  def formatDialNumber(s: String): String = s filter ("*#0123456789" contains _)

  override def dial(
      destination: String,
      variables: Map[String, String],
      sender: ActorRef
  ): Unit = doIt(s"number=${formatDialNumber(destination)}")
  override def answer(call: Option[DeviceCall], sender: ActorRef): Unit =
    doIt("key=ENTER")
  override def hangup(sender: ActorRef): Unit = doIt("key=CANCEL")
  override def attendedTransfer(destination: String, sender: ActorRef): Unit =
    doIt(s"number=${formatDialNumber(destination)}")
  override def completeTransfer(sender: ActorRef): Unit = doIt("key=TRANSFER")
  override def cancelTransfer(sender: ActorRef): Unit   = doIt("key=CANCEL")
  override def conference(sender: ActorRef): Unit       = doIt("key=CONFERENCE")
  override def hold(call: Option[DeviceCall], sender: ActorRef): Unit =
    doIt("key=F_HOLD")

  private def doIt(command: String): Unit = {
    try {
      ws.url(s"http://$ip/command.htm?$command")
        .withAuth(config.SnomUser, config.SnomPassword, WSAuthScheme.BASIC)
        .get()
        .recoverWith(
          LogFutureFailure.logFailure(
            s"Unable to call Snom device web service $command on $ip"
          )
        )
    } catch {
      case e: IllegalArgumentException =>
        logger.error(s"Unable to call service $command", e)
    }
  }
}
