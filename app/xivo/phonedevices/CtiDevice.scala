package xivo.phonedevices

import org.apache.pekko.actor.ActorRef
import services.calltracking.DeviceCall
import services.calltracking.SipDriver.SipDriver
import xivo.models.{Agent, Line}
import xivo.xuc.XucConfig
import xivo.xucami.models.QueueCall

class CtiDevice(val line: Line, val phoneNb: String, config: XucConfig)
    extends DeviceAdapter {

  override def dial(
      destination: String,
      variables: Map[String, String],
      sender: ActorRef
  ): Unit =
    sender ! StandardDial(
      line,
      destination,
      variables,
      config.xivoHost,
      config.sipDriver,
      line.dev.map(_.vendor)
    )
  override def listenCallbackMessage(
      voiceMessageRef: String,
      variables: Map[String, String],
      sender: ActorRef
  ): Unit =
    sender ! ListenCallbackMessage(
      line,
      voiceMessageRef,
      variables,
      config.xivoHost,
      config.sipDriver,
      line.dev.map(_.vendor)
    )
  override def dialFromMobile(
      mobileNumber: String,
      destination: String,
      callerNumber: String,
      callerName: String,
      variables: Map[String, String],
      sender: ActorRef,
      xivoUserId: Long
  ): Unit =
    sender ! DialFromMobile(
      mobileNumber,
      destination,
      callerNumber,
      callerName,
      variables,
      xivoHost = config.xivoHost,
      xivoUserId = xivoUserId
    )
  override def answer(
      call: Option[DeviceCall] = None,
      sender: ActorRef
  ): Unit = {}
  override def hangup(sender: ActorRef): Unit =
    sender ! HangupCommand(line, phoneNb)
  override def attendedTransfer(destination: String, sender: ActorRef): Unit =
    sender ! AttendedTransferCommand(line, destination)
  override def completeTransfer(sender: ActorRef): Unit =
    sender ! CompleteTransferCommand(line)
  override def completeTransferAmi(
      channel1: String,
      channel2: String,
      sender: ActorRef
  ): Unit = {
    sender ! SetVarCommand(channel1, "XIVO_CHAN_TO_BRIDGE", channel2)
    sender ! RedirectCommand(channel1, "xivo_attended_xfer_end", "s", 1)
  }
  override def cancelTransfer(sender: ActorRef): Unit =
    sender ! CancelTransferCommand(line, phoneNb)
  override def directTransfer(destination: String, sender: ActorRef): Unit =
    sender ! DirectTransferCommand(line, destination)
  override def hold(
      call: Option[DeviceCall] = None,
      sender: ActorRef
  ): Unit = {}
  override def odial(
      destination: String,
      agentId: Agent.Id,
      queueNumber: String,
      variables: Map[String, String],
      xivoUserId: Long,
      sender: ActorRef,
      driver: SipDriver,
      vendor: Option[String]
  ): Unit =
    if (destination.length > config.outboundLength && queueNumber != "") {
      sender ! OutboundDial(
        destination,
        agentId,
        queueNumber,
        variables,
        config.xivoHost,
        xivoUserId,
        driver,
        vendor
      )
    } else {
      dial(destination, variables, sender)
    }
  override def retrieveQueueCall(
      sourceChannel: String,
      sourceNumber: Option[String],
      sourceName: String,
      queueCall: QueueCall,
      variables: Map[String, String],
      sender: ActorRef,
      callId: String,
      queueName: Option[String],
      agentNum: Option[String],
      autoAnswer: Boolean,
      driver: SipDriver,
      vendor: Option[String]
  ): Unit = {
    sender ! SetVarCommand(
      queueCall.channel,
      "XIVO_RETRIEVE_QUEUE_NAME",
      queueName.getOrElse("")
    )
    sender ! SetVarCommand(queueCall.channel, "XIVO_UNIQUE_ID", callId)
    sender ! SetVarCommand(
      queueCall.channel,
      "XIVO_AGENT_NUM",
      agentNum.getOrElse("")
    )
    sender ! RetrieveQueueCall(
      sourceChannel,
      sourceNumber,
      sourceName,
      queueCall.channel,
      queueCall.number,
      queueCall.name,
      variables,
      config.xivoHost,
      callId,
      queueName,
      agentNum,
      autoAnswer,
      driver,
      vendor
    )
  }
}
