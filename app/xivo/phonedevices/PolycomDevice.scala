package xivo.phonedevices

import org.apache.pekko.actor.ActorRef
import helpers.LogFutureFailure
import play.api.Logger
import play.api.libs.ws.{WSAuthScheme, WSClient}
import services.calltracking.DeviceCall
import xivo.models.Line
import xivo.xuc.XucConfig
import play.api.libs.ws.DefaultBodyWritables.writeableOf_String
import play.api.libs.ws.WSBodyWritables.writeableOf_String
import play.api.libs.ws.writeableOf_String
import scala.concurrent.ExecutionContext.Implicits.global

class PolycomDevice(
    ip: String,
    line: Line,
    lineNumber: String,
    ws: WSClient,
    config: XucConfig
) extends CtiDevice(line, lineNumber, config) {
  override implicit val logger: Logger = Logger(getClass.getName)

  override def answer(call: Option[DeviceCall], sender: ActorRef): Unit =
    pushKey("Line1")

  override def hold(call: Option[DeviceCall], sender: ActorRef): Unit =
    pushKey("Hold")

  private def pushKey(key: String): Unit = {
    try {
      ws.url(s"http://$ip/push")
        .withAuth(
          config.PolycomUser,
          config.PolycomPassword,
          WSAuthScheme.DIGEST
        )
        .withHttpHeaders("Content-Type" -> "application/x-com-polycom-spipx")
        .post(
          s"""<PolycomIPPhone><Data priority="Critical">Key:$key</Data></PolycomIPPhone>"""
        )
        .recoverWith(
          LogFutureFailure.logFailure(
            s"Unable to call Polycom device web service $key on $ip"
          )
        )
    } catch {
      case e: IllegalArgumentException =>
        logger.error(s"Unable to call service $key", e)
    }
  }
}
