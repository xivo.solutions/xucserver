package xivo.websocket

import org.apache.pekko.event.{ActorEventBus, LookupClassification}
import com.google.inject.Singleton
import play.api.libs.json.JsValue
import xivo.websocket.WsBus.WsMessageEvent

object WsBus {
  def browserTopic(username: String): String = s"/ws/$username/"

  case class WsContent(browserMsg: JsValue)
  case class WsMessageEvent(val channel: String, val message: Any)
}

@Singleton
class WsBus extends ActorEventBus with LookupClassification {

  override type Event      = WsMessageEvent
  override type Classifier = String

  override protected def mapSize(): Int = 300

  override def publish(event: Event): Unit = super.publish(event)
  override def subscribe(subscriber: Subscriber, to: Classifier): Boolean =
    super.subscribe(subscriber, to)
  override def unsubscribe(subscriber: Subscriber, from: Classifier): Boolean =
    super.unsubscribe(subscriber, from)
  override def unsubscribe(subscriber: Subscriber): Unit =
    super.unsubscribe(subscriber)

  override protected def publish(event: Event, subscriber: Subscriber): Unit = {
    subscriber ! event.message
  }

  override protected def classify(event: Event): Classifier = {
    event.channel
  }

}
