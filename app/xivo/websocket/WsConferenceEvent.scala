package xivo.websocket

import play.api.libs.json.*
import services.calltracking.ConferenceParticipant
import services.calltracking.ConferenceTracker.*
import services.calltracking.{OrganizerRole, UserRole}
import services.calltracking.DeviceConferenceAction.*
import helpers.DateUtil
import play.api.libs.functional.syntax.toFunctionalBuilderOps

sealed trait WsConferenceParticipantRole
case object WsConferenceParticipantOrganizerRole
    extends WsConferenceParticipantRole
case object WsConferenceParticipantUserRole extends WsConferenceParticipantRole

case class WsConferenceParticipant(
    index: Int,
    callerIdName: String,
    callerIdNum: String,
    since: Int,
    isTalking: Boolean = false,
    role: WsConferenceParticipantRole = WsConferenceParticipantUserRole,
    isMuted: Boolean = false,
    isMe: Boolean = false,
    username: Option[String] = None,
    isDeaf: Boolean = false
)

object WsConferenceParticipant {
  def from(p: ConferenceParticipant, me: List[Int]): WsConferenceParticipant = {
    val role = p.role match {
      case OrganizerRole => WsConferenceParticipantOrganizerRole
      case UserRole      => WsConferenceParticipantUserRole
    }

    WsConferenceParticipant(
      p.index,
      p.callerId.name,
      p.callerId.number,
      DateUtil.secondsSince(p.joinTime).toInt,
      p.isTalking,
      role,
      p.isMuted,
      me.contains(p.index),
      isDeaf = p.isDeaf
    )
  }
}

sealed trait WsConferenceEventType
case object WsConferenceEventJoin  extends WsConferenceEventType
case object WsConferenceEventLeave extends WsConferenceEventType

sealed trait WsConferenceParticipantEventType
case object WsConferenceParticipantEventJoin
    extends WsConferenceParticipantEventType
case object WsConferenceParticipantEventLeave
    extends WsConferenceParticipantEventType
case object WsConferenceParticipantEventUpdate
    extends WsConferenceParticipantEventType

case class WsConferenceEvent(
    eventType: WsConferenceEventType,
    uniqueId: String,
    phoneNumber: String,
    conferenceNumber: String,
    conferenceName: String,
    participants: List[WsConferenceParticipant],
    since: Int
)

object WsConferenceEvent {

  def from(
      evt: DeviceConference,
      uniqueId: String,
      phoneNumber: String,
      me: List[Int]
  ): WsConferenceEvent = {
    val eventType = evt match {
      case _: DeviceJoinConference  => WsConferenceEventJoin
      case _: DeviceLeaveConference => WsConferenceEventLeave
    }
    val ps =
      evt.conference.participants.map(WsConferenceParticipant.from(_, me))
    val since =
      evt.conference.startTime.map(DateUtil.secondsSince(_).toInt).getOrElse(0)
    WsConferenceEvent(
      eventType,
      uniqueId,
      phoneNumber,
      evt.conference.number,
      evt.conference.name,
      ps,
      since
    )
  }

  implicit val wsConferenceParticipantRoleWrite
      : Writes[WsConferenceParticipantRole] =
    new Writes[WsConferenceParticipantRole] {
      def writes(r: WsConferenceParticipantRole): JsValue =
        r match {
          case WsConferenceParticipantOrganizerRole => JsString("Organizer")
          case WsConferenceParticipantUserRole      => JsString("User")
        }
    }

  implicit val wsConferenceParticipantRoleReads
      : Reads[WsConferenceParticipantRole] =
    new Reads[WsConferenceParticipantRole] {
      override def reads(json: JsValue): JsResult[WsConferenceParticipantRole] =
        json match {
          case JsString("Organizer") =>
            JsSuccess(WsConferenceParticipantOrganizerRole)
          case JsString("User") => JsSuccess(WsConferenceParticipantUserRole)
          case _                => JsError("Invalid Conference Role")
        }
    }

  implicit val wsParticipantWrites: Writes[WsConferenceParticipant] = (
    (JsPath \ "index").write[Int] and
      (JsPath \ "callerIdName").write[String] and
      (JsPath \ "callerIdNum").write[String] and
      (JsPath \ "since").write[Int] and
      (JsPath \ "isTalking").write[Boolean] and
      (JsPath \ "role").write[WsConferenceParticipantRole] and
      (JsPath \ "isMuted").write[Boolean] and
      (JsPath \ "isMe").write[Boolean] and
      (JsPath \ "username").writeNullable[String] and
      (JsPath \ "isDeaf").write[Boolean]
  )(o =>
    (
      o.index,
      o.callerIdName,
      o.callerIdNum,
      o.since,
      o.isTalking,
      o.role,
      o.isMuted,
      o.isMe,
      o.username,
      o.isDeaf
    )
  )

  implicit val wsConferenceEventTypeWrite: Writes[WsConferenceEventType] =
    new Writes[WsConferenceEventType] {
      def writes(eventType: WsConferenceEventType): JsValue =
        eventType match {
          case WsConferenceEventJoin  => JsString("Join")
          case WsConferenceEventLeave => JsString("Leave")
        }
    }

  implicit val wsConferenceEventWrites: Writes[WsConferenceEvent] = (
    (JsPath \ "eventType").write[WsConferenceEventType] and
      (JsPath \ "uniqueId").write[String] and
      (JsPath \ "phoneNumber").write[String] and
      (JsPath \ "conferenceNumber").write[String] and
      (JsPath \ "conferenceName").write[String] and
      (JsPath \ "participants").write[List[WsConferenceParticipant]] and
      (JsPath \ "since").write[Int]
  )(o =>
    (
      o.eventType,
      o.uniqueId,
      o.phoneNumber,
      o.conferenceNumber,
      o.conferenceName,
      o.participants,
      o.since
    )
  )

}

case class WsConferenceParticipantEvent(
    eventType: WsConferenceParticipantEventType,
    uniqueId: String,
    phoneNumber: String,
    conferenceNumber: String,
    index: Int,
    callerIdName: String,
    callerIdNum: String,
    since: Int,
    isTalking: Boolean = false,
    role: WsConferenceParticipantRole = WsConferenceParticipantUserRole,
    isMuted: Boolean = false,
    isMe: Boolean = false,
    username: Option[String] = None,
    isDeaf: Boolean = false
)

object WsConferenceParticipantEvent {

  def from(
      evt: ConferenceParticipantChange,
      uniqueId: String,
      phoneNumber: String,
      me: List[Int],
      username: Option[String] = None
  ): WsConferenceParticipantEvent = {
    val eventType = evt match {
      case _: ParticipantJoinConference  => WsConferenceParticipantEventJoin
      case _: ParticipantLeaveConference => WsConferenceParticipantEventLeave
      case _: ParticipantUpdated         => WsConferenceParticipantEventUpdate
    }

    val role = evt.participant.role match {
      case OrganizerRole => WsConferenceParticipantOrganizerRole
      case UserRole      => WsConferenceParticipantUserRole
    }

    WsConferenceParticipantEvent(
      eventType,
      uniqueId,
      phoneNumber,
      evt.numConf,
      evt.participant.index,
      evt.participant.callerId.name,
      evt.participant.callerId.number,
      DateUtil.secondsSince(evt.participant.joinTime).toInt,
      evt.participant.isTalking,
      role,
      evt.participant.isMuted,
      me.contains(evt.participant.index),
      username,
      isDeaf = evt.participant.isDeaf
    )
  }

  import WsConferenceEvent.wsConferenceParticipantRoleWrite

  implicit val wsConferenceParticipantEventTypeWrite
      : Writes[WsConferenceParticipantEventType] =
    new Writes[WsConferenceParticipantEventType] {
      def writes(eventType: WsConferenceParticipantEventType): JsValue =
        eventType match {
          case WsConferenceParticipantEventJoin   => JsString("Join")
          case WsConferenceParticipantEventLeave  => JsString("Leave")
          case WsConferenceParticipantEventUpdate => JsString("Update")
        }
    }

  implicit val wsConferenceParticipantEvent
      : OWrites[WsConferenceParticipantEvent] = (
    (JsPath \ "eventType").write[WsConferenceParticipantEventType] and
      (JsPath \ "uniqueId").write[String] and
      (JsPath \ "phoneNumber").write[String] and
      (JsPath \ "conferenceNumber").write[String] and
      (JsPath \ "index").write[Int] and
      (JsPath \ "callerIdName").write[String] and
      (JsPath \ "callerIdNum").write[String] and
      (JsPath \ "since").write[Int] and
      (JsPath \ "isTalking").write[Boolean] and
      (JsPath \ "role").write[WsConferenceParticipantRole] and
      (JsPath \ "isMuted").write[Boolean] and
      (JsPath \ "isMe").write[Boolean] and
      (JsPath \ "username").writeNullable[String] and
      (JsPath \ "isDeaf").write[Boolean]
  )(o =>
    (
      o.eventType,
      o.uniqueId,
      o.phoneNumber,
      o.conferenceNumber,
      o.index,
      o.callerIdName,
      o.callerIdNum,
      o.since,
      o.isTalking,
      o.role,
      o.isMuted,
      o.isMe,
      o.username,
      o.isDeaf
    )
  )
}

case class WsConferenceCommandError(error: DeviceConferenceCommandErrorType)

object WsConferenceCommandError {
  implicit val wsDeviceConferenceCommandErrorType
      : Writes[DeviceConferenceCommandErrorType] =
    new Writes[DeviceConferenceCommandErrorType] {
      def writes(error: DeviceConferenceCommandErrorType): JsValue =
        error match {
          case NotAMember          => JsString("NotAMember")
          case NotOrganizer        => JsString("NotOrganizer")
          case CannotKickOrganizer => JsString("CannotKickOrganizer")
          case ParticipantNotFound => JsString("ParticipantNotFound")
          case ConferenceNotFound  => JsString("ConferenceNotFound")
          case OrganizerStillThere => JsString("OrganizerStillThere")
        }
    }

  implicit val writes: Writes[WsConferenceCommandError] = (JsPath \ "error")
    .write[DeviceConferenceCommandErrorType]
    .contramap(o => o.error)
}
