package xivo.websocket

import models._
import org.xivo.cti.message.{QueueConfigUpdate => _, UserConfigUpdate => _, _}
import org.xivo.cti.model.{Meetme, PhoneHintStatus}
import play.api.libs.json.Format.GenericFormat
import play.api.libs.json._
import services.XucStatsEventBus.AggregatedStatEvent
import services.agent.AgentStatistic
import services.chat.model.BrowserEventEnvelope
import services.config.ConfigDispatcher.AgentDirectory
import services.request._
import services.user.UserRight
import services.video.model._
import services.voicemail.VoiceMailStatusUpdate
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped}
import xivo.events.cti.models.{CtiStatus, CtiStatusLegacy, CtiStatusTraits}
import xivo.events.{
  AgentState,
  CurrentCallsPhoneEvents,
  PhoneEvent,
  PhoneHintStatusEvent
}
import xivo.models._
import xivo.services.AuthenticationToken
import xivo.services.XivoDirectory._
import xivo.websocket.WSMsgType.WSMsgType

object WebSocketEvent extends CtiStatusTraits {
  import XivoObject._

  val ErrorKey = "Error"

  val MsgType    = "msgType"
  val CtiMessage = "ctiMessage"
  class EventBuilder(msgType: WSMsgType) {
    val jsonEvent: JsObject = JsObject(Seq(MsgType -> Json.toJson(msgType)))

    def asEvent: JsValue = jsonEvent

    def asEvent[T](o: T)(implicit tjs: Writes[T]): JsValue = {
      jsonEvent + (CtiMessage -> tjs.writes(o))
    }

    def asEvent(payLoad: JsValue): JsValue = {
      jsonEvent + (CtiMessage -> payLoad)
    }
  }

  def createLoggedOnEvent(): JsValue = new EventBuilder(
    WSMsgType.LoggedOn
  ).asEvent

  def createEvent(agent: Agent): JsValue =
    new EventBuilder(WSMsgType.AgentConfig) `asEvent` agent

  def createEvent(agentDirectory: AgentDirectory): JsValue =
    new EventBuilder(WSMsgType.AgentDirectory) `asEvent` agentDirectory
  def createEvent(aqm: AgentQueueMember): JsValue =
    new EventBuilder(WSMsgType.QueueMember) `asEvent` aqm
  def createEvent(aglStarted: AgentListenStarted): JsValue =
    new EventBuilder(WSMsgType.AgentListen) `asEvent` aglStarted
  def createEvent(aglStopped: AgentListenStopped): JsValue =
    new EventBuilder(WSMsgType.AgentListen) `asEvent` aglStopped
  def createEvent(agentState: AgentState): JsValue =
    new EventBuilder(WSMsgType.AgentStateEvent) `asEvent` agentState
  def createEvent(agentStatistic: AgentStatistic): JsValue =
    new EventBuilder(WSMsgType.AgentStatistics) `asEvent` agentStatistic
  def createEvent(ls: LinkStatusUpdate): JsValue =
    new EventBuilder(WSMsgType.LinkStatusUpdate) `asEvent` ls
  def createEvent(richResult: RichResult, richType: ResultType): JsValue =
    richType match
      case models.ResultType.Research =>
        new EventBuilder(WSMsgType.DirectoryResult) `asEvent` richResult
      case models.ResultType.Favorite =>
        new EventBuilder(WSMsgType.Favorites) `asEvent` richResult

  def createEvent(
      contactSheetData: ContactSheetWrapper,
      wrapperType: ResultType
  ): JsValue =
    wrapperType match
      case ResultType.Research =>
        new EventBuilder(WSMsgType.ContactSheet) `asEvent` contactSheetData
      case ResultType.Favorite =>
        new EventBuilder(
          WSMsgType.FavoriteContactSheet
        ) `asEvent` contactSheetData
  def createEvent(statEvent: AggregatedStatEvent): JsValue =
    new EventBuilder(WSMsgType.QueueStatistics) `asEvent` statEvent
  def createAgentListEvent(agents: List[Agent]): JsValue =
    new EventBuilder(WSMsgType.AgentList) `asEvent` agents
  def createAgentGroupsEvent(agentGroups: List[AgentGroup]): JsValue =
    new EventBuilder(WSMsgType.AgentGroupList) `asEvent` agentGroups
  def createAgentQueueMembersEvent(
      agentQueueMembers: List[AgentQueueMember]
  ): JsValue =
    new EventBuilder(WSMsgType.QueueMemberList) `asEvent` agentQueueMembers

  def createUserGroupsEvent(userGroups: List[UserGroup]): JsValue =
    new EventBuilder(WSMsgType.UserGroups) `asEvent` userGroups
  def createUserGroupUpdateEvent(userGroupUpdate: UserGroupUpdate): JsValue =
    new EventBuilder(WSMsgType.UserGroupUpdate) `asEvent` userGroupUpdate
  // Deprecated It will be removed
  def createEventLegacy(ctiStatusesLegacy: List[CtiStatusLegacy]): JsValue =
    new EventBuilder(WSMsgType.UsersStatuses) `asEvent` ctiStatusesLegacy

  def createEvent(ctiStatuses: List[CtiStatus]): JsValue =
    new EventBuilder(WSMsgType.CtiStatuses) `asEvent` ctiStatuses

  def createEvent(queue: QueueConfigUpdate): JsValue =
    new EventBuilder(WSMsgType.QueueConfig) `asEvent` queue
  def createQueuesEvent(queues: List[QueueConfigUpdate]): JsValue =
    new EventBuilder(WSMsgType.QueueList) `asEvent` queues
  def createEvent(st: Sheet): JsValue =
    new EventBuilder(WSMsgType.Sheet) `asEvent` st
  def createEvent(statistic: QueueStatistics): JsValue =
    new EventBuilder(WSMsgType.QueueStatistics) `asEvent` statistic
  def createEvent(ph: PhoneHintStatus): JsValue =
    new EventBuilder(WSMsgType.PhoneStatusUpdate) `asEvent` ph
  def createEvent(us: UserStatusUpdate): JsValue =
    new EventBuilder(WSMsgType.UserStatusUpdate) `asEvent` us
  def createEvent(lc: LineConfig): JsValue =
    new EventBuilder(WSMsgType.LineConfig) `asEvent` lc
  def createConferenceListEvent(meetmes: List[Meetme]): JsValue =
    new EventBuilder(WSMsgType.ConferenceList) `asEvent` meetmes
  def createEvent(msg: WsConferenceEvent): JsValue =
    new EventBuilder(WSMsgType.ConferenceEvent) `asEvent` msg
  def createEvent(msg: WsConferenceParticipantEvent): JsValue =
    new EventBuilder(WSMsgType.ConferenceParticipantEvent) `asEvent` msg
  def createEvent(msg: WsConferenceCommandError): JsValue =
    new EventBuilder(WSMsgType.ConferenceCommandError) `asEvent` msg
  def createEvent(voiceMailStatusUpdate: VoiceMailStatusUpdate): JsValue =
    new EventBuilder(
      WSMsgType.VoiceMailStatusUpdate
    ) `asEvent` voiceMailStatusUpdate
  def createEvent(queueCalls: QueueCallList): JsValue =
    new EventBuilder(WSMsgType.QueueCalls) `asEvent` queueCalls
  def createEvent(callHistory: CallHistory): JsValue =
    new EventBuilder(WSMsgType.CallHistory) `asEvent` callHistory
  def createEvent(callHistory: RichCallHistory): JsValue =
    new EventBuilder(WSMsgType.CallHistory) `asEvent` callHistory
  def createEvent(msg: FavoriteUpdated): JsValue =
    new EventBuilder(WSMsgType.FavoriteUpdated) `asEvent` msg
  def createEvent(msg: CallbackLists): JsValue =
    new EventBuilder(WSMsgType.CallbackLists) `asEvent` msg
  def createEvent(msg: FindCallbackResponseWithId): JsValue =
    new EventBuilder(WSMsgType.FindCallbackResponseWithId) `asEvent` msg
  def createEvent(msg: PreferredCallbackPeriodList): JsValue =
    new EventBuilder(WSMsgType.PreferredCallbackPeriodList) `asEvent` msg
  def createEvent(msg: CallbackTaken): JsValue =
    new EventBuilder(WSMsgType.CallbackTaken) `asEvent` msg
  def createEvent(msg: CallbackRequestUpdated): JsValue =
    new EventBuilder(WSMsgType.CallbackRequestUpdated) `asEvent` msg
  def createEvent(msg: CallbackReleased): JsValue =
    new EventBuilder(WSMsgType.CallbackReleased) `asEvent` msg
  def createEvent(msg: CallbackStarted): JsValue =
    new EventBuilder(WSMsgType.CallbackStarted) `asEvent` msg
  def createEvent(msg: CallbackClotured): JsValue =
    new EventBuilder(WSMsgType.CallbackClotured) `asEvent` msg
  def createEvent(phoneEvent: PhoneEvent): JsValue =
    new EventBuilder(WSMsgType.PhoneEvent) `asEvent` phoneEvent
  def createEvent(ccpe: CurrentCallsPhoneEvents): JsValue =
    new EventBuilder(WSMsgType.CurrentCallsPhoneEvents) `asEvent` ccpe
  def createEvent(phoneStatusEvent: PhoneHintStatusEvent): JsValue =
    new EventBuilder(WSMsgType.PhoneHintStatusEvent) `asEvent` phoneStatusEvent
  def createEvent(uqm: UserQueueDefaultMembership): JsValue =
    new EventBuilder(WSMsgType.UserQueueDefaultMembership) `asEvent` uqm
  def createEvent(uqm: UsersQueueDefaultMembership): JsValue =
    new EventBuilder(WSMsgType.UsersQueueDefaultMembership) `asEvent` uqm
  def createEvent(token: AuthenticationToken): JsValue =
    new EventBuilder(WSMsgType.AuthenticationToken) `asEvent` token
  def createEvent(r: UserRight): JsValue =
    new EventBuilder(WSMsgType.RightProfile) `asEvent` r
  def createEvent(ccHist: CustomerCallHistoryResponseWithId): JsValue =
    new EventBuilder(
      WSMsgType.CustomerCallHistoryResponseWithId
    ) `asEvent` ccHist
  def createEvent(msg: BrowserEventEnvelope): JsValue =
    new EventBuilder(WSMsgType.FlashTextEvent) `asEvent` msg
  def createEvent(msg: UserDisplayName): JsValue =
    new EventBuilder(WSMsgType.UserDisplayName) `asEvent` msg
  def createEvent(iceCfg: IceConfig): JsValue =
    new EventBuilder(WSMsgType.IceConfig) `asEvent` iceCfg
  def createEvent(videoEventStatus: VideoStatusEvent): JsValue =
    new EventBuilder(WSMsgType.VideoStatusEvent) `asEvent` videoEventStatus
  def createEvent(videoInvitation: VideoInvitationEvent): JsValue =
    videoInvitation match {
      case MeetingRoomInvite(_, _, _, _, _) =>
        new EventBuilder(
          WSMsgType.MeetingRoomInvite
        ) `asEvent` videoInvitation
      case MeetingRoomInviteAckReply(_, _, _, _) =>
        new EventBuilder(
          WSMsgType.MeetingRoomInviteAck
        ) `asEvent` videoInvitation
      case MeetingRoomInviteResponse(_, _, _, _) =>
        new EventBuilder(
          WSMsgType.MeetingRoomInviteResponse
        ) `asEvent` videoInvitation
    }
  def createUserPreferenceEvent(
      userPreferencesUpdate: List[UserPreference]
  ): JsValue =
    new EventBuilder(WSMsgType.UserPreference) `asEvent` userPreferencesUpdate

  def createWebRTCCommand(cmd: WSWebRTCCmd): JsValue =
    new EventBuilder(WSMsgType.WebRTCCmd) `asEvent` cmd

  def createEvent(agentError: IpbxCommandResponse): JsValue = {
    agentError.getError_string match {
      case "agent_login_invalid_exten" =>
        createError(WSMsgType.AgentError, "PhoneNumberUnknown")
      case "agent_login_exten_in_use" =>
        createError(WSMsgType.AgentError, "PhoneNumberAlreadyInUse")
      case _ =>
        createError(
          WSMsgType.AgentError,
          s"Unknown:${agentError.getError_string}"
        )
    }
  }

  def createEvent(userConfigUpdate: UserConfigUpdate): JsValue =
    new EventBuilder(WSMsgType.UserConfigUpdate) `asEvent` userConfigUpdate

  def createError(
      errorType: WSMsgType,
      errorMsg: String,
      data: Map[String, String] = Map()
  ): JsValue = {
    val errData =
      (Map(ErrorKey -> errorMsg) ++ data).view.mapValues(JsString.apply).toMap
    val js = JsObject(errData)
    new EventBuilder(errorType) `asEvent` js
  }

}
