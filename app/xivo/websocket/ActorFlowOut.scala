/*
 * Copyright (C) 2009-2018 Lightbend Inc. <https://www.lightbend.com>
 *   adapted by Avencall 2021
 */
package xivo.websocket

import org.apache.pekko.Done
import org.apache.pekko.actor._
import org.apache.pekko.stream._
import org.apache.pekko.stream.scaladsl.{Flow, Keep, RestartFlow, Sink, Source}
import play.api.Logger

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}
import xivo.xuc.WebSocketConfig

/** Provides a flow that is handled by an actor.
  *
  * See https://github.com/akka/akka/issues/16985.
  *
  * This modification adds an acknowledge message which is sent back to the
  * sender to notify that the message has been published to the connected web
  * socket and that the is ready for another message. It prevents dropping new
  * messages in case the buffer is full.
  *
  * See https://github.com/playframework/playframework/issues/6246
  */
object ActorFlowOut {

  case object ActorFlowOutAck

  def actorRef[In, Out](
      props: ActorRef => Props,
      config: WebSocketConfig,
      bufferSize: Int = 16,
      overflowStrategy: OverflowStrategy = OverflowStrategy.dropNew
  )(implicit
      factory: ActorRefFactory,
      mat: Materializer,
      ec: ExecutionContext,
      system: ActorSystem
  ): Flow[In, Out, ?] = {
    val log = Logger(getClass.getName)

    val (wsActorOutlet, wsActorPublisher) = Source
      .actorRef(
        completionMatcher = { case Done =>
          log.debug(s"WsActor publisher successfully completed")
          CompletionStrategy.immediately
        },
        failureMatcher = { case Status.Failure(e) =>
          log.error(s"WsActor publisher completed with error $e")
          e
        },
        bufferSize,
        overflowStrategy
      )
      .toMat(Sink.asPublisher(false))(Keep.both)
      .run()

    val wsActorInlet = factory.actorOf(Props(new Actor {
      val wsActor: ActorRef =
        context.watch(context.actorOf(props(wsActorOutlet), "WsActorImpl"))

      def receive: Receive = {
        case Status.Success(_) | Status.Failure(_) =>
          wsActor ! PoisonPill
        case Terminated(_) => context.stop(self)
        case other         => wsActor ! other
      }

      override def supervisorStrategy: SupervisorStrategy =
        OneForOneStrategy() { case e =>
          log.error(s"WsActor inlet supervision triggered stop with error $e")
          SupervisorStrategy.Stop
        }
    }))

    val slowRestartAbleFlow =
      RestartFlow.withBackoff(
        RestartSettings(
          minBackoff = 5.seconds,
          maxBackoff = 15.seconds,
          randomFactor = 0.2
        )
          .withMaxRestarts(3, 60.seconds)
      ) { () =>
        Flow[In]
          .throttle(
            elements = config.webSocketMessagePerDuration,
            per = 30.seconds,
            maximumBurst = config.webSocketMaxBurst,
            ThrottleMode.Enforcing
          )
          .recover { case e: RateExceededException =>
            log.error(s"WebSocket exceeded rate limit $e")
            wsActorOutlet ! WebSocketEvent.createError(
              WSMsgType.Error,
              e.getMessage
            )
          }
      }

    val wsActorAsSink = Sink
      .actorRef(
        ref = wsActorInlet,
        onCompleteMessage = Status.Success(()),
        onFailureMessage = { e =>
          log.error(s"WsActor inlet completed with error $e")
          Status.Failure(e)
        }
      )

    val wsActorAsSource = Source.fromPublisher(wsActorPublisher)

    val ackFlow = Flow[Out].map { elem =>
      wsActorInlet ! ActorFlowOutAck
      elem
    }

    val flowFromSinkAndSource = Flow
      .fromSinkAndSource(wsActorAsSink, wsActorAsSource)
      .via(ackFlow)

    slowRestartAbleFlow
      .via(flowFromSinkAndSource)
      .watchTermination()((_, future) =>
        future.onComplete {
          case Failure(e) =>
            wsActorOutlet ! Status.Failure(e)
            log.error(
              s"Websocket flow execution completed with error ${e.getMessage}"
            )
          case Success(_) =>
            wsActorOutlet ! Done
            log.info("Websocket flow execution completed successfully")
        }
      )
  }
}
