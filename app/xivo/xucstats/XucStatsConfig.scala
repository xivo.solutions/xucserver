package xivo.xucstats

import com.google.inject.{ImplementedBy, Inject, Singleton}
import models.{ConfigurationEntry, ConfigurationIntSetEntry, XucConfiguration}
import play.api.Configuration

@ImplementedBy(classOf[XucStatsConfig])
trait XucBaseStatsConfig {
  def resetSchedule: String
  val initFromMidnight: Boolean
  val aggregationPeriod: Long
  val statsLogReporter: Boolean
  val statsLogReporterPeriod: Long
  val queuesStatTresholdsInSec: Set[Int]
  def getValue(path: String): Option[XucConfiguration]
}

@Singleton
class XucStatsConfig @Inject() (configuration: Configuration)
    extends XucBaseStatsConfig {
  def resetSchedule: String =
    configuration
      .getOptional[String]("xucstats.resetSchedule")
      .getOrElse("0 0 0 * * ?")
  val initFromMidnight: Boolean = configuration
    .getOptional[Boolean]("xucstats.initFromMidnight")
    .getOrElse(false)

  val aggregationPeriod: Long =
    configuration.getOptional[Long]("xucstats.aggregationPeriod").getOrElse(1)

  val statsLogReporter: Boolean =
    configuration.getOptional[Boolean]("metrics.logReporter").getOrElse(true)
  val statsLogReporterPeriod: Long =
    configuration.getOptional[Long]("metrics.logReporterPeriod").getOrElse(5)

  val queuesStatTresholdsInSec: Set[Int] = configuration
    .getOptional[Seq[Int]]("xucstats.queues.statTresholdsInSec")
    .map(_.toList.filter(_ > 0).toSet)
    .getOrElse(Set.empty) + 15

  def getValue(path: String): Option[XucConfiguration] =
    path match {
      case "queues.statTresholdsInSec" =>
        Option(
          ConfigurationIntSetEntry("xucstats." + path, queuesStatTresholdsInSec)
        )
      case _ =>
        configuration
          .getOptional[String]("xucstats." + path)
          .map(v => ConfigurationEntry("xucstats." + path, v))

    }
}
