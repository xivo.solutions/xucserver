package xivo.services

import com.google.inject.Inject
import com.google.inject.name.Named
import models.{DirSearchResult, ResultType, Token, XivoUser}
import org.apache.pekko.actor.{
  Actor,
  ActorLogging,
  ActorRef,
  Cancellable,
  Props
}
import play.api.libs.json.*
import play.api.libs.ws.WSRequest
import services.ActorIds
import services.config.ConfigRepository
import services.directory.DirectoryTransformer.RawDirectoryResult
import services.request.*
import xivo.network.XiVOWS
import xivo.services.XivoDirectory.*
import xivo.services.XivoDirectory.Action.Action
import xivo.xuc.XucConfig

import java.net.URLEncoder
import scala.concurrent.Await

object XivoDirectory {
  sealed trait XivoDirectoryMsg

  sealed trait DirectoryResult extends XivoDirectoryMsg {
    val result: DirSearchResult
    val toContactSheet: Boolean
  }
  case class DirLookupResult(
      result: DirSearchResult,
      toContactSheet: Boolean = false
  ) extends XucRequest
      with DirectoryResult
  case class Favorites(
      result: DirSearchResult,
      toContactSheet: Boolean = false
  ) extends XucRequest
      with DirectoryResult
  object Action extends Enumeration {
    type Action = Value
    val Added: XivoDirectory.Action.Value      = Value
    val AddFail: XivoDirectory.Action.Value    = Value
    val Removed: XivoDirectory.Action.Value    = Value
    val RemoveFail: XivoDirectory.Action.Value = Value
    implicit val actionWrites: Writes[Action] = (`enum`: Action) =>
      JsString(`enum`.toString)
  }

  case class FavoriteUpdated(action: Action, contactId: String, source: String)
      extends XucRequest
      with XivoDirectoryMsg
  object FavoriteUpdated {
    implicit val writes: Writes[FavoriteUpdated] = (f: FavoriteUpdated) =>
      JsObject(
        Seq(
          "action"     -> JsString(f.action.toString),
          "contact_id" -> JsString(f.contactId),
          "source"     -> JsString(f.source)
        )
      )
  }

  case object TokenRetrievalTimeout extends XucRequest with XivoDirectoryMsg
  case object XivoAuthTimeout       extends XucRequest with XivoDirectoryMsg
  case object XivoAuthError         extends XucRequest with XivoDirectoryMsg
  case object XivoDirdTimeout       extends XucRequest with XivoDirectoryMsg
}

class XivoDirectory @Inject() (
    @Named(ActorIds.XivoAuthenticationId) xauth: ActorRef,
    @Named(ActorIds.DirectoryTransformerId) dirTransformer: ActorRef,
    configRepo: ConfigRepository,
    xivoWS: XiVOWS,
    config: XucConfig
) extends Actor
    with ActorLogging {

  def receive: Receive = {
    case UserBaseRequest(ref, request: DirectoryRequest, user) =>
      configRepo.getCtiUser(user.username) match {
        case None =>
          log.error(s"Got directory look up for unknown user ${user.username}")
        case Some(user) =>
          log.debug(s"Received $request by user ${user.id}")
          retrieveToken(user, BaseRequest(ref, request))
      }

    case (
          t: Token,
          BaseRequest(ref, DirectoryLookUp(term)),
          requesterUserId: Long
        ) =>
      search(term, t, false) match {
        case r: DirLookupResult =>
          dirTransformer ! RawDirectoryResult(ref, r, requesterUserId)
        case other =>
          ref ! other
      }

    case (
          t: Token,
          BaseRequest(ref, SearchContacts(term)),
          requesterUserId: Long
        ) =>
      search(term, t) match
        case r: DirLookupResult =>
          dirTransformer ! RawDirectoryResult(ref, r, requesterUserId)
        case other =>
          ref ! other

    case (
          t: Token,
          BaseRequest(ref, GetFavorites),
          requesterUserId: Long
        ) =>
      getFavorites(t, false) match {
        case f: Favorites =>
          dirTransformer ! RawDirectoryResult(ref, f, requesterUserId)
        case other =>
          ref ! other
      }

    case (
          t: Token,
          BaseRequest(ref, GetFavoriteContacts),
          requesterUserId: Long
        ) =>
      getFavorites(t) match {
        case f: Favorites =>
          dirTransformer ! RawDirectoryResult(ref, f, requesterUserId)
        case other =>
          ref ! other
      }

    case (
          t: Token,
          BaseRequest(ref, AddFavorite(contactId, directory)),
          requesterUserId: Long
        ) =>
      setFavorite(contactId, directory, t) match {
        case r: FavoriteUpdated =>
          log.debug(s"sending $r")
          dirTransformer ! RawDirectoryResult(ref, r, requesterUserId)
        case other =>
          log.debug(s"sending $other")
          ref ! other
      }

    case (
          t: Token,
          BaseRequest(ref, RemoveFavorite(contactId, directory)),
          requesterUserId: Long
        ) =>
      removeFavorite(contactId, directory, t) match {
        case r: FavoriteUpdated =>
          dirTransformer ! RawDirectoryResult(ref, r, requesterUserId)
        case other =>
          ref ! other
      }

    case any =>
      log.debug(s"Received unprocessed message $any")
  }

  private def retrieveToken(user: XivoUser, baseRequest: BaseRequest): Unit = {
    val replyTo = self
    context.actorOf(Props(new Actor() {
      xauth ! XivoAuthentication.GetCtiToken(user.id)

      def receive: Receive = {
        case TokenRetrievalTimeout =>
          log.error("Get token timeout")
          baseRequest.requester ! XivoAuthTimeout
          context.stop(self)

        case t: Token =>
          log.debug(s"Got token, searching")
          replyTo ! ((t, baseRequest, user.id))
          timeout.cancel()
          context.stop(self)

        case any =>
          log.error(s"Received unexpected response $any")
          baseRequest.requester ! XivoAuthError
          context.stop(self)
      }

      import context.dispatcher

      val timeout: Cancellable =
        context.system.scheduler.scheduleOnce(config.defaultWSTimeout) {
          self ! TokenRetrievalTimeout
        }
    }))
  }

  private def search(
      term: String,
      t: Token,
      toContactSheet: Boolean = true
  ): XivoDirectoryMsg = {
    val searchURI =
      s"${config.XivoDir.searchURI}/${config.XivoDir.defaultProfile}${config.XivoDir.searchArg}" + URLEncoder
        .encode(term, "UTF-8")
    val request = xivoWS.get(
      config.xivoHost,
      searchURI,
      headers = Map("X-Auth-Token" -> t.token),
      port = Some(config.XivoDir.port)
    )
    searchForResults(request, t, toContactSheet)
  }

  private def getFavorites(
      t: Token,
      toContactSheet: Boolean = true
  ): XivoDirectoryMsg = {
    val requestURI =
      s"${config.XivoDir.favoriteURI}/${config.XivoDir.defaultProfile}"
    val request = xivoWS.get(
      config.xivoHost,
      requestURI,
      headers = Map("X-Auth-Token" -> t.token),
      port = Some(config.XivoDir.port)
    )
    searchForFavorites(request, t, toContactSheet)
  }

  private def executeSearchRequest(
      request: WSRequest,
      t: Token
  ): DirSearchResult = {
    import context.dispatcher
    Await.result(
      request
        .execute()
        .map(resp => DirSearchResult.parse(resp.json)),
      config.defaultWSTimeout
    )
  }

  private def searchForResults(
      request: WSRequest,
      t: Token,
      toContactSheet: Boolean
  ): XivoDirectoryMsg =
    DirLookupResult(executeSearchRequest(request, t), toContactSheet)

  private def searchForFavorites(
      request: WSRequest,
      t: Token,
      toContactSheet: Boolean
  ): XivoDirectoryMsg =
    Favorites(executeSearchRequest(request, t), toContactSheet)

  private def setFavorite(
      contactId: String,
      directory: String,
      t: Token
  ): XivoDirectoryMsg = {
    val requestURI = s"${config.XivoDir.favoriteURI}/$directory/$contactId"
    val request = xivoWS.put(
      config.xivoHost,
      requestURI,
      headers = Map("X-Auth-Token" -> t.token),
      port = Some(config.XivoDir.port)
    )
    processFavoriteRequest(
      request,
      contactId,
      directory,
      Action.Added,
      Action.AddFail
    )
  }

  private def removeFavorite(
      contactId: String,
      directory: String,
      t: Token
  ): XivoDirectoryMsg = {
    val requestURI = s"${config.XivoDir.favoriteURI}/$directory/$contactId"
    val request = xivoWS.del(
      config.xivoHost,
      requestURI,
      headers = Map("X-Auth-Token" -> t.token),
      port = Some(config.XivoDir.port)
    )
    processFavoriteRequest(
      request,
      contactId,
      directory,
      Action.Removed,
      Action.RemoveFail
    )
  }

  private def processFavoriteRequest(
      request: WSRequest,
      contactId: String,
      directory: String,
      actionSuccess: Action,
      actionFail: Action
  ): XivoDirectoryMsg = {
    val wsResult     = request.execute()
    val searchResult = Await.result(wsResult, config.defaultWSTimeout)
    log.debug(s"Got set favorite result: ${searchResult.status}")
    if (searchResult.status == 204) {
      FavoriteUpdated(actionSuccess, contactId, directory)
    } else {
      FavoriteUpdated(actionFail, contactId, directory)
    }
  }
}
