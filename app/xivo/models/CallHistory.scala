package xivo.models

import org.joda.time.format.{DateTimeFormat, PeriodFormatterBuilder}
import org.joda.time.{DateTime, Period}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import org.joda.time.format.{DateTimeFormatter, PeriodFormatter}

trait CallDetailTrait {
  def start: DateTime
  def duration: Option[Period]
  def srcNum: String
  def dstNum: Option[String]
  def status: CallStatus.CallStatus
  def srcFirstName: Option[String]
  def srcLastName: Option[String]
  def dstFirstName: Option[String]
  def dstLastName: Option[String]
}

case class CallDetail(
    start: DateTime,
    duration: Option[Period],
    srcNum: String,
    dstNum: Option[String],
    status: CallStatus.CallStatus,
    srcFirstName: Option[String] = None,
    srcLastName: Option[String] = None,
    dstFirstName: Option[String] = None,
    dstLastName: Option[String] = None
) extends CallDetailTrait

object CallDetail {

  val format: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  val periodFormat: PeriodFormatter = new PeriodFormatterBuilder()
    .minimumPrintedDigits(2)
    .printZeroAlways()
    .appendHours()
    .appendSeparator(":")
    .appendMinutes()
    .appendSeparator(":")
    .appendSeconds()
    .toFormatter

  def validate(json: JsValue): JsResult[CallDetail] = json.validate[CallDetail]

  implicit val reads: Reads[CallDetail] =
    ((JsPath \ "start").read[String].map(format.parseDateTime) and
      (JsPath \ "duration")
        .readNullable[String]
        .map(sOpt => sOpt.map(periodFormat.parsePeriod)) and
      (JsPath \ "src_num").read[String] and
      (JsPath \ "dst_num").readNullable[String] and
      (JsPath \ "status")
        .readNullable[String]
        .map(s => CallStatus.withName(s.getOrElse("answered"))) and
      (JsPath \ "src_firstname").readNullable[String] and
      (JsPath \ "src_lastname").readNullable[String] and
      (JsPath \ "dst_firstname").readNullable[String] and
      (JsPath \ "dst_lastname").readNullable[String])(CallDetail.apply)

  implicit val writes: Writes[CallDetail] = new Writes[CallDetail] {
    def writes(call: CallDetail): JsValue =
      Json.obj(
        "start"        -> call.start.toString(format),
        "duration"     -> call.duration.map(_.toString(periodFormat)),
        "srcNum"       -> call.srcNum,
        "dstNum"       -> call.dstNum,
        "status"       -> call.status.toString,
        "srcFirstName" -> call.srcFirstName,
        "srcLastName"  -> call.srcLastName,
        "dstFirstName" -> call.dstFirstName,
        "dstLastName"  -> call.dstLastName
      )
  }
}

case class CallHistory(calls: List[CallDetail])

object CallHistory {
  def validate(json: JsValue): JsResult[CallHistory] =
    json.validate[List[CallDetail]].map(CallHistory.apply)

  implicit val writes: Writes[CallHistory] = new Writes[CallHistory] {
    override def writes(ch: CallHistory): JsValue = JsArray(
      ch.calls.map(Json.toJson(_))
    )
  }
}
