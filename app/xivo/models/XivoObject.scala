package xivo.models

import java.util.Date

import org.xivo.cti.message.{
  QueueStatistics,
  Sheet,
  UserConfigUpdate => CtiUserConfigUpdate,
  UserStatusUpdate
}
import org.xivo.cti.model.sheet.{Profile, SheetInfo, User}
import org.xivo.cti.model.*
import play.api.libs.json.*

import scala.jdk.CollectionConverters.*

object XivoObject {

  object ObjectType extends Enumeration {
    type ObjectType = Value
    val Queue: XivoObject.ObjectType.Value = Value
  }

  import ObjectType._
  case class ObjectDefinition(
      objectType: ObjectType,
      objectRef: Option[Int] = None
  ) {
    def hasObjectRef: Boolean = objectRef.isDefined
  }

  implicit val actionWrites: Writes[Action] = (o: Action) =>
    Json.obj(
      "name"       -> o.getName,
      "parameters" -> o.getParameters
    )

  implicit val userStatusWrites: Writes[UserStatus] = new Writes[UserStatus] {
    def writes(o: UserStatus): JsValue =
      Json.obj(
        "name"     -> o.getName,
        "color"    -> Option(o.getColor),
        "longName" -> Option(o.getLongName),
        "actions"  -> o.getActions.asScala
      )
  }

  implicit val meetmeMemberWrites: Writes[MeetmeMember] =
    (o: MeetmeMember) =>
      Json.obj(
        "joinOrder" -> o.getJoinOrder,
        "joinTime"  -> o.getJoinTime,
        "muted"     -> o.isMuted,
        "name"      -> o.getName,
        "number"    -> o.getNumber
      )

  implicit val meetmeWrites: Writes[Meetme] = new Writes[Meetme] {
    def writes(o: Meetme): JsValue =
      Json.obj(
        "number"      -> o.getNumber,
        "name"        -> o.getName,
        "pinRequired" -> o.isPinRequired,
        "startTime"   -> o.getStartTime,
        "members"     -> o.getMembers.asScala
      )
  }

  implicit val sheetInfoWrites: Writes[SheetInfo] = (o: SheetInfo) =>
    Json.obj(
      "type"  -> o.getType,
      "name"  -> o.getName,
      "order" -> o.getOrder.longValue,
      "value" -> o.getValue
    )

  implicit val userWrites: Writes[User] = (o: User) =>
    Json.obj(
      "sheetInfo" -> o.getSheetInfo.asScala
    )

  implicit val profileWrites: Writes[Profile] = (o: Profile) =>
    Json.obj(
      "user" -> o.getUser
    )

  implicit val payloadWrites: Writes[Payload] = (o: Payload) =>
    Json.obj(
      "profile" -> o.getProfile
    )

  implicit val sheetWrites: Writes[Sheet] = new Writes[Sheet] {

    implicit val timeWrites: Writes[Date] =
      Writes.dateWrites("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

    def writes(o: Sheet): JsValue =
      Json.obj(
        "timenow"    -> o.timenow,
        "compressed" -> Option(o.compressed),
        "serial"     -> o.serial,
        "payload"    -> Option(o.payload),
        "channel"    -> o.channel
      )
  }

  implicit val counterWrites: Writes[Counter] = (o: Counter) =>
    Json.obj(
      "statName" -> o.getStatName.toString(),
      "value"    -> o.getValue
    )

  implicit val queueStatisticsWrites: Writes[QueueStatistics] =
    (o: QueueStatistics) =>
      Json.obj(
        "queueId"  -> o.getQueueId,
        "counters" -> o.getCounters.asScala
      )

  implicit val phoneHintStatusWrites: Writes[PhoneHintStatus] =
    (o: PhoneHintStatus) =>
      Json.obj(
        "status" -> o.toString()
      )

  implicit val userStatusUpdateWrites: Writes[UserStatusUpdate] =
    (o: UserStatusUpdate) =>
      Json.obj(
        "status" -> o.getStatus
      )

  implicit val userConfigUpdateWrites: Writes[CtiUserConfigUpdate] =
    (o: CtiUserConfigUpdate) =>
      Json.obj(
        "userId"             -> o.getUserId,
        "dndEnabled"         -> o.isDndEnabled,
        "naFwdEnabled"       -> o.isNaFwdEnabled,
        "naFwdDestination"   -> Option(o.getNaFwdDestination),
        "uncFwdEnabled"      -> o.isUncFwdEnabled,
        "uncFwdDestination"  -> Option(o.getUncFwdDestination),
        "busyFwdEnabled"     -> o.isBusyFwdEnabled,
        "busyFwdDestination" -> Option(o.getBusyFwdDestination),
        "firstName"          -> Option(o.getFirstName),
        "lastName"           -> Option(o.getLastName),
        "fullName"           -> Option(o.getFullName),
        "mobileNumber"       -> Option(o.getMobileNumber),
        "agentId"            -> Option(o.getAgentId),
        "lineIds"            -> o.getLineIds.asScala.map(_.intValue),
        "voiceMailId"        -> Option(o.getVoiceMailId),
        "voiceMailEnabled"   -> Option(o.isVoiceMailEnabled)
      )

}
