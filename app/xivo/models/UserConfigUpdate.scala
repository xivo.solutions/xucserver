package xivo.models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Json, Writes}

case class UserConfigUpdated(userId: Long, agentId: Option[Long] = None)
case class UserConfigUpdate(
    userId: Long,
    firstName: String,
    lastName: String,
    fullName: String,
    agentId: Long,
    dndEnabled: Boolean,
    naFwdEnabled: Boolean,
    naFwdDestination: String,
    uncFwdEnabled: Boolean,
    uncFwdDestination: String,
    busyFwdEnabled: Boolean,
    busyFwdDestination: String,
    mobileNumber: String,
    lineIds: List[Int],
    voiceMailId: Long,
    voiceMailEnabled: Boolean
)

object UserConfigUpdate {
  implicit val writes: Writes[UserConfigUpdate] = (
    (JsPath \ "userId").write[Long] and
      (JsPath \ "firstName").write[String] and
      (JsPath \ "lastName").write[String] and
      (JsPath \ "fullName").write[String] and
      (JsPath \ "agentId").write[Long] and
      (JsPath \ "dndEnabled").write[Boolean] and
      (JsPath \ "naFwdEnabled").write[Boolean] and
      (JsPath \ "naFwdDestination").write[String] and
      (JsPath \ "uncFwdEnabled").write[Boolean] and
      (JsPath \ "uncFwdDestination").write[String] and
      (JsPath \ "busyFwdEnabled").write[Boolean] and
      (JsPath \ "busyFwdDestination").write[String] and
      (JsPath \ "mobileNumber").write[String] and
      (JsPath \ "lineIds").write[List[Int]] and
      (JsPath \ "voiceMailId").write[Long] and
      (JsPath \ "voiceMailEnabled").write[Boolean]
  )(o =>
    (
      o.userId,
      o.firstName,
      o.lastName,
      o.fullName,
      o.agentId,
      o.dndEnabled,
      o.naFwdEnabled,
      o.naFwdDestination,
      o.uncFwdEnabled,
      o.uncFwdDestination,
      o.busyFwdEnabled,
      o.busyFwdDestination,
      o.mobileNumber,
      o.lineIds,
      o.voiceMailId,
      o.voiceMailEnabled
    )
  )
}
