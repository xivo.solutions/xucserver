package xivo.models

import play.api.libs.json.*
import xivo.models.MembershipStatus.Available

case class UserGroupMember(userId: Long, membershipStatus: MembershipStatus)

object UserGroupMember {
  implicit val userGroupMemberReads: Reads[UserGroupMember] =
    ((JsPath \ "userid")
      .read[Long])
      .map(userId => UserGroupMember(userId, Available))
}

enum MembershipStatus:
  case Available, Exited, Paused

object MembershipStatus {

  implicit val membershipStatusWrites: Writes[MembershipStatus] = Writes {
    status =>
      JsString(status.toString)
  }
}
