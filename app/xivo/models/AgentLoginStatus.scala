package xivo.models

import java.util.Date

import anorm.SqlParser._
import anorm._
import com.google.inject.{ImplementedBy, Inject}
import org.joda.time.DateTime
import play.api.db.Database

case class AgentLoginStatus(
    id: Agent.Id,
    phoneNumber: String,
    loginDate: DateTime
)

@ImplementedBy(classOf[AgentLoginStatusDaoDb])
trait AgentLoginStatusDao {
  def getLoginStatus(id: Agent.Id): Option[AgentLoginStatus]
}

class AgentLoginStatusDaoDb @Inject() (db: Database)
    extends AgentLoginStatusDao {

  val simple: RowParser[AgentLoginStatus] = {
    get[Long]("agent_id") ~
      get[String]("extension") ~
      get[Date]("login_at") map { case id ~ phoneNumber ~ loginDate =>
        AgentLoginStatus(id, phoneNumber, new DateTime(loginDate))
      }
  }

  def getLoginStatus(id: Agent.Id): Option[AgentLoginStatus] = {
    db.withConnection { implicit c =>
      SQL(
        "select agent_id, extension, login_at from agent_login_status where agent_id = {id}"
      ).on("id" -> id).as(simple.singleOpt)
    }
  }

}
