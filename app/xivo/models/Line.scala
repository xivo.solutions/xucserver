package xivo.models

import anorm.Column.columnToArray
import anorm.SqlParser.{get => aGet}
import anorm.{~, SQL}
import com.google.inject.{ImplementedBy, Inject}
import play.api.Logger
import play.api.db.Database
import play.api.http.Status.*
import play.api.libs.json.*
import services.calltracking.SipDriver.SipDriver
import services.config.ConfigDispatcher
import services.config.ConfigDispatcher.DeviceType
import xivo.models.Line.Id
import xivo.network.XiVOWS
import xivo.xuc.XucBaseConfig
import xivo.xucami.models.CallerId
import anorm.as

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps
import anorm.RowParser
import play.api.libs.functional.syntax.toFunctionalBuilderOps

import scala.concurrent.duration.FiniteDuration

case class XivoDevice(id: String, ip: Option[String], vendor: String)
case class XivoLine(
    id: Int,
    context: String,
    protocol: String,
    name: String,
    device_id: Option[String]
)
case class XivoSipEndpointLink(line_id: Int, endpoint_id: Int, endpoint: String)
case class XivoEndpoint(username: String, secret: String)

trait XivoFeature {
  def interface: String
  def trackingInterface: String = interface
}

case class Line(
    id: Int,
    context: String,
    protocol: String,
    name: String,
    dev: Option[XivoDevice],
    password: Option[String] = None,
    xivoIp: String,
    webRTC: Boolean = false,
    number: Option[String] = None,
    registrar: Option[String] = None,
    ua: Boolean = false,
    callerId: CallerId,
    driver: SipDriver,
    mobileApp: Boolean = false
) extends XivoFeature {
  def interface: String = {
    protocol.toUpperCase match {
      case "CUSTOM" => name.capitalize.replaceAll("/n", "")
      case "SIP"    => s"${driver.toString.toUpperCase}/$name"
      case _        => s"${protocol.toUpperCase}/$name"
    }
  }

  override def trackingInterface: String = interface + "-"
  def isCustom: Boolean                  = protocol.toUpperCase == "CUSTOM"
}

case class Trunk(
    id: Int,
    context: String,
    protocol: String,
    name: String,
    driver: SipDriver
) extends XivoFeature {
  def interface: String = {
    protocol.toUpperCase match {
      case "SIP" => s"${driver.toString.toUpperCase}/$name"
      case _     => s"${protocol.toUpperCase}/$name"
    }
  }
}

case class MediaServerTrunk(mdsName: String, driver: SipDriver)
    extends XivoFeature {
  def interface: String = {
    s"${driver.toString.toUpperCase}/$mdsName"
  }
}

case object DAHDITrunk extends XivoFeature {
  def interface = "DAHDI"
}

case object LocalChannelFeature extends XivoFeature {
  def interface = "Local/"
}

case class AnonymousChannelFeature(driver: SipDriver) extends XivoFeature {
  def interface = s"${driver.toString.toUpperCase}/anonymous-"
}

@ImplementedBy(classOf[LineFactoryImpl])
trait LineFactory {
  def get(Id: Line.Id): Option[Line]
  def get(endpoint: SipEndpoint): Option[Line]
  def get(Id: Line.Id, deviceType: DeviceType): Option[Line]
  def all(): Future[List[Line]]
  def trunks: Future[List[Trunk]]
}

object Line {
  type Id         = Long
  type SipOptions = Array[List[String]]

  private def extractWebRTCTuple(opts: SipOptions): Option[List[String]] = {
    opts.find(p => p.contains("webrtc"))
  }

  private def checkSipOptions(
      options: Option[SipOptions],
      matches: SipOptions => Boolean
  ): Boolean = {
    options match {
      case None       => false
      case Some(opts) => matches(opts)
    }
  }

  def isWebRTC(sipOptions: Option[SipOptions]): Boolean = {
    def matches(opts: SipOptions): Boolean =
      extractWebRTCTuple(opts) match {
        case Some(List("webrtc", "yes")) => true
        case Some(List("webrtc", "ua"))  => true
        case _                           => false
      }

    checkSipOptions(sipOptions, matches)
  }

  def isUa(sipOptions: Option[SipOptions]): Boolean = {
    def matches(opts: SipOptions): Boolean =
      extractWebRTCTuple(opts) match {
        case Some(List("webrtc", "ua")) => true
        case _                          => false
      }

    checkSipOptions(sipOptions, matches)
  }
}

object Endpoint {
  type Id = Long
}

sealed trait Endpoint {
  val id: Endpoint.Id
}
case class SipEndpoint(id: Endpoint.Id) extends Endpoint

class LineFactoryImpl @Inject() (
    db: Database,
    xivoWs: XiVOWS,
    xucConfig: XucBaseConfig
) extends LineFactory {
  val waitResult: FiniteDuration = 10 seconds

  val logger: Logger = Logger(getClass.getName)
  implicit val context: ExecutionContext =
    scala.concurrent.ExecutionContext.Implicits.global
  implicit val lineReads: Reads[XivoLine] = (
    (JsPath \ "id").read[Int] and
      (JsPath \ "context").read[String] and
      (JsPath \ "protocol").read[String] and
      (JsPath \ "name").read[String] and
      (JsPath \ "device_id").readNullable[String]
  )(XivoLine.apply)
  implicit val deviReads: Reads[XivoDevice] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "ip").readNullable[String] and
      (JsPath \ "vendor").read[String]
  )(XivoDevice.apply)
  implicit val sipEndpointLinkReads: Reads[XivoSipEndpointLink] = (
    (JsPath \ "line_id").read[Int] and
      (JsPath \ "endpoint_id").read[Int] and
      (JsPath \ "endpoint").read[String]
  )(XivoSipEndpointLink.apply)
  implicit val endpointReads: Reads[XivoEndpoint] = (
    (JsPath \ "username").read[String] and
      (JsPath \ "secret").read[String]
  )(XivoEndpoint.apply)

  def query(): String = """
    select
      l.id, l.context, l.name, l.protocol, l.device, l.number,
      sip.secret, sip.options, l.configregistrar, u.callerid, u.mobile_push_token
    from
      linefeatures l
    inner join user_line ul on ul.line_id=l.id and ul.main_user=true and ul.main_line=true
    inner join userfeatures u on ul.user_id=u.id
    left join
      usersip sip on l.protocolid=sip.id
    where l.name is not null and l.protocol is not null"""

  def query(id: Line.Id): String                = query() + s" AND l.id='$id'"
  def querySipEndpoint(id: Endpoint.Id): String = query() + s" AND sip.id='$id'"

  val trunk: RowParser[Trunk] = {
    aGet[Int]("id") ~
      aGet[String]("context") ~
      aGet[String]("protocol") ~
      aGet[String]("name") map { case id ~ context ~ protocol ~ name =>
        Trunk(id, context, protocol, name, xucConfig.sipDriver)
      }
  }

  val simple: RowParser[(Line, Option[String])] = {
    aGet[Int]("id") ~
      aGet[String]("context") ~
      aGet[String]("name") ~
      aGet[String]("protocol") ~
      aGet[Option[String]]("number") ~
      aGet[Option[String]]("device") ~
      aGet[Option[String]]("secret") ~
      aGet[Option[Array[List[String]]]]("options") ~
      aGet[Option[String]]("configregistrar") ~
      aGet[Option[String]]("callerid") ~
      aGet[Option[String]]("mobile_push_token") map {
        case id ~ context ~ linename ~ protocol ~ number ~ deviceId ~ sipsecret ~ sipOptions ~ registrar ~ callerid ~ mobile_push_token =>
          (
            Line(
              id,
              context,
              protocol,
              linename,
              None,
              sipsecret,
              xucConfig.xivoHost,
              webRTC = Line.isWebRTC(sipOptions),
              number = number,
              registrar,
              ua = Line.isUa(sipOptions),
              callerId = CallerId.from(callerid, number),
              xucConfig.sipDriver,
              mobileApp = mobile_push_token.isDefined
            ),
            deviceId
          )
      }
  }

  def all(): Future[List[Line]] =
    Future(
      db.withConnection { implicit c =>
        SQL(query())
          .as(simple.*)
          .map(t => enrichDevice(t._1, t._2))
          .map(enrichUaAsDefaultDevice)
      }
    )

  def trunks: Future[List[Trunk]] =
    Future(
      db.withConnection { implicit c =>
        SQL("""SELECT t.id, t.protocol, u.context, u.name
        FROM trunkfeatures t
        INNER JOIN usersip u ON t.protocolid=u.id
        WHERE u.context is not null""").as(trunk.*)
      }
    )

  def enrichDevice(line: Line, deviceId: Option[String]): Line =
    deviceId match {
      case Some(id) if !id.isEmpty =>
        val result = line.copy(dev = getDevice(id))
        logger.debug(s"Returning line: $result for id $id")
        result
      case _ =>
        logger.debug(s"Returning line: $line for id ${line.id}")
        line
    }

  def enrichUaAsDefaultDevice(line: Line): Line = {
    if (line.ua) line.copy(name = line.name + "_w", dev = None) else line
  }

  def enrichUaAsPhoneDevice(line: Line): Line = {
    if (line.ua) line.copy(webRTC = false) else line
  }

  def get(id: Line.Id): Option[Line] =
    get(id, deviceType = ConfigDispatcher.TypeDefaultDevice)

  def get(id: Line.Id, deviceType: DeviceType): Option[Line] = {
    val row = db.withConnection { implicit c =>
      SQL(query(id)).as(simple.*).headOption
    }
    processRow(id, deviceType, row)
  }

  def get(endpoint: SipEndpoint): Option[Line] = {
    val row = db.withConnection { implicit c =>
      SQL(querySipEndpoint(endpoint.id)).as(simple.*).headOption
    }
    processRow(endpoint.id, ConfigDispatcher.TypeDefaultDevice, row)
  }

  private def processRow(
      id: Id,
      deviceType: DeviceType,
      row: Option[(Line, Option[String])]
  ): Option[Line] = {
    row match {
      case Some((line: Line, deviceId: Option[String])) =>
        logger.debug(s"Got line from database: $line, $deviceId")
        Some(enrichDevice(line, deviceId)).map { l =>
          if (l.ua) processUniqueAccountLine(deviceType, l)
          else l
        }
      case None =>
        logger.info(s"Unable to get line from database, lineId: $id")
        None
    }
  }

  private def processUniqueAccountLine(deviceType: DeviceType, l: Line) = {
    deviceType match {
      case ConfigDispatcher.TypePhoneDevice   => enrichUaAsPhoneDevice(l)
      case ConfigDispatcher.TypeDefaultDevice => enrichUaAsDefaultDevice(l)
    }
  }

  def getDevice(id: String): Option[XivoDevice] = {
    val responseFuture = xivoWs.withWS(s"devices/$id").get()
    val resultFuture = responseFuture map { response =>
      if (response.status == OK) {
        response.json.validate[XivoDevice] match {
          case JsError(e) =>
            logger.warn(s"Unparsable JSON received for device $id : $e")
            None
          case JsSuccess(device, _) => Some(device)
        }
      } else {
        logger.warn(
          s"Webservice get device for id: $id failed, response: $response"
        )
        None
      }
    }
    Await.result(resultFuture, waitResult)
  }
}
