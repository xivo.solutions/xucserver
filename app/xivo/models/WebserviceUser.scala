package xivo.models

sealed trait WebserviceUserAction

case class WebserviceUserActionCreated(login: String)
    extends WebserviceUserAction
case class WebserviceUserActionEdited(login: String)
    extends WebserviceUserAction
case object WebserviceUsersActionReload extends WebserviceUserAction
