package xivo.models

import anorm.*
import anorm.SqlParser.*
import com.google.inject.{ImplementedBy, Inject}
import play.api.Logger
import play.api.db.Database
import play.api.libs.functional.syntax.*
import play.api.libs.json.*
import services.config.ConfigServerRequester
import xivo.network.XiVOWS

import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.concurrent.{Await, ExecutionContext}

case class GroupConfig(
    users: List[UserGroupMember],
    groupId: Long,
    groupName: String,
    groupNumber: String
)

object GroupConfig {
  implicit val groupMemberReads: Reads[GroupConfig] = (
    (JsPath \ "members").read[List[UserGroupMember]] and
      (JsPath \ "id").read[Long] and
      (JsPath \ "name").read[String] and
      (JsPath \ "number").read[String]
  )(GroupConfig.apply)
}

@ImplementedBy(classOf[GroupMemberFactoryImpl])
trait GroupMemberFactory {
  def all(): List[GroupConfig]
}

class GroupMemberFactoryImpl @Inject() (
    db: Database,
    xivoWs: XiVOWS,
    configServerRequester: ConfigServerRequester
)(implicit ec: ExecutionContext)
    extends GroupMemberFactory {

  val logger: Logger             = Logger(getClass.getName)
  val waitResult: FiniteDuration = 10.seconds

  def all(): List[GroupConfig] = {
    val futRes = configServerRequester.getAllGroupsMembers

    Await.result(futRes, waitResult)
  }
}
