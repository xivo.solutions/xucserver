package xivo.models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Format, JsPath, Json, Reads, Writes}
import play.api.libs.functional.syntax._

sealed trait MeetingRoom
case object PersonalMeetingRoom  extends MeetingRoom
case object StaticMeetingRoom    extends MeetingRoom
case object TemporaryMeetingRoom extends MeetingRoom

case class MeetingRoomToken(uuid: String, token: String)

object MeetingRoomToken {
  implicit val format: Format[MeetingRoomToken] = (
    (JsPath \ "uuid").format[String] and
      (JsPath \ "token").format[String]
  )(MeetingRoomToken.apply, o => (o.uuid, o.token))
}

case class MeetingRoomAlias(alias: Option[String])
object MeetingRoomAlias {
  implicit val format: Format[MeetingRoomAlias] =
    (JsPath \ "alias")
      .formatNullable[String]
      .inmap(s => MeetingRoomAlias(s), mr => mr.alias)
}
