package xivo.models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.*

case class MediaServerConfig(
    id: Long,
    name: String,
    display_name: String,
    voip_ip: String,
    read_only: Boolean
)

object MediaServerConfig {
  implicit val format: Format[MediaServerConfig] = (
    (JsPath \ "id").format[Long] and
      (JsPath \ "name").format[String] and
      (JsPath \ "display_name").format[String] and
      (JsPath \ "voip_ip").format[String] and
      (JsPath \ "read_only").format[Boolean]
  )(
    MediaServerConfig.apply,
    o => (o.id, o.name, o.display_name, o.voip_ip, o.read_only)
  )
}
