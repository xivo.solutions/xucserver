package xivo.models

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.*

case class UserPreference(
    userId: Long,
    key: String,
    value: String,
    valueType: String
)
case class UserPreferencePayload(
    key: Option[String],
    value: String,
    value_type: String
)

object UserPreferenceKey {
  val PreferredDevice = "PREFERRED_DEVICE"
  val MobileAppInfo   = "MOBILE_APP_INFO"
}

object UserPreferenceType {
  val StringType  = "String"
  val BooleanType = "Boolean"
}

object UserPreference {
  implicit val writes: Writes[UserPreference] = new Writes[UserPreference] {
    override def writes(up: UserPreference): JsValue =
      Json.obj("value" -> up.value, "value_type" -> up.valueType)
  }
  implicit val writesList: Writes[List[UserPreference]] =
    new Writes[List[UserPreference]] {
      override def writes(ups: List[UserPreference]): JsValue = {
        ups.foldLeft(Json.obj())((obj, up) => obj + (up.key -> Json.toJson(up)))
      }
    }
  implicit val reads: Reads[UserPreference] = (
    (JsPath \ "userId").format[Long] and
      (JsPath \ "key").format[String] and
      (JsPath \ "value").format[String] and
      (JsPath \ "valueType").format[String]
  )(UserPreference.apply, o => (o.userId, o.key, o.value, o.valueType))
}

object UserPreferencePayload {
  implicit val format: Format[UserPreferencePayload] = (
    (JsPath \ "key").formatNullable[String] and
      (JsPath \ "value").format[String] and
      (JsPath \ "value_type").format[String]
  )(UserPreferencePayload.apply, o => (o.key, o.value, o.value_type))
}
