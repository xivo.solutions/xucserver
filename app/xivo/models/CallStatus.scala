package xivo.models

object CallStatus extends Enumeration {
  type CallStatus = Value
  val Emitted: Value        = Value("emitted")
  val Answered: Value       = Value("answered")
  val Missed: Value         = Value("missed")
  val Ongoing: Value        = Value("ongoing")
  val Abandoned: Value      = Value("abandoned")
  val DivertCaRatio: Value  = Value("divert_ca_ratio")
  val DivertWaitTime: Value = Value("divert_waittime")
  val Closed: Value         = Value("closed")
  val Full: Value           = Value("full")
  val JoinEmpty: Value      = Value("joinempty")
  val LeaveEmpty: Value     = Value("leaveempty")
  val Timeout: Value        = Value("timeout")
  val ExitWithKey: Value    = Value("exit_with_key")
}
