package xivo.models

import anorm.SqlParser._
import anorm._
import com.google.inject.{ImplementedBy, Inject}
import play.api.db.Database
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class AgentGroup(
    id: Option[Long],
    name: String,
    displayName: Option[String] = None
)

object AgentGroup {
  implicit val agGroupWrites: OWrites[AgentGroup] =
    ((__ \ "id").write[Option[Long]] and
      (__ \ "name").write[String] and
      (__ \ "displayName").write[Option[String]])(agw =>
      (
        agw.id,
        agw.name,
        agw.displayName
      )
    )
}

@ImplementedBy(classOf[AgentGroupFactoryImpl])
trait AgentGroupFactory {
  def all(): List[AgentGroup]
}

class AgentGroupFactoryImpl @Inject() (db: Database) extends AgentGroupFactory {
  val simple: RowParser[AgentGroup] =
    get[Long]("id") ~ get[String]("name") ~ get[Option[String]](
      "description"
    ) map { case id ~ name ~ displayName =>
      AgentGroup(Some(id), name, displayName)
    }

  override def all(): List[AgentGroup] = {
    db.withConnection { implicit c =>
      SQL("select  id, name, description from agentgroup where deleted=0")
        .as(simple.*)
    }
  }

  def toJson(lagentGroup: List[AgentGroup]): JsValue = {
    Json.toJson(lagentGroup)
  }
}
