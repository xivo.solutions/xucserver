package xivo.models

import play.api.libs.json.*
import play.api.libs.functional.syntax.*
import xivo.models.MembershipStatus.Available

case class UserGroup(
    groupId: Long,
    groupName: String,
    groupNumber: String,
    membershipStatus: MembershipStatus
)

type UserGroupUpdate = UserGroup

object UserGroup {

  implicit val gmWrites: OWrites[UserGroup] =
    ((__ \ "groupId").write[Long] and
      (__ \ "groupName").write[String] and
      (__ \ "groupNumber").write[String] and
      (__ \ "membershipStatus").write[MembershipStatus])(gm =>
      (
        gm.groupId,
        gm.groupName,
        gm.groupNumber,
        gm.membershipStatus
      )
    )

  implicit val gmReads: Reads[UserGroup] =
    ((JsPath \ "groupId").read[Long] and
      (JsPath \ "groupName").read[String] and
      (JsPath \ "groupNumber").read[String]).tupled.map {
      case (groupId, groupName, groupNumber) =>
        UserGroup(groupId, groupName, groupNumber, Available)
    }
}
