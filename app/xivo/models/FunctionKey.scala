package xivo.models

import services.XucAmiBus.AmiMessage
import services.XucAmiBus.SetVarActionRequest
import services.XucAmiBus.SetVarRequest

sealed trait FunctionState {
  def value: String
}
case object KeyInUse    extends FunctionState { def value = "INUSE"     }
case object KeyNotInUse extends FunctionState { def value = "NOT_INUSE" }

object FunctionState {
  def inUse(enabled: Boolean): FunctionState =
    if (enabled) KeyInUse else KeyNotInUse
}

case class FunctionKey(exten: String, state: FunctionState) {
  def toAmi: AmiMessage =
    SetVarRequest(
      SetVarActionRequest(s"DEVICE_STATE(Custom:$exten)", state.value)
    )
}
