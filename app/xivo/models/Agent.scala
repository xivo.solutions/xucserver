package xivo.models

import anorm.SqlParser._
import anorm._
import com.google.inject.{ImplementedBy, Inject}
import play.api.db.Database
import play.api.libs.functional.syntax._
import play.api.libs.json.Writes._
import play.api.libs.json._

case class Agent(
    val id: Agent.Id,
    val firstName: String,
    val lastName: String,
    val number: String,
    val context: String,
    val groupId: Long = 0,
    val userId: Long = 0
)

@ImplementedBy(classOf[AgentFactoryImpl])
trait AgentFactory {
  def getById(id: Agent.Id): Option[Agent]
  def moveAgentToGroup(agentId: Long, groupId: Long): Unit
}

object Agent {
  type Id     = Long
  type Number = String

  implicit val agWrites: OWrites[Agent] = ((__ \ "id").write[Long] and
    (__ \ "firstName").write[String] and
    (__ \ "lastName").write[String] and
    (__ \ "number").write[String] and
    (__ \ "context").write[String] and
    (__ \ "groupId").write[Long] and
    (__ \ "userId").write[Long])(agw =>
    (
      agw.id,
      agw.firstName,
      agw.lastName,
      agw.number,
      agw.context,
      agw.groupId,
      agw.userId
    )
  )
}

class AgentFactoryImpl @Inject() (db: Database) extends AgentFactory {

  val simple: RowParser[Agent] = {
    get[Long]("id") ~
      get[String]("firstname") ~
      get[String]("lastname") ~
      get[String]("number") ~
      get[String]("context") ~
      get[Long]("numgroup") ~
      get[Long]("userid") map {
        case id ~ firstname ~ lastname ~ number ~ context ~ groupId ~ userId =>
          Agent(id, firstname, lastname, number, context, groupId, userId)
      }
  }

  def getById(id: Agent.Id): Option[Agent] = {
    db.withConnection { implicit c =>
      SQL("""select a.id,
          |       a.firstname as firstname,
          |       a.lastname as lastname,
          |       a.number as number,
          |       a.context as context,
          |       a.numgroup as numgroup,
          |       u.id as userid
          |from agentfeatures a inner join userfeatures u on u.agentid=a.id
          |where a.id={id} order by u.id limit 1""".stripMargin)
        .on("id" -> id)
        .as(simple.*)
        .headOption
    }

  }

  override def moveAgentToGroup(agentId: Agent.Id, groupId: Agent.Id): Unit = {
    db.withConnection(implicit c =>
      SQL("UPDATE agentfeatures SET numgroup={groupId} WHERE id = {agentId}")
        .on("groupId" -> groupId, "agentId" -> agentId)
        .executeUpdate()
    )
  }
}
