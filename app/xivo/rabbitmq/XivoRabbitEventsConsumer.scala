package xivo.rabbitmq

import org.apache.pekko.actor.ActorRef
import com.rabbitmq.client._
import play.api.Logger
import play.api.libs.json._
import xivo.models._

import scala.util.{Failure, Success, Try}

class XivoRabbitEventsConsumer(
    manager: ActorRef,
    eventFactory: XivoRabbitEventsFactory
) {
  val autoAck              = false
  val log: Logger          = Logger(getClass.getName)
  val channel: Channel     = eventFactory.channel
  val queueName: String    = eventFactory.queueName
  val exchangeName: String = eventFactory.exchangeName
  val consumerTag: String  = eventFactory.consumerTag

  val consumer: DefaultConsumer = new DefaultConsumer(channel) {
    override def handleDelivery(
        consumerTag: String,
        envelope: Envelope,
        properties: AMQP.BasicProperties,
        body: Array[Byte]
    ): Unit = {
      val userId      = properties.getUserId
      val deliveryTag = envelope.getDeliveryTag
      val message     = body.map(_.toChar).mkString
      log.debug(
        s"Received event with body: ${body.map(_.toChar).mkString} sent by: $userId"
      )
      channel.basicAck(deliveryTag, false)
      parseMessage(message).foreach(event => manager ! event)
    }
  }

  channel.basicConsume(queueName, autoAck, consumerTag, consumer)

  def parseMessage(message: String): Option[RabbitEvent] = {
    Try(Json.parse(message)) match {
      case Success(messageParsed) =>
        messageParsed \ "name" match {
          case e: JsUndefined =>
            log.error(s"This json message does not contain expected key $e")
            None
          case value: JsDefined =>
            value.asOpt[String].getOrElse("") match {
              case RabbitEventType.QueueCreated =>
                validate[RabbitEventQueueCreated](messageParsed)
              case RabbitEventType.QueueEdited =>
                validate[RabbitEventQueueEdited](messageParsed)
              case RabbitEventType.GroupEdited =>
                validate[RabbitEventGroupEdited](messageParsed)
              case RabbitEventType.GroupDeleted =>
                validate[RabbitEventGroupDeleted](messageParsed)
              case RabbitEventType.GroupCreated =>
                validate[RabbitEventGroupCreated](messageParsed)
              case RabbitEventType.AgentEdited =>
                validate[RabbitEventAgentEdited](messageParsed)
              case RabbitEventType.AgentDeleted =>
                validate[RabbitEventAgentDeleted](messageParsed)
              case RabbitEventType.AgentCreated =>
                validate[RabbitEventAgentCreated](messageParsed)
              case RabbitEventType.MediaServerCreated =>
                validate[RabbitEventMediaServerCreated](messageParsed)
              case RabbitEventType.MediaServerEdited =>
                validate[RabbitEventMediaServerEdited](messageParsed)
              case RabbitEventType.MediaServerDeleted =>
                validate[RabbitEventMediaServerDeleted](messageParsed)
              case RabbitEventType.SipEndpointEdited =>
                validate[RabbitEventSipEndpointEdited](messageParsed)
              case RabbitEventType.UserServicesEdited =>
                validate[RabbitEventUserServicesEdited](messageParsed)
              case RabbitEventType.SipConfigEdited =>
                validate[RabbitEventSipConfigEdited](messageParsed)
              case RabbitEventType.UserConfigEdited =>
                validate[RabbitEventUserConfigEdited](messageParsed)
              case RabbitEventType.UserPreferenceCreated =>
                validate[RabbitEventUserPreferenceCreated](messageParsed)
              case RabbitEventType.UserPreferenceEdited =>
                validate[RabbitEventUserPreferenceEdited](messageParsed)
              case RabbitEventType.UserPreferenceDeleted =>
                validate[RabbitEventUserPreferenceDeleted](messageParsed)
              case RabbitEventType.MobilePushTokenAdded =>
                validate[RabbitEventMobilePushTokenAdded](messageParsed)
              case RabbitEventType.MobilePushTokenDeleted =>
                validate[RabbitEventMobilePushTokenDeleted](messageParsed)
              case RabbitEventType.WebserviceUserCreated =>
                validate[RabbitEventWebserviceUserCreated](messageParsed)
              case RabbitEventType.WebserviceUserEdited =>
                validate[RabbitEventWebserviceUserEdited](messageParsed)
              case RabbitEventType.WebserviceUsersReload =>
                validate[RabbitEventWebserviceUsersReload](messageParsed)
              case RabbitEventType.CtiStatusReload =>
                validate[RabbitEventCtiStatusReload](messageParsed)
              case unknown =>
                log.error(
                  s"Unhandled event type in rabbitmq event message received: $unknown"
                )
                None
            }
        }
      case Failure(f) =>
        log.error(
          s"This message is not in JSON format: $message"
        )
        None
    }
  }

  private def validate[T](
      messageParsed: JsValue
  )(implicit reads: Reads[T]): Option[T] = {
    messageParsed.validate[T] match {
      case JsSuccess(event, _) =>
        Some(event)
      case JsError(e) =>
        log.error(
          s"Non understandable JSON received: ${messageParsed.toString()}"
        )
        None
    }
  }
}
