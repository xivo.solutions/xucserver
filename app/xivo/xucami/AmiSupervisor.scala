package xivo.xucami

import org.apache.pekko.actor.SupervisorStrategy._
import org.apache.pekko.actor._
import com.codahale.metrics.SharedMetricRegistries
import com.google.inject.Inject
import com.google.inject.name.Named
import services.XucAmiBus.AmiFailure
import services.channel.ChannelManager
import services.config.{ConfigRepository, ConfigServerRequester}
import services.{ActorIds, AmiRequestManager, XucAmiBus}
import xivo.models.MediaServerConfig
import xivo.xuc.{ConfigServerConfig, XucBaseConfig}
import xivo.xucami.ami.ManagerConnector

import scala.concurrent.duration._
import scala.util.{Failure, Success}
import com.codahale.metrics.{Counter, MetricRegistry}

object AmiSupervisor {
  final val ManagerConnectorName = "XucAmiManagerConnector"
  final val ChannelManagerName   = "XucAmiChannelManager"
  final val SetVarManagerName    = "XucAmiSetVarManager"
  final val FailureMessage       = "Actor Crash"
  case class NewMds(mds: MediaServerConfig)
  case object GetConnectedMds
  case class ConnectedMds(list: List[MediaServerConfig])
  case class DeleteMds(mdsId: Long)
  case class RestartMds(mdsId: Long)
  case class FailedMdsActor(ref: ActorRef)
  case class LoadOrReloadMds(mdsId: Long)
  case class ScheduleMdsReload(mdsId: Long)
  case object TryLoadAll

  case class MediaServer(
      config: MediaServerConfig,
      ref: ActorRef,
      restartWithoutDelay: Boolean = false
  )
}

class AmiSupervisor @Inject() (
    amiBus: XucAmiBus,
    xucConfig: XucBaseConfig,
    xucAmiConfig: XucBaseAmiConfig,
    configServerRequester: ConfigServerRequester,
    configServerConfig: ConfigServerConfig,
    @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef,
    @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
    configRepo: ConfigRepository
) extends Actor
    with ActorLogging {
  import AmiSupervisor._
  import context.dispatcher

  val MediaServerConnectorPrefix    = "MdsAmiManagerConnector"
  val amiRetryDelay: FiniteDuration = 30.seconds

  val childs: List[(Props, String)] = List(
    (ChannelManager.props(amiBus, xucConfig = xucConfig), ChannelManagerName),
    (AmiRequestManager.props(amiBus), SetVarManagerName)
  )

  def scheduler: Scheduler = context.system.scheduler

  val registry: MetricRegistry =
    SharedMetricRegistries.getOrCreate(xucConfig.metricsRegistryName)
  val totalAmiFailures: Counter = registry.counter("global.ami.failures")

  def createManagerConnector(mds: MediaServerConfig): ActorRef = {
    val cfg = new XucBaseAmiConfig {
      val ipAddress: String                = mds.voip_ip
      val port: Int                        = xucAmiConfig.port
      val username: String                 = xucAmiConfig.username
      val secret: String                   = xucAmiConfig.secret
      val keepaliveTimeout: FiniteDuration = xucAmiConfig.keepaliveTimeout
      val amiReconnectOnMissedPings: Boolean =
        xucAmiConfig.amiReconnectOnMissedPings
    }
    log.info(s"starting ami manager connector for $mds")
    val ref = context.actorOf(
      ManagerConnector
        .props(
          amiBus,
          cfg,
          mds.name,
          amiBusConnector,
          configDispatcher,
          configRepo
        ),
      s"${MediaServerConnectorPrefix}_${mds.name}"
    )
    context.watch(ref)
  }

  private def getDefaultMdsConfig: MediaServerConfig = {
    MediaServerConfig(0, "default", "MDS Main", xucAmiConfig.ipAddress, true)
  }

  private def getMdsFromRef(
      mdsMap: Map[Long, MediaServer],
      ref: ActorRef
  ): Option[MediaServer] =
    mdsMap.values.toList.find(mds => mds.ref == ref)

  private def createMdsFromList(
      mdsList: List[MediaServerConfig],
      filter: MediaServerConfig => Boolean = _ => true
  ): Unit = {
    if (mdsList.size <= 1) {
      log.debug(
        s"Create mds from default config as mds list contains only one element (main)"
      )
      self ! NewMds(getDefaultMdsConfig)
    } else {
      val filteredMds = mdsList.filter(filter)
      log.debug(s"List of mediaServers to be created : $filteredMds")
      filteredMds
        .map(NewMds.apply)
        .foreach(self ! _)
    }
  }

  private def loadMediaServers(
      filter: MediaServerConfig => Boolean = _ => true
  ): Unit = {
    log.debug(s"Request list of all mediaServers with filter")
    configServerRequester.getMediaServerConfigAll.onComplete {
      case Success(mdsList) =>
        createMdsFromList(mdsList, filter)
      case Failure(t) =>
        log.error(
          t,
          "Got error while fetching mediaServers list from configMgt, retrying in {}",
          configServerConfig.retryDelay
        )
        scheduler.scheduleOnce(
          configServerConfig.retryDelay,
          self,
          AmiSupervisor.TryLoadAll
        )
    }
  }

  private def killMds(mdsMap: Map[Long, MediaServer], mdsId: Long): Unit = {
    mdsMap
      .get(mdsId)
      .map(_.ref)
      .foreach(_ ! PoisonPill)
  }

  override def preStart(): Unit = {
    childs.foreach({ case (props, name) =>
      context.actorOf(props, name)
    })
    loadMediaServers()
  }

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(-1, Duration.Inf, true)(decider)

  def receiveWithMds(mdsMap: Map[Long, MediaServer]): Receive = {
    case NewMds(mdsConfig) =>
      mdsMap
        .get(mdsConfig.id) match {
        case Some(mds) =>
          log.debug(
            s"Manager Connector already exists for mds ${mdsConfig.name}, restart it"
          )
          context.become(
            receiveWithMds(
              mdsMap.updated(mdsConfig.id, mds.copy(restartWithoutDelay = true))
            )
          )
          self ! RestartMds(mds.config.id)
        case None =>
          val ref  = createManagerConnector(mdsConfig)
          val newM = MediaServer(mdsConfig, ref)
          log.info(s"New media server created $newM")
          context.become(receiveWithMds(mdsMap.+(mdsConfig.id -> newM)))
      }

    case DeleteMds(mdsId) =>
      log.debug(s"Kill mds $mdsId without restart")
      context.become(receiveWithMds(mdsMap - mdsId))
      killMds(mdsMap, mdsId)

    case RestartMds(mdsId) =>
      log.debug(s"Kill mds $mdsId with restart")
      killMds(mdsMap, mdsId)

    case GetConnectedMds =>
      sender() ! ConnectedMds(mdsMap.values.map(_.config).toList)

    case TryLoadAll =>
      loadMediaServers()

    case FailedMdsActor(ref) =>
      getMdsFromRef(mdsMap, ref)
        .foreach(mds => {
          amiBus.publish(AmiFailure(FailureMessage, mds.config.name))
          self ! RestartMds(mds.config.id)
        })

    case Terminated(ref) =>
      getMdsFromRef(mdsMap, ref) match {
        case Some(mds) =>
          self ! ScheduleMdsReload(mds.config.id)
        case None =>
          log.debug(
            s"Got Terminated message from $ref, mds will NOT be restarted"
          )
      }

    case ScheduleMdsReload(mdsId) =>
      val withoutDelay =
        mdsMap.get(mdsId).exists(_.restartWithoutDelay)

      context.become(receiveWithMds(mdsMap - mdsId))
      if (withoutDelay) {
        log.debug(
          s"Got Terminated message from mds id $mdsId, retry is instant"
        )
        self ! LoadOrReloadMds(mdsId)
      } else {
        log.debug(
          s"Got Terminated message from mds id $mdsId, retry is scheduled"
        )
        scheduler.scheduleOnce(amiRetryDelay, self, LoadOrReloadMds(mdsId))
      }

    case LoadOrReloadMds(mdsId) =>
      loadMediaServers(_.id == mdsId)

    case any =>
      log.info(s"Not processing $any")
  }

  def receive: Receive = receiveWithMds(Map.empty)

  def decider: Decider = { case ex: Exception =>
    log.error(s"$ex Ami failure ${sender()}")
    totalAmiFailures.inc()
    self ! FailedMdsActor(sender())
    Stop
  }

}
