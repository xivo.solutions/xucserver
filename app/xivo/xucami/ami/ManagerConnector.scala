package xivo.xucami.ami

import org.apache.pekko.actor.*
import com.google.inject.name.Named
import helpers.JmxActorMonitor
import org.asteriskjava.manager.action.{EventGeneratingAction, PingAction}
import org.asteriskjava.manager.event.*
import org.asteriskjava.manager.response.{ManagerResponse, PingResponse}
import org.asteriskjava.manager.{
  ManagerConnection,
  ManagerConnectionFactory,
  ManagerEventListener,
  SendActionCallback
}
import play.api.Logger
import services.XucAmiBus.*
import services.channel.AmiLogger
import services.config.ConfigRepository
import services.{ActorIds, XucAmiBus}
import xivo.phonedevices.QueueSummaryCommand
import xivo.xucami.XucBaseAmiConfig
import xivo.xucami.models.{AttendedTransferFinished, EnterQueue, LeaveQueue}
import xivo.xucami.userevents.{
  HangupEvent as HangupUserEvent,
  QueueMemberWrapupStartEvent,
  UserEventAgent,
  UserEventAgentLogin,
  UserEventAgentLogoff,
  UserEventRecordOnDemand
}

import scala.collection.immutable.HashMap
import scala.concurrent.Future
import scala.concurrent.duration.*
import scala.util.Try
import helpers.{JmxLongMetric, JmxStringMetric}

class AsteriskManagerListener(actor: ActorRef)
    extends ManagerEventListener
    with SendActionCallback {
  val log: Logger = Logger(getClass.getName)
  def onManagerEvent(event: ManagerEvent): Unit = {
    log.debug(s"Received manager event: $event")
    actor ! event
  }
  def onResponse(response: ManagerResponse): Unit = {
    log.debug(s"Received manager response: $response")
    actor ! response
  }
}

object ManagerConnector {
  case object Login
  case object LoginTimeout
  case object SendPing
  case class MissedPing(actionId: Long, nbMissedPing: Int)
  type PendingRequests = HashMap[Long, AmiAction]

  class LoginTimeoutException extends Exception

  def managerFactory(xucAmiConfig: XucBaseAmiConfig) =
    new ManagerConnectionFactory(
      xucAmiConfig.ipAddress,
      xucAmiConfig.port,
      xucAmiConfig.username,
      xucAmiConfig.secret
    )
  def props(
      amiBus: XucAmiBus,
      config: XucBaseAmiConfig,
      mdsName: String,
      @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef,
      @Named(ActorIds.ConfigDispatcherId) configDispatcher: ActorRef,
      configRepo: ConfigRepository
  ): Props =
    Props(
      new ManagerConnector(
        amiBus,
        managerFactory(config),
        mdsName,
        amiBusConnector,
        configDispatcher,
        configRepo,
        config.keepaliveTimeout,
        config.amiReconnectOnMissedPings
      )
    )
}

class ManagerConnector(
    amiBus: XucAmiBus,
    managerFactory: ManagerConnectionFactory,
    mdsName: String,
    @Named(ActorIds.AmiBusConnectorId) amiBusConnector: ActorRef,
    @Named(ActorIds.ConfigDispatcherId) val configDispatcher: ActorRef,
    configRepo: ConfigRepository,
    keepaliveTimeout: FiniteDuration,
    amiReconnectOnMissedPings: Boolean
) extends Actor
    with ActorLogging
    with JmxActorMonitor {
  import ManagerConnector._
  import scala.concurrent.ExecutionContext.Implicits.global
  private[ami] var managerConnection: ManagerConnection     = null
  private[ami] var loginTimeout: Cancellable                = null
  private[ami] var managerListener: AsteriskManagerListener = null
  private[ami] var pendingRequests: PendingRequests         = HashMap()
  var scheduledPing: Option[Cancellable]                    = None
  val loggerAmiRequests: Logger                             = Logger("amirequests")
  jmxBean.addString("MDS", mdsName)
  val jmxAMIHostName: Try[JmxStringMetric] =
    jmxBean.addString("AMIHostname", "")
  val jmxAMIPort: Try[JmxLongMetric] = jmxBean.addLong("AMIPort", 0L)
  val jmxCnxState: Try[JmxStringMetric] =
    jmxBean.addString("State", "Initializing")
  val logger: Logger             = Logger(getClass.getName)
  val maxMissedPingsAllowed: Int = 3
  val scheduler: Scheduler       = context.system.scheduler

  var nextActionId: Long = 0
  var lastPingID: Long   = 0

  def getNextActorId: Long = {
    nextActionId = nextActionId + 1
    nextActionId
  }

  override def preStart(): Unit = {
    jmxBean
      .register()
      .failed
      .map(t => log.error(t, "Error while registering mbean"))
    amiBus.subscribe(self, AmiType.AmiAction)
    managerListener = new AsteriskManagerListener(self)
    managerConnection = managerFactory.createManagerConnection()
    managerConnection.addEventListener(managerListener)
    managerConnection.registerUserEventClass(classOf[UserEventAgentLogin])
    managerConnection.registerUserEventClass(classOf[UserEventAgentLogoff])
    managerConnection.registerUserEventClass(classOf[HangupUserEvent])
    managerConnection.registerUserEventClass(classOf[UserEventRecordOnDemand])
    managerConnection.registerUserEventClass(
      classOf[QueueMemberWrapupStartEvent]
    )
    logger.info(
      s"Starting AMI connection for ${managerConnection.getHostname}:${managerConnection.getPort}"
    )
    jmxAMIHostName.set(managerConnection.getHostname)
    jmxAMIPort.set(managerConnection.getPort)
    self ! Login
  }

  def receive: Receive = login(List())

  def login(actionsToProcess: List[AmiAction]): Receive = {
    case Login =>
      jmxCnxState.set("Loging In")
      loginTimeout = scheduler.scheduleOnce(5.seconds, self, LoginTimeout)
      Future(managerConnection.login()).failed.foreach {
        logger.warn(
          s"AMI login failed for: ${managerConnection.getHostname}:${managerConnection.getPort}",
          _
        )
      }

    case LoginTimeout =>
      jmxCnxState.set("Login Timeout")
      log.error("Manager login timed out")
      throw new LoginTimeoutException

    case event: ManagerEvent =>
      event match {
        case connected: ConnectEvent =>
          log.info("AMI logged on")
          jmxCnxState.set("Connected")
          loginTimeout.cancel()
          amiBus.publish(AmiConnected(mdsName))
          actionsToProcess.foreach(action => processAction(action))
          context.become(logged)
          log.info(s"Starting pinging for $mdsName in AMI connector")
          sendPing(0)
        case any: Any =>
      }

    case action: AmiAction =>
      log.debug(s"Buffering action to process once logged: $action")
      context.become(login(actionsToProcess :+ action))

    case any =>
      log.warning(s"Receive/login: Unprocessed message received: $any")
  }

  private def sendPing(missedPings: Int): Unit = {
    if (missedPings >= maxMissedPingsAllowed && amiReconnectOnMissedPings) {
      logger.warn(
        s"""Missed too many AMI pings for $mdsName ($maxMissedPingsAllowed miss), restarting AMI connection..."""
      )
      self ! PoisonPill
    } else {
      scheduledPing.map(_.cancel())
      lastPingID = getNextActorId
      logger.debug(
        s"""Sending ping (ID: ${lastPingID}) for $mdsName"""
      )
      scheduledPing = Some(
        scheduler.scheduleOnce(
          keepaliveTimeout,
          self,
          MissedPing(lastPingID, missedPings + 1)
        )
      )
      processAction(new AmiAction(new PingAction), lastPingID)
    }
  }

  def logged: Receive = {

    case pingResponse: PingResponse =>
      if (lastPingID.toString.equals(pingResponse.getActionId)) {
        logger.debug(
          s"""Received matching ping (ID: ${pingResponse.getActionId}) in time for $mdsName"""
        )
        scheduledPing.map(_.cancel())
        scheduledPing = Some(
          scheduler.scheduleOnce(
            keepaliveTimeout,
            self,
            SendPing
          )
        )
      } else
        logger.warn(
          s"""Got late ping response (ID: ${pingResponse.getActionId}), expecting ID $lastPingID after timeout for $mdsName"""
        )

    case MissedPing(pingId, nbMissedPing) =>
      if (lastPingID.equals(pingId)) {
        val baseLogMsg =
          s"Missed one AMI ping (ID: $lastPingID) for $mdsName, $nbMissedPing missed in a row"
        val fullLogMsg = baseLogMsg.concat(
          if (amiReconnectOnMissedPings)
            s", restarting in ${(maxMissedPingsAllowed) - nbMissedPing} ping(s)"
          else ""
        )
        logger.warn(fullLogMsg)
        sendPing(nbMissedPing)
      } else {
        logger.debug(
          s"Ignore missed ping request (ID: $pingId), expecting ID $lastPingID (Maybe the ping response was received before the missed ping request was handled ?)"
        )
      }
    case SendPing =>
      sendPing(0)

    case event: ManagerEvent =>
      AmiLogger.logEvent(AmiEvent(event, mdsName))
      processEvent(event)

    case action: AmiAction =>
      processAction(action)

    case response: ManagerResponse =>
      processManagerResponse(response)

    case AmiEvent(
          event @ (_: VarSetEvent | _: PeerStatusEvent | _: RegistryEvent |
          _: DeviceStateChangeEvent | _: NewExtenEvent | _: CelEvent |
          _: ContactStatusEvent),
          _
        ) =>
      log.debug(s"Receive/logged: Unprocessed message received: $event")

    case any =>
      log.warning(s"Receive/logged: Unprocessed message received: $any")
  }

  override def postStop(): Unit = {
    jmxBean.unregister()
    scheduledPing.map(_.cancel())
    try { managerConnection.logoff() }
    catch { case any: Throwable => log.error(s"Exception in postStop: $any") }
    managerListener = null
    managerConnection = null
  }

  private def processEvent(event: ManagerEvent): Unit = {
    event match {
      case responseEvent: ResponseEvent =>
        processResponseEvent(responseEvent)

      case agentConnect: AgentConnectEvent =>
        log.debug(
          s"Publishing AmiEvent: ${agentConnect.getBridgedChannel}, $agentConnect"
        )
        amiBus.publish(AmiEvent(agentConnect, mdsName))

      case agentEvent: UserEventAgent =>
        log.debug(s"Publishing UserEventAgent: $agentEvent")
        amiBus.publish(AmiAgentEvent(agentEvent))

      case e: QueueMemberPauseEvent =>
        log.debug(s"Publishing QueueMemberPauseEvent: $e")
        amiBus.publish(AmiAgentEvent(e))

      case e: QueueMemberWrapupStartEvent =>
        log.debug(s"Publishing QueueMemberWrapupStartEvent: $e")
        amiBus.publish(AmiAgentEvent(e))

      case joinEvent: QueueCallerJoinEvent =>
        log.debug(s"Transforming and publishing a JoinEvent: $joinEvent")
        amiBus.publish(EnterQueue(joinEvent, mdsName))
        amiBusConnector ! QueueSummaryCommand(
          joinEvent.getQueue
        )

      case leaveEvent: QueueCallerLeaveEvent =>
        log.debug(s"Transforming and publishing a LeaveEvent: $leaveEvent")
        amiBus.publish(LeaveQueue(leaveEvent))
        amiBusConnector ! QueueSummaryCommand(
          leaveEvent.getQueue
        )

      case attendedTransferEvent: AttendedTransferEvent =>
        log.debug(
          s"Transforming and publishing an AttendedTransferEvent: $attendedTransferEvent"
        )
        amiBus.publish(AttendedTransferFinished(attendedTransferEvent))

      case exs: ExtensionStatusEvent =>
        amiBus.publish(AmiExtensionStatusEvent(exs))

      case _ => amiBus.publish(AmiEvent(event, mdsName))
    }
  }

  private def processManagerResponse(response: ManagerResponse): Unit = {
    Option(response.getActionId)
      .flatMap(id => Try(id.toInt).toOption)
      .foreach(actionId => {
        loggerAmiRequests.debug(
          s"Publishing AMI Response: $response with actionId: $actionId}"
        )
        val richResponse: XucManagerResponse =
          (response, pendingRequests.get(actionId))

        richResponse._2.flatMap(_.requester) match {
          case Some(ref) => ref ! AmiResponse(richResponse)
          case None      => amiBus.publish(AmiResponse(richResponse))
        }
        pendingRequests -= actionId
      })
  }

  private def processResponseEvent(response: ResponseEvent): Unit = {
    val actionId =
      Option(response.getActionId).flatMap(id => Try(id.toInt).toOption)
    loggerAmiRequests.debug(
      s"Publishing AMI Response: $response with actionId: $actionId}"
    )
    val amiEvent = AmiEvent(response, mdsName)

    val request = actionId.flatMap(pendingRequests.get(_))
    request.flatMap(_.requester) match {
      case Some(ref) => ref ! amiEvent
      case None =>
        publishResponseEvent(amiEvent)
    }

    val doDeleteRequest = request
      .map(_.message)
      .collect({ case x: EventGeneratingAction => x })
      .forall(_.getActionCompleteEventClass.isInstance(response))

    if (doDeleteRequest) {
      actionId.foreach(pendingRequests -= _)
    }
  }

  private def publishResponseEvent(event: XucAmiBus.AmiEvent): Unit = {
    event match {
      case AmiEvent(e: QueueMemberEvent, _) =>
        log.debug(s"Publishing QueueMemberStatusEvent: $e")
        amiBus.publish(AmiAgentEvent(e))
      case AmiEvent(e: QueueEntryEvent, _) =>
        log.debug(s"Publishing QueueEntryEvent: $e")
        amiBus.publish(AmiAgentEvent(e))
      case e: AmiEvent =>
        log.debug(s"Publishing answer event $e")
        amiBus.publish(e)
    }
  }

  private def sendAndLogAction(actionId: Long, action: AmiAction): Long = {
    action.message.setActionId(actionId.toString)
    loggerAmiRequests.debug(
      s"Sending action to AMI: $action with actionId: $actionId"
    )
    managerConnection.sendAction(action.message, managerListener)
    actionId
  }

  private def processAction(
      action: AmiAction,
      actionId: Long = getNextActorId
  ): Option[Long] = {
    for {
      _ <- Option.when(
        action.targetMds.isEmpty || action.targetMds.contains(mdsName)
      )(())
      _ = pendingRequests += actionId -> action
    } yield sendAndLogAction(actionId, action)
  }
}
