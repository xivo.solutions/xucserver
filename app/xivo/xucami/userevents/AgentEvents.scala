package xivo.xucami.userevents

import org.asteriskjava.manager.event.UserEvent

class UserEventAgent(source: Object) extends UserEvent(source) {
  private var agentNumber: String = ""
  private var agentId: String     = ""

  def setAgentnumber(agn: String): Unit = agentNumber = agn
  def getAgentnumber: String            = agentNumber

  def setAgentId(agId: String): Unit = agentId = agId
  def getAgentId(): String           = agentId

}

class UserEventAgentLogin(source: Object) extends UserEventAgent(source) {
  private var extension: String = ""

  def setExtension(extn: String): Unit = extension = extn
  def getExtension(): String           = extension
}

class UserEventAgentLogoff(source: Object) extends UserEventAgent(source)
