package xivo.xucami

import com.google.inject.{ImplementedBy, Inject, Singleton}
import play.api.Configuration
import scala.concurrent.duration.*

@ImplementedBy(classOf[XucAmiConfig])
trait XucBaseAmiConfig {
  val prefix = "xucami"
  val ipAddress: String
  val port: Int
  val username: String
  val secret: String
  val keepaliveTimeout: FiniteDuration
  val amiReconnectOnMissedPings: Boolean
}

@Singleton
class XucAmiConfig @Inject() (configuration: Configuration)
    extends XucBaseAmiConfig {
  val ipAddress: String = configuration
    .getOptional[String](s"$prefix.amiIpAddress")
    .getOrElse("127.0.0.1")
  val port: Int =
    configuration.getOptional[Int](s"$prefix.amiPort").getOrElse(5038)
  val username: String =
    configuration.getOptional[String](s"$prefix.username").getOrElse("xuc")
  val secret: String =
    configuration.getOptional[String](s"$prefix.secret").getOrElse("8uc53c837")
  val keepaliveTimeout: FiniteDuration =
    configuration
      .getOptional[Int]("amiKeepaliveTimeout")
      .map(_.seconds)
      .getOrElse(10.seconds)
  val amiReconnectOnMissedPings: Boolean =
    configuration
      .getOptional[Boolean]("amiReconnectOnMissedPings")
      .getOrElse(false)
}
