package xivo.xuc.api

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.pattern.ask
import org.apache.pekko.util.Timeout
import org.apache.pekko.util.Timeout.durationToTimeout
import controllers.helpers._
import models.XucUser
import org.xivo.cti.model.PhoneHintStatus
import play.api.Logger
import services.config.ConfigServiceManager.ExportQualificationsCsv
import services.config.{ConfigRepository, ExportTicketsCsv, ImportCsvCallback}
import services.request._
import services.user.CallHistoryProxy
import services.video.model.VideoEvents
import services.video.model.VideoEvents.Event
import services._
import xivo.models.{CallHistory, RichCallHistory}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}

object Requester {
  import services.config.ConfigManager._

  val log: Logger                                     = Logger(getClass.getName)
  implicit val timeout: org.apache.pekko.util.Timeout = 5.seconds

  def handShake(configManager: ActorRef)(implicit system: ActorSystem): Unit = {
    log.debug("handshake")
    configManager ! PublishUserPhoneStatuses
  }

  def execute(
      repo: ConfigRepository,
      ctiRouterFactory: ActorRef,
      username: String,
      ctiRequest: org.json.JSONObject
  )(implicit
      system: ActorSystem,
      ec: ExecutionContext
  ): Future[RequestResult] = {
    log.debug(s"Executing request $ctiRequest for $username")
    connect(repo, ctiRouterFactory, username).map {
      log.info(s"$username connected")
      result =>
        result match {
          case t: RequestSuccess =>
            selectRouterActor(ctiRouterFactory, username) ! ctiRequest
            RequestSuccess("Request processed")
          case _ => RequestError("Request error")
        }
    }
  }

  def execute(
      repo: ConfigRepository,
      ctiRouterFactory: ActorRef,
      username: String,
      request: XucRequest
  )(implicit
      system: ActorSystem,
      ec: ExecutionContext
  ): Future[RequestResult] = {
    connect(repo, ctiRouterFactory, username).map {
      log.info(s"$username connected")
      result =>
        result match {
          case t: RequestSuccess =>
            selectRouterActor(ctiRouterFactory, username) ! BaseRequest(
              system.deadLetters,
              request
            )
            RequestSuccess("Request processed")
          case _ => RequestError("Request error")
        }
    }
  }

  def connect(
      repo: ConfigRepository,
      ctiRouterFactory: ActorRef,
      username: String
  )(implicit
      system: ActorSystem,
      ec: ExecutionContext
  ): Future[RequestResult] = {
    log.debug(s"connect $username")

    repo.getCtiUser(username) match {
      case None =>
        log.debug(s"$username Not found")
        Future(RequestUnauthorized(s"unable to connect $username"))
      case Some(xivoUser) =>
        val user = XucUser(xivoUser.username.getOrElse(""), xivoUser)
        log.debug(s"got user $user from config repo")

        (ctiRouterFactory ? GetRouter(user))
          .collect { case Router(_, ref) =>
            log.debug(s"got router $ref for $username")
            ref
          }
          .flatMap(_ ? IsReady)
          .map {
            case st: Started =>
              log.debug(
                "Login of user: \"" + username + "\" to the CTI server Ok"
              )
              RequestSuccess("Request processed")
            case t: RequestTimeout =>
              log.error(s"connect $username Request timeout")
              t
            case x =>
              log.error(s"unable to connect $username $x")
              RequestError(s"unable to connect $username $x")
          }
    }
  }

  def phoneNbToUserName(repo: ConfigRepository, s: String): String =
    repo.userNameFromPhoneNb(s).getOrElse("")

  def phoneNbToPhoneStatus(repo: ConfigRepository, s: String): PhoneHintStatus =
    repo.richPhones.getOrElse(s, PhoneHintStatus.UNEXISTING)

  def phoneNbToVideoStatus(repo: ConfigRepository, s: String): Event =
    repo
      .getUserVideoStatus(repo.userNameFromPhoneNb(s).getOrElse(""))
      .getOrElse(VideoEvents.Available)

  def richCallHistory(
      username: String,
      param: HistorySize,
      repo: ConfigRepository,
      callHistoryManager: ActorRef
  )(implicit
      system: ActorSystem,
      ec: ExecutionContext
  ): Future[RichCallHistory] = {
    val proxy = system.actorOf(CallHistoryProxy.props(callHistoryManager))

    implicit val timeout: Timeout = Timeout(3.seconds)
    val result                    = proxy ? UserCallHistoryRequest(param, username)
    result
      .mapTo[CallHistory]
      .map(
        RichCallHistory.apply(
          _,
          phoneNbToUserName(repo, _),
          phoneNbToPhoneStatus(repo, _),
          phoneNbToVideoStatus(repo, _)
        )
      )
  }

  def richCallHistoryByDays(
      username: String,
      param: HistoryDays,
      repo: ConfigRepository,
      callHistoryManager: ActorRef
  )(implicit
      system: ActorSystem,
      ec: ExecutionContext
  ): Future[RichCallHistory] = {
    val proxy = system.actorOf(CallHistoryProxy.props(callHistoryManager))

    implicit val timeout: Timeout = Timeout(3.seconds)
    val result                    = proxy ? UserCallHistoryRequestByDays(param, username)
    result
      .mapTo[CallHistory]
      .map(
        RichCallHistory.apply(
          _,
          phoneNbToUserName(repo, _),
          phoneNbToPhoneStatus(repo, _),
          phoneNbToVideoStatus(repo, _)
        )
      )
  }

  def importCsvCallback(
      callbackManager: ActorRef,
      listUuid: String,
      text: String
  )(implicit system: ActorSystem): Future[RequestResult] = {
    implicit val timeout: Timeout = Timeout(10.seconds)
    val result                    = callbackManager ? ImportCsvCallback(listUuid, text)
    result.mapTo[RequestResult]
  }

  def exportTicketsCsv(callbackManager: ActorRef, listUuid: String)(implicit
      system: ActorSystem
  ): Future[RequestResult] = {
    implicit val timeout: Timeout = Timeout(10.seconds)
    val result                    = callbackManager ? ExportTicketsCsv(listUuid)
    result.mapTo[RequestResult]
  }

  def exportQualificationsCsv(
      configServiceManager: ActorRef,
      queueId: Long,
      fromDate: String,
      toDate: String
  )(implicit system: ActorSystem): Future[RequestResult] = {
    implicit val timeout: Timeout = Timeout(10.seconds)
    val result =
      configServiceManager ? ExportQualificationsCsv(queueId, fromDate, toDate)
    result.mapTo[RequestResult]
  }

  private def selectRouterActor(ctiRouterFactory: ActorRef, username: String)(
      implicit system: ActorSystem
  ) =
    system.actorSelection(ActorIds.ctiRouterPath(ctiRouterFactory, username))
}
