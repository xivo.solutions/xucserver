package xivo.data

import org.apache.pekko.actor.{Actor, ActorLogging, ActorRef}
import com.google.inject.Inject
import com.google.inject.name.Named
import models.QLogTransformer
import models.QLogTransformer.QLogDataTransformer
import models.QueueLog.QueueLogData
import stats.Statistic.ResetStat
import services.ActorIds

class QLogDispatcher @Inject() (
    @Named(ActorIds.QueueEventDispatcherId) queueEventDispatcher: ActorRef
) extends Actor
    with ActorLogging {

  def qLogTransformer: QLogDataTransformer = QLogTransformer.transform

  def receive: PartialFunction[Any, Unit] = {

    case ResetStat => queueEventDispatcher ! ResetStat

    case qLogData: QueueLogData =>
      log.debug(s"$qLogData")
      queueEventDispatcher ! qLogTransformer(qLogData)
  }
}
