# Xucserver
XiVOcc / XiVOuc integration server

![scala steward](https://img.shields.io/badge/Scala_Steward-helping-light.svg)
![pipeline](https://gitlab.com/xivo.solutions/xucserver/badges/master/pipeline.svg?ignore_skipped=true)

![scalaversion](https://img.shields.io/badge/Scala%203-DC322F)
![pekkoversion](https://img.shields.io/badge/Pekko-448aff)
![play](https://img.shields.io/badge/Play%20Framework-92d13d)

## Dependencies

### To install beforehand (elsewhere):

- play-authentication

Retrieve the play_auth tag used in your xuc version. This version can be found in the file `project/Dependencies.scala`, in the `Version` object, under the variable `playauthentication` (e.g. `val playauthentication = "2023.11.00-play3"`).

You can retrieve it via a script by executing the following line in the root of the xucserver folder:

```bash
grep -m 1 "val playauthentication" project/Dependencies.scala| cut -d '"' -f2
```

```bash
echo -n "Enter the play_auth tag: " && read PLAY_AUTH_TAG
git clone git@gitlab.com:xivoxc/play-authentication.git
cd play-authentication
git checkout $PLAY_AUTH_TAG
sbt clean compile publishLocal
```

- asterisk-java
```bash
git clone git@gitlab.com:xivocc/asterisk-java.git
cd asterisk-java
git checkout xivocc_version
mvn install
```

- xivo-javactilib (installed with java8)
```bash
git clone git@gitlab.com:xivo.solutions/xivo-javactilib.git
cd xivo-javactilib
mvn install
```

### To run with

- sbt
- Java 17 & Scala3

### Format

```
sbt scalafmt
```

## How to launch app in dev

Create a conf file named application-*.conf like `application-dev.conf` in repo `conf` and fill it with your xivo/xivocc variables :
```conf
include "application.conf"

XIVO_HOST = <TOFILL>
PLAY_AUTH_TOKEN = <TOFILL># PLAY_AUTH_TOKEN custom env
RECORDING_SERVER_HOST = <TOFILL>
XIVO_AMI_SECRET = <TOFILL> # secret in /etc/asterisk/manager.d/02-xivocc.conf on pbx
```

You can also directly override a configuration key if no env variable is defined.

Starting the application in play framework :

```sh
sbt run -Dhttp.port=8090 -Dconfig.file=conf/application-dev.conf -Dlogger.file=conf/debug-logger.xml
```

If you want to bind xuc with an HTTPS port, you will need some extra parameters `--add-exports=java.base/sun.security.x509=ALL-UNNAMED -Dhttps.port=8443`
The command will then look like this :
```sh
sbt run --add-exports=java.base/sun.security.x509=ALL-UNNAMED -Dhttp.port=8090 -Dhttps.port=8443 -Dconfig.file=conf/application-dev.conf -Dlogger.file=conf/debug-logger.xml
```

### Optional logs for development: WS and Ami messages

To log WS and Ami messages as JSON to file `logs/xuc_events.log`, use config `conf/debug-logger.xml` and change priority
level for logger `"xucevents"` to `"DEVEL"`:

```xml
<logger name="xucevents" level="DEVEL" additivity="false">
    <appender-ref ref="XUCEVENTS" />
</logger>
```

Or you can enable selectively using Logback's
[Level Inheritance](http://logback.qos.ch/manual/architecture.html#effectiveLevel). Definned loggers:

* `"xucevents.cti.in."` + CTI user name
* `"xucevents.cti.out."` + CTI user name
* `"xucevents.ws.in."` + CTI user name
* `"xucevents.ws.out."` + CTI user name

Configuration for logging only incomming AMI for user john events to both file and console:

```xml
<logger name="xucevents.cti.in.john" level="DEBUG">
    <appender-ref ref="XUCEVENTS" />
    <appender-ref ref="STDOUT" />
</logger>
```

To have JSON pretty-printed (human readable) in `conf/application.conf` change `logger.xucevents.prettyPrint=false` to `true`.

## Testing

For the tests you need:

* a link towards your XiVO dev, an AMI access and a webservice user
* a test Database

### Link to XiVO

In the `/etc/hosts` of your machine, map the IP of an existing XiVO to the hostnames

* `xivo-integration` (see `BaseTest.scala` for example)
* `xivo` (see `test/resources/application.conf`)

For example, if your XiVO is accessible on `192.168.51.232`, you should have:

```
192.168.51.232 xivo-integration xivo
```

#### AMI Access

The tests will try to connect with user `xuc` and password `xucpass` for AMI access.
Therefore in `/etc/asterisk/manager.d/02-xivocc.conf` make sure you have a user `xuc` configured with secret `xucpass` as in extract:

```plain
[xuc]
secret = xucpass
...
```

Make sure also your IP address is authorized in the ACL.
For example, if your XIVO is on `192.168.51.232` and your PC has IP `192.168.51.10`, you should have a line:

```plain
permit=192.168.51.10/255.255.255.255
```

#### Webservice Access

The tests need also an access to the Webservice. For this:

* go to Configuration -> Web Services Access
* add a Webservice user with:
  * user: `xivows`
  * host: `*your_host_ip*`

See [documentation](https://documentation.xivo.solutions/en/latest/installation/xivocc/installation/manual_configuration.html#customizations-in-the-web-interface).

### Test Database

### Setup a test database *testdata* user *test* pwd *test*

```sql
CREATE USER test WITH PASSWORD 'test';
CREATE DATABASE testdata WITH OWNER test;
```

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the [COPYING](COPYING) and [COPYING.LESSER](COPYING.LESSER) files for details.
