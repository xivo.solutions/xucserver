package xuccli

object CommandToBean {
  def apply(cmd: InfoCommand): List[BeanProperty[?]] = {
    cmd match {
      case GetVersion   => List(XucBeans.MainRunner.version)
      case GetStartTime => List(XucBeans.MainRunner.startTime)
      case GeneralInfo =>
        List(
          XucBeans.MainRunner.version,
          XucBeans.MainRunner.startTime,
          XucBeans.ConfigDispatcher.agents,
          XucBeans.ConfigDispatcher.users
        )
      case MdsState(mds) =>
        implicit val bean = XucBeans.Mds.bean(mds)
        List(
          XucBeans.Mds.MDS,
          XucBeans.Mds.State,
          XucBeans.Mds.AMIHostname,
          XucBeans.Mds.AMIPort
        )
      case SipTrackerInfo(peer, driver) =>
        implicit val bean = XucBeans.SipTracker.bean(peer, driver)
        List(
          XucBeans.SipTracker.Calls,
          XucBeans.SipTracker.ChannelEvent,
          XucBeans.SipTracker.PartyInformations,
          XucBeans.SipTracker.PathsFromChannel
        )
    }
  }
}
