package xuccli

object CommandToOperation {
  def apply(cmd: OperationCommand): BeanOperation = {
    cmd match {
      case SipTrackerCommand(peer, `Stop`, driver) =>
        BeanOperation(XucBeans.SipTracker.bean(peer, driver), "stop")
      case SipTrackerCommand(peer, `ClearCalls`, driver) =>
        BeanOperation(XucBeans.SipTracker.bean(peer, driver), "clearCalls")
    }
  }
}
