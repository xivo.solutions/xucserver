package xuccli

import cats.effect._
import cats.implicits._

import com.monovore.decline._
import com.monovore.decline.effect._

import javax.management._

object XucCliApp
    extends CommandIOApp(
      name = "xuc-cli",
      header = "Xuc Command Line util",
      version = xuccli.info.BuildInfo.version
    ) {

  override def main: Opts[IO[ExitCode]] = {
    CliParser.parse.map {
      case CliCommand(pid, command: InfoCommand) =>
        JmxClientBuilder(pid).resource
          .use { implicit server =>
            for {
              beans <- IO.pure(CommandToBean(command))
              res   <- JmxClient.getAttributes(beans)
              _     <- printResults(res)
            } yield ExitCode.Success
          }
          .redeemWith(
            e => handleCommandError(e, command),
            _ => IO(ExitCode.Success)
          )
      case CliCommand(pid, command: OperationCommand) =>
        JmxClientBuilder(pid).resource
          .use { implicit server =>
            for {
              op <- IO.pure(CommandToOperation(command))
              _  <- JmxClient.invoke(op)
              _  <- printOk("Command Successful")
            } yield ExitCode.Success
          }
          .redeemWith(
            e => handleCommandError(e, command),
            _ => IO(ExitCode.Success)
          )
    }
  }

  def handleCommandError(t: Throwable, cmd: SubCommand): IO[ExitCode] =
    ((t, cmd) match {
      case (_: InstanceNotFoundException, MdsState(mds)) =>
        printErr(
          s"MDS connection object not found, maybe you mispelled the mds name: ${Console.BOLD}${mds}${Console.RESET}"
        )
      case (_: InstanceNotFoundException, SipTrackerInfo(peer, "PJSIP")) =>
        printErr(
          s"SipDeviceTracker object not found, maybe you mispelled the peer name: ${Console.BOLD}${peer}${Console.RESET}"
        )
      case (
            _: InstanceNotFoundException,
            SipTrackerCommand(peer, _, "PJSIP")
          ) =>
        printErr(
          s"SipDeviceTracker object not found, maybe you mispelled the peer name: ${Console.BOLD}${peer}${Console.RESET}"
        )
      case (_, _) =>
        printErr(
          s"An error occurred while executing command $cmd: ${t.getClass.getName}(${t.getMessage})"
        )
    }) *> IO(ExitCode.Error)

  def printErr(s: String): IO[Unit] =
    IO(println(s"${Console.RED}ERROR:${Console.RESET} $s"))

  def printOk(s: String): IO[Unit] =
    IO(println(s"${Console.GREEN}OK:${Console.RESET} $s"))

  def printBeanResult(r: BeanResult[?]): IO[Unit] =
    IO(println(s"\t- ${r.property.attribute}: ${r.value}"))

  def printXucBeanResult(b: XucBean, l: List[BeanResult[?]]): IO[Unit] =
    for {
      _ <- IO(println(s"${Console.BOLD}[${b.name}]${Console.RESET}"))
      _ <- l.traverse(printBeanResult)
    } yield ()

  def printResults(l: List[BeanResult[?]]): IO[Unit] =
    l.groupBy(_.property.bean)
      .map(p => printXucBeanResult(p._1, p._2))
      .toList
      .sequence_

}
