package xuccli

import javax.management.ObjectName
import scala.reflect.ClassTag

case class XucBean(name: String) {
  def toObjectName() = new ObjectName(name)
}

object XucBeans {
  object MainRunner {
    private val bean = XucBean("xuc.system:type=MainRunner")
    val version      = StringBeanProperty(bean, "version")
    val startTime    = StringBeanProperty(bean, "startTime")
  }

  object ConfigDispatcher {
    private val bean = XucBean("xuc.services.config:type=ConfigDispatcher")
    val agents       = LongBeanProperty(bean, "Agents")
    val users        = LongBeanProperty(bean, "Users")
  }

  object Mds {
    def bean(mds: String) =
      XucBean(
        s"xuc.xivo.xucami.ami:type=ManagerConnector,name=MdsAmiManagerConnector_$mds"
      )

    def AMIHostname(implicit b: XucBean) = StringBeanProperty(b, "AMIHostname")
    def AMIPort(implicit b: XucBean)     = LongBeanProperty(b, "AMIPort")
    def MDS(implicit b: XucBean)         = StringBeanProperty(b, "MDS")
    def State(implicit b: XucBean)       = StringBeanProperty(b, "State")
  }

  object SipTracker {
    def bean(name: String, driver: String) =
      XucBean(
        s"xuc.services.calltracking:type=SipDeviceTracker,name=${driver}_${name}-"
      )

    def Calls(implicit b: XucBean)        = LongBeanProperty(b, "Calls")
    def ChannelEvent(implicit b: XucBean) = LongBeanProperty(b, "ChannelEvent")
    def PartyInformations(implicit b: XucBean) =
      LongBeanProperty(b, "PartyInformations")
    def PathsFromChannel(implicit b: XucBean) =
      LongBeanProperty(b, "PathsFromChannel")
  }
}

sealed trait BeanProperty[T] {
  val bean: XucBean
  val attribute: String
}

case class StringBeanProperty(bean: XucBean, attribute: String)
    extends BeanProperty[String]
case class LongBeanProperty(bean: XucBean, attribute: String)
    extends BeanProperty[Long]

case class BeanResult[T: ClassTag](property: BeanProperty[T], value: T)

case class BeanOperation(bean: XucBean, operation: String)
