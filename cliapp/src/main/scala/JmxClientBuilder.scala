package xuccli

import cats.implicits.*
import cats.effect.*
import cats.*

import javax.management.remote.{
  JMXConnector,
  JMXConnectorFactory,
  JMXServiceURL
}
import javax.management.MBeanServerConnection
import scala.util.Try
import com.sun.tools.attach.VirtualMachine
import scala.reflect.ClassTag

class JmxClientBuilder private (val pid: Int) {

  private def startManagementAgent(): IO[String] = {
    IO.fromTry(
      Try(VirtualMachine.attach(pid.toString).startLocalManagementAgent())
    )
  }

  private def connect: Resource[IO, JMXConnector] = {
    val address = startManagementAgent()

    Resource.make({
      address
        .flatMap(adr => IO.fromTry(Try(new JMXServiceURL(adr))))
        .flatMap(url => IO.fromTry(Try(JMXConnectorFactory.connect(url, null))))
    })(cnx => IO.delay(cnx.close))
  }

  def resource: Resource[IO, MBeanServerConnection] =
    for {
      jmxCnx <- connect
    } yield jmxCnx.getMBeanServerConnection

}

object JmxClientBuilder {
  def apply(pid: Int): JmxClientBuilder =
    new JmxClientBuilder(pid)
}

object JmxClient {

  private def unsafeGetAttribute[T](
      p: BeanProperty[T]
  )(implicit server: MBeanServerConnection): Try[Object] =
    Try(server.getAttribute(p.bean.toObjectName(), p.attribute))

  private def safeCast[T](o: Object)(implicit classTag: ClassTag[T]): Try[T] =
    Try(o.asInstanceOf[T])

  def getAttribute[T](p: BeanProperty[T])(implicit
      server: MBeanServerConnection,
      classTag: ClassTag[T],
      ae: ApplicativeError[IO, Throwable]
  ): IO[BeanResult[T]] =
    IO(unsafeGetAttribute(p))
      .flatMap(IO.fromTry)
      .flatMap(o => IO(safeCast[T](o)))
      .flatMap(IO.fromTry)
      .map(v => BeanResult[T](p, v))

  def getAttributes(
      l: List[BeanProperty[?]]
  )(implicit server: MBeanServerConnection): IO[List[BeanResult[?]]] =
    l.map {
      case b @ StringBeanProperty(bean: XucBean, attribute: String) =>
        getAttribute[String](b)
      case b @ LongBeanProperty(bean, attribute) => getAttribute[Long](b)
    }.sequence

  def invoke(
      op: BeanOperation
  )(implicit server: MBeanServerConnection): IO[Unit] =
    IO.fromTry(
      Try(server.invoke(op.bean.toObjectName(), op.operation, null, null))
    )

}
