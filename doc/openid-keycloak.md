# Keycloak test platform

To setup your keycloak server =>
  https://www.keycloak.org
  Download and unzip latest version
  Launch standalone.sh from the bin folder inside the unzipped one
  OR set it up via docker
  
## Keycloak server configuration

1. Run keycloak in docker `docker run --rm -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin quay.io/keycloak/keycloak:10.0.2`
1. Goto http://localhost:8080/auth/admin/ and use admin/admin as credentials
1. Hover the `Master` dropdown on the top left and click on `Add a realm`
1. Set a name like `garden` and make sure the realm is enabled before saving
1. Click `Clients` in the left menu and click `Create`, set `xuc-test` as name and `Save`
1. You are redirected to the `Settings` page: 
  1. Enable `Implicit Flow` 
  1. Type `*` in `Valid Redirect URIs`
  1. `Save`

## Keycloak Audience mapping

1. From `Configure/Client Scopes`, click `Create`
   * `Name`: `xivo-applications`
   * `Include In Token Scope` must be checked (this is the default)
   * Click `Save` 
1. Click `Create` in `Mappers` tab:
   * `Name`: `Audience for xivo-applications`
   * `Mapper Type`: `Audience` 
   * `Included Client Audience`: `xuc-test` (or the client id defined earlier)
   * Click `Save`

## Keycloak user creation

1. From the Users menu on the left, click `Add user`
1. Add `username` and make sure it matches a XiVO cti username
1. `Save`
1. Goto the `Credentials` and set a password
1. Uncheck `Temporary`
1. Click `Set password` and confirm

## Front end configuration

Run the front-end with either the following variables set: (`garden` is the realm defined before)
* OIDC_SERVER_URL=http://localhost:8080/auth/realms/garden
* OIDC_CLIENT_ID=xuc-test

Or use the following argument of `sbt run` : `-Dxuc.openidServerUrl=http://localhost:8080/auth/realms/garden -Dxuc.openidClientId=xuc-test`

## Back end configuration

Run the xuc backend with either the following variables set:
* ENABLE_OIDC=true
* OIDC_SERVER_URL=http://localhost:8080/auth/realms/garden
* OIDC_CLIENT_ID=xuc-test

Or use the following argument of `sbt run` : ` -Doidc.enable=true -Doidc.trustedServerUrl=http://localhost:8080/auth/realms/garden -Doidc.clientId=xuc-test`



## Notes
Additional doc => https://www.keycloak.org/docs/latest/securing_apps/#_javascript_adapter



  
