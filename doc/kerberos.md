# Kerberos

This is the procedure to test kerberos against a XiVOcc version >= 2019.11.00 (Deneb).

## On XivoCC

Run: `docker run -it --rm --network=host --name=kdc jpthomasset/kerberos`

In another console, extract keytab
```
mkdir /etc/docker/xuc
docker cp kdc:/xuc.keytab /etc/docker/xuc/
chmod +r /etc/docker/xuc/xuc.keytab
```

Create a /etc/krb5.conf file with the following content:
```
[libdefaults]
	default_realm = DOCKERDOMAIN

```

Create an override file for docker: docker-xivocc.override.yml

```
version: "2.4"

services:
  xuc:
    environment:
    - JAVA_OPTS=-Xms512m -Xmx1024m -Dsecured.krb5.principal=HTTP/xuc.dockerdomain -Dsecured.krb5.password=xuc -Dsecured.krb5.keyTab=/etc/xuc.keytab -Dsun.security.krb5.debug=true

    volumes:
    - /etc/docker/xuc/xuc.keytab:/etc/xuc/xuc.keytab
    - /etc/docker/xuc/krb5.conf:/etc/krb5.conf

  xucmgt:
    environment:
    - USE_SSO=true
    - XUC_HOST=xuc.dockerdomain
```

Restart services: `xivocc-dcomp up -d`

## On Xivo 

Create a user with 'jbond' as username.

## On Client machine
Install kerberos client
Debian: `sudo apt-get install krb5-user libpam-krb5 libpam-ccreds auth-client-config`
Archlinux: `sudo pacman -S krb5`

Configure /etc/krb5.conf on client
```
[libdefaults]
	default_realm = DOCKERDOMAIN

[realms]
# use "kdc = ..." if realm admins haven't put SRV records into DNS
        DOCKERDOMAIN = {
		kdc = xuc.dockerdomain
		admin_server = xuc.dockerdomain
	}

[domain_realm]

[logging]
#	kdc = CONSOLE
```

Edit `/etc/hosts` and add name for your xivocc host:
```
192.168.56.11 	xuc.dockerdomain
```

## Login/logout on client machine

* Login: `kinit -V -p jbond@DOCKERDOMAIN` (Password is `jbond`)
* Logout: `kdestroy`
* List credentials: `klist`

For example:

```
jpthomasset@rnd-jpt-arch ~ % kinit -V -p jbond@DOCKERDOMAIN
Using default cache: /tmp/krb5cc_1000
Using principal: jbond@DOCKERDOMAIN
Password for jbond@DOCKERDOMAIN: 
Authenticated to Kerberos v5

jpthomasset@rnd-jpt-arch ~ % klist
Ticket cache: FILE:/tmp/krb5cc_1000
Default principal: jbond@DOCKERDOMAIN

Valid starting       Expires              Service principal
26/09/2019 16:08:13  27/09/2019 16:08:13  krbtgt/DOCKERDOMAIN@DOCKERDOMAIN
```

## Configure firefox on client machine (linux)

Go to `about:config`
* add domain (without protocol) to the `network.negotiate-auth.delegation-uris` entry (ie. xuc.dockerdomain).
* add domain (without protocol) to the `network.negotiate-auth.trusted-uris` entry (ie. xuc.dockerdomain).
* Set `network.negotiate-auth.allow-non-fqdn` to true

Go to https://xuc.dockerdomain
Enjoy SSO!

# Usefull commands

* Login: `kinit -V -p jbond@DOCKERDOMAIN` (Password is `jbond`)
* Logout: `kdestroy`
* **`klist -e`** to list ticket available on client with key type:

  ```
  Ticket cache: FILE:/tmp/krb5cc_1000
  Default principal: jbond@DOCKERDOMAIN

    Valid starting       Expires              Service principal
    26/09/2019 16:08:13  27/09/2019 16:08:13  krbtgt/DOCKERDOMAIN@DOCKERDOMAIN
	    Etype (skey, tkt): aes256-cts-hmac-sha1-96, aes256-cts-hmac-sha1-96 
    26/09/2019 16:46:45  27/09/2019 16:08:13  HTTP/xuc.dockerdomain@
	    Etype (skey, tkt): aes256-cts-hmac-sha1-96, aes256-cts-hmac-sha1-96 
    26/09/2019 16:46:45  27/09/2019 16:08:13  HTTP/xuc.dockerdomain@DOCKERDOMAIN
	    Etype (skey, tkt): aes256-cts-hmac-sha1-96, aes256-cts-hmac-sha1-96 
    ```

* **`klist -e -k xuc.keytab`** to list service defined in a keytab file:

    ```
    Keytab name: FILE:xuc.keytab
    KVNO Principal
    ---- --------------------------------------------------------------------------
       3 HTTP/xuc.dockerdomain@DOCKERDOMAIN (aes256-cts-hmac-sha1-96) 
       3 HTTP/xuc.dockerdomain@DOCKERDOMAIN (aes128-cts-hmac-sha1-96) 
       3 HTTP/xuc.dockerdomain@DOCKERDOMAIN (des3-cbc-sha1) 
       3 HTTP/xuc.dockerdomain@DOCKERDOMAIN (arcfour-hmac) 
    ```

## Kerberos server commands

Inside the container `docker exec -it kdc /bin/bash` run `kadmin.local`

* Add an account: `addprinc jbond` where jbond is the user to create
* Add a service account `addprinc HTTP/xuc.dockerdomain` where the last part is the xuc fqdn
* Export keytab file for the given service account: `ktadd -k xuc.keytab HTTP/xuc.dockerdomain`


