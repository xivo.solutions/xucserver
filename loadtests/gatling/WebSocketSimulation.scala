package ws

import com.excilys.ebi.gatling.core.Predef._
import com.excilys.ebi.gatling.http.Predef._
import com.excilys.ebi.gatling.jdbc.Predef._
import com.excilys.ebi.gatling.http.Headers.Names._
import org.apache.pekko.util.duration._
import com.giltgroupe.util.gatling.websocket.Predef._
import bootstrap._

case class XucUser(username: String, password: String, number: String)

class WebsocketSimulation extends Simulation {

  val server = "192.168.51.157:9000"
  val user   = XucUser("bruce", "bruce", "1050")

  val send_ping = websocket("socket").sendMessage("""{"claz":"ping"}""", "ping")
  def request_queue_config =
    websocket("socket").sendMessage(
      """{"claz":"web", "command":"getConfig", "objectType" : "queue"}""",
      "req_queue_config"
    )
  def request_agent_config =
    websocket("socket").sendMessage(
      """{"claz":"web", "command":"getConfig", "objectType" : "agent"}""",
      "req_agent_config"
    )
  def request_queuemember_config =
    websocket("socket").sendMessage(
      """{"claz":"web", "command":"getConfig", "objectType" : "queuemember"}""",
      "req_queue_member"
    )
  def subscribeToStats =
    websocket("socket").sendMessage(
      """{"claz":"web", "command":"subscribeToQueueStats"}""",
      "subs_to_stat"
    )
  def subscribeToAgentEvents =
    websocket("socket").sendMessage(
      """{"claz":"web", "command":"subscribeToAgentEvents"}""",
      "subs_to_agent"
    )
  val url =
    "ws://" + server + "/xuc/ctichannel?username=" + user.username + "&agentNumber=" + user.number + "&password=" + user.password

  def connectUserUrl(username: String, password: String, number: String) = {
    "ws://" + server + "/xuc/ctichannel?username=" + username + "&agentNumber=" + number + "&password=" + password
  }
  val doPing = exec(send_ping)

  val scn = scenario("Scenario name")
    .feed(csv("xuc_user.csv").random)
    .exec(
      websocket("socket").open(
        connectUserUrl("${username}", "${password}", "${number}"),
        "socket_open"
      )
    )
    .pause(5)
    .exec(send_ping)
    .pause(1, 10)
    .exec(request_queue_config)
    .pause(2, 15)
    .exec(request_agent_config)
    .pause(1, 10)
    .exec(request_queuemember_config)
    .pause(3)
    .exec(subscribeToStats)
    .pause(3)
    .exec(subscribeToAgentEvents)
    .pause(1)
    .repeat(20) {
      doPing
        .pause(5, 20)
    }
    .exec(send_ping)
    .pause(50, 600)
    .exec(websocket("socket").close("socket_close"))

  setUp(scn.users(10000).ramp(12000))
}

/*
      var wsurl = "ws://" + server + "/xuc/ctichannel?username=" + username + "&amp;agentNumber="
                        + phoneNumber + "&amp;password=" + password;
 */
