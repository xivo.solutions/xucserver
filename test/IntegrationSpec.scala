import org.apache.pekko.actor.{Actor, Props}
import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.asteriskjava.manager.action.CoreStatusAction
import org.scalatestplus.mockito.MockitoSugar.mock
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Helpers.running
import services.XucAmiBus
import services.XucAmiBus.{AmiAction, AmiResponse, AmiType}
import services.config.ConfigRepository
import xivo.xucami.XucAmiConfig
import xivo.xucami.ami.ManagerConnector

import scala.concurrent.duration._
import play.api.Application

class IntegrationSpec
    extends TestKitSpec("Integration")
    with GuiceOneAppPerSuite {

  val testConfig: Map[String, Any] = {
    Map(
      "logger.root"                      -> "INFO",
      "logger.application"               -> "INFO",
      "logger.services"                  -> "INFO",
      "pekko.loggers"                    -> List("org.apache.pekko.event.slf4j.Slf4jLogger"),
      "log-dead-letters-during-shutdown" -> "on",
      "loglevel"                         -> "ERROR",
      "xucami.amiIpAddress"              -> "xivo-integration",
      "xucami.amiPort"                   -> "5038",
      "xucami.username"                  -> "xuc",
      "xucami.secret"                    -> "xucpass",
      "api.eventUrl"                     -> ""
    )
  }
  val amiBusConnector: TestProbe  = TestProbe()
  val configDispatcher: TestProbe = TestProbe()
  val repo: ConfigRepository      = mock[ConfigRepository]

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration = Configuration.from(testConfig))
      .build()

  "Application" should (
    "be able to connect to AMI and get CoreStatus" in {
      running(app) {
        val probe  = TestProbe()
        val amiBus = new XucAmiBus()
        val mdsId = 1
        val amiManager = TestProbe()

        val manager = system.actorOf(
          ManagerConnector.props(
            amiBus,
            new XucAmiConfig(app.configuration),
            "default",
            amiBusConnector.ref,
            configDispatcher.ref,
            repo
          )
        )
        system.actorOf(Props(new Actor() {
          amiBus.subscribe(self, AmiType.separator)

          def receive: Receive = { case response: AmiResponse =>
            probe.ref ! response
          }
        }))
        manager ! AmiAction(new CoreStatusAction())
        probe.expectMsgClass(5.seconds, classOf[AmiResponse])
      }
    }
  )
}
