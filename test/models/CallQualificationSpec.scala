package models

import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import xivo.models.{
  CallQualification,
  CallQualificationAnswer,
  SubQualification
}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class CallQualificationSpec extends AnyWordSpec with Matchers {

  "A CallQualificationAnswer" should {
    "convert to json" in {
      val qa = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "some custom data"
      )

      val json: JsValue = Json.parse(
        """
          | {"sub_qualification_id": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
          | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "some custom data"}
          | """.stripMargin
      )

      Json.toJson(qa) shouldEqual json
    }

    "be created from json" in {
      val qa = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "some custom data"
      )

      val json: JsValue = Json.parse(
        """
          | {"sub_qualification_id": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
          | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "some custom data"}
          | """.stripMargin
      )

      json.validate[CallQualificationAnswer] match {
        case JsSuccess(cq, _) => cq shouldEqual qa
        case JsError(errors)  => fail()
      }
    }
  }

  "A CallQualification" should {
    "convert to json" in {
      val sq = List(SubQualification(Some(1), "subqualif1"))
      val q  = CallQualification(Some(1), "qualif1", sq)

      Json.toJson(q) shouldEqual Json.obj(
        "id"                -> q.id,
        "name"              -> q.name,
        "subQualifications" -> sq
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse(
        """{"id": 1, "name": "qualif1",
          | "subQualifications": [{"id": 1, "name": "subqualif1"}, {"id": 2, "name": "subqualif2"}]}""".stripMargin
      )

      val sq = List(
        SubQualification(Some(1), "subqualif1"),
        SubQualification(Some(2), "subqualif2")
      )
      val qualif = CallQualification(Some(1), "qualif1", sq)

      json.validate[CallQualification] match {
        case JsSuccess(q, _) => q shouldEqual qualif
        case JsError(errors) => fail()
      }
    }
  }
}
