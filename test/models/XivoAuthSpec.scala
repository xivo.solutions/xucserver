package models

import org.joda.time.DateTime
import play.api.libs.json.Json
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class XivoAuthSpec extends AnyWordSpec with Matchers {

  val tokenResponse: String =
    """
      |{"data": {"issued_at": "2015-08-21T15:58:29.384441", "token": "171459c4-eb68-68e8-e95d-b4c8b3f4d68a",
      |"auth_id": "8fa5473d-e44b-474f-af92-0bef6caf216f", "expires_at": "2015-08-22T10:10:29.384472",
      |"xivo_user_uuid": "8fa5473d-e44b-474f-af92-0bef6caf216f", "acls": []}}
    """.stripMargin

  "XivoAuth" should {
    "be able to decode token from json response" in {
      val token: XivoAuthToken = Json.parse(tokenResponse).as[XivoAuthToken]
      token.authId shouldBe "8fa5473d-e44b-474f-af92-0bef6caf216f"
      token.expiresAt shouldBe new DateTime(2015, 8, 22, 10, 10, 29, 384)
      token.token shouldBe "171459c4-eb68-68e8-e95d-b4c8b3f4d68a"
      token.issuedAt shouldBe new DateTime(2015, 8, 21, 15, 58, 29, 384)
      token.xivoUserUuid shouldBe Some("8fa5473d-e44b-474f-af92-0bef6caf216f")
      token.acls shouldBe List.empty
    }
  }
}
