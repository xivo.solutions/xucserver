package models

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.scalatest._
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class RescheduleCallbackSpec
    extends AnyWordSpec
    with Matchers
    with OptionValues {
  import RescheduleCallback._

  "RescheduleCallback" should {
    "convert to json" in {
      val o = RescheduleCallback(
        DateTime.parse("2016-08-09"),
        "268ce207-7619-4d79-bcf7-27215e8636eb"
      )
      Json.toJson(o) shouldEqual Json.obj(
        "dueDate"    -> "2016-08-09",
        "periodUuid" -> "268ce207-7619-4d79-bcf7-27215e8636eb"
      )
    }

    "be created from json" in {
      val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd")
      val json: JsValue = Json.parse(
        """{"dueDate": "2016-08-09", "periodUuid": "268ce207-7619-4d79-bcf7-27215e8636eb"}"""
      )
      json.validate[RescheduleCallback] match {
        case JsSuccess(o, _) =>
          o shouldEqual RescheduleCallback(
            DateTime.parse("2016-08-09", dateFormat),
            "268ce207-7619-4d79-bcf7-27215e8636eb"
          )
        case JsError(e) => fail(e.mkString("\n"))
      }
    }
  }

}
