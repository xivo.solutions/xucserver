package models

import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.model.PhoneHintStatus
import pekkotest.TestKitSpec
import play.api.libs.json.Json
import services.video.model.VideoEvents

class ContactSheetSpec
    extends TestKitSpec("ContactSheetSpec")
    with MockitoSugar {
  "A ContactSheet" should {
    "convert to json" in {
      val cs = ContactSheet(
        "Tony Mourier",
        "Web software developer junior",
        "Aka Furious turtle",
        "https//my-url/turtle-picture.png",
        false,
        true,
        false,
        true,
        ContactSheetStatus(PhoneHintStatus.AVAILABLE, VideoEvents.Available),
        Map(
          ContactSheetActionEnum.Call -> ContactSheetAction(
            List("1000"),
            false
          ),
          ContactSheetActionEnum.Video -> ContactSheetAction(
            List("tmourier"),
            false
          ),
          ContactSheetActionEnum.Chat -> ContactSheetAction(
            List("tmourier"),
            false
          ),
          ContactSheetActionEnum.Mail -> ContactSheetAction(
            List("tony@mourier.org"),
            false
          ),
          ContactSheetActionEnum.Edit -> ContactSheetAction(List("42"), false)
        ),
        List(ContactSheetSources("xivo", "42")),
        List(
          ContactSheetDetailCategorie(
            "Contacts",
            List(
              ContactSheetDetailField(
                "Téléphone interne",
                "1000",
                ContactSheetDataType.PhoneNumber
              ),
              ContactSheetDetailField(
                "Téléphone pro",
                "1001",
                ContactSheetDataType.PhoneNumber
              ),
              ContactSheetDetailField(
                "E-mail",
                "tony@mourier.org",
                ContactSheetDataType.Mail
              ),
              ContactSheetDetailField(
                "Site Web",
                "https://mourier.org",
                ContactSheetDataType.Url
              )
            )
          )
        )
      )
      Json.toJson(cs) shouldEqual Json.obj(
        "name"          -> "Tony Mourier",
        "subtitle1"     -> "Web software developer junior",
        "subtitle2"     -> "Aka Furious turtle",
        "picture"       -> "https//my-url/turtle-picture.png",
        "isPersonal"    -> false,
        "isFavorite"    -> true,
        "isMeetingroom" -> false,
        "canBeFavorite" -> true,
        "status" -> Json.obj(
          "phone" -> 0,
          "video" -> "Available"
        ),
        "actions" -> Json.obj(
          "Call" -> Json.obj(
            "args" -> Json.arr(
              "1000"
            ),
            "disable" -> false
          ),
          "Edit" -> Json.obj(
            "args" -> Json.arr(
              "42"
            ),
            "disable" -> false
          ),
          "Video" -> Json.obj(
            "args" -> Json.arr(
              "tmourier"
            ),
            "disable" -> false
          ),
          "Chat" -> Json.obj(
            "args" -> Json.arr(
              "tmourier"
            ),
            "disable" -> false
          ),
          "Mail" -> Json.obj(
            "args" -> Json.arr(
              "tony@mourier.org"
            ),
            "disable" -> false
          )
        ),
        "sources" -> Json.arr(
          Json.obj("name" -> "xivo", "id" -> "42")
        ),
        "details" -> Json.arr(
          Json.obj(
            "name" -> "Contacts",
            "fields" -> Json.arr(
              Json.obj(
                "name"     -> "Téléphone interne",
                "data"     -> "1000",
                "dataType" -> "PhoneNumber"
              ),
              Json.obj(
                "name"     -> "Téléphone pro",
                "data"     -> "1001",
                "dataType" -> "PhoneNumber"
              ),
              Json.obj(
                "name"     -> "E-mail",
                "data"     -> "tony@mourier.org",
                "dataType" -> "Mail"
              ),
              Json.obj(
                "name"     -> "Site Web",
                "data"     -> "https://mourier.org",
                "dataType" -> "Url"
              )
            )
          )
        )
      )
    }
  }
}
