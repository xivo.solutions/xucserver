package models

import java.util.UUID

import org.joda.time.{LocalDate, LocalTime}
import play.api.libs.json.Json
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class CallbackListSpec extends AnyWordSpec with Matchers {

  "A CallbackRequest" should {
    "be written to JSON" in {
      val listUuid = UUID.randomUUID()
      val cbUuid   = UUID.randomUUID()
      val period = PreferredCallbackPeriod(
        None,
        "the period",
        new LocalTime(9, 0, 0),
        new LocalTime(12, 0, 0),
        true
      )
      Json
        .toJson(
          CallbackRequest(
            Some(cbUuid),
            listUuid,
            Some("1000"),
            Some("7200"),
            Some("John"),
            Some("Doe"),
            Some("company"),
            Some("my description"),
            Some(12),
            Some(3),
            false,
            Some(period),
            new LocalDate(2015, 1, 10),
            Some("1234")
          )
        )
        .toString() shouldEqual
        s"""{"uuid":"$cbUuid","listUuid":"$listUuid","phoneNumber":"1000","mobilePhoneNumber":"7200","firstName":"John",""" +
        s""""lastName":"Doe","company":"company","description":"my description","agentId":12,"queueId":3,"clotured":false,"preferredPeriod":${Json
            .toJson(period)
            .toString},""" +
        s""""dueDate":"2015-01-10","voiceMessageRef":"1234"}"""
    }
  }

}
