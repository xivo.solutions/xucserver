package models

import org.scalatest.OptionValues
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class UsersQueueMembershipSpec
    extends AnyWordSpec
    with Matchers
    with OptionValues {
  import UsersQueueMembership._
  import QueueMembership._

  "UsersQueueMembership" should {
    "convert to json" in {
      val o = UsersQueueMembership(
        List(10, 19),
        List(QueueMembership(1, 2), QueueMembership(3, 5))
      )

      Json.toJson(o) shouldEqual Json.obj(
        "userIds"    -> o.userIds,
        "membership" -> o.membership
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse(
        """{"userIds": [10, 19], "membership": [{"queueId":1, "penalty":2}, {"queueId":3, "penalty":5}]}"""
      )
      val res = UsersQueueMembership(
        List(10, 19),
        List(QueueMembership(1, 2), QueueMembership(3, 5))
      )

      json.validate[UsersQueueMembership] match {
        case JsSuccess(qm, _) => qm shouldEqual res
        case JsError(errors)  => fail()
      }
    }
  }

}
