package models

import play.api.libs.json.Json
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import fixtures.DirectoryFixtures

class XivoDirdSpec extends AnyWordSpec with Matchers with DirectoryFixtures {

  val responseWithFullContactSheetInfos: String =
    """{
      |  "column_headers": [
      |    "Name",
      |    "Post",
      |    "Service",
      |    "Internal phone",
      |    "Professional phone",
      |    "Mobile phone",
      |    "Home phone",
      |    "E-mail",
      |    "Fax",
      |    "Manager",
      |    "Company",
      |    "Website",
      |    "Office",
      |    "Adress",
      |    "",
      |    "",
      |    ""
      |  ],
      |  "column_types": [
      |    "name",
      |    "info",
      |    "info",
      |    "number",
      |    "callable",
      |    "callable",
      |    "callable",
      |    "email",
      |    "callable",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "favorite",
      |    "personal",
      |    "picture"
      |  ],
      |  "results": [
      |    {
      |      "column_values": [
      |        "Allovon Etienne",
      |        "Developper",
      |        "RandD",
      |        "0033427466334",
      |        "123456",
      |        "0033633480694",
      |        "654321",
      |        "eallovon@xivo.solutions",
      |        "9876",
      |        "Laurent Meiller",
      |        "Avencall",
      |        "documentation.xivo.solutions",
      |        "Software Factory",
      |        "Dardilly",
      |        true,
      |        true,
      |        "edrcftvgbyhnujdeszrftvgbyhnjujuhttnjhlkdhrgyudeijgyuhsrdgyuhsbbguh"
      |      ],
      |      "relations": {
      |        "xivo_id": null,
      |        "agent_id": null,
      |        "user_id": 1,
      |        "user_uuid": null,
      |        "endpoint_id": null,
      |        "source_entry_id": null
      |      },
      |      "source": "avldap"
      |    }
      |  ]
      |}""".stripMargin

  val expectedParsingForFullInfos: DirSearchItem =
    dirSearchItem(
      item = contactSheetDisplayView(
        name = ContactSheetValueString("Allovon Etienne"),
        subtitle1 = ContactSheetValueString(""),
        subtitle2 = ContactSheetValueString(""),
        title = ContactSheetValueWithLabel("Post", "Developper"),
        service = ContactSheetValueWithLabel("Service", "RandD"),
        phone = ContactSheetValueWithLabel("Internal phone", "0033427466334"),
        phonePro = ContactSheetValueWithLabel("Professional phone", "123456"),
        phoneMobile = ContactSheetValueWithLabel("Mobile phone", "0033633480694"),
        phoneHome = ContactSheetValueWithLabel("Home phone", "654321"),
        mail = ContactSheetValueWithLabel("E-mail", "eallovon@xivo.solutions"),
        fax = ContactSheetValueWithLabel("Fax", "9876"),
        manager = ContactSheetValueWithLabel("Manager", "Laurent Meiller"),
        company = ContactSheetValueWithLabel("Company", "Avencall"),
        website = ContactSheetValueWithLabel("Website", "documentation.xivo.solutions"),
        office = ContactSheetValueWithLabel("Office", "Software Factory"),
        location = ContactSheetValueWithLabel("Adress", "Dardilly"),
        favorite = ContactSheetValueBoolean(true),
        personal = ContactSheetValueBoolean(true),
        meetingroom = ContactSheetValueBoolean(false),
        picture = ContactSheetValueString(
          "edrcftvgbyhnujdeszrftvgbyhnjujuhttnjhlkdhrgyudeijgyuhsrdgyuhsbbguh"
        )
      ),
      relations = relations(
        userId = Some(1)
      ),
      source = List(dirSource("avldap"))
    )

  val responseWithPartialContactSheetInfos: String =
    """{
      |  "column_headers": [
      |    "Name",
      |    "subtitle1",
      |    "subtitle2",
      |    "Post",
      |    "Service",
      |    "Internal phone",
      |    "Professional phone",
      |    "Mobile phone",
      |    "Home phone",
      |    "E-mail",
      |    "Fax",
      |    "Manager",
      |    "Company",
      |    "Website",
      |    "Office",
      |    "Adress",
      |    "",
      |    "",
      |    ""
      |  ],
      |  "column_types": [
      |    "name",
      |    "subtitle1",
      |    "subtitle2",
      |    "info",
      |    "info",
      |    "number",
      |    "callable",
      |    "callable",
      |    "callable",
      |    "email",
      |    "callable",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "favorite",
      |    "personal",
      |    "picture"
      |  ],
      |  "results": [
      |    {
      |      "column_values": [
      |        "Allovon Etienne",
      |        "",
      |        "",
      |        "Developper",
      |        "RandD",
      |        "0033427466334",
      |        null,
      |        "0033633480694",
      |        null,
      |        "eallovon@xivo.solutions",
      |        null,
      |        "Laurent Meiller",
      |        "Avencall",
      |        null,
      |        "Software Factory",
      |        "Dardilly",
      |        false,
      |        false,
      |        null
      |      ],
      |      "relations": {
      |        "xivo_id": null,
      |        "agent_id": null,
      |        "user_id": 1,
      |        "user_uuid": null,
      |        "endpoint_id": null,
      |        "source_entry_id": null
      |      },
      |      "source": "avldap"
      |    }
      |  ]
      |}""".stripMargin

  val expectedParsingForPartialInfos: DirSearchItem =
    dirSearchItem(
      item = contactSheetDisplayView(
        name = ContactSheetValueString("Allovon Etienne"),
        title = ContactSheetValueWithLabel("Post", "Developper"),
        service = ContactSheetValueWithLabel("Service", "RandD"),
        phone = ContactSheetValueWithLabel("Internal phone", "0033427466334"),
        phoneMobile = ContactSheetValueWithLabel("Mobile phone", "0033633480694"),
        mail = ContactSheetValueWithLabel("E-mail", "eallovon@xivo.solutions"),
        manager = ContactSheetValueWithLabel("Manager", "Laurent Meiller"),
        company = ContactSheetValueWithLabel("Company", "Avencall"),
        office = ContactSheetValueWithLabel("Office", "Software Factory"),
        location = ContactSheetValueWithLabel("Adress", "Dardilly")
      ),
      relations = relations(
        userId = Some(1)
      ),
      source = List(dirSource("avldap"))
    )

  val ecampbellRawDirdResult: String =
    """{
      |  "column_headers": [
      |    "Nom",
      |    "Subtitle1",
      |    "Subtitle2",
      |    "Téléphone interne",
      |    "Téléphone pro",
      |    "Téléphone mobile",
      |    "Téléphone autre",
      |    "E-mail",
      |    "Télécopie",
      |    "Fonction",
      |    "Service",
      |    "Manager",
      |    "Société",
      |    "Site web",
      |    "Bureau",
      |    "Adresse",
      |    "",
      |    "",
      |    "Avatar"
      |  ],
      |  "column_types": [
      |    "name",
      |    "subtitle1",
      |    "subtitle2",
      |    "number",
      |    "callable",
      |    "callable",
      |    "callable",
      |    "email",
      |    "callable",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "favorite",
      |    "personal",
      |    "picture"
      |  ],
      |  "results": [
      |    {
      |      "column_values": [
      |        "Elizabeth Campbell",
      |        "Petroleum engineer",
      |        "Ressources Humaines",
      |        "1568",
      |        "0544195920",
      |        "0658840188",
      |        null,
      |        "elizabeth.campbell@xivo-dev.com",
      |        null,
      |        "Petroleum engineer",
      |        "Ressources Humaines",
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        false,
      |        false,
      |        null
      |      ],
      |      "relations": {
      |        "xivo_id": null,
      |        "agent_id": null,
      |        "user_id": null,
      |        "user_uuid": null,
      |        "endpoint_id": null,
      |        "source_entry_id": null
      |      },
      |      "source": "ldap"
      |    },
      |    {
      |      "column_values": [
      |        "Elizabeth Campbell",
      |        null,
      |        null,
      |        "1630",
      |        null,
      |        null,
      |        null,
      |        "elizabeth.campbell@xivo-dev.com",
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        false,
      |        false,
      |        null
      |      ],
      |      "relations": {
      |        "xivo_id": "4e288243-ee47-4bc7-91f5-5eca844ab05b",
      |        "agent_id": null,
      |        "user_id": 171,
      |        "user_uuid": "a8cf14f4-b099-444d-85a3-dfdd9b2bc557",
      |        "endpoint_id": 160,
      |        "source_entry_id": "171"
      |      },
      |      "source": "internal"
      |    }
      |  ],
      |  "term": "eliz"
      |}
      |
      |
      |""".stripMargin

  val sourcelessEcampbellRawDirdResult: String =
    """{
      |  "column_headers": [
      |    "Nom",
      |    "Subtitle1",
      |    "Subtitle2",
      |    "Téléphone interne",
      |    "Téléphone pro",
      |    "Téléphone mobile",
      |    "Téléphone autre",
      |    "E-mail",
      |    "Télécopie",
      |    "Fonction",
      |    "Service",
      |    "Manager",
      |    "Société",
      |    "Site web",
      |    "Bureau",
      |    "Adresse",
      |    "",
      |    "",
      |    "Avatar"
      |  ],
      |  "column_types": [
      |    "name",
      |    "subtitle1",
      |    "subtitle2",
      |    "number",
      |    "callable",
      |    "callable",
      |    "callable",
      |    "email",
      |    "callable",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "info",
      |    "favorite",
      |    "personal",
      |    "picture"
      |  ],
      |  "results": [
      |    {
      |      "column_values": [
      |        "Elizabeth Campbell",
      |        null,
      |        null,
      |        "1630",
      |        null,
      |        null,
      |        null,
      |        "elizabeth.campbell@xivo-dev.com",
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        null,
      |        false,
      |        false,
      |        null
      |      ],
      |      "relations": {
      |        "xivo_id": "4e288243-ee47-4bc7-91f5-5eca844ab05b",
      |        "agent_id": null,
      |        "user_id": 171,
      |        "user_uuid": "a8cf14f4-b099-444d-85a3-dfdd9b2bc557",
      |        "endpoint_id": 160,
      |        "source_entry_id": "171"
      |      }
      |    }
      |  ],
      |  "term": "eliz"
      |}
      |
      |
      |""".stripMargin

  val ecampbellLdap: DirSearchItem = dirSearchItem(
    item = contactSheetDisplayViewFR(
      name = ContactSheetValueString("Elizabeth Campbell"),
      subtitle1 = ContactSheetValueString("Petroleum engineer"),
      subtitle2 = ContactSheetValueString("Ressources Humaines"),
      title = ContactSheetValueWithLabel("Fonction","Petroleum engineer"),
      service = ContactSheetValueWithLabel("Service","Ressources Humaines"),
      phone = ContactSheetValueWithLabel("Téléphone interne", "1568"),
      phonePro = ContactSheetValueWithLabel("Téléphone pro", "0544195920"),
      phoneMobile = ContactSheetValueWithLabel("Téléphone mobile", "0658840188"),
      mail = ContactSheetValueWithLabel("E-mail", "elizabeth.campbell@xivo-dev.com")
    ),
    source = List(dirSource("ldap"))
  )

  val ecampbellInternal: DirSearchItem = dirSearchItem(
    item = contactSheetDisplayViewFR(
      name = ContactSheetValueString("Elizabeth Campbell"),
      phone = ContactSheetValueWithLabel("Téléphone interne", "1630"),
      mail = ContactSheetValueWithLabel("E-mail", "elizabeth.campbell@xivo-dev.com")
    ),
    relations = relations(
      endpointId = Some(160),
      userId = Some(171),
      xivoId = Some("4e288243-ee47-4bc7-91f5-5eca844ab05b")
    ),
    source = List(dirSource("internal",Some("171")))
  )

  "XivoDird" should {
    "decode display filter default format" in :
      val response: DirSearchResult =
        DirSearchResult.parse(Json.parse(responseWithFullContactSheetInfos))
      response.items.head shouldBe expectedParsingForFullInfos

    "be able to decode search response with missing infos" in :
      val response: DirSearchResult =
        DirSearchResult.parse(Json.parse(responseWithPartialContactSheetInfos))
      response.items.head shouldBe expectedParsingForPartialInfos

    "be able to decode search response with multiple entries" in :
      val response: DirSearchResult = DirSearchResult.parse(Json.parse(ecampbellRawDirdResult))
      response.items.head shouldBe ecampbellLdap.copy(
        rank = 0
      )
      response.items(1) shouldBe ecampbellInternal.copy(
        rank = 1
      )

    "not break on sourceless entries" in:
      val response: DirSearchResult = DirSearchResult.parse(Json.parse(sourcelessEcampbellRawDirdResult))
      response.items.head shouldBe ecampbellInternal.copy(source = List(DirSource(id="",entry=Some("171"))))

  }
}
