import pekkotest.TestKitSpec
import models.LineConfig
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.Json
import xivo.models.XivoDevice
import xuctest.JsonMatchers

class LineConfigSpec
    extends TestKitSpec("LineConfigSpec")
    with MockitoSugar
    with JsonMatchers {

  import xivo.models.LineHelper.makeLine

  "A LineConfig" should {
    "be written to JSON without password when device is defined" in {
      val device = XivoDevice("1000", Some("192.168.0.1"), "Yealink")
      val lineWithDevice =
        makeLine(3, "context", "SIP", "ojhf", Some(device), None, "ip", false)
      val lineCfg = LineConfig("3", "2002", Some(lineWithDevice))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf","xivoIp":"ip","hasDevice":true,"isUa":false,"webRtc":false,"vendor":"Yealink","number":"2002", "mobileApp":false}"""
      )
    }
    "be written to JSON with password when device is not defined" in {
      val lineWithDevice = makeLine(
        3,
        "context",
        "SIP",
        "ojhf",
        None,
        Some("extraPasswd"),
        "ip",
        true
      )
      val lineCfg = LineConfig("3", "2002", Some(lineWithDevice))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf","xivoIp":"ip","hasDevice":false,"isUa":false,"webRtc":true,"vendor":null,"number":"2002","password":"extraPasswd","sipProxyName":"default", "mobileApp":false}"""
      )
    }
    "set hasDevice to false if line is ua (unique account)" in {
      val lineWithUa = makeLine(
        3,
        "context",
        "SIP",
        "ojhf_w",
        None,
        None,
        "ip",
        webRTC = true,
        ua = true,
        registrar = Some("default")
      )
      val lineCfg = LineConfig("3", "2002", Some(lineWithUa))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf_w","xivoIp":"ip","hasDevice":false,"isUa":true,"webRtc":true,"vendor":null,"number":"2002","password":"","sipProxyName":"default", "mobileApp":false}"""
      )
    }
    "include sipProxyName" in {
      val lineWithUa = makeLine(
        3,
        "context",
        "SIP",
        "ojhf_w",
        None,
        None,
        "ip",
        registrar = Some("mds1"),
        webRTC = true,
        ua = true
      )
      val lineCfg = LineConfig("3", "2002", Some(lineWithUa))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf_w","xivoIp":"ip","hasDevice":false,"isUa":true,"webRtc":true,"vendor":null,"number":"2002","password":"","sipProxyName":"mds1", "mobileApp":false}"""
      )
    }
    "set isUa to true if line is ua (unique account)" in {
      val lineWithUa = makeLine(
        3,
        "context",
        "SIP",
        "ojhf_w",
        None,
        None,
        "ip",
        webRTC = true,
        ua = true,
        registrar = Some("default")
      )
      val lineCfg = LineConfig("3", "2002", Some(lineWithUa))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf_w","xivoIp":"ip","hasDevice":false,"isUa":true,"webRtc":true,"vendor":null,"number":"2002","password":"","sipProxyName":"default", "mobileApp":false}"""
      )
    }
    "set isUa to false if line is not ua (unique account)" in {
      val lineWithoutUa = makeLine(
        3,
        "context",
        "SIP",
        "ojhf_w",
        None,
        None,
        "ip",
        webRTC = true,
        ua = false,
        registrar = Some("default")
      )
      val lineCfg = LineConfig("3", "2002", Some(lineWithoutUa))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf_w","xivoIp":"ip","hasDevice":false,"isUa":false,"webRtc":true,"vendor":null,"number":"2002","password":"","sipProxyName":"default", "mobileApp":false}"""
      )
    }
    "set mobileApp to true if line has mobileApp" in {
      val lineWithMobileApp = makeLine(
        3,
        "context",
        "SIP",
        "ojhf",
        None,
        None,
        "ip",
        webRTC = true,
        registrar = Some("default"),
        mobileApp = true
      )
      val lineCfg = LineConfig("3", "2002", Some(lineWithMobileApp))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf","xivoIp":"ip","hasDevice":false,"isUa":false,"webRtc":true,"vendor":null,"number":"2002","password":"","sipProxyName":"default", "mobileApp":true}"""
      )
    }
    "set mobileApp to false if line has no mobileApp" in {
      val lineWithoutMobileApp = makeLine(
        3,
        "context",
        "SIP",
        "ojhf",
        None,
        None,
        "ip",
        webRTC = true,
        registrar = Some("default"),
        mobileApp = false
      )
      val lineCfg = LineConfig("3", "2002", Some(lineWithoutMobileApp))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf","xivoIp":"ip","hasDevice":false,"isUa":false,"webRtc":true,"vendor":null,"number":"2002","password":"","sipProxyName":"default", "mobileApp":false}"""
      )
    }
    "set lineConfig with sipPort" in {

      val line = makeLine(
        3,
        "context",
        "SIP",
        "ojhf_w",
        None,
        None,
        "ip",
        webRTC = true,
        ua = false,
        registrar = Some("default")
      )
      var lineCfg = LineConfig("3", "2002", Some(line), Option("5060"))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf_w","xivoIp":"ip","hasDevice":false,"isUa":false,"webRtc":true,"vendor":null,"number":"2002","mobileApp":false,"password":"","sipProxyName":"default", "sipPort":"5060"}"""
      )

      lineCfg = LineConfig("3", "2002", Some(line))
      Json.toJson(lineCfg).toString() should matchJson(
        s"""{"id":"3","name":"ojhf_w","xivoIp":"ip","hasDevice":false,"isUa":false,"webRtc":true,"vendor":null,"number":"2002","mobileApp":false,"password":"","sipProxyName":"default"}"""
      )

    }

  }
}
