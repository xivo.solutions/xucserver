package models

import org.scalatest.OptionValues
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class UserIdListSpec extends AnyWordSpec with Matchers with OptionValues {
  import UserIdList._

  "UserIdList" should {
    "convert to json" in {
      val o = UserIdList(List(1, 5, 7))
      Json.toJson(o) shouldEqual Json.obj(
        "userIds" -> o.userIds
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse("""{"userIds": [1,5,7,42]}""")
      json.validate[UserIdList] match {
        case JsSuccess(o, _) => o shouldEqual UserIdList(List(1, 5, 7, 42))
        case JsError(errors) => fail()
      }
    }
  }

}
