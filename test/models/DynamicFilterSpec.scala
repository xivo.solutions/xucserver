package models

import anorm.{NamedParameter, ToParameterValue}
import play.api.libs.json._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class DynamicFilterSpec extends AnyWordSpec with Matchers {

  "NoCasing" should {
    "preserve camel case" in {
      NoCasing.transform("firstNameOfUser") should be("firstNameOfUser")
    }

    "preserve snake case" in {
      NoCasing.transform("first_name_of_user") should be("first_name_of_user")
    }
  }

  "ToSnakeCase" should {
    "convert camel case to snake case" in {
      ToSnakeCase.transform("firstNameOfUser") should be("first_name_of_user")
    }

    "preserve snake case" in {
      ToSnakeCase.transform("first_name_of_user") should be(
        "first_name_of_user"
      )
    }
  }

  "DynamicFilterOperator" should {
    "convert string repr to object" in {
      DynamicFilterOperator.toObject(Some("=")) should be(Some(OperatorEq))
      DynamicFilterOperator.toObject(Some("!=")) should be(Some(OperatorNeq))
      DynamicFilterOperator.toObject(Some(">")) should be(Some(OperatorGt))
      DynamicFilterOperator.toObject(Some(">=")) should be(Some(OperatorGte))
      DynamicFilterOperator.toObject(Some("<")) should be(Some(OperatorLt))
      DynamicFilterOperator.toObject(Some("<=")) should be(Some(OperatorLte))
      DynamicFilterOperator.toObject(Some("like")) should be(Some(OperatorLike))
      DynamicFilterOperator.toObject(Some("ilike")) should be(
        Some(OperatorIlike)
      )
      DynamicFilterOperator.toObject(Some("is null")) should be(
        Some(OperatorIsNull)
      )
      DynamicFilterOperator.toObject(Some("is not null")) should be(
        Some(OperatorIsNotNull)
      )
    }
  }

  "DynamicFilter" should {
    "convert DynamicFilter to sql condition" in {
      implicit val casing: NoCasing.type = NoCasing
      val f                              = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"))
      DynamicFilter.toSqlCondition(f) should be("myField = {myField}")
    }

    "convert DynamicFilter with is null operator to sql condition" in {
      implicit val casing: NoCasing.type = NoCasing
      val f                              = DynamicFilter("myField", Some(OperatorIsNull), None)
      DynamicFilter.toSqlCondition(f) should be("myField is null")
    }

    "convert DynamicFilter with is not null operator to sql condition" in {
      implicit val casing: NoCasing.type = NoCasing
      val f                              = DynamicFilter("myField", Some(OperatorIsNotNull), None)
      DynamicFilter.toSqlCondition(f) should be("myField is not null")
    }

    "convert list of DynamicFilter to sql condition" in {
      implicit val casing: NoCasing.type = NoCasing
      val f = List(
        DynamicFilter("myField", Some(OperatorEq), Some("myvalue")),
        DynamicFilter("myOtherField", Some(OperatorEq), Some("myOthervalue"))
      )
      DynamicFilter.toSqlCondition(f) should be(
        "myField = {myField} AND myOtherField = {myOtherField}"
      )
    }

    "convert list of DynamicFilter, including nullity test, to sql condition" in {
      implicit val casing: NoCasing.type = NoCasing
      val f = List(
        DynamicFilter("myField", Some(OperatorEq), Some("myvalue")),
        DynamicFilter("myOtherField", Some(OperatorIsNull), None)
      )
      DynamicFilter.toSqlCondition(f) should be(
        "myField = {myField} AND myOtherField is null"
      )
    }

    "convert DynamicFilter to NamedParameter" in {
      implicit val typer: AllFieldsString.type = AllFieldsString
      val f                                    = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"))
      val t                                    = ToParameterValue.apply[String]
      DynamicFilter.toNamedParameter(f) should be(
        NamedParameter(f.field, t(f.value.get))
      )
    }

    "convert list of DynamicFilter to list of NamedParameter" in {
      implicit val typer: AllFieldsString.type = AllFieldsString
      val f = List(
        DynamicFilter("myField", Some(OperatorEq), Some("myvalue")),
        DynamicFilter("myOtherField", Some(OperatorEq), Some("myOthervalue"))
      )
      val t        = ToParameterValue.apply[String]
      val expected = f map (i => NamedParameter(i.field, t(i.value.get)))
      DynamicFilter.toNamedParameterList(f) should be(expected)
    }

    "convert DynamicFilter to json" in {
      implicit val casing: NoCasing.type = NoCasing
      val f                              = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"))

      Json.toJson(f) should be(
        Json.parse(
          """{"field": "myField", "operator": "=", "value": "myvalue", "order": null}"""
        )
      )
    }

    "convert json to DynamicFilter" in {
      implicit val casing: NoCasing.type = NoCasing
      val expected                       = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"))

      val result = Json
        .parse("""{"field": "myField", "operator": "=", "value": "myvalue"}""")
        .validate[DynamicFilter]
      result should be(JsSuccess(expected))
    }

    "convert DynamicFilter to Order clause" in {
      implicit val casing: NoCasing.type = NoCasing
      val a = DynamicFilter(
        "myField",
        Some(OperatorEq),
        Some("myvalue"),
        Some(OrderAsc)
      )
      val d = DynamicFilter(
        "myField",
        Some(OperatorEq),
        Some("myvalue"),
        Some(OrderDesc)
      )
      val no = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"), None)

      DynamicFilter.toOrderClause(a) should be(Some("myField ASC"))
      DynamicFilter.toOrderClause(d) should be(Some("myField DESC"))
      DynamicFilter.toOrderClause(no) should be(None)
    }

    "convert List of DynamicFilter to Order clause" in {
      implicit val casing: NoCasing.type = NoCasing
      val l = List(
        DynamicFilter(
          "myField1",
          Some(OperatorEq),
          Some("myvalue"),
          Some(OrderAsc)
        ),
        DynamicFilter(
          "myField2",
          Some(OperatorEq),
          Some("myvalue"),
          Some(OrderDesc)
        ),
        DynamicFilter("myField3", Some(OperatorEq), Some("myvalue"), None)
      )

      DynamicFilter.toOrderClause(l) should be(
        " ORDER BY myField1 ASC, myField2 DESC"
      )
    }
  }
}
