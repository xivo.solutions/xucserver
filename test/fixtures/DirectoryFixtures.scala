package fixtures

import models.{
  ContactSheet,
  ContactSheetAction,
  ContactSheetActionEnum,
  ContactSheetDetailCategorie,
  ContactSheetDisplayView,
  ContactSheetSources,
  ContactSheetStatus,
  ContactSheetValueBoolean,
  ContactSheetValueString,
  ContactSheetValueWithLabel,
  DirSearchItem,
  DirSearchResult,
  DirSource,
  Relations,
  RichEntry
}
import org.xivo.cti.model.PhoneHintStatus
import services.video.model.VideoEvents
import xivo.services.XivoDirectory.{DirLookupResult, Favorites}

import scala.collection.mutable

trait DirectoryFixtures:

  def contactSheetValueWithLabel(
      label: String = "",
      value: String = ""
  ): ContactSheetValueWithLabel = ContactSheetValueWithLabel(
    label = label,
    value = value
  )

  def dirSource(
      id: String = "",
      entry: Option[String] = None
  ): DirSource = DirSource(
    id = id,
    entry = entry
  )

  def contactSheetSources(
      name: String = "",
      id: String = ""
  ): ContactSheetSources = ContactSheetSources(
    name = name,
    id = id
  )

  def contactSheetAction(
      args: List[String] = List.empty,
      disable: Boolean = false
  ): ContactSheetAction = ContactSheetAction(
    args = args,
    disable = disable
  )

  def contactSheetStatus(
      phone: PhoneHintStatus = PhoneHintStatus.AVAILABLE,
      video: VideoEvents.Event = VideoEvents.Available
  ): ContactSheetStatus = ContactSheetStatus(
    phone = phone,
    video = video
  )

  def contactSheet(
      name: String = "",
      subtitle1: String = "",
      subtitle2: String = "",
      picture: String = "",
      isPersonal: Boolean = false,
      isFavorite: Boolean = false,
      isMeetingroom: Boolean = false,
      canBeFavorite: Boolean = false,
      status: ContactSheetStatus = contactSheetStatus(),
      actions: Map[ContactSheetActionEnum.Action, ContactSheetAction] =
        Map.empty,
      sources: List[ContactSheetSources] = List.empty,
      details: List[ContactSheetDetailCategorie] = List.empty
  ): ContactSheet = ContactSheet(
    name = name,
    subtitle1 = subtitle1,
    subtitle2 = subtitle2,
    picture = picture,
    isPersonal = isPersonal,
    isFavorite = isFavorite,
    isMeetingroom = isMeetingroom,
    canBeFavorite = canBeFavorite,
    status = status,
    actions = actions,
    sources = sources,
    details = details
  )

  def richEntry(
      status: PhoneHintStatus = PhoneHintStatus.AVAILABLE,
      fields: mutable.Buffer[Any] = mutable.Buffer(),
      videoStatus: Option[VideoEvents.Event] = None,
      contactId: Option[String] = None,
      source: List[String] = List.empty,
      favorite: Option[Boolean] = None,
      username: Option[String] = None,
      url: Option[String] = None,
      personal: Boolean = false
  ): RichEntry = RichEntry(
    status = status,
    fields = fields,
    videoStatus = videoStatus,
    contactId = contactId,
    source = source,
    favorite = favorite,
    username = username,
    url = url,
    personal = personal
  )

  def favorites(
      result: DirSearchResult = dirSearchResult(),
      toContactSheet: Boolean = false
  ): Favorites = Favorites(
    result = result,
    toContactSheet = toContactSheet
  )

  def dirLookupResult(
      result: DirSearchResult = dirSearchResult(),
      toContactSheet: Boolean = false
  ): DirLookupResult = DirLookupResult(
    result = result,
    toContactSheet = toContactSheet
  )

  def dirSearchResult(
      columnHeaders: List[String] = List.empty,
      columnTypes: List[String] = List.empty,
      items: List[DirSearchItem] = List.empty
  ): DirSearchResult = DirSearchResult(
    columnHeaders = columnHeaders,
    columnTypes = columnTypes,
    items = items
  )

  def relations(
      agentId: Option[Long] = None,
      endpointId: Option[Long] = None,
      userId: Option[Long] = None,
      xivoId: Option[String] = None
  ): Relations = Relations(
    agentId = agentId,
    endpointId = endpointId,
    userId = userId,
    xivoId = xivoId
  )

  def dirSearchItem(
      item: ContactSheetDisplayView = contactSheetDisplayView(),
      relations: Relations = relations(),
      source: List[DirSource] = List.empty,
      rank: Int = 0
  ): DirSearchItem = DirSearchItem(
    item = item,
    relations = relations,
    source = source,
    rank = rank
  )

  def contactSheetDisplayViewFR(
      name: ContactSheetValueString = ContactSheetValueString(""),
      subtitle1: ContactSheetValueString = ContactSheetValueString(""),
      subtitle2: ContactSheetValueString = ContactSheetValueString(""),
      title: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Fonction", ""),
      service: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Service", ""),
      phone: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Téléphone interne", ""),
      phonePro: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Téléphone pro", ""),
      phoneMobile: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Téléphone mobile", ""),
      phoneHome: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Téléphone autre", ""),
      mail: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("E-mail", ""),
      fax: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Télécopie", ""),
      manager: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Manager", ""),
      company: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Société", ""),
      website: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Site web", ""),
      office: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Bureau", ""),
      location: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Adresse", ""),
      favorite: ContactSheetValueBoolean = ContactSheetValueBoolean(false),
      personal: ContactSheetValueBoolean = ContactSheetValueBoolean(false),
      meetingroom: ContactSheetValueBoolean = ContactSheetValueBoolean(false),
      picture: ContactSheetValueString = ContactSheetValueString("")
  ): ContactSheetDisplayView = ContactSheetDisplayView(
    name = name,
    subtitle1 = subtitle1,
    subtitle2 = subtitle2,
    title = title,
    service = service,
    phone = phone,
    phonePro = phonePro,
    phoneMobile = phoneMobile,
    phoneHome = phoneHome,
    mail = mail,
    fax = fax,
    manager = manager,
    company = company,
    website = website,
    office = office,
    location = location,
    favorite = favorite,
    personal = personal,
    meetingroom = meetingroom,
    picture = picture
  )

  def contactSheetDisplayView(
      name: ContactSheetValueString = ContactSheetValueString(""),
      subtitle1: ContactSheetValueString = ContactSheetValueString(""),
      subtitle2: ContactSheetValueString = ContactSheetValueString(""),
      title: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Title", ""), // ?
      service: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Service", ""), // ?
      phone: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Internal phone", ""),
      phonePro: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Professional phone", ""),
      phoneMobile: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Mobile phone", ""),
      phoneHome: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Home phone", ""),
      mail: ContactSheetValueWithLabel = ContactSheetValueWithLabel("Mail", ""),
      fax: ContactSheetValueWithLabel = ContactSheetValueWithLabel("Fax", ""),
      manager: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Manager", ""),
      company: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Company", ""),
      website: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Website", ""),
      office: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Office", ""),
      location: ContactSheetValueWithLabel =
        ContactSheetValueWithLabel("Office location", ""),
      favorite: ContactSheetValueBoolean = ContactSheetValueBoolean(false),
      personal: ContactSheetValueBoolean = ContactSheetValueBoolean(false),
      meetingroom: ContactSheetValueBoolean = ContactSheetValueBoolean(false),
      picture: ContactSheetValueString = ContactSheetValueString("")
  ): ContactSheetDisplayView = ContactSheetDisplayView(
    name = name,
    subtitle1 = subtitle1,
    subtitle2 = subtitle2,
    title = title,
    service = service,
    phone = phone,
    phonePro = phonePro,
    phoneMobile = phoneMobile,
    phoneHome = phoneHome,
    mail = mail,
    fax = fax,
    manager = manager,
    company = company,
    website = website,
    office = office,
    location = location,
    favorite = favorite,
    personal = personal,
    meetingroom = meetingroom,
    picture = picture
  )
