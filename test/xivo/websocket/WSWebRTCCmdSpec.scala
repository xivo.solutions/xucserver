package xivo.websocket

import play.api.libs.json.Json
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class WSWebRTCCmdSpec extends AnyWordSpec with Matchers {
  "WSWebRTCCmd without callId" should {
    "serialize to JSON Answer command" in {
      Json.toJson(WebRTCAnswerCmd(None)) shouldEqual Json.parse(
        """{"command":"Answer"}"""
      )
    }
    "serialize to JSON Hold command" in {
      Json.toJson(WebRTCHoldCmd(None)) shouldEqual Json.parse(
        """{"command":"Hold"}"""
      )
    }
    "serialize to JSON SendDtmf command" in {
      Json.toJson(WebRTCSendDtmfCmd('4')) shouldEqual Json.parse(
        """{"command":"SendDtmf","key":"4"}"""
      )
    }
    "serialize to JSON MuteMe command" in {
      Json.toJson(WebRTCMuteCmd(None)) shouldEqual Json.parse(
        """{"command":"ToggleMicrophone"}"""
      )
    }
  }

  "WSWebRTCCmd with callId" should {
    val sipCallId = "123-456@192.168"
    "serialize to JSON Answer command" in {
      Json.toJson(WebRTCAnswerCmd(Some(sipCallId))) shouldEqual Json.parse(
        s"""{"command":"Answer","sipCallId": "$sipCallId"}"""
      )
    }
    "serialize to JSON Hold command" in {
      Json.toJson(WebRTCHoldCmd(Some(sipCallId))) shouldEqual Json.parse(
        s"""{"command":"Hold","sipCallId": "$sipCallId"}"""
      )
    }
    "serialize to JSON MuteMe command" in {
      Json.toJson(WebRTCMuteCmd(Some(sipCallId))) shouldEqual Json.parse(
        s"""{"command":"ToggleMicrophone","sipCallId": "$sipCallId"}"""
      )
    }
  }
}
