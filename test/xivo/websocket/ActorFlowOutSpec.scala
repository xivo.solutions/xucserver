package xivo.websocket

import org.apache.pekko.Done
import org.apache.pekko.actor.{Actor, ActorRef, Props}
import org.apache.pekko.stream.{Materializer, OverflowStrategy}
import org.apache.pekko.stream.scaladsl.{Source, *}
import org.apache.pekko.stream.testkit.scaladsl.TestSink
import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import org.scalatestplus.mockito.MockitoSugar
import org.mockito.Mockito.*
import play.api.libs.json.{JsValue, Json}
import xivo.websocket.ActorFlowOut.ActorFlowOutAck
import xuctest.XucUserHelper
import xivo.xuc.XucBaseConfig

import scala.concurrent.duration.DurationInt
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import org.apache.pekko.stream.testkit.TestSubscriber

class ActorFlowOutSpec
  extends TestKitSpec("ActorFlowOutSpec")
    with MockitoSugar
    with XucUserHelper {

  class Helper() {
    val upstreamRef: TestProbe = TestProbe()
    val config: XucBaseConfig = mock[XucBaseConfig]

    when(config.webSocketMaxBurst).thenReturn(180)
    when(config.webSocketMessagePerDuration).thenReturn(90)

    def fakeWsActorProps(downstreamRef: ActorRef): Props =
      Props(new Actor {
        override def receive: Receive = {
          case ActorFlowOutAck =>
            upstreamRef.ref ! ActorFlowOutAck
          case msg =>
            downstreamRef ! msg
        }
      })

    val testSink: Sink[JsValue, TestSubscriber.Probe[JsValue]] =
      TestSink.probe[JsValue]
  }

  "ActorFlowOutSpec" should {
    "emit ack message after received a message" in new Helper {
      val message: JsValue = Json.toJson("message")
      val sinkHelper: Sink[JsValue, Future[Done]] =
        Flow[JsValue].toMat(Sink.ignore)(Keep.right)
      val actorFlow: Flow[JsValue, JsValue, ?] =
        ActorFlowOut.actorRef(fakeWsActorProps, config)

      val (ref, future) = Source
        .actorRef(3, OverflowStrategy.fail)
        .via(actorFlow)
        .toMat(sinkHelper)(Keep.both)
        .run()

      ref ! message
      ref ! message
      ref ! message

      upstreamRef.expectMsgAllOf(
        5.seconds,
        ActorFlowOutAck,
        ActorFlowOutAck,
        ActorFlowOutAck
      )
    }
  }

  "allow some burstiness on input" in new Helper {
    val message: JsValue = Json.toJson("message")

    val maxBurstMessages: List[JsValue] = List.fill(180)(message)

    val actorFlow: Flow[JsValue, JsValue, ?] =
      ActorFlowOut.actorRef(fakeWsActorProps, config, 185)
    Source(maxBurstMessages)
      .via(actorFlow)
      .runWith(testSink)
      .request(180)
      .expectNextN(maxBurstMessages)
      .request(1)
      .expectNoMessage()
  }

  "throttle too many messages on input" in new Helper {
    val message: JsValue = Json.toJson("message")
   
    val throttleExceededMessage: JsValue = Json.parse(
      """{"msgType":"Error","ctiMessage":{"Error":"Maximum throttle throughput exceeded."}}"""
    )
    
    val actorFlow: Flow[JsValue, JsValue, ?] =
      ActorFlowOut.actorRef(fakeWsActorProps, config, 185)

    val probe: TestSubscriber.Probe[JsValue] = Source(List.fill(185)(message))
      .via(actorFlow)
      .runWith(testSink)

    probe.request(185)

    val res: Seq[JsValue] = probe.expectNextN(185)
    res should contain(throttleExceededMessage)
  }
}
