package xivo.websocket

import java.util.{Date, UUID}
import models.*
import models.ResultType.*
import org.joda.time.{DateTime, LocalDate, LocalTime}
import org.xivo.cti.message.{QueueConfigUpdate as _, UserConfigUpdate as _, *}
import org.xivo.cti.model.*
import play.api.libs.json.*
import services.XucStatsEventBus.{AggregatedStatEvent, Stat}
import services.agent.{AgentStatistic, StatDateTime, StatPeriod, Statistic}
import services.calltracking.DeviceConferenceAction.*
import services.config.AgentDirectoryEntry
import services.config.ConfigDispatcher.AgentDirectory
import services.request.*
import services.user.AdminRight
import xivo.ami.AmiBusConnector.{AgentListenStarted, AgentListenStopped}
import xivo.events.AgentState.AgentReady
import xivo.events.*
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.*
import xivo.services.XivoDirectory
import xivo.services.XivoDirectory.*
import xivo.websocket.WSMsgType.WSMsgType
import xivo.xucami.models.QueueCall
import xuctest.JsonMatchers

import scala.jdk.CollectionConverters.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import services.video.model.{
  MeetingRoomInvite,
  MeetingRoomInviteAckReply,
  MeetingRoomInviteResponse,
  VideoInviteAck,
  VideoInviteResponse
}

class WebSocketEventSpec extends AnyWordSpec with Matchers with JsonMatchers {
  import xivo.models.LineHelper.makeLine
  "Websocket event" should {

    "return an evtSheet message when asked for" in {

      val sheet = new Sheet()
      import XivoObject._
      sheet.channel = "testChannel"
      sheet.compressed = true
      sheet.serial = "12333"
      sheet.timenow = new Date()

      val evtSheet = WebSocketEvent.createEvent(sheet)
      (evtSheet \ "msgType").as[String] should be("Sheet")

      messageTypeShouldBe(evtSheet, WSMsgType.Sheet)
      ctiMessage(evtSheet) should be(Json.toJson(sheet).toString)
    }

    "return a json agent message" in {
      val agent = Agent(32L, "Noe", "Rasp", "3457", "default", 45, 30)

      val event = WebSocketEvent.createEvent(agent)

      messageTypeShouldBe(event, WSMsgType.AgentConfig)
      ctiMessage(event) should be(
        """{"id":32,"firstName":"Noe","lastName":"Rasp","number":"3457","context":"default","groupId":45,"userId":30}"""
      )
    }

    "return a json agent directory message" in {
      val agent = Agent(32L, "Noe", "Rasp", "3457", "default", 45, 30)
      val agentState =
        AgentReady(3L, new DateTime(), "1001", List(), agentNb = "2000")
      val agDir = AgentDirectory(List(AgentDirectoryEntry(agent, agentState)))

      val event = WebSocketEvent.createEvent(agDir)

      messageTypeShouldBe(event, WSMsgType.AgentDirectory)
      ctiMessage(event) should matchJson(
        "{\"directory\":[{\"agent\":{\"id\":32,\"firstName\":\"Noe\",\"lastName\":\"Rasp\",\"number\":\"3457\",\"context\":\"default\",\"groupId\":45,\"userId\":30},\"agentState\":{\"name\":\"AgentReady\",\"agentId\":3,\"phoneNb\":\"1001\",\"since\":0,\"queues\":[],\"cause\":\"\"}}]}"
      )
    }

    "return a json queue member message" in {
      val qm = AgentQueueMember(23L, 45, 2)

      val event = WebSocketEvent.createEvent(qm)

      messageTypeShouldBe(event, WSMsgType.QueueMember)
      ctiMessage(event) should be(
        "{\"agentId\":23,\"queueId\":45,\"penalty\":2}"
      )

    }
    "return an agent state event" in {
      val agentState =
        AgentReady(3L, new DateTime(), "1001", List(2, 3), agentNb = "2000")

      val event = WebSocketEvent.createEvent(agentState)

      messageTypeShouldBe(event, WSMsgType.AgentStateEvent)
      ctiMessage(event) should matchJson(
        "{\"name\":\"AgentReady\",\"agentId\":3,\"phoneNb\":\"1001\",\"since\":0,\"queues\":[2,3],\"cause\":\"\"}"
      )

    }

    "return a link state event" in {
      val ls = LinkStatusUpdate(LinkState.down)

      val event = WebSocketEvent.createEvent(ls)

      messageTypeShouldBe(event, WSMsgType.LinkStatusUpdate)
      ctiMessage(event) should be("{\"status\":\"down\"}")

    }

    "return a directory result event" in {
      val rds = new RichResult(List("name"))

      val event = WebSocketEvent.createEvent(rds, Research)

      messageTypeShouldBe(event, WSMsgType.DirectoryResult)

      ctiMessage(event) should be("{\"headers\":[\"name\"],\"entries\":[]}")
    }
    "return a list of agents event" in {
      val agent = Agent(32L, "Noe", "Rasp", "3457", "default", 45, 30)

      val event = WebSocketEvent.createAgentListEvent(List(agent))

      messageTypeShouldBe(event, WSMsgType.AgentList)

      ctiMessage(event) should be(
        "[{\"id\":32,\"firstName\":\"Noe\",\"lastName\":\"Rasp\",\"number\":\"3457\",\"context\":\"default\",\"groupId\":45,\"userId\":30}]"
      )
    }
    "return a list of agent groups" in {
      val agentGroup = AgentGroup(Some(3), "boats", Some("Boats"))

      val event = WebSocketEvent.createAgentGroupsEvent(List(agentGroup))

      messageTypeShouldBe(event, WSMsgType.AgentGroupList)

      ctiMessage(event) should be(
        "[{\"id\":3,\"name\":\"boats\",\"displayName\":\"Boats\"}]"
      )
    }
    "return a list of agent queue members " in {
      val qm = AgentQueueMember(23L, 45, 2)

      val event = WebSocketEvent.createAgentQueueMembersEvent(List(qm))

      messageTypeShouldBe(event, WSMsgType.QueueMemberList)

      ctiMessage(event) should be(
        "[{\"agentId\":23,\"queueId\":45,\"penalty\":2}]"
      )
    }

    "return a statistic event" in {
      import xivo.models.XivoObject.ObjectType._

      val stat = AggregatedStatEvent(
        ObjectDefinition(Queue, Some(32)),
        List(Stat("totalnumber", 32))
      )

      val event = WebSocketEvent.createEvent(stat)

      messageTypeShouldBe(event, WSMsgType.QueueStatistics)
      ctiMessage(event) should be(
        "{\"queueId\":32,\"counters\":[{\"statName\":\"totalnumber\",\"value\":32}]}"
      )
    }

    "return a logged on event" in {
      val event = WebSocketEvent.createLoggedOnEvent()

      messageTypeShouldBe(event, WSMsgType.LoggedOn)
    }

    "return queue config feature event" in {
      val queue = QueueConfigUpdate(
        32,
        "q1",
        "Queue One",
        "1010",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      val event = WebSocketEvent.createEvent(queue)

      messageTypeShouldBe(event, WSMsgType.QueueConfig)

      ctiMessage(event) should be(
        "{\"id\":32,\"name\":\"q1\",\"displayName\":\"Queue One\",\"number\":\"1010\",\"context\":\"default\",\"data_quality\":1,\"retries\":1,\"ring\":1,\"transfer_user\":1,\"transfer_call\":1,\"write_caller\":1,\"write_calling\":1,\"url\":\"url\",\"announceoverride\":\"\",\"timeout\":1,\"preprocess_subroutine\":\"preprocess_subroutine\",\"announce_holdtime\":1,\"waittime\":1,\"waitratio\":1,\"ignore_forward\":1,\"recording_mode\":\"recorded\",\"recording_activated\":1}"
      )
    }

    "return a list of queue feature events" in {
      val queue = QueueConfigUpdate(
        32,
        "q1",
        "Queue One",
        "3010",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      val event = WebSocketEvent.createQueuesEvent(List(queue))

      messageTypeShouldBe(event, WSMsgType.QueueList)

      ctiMessage(event) should be(
        "[{\"id\":32,\"name\":\"q1\",\"displayName\":\"Queue One\",\"number\":\"3010\",\"context\":\"default\",\"data_quality\":1,\"retries\":1,\"ring\":1,\"transfer_user\":1,\"transfer_call\":1,\"write_caller\":1,\"write_calling\":1,\"url\":\"url\",\"announceoverride\":\"\",\"timeout\":1,\"preprocess_subroutine\":\"preprocess_subroutine\",\"announce_holdtime\":1,\"waittime\":1,\"waitratio\":1,\"ignore_forward\":1,\"recording_mode\":\"recorded\",\"recording_activated\":1}]"
      )
    }

    "return queue statistic event" in {

      val qstat = new QueueStatistics()
      qstat.setQueueId(45)
      qstat.addCounter(new Counter(StatName.WaitingCalls, 34))

      val event = WebSocketEvent.createEvent(qstat)

      messageTypeShouldBe(event, WSMsgType.QueueStatistics)

      ctiMessage(event) should be(
        "{\"queueId\":45,\"counters\":[{\"statName\":\"WaitingCalls\",\"value\":34}]}"
      )

    }
    "return phone hint status" in {
      val ph: PhoneHintStatus = PhoneHintStatus.getHintStatus(0)

      val event = WebSocketEvent.createEvent(ph)

      messageTypeShouldBe(event, WSMsgType.PhoneStatusUpdate)

      ctiMessage(event) should be("{\"status\":\"AVAILABLE\"}")
    }

    "return an error" in {

      val event = WebSocketEvent.createError(
        WSMsgType.Error,
        "Unable to dial this number",
        Map("Extenstion" -> "3215")
      )

      messageTypeShouldBe(event, WSMsgType.Error)

      ctiMessage(event) should be(
        "{\"Error\":\"Unable to dial this number\",\"Extenstion\":\"3215\"}"
      )

    }

    "return a user status update" in {
      val userStatusUpdate = new UserStatusUpdate()
      userStatusUpdate.setUserId(32)
      userStatusUpdate.setStatus("donotdisturb")

      val event = WebSocketEvent.createEvent(userStatusUpdate)

      messageTypeShouldBe(event, WSMsgType.UserStatusUpdate)

      ctiMessage(event) should be("{\"status\":\"donotdisturb\"}")
    }

    "return an agent error on agent request error invalid extension" in {
      val agentRequestError =
        new IpbxCommandResponse("agent_login_invalid_exten", new Date)

      val event = WebSocketEvent.createEvent(agentRequestError)

      messageTypeShouldBe(event, WSMsgType.AgentError)
      ctiMessage(event) should be("{\"Error\":\"PhoneNumberUnknown\"}")

    }
    "return an agent error on agent request error agent_login_exten_in_use" in {
      val agentRequestError =
        new IpbxCommandResponse("agent_login_exten_in_use", new Date)

      val event = WebSocketEvent.createEvent(agentRequestError)

      messageTypeShouldBe(event, WSMsgType.AgentError)
      ctiMessage(event) should be("{\"Error\":\"PhoneNumberAlreadyInUse\"}")

    }
    "return an agent error on agent request error unkown message" in {
      val agentRequestError = new IpbxCommandResponse("agent_error", new Date)

      val event = WebSocketEvent.createEvent(agentRequestError)

      messageTypeShouldBe(event, WSMsgType.AgentError)
      ctiMessage(event) should be("{\"Error\":\"Unknown:agent_error\"}")

    }

    "return an line config" in {
      val lineCfg = LineConfig(
        "1",
        "2005",
        Some(
          makeLine(
            1,
            "default",
            "SIP",
            "name",
            None,
            Some("passmot"),
            "xivoAddr"
          )
        )
      )

      val event = WebSocketEvent.createEvent(lineCfg)

      messageTypeShouldBe(event, WSMsgType.LineConfig)

      ctiMessage(event) should matchJson(
        "{\"id\":\"1\",\"name\":\"name\",\"xivoIp\":\"xivoAddr\",\"hasDevice\":false,\"isUa\":false,\"webRtc\":false,\"vendor\":null,\"number\":\"2005\",\"password\":\"passmot\", \"mobileApp\":false}"
      )
    }

    "return a conference list" in {
      val startTime = new Date(24560321)
      val meetme = new Meetme(
        "4000",
        "test",
        true,
        startTime,
        List(new MeetmeMember(1, startTime, false, "John Doe", "2000")).asJava
      )

      val event = WebSocketEvent.createConferenceListEvent(List(meetme))

      messageTypeShouldBe(event, WSMsgType.ConferenceList)
      ctiMessage(event) should be(
        "[{\"number\":\"4000\",\"name\":\"test\",\"pinRequired\":true,\"startTime\":24560321,\"members\":[" +
          "{\"joinOrder\":1,\"joinTime\":24560321,\"muted\":false,\"name\":\"John Doe\",\"number\":\"2000\"}]}]"
      )
    }

    "return a conference error" in {
      val errors = Map(
        NotAMember          -> "NotAMember",
        NotOrganizer        -> "NotOrganizer",
        CannotKickOrganizer -> "CannotKickOrganizer",
        ParticipantNotFound -> "ParticipantNotFound"
      )

      errors.foreach { case (error, errorString) =>
        val event =
          WebSocketEvent.createEvent(WsConferenceCommandError(error))
        messageTypeShouldBe(event, WSMsgType.ConferenceCommandError)
        ctiMessage(event) should be(s"""{"error":"$errorString"}""")
      }
    }

    "return a deaf conference participant event" in {

      val msg = WsConferenceParticipantEvent(
        WsConferenceParticipantEventUpdate,
        uniqueId = "123456.789",
        phoneNumber = "2001",
        conferenceNumber = "4000",
        index = 2,
        callerIdName = "Jason Bourne",
        callerIdNum = "2001",
        since = 9,
        isTalking = false,
        role = WsConferenceParticipantOrganizerRole,
        isMuted = false,
        isMe = true,
        username = Some("userone"),
        isDeaf = true
      )

      val event = WebSocketEvent.createEvent(msg)

      messageTypeShouldBe(event, WSMsgType.ConferenceParticipantEvent)

      ctiMessage(event) should be(
        "{\"eventType\":\"Update\",\"uniqueId\":\"123456.789\",\"phoneNumber\":\"2001\",\"conferenceNumber\":\"4000\",\"index\":2,\"callerIdName\":\"Jason Bourne\",\"callerIdNum\":\"2001\",\"since\":9,\"isTalking\":false,\"role\":\"Organizer\",\"isMuted\":false,\"isMe\":true,\"username\":\"userone\",\"isDeaf\":true}"
      )
    }

    "return a user config update " in {

      val userConfigUpdate = UserConfigUpdate(
        0,
        "John",
        "Doe",
        "John Doe",
        0,
        false,
        false,
        "",
        false,
        "",
        false,
        "",
        "",
        List(),
        0,
        false
      )

      val event = WebSocketEvent.createEvent(userConfigUpdate)

      messageTypeShouldBe(event, WSMsgType.UserConfigUpdate)

      ctiMessage(event) should be(
        "{\"userId\":0,\"firstName\":\"John\",\"lastName\":\"Doe\",\"fullName\":\"John Doe\",\"agentId\":0," +
          "\"dndEnabled\":false,\"naFwdEnabled\":false,\"naFwdDestination\":\"\",\"uncFwdEnabled\":false," +
          "\"uncFwdDestination\":\"\",\"busyFwdEnabled\":false,\"busyFwdDestination\":\"\",\"mobileNumber\":\"\"," +
          "\"lineIds\":[],\"voiceMailId\":0,\"voiceMailEnabled\":false}"
      )

    }

    "return an agent listen started" in {
      val agls = AgentListenStarted("8741", Some(45L))

      val event = WebSocketEvent.createEvent(agls)

      messageTypeShouldBe(event, WSMsgType.AgentListen)

      ctiMessage(event) should be(
        "{\"started\":true,\"phoneNumber\":\"8741\",\"agentId\":45}"
      )

    }

    "return an agent listen stopped" in {
      val aglstopped = AgentListenStopped("4561", Some(27L))

      val event = WebSocketEvent.createEvent(aglstopped)

      messageTypeShouldBe(event, WSMsgType.AgentListen)

      ctiMessage(event) should be(
        "{\"started\":false,\"phoneNumber\":\"4561\",\"agentId\":27}"
      )
    }

    "return a period agent statistic" in {
      val agstat =
        AgentStatistic(34L, List(Statistic("LoginTime", StatPeriod(34))))

      val event = WebSocketEvent.createEvent(agstat)

      messageTypeShouldBe(event, WSMsgType.AgentStatistics)

      ctiMessage(event) should be(
        "{\"id\":34,\"statistics\":[{\"name\":\"LoginTime\",\"value\":34}]}"
      )
    }
    "return a date agent statistic" in {
      val agstat = AgentStatistic(
        34L,
        List(
          Statistic(
            "LoginTime",
            StatDateTime(new DateTime(2005, 3, 26, 12, 20, 34, 58))
          )
        )
      )

      val event = WebSocketEvent.createEvent(agstat)

      messageTypeShouldBe(event, WSMsgType.AgentStatistics)

      ctiMessage(event) should be(
        "{\"id\":34,\"statistics\":[{\"name\":\"LoginTime\",\"value\":\"2005-03-26T12:20:34.058+01:00\"}]}"
      )
    }
    "return QueueCalls" in {
      val queueCalls = QueueCallList(
        5,
        List(
          QueueCall(
            1,
            Some("foo"),
            "333456789",
            new DateTime(2005, 3, 26, 12, 20, 34, 58),
            "SIP/abcd-000001",
            "main"
          ),
          QueueCall(
            2,
            Some("bar"),
            "4745468793",
            new DateTime(2005, 3, 26, 12, 20, 35, 40),
            "SIP/abcd-000002",
            "main"
          )
        )
      )

      val event = WebSocketEvent.createEvent(queueCalls)

      messageTypeShouldBe(event, WSMsgType.QueueCalls)
      ctiMessage(event) should be(
        """{"queueId":5,"calls":[{"position":1,"name":"foo","number":"333456789","queueTime":"2005-03-26T12:20:34.058+01:00","channel":"SIP/abcd-000001","mdsName":"main"},""" +
          """{"position":2,"name":"bar","number":"4745468793","queueTime":"2005-03-26T12:20:35.040+01:00","channel":"SIP/abcd-000002","mdsName":"main"}]}"""
      )
    }
    "return favorites" in {
      val event =
        WebSocketEvent.createEvent(new RichResult(List("name")), Favorite)

      messageTypeShouldBe(event, WSMsgType.Favorites)
      ctiMessage(event) should be("""{"headers":["name"],"entries":[]}""")
    }
    "return favorite added" in {
      val event = WebSocketEvent.createEvent(
        FavoriteUpdated(XivoDirectory.Action.Added, "12", "xivou")
      )
      messageTypeShouldBe(event, WSMsgType.FavoriteUpdated)
      ctiMessage(event) should be(
        """{"action":"Added","contact_id":"12","source":"xivou"}"""
      )
    }
    "return favorite added fail" in {
      val event = WebSocketEvent.createEvent(
        FavoriteUpdated(XivoDirectory.Action.AddFail, "12", "xivou")
      )
      messageTypeShouldBe(event, WSMsgType.FavoriteUpdated)
      ctiMessage(event) should be(
        """{"action":"AddFail","contact_id":"12","source":"xivou"}"""
      )
    }

    "return callbacklists" in {
      val uuid1     = UUID.randomUUID()
      val uuid2     = UUID.randomUUID()
      val listUuid1 = UUID.randomUUID()
      val listUuid2 = UUID.randomUUID()
      val cb1 = CallbackRequest(
        Some(uuid1),
        listUuid1,
        Some("1000"),
        Some("7200"),
        Some("John"),
        Some("Doe"),
        Some("Company"),
        Some("description")
      )
      val cb2 = CallbackRequest(
        Some(uuid2),
        listUuid1,
        None,
        Some("7200"),
        None,
        Some("Doe"),
        None,
        None
      )
      val lists = List(
        CallbackList(Some(listUuid1), "first list", 1, List(cb1, cb2)),
        CallbackList(Some(listUuid2), "second list", 1, List())
      )

      val event = WebSocketEvent.createEvent(CallbackLists(lists))

      messageTypeShouldBe(event, WSMsgType.CallbackLists)
      ctiMessage(event) should be(
        s"""[{"uuid":"$listUuid1","name":"first list","queueId":1,"callbacks":[${CallbackRequest.writes
            .writes(cb1)},${CallbackRequest.writes.writes(cb2)}]},""" +
          s"""{"uuid":"$listUuid2","name":"second list","queueId":1,"callbacks":[]}]"""
      )
    }

    "return preferred periods" in {
      val uuid1 = UUID.randomUUID()
      val uuid2 = UUID.randomUUID()
      val p1 = PreferredCallbackPeriod(
        Some(uuid1),
        "morning",
        new LocalTime(8, 0, 0),
        new LocalTime(11, 0, 0),
        true
      )

      val p2 = PreferredCallbackPeriod(
        Some(uuid2),
        "afternoon",
        new LocalTime(14, 0, 0),
        new LocalTime(18, 0, 0),
        false
      )

      val event =
        WebSocketEvent.createEvent(PreferredCallbackPeriodList(List(p1, p2)))

      messageTypeShouldBe(event, WSMsgType.PreferredCallbackPeriodList)
      ctiMessage(event) should be(
        s"""[{"uuid":"$uuid1","name":"morning","periodStart":"08:00:00","periodEnd":"11:00:00","default":true},{"uuid":"$uuid2","name":"afternoon","periodStart":"14:00:00","periodEnd":"18:00:00","default":false}]"""
      )
    }

    "return CallbackTaken" in {
      val uuid    = UUID.randomUUID()
      val cbTaken = CallbackTaken(uuid, 12)

      val event = WebSocketEvent.createEvent(cbTaken)

      messageTypeShouldBe(event, WSMsgType.CallbackTaken)
      ctiMessage(event) should be(s"""{"uuid":"$uuid","agentId":12}""")
    }

    "return CallbackReleased" in {
      val uuid       = UUID.randomUUID()
      val cbReleased = CallbackReleased(uuid)

      val event = WebSocketEvent.createEvent(cbReleased)

      messageTypeShouldBe(event, WSMsgType.CallbackReleased)
      ctiMessage(event) should be(s"""{"uuid":"$uuid"}""")
    }

    "return CallbackStarted" in {
      val requestUuid = UUID.randomUUID()
      val ticketUuid  = UUID.randomUUID()
      val cbStarted   = CallbackStarted(requestUuid, ticketUuid)

      val event = WebSocketEvent.createEvent(cbStarted)
      messageTypeShouldBe(event, WSMsgType.CallbackStarted)
      ctiMessage(event) should be(
        s"""{"requestUuid":"$requestUuid","ticketUuid":"$ticketUuid"}"""
      )
    }

    "return CallbackClotured" in {
      val uuid       = UUID.randomUUID()
      val cbClotured = CallbackClotured(uuid)

      val event = WebSocketEvent.createEvent(cbClotured)

      messageTypeShouldBe(event, WSMsgType.CallbackClotured)
      ctiMessage(event) should be(s"""{"uuid":"$uuid"}""")
    }

    "return CallbackRequestUpdated" in {
      val cbUuid   = UUID.randomUUID()
      val listUuid = UUID.randomUUID()
      val cb = CallbackRequest(
        Some(cbUuid),
        listUuid,
        Some("1000"),
        None,
        Some("James"),
        Some("Bond"),
        None,
        None,
        dueDate = LocalDate.parse("2016-08-11")
      )
      val cbu = CallbackRequestUpdated(cb)

      val event = WebSocketEvent.createEvent(cbu)

      messageTypeShouldBe(event, WSMsgType.CallbackRequestUpdated)
      ctiMessage(event) should be(
        s"""{"request":{"uuid":"$cbUuid","listUuid":"$listUuid","phoneNumber":"1000","mobilePhoneNumber":null,"firstName":"James","lastName":"Bond","company":null,"description":null,"agentId":null,"queueId":null,"clotured":false,"preferredPeriod":null,"dueDate":"2016-08-11","voiceMessageRef":null}}"""
      )
    }

    "return PhoneEvent" in {
      val phoneEvent = PhoneEvent(
        PhoneEventType.EventRinging,
        "1500",
        "0664486754",
        "Dee Dee Bridgewater",
        "1230045.6",
        "1230045.8",
        userData = Map("USR_DATA1" -> "dom")
      )

      val event = WebSocketEvent.createEvent(phoneEvent)

      messageTypeShouldBe(event, WSMsgType.PhoneEvent)

      ctiMessage(event) should be(
        s"""{"eventType":"EventRinging","DN":"1500","otherDN":"0664486754",
           |"otherDName":"Dee Dee Bridgewater","linkedId":"1230045.6","uniqueId":"1230045.8",
           |"userData":{"USR_DATA1":"dom"},"callDirection":"DirectionUnknown"}""".stripMargin
          .replaceAll("\n", "")
      )
    }

    "return CurrentCallsPhoneEvents" in {
      val ccpe = CurrentCallsPhoneEvents(
        "1500",
        List(
          PhoneEvent(
            PhoneEventType.EventRinging,
            "1500",
            "0664486754",
            "Dee Dee Bridgewater",
            "1230045.6",
            "1230045.8",
            userData = Map("USR_DATA1" -> "dom"),
            callDirection = CallDirection.Outgoing
          ),
          PhoneEvent(
            PhoneEventType.EventRinging,
            "1500",
            "0664486754",
            "Dee Dee Bridgewater",
            "1230045.6",
            "1230045.8",
            userData = Map("USR_DATA1" -> "dom")
          )
        )
      )

      val event = WebSocketEvent.createEvent(ccpe)

      messageTypeShouldBe(event, WSMsgType.CurrentCallsPhoneEvents)

      ctiMessage(event) should be(s"""{"events":[
           |{"eventType":"EventRinging","DN":"1500","otherDN":"0664486754","otherDName":"Dee Dee Bridgewater",
             |"linkedId":"1230045.6","uniqueId":"1230045.8","userData":{"USR_DATA1":"dom"},"callDirection":"Outgoing"},
           |{"eventType":"EventRinging","DN":"1500","otherDN":"0664486754","otherDName":"Dee Dee Bridgewater",
             |"linkedId":"1230045.6","uniqueId":"1230045.8","userData":{"USR_DATA1":"dom"},"callDirection":"DirectionUnknown"}
           |]}""".stripMargin.replaceAll("\n", ""))
    }

    "return a UserQueueDefaultmembership" in {
      val uqm = UserQueueDefaultMembership(
        123,
        List(QueueMembership(1, 2), QueueMembership(5, 2))
      )
      val event = WebSocketEvent.createEvent(uqm)
      messageTypeShouldBe(event, WSMsgType.UserQueueDefaultMembership)
      ctiMessage(event) should be(
        """{"userId":123,"membership":[{"queueId":1,"penalty":2},{"queueId":5,"penalty":2}]}"""
      )

    }

    "return a PhoneHintStatusEvent" in {
      val o     = PhoneHintStatusEvent("1000", PhoneHintStatus.getHintStatus(0))
      val event = WebSocketEvent.createEvent(o)
      messageTypeShouldBe(event, WSMsgType.PhoneHintStatusEvent)
      ctiMessage(event) should be("""{"number":"1000","status":0}""")

    }

    "return a RightProfile" in {
      val event = WebSocketEvent.createEvent(AdminRight())
      messageTypeShouldBe(event, WSMsgType.RightProfile)
      ctiMessage(event) should be(
        """{"profile":"Admin","rights":["recording"]}"""
      )
    }

    "return a WebRTC command" in {
      val event = WebSocketEvent.createWebRTCCommand(WebRTCAnswerCmd(None))
      messageTypeShouldBe(event, WSMsgType.WebRTCCmd)
      ctiMessage(event) should be("""{"command":"Answer"}""")
    }

    "return existing user display name" in {
      val event =
        WebSocketEvent.createEvent(UserDisplayName("jbond", "James Bond"))
      messageTypeShouldBe(event, WSMsgType.UserDisplayName)
      ctiMessage(event) should be(
        """{"userName":"jbond","displayName":"James Bond"}"""
      )
    }

    "return non-existing user display name" in {
      val event = WebSocketEvent.createEvent(UserDisplayName("jbond", "jbond"))
      messageTypeShouldBe(event, WSMsgType.UserDisplayName)
      ctiMessage(event) should be(
        """{"userName":"jbond","displayName":"jbond"}"""
      )
    }

    "return existing IceConfig" in {
      val event = WebSocketEvent.createEvent(
        IceConfig(
          Some(StunConfig(List("stun:host:3478"))),
          Some(TurnConfig(List("turn:host:3478"), "2002:84600", "pwd", 84600))
        )
      )
      messageTypeShouldBe(event, WSMsgType.IceConfig)
      ctiMessage(event) should be(
        """{"stunConfig":{"urls":["stun:host:3478"]},"turnConfig":{"urls":["turn:host:3478"],"username":"2002:84600","credential":"pwd","ttl":84600}}"""
      )
    }

    "return non-existing IceConfig" in {
      val event = WebSocketEvent.createEvent(IceConfig(None, None))
      messageTypeShouldBe(event, WSMsgType.IceConfig)
      ctiMessage(event) should be("""{}""")
    }

    "return meeting room invitation" in {
      val event = WebSocketEvent.createEvent(
        MeetingRoomInvite(42, "my.token", "bwillis", "jbond", "Bruce Willis")
      )
      messageTypeShouldBe(event, WSMsgType.MeetingRoomInvite)
      ctiMessage(event) should be(
        """{"requestId":42,"token":"my.token","username":"jbond","displayName":"Bruce Willis"}"""
      )
    }

    "return meeting room ack" in {
      val event = WebSocketEvent.createEvent(
        MeetingRoomInviteAckReply(
          42,
          "jbond",
          "Bruce Willis",
          VideoInviteAck.ACK
        )
      )
      messageTypeShouldBe(event, WSMsgType.MeetingRoomInviteAck)
      ctiMessage(event) should be(
        """{"requestId":42,"displayName":"Bruce Willis","ackType":"ACK"}"""
      )
    }

    "return meeting room nack" in {
      val event = WebSocketEvent.createEvent(
        MeetingRoomInviteAckReply(
          42,
          "jbond",
          "Bruce Willis",
          VideoInviteAck.NACK
        )
      )
      messageTypeShouldBe(event, WSMsgType.MeetingRoomInviteAck)
      ctiMessage(event) should be(
        """{"requestId":42,"displayName":"Bruce Willis","ackType":"NACK"}"""
      )
    }

    "return meeting room invite accept" in {
      val event = WebSocketEvent.createEvent(
        MeetingRoomInviteResponse(
          42,
          "jbond",
          "Bruce Willis",
          VideoInviteResponse.ACCEPT
        )
      )
      messageTypeShouldBe(event, WSMsgType.MeetingRoomInviteResponse)
      ctiMessage(event) should be(
        """{"requestId":42,"displayName":"Bruce Willis","responseType":"Accept"}"""
      )
    }

    "return meeting room invite reject" in {
      val event = WebSocketEvent.createEvent(
        MeetingRoomInviteResponse(
          42,
          "jbond",
          "Bruce Willis",
          VideoInviteResponse.REJECT
        )
      )
      messageTypeShouldBe(event, WSMsgType.MeetingRoomInviteResponse)
      ctiMessage(event) should be(
        """{"requestId":42,"displayName":"Bruce Willis","responseType":"Reject"}"""
      )
    }

    "return UserPreference as a dictionnary" in {
      val event = WebSocketEvent.createUserPreferenceEvent(
        List(
          UserPreference(4, "PREFERRED_DEVICE", "phone", "String"),
          UserPreference(12, "PHONE_INFO", "True", "Boolean")
        )
      )
      messageTypeShouldBe(event, WSMsgType.UserPreference)
      ctiMessage(event) should be(
        """{"PREFERRED_DEVICE":{"value":"phone","value_type":"String"},"PHONE_INFO":{"value":"True","value_type":"Boolean"}}"""
      )
    }
  }
  private def messageTypeShouldBe(event: JsValue, msgType: WSMsgType) =
    (event \ WebSocketEvent.MsgType).as[String] should be(msgType.toString)

  private def ctiMessage(event: JsValue) =
    (event \ WebSocketEvent.CtiMessage).get.toString

}
