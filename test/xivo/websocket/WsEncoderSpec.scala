package xivo.websocket

import java.util.UUID

import models.{Queue => _, _}
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, LocalTime, Period}
import org.scalatest.BeforeAndAfterEach
import org.xivo.cti.model.PhoneHintStatus
import services.XucStatsEventBus.{AggregatedStatEvent, Stat}
import services.agent.{AgentStatistic, StatDateTime, Statistic}
import services.config.ConfigDispatcher._
import services.request._
import xivo.events.AgentState.AgentReady
import xivo.events.PhoneHintStatusEvent
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType._
import xivo.models._
import xivo.websocket.WsBus.WsContent
import xivo.xucami.models.QueueCall
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class WsEncoderSpec extends AnyWordSpec with Matchers with BeforeAndAfterEach {
  import xivo.models.LineHelper.makeLine

  var encoder: WsEncoder = null

  override def beforeEach(): Unit = {
    encoder = new WsEncoder
  }

  "Ws encoder" should {
    "do not encode unknow message" in {
      encoder.encode("unknown message") should be(None)
    }
    "encode agent state event" in {
      val agentState =
        AgentReady(32, new DateTime(), "4350", List(), agentNb = "2000")
      val result = Some(WsContent(WebSocketEvent.createEvent(agentState)))

      val encoded = encoder.encode(agentState)

      encoded should be(result)

    }
    "encode queue list event" in {
      val qConfig = QueueConfigUpdate(
        1,
        "q1",
        "Queue One",
        "111",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )
      val queues = List(qConfig)

      val encoded = encoder.encode(QueueList(queues))

      encoded should be(
        Some(WsContent(WebSocketEvent.createQueuesEvent(queues)))
      )

    }
    "encode agent list " in {
      val agents = List(Agent(1, "John", "Doe", "2250", "default"))

      val encoded = encoder.encode(AgentList(agents))

      encoded should be(
        Some(WsContent(WebSocketEvent.createAgentListEvent(agents)))
      )
    }
    "encode agent group list" in {
      val agentGroups = List(AgentGroup(Some(1), "yellow"))

      val encoded = encoder.encode(AgentGroupList(agentGroups))

      encoded should be(
        Some(WsContent(WebSocketEvent.createAgentGroupsEvent(agentGroups)))
      )

    }
    "encode queue member list" in {
      val agentQueueMembers = List(AgentQueueMember(2, 3, 5))

      val encoded = encoder.encode(AgentQueueMemberList(agentQueueMembers))

      encoded should be(
        Some(
          WsContent(
            WebSocketEvent.createAgentQueueMembersEvent(agentQueueMembers)
          )
        )
      )
    }

    "encode agent" in {
      val agent = Agent(1, "John", "Doe", "2250", "default")

      val encoded = encoder.encode(agent)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(agent))))
    }
    "encode agent queue member" in {
      val queueM = AgentQueueMember(1, 2, 8)

      val encoded = encoder.encode(queueM)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(queueM))))
    }
    "encode queue feature" in {
      val queueConfig = QueueConfigUpdate(
        32,
        "q1",
        "Queue One",
        "1010",
        Some("default"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "",
        Some(1),
        Some("preprocess_subroutine"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      val encoded = encoder.encode(queueConfig)

      encoded should be(
        Some(WsContent(WebSocketEvent.createEvent(queueConfig)))
      )
    }
    "encode agent directory" in {
      val ad = AgentDirectory(List())

      val encoded = encoder.encode(ad)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(ad))))

    }
    "encode agregated stat event" in {
      val aggregatedStatEvent = AggregatedStatEvent(
        ObjectDefinition(Queue, Some(78)),
        List[Stat](Stat("totalnumber", 32))
      )

      val encoded = encoder.encode(aggregatedStatEvent)

      encoded should be(
        Some(WsContent(WebSocketEvent.createEvent(aggregatedStatEvent)))
      )
    }
    "encode agentstatistic with date time event" in {
      val agentstat = AgentStatistic(
        32,
        List(Statistic("loginTime", StatDateTime(new DateTime())))
      )

      val encoded = encoder.encode(agentstat)

      encoded should be(Some(WsContent(WebSocketEvent.createEvent(agentstat))))
    }
    "encode queuecalls" in {
      val queueCalls = QueueCallList(
        4,
        List(
          QueueCall(
            1,
            Some("foo"),
            "33345678",
            new DateTime,
            "SIP/abcd-000001",
            "main"
          ),
          QueueCall(
            2,
            Some("bar"),
            "4478965",
            new DateTime,
            "SIP/abcd-0000012",
            "main"
          )
        )
      )

      encoder.encode(queueCalls) should be(
        Some(WsContent(WebSocketEvent.createEvent(queueCalls)))
      )
    }
    "encode CallHistory" in {
      val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
      val callHistory = CallHistory(
        List(
          CallDetail(
            format.parseDateTime("2014-01-01 08:00:00"),
            Some(new Period(0, 12, 47, 0)),
            "3345678",
            Some("1235748"),
            CallStatus.Ongoing
          )
        )
      )

      encoder.encode(callHistory) should be(
        Some(WsContent(WebSocketEvent.createEvent(callHistory)))
      )
    }

    "encode CallbackLists" in {
      val list = CallbackList(Some(UUID.randomUUID()), "Test", 1, List())
      encoder.encode(CallbackLists(List(list))) should be(
        Some(WsContent(WebSocketEvent.createEvent(CallbackLists(List(list)))))
      )
    }

    "encode List[CallbackRequest]" in {
      val uuid1    = UUID.randomUUID()
      val uuid2    = UUID.randomUUID()
      val listUuid = UUID.randomUUID()

      val expected = FindCallbackResponseWithId(
        1,
        FindCallbackResponse(
          2,
          List(
            CallbackRequest(
              Some(uuid1),
              listUuid,
              Some("1000"),
              None,
              None,
              None,
              Some("The company"),
              None,
              queueId = Some(23)
            ),
            CallbackRequest(
              Some(uuid2),
              listUuid,
              Some("1000"),
              None,
              None,
              None,
              Some("The company"),
              None,
              queueId = Some(23)
            )
          )
        )
      )
      encoder.encode(expected) should be(
        Some(WsContent(WebSocketEvent.createEvent(expected)))
      )
    }

    "encode PreferredCallbackPeriodList" in {
      val uuid1 = UUID.randomUUID()
      val uuid2 = UUID.randomUUID()
      val p1 = PreferredCallbackPeriod(
        Some(uuid1),
        "morning",
        new LocalTime(8, 0, 0),
        new LocalTime(11, 0, 0),
        true
      )

      val p2 = PreferredCallbackPeriod(
        Some(uuid2),
        "afternoon",
        new LocalTime(14, 0, 0),
        new LocalTime(18, 0, 0),
        false
      )

      val list = List(p1, p2)
      encoder.encode(PreferredCallbackPeriodList(list)) should be(
        Some(
          WsContent(
            WebSocketEvent.createEvent(PreferredCallbackPeriodList(list))
          )
        )
      )
    }

    "encode CallbackTaken" in {
      val cbTaken = CallbackTaken(UUID.randomUUID(), 12)
      encoder.encode(cbTaken) should be(
        Some(WsContent(WebSocketEvent.createEvent(cbTaken)))
      )
    }

    "encode CallbackReleased" in {
      val cbReleased = CallbackReleased(UUID.randomUUID())
      encoder.encode(cbReleased) should be(
        Some(WsContent(WebSocketEvent.createEvent(cbReleased)))
      )
    }

    "encode CallbackStarted" in {
      val requestUuid = UUID.randomUUID()
      val ticketUuid  = UUID.randomUUID()
      val cbStarted   = CallbackStarted(requestUuid, ticketUuid)
      encoder.encode(cbStarted) should be(
        Some(WsContent(WebSocketEvent.createEvent(cbStarted)))
      )
    }

    "encode CallbackClotured" in {
      val cbClotured = CallbackClotured(UUID.randomUUID())
      encoder.encode(cbClotured) should be(
        Some(WsContent(WebSocketEvent.createEvent(cbClotured)))
      )
    }

    "encode CallbackRequestUpdated" in {
      val cbUuid   = UUID.randomUUID()
      val listUuid = UUID.randomUUID()
      val cb = CallbackRequest(
        Some(cbUuid),
        listUuid,
        Some("1000"),
        None,
        Some("James"),
        Some("Bond"),
        None,
        None
      )
      val cbu = CallbackRequestUpdated(cb)
      encoder.encode(cbu) should be(
        Some(WsContent(WebSocketEvent.createEvent(cbu)))
      )
    }

    "encode LineConfig" in {
      val lineCfg = LineConfig(
        "1",
        "2005",
        Some(
          makeLine(
            1,
            "default",
            "SIP",
            "name",
            None,
            Some("password"),
            "xivoIP"
          )
        )
      )
      encoder.encode(lineCfg) should be(
        Some(WsContent(WebSocketEvent.createEvent(lineCfg)))
      )
    }

    "encode UserQueueDefaultMembership" in {
      val uqm = UserQueueDefaultMembership(
        123,
        List(QueueMembership(1, 2), QueueMembership(5, 2))
      )
      encoder.encode(uqm) should be(
        Some(WsContent(WebSocketEvent.createEvent(uqm)))
      )
    }

    "encode PhoneHintStatusEvent" in {
      val o = PhoneHintStatusEvent("1000", PhoneHintStatus.getHintStatus(0))
      encoder.encode(o) should be(
        Some(WsContent(WebSocketEvent.createEvent(o)))
      )
    }

    "encode List[Customer Call History Request]" in {
      val cDetail = CustomerCallDetail(
        new DateTime(),
        Some(new Period()),
        Some(new Period()),
        Some("Bruce Willis"),
        Some("2200"),
        Some("blue"),
        Some("3000"),
        CallStatus.Answered
      )

      val expected = CustomerCallHistoryResponseWithId(
        1,
        FindCustomerCallHistoryResponse(1, List(cDetail))
      )

      encoder.encode(expected) should be(
        Some(WsContent(WebSocketEvent.createEvent(expected)))
      )
    }

    "encode UserDisplayName" in {
      val udn = UserDisplayName("jbond", "James Bond")
      encoder.encode(udn) should be(
        Some(WsContent(WebSocketEvent.createEvent(udn)))
      )
    }

    "encode IceConfig" in {
      val iceCfg = IceConfig(
        Some(StunConfig(List("stun:host:3478"))),
        Some(TurnConfig(List("turn:host:3478"), "2002:84600", "pwd", 84600))
      )
      encoder.encode(iceCfg) should be(
        Some(WsContent(WebSocketEvent.createEvent(iceCfg)))
      )
    }
  }

}
