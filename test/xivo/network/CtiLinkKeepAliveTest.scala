package xivo.network

import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import org.json.JSONObject
import pekkotest.TestKitSpec
import org.xivo.cti.MessageFactory
import xivo.network.CtiLinkKeepAlive.{StartKeepAlive, StopKeepALive}

class CtiLinkKeepAliveTest extends TestKitSpec("CtiLinkKeepAliveTest") {

  class Helper {
    def actor(): (TestActorRef[CtiLinkKeepAlive], CtiLinkKeepAlive) = {
      val a =
        TestActorRef[CtiLinkKeepAlive](CtiLinkKeepAlive.props("testUser", 600))
      (a, a.underlyingActor)
    }
  }

  "keep alive" should {
    "send a user status to ctiLink" in new Helper {
      val (ref, ctiLinkKeepAlive) = actor()
      val ctiLink: TestProbe      = TestProbe()

      ref ! StartKeepAlive("alice", ctiLink.ref)

      val msg: JSONObject = new MessageFactory().createGetUserStatus("alice")

      ref ! msg

      ctiLink.expectMsg(msg)

    }

    "be able to restart when stopped" in new Helper {
      val (ref, ctiLinkKeepAlive) = actor()
      val ctiLink: TestProbe      = TestProbe()

      ref ! StartKeepAlive("alice", ctiLink.ref)
      ref ! StopKeepALive

      val msg2: JSONObject = new MessageFactory().createGetUserStatus("bob")
      ref ! msg2

      ctiLink.expectNoMessage()

      ref ! StartKeepAlive("bob", ctiLink.ref)
      ref ! msg2

      ctiLink.expectMsg(msg2)

    }
  }

}
