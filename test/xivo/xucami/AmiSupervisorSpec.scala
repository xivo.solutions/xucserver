package xivo.xucami

import org.apache.pekko.actor._
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.mockito.ArgumentMatchers.{eq => meq, _}
import org.mockito.Mockito.{reset, timeout, times, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import services.XucAmiBus
import services.XucAmiBus.{AmiFailure, AmiType}
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models.MediaServerConfig
import xivo.network.WebServiceException
import xivo.xuc.{ConfigServerConfig, XucBaseConfig}
import xivo.xucami.AmiSupervisor.{
  DeleteMds,
  FailedMdsActor,
  LoadOrReloadMds,
  MediaServer,
  ScheduleMdsReload
}
import xuctest.IntegrationConfig

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import play.api.Application

class AmiSupervisorSpec
    extends TestKitSpec("AmiSupervisorSpec")
    with IntegrationConfig
    with GuiceOneAppPerSuite
    with MockitoSugar {

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .build()

  val mockTimeout = 100

  var lastId: Int = 0

  def nextId: Int = {
    lastId = lastId + 1
    lastId
  }
  class CrashIt(mds: MediaServerConfig, watcher: ActorRef) extends Actor {
    override def preStart(): Unit = watcher ! mds
    def receive: PartialFunction[Any, Unit] = { case any =>
      throw new IllegalArgumentException
    }
  }

  object CrashIt {
    def props(mds: MediaServerConfig, watcher: ActorRef): Props =
      Props(new CrashIt(mds, watcher))
    // def child: (XucAmiBus => Props, String) = (_ => props, "crashIt")
  }

  class FakeActor extends Actor {
    def receive: Receive = Actor.emptyBehavior
  }

  class Helper(
      _childs: List[(Props, String)] = List.empty,
      withFakeManagerConnector: Boolean = false
  ) {
    val amiBus                   = new XucAmiBus()
    val xucConfig: XucBaseConfig = app.injector.instanceOf[XucBaseConfig]
    val xucAmiConfig: XucBaseAmiConfig = new XucBaseAmiConfig {
      val ipAddress                          = "10.49.0.1"
      val port                               = 1234
      val username                           = "toto"
      val secret                             = "tutu"
      val keepaliveTimeout: FiniteDuration   = 1.second
      val amiReconnectOnMissedPings: Boolean = false
    }
    val configServer: ConfigServerRequester    = mock[ConfigServerRequester]
    val configServerConfig: ConfigServerConfig = mock[ConfigServerConfig]
    val createManagerConnectorMock: MediaServerConfig => ActorRef =
      mock[MediaServerConfig => ActorRef]
    val childReadyWatcher: TestProbe = TestProbe()
    val amiBusConnector: TestProbe   = TestProbe()
    val configDispatcher: TestProbe  = TestProbe()
    val repo: ConfigRepository       = mock[ConfigRepository]

    class FakeAmiSupervisor
        extends AmiSupervisor(
          amiBus,
          xucConfig,
          xucAmiConfig,
          configServer,
          configServerConfig,
          amiBusConnector.ref,
          configDispatcher.ref,
          repo
        ) {
      override val childs: List[(Props, String)] = _childs
      override val scheduler: Scheduler          = mock[Scheduler]

      override def createManagerConnector(mds: MediaServerConfig): ActorRef = {
        if (withFakeManagerConnector)
          context.actorOf(
            CrashIt.props(mds, childReadyWatcher.ref),
            s"${MediaServerConnectorPrefix}_${mds.name}"
          )
        else
          createManagerConnectorMock.apply(mds)
      }
    }

    def actor(
        retryDelay: FiniteDuration = 2.minutes
    ): (TestActorRef[FakeAmiSupervisor], FakeAmiSupervisor) = {
      when(configServerConfig.retryDelay).thenReturn(retryDelay)
      val sa = TestActorRef(
        new FakeAmiSupervisor(),
        "TestSupervisor" + nextId.toString
      )

      (sa, sa.underlyingActor)
    }

    def runningActor(
        mdsMap: Map[Long, MediaServer]
    ): (TestActorRef[AmiSupervisor], TestProbe) = {
      val fakeSelf = TestProbe()
      val a = TestActorRef(
        new AmiSupervisor(
          amiBus,
          xucConfig,
          xucAmiConfig,
          configServer,
          configServerConfig,
          amiBusConnector.ref,
          configDispatcher.ref,
          repo
        ) {

          override def preStart(): Unit = {}

          /*def forwardReceive: Receive = { case l: LoadOrReloadMds =>
            fakeSelf.ref ! l
          }*/

          override def receive: Receive =
            /*forwardReceive orElse*/ receiveWithMds(mdsMap)
        }
      )
      (a, fakeSelf)

    }
  }

  "AmiSupervisor" should {
    "send an ami failure message on child crash" in new Helper(
      List.empty,
      withFakeManagerConnector = true
    ) {
      val mdsMain: MediaServerConfig =
        MediaServerConfig(0, "default", "MDS Main", "10.49.0.1", true)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(List(mdsMain))
      )
      var (ref, a) = actor()
      // Wait for fake ManagerConnector to be ready
      childReadyWatcher.expectMsg(mdsMain)

      val rec: TestProbe = TestProbe()

      amiBus.subscribe(rec.ref, AmiType.AmiService)

      system.actorSelection(
        ref.path / "MdsAmiManagerConnector_default"
      ) ! "Arghhhh !!!"

      rec.expectMsg(AmiFailure(AmiSupervisor.FailureMessage, "default"))
    }

    "send an ami failure message corresponding to the mds crashing" in new Helper(
      List.empty,
      withFakeManagerConnector = true
    ) {
      val mdsMain: MediaServerConfig =
        MediaServerConfig(0, "mds0", "MDS 0 (main)", "10.49.0.10", true)
      val mds1: MediaServerConfig =
        MediaServerConfig(1, "mds1", "MDS 1", "10.49.0.11", false)
      val mds2: MediaServerConfig =
        MediaServerConfig(2, "mds2", "MDS 2", "10.49.0.12", false)
      val allMds: List[MediaServerConfig] = List(mdsMain, mds1, mds2)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(allMds)
      )
      var (ref, a) = actor()

      val rec: TestProbe = TestProbe()

      amiBus.subscribe(rec.ref, AmiType.AmiService)
      // Wait for fake ManagerConnector to be ready
      childReadyWatcher.expectMsgAllOf(mdsMain, mds1, mds2)

      system.actorSelection(
        ref.path / "MdsAmiManagerConnector_mds1"
      ) ! "Arghhhh !!!"
      rec.expectMsg(AmiFailure(AmiSupervisor.FailureMessage, "mds1"))

      system.actorSelection(
        ref.path / "MdsAmiManagerConnector_mds0"
      ) ! "Arghhhh !!!"
      rec.expectMsg(AmiFailure(AmiSupervisor.FailureMessage, "mds0"))

      system.actorSelection(
        ref.path / "MdsAmiManagerConnector_mds2"
      ) ! "Arghhhh !!!"
      rec.expectMsg(AmiFailure(AmiSupervisor.FailureMessage, "mds2"))
    }

    "create a ManagerConnector actor for each media server returned by config mgt" in new Helper() {
      val mdsMain: MediaServerConfig =
        MediaServerConfig(0, "default", "MDS Main", "10.49.0.10", true)
      val mds1: MediaServerConfig =
        MediaServerConfig(1, "mds1", "MDS 1", "10.49.0.11", false)
      val mds2: MediaServerConfig =
        MediaServerConfig(2, "mds2", "MDS 2", "10.49.0.12", false)
      val allMds: List[MediaServerConfig] = List(mdsMain, mds1, mds2)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(allMds)
      )
      when(createManagerConnectorMock.apply(any[MediaServerConfig]()))
        .thenReturn(TestProbe().ref)
      var (ref, a) = actor()

      allMds.foreach(m => {
        verify(createManagerConnectorMock, timeout(500)).apply(m)
      })
    }

    "reschedule ManagerConnector initialization when config mgt is not responding" in new Helper() {
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.failed(new WebServiceException("Something went wrong"))
      )
      when(createManagerConnectorMock.apply(any[MediaServerConfig]()))
        .thenReturn(TestProbe().ref)
      var (ref, a) = actor(1.minute)

      verify(configServer).getMediaServerConfigAll
      verify(a.scheduler).scheduleOnce(1.minute, ref, AmiSupervisor.TryLoadAll)(
        a.context.dispatcher,
        ref
      )
    }

    "retry loading mds from config mgt when timeout expires" in new Helper() {
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.failed(new WebServiceException("Something went wrong"))
      )
      when(createManagerConnectorMock.apply(any[MediaServerConfig]()))
        .thenReturn(TestProbe().ref)
      var (ref, a) = actor(1.minute)

      verify(configServer, timeout(500)).getMediaServerConfigAll
      verify(a.scheduler, timeout(500)).scheduleOnce(
        1.minute,
        ref,
        AmiSupervisor.TryLoadAll
      )(a.context.dispatcher, ref)
      reset(configServer)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.failed(new WebServiceException("Something went wrong"))
      )
      ref ! AmiSupervisor.TryLoadAll
      verify(configServer, timeout(500)).getMediaServerConfigAll
    }

    "create a ManagerConnector using the xivo ip instead of mds if only one MDS is configured" in new Helper() {
      val mds0: MediaServerConfig =
        MediaServerConfig(0, "default", "MDS Main", "10.49.0.99", true)
      val mdsMain: MediaServerConfig = MediaServerConfig(
        0,
        "default",
        "MDS Main",
        xucAmiConfig.ipAddress,
        true
      )
      val allMds: List[MediaServerConfig] = List(mds0)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(allMds)
      )
      when(createManagerConnectorMock.apply(any[MediaServerConfig]()))
        .thenReturn(TestProbe().ref)

      var (ref, a) = actor()

      verify(createManagerConnectorMock, timeout(500)).apply(mdsMain)
    }

    "create a ManagerConnector using the xivo ip instead of mds if no MDS defined" in new Helper() {
      val mdsMain: MediaServerConfig = MediaServerConfig(
        0,
        "default",
        "MDS Main",
        xucAmiConfig.ipAddress,
        true
      )
      val allMds = List.empty[MediaServerConfig]
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(allMds)
      )
      when(createManagerConnectorMock.apply(any[MediaServerConfig]()))
        .thenReturn(TestProbe().ref)

      var (ref, a) = actor()

      verify(createManagerConnectorMock, timeout(500)).apply(mdsMain)
    }

    "restart a ManagerConnector using the xivo ip instead of mds if only one MDS is configured" in new Helper() {
      val mds0: MediaServerConfig =
        MediaServerConfig(0, "default", "MDS Main", "10.49.0.99", true)
      val mdsMain: MediaServerConfig = MediaServerConfig(
        0,
        "default",
        "MDS Main",
        xucAmiConfig.ipAddress,
        true
      )
      val allMds: List[MediaServerConfig] = List(mds0)
      val probe: TestProbe                = TestProbe()
      val deathWatcher: TestProbe         = TestProbe()
      deathWatcher.watch(probe.ref)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(allMds)
      )
      when(configServer.getMediaServerConfig(0))
        .thenReturn(Future.successful(mdsMain))
      when(createManagerConnectorMock.apply(any[MediaServerConfig]()))
        .thenReturn(probe.ref)

      var (ref, a) = actor()
      verify(createManagerConnectorMock, timeout(500)).apply(mdsMain)
      reset(createManagerConnectorMock)

      ref ! DeleteMds(0)
      deathWatcher.expectTerminated(probe.ref)
      ref ! LoadOrReloadMds(0)
      verify(createManagerConnectorMock, timeout(500)).apply(mdsMain)
    }

    "return list of mds connected" in new Helper() {
      val mdsMain: MediaServerConfig =
        MediaServerConfig(0, "default", "MDS Main", "10.49.0.10", true)
      val mds1: MediaServerConfig =
        MediaServerConfig(1, "mds1", "MDS 1", "10.49.0.11", false)
      val mds2: MediaServerConfig =
        MediaServerConfig(2, "mds2", "MDS 2", "10.49.0.12", false)
      val allMds: List[MediaServerConfig] = List(mdsMain, mds1, mds2)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(allMds)
      )
      when(createManagerConnectorMock.apply(any[MediaServerConfig]()))
        .thenReturn(TestProbe().ref)
      var (ref, a) = actor()

      allMds.foreach(m => {
        verify(createManagerConnectorMock, timeout(500)).apply(m)
      })

      ref ! AmiSupervisor.GetConnectedMds

      expectMsgPF(1.second) { case AmiSupervisor.ConnectedMds(list) =>
        list should contain allElementsOf list
      }

    }

    "stop manager for given mds when mds is removed" in new Helper() {
      val mds1: MediaServerConfig =
        MediaServerConfig(1, "mds1", "MDS 1", "10.49.0.11", false)
      val mds2: MediaServerConfig =
        MediaServerConfig(2, "mds2", "MDS 2", "10.49.0.12", false)
      val allMds: List[MediaServerConfig] = List(mds1, mds2)
      val manager: TestProbe              = TestProbe()
      val managerWatcher: TestProbe       = TestProbe()
      managerWatcher.watch(manager.ref)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(allMds)
      )
      when(createManagerConnectorMock.apply(any[MediaServerConfig]()))
        .thenReturn(manager.ref)
      var (ref, a) = actor()

      allMds.foreach(m => {
        verify(createManagerConnectorMock, timeout(500)).apply(m)
      })

      ref ! AmiSupervisor.DeleteMds(mds1.id)
      managerWatcher.expectTerminated(manager.ref)

      ref ! AmiSupervisor.GetConnectedMds
      expectMsgPF(1.second) { case AmiSupervisor.ConnectedMds(list) =>
        list should not contain mds1
      }
    }

    "create manager for given mds when mds is created after startup" in new Helper() {
      val mds0: MediaServerConfig = MediaServerConfig(
        0,
        "default",
        "MDS Main",
        xucAmiConfig.ipAddress,
        true
      )
      val mds1: MediaServerConfig =
        MediaServerConfig(1, "mds1", "MDS 1", "10.49.0.11", false)
      val allMds             = List.empty[MediaServerConfig]
      val manager: TestProbe = TestProbe()

      when(configServer.getMediaServerConfigAll)
        .thenReturn(
          Future.successful(List(mds0)),
          Future.successful(List(mds0, mds1))
        )

      when(createManagerConnectorMock.apply(mds1)).thenReturn(manager.ref)

      var (ref, a) = actor()

      ref ! AmiSupervisor.LoadOrReloadMds(mds1.id)
      verify(createManagerConnectorMock, timeout(500)).apply(mds1)

      ref ! AmiSupervisor.GetConnectedMds

      expectMsgPF(1.second) { case AmiSupervisor.ConnectedMds(list) =>
        list should contain.only(mds0, mds1)
      }
    }

    "recreate manager instantly for given mds when mds is updated" in new Helper() {
      val mds1: MediaServerConfig =
        MediaServerConfig(1, "mds1", "MDS 1", "10.49.0.11", false)
      val mds2: MediaServerConfig =
        MediaServerConfig(2, "mds2", "MDS 2", "10.49.0.12", false)
      val mds1Reloaded: MediaServerConfig =
        MediaServerConfig(1, "mds1", "MDS 1 Reloaded", "10.49.0.111", false)
      val allMds: List[MediaServerConfig]         = List(mds1, mds2)
      val allMdsReloaded: List[MediaServerConfig] = List(mds1Reloaded, mds2)
      val manager: TestProbe                      = TestProbe()
      val managerReloaded: TestProbe              = TestProbe()
      val managerWatcher: TestProbe               = TestProbe()
      managerWatcher.watch(manager.ref)
      when(configServer.getMediaServerConfigAll)
        .thenReturn(
          Future.successful(allMds),
          Future.successful(allMdsReloaded)
        )

      when(createManagerConnectorMock.apply(mds1)).thenReturn(manager.ref)
      when(createManagerConnectorMock.apply(mds1Reloaded))
        .thenReturn(managerReloaded.ref)
      var (ref, a) = actor()

      allMds.foreach(m => {
        verify(createManagerConnectorMock, timeout(500)).apply(m)
      })

      ref ! LoadOrReloadMds(mds1.id)
      managerWatcher.expectTerminated(manager.ref)
      ref ! ScheduleMdsReload(mds1.id)
      verify(createManagerConnectorMock, timeout(500)).apply(mds1Reloaded)

      ref ! AmiSupervisor.GetConnectedMds

      verify(a.scheduler, times(0)).scheduleOnce(
        any[FiniteDuration],
        any[ActorRef],
        meq(LoadOrReloadMds(mds1.id))
      )(any[ExecutionContext], any[ActorRef])

      expectMsgPF(1.second) { case AmiSupervisor.ConnectedMds(list) =>
        list should contain.only(mds1Reloaded, mds2)
      }
    }

    "Recreate an mds manager on mds actor restart request" in new Helper {
      val mds1: MediaServerConfig =
        MediaServerConfig(1, "mds1", "MDS 1", "10.49.0.11", read_only = false)
      val mds2: MediaServerConfig =
        MediaServerConfig(2, "mds2", "MDS 2", "10.49.0.12", read_only = false)

      val manager1: TestProbe     = TestProbe()
      val manager2: TestProbe     = TestProbe()
      val deathWatcher: TestProbe = TestProbe()
      deathWatcher.watch(manager1.ref)
      deathWatcher.watch(manager2.ref)
      val mediaServers: Map[Long, MediaServer] = Map(
        mds1.id -> MediaServer(mds1, manager1.ref),
        mds2.id -> MediaServer(mds2, manager2.ref)
      )

      val mdsMain: MediaServerConfig =
        MediaServerConfig(0, "default", "MDS Main", "10.49.0.10", true)
      when(configServer.getMediaServerConfigAll).thenReturn(
        Future.successful(List(mds1, mds2))
      )
      when(createManagerConnectorMock.apply(mds1)).thenReturn(manager1.ref)
      when(createManagerConnectorMock.apply(mds2)).thenReturn(manager2.ref)

      var (ref, a) = actor()

      ref ! FailedMdsActor(manager2.ref)
      deathWatcher.expectTerminated(manager2.ref)

      ref ! ScheduleMdsReload(mds2.id)

      verify(a.scheduler, timeout(mockTimeout)).scheduleOnce(
        any[FiniteDuration],
        any[ActorRef],
        meq(LoadOrReloadMds(mds2.id))
      )(any[ExecutionContext], any[ActorRef])

    }
  }

}
