package xivo.services

import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import controllers.helpers.{RequestError, RequestSuccess, RequestUnauthorized}
import models.{Token, XivoUser}
import org.joda.time.DateTime
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import services.config.ConfigRepository
import xivo.services.TokenRetriever.TokenByLogin
import xivo.services.XivoAuthentication.{
  AuthTimeSynchronizationError,
  AuthTimeout,
  AuthUnknownUser
}

class TokenRetrieverSpec
    extends TestKitSpec("TokenRetrieverSpec")
    with MockitoSugar {

  class Helper {
    val xivoAuthentication: TestProbe = TestProbe()
    val repo: ConfigRepository        = mock[ConfigRepository]

    def actor(): (TestActorRef[TokenRetriever], TokenRetriever) = {
      val a = TestActorRef(new TokenRetriever(xivoAuthentication.ref, repo))
      (a, a.underlyingActor)
    }
  }

  "A TokenRetriever" should {
    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetCtiToken request
      | answer with RequestSuccess on success""" in new Helper {
      val login = "j.doe"
      val user: XivoUser =
        XivoUser(
          12,
          None,
          None,
          "John",
          Some("Doe"),
          Some(login),
          None,
          None,
          None
        )
      val token: Token =
        Token(
          "thetoken",
          new DateTime(),
          new DateTime(),
          "cti",
          Some("userId"),
          List.empty
        )
      when(repo.getCtiUser(login)).thenReturn(Some(user))
      var (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetCtiToken(user.id))
      xivoAuthentication.reply(token)
      expectMsg(RequestSuccess(token.token))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetCtiToken request
      | answer with RequestError on AuthTimeout""" in new Helper {
      val login = "j.doe"
      val user: XivoUser =
        XivoUser(
          12,
          None,
          None,
          "John",
          Some("Doe"),
          Some(login),
          None,
          None,
          None
        )
      when(repo.getCtiUser(login)).thenReturn(Some(user))
      var (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetCtiToken(user.id))
      xivoAuthentication.reply(AuthTimeout)
      expectMsg(RequestError("Token retrieval timeout"))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetCtiToken request
      | answer with RequestError on AuthTimeSynchronizationError""" in new Helper {
      val login = "j.doe"
      val user: XivoUser =
        XivoUser(
          12,
          None,
          None,
          "John",
          Some("Doe"),
          Some(login),
          None,
          None,
          None
        )
      when(repo.getCtiUser(login)).thenReturn(Some(user))
      var (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetCtiToken(user.id))
      xivoAuthentication.reply(AuthTimeSynchronizationError)
      expectMsg(RequestError("Token time synchronization error"))
    }

    """when receiving TokenByLogin:
      | find the associated user
      | send a send a GetCtiToken request
      | answer with RequestUnauthorized on AuthUnknownUser""" in new Helper {
      val login = "j.doe"
      val user: XivoUser =
        XivoUser(
          12,
          None,
          None,
          "John",
          Some("Doe"),
          Some(login),
          None,
          None,
          None
        )
      when(repo.getCtiUser(login)).thenReturn(Some(user))
      var (ref, a) = actor()

      ref ! TokenByLogin(login)

      xivoAuthentication.expectMsg(XivoAuthentication.GetCtiToken(user.id))
      xivoAuthentication.reply(AuthUnknownUser)
      expectMsg(RequestUnauthorized(s"User $login unknown by the token server"))
    }

    "answer with RequestUnauthorized if the user is not found in ConfigRepository" in new Helper {
      val login = "j.doe"
      when(repo.getCtiUser(login)).thenReturn(None)
      var (ref, a) = actor()

      ref ! TokenByLogin(login)

      expectMsg(RequestUnauthorized(s"User $login unknown by the Xuc"))
    }
  }
}
