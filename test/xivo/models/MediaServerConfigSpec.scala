package xivo.models

import play.api.libs.json._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class MediaServerConfigSpec extends AnyWordSpec with Matchers {
  "MediaServerConfig" should {
    "be serialized to json" in {
      val expected = Json.parse(
        """{"id": 1, "name":"mds1", "display_name":"MDS 1", "voip_ip": "192.168.1.232", "read_only": false}"""
      )
      val mds1 = MediaServerConfig(1, "mds1", "MDS 1", "192.168.1.232", false)
      Json.toJson(mds1) shouldBe expected
    }

    "be deserialized from json" in {
      val json = Json.parse(
        """{"id": 1, "name":"mds1", "display_name":"MDS 1", "voip_ip": "192.168.1.232", "read_only": false}"""
      )
      val expected =
        MediaServerConfig(1, "mds1", "MDS 1", "192.168.1.232", false)
      json.as[MediaServerConfig] shouldBe expected
    }
  }
}
