package xivo.models

import org.scalatest._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.WsScalaTestClient
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.guice.GuiceApplicationBuilder
import services.config.ConfigServerRequester
import xivo.data.dbunit.DBUtil
import xivo.network.XiVOWS
import xuctest.{BaseTest, IntegrationTest}
import org.mockito.Mockito.when
import play.api.db.Database

import scala.concurrent.{ExecutionContext, Future}
import play.api.Application

class AgentQueueMemberSpec
    extends BaseTest
    with OptionValues
    with WsScalaTestClient
    with GuiceOneAppPerSuite
    with MockitoSugar {

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig ++ dbtestConfig)
    )
      .build()

  val configRequester: ConfigServerRequester = mock[ConfigServerRequester]

  private def getQueueMember(queueId: Long, penalty: Int): QueueMember =
    QueueMember(
      "queueNameForId" + queueId,
      queueId,
      "interface",
      penalty,
      0,
      "userType",
      1,
      "channel",
      "category",
      0
    )

  implicit lazy val executionContext: ExecutionContext =
    app.injector.instanceOf[ExecutionContext]

  "agent queue member" should {

    "be able to have penalty updated" taggedAs IntegrationTest ignore {
      val agentId          = 19
      val queueId          = 1
      val agentQueueMember = app.injector.instanceOf[AgentQueueMemberFactory]
      val result           = agentQueueMember.setAgentQueue(agentId, queueId, 11)
      result should be(Some(AgentQueueMember(agentId, queueId, 11)))
    }
    "be able to remove an agent from a queue" taggedAs IntegrationTest ignore {
      val agentId          = 19
      val queueId          = 1
      val agentQueueMember = app.injector.instanceOf[AgentQueueMemberFactory]
      agentQueueMember.setAgentQueue(agentId, queueId, 11)

      val result = agentQueueMember.removeAgentFromQueue(agentId, queueId)

      result should be(Some(AgentQueueMember(agentId, queueId, -1)))
    }

    "be able to retrieve a list of queue members from db" in withTestDataDatabase {
      implicit database =>
        DBUtil.setupData("agentqueuemember.xml")

        val agentQueueMember = new AgentQueueMemberFactoryImpl(
          database,
          mock[XiVOWS],
          mock[ConfigServerRequester]
        )
        val aqms = agentQueueMember.all()

        aqms.length should be(2)
        aqms should contain(AgentQueueMember(21, 101, 2))

    }
    "be able to retrieve queue members from agent number and queue name" in withTestDataDatabase {
      implicit database =>
        DBUtil.setupData("agentqueuemember.xml")

        val agentQueueMember = new AgentQueueMemberFactoryImpl(
          database,
          mock[XiVOWS],
          mock[ConfigServerRequester]
        )
        val agqm = agentQueueMember.getQueueMember("1230", "yellow")

        agqm should be(Some(AgentQueueMember(21, 102, 7)))

    }

    "get agent association from config mgt" in {
      val agentQueueMember = new AgentQueueMemberFactoryImpl(
        mock[Database],
        mock[XiVOWS],
        configRequester
      )
      val expectedQueueMember = getQueueMember(5L, 0)
      when(configRequester.getAgentConfig(3L)).thenReturn(
        Future(
          AgentConfigUpdate(
            3L,
            "first",
            "last",
            "2002",
            "default",
            List(expectedQueueMember),
            1,
            Some(3)
          )
        )
      )

      agentQueueMember.getAgentAssociation(3L, 5L) shouldEqual Some(
        AgentQueueMember(3L, 5L, 0)
      )
    }

  }
}
