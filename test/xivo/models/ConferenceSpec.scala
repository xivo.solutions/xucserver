package xivo.models

import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.concurrent.ScalaFutures
import xivo.data.dbunit.DBUtil
import xuctest.BaseTest

class ConferenceSpec extends BaseTest with MockitoSugar with ScalaFutures {

  "Conference Factory" should {
    "be able to retrieve conference with user and admin pin" in withTestDataDatabase {
      implicit database =>
        DBUtil.setupData("meetme.xml")
        val conferenceFactory = new Conference(database)

        whenReady(conferenceFactory.all()) { conferences =>
          val meetme = conferences.head
          meetme.confno shouldBe "4000"
          meetme.user_pin shouldBe Some("1234")
          meetme.admin_pin shouldBe Some("4321")
        }
    }
  }

}
