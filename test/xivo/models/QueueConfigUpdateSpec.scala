package xivo.models

import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class QueueConfigUpdateSpec extends AnyWordSpec with Matchers {
  "A QueueConfigUpdate" should {
    "convert to json" in {
      val queueFeature = QueueConfigUpdate(
        1,
        "queue",
        "queue",
        "1010",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      Json.toJson(queueFeature) shouldEqual Json.obj(
        "id"                    -> 1L,
        "name"                  -> queueFeature.name,
        "displayName"           -> queueFeature.displayName,
        "number"                -> queueFeature.number,
        "context"               -> queueFeature.context,
        "data_quality"          -> queueFeature.data_quality,
        "retries"               -> queueFeature.retries,
        "ring"                  -> queueFeature.ring,
        "transfer_user"         -> queueFeature.transfer_user,
        "transfer_call"         -> queueFeature.transfer_call,
        "write_caller"          -> queueFeature.write_caller,
        "write_calling"         -> queueFeature.write_calling,
        "url"                   -> queueFeature.url,
        "announceoverride"      -> queueFeature.announceoverride,
        "timeout"               -> queueFeature.timeout,
        "preprocess_subroutine" -> queueFeature.preprocess_subroutine,
        "announce_holdtime"     -> queueFeature.announce_holdtime,
        "waittime"              -> queueFeature.waittime,
        "waitratio"             -> queueFeature.waitratio,
        "ignore_forward"        -> queueFeature.ignore_forward,
        "recording_mode"        -> "recorded",
        "recording_activated"   -> 1
      )
    }

    "be created from json" in {
      val queueFeature = QueueConfigUpdate(
        1,
        "queue",
        "queue",
        "1010",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        "recorded",
        1
      )

      val json: JsValue = Json.parse(
        """
          | {
          | "id":1,"name":"queue","displayName":"queue","number":"1010","context":"context","data_quality":1,
          | "retries":1,"ring":1,"transfer_user":1,"transfer_call":1,"write_caller":1,"write_calling":1,"url":"url",
          | "announceoverride":"announce","timeout":1,"preprocess_subroutine":"fs","announce_holdtime":1,"waittime":1,
          | "waitratio":1,"ignore_forward":1,"recording_mode":"recorded","recording_activated":1
          | }
          | """.stripMargin
      )

      json.validate[QueueConfigUpdate] match {
        case JsSuccess(qf, _) => qf shouldEqual queueFeature
        case JsError(errors)  => fail()
      }
    }
  }
}
