package xivo.models

import models.QueueLogImpl
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import xivo.data.dbunit.DBUtil
import xivo.xucstats.XucStatsConfig
import xuctest.BaseTest

class QueueLogSpec extends BaseTest with MockitoSugar {

  "queuelog" should {
    "be able to be retreived" in withTestDataDatabase { implicit database =>
      val statConfig = mock[XucStatsConfig]
      when(statConfig.initFromMidnight).thenReturn(true)

      DBUtil.setupData("queuelog.xml")
      val queueLog = new QueueLogImpl(database, statConfig)

      queueLog.getAll("'2014-04-18 12:12:20.225111'").size should be > 1

    }
  }

}
