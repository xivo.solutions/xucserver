package xivo.models

import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.Json
import xuctest.BaseTest

class CustomerCallHistorySpec extends BaseTest with MockitoSugar {

  "A CustomerCallHistory" should {
    "be serialized to json" in {
      val startDate =
        CustomerCallDetail.format.parseDateTime("2017-01-01 08:00:00")
      val duration =
        Some(CustomerCallDetail.periodFormat.parsePeriod("00:10:15"))
      val waitTime =
        Some(CustomerCallDetail.periodFormat.parsePeriod("00:00:05"))
      val agentName = Some("James Bond")
      val agentNum  = Some("8000")
      val queueName = Some("test Queue")
      val queueNum  = Some("3000")
      val status    = CallStatus.Answered

      val historyDetail = CustomerCallDetail(
        startDate,
        duration,
        waitTime,
        agentName,
        agentNum,
        queueName,
        queueNum,
        status
      )

      Json.toJson(historyDetail) shouldEqual Json.obj(
        "start"     -> "2017-01-01 08:00:00",
        "duration"  -> "00:10:15",
        "waitTime"  -> "00:00:05",
        "agentName" -> "James Bond",
        "agentNum"  -> "8000",
        "queueName" -> "test Queue",
        "queueNum"  -> "3000",
        "status"    -> CallStatus.Answered.toString
      )
    }
  }

}
