package xivo.models

import xivo.data.dbunit.DBUtil
import xuctest.BaseTest

class UserLineNumberSpec extends BaseTest {

  "UserLineNumber" should {

    "be able to be retreived from base by user id" in withTestDataDatabase {
      implicit database =>
        {

          DBUtil.setupData("userlinenumber.xml")

          val uln            = UserLineNumber(1, 10, "1000")
          val userLineNumber = new UserLineNumberFactoryImpl(database)

          userLineNumber.get(1) should be(Some(uln))
        }
    }
    "return none if line with this user id does not exists" in withTestDataDatabase {
      implicit database =>
        {
          DBUtil.setupData("userlinenumber.xml")
          val userLineNumber = new UserLineNumberFactoryImpl(database)

          userLineNumber.get(289) should be(None)
        }

    }
  }
}
