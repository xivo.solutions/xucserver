package xivo.models

import play.api.libs.json.{JsError, Json}
import xuctest.BaseTest

class PersonalContactSpec extends BaseTest {

  "Personal Contact" should {
    "be able to be transformed to json" in {
      // all of following should be converted to None: missing key, empty string and
      //   string containing white space characters only
      // lastname => missing key, fax => empty string, mobile => white space only string

      val contact = Json.parse("""
          |{"firstname":"John",
          | "number":"1001",
          | "mobile":" ",
          | "fax":"",
          | "email":"john@doe.org",
          | "company":"John Doe Enter_price"
          | }
          |""".stripMargin)

      val pc: PersonalContactRequest =
        contact.validate[PersonalContactRequest].get

      val expected = PersonalContactRequest(
        Some("John"),
        None,
        Some("1001"),
        None,
        None,
        Some("john@doe.org"),
        Some("John Doe Enter_price")
      )
      pc shouldBe expected
    }

    "not be transformed to json if firstname and lastname are empty" in {

      val contact = Json.parse("""
          |{"firstname":"",
          | "lastname":"",
          | "number":"1001",
          | "mobile":"",
          | "fax":"",
          | "email":"john@doe.org",
          | "company":"John Doe Enter_price"
          | }
          |""".stripMargin)

      contact.validate[PersonalContactRequest] match {
        case JsError(_) => succeed
        case _          => fail()
      }

    }

    "not be transformed to json if number, mobile and fax are empty" in {

      val contact = Json.parse("""
          |{"firstname":"John",
          | "lastname":"",
          | "number":"",
          | "mobile":"",
          | "fax":"",
          | "email":"john@doe.org",
          | "company":"John Doe Enter_price"
          | }
          |""".stripMargin)

      contact.validate[PersonalContactRequest] match {
        case JsError(_) => succeed
        case _          => fail()
      }
    }
  }
}
