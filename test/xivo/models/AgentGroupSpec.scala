package xivo.models

import play.api.libs.json.Json
import xivo.data.dbunit.DBUtil
import xuctest.BaseTest

class AgentGroupSpec extends BaseTest {

  "agent group" should {
    "be able to be retrieved" in withTestDataDatabase { implicit database =>
      DBUtil.setupData("agentgroup.xml")

      val group1     = AgentGroup(Some(1), "group1", Some("First group"))
      val group2     = AgentGroup(Some(2), "biggroup", None)
      val agentGroup = new AgentGroupFactoryImpl(database)
      val groups     = agentGroup.all()

      groups.length should be(2)
      groups should contain(group1)
      groups should contain(group2)
    }

    "do not retrieve deleted group" in withTestDataDatabase {
      implicit database =>
        DBUtil.setupData("agentgroup.xml")

        val group1     = AgentGroup(Some(3), "deletedgroup", None)
        val agentGroup = new AgentGroupFactoryImpl(database)
        val groups     = agentGroup.all()

        groups should not contain group1
    }

    "be able to be transformed to json" in {
      val ag = AgentGroup(Some(34), "biggroup", Some("Big Group"))

      val agJson = Json.toJson(ag)

      agJson shouldEqual Json.obj(
        "id"          -> 34,
        "name"        -> "biggroup",
        "displayName" -> "Big Group"
      )
    }
  }

}
