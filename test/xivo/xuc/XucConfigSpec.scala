package xivo.xuc

import play.api.Configuration
import com.typesafe.config.ConfigFactory
import org.scalatestplus.mockito.MockitoSugar
import pekkotest.TestKitSpec

class XucConfigSpec extends TestKitSpec("XucConfigSpec") with MockitoSugar {

  "A XucConfigSpec" should {
    "return correct map of remote nodes and their extensions" in {
      val mockConfig =
        """
          pekko.cluster.seed-nodes = ["node1:2551", "node2:2552", "node3:2553"]
          cluster.conferences{
            extensionsToRemoteHosts = ["1000@ipbx1.test.avencall.com", "1001@ipbx2.test.avencall.com", "1002"]
            contextToRemoteHosts = "customContext"
          }
          pekko.remote.artery.canonical.hostname = "node1"
          pekko.remote.artery.canonical.port = "2551"
        """

      val config    = Configuration(ConfigFactory.parseString(mockConfig))
      val xucConfig = new XucConfig(config)
      val result    = xucConfig.getConfExtensionPerRemoteNode

      val expected = Map(
        "node2:2552" -> RemoteConferenceExtension(
          "1001",
          "customContext",
          Some("ipbx2.test.avencall.com")
        ),
        "node3:2553" -> RemoteConferenceExtension("1002", "customContext", None)
      )

      result shouldEqual expected
    }

    "return an empty map when no nodes are defined" in {
      val config    = Configuration(ConfigFactory.empty())
      val xucConfig = new XucConfig(config)
      val result    = xucConfig.getConfExtensionPerRemoteNode
      result shouldBe empty
    }

    "load correctly directorySourcesPriority and directoryInternalSource" in {
      val mockConfig =
        """
directory {
  sources.priority = "    ldap1, internal,    ldap1,ldap3"
  internal.source = "internal, ldap1,    ldap2,ldap2    ,   "
}
              """
      val config = Configuration(ConfigFactory.parseString(mockConfig))

      val xucConfig = new XucConfig(config)
      assert(xucConfig.directorySourcesPriority == Set("ldap1","internal","ldap3"))
      assert(xucConfig.directoryInternalSource == Set("internal","ldap1","ldap3"))
    }
  }
}
