
drop table if exists meetmefeatures;

CREATE TABLE meetmefeatures
(
  id serial NOT NULL,
  meetmeid integer NOT NULL,
  name character varying(128) NOT NULL,
  confno character varying(40) NOT NULL,
  context character varying(39) NOT NULL, 
  user_pin character varying(12),
  admin_pin character varying(11)
)
