package controllers.helpers

import org.apache.pekko.actor.ActorRef
import models.Token
import org.joda.time.DateTime
import xivo.xuc.XucBaseConfig

import scala.concurrent.ExecutionContext

object XivoAuthenticationHelper {
  def getXivoAuthToken(
      xivoUserId: Long,
      config: XucBaseConfig
  )(implicit
      ec: ExecutionContext,
      xivoAuthentication: ActorRef
  ): Option[Token] = {
    val date = new DateTime(2023, 4, 1, 1, 22, 3)
    val tcti =
      Token(
        "token",
        date,
        date,
        "cti",
        Some("authId"),
        List("alias.ctiuser")
      )
    Some(tcti)
  }
}
