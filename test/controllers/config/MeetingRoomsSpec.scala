package controllers.config

import models.XivoUser
import models.ws.auth.AuthenticationInformation
import org.apache.pekko.stream.Materializer
import org.mockito.Mockito.{reset, timeout, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.ws.WSResponse
import play.api.mvc.*
import play.api.test.Helpers.*
import play.api.test.*
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models.{
  MeetingRoomToken,
  PersonalMeetingRoom,
  StaticMeetingRoom,
  TemporaryMeetingRoom
}
import xuctest.BaseTest
import org.joda.time.DateTime
import play.api.Play.materializer

import scala.concurrent.Future
import play.api.Application

class MeetingRoomsSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {

  val repo: ConfigRepository                     = mock[ConfigRepository]
  val configRequesterMock: ConfigServerRequester = mock[ConfigServerRequester]

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[ConfigServerRequester].to(configRequesterMock))
      .build()

  class Helper {
    reset(repo)
    reset(configRequesterMock)
    val expires = 54000

    val user: XivoUser =
      XivoUser(
        1,
        None,
        None,
        "James",
        Some("Bond"),
        Some("jbond"),
        None,
        None,
        None
      )
    when(repo.getCtiUser("jbond")).thenReturn(Some(user))

    val now: Long = new DateTime().getMillis / 1000
    val token: AuthenticationInformation = AuthenticationInformation(
      user.username.get,
      now + expires,
      now,
      "cti",
      List(
        "alias.ctiuser"
      ),
      None,
      None
    )
    val wsResponse: WSResponse = mock[WSResponse]
    val fakeAuth: FakeHeaders = FakeHeaders(
      Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
    )
  }

  "MeetingRoomsSpec" should {
    "Forward token request to configmgt for personal meetingRoom" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/meetingrooms/personal/42",
        fakeAuth,
        AnyContentAsEmpty
      )
      when(
        configRequesterMock.getMeetingRoomToken(
          "42",
          Some(1),
          PersonalMeetingRoom
        )
      )
        .thenReturn(
          Future.successful(MeetingRoomToken("some-uuid", "some-token"))
        )

      val ctrl: MeetingRooms         = app.injector.instanceOf[MeetingRooms]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] =
        call(ctrl.getTokenById("42", PersonalMeetingRoom), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).getMeetingRoomToken(
        "42",
        Some(1),
        PersonalMeetingRoom
      )
    }

    "Forward token request to configmgt for static meetingRoom" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/meetingrooms/personal/42",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(
        configRequesterMock.getMeetingRoomToken(
          "42",
          Some(1),
          StaticMeetingRoom
        )
      )
        .thenReturn(
          Future.successful(MeetingRoomToken("some-uuid", "some-token"))
        )

      val ctrl: MeetingRooms         = app.injector.instanceOf[MeetingRooms]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] =
        call(ctrl.getTokenById("42", StaticMeetingRoom), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).getMeetingRoomToken(
        "42",
        Some(1),
        StaticMeetingRoom
      )
    }

    "Forward token request to configmgt for temporary meetingRoom" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/meetingrooms/temporary/token/User1",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(
        configRequesterMock.getMeetingRoomToken(
          "User1",
          Some(1),
          TemporaryMeetingRoom
        )
      )
        .thenReturn(
          Future.successful(MeetingRoomToken("some-uuid", "some-token"))
        )

      val ctrl: MeetingRooms         = app.injector.instanceOf[MeetingRooms]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] =
        call(ctrl.getTokenById("User1", TemporaryMeetingRoom), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).getMeetingRoomToken(
        "User1",
        Some(1),
        TemporaryMeetingRoom
      )
    }

    "Forward getPersonalRoomById request to configmgt" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/meetingrooms/personal/5",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(wsResponse.status).thenReturn(OK)
      when(wsResponse.headers).thenReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      when(
        configRequesterMock.forward(
          s"meetingrooms/personal/5?userId=${user.id}",
          "GET",
          None,
          "2.0",
          None
        )
      )
        .thenReturn(Future.successful(wsResponse))

      val ctrl: MeetingRooms         = app.injector.instanceOf[MeetingRooms]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] = call(ctrl.getPersonalRoomById(5), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        s"meetingrooms/personal/5?userId=${user.id}",
        "GET",
        None,
        "2.0",
        None
      )
    }

    "Forward addPersonalRoom request to configmgt" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "POST",
        "/xuc/api/2.0/config/meetingrooms/personal",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(wsResponse.status).thenReturn(OK)
      when(wsResponse.headers).thenReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      when(
        configRequesterMock.forward(
          s"meetingrooms/personal?userId=${user.id}",
          "POST",
          None,
          "2.0",
          None
        )
      )
        .thenReturn(Future.successful(wsResponse))

      val ctrl: MeetingRooms         = app.injector.instanceOf[MeetingRooms]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] = call(ctrl.addPersonalRoom(), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        s"meetingrooms/personal?userId=${user.id}",
        "POST",
        None,
        "2.0",
        None
      )
    }

    "Forward editPersonalRoom request to configmgt" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "PUT",
        "/xuc/api/2.0/config/meetingrooms/personal",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(wsResponse.status).thenReturn(OK)
      when(wsResponse.headers).thenReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      when(
        configRequesterMock.forward(
          s"meetingrooms/personal?userId=${user.id}",
          "PUT",
          None,
          "2.0",
          None
        )
      )
        .thenReturn(Future.successful(wsResponse))

      val ctrl: MeetingRooms         = app.injector.instanceOf[MeetingRooms]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] = call(ctrl.editPersonalRoom(), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        s"meetingrooms/personal?userId=${user.id}",
        "PUT",
        None,
        "2.0",
        None
      )
    }

    "Forward deletePersonalRoom request to configmgt" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "DELETE",
        "/xuc/api/2.0/config/meetingrooms/personal/5",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(wsResponse.status).thenReturn(OK)
      when(wsResponse.headers).thenReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      when(
        configRequesterMock.forward(
          s"meetingrooms/personal/5?userId=${user.id}",
          "DELETE",
          None,
          "2.0",
          None
        )
      )
        .thenReturn(Future.successful(wsResponse))

      val ctrl: MeetingRooms         = app.injector.instanceOf[MeetingRooms]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] = call(ctrl.deletePersonalRoom(5), rq)

      status(res) shouldBe OK
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        s"meetingrooms/personal/5?userId=${user.id}",
        "DELETE",
        None,
        "2.0",
        None
      )
    }
  }
}
