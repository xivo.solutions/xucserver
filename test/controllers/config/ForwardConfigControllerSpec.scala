package controllers.config
import org.apache.pekko.util.ByteString
import models.{WebServiceUser, XivoUser}
import models.ws.auth.AuthenticationInformation
import org.joda.time.DateTime
import org.mockito.Mockito.{reset, timeout, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.WSResponse
import play.api.mvc.*
import play.api.test.Helpers.*
import play.api.test.*
import services.config.{ConfigRepository, ConfigServerRequester}
import xuctest.BaseTest
import org.apache.pekko.stream.Materializer

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import play.api.Application

class ForwardConfigControllerSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {
  val repo: ConfigRepository                     = mock[ConfigRepository]
  val configRequesterMock: ConfigServerRequester = mock[ConfigServerRequester]

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[ConfigServerRequester].to(configRequesterMock))
      .build()

  "ForwardConfig Controller" should {
    "allow forward any request to /config towards configmgt" in new Helper {
      val rawText       = "{\"data\":{}}"
      val json: JsValue = Json.parse(rawText)
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        "/xuc/api/2.0/config/handler",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(wsResponse.status).thenReturn(OK)
      when(wsResponse.bodyAsBytes).thenReturn(ByteString(rawText))
      when(wsResponse.headers).thenReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      when(
        configRequesterMock.forward(
          "handler",
          "GET",
          None,
          "2.0",
          Some("jbond")
        )
      )
        .thenReturn(Future(wsResponse))
      val ctrl: ForwardConfigController =
        app.injector.instanceOf[ForwardConfigController]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] = call(ctrl.forward("handler", "2.0"), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldBe json
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        "handler",
        "GET",
        None,
        "2.0",
        Some("jbond")
      )
    }

    "allow forward any request to /config towards configmgt and forward the body" in new Helper {
      val rawText       = "{\"data\":{}}"
      val json: JsValue = Json.parse(rawText)
      val body: JsValue = Json.parse("""{"fileName": "queue1_fileName"}""")
      val rq: FakeRequest[JsValue] =
        FakeRequest("GET", "/xuc/api/2.0/config/handler", fakeAuth, body)

      when(wsResponse.status).thenReturn(OK)
      when(wsResponse.bodyAsBytes).thenReturn(ByteString(rawText))
      when(wsResponse.headers).thenReturn(
        Map("Content-Type" -> Seq("application/json"))
      )
      when(
        configRequesterMock.forward(
          "handler",
          "GET",
          Some(body),
          "2.0",
          Some("jbond")
        )
      )
        .thenReturn(Future(wsResponse))
      val ctrl: ForwardConfigController =
        app.injector.instanceOf[ForwardConfigController]
      implicit val mat: Materializer = app.materializer

      val res: Future[Result] = call(ctrl.forward("handler", "2.0"), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldBe json
      header("Content-Type", res) shouldBe Some("application/json")
      verify(configRequesterMock, timeout(500)).forward(
        "handler",
        "GET",
        Some(body),
        "2.0",
        Some("jbond")
      )
    }
  }

  class Helper {
    reset(repo)
    reset(configRequesterMock)
    val expires = 54000

    val user: XivoUser =
      XivoUser(
        1,
        None,
        None,
        "James",
        Some("Bond"),
        Some("jbond"),
        None,
        None,
        None
      )
    when(repo.getCtiUser("jbond")).thenReturn(Some(user))
    val now: Long = new DateTime().getMillis / 1000

    val token: AuthenticationInformation =
      AuthenticationInformation(
        user.username.get,
        now + expires,
        now,
        "cti",
        List("alias.ctiuser"),
        None,
        None
      )
    val wsResponse: WSResponse = mock[WSResponse]
    val fakeAuth: FakeHeaders = FakeHeaders(
      Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
    )
  }
}
