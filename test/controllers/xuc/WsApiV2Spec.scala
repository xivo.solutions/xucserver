package controllers.xuc

import org.apache.pekko.actor.ActorRef
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.Materializer
import org.apache.pekko.testkit.TestProbe
import models.{WebServiceUser, XivoUser, XivoUserDao}
import models.ws.auth.AuthenticationInformation
import org.joda.time.DateTime
import org.mockito.ArgumentMatcher
import org.mockito.ArgumentMatchers.{any, anyString}
import org.mockito.Mockito.*
import org.scalatest.matchers.should.Matchers
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc.*
import play.api.test.Helpers.*
import play.api.test.*
import services.ActorIdsFactory
import services.callhistory.CallHistoryManager
import services.config.{ConfigRepository, ConfigServerRequester}
import services.request.PhoneRequest.Dial
import services.request.{BaseRequest, DialFromQueue, DialToQueue}
import xivo.models.PartialUserServices
import xuctest.BaseTest

import scala.concurrent.Future
import play.api.Application

class WsApiV2Spec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {

  implicit val repo: ConfigRepository   = mock[ConfigRepository]
  implicit lazy val mat: Materializer   = app.materializer
  implicit val system: ActorSystem      = ActorSystem()
  implicit val xivoUserDao: XivoUserDao = mock[XivoUserDao]
  implicit val configServerRequester: ConfigServerRequester =
    mock[ConfigServerRequester]
  implicit val callHistoryManager: CallHistoryManager = mock[CallHistoryManager]

  val actorIdMock: ActorIdsFactory = mock[ActorIdsFactory]

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ActorSystem].to(system))
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[ActorIdsFactory].to(actorIdMock))
      .overrides(bind[XivoUserDao].to(xivoUserDao))
      .overrides(bind[ConfigServerRequester].to(configServerRequester))
      .overrides(bind[CallHistoryManager].to(callHistoryManager))
      .build()

  class Helper {
    reset(repo)

    val actorProbe: TestProbe = TestProbe()
    when(
      actorIdMock
        .queueDispatcherPath(
          any[ActorRef]
        )
    )
      .thenReturn(actorProbe.ref.path)

    when(
      actorIdMock
        .ctiRouterPath(
          any[ActorRef],
          anyString()
        )
    )
      .thenReturn(actorProbe.ref.path)

    when(
      actorIdMock
        .personalContactRepositoryPath(
          any[ActorRef],
          anyString()
        )
    )
      .thenReturn(actorProbe.ref.path)

    when(repo.phoneNumberForUser("alafa")).thenReturn(Some("1000"))

    when(
      configServerRequester.setUserServices(any[Long], any[PartialUserServices])
    )
      .thenReturn(Future.successful(any[PartialUserServices]))

    def getCtrl: WsApiV2 = app.injector.instanceOf[WsApiV2]

    val user: WebServiceUser =
      WebServiceUser(login = "web", password = "web", name = "webservice")
    when(repo.getWebServiceUser("web")).thenReturn(Some(user))
    val expires   = 54000
    val now: Long = new DateTime().getMillis / 1000
    val token: AuthenticationInformation =
      AuthenticationInformation(
        user.login,
        now + expires,
        now,
        "webservice",
        List(
          "xuc.rest.dial.toqueue.create",
          "xuc.rest.dial.number.*.create",
          "xuc.rest.dial.user.*.create",
          "xuc.rest.dial.fromqueue.*.create",
          "xuc.rest.dnd.*.create",
          "xuc.rest.forward.unconditional.*.create",
          "xuc.rest.forward.noanswer.*.create",
          "xuc.rest.forward.busy.*.create",
          "xuc.rest.history.*.read",
          "xuc.rest.history.days.*.read",
          "xuc.rest.callback_lists.callback_requests.csv.create",
          "xuc.rest.callback_lists.callback_tickets.csv.create"
        ),
        None,
        None
      )
    val fakeAuth: FakeHeaders = FakeHeaders(
      Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
    )
  }

  "WsApiV2 Controller" should {

    "add a caller into a queue" in new Helper {
      val queueDialRaw: String =
        """|{
           |  "destination": "0123456789",
           |  "queueName": "std_notaire",
           |  "callerIdNumber": "0472727272",
           |  "variables": {
           |    "varname1": "varval1",
           |    "varname2": "varval2"
           |  }
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/dial/toqueue",
        fakeAuth,
        AnyContentAsJson(Json.parse(queueDialRaw))
      )
      val ctrl: Future[Result] = call(getCtrl.dialToQueue(), rq)

      status(ctrl) shouldBe OK
      actorProbe.expectMsg(
        DialToQueue(
          "0123456789",
          "std_notaire",
          "0472727272",
          Map("varname1" -> "varval1", "varname2" -> "varval2")
        )
      )
    }

    "dial from an user to an extension" in new Helper {
      val dialRaw: String =
        """|{
           |  "destination": "0123456789",
           |  "variables": {
           |    "varname1": "varval1",
           |    "varname2": "varval2"
           |  }
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/dial/number/jbond",
        fakeAuth,
        AnyContentAsJson(Json.parse(dialRaw))
      )
      val ctrl: Future[Result] = call(getCtrl.dialNumber("jbond"), rq)

      status(ctrl) shouldBe OK
      actorProbe.expectMsg(
        BaseRequest(
          system.deadLetters,
          Dial(
            "0123456789",
            Map("varname1" -> "varval1", "varname2" -> "varval2")
          )
        )
      )
    }

    "dial by username from an user to another user" in new Helper {
      val dialByUsernameRaw: String =
        """|{
           |  "username": "alafa",
           |  "variables": {
           |    "varname1": "varval1",
           |    "varname2": "varval2"
           |  }
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/dial/user/jbond",
        fakeAuth,
        AnyContentAsJson(Json.parse(dialByUsernameRaw))
      )
      val ctrl: Future[Result] = call(getCtrl.dialUser("jbond"), rq)

      status(ctrl) shouldBe OK
      actorProbe.expectMsg(
        BaseRequest(
          system.deadLetters,
          Dial("1000", Map("varname1" -> "varval1", "varname2" -> "varval2"))
        )
      )
    }

    "dial an extension from a queue" in new Helper {
      val dialFromQueue: String =
        """|{
           |  "destination": "0123456789",
           |  "queueId": 1
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/dial/fromqueue/jbond",
        fakeAuth,
        AnyContentAsJson(Json.parse(dialFromQueue))
      )
      val ctrl: Future[Result] = call(getCtrl.dialFromQueue("jbond"), rq)

      status(ctrl) shouldBe OK
      actorProbe.expectMsg(
        DialFromQueue("0123456789", 1, "", "", Map.empty, "default")
      )
    }

    "set an unconditional forwarding for an user" in new Helper {
      val forwarding: String =
        """|{
           |  "destination": "0123456789",
           |  "enabled": true
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/forward/unconditional/jbond",
        fakeAuth,
        AnyContentAsJson(Json.parse(forwarding))
      )

      when(xivoUserDao.getCtiUserByLogin("jbond"))
        .thenReturn(
          Future.successful(
            XivoUser(
              1,
              None,
              None,
              "jbond",
              None,
              Some("jbond"),
              None,
              None,
              None
            )
          )
        )

      val ctrl: Future[Result] = call(getCtrl.uncForward("jbond"), rq)

      status(ctrl) shouldBe OK
    }

    "set a noanswer forwarding for an user" in new Helper {
      val forwarding: String =
        """|{
           |  "destination": "0123456789",
           |  "enabled": true
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/forward/noanswer/jbond",
        fakeAuth,
        AnyContentAsJson(Json.parse(forwarding))
      )

      when(xivoUserDao.getCtiUserByLogin("jbond"))
        .thenReturn(
          Future.successful(
            XivoUser(
              1,
              None,
              None,
              "jbond",
              None,
              Some("jbond"),
              None,
              None,
              None
            )
          )
        )

      val ctrl: Future[Result] = call(getCtrl.naForward("jbond"), rq)

      status(ctrl) shouldBe OK
    }

    "set a busy forwarding for an user" in new Helper {
      val forwarding: String =
        """|{
           |  "destination": "0123456789",
           |  "enabled": true
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/forward/busy/jbond",
        fakeAuth,
        AnyContentAsJson(Json.parse(forwarding))
      )

      when(xivoUserDao.getCtiUserByLogin("jbond"))
        .thenReturn(
          Future.successful(
            XivoUser(
              1,
              None,
              None,
              "jbond",
              None,
              Some("jbond"),
              None,
              None,
              None
            )
          )
        )

      val ctrl: Future[Result] = call(getCtrl.busyForward("jbond"), rq)

      status(ctrl) shouldBe OK
    }

    "set an user to do not disturb" in new Helper {
      val forwarding: String =
        """|{
           |  "state": true
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/dnd/jbond",
        fakeAuth,
        AnyContentAsJson(Json.parse(forwarding))
      )

      when(xivoUserDao.getCtiUserByLogin("jbond"))
        .thenReturn(
          Future.successful(
            XivoUser(
              1,
              None,
              None,
              "jbond",
              None,
              Some("jbond"),
              None,
              None,
              None
            )
          )
        )

      val ctrl: Future[Result] = call(getCtrl.dnd("jbond"), rq)

      status(ctrl) shouldBe OK
    }

    "get a call history by username" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        "/xuc/api/2.0/history/jbond?size=10",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(repo.interfaceFromUsername("jbond"))
        .thenReturn(Some("abcd"))

      val ctrl: Future[Result] =
        call(getCtrl.getUserCallHistory("jbond", 10), rq)

      status(ctrl) shouldBe OK
    }

    "get a call history by days" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        "/xuc/api/2.0/history/days/jbond?days=10",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(repo.interfaceFromUsername("jbond"))
        .thenReturn(Some("abcd"))

      val ctrl: Future[Result] =
        call(getCtrl.getUserCallHistory("jbond", 10), rq)

      status(ctrl) shouldBe OK
    }

    "do a callback import request" in new Helper {
      val callbackImportRequest: String =
        """|{
           |  "listUuid": "1",
           |  "csv": "foo|bar|bat"
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/callback_lists/callback_requests/csv",
        fakeAuth,
        AnyContentAsJson(Json.parse(callbackImportRequest))
      )

      val ctrl: Future[Result] = call(getCtrl.importRequests(), rq)

      status(ctrl) shouldBe OK
    }

    "do an export ticket request" in new Helper {
      val exportTicketRequest: String =
        """|{
           |  "listUuid": "1"
           |}""".stripMargin

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest(
        "POST",
        "/xuc/api/2.0/callback_lists/callback_tickets/csv",
        fakeAuth,
        AnyContentAsJson(Json.parse(exportTicketRequest))
      )

      val ctrl: Future[Result] = call(getCtrl.exportTickets(), rq)

      status(ctrl) shouldBe OK
    }
  }
}
