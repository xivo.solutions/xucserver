package controllers.xuc

import models.authentication.AuthenticationProvider
import models.ws.auth.*
import models.{AuthenticatedUser, Token, XivoUser}
import org.apache.pekko.stream.Materializer
import org.joda.time.DateTime
import org.mockito.Mockito.{atLeastOnce, reset, spy, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.{Application, Configuration}
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc.*
import play.api.test.*
import play.api.test.Helpers.*
import play.api.{Application, Configuration}
import services.auth.WebService
import services.calltracking.SipDriver
import services.config.ConfigRepository
import xivo.models.{Line, LineHelper}
import xivo.xuc.XucBaseConfig
import xivo.xucami.models.CallerId
import xuctest.BaseTest

import scala.concurrent.Future

class AuthenticationControllerSpec
    extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {

  implicit lazy val mat: Materializer      = app.materializer
  val repo: ConfigRepository               = mock[ConfigRepository]
  val authProvider: AuthenticationProvider = mock[AuthenticationProvider]
  val config: XucBaseConfig                = mock[XucBaseConfig]
  val webService: WebService               = mock[WebService]

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(
        Map(
          "authentication.expires"               -> 99999,
          "authentication.webservice.maxExpires" -> 99999
        )
      )
    )
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[AuthenticationProvider].to(authProvider))
      .overrides(bind[WebService].to(webService))
      .build()

  def getCtrl: AuthenticationController =
    app.injector.instanceOf[AuthenticationController]

  class Helper {
    val authToken: Token = Token(
      "adeblouse",
      new DateTime(1680011247),
      new DateTime(1679924847),
      "webservice",
      None,
      List("..read")
    )

    val jwtToken =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6ImFkZWJsb3VzZSIsImV4cGlyZXNBdCI6MTY4MDAxMTI0NywiaXN1ZWRBdCI6MTY3OTkyNDg0NywidXNlclR5cGUiOiJ3ZWJzZXJ2aWNlIiwiYWNscyI6WyJhbGlhcy5jdGl1c2VyIl19.YuRBDpy_Amtwz_E8CPbdXT3GNg3BlOzjRALHe3E5R7s"

    val authenticationInformation: AuthenticationInformation =
      AuthenticationInformation(
        "adeblouse",
        1680011247,
        1679924847,
        "webservice",
        List("alias.ctiuser"),
        None,
        None
      )

    when(webService.getAuthenticationInformation("adeblouse", authToken))
      .thenReturn(authenticationInformation)
    when(webService.encodeToJWT(authenticationInformation))
      .thenReturn(jwtToken)

  }

  "AuthenticationController" should {
    "allow user to login" in {
      val authUser = mock[AuthenticatedUser]
      when(authProvider.authenticate("jbond", "mypass"))
        .thenReturn(Some(authUser))

      reset(repo)
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      import LineHelper.makeLine
      when(repo.getLineForUser("jbond")).thenReturn(
        Some(makeLine(1, "default", "sip", "abcde", None, None, "ip"))
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "jbond", "password":"mypass", "applicationType": "uc"}"""
        )
      )
      val spyCtrl = spy(getCtrl)
      val ctrl    = call(spyCtrl.login(), rq)

      status(ctrl) shouldBe OK
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "login").as[String] shouldBe "jbond"

      verify(spyCtrl, atLeastOnce()).logLoginType(
        "jbond",
        AuthType.basic,
        SoftwareType.unknown,
        ApplicationType.uc
      )
    }

    "disallow user to login" in {
      when(authProvider.authenticate("jbond", "mywrongpassword"))
        .thenReturn(None)
      reset(repo)
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse("""{"login": "jbond", "password":"mywrongpassword"}""")
      )
      val ctrl = call(getCtrl.login(), rq)

      status(ctrl) shouldBe UNAUTHORIZED
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe "InvalidCredentials"
    }

    "disallow xuc user to login" in {
      when(authProvider.authenticate("jbond", "mywrongpassword"))
        .thenReturn(None)
      reset(repo)
      when(repo.getCtiUser("xuc")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "Xuc",
            Some("Technical"),
            Some("xuc"),
            Some("xucpass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse("""{"login": "xuc", "password":"xucpass"}""")
      )
      val ctrl = call(getCtrl.login(), rq)

      status(ctrl) shouldBe UNAUTHORIZED
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe "InvalidCredentials"
    }

    "disallow mobile software type to login if mobile config is not ok" in {
      val authUser = mock[AuthenticatedUser]
      when(authProvider.authenticate("jbond", "mypass"))
        .thenReturn(Some(authUser))
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "jbond", "password":"mypass", "softwareType":"mobile"}"""
        )
      )

      import LineHelper.makeLine
      when(repo.getLineForUser("jbond")).thenReturn(
        Some(makeLine(1, "default", "sip", "abcde", None, None, "ip"))
      )

      val ctrl = call(getCtrl.login(), rq)

      status(ctrl) shouldBe FORBIDDEN
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe "WrongMobileSetup"
    }

    "allow mobile software type to login if mobile config is ok" in {
      val authUser = mock[AuthenticatedUser]
      when(authProvider.authenticate("jbond", "mypass"))
        .thenReturn(Some(authUser))
      when(repo.mobileConfigIsValid).thenReturn(true)
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "jbond", "password":"mypass", "softwareType":"mobile"}"""
        )
      )

      import LineHelper.makeLine
      when(repo.getLineForUser("jbond")).thenReturn(
        Some(makeLine(1, "default", "sip", "abcde", None, None, "ip"))
      )

      val ctrl = call(getCtrl.login(), rq)
      status(ctrl) shouldBe OK
    }

    "check username upon login" in {
      reset(repo)
      when(repo.getCtiUser("otheruser")).thenReturn(None)

      val rq = FakeRequest().withJsonBody(
        Json.parse("""{"login": "otheruser", "password":"mypass"}""")
      )
      val ctrl = call(getCtrl.login(), rq)

      status(ctrl) shouldBe UNAUTHORIZED
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe "InvalidCredentials"
    }

    "check username is not empty" in {
      reset(repo)
      val rq = FakeRequest().withJsonBody(
        Json.parse("""{"login": "", "password":"mypass"}""")
      )
      val ctrl = call(getCtrl.login(), rq)

      status(ctrl) shouldBe BAD_REQUEST
      val jsonResult = contentAsJson(ctrl)
      (jsonResult \ "error").as[String] shouldBe "EmptyLogin"
    }

    "validate credentials and check api can be called with ACL cti aliases" in {
      reset(repo)
      val authUser = mock[AuthenticatedUser]
      when(authProvider.authenticate("jbond", "mypass"))
        .thenReturn(Some(authUser))
      when(repo.getCtiUser("jbond")).thenReturn(
        Some(
          XivoUser(
            1,
            None,
            None,
            "James",
            Some("Bond"),
            Some("jbond"),
            Some("mypass"),
            None,
            None
          )
        )
      )

      val rq = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "jbond", "password":"mypass", "softwareType":"webBrowser"}"""
        )
      )

      import LineHelper.makeLine
      when(repo.getLineForUser("jbond")).thenReturn(
        Some(makeLine(1, "default", "sip", "abcde", None, None, "ip"))
      )

      val loginCtrl = call(getCtrl.login(), rq)

      val jsonResult = contentAsJson(loginCtrl)
      val token      = (jsonResult \ "token").as[String]
      val rq1 =
        FakeRequest("GET", "/xuc/api/2.0/contact/personal").withHeaders(
          ("Authorization", "Bearer " + token)
        )

      val checkCtrl1 = call(getCtrl.check(), rq1)
      status(checkCtrl1) shouldBe OK

      val rq2 =
        FakeRequest("GET", "/xuc/api/2.0/contact/export/personal").withHeaders(
          ("Authorization", "Bearer " + token)
        )

      val checkCtrl2 = call(getCtrl.check(), rq2)
      status(checkCtrl2) shouldBe OK

      val rq3 =
        FakeRequest("GET", "/xuc/api/2.0/config/meetingrooms/personal/1")
          .withHeaders(
            ("Authorization", "Bearer " + token)
          )

      val checkCtrl3 = call(getCtrl.check(), rq3)
      status(checkCtrl3) shouldBe OK

      val rq4 =
        FakeRequest("GET", "/xuc/api/2.0/dial/xivo/username/").withHeaders(
          ("Authorization", "Bearer " + token)
        )

      val checkCtrl4 = call(getCtrl.check(), rq4)
      status(checkCtrl4) shouldBe FORBIDDEN
    }

    "log LoginType" in {
      val line = Line(
        0,
        "default",
        "",
        "ato",
        None,
        None,
        "",
        webRTC = true,
        None,
        None,
        ua = false,
        CallerId("", ""),
        SipDriver.SIP
      )
      LineType.lineToLineType(line).shouldBe(LineType.webRTC)

      val line2 = Line(
        0,
        "default",
        "",
        "ato",
        None,
        None,
        "",
        webRTC = true,
        None,
        None,
        ua = true,
        CallerId("", ""),
        SipDriver.SIP
      )
      LineType.lineToLineType(line2).shouldBe(LineType.ua)
    }

    "allow web service user to get JWT token with expiration time set to 3s" in new Helper {
      when(webService.authenticate("adeblouse", "rightpassword", 3))
        .thenReturn(Future.successful(authToken))

      val rq: FakeRequest[AnyContentAsJson] = FakeRequest().withJsonBody(
        Json.parse(
          """{"login": "adeblouse", "password":"rightpassword", "expiration": 3}"""
        )
      )

      val ctrl: Future[Result] = call(getCtrl.webService(), rq)
      status(ctrl) shouldBe OK
      contentAsString(
        ctrl
      ) shouldBe s"""{"login":"adeblouse","token":"$jwtToken","TTL":3}"""

    }
  }

  "throw a error message if no login is provided" in new Helper {
    when(webService.authenticate("", "rightpassword", 3600))
      .thenReturn(
        Future.failed(
          new AuthenticationException(
            AuthenticationError.InvalidCredentials,
            "Invalid credentials"
          )
        )
      )

    val rq: FakeRequest[AnyContentAsJson] = FakeRequest().withJsonBody(
      Json.parse(
        """{"login": "", "password":"rightpassword", "expiration": 3600}"""
      )
    )

    val ctrl: Future[Result] = call(getCtrl.webService(), rq)
    status(ctrl) shouldBe UNAUTHORIZED
    contentAsString(
      ctrl
    ) shouldBe """{"error":"InvalidCredentials","message":"Invalid credentials"}"""
  }

  "throw a error message if credentials are invalid" in new Helper {
    when(webService.authenticate("webservice", "wrongpassword", 3))
      .thenReturn(
        Future.failed(
          new AuthenticationException(
            AuthenticationError.InvalidCredentials,
            "Invalid credentials"
          )
        )
      )

    val rq: FakeRequest[AnyContentAsJson] = FakeRequest().withJsonBody(
      Json.parse(
        """{"login": "webservice", "password":"wrongpassword", "expiration": 3}"""
      )
    )

    val ctrl: Future[Result] = call(getCtrl.webService(), rq)
    status(ctrl) shouldBe UNAUTHORIZED
    contentAsString(
      ctrl
    ) shouldBe """{"error":"InvalidCredentials","message":"Invalid credentials"}"""
  }

  "allow web service user to get JWT token with default expiration time" in new Helper {
    when(webService.authenticate("webservice", "rightpassword", 86400))
      .thenReturn(Future.successful(authToken))
    when(webService.getAuthenticationInformation("webservice", authToken))
      .thenReturn(authenticationInformation)
    when(webService.encodeToJWT(authenticationInformation))
      .thenReturn(jwtToken)

    val rq: FakeRequest[AnyContentAsJson] = FakeRequest().withJsonBody(
      Json.parse(
        """{"login": "webservice", "password":"rightpassword"}"""
      )
    )

    val ctrl: Future[Result] = call(getCtrl.webService(), rq)
    status(ctrl) shouldBe OK
    contentAsString(
      ctrl
    ) shouldBe s"""{"login":"webservice","token":"$jwtToken","TTL":86400}"""
  }

  "get the correct default expiry for cti users" in new Helper {
    val ctrl: AuthenticationController = getCtrl
    ctrl.getExpiry(99999, AuthUserType.Cti.toString) shouldBe 99999
  }

  "get the correct expiry in range for cti users" in new Helper {
    val ctrl: AuthenticationController = getCtrl
    ctrl.getExpiry(15000, AuthUserType.Cti.toString) shouldBe 15000
  }

  "get the correct expiry if out of range for cti users" in new Helper {
    val ctrl: AuthenticationController = getCtrl
    ctrl.getExpiry(123456, AuthUserType.Cti.toString) shouldBe 99999
  }

  "get the correct default expiry for webservice users" in new Helper {
    val ctrl: AuthenticationController = getCtrl
    ctrl.getExpiry(99999, AuthUserType.Webservice.toString) shouldBe 99999
  }

  "get the correct expiry in range for webservice users" in new Helper {
    val ctrl: AuthenticationController = getCtrl
    ctrl.getExpiry(15000, AuthUserType.Webservice.toString) shouldBe 15000
  }

  "get the correct expiry if out of range for webservice users" in new Helper {
    val ctrl: AuthenticationController = getCtrl
    ctrl.getExpiry(123456, AuthUserType.Webservice.toString) shouldBe 99999
  }
}
