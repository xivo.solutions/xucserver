package controllers.xuc

import models.ws.auth.ApplicationType.ccagent
import org.apache.pekko.actor.{Actor, ActorRef, ActorSystem, Props}
import org.apache.pekko.stream.Materializer
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import models.{GetLineConfigInUse, LineConfig, XivoUser, XivoUserDao, XucUser}
import models.ws.auth.{AuthenticationInformation, SoftwareType}
import org.joda.time.DateTime
import org.mockito.ArgumentMatchers.{any, anyString}
import org.mockito.Mockito.*
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.mvc.*
import play.api.test.Helpers.*
import play.api.test.*
import services.{ActorIdsFactory, GetRouter, Router}
import services.callhistory.CallHistoryManager
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models.{Agent, AgentQueueMember, Line, PartialUserServices}
import xuctest.BaseTest

import scala.concurrent.Future
import play.api.Application
import services.calltracking.SipDriver
import services.config.ConfigDispatcher.LineConfigQueryById
import xivo.xucami.models.CallerId

class FakeCtiRouterFactory(fakeUser: XivoUser, fakeCtiRouterRef: ActorRef) extends Actor {

  override def receive: Receive =
    case _: GetRouter =>
      sender() ! Router(XucUser(fakeUser.username.get, fakeUser), fakeCtiRouterRef)

}

class FakeCtiRouter(var fakeLineConfig: Option[LineConfig]) extends Actor {

  def setFakeLineConfig(lc: Option[LineConfig]): Unit = fakeLineConfig = lc

  override def receive: Receive =
    case GetLineConfigInUse =>
      sender() ! fakeLineConfig
}

class CtiApiSpec
  extends BaseTest
    with Results
    with GuiceOneAppPerSuite
    with MockitoSugar {

  implicit val repo: ConfigRepository = mock[ConfigRepository]
  implicit lazy val mat: Materializer = app.materializer
  implicit val system: ActorSystem = ActorSystem()
  implicit val xivoUserDao: XivoUserDao = mock[XivoUserDao]
  implicit val configServerRequester: ConfigServerRequester =
    mock[ConfigServerRequester]
  implicit val callHistoryManager: CallHistoryManager = mock[CallHistoryManager]
  val actorIdMock: ActorIdsFactory = mock[ActorIdsFactory]

  val fakeUser = XivoUser(5, Some(2L), None, "ctiuser", None, Some("ctiuser"), None, None, None)
  val ctiRouter = TestActorRef[FakeCtiRouter](Props(new FakeCtiRouter(Some(LineConfig("3", "3000", Some(Line(1, "default", "PJSIP", "linename", None, None, "127.0.0.1",
    true, None, None, true, CallerId("toto", "1000"), SipDriver.PJSIP, false)))))))
  val ctiRouterFactory = TestActorRef[FakeCtiRouterFactory](Props(new FakeCtiRouterFactory(fakeUser, ctiRouter)))

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ActorSystem].to(system))
      .overrides(bind[ActorRef].qualifiedWith("CtiRouterFactory").toInstance(ctiRouterFactory))
      .overrides(bind[ConfigRepository].to(repo))
      .overrides(bind[ActorIdsFactory].to(actorIdMock))
      .overrides(bind[XivoUserDao].to(xivoUserDao))
      .overrides(bind[ConfigServerRequester].to(configServerRequester))
      .overrides(bind[CallHistoryManager].to(callHistoryManager))
      .build()

  class Helper {
    val agentQueueMembers = List(AgentQueueMember(2, 1, 1), AgentQueueMember(2, 2, 1), AgentQueueMember(4, 1, 2))
    when(repo.getAgentQueueMembers()).thenReturn(agentQueueMembers)
    val actorProbe: TestProbe = TestProbe()
    when(
      actorIdMock
        .queueDispatcherPath(
          any[ActorRef]
        )
    )
      .thenReturn(actorProbe.ref.path)

    when(
      actorIdMock
        .ctiRouterPath(
          any[ActorRef],
          anyString()
        )
    )
      .thenReturn(actorProbe.ref.path)

    when(
      actorIdMock
        .personalContactRepositoryPath(
          any[ActorRef],
          anyString()
        )
    )
      .thenReturn(actorProbe.ref.path)

    when(repo.phoneNumberForUser("alafa")).thenReturn(Some("1000"))
    when(repo.configServerRequester).thenReturn(
      configServerRequester
    )

    when(
      configServerRequester.setUserServices(any[Long], any[PartialUserServices])
    )
      .thenReturn(Future.successful(any[PartialUserServices]))

    def getCtrl: CtiApi = app.injector.instanceOf[CtiApi]

    val expires = 54000
    val now: Long = new DateTime().getMillis / 1000
    val token: AuthenticationInformation =
      AuthenticationInformation(
        "ctiuser",
        now + expires,
        now,
        "cti",
        List(
          "alias.ctiuser"
        ),
        Some(SoftwareType.webBrowser),
        Some(ccagent)
      )
    val fakeAuth: FakeHeaders = FakeHeaders(
      Seq(("Authorization", "Bearer " + token.encode(authenticationSecret)))
    )
  }

  "CtiApiSpec Controller" should {

    "get an agent config" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        s"/xuc/api/2.0/agent",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(repo.getCtiUser("ctiuser"))
        .thenReturn(Some(XivoUser(5, Some(2L), None, "ctiuser", None, Some("ctiuser"), None, None, None)))
      when(repo.getAgent(2L))
        .thenReturn(Some(Agent(2, "fname", "lname", "0123", "default", 3L, 5)))

      val ctrl: Future[Result] =
        call(getCtrl.getAgentConfig(), rq)

      status(ctrl) shouldBe OK
      contentAsString(ctrl).shouldBe(s"""{"res":"success","data":{"id":2,"firstName":"fname","lastName":"lname","number":"0123","context":"default","groupId":3,"userId":5}}""")
    }

    "try to get a non existing agent config" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        s"/xuc/api/2.0/agent",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(repo.getCtiUser("ctiuser"))
        .thenReturn(Some(XivoUser(5, Some(2L), None, "ctiuser", None, Some("ctiuser"), None, None, None)))
      when(repo.getAgent(2L))
        .thenReturn(None)

      val ctrl: Future[Result] =
        call(getCtrl.getAgentConfig(), rq)

      status(ctrl) shouldBe OK
      contentAsString(ctrl).shouldBe(s"""{"res":"success"}""")
    }

    "get an agent in-use line config" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        s"/xuc/api/2.0/agent/line",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(repo.getCtiUser("ctiuser"))
        .thenReturn(Some(XivoUser(5, Some(2L), None, "ctiuser", None, Some("ctiuser"), None, None, None)))

      val ctrl: Future[Result] =
        call(getCtrl.getAgentLineConfig(), rq)

      status(ctrl) shouldBe OK
      contentAsString(ctrl).shouldBe(s"""{"res":"success","data":{"id":"3","name":"linename","xivoIp":"127.0.0.1","hasDevice":false,"isUa":true,"webRtc":true,"vendor":null,"number":"3000","mobileApp":false,"password":"","sipProxyName":"default"}}""")
    }

    "try to get a agent in-use line config while an agent is not using a line" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        s"/xuc/api/2.0/agent/line",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(repo.getCtiUser("ctiuser"))
        .thenReturn(Some(XivoUser(5, Some(2L), None, "ctiuser", None, Some("ctiuser"), None, None, None)))

      ctiRouter.underlyingActor.setFakeLineConfig(None)

      val ctrl: Future[Result] =
        call(getCtrl.getAgentLineConfig(), rq)

      status(ctrl) shouldBe OK
      contentAsString(ctrl).shouldBe(s"""{"res":"success"}""")
    }

    "get an user line config" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        s"/xuc/api/2.0/user/line",
        fakeAuth,
        AnyContentAsEmpty
      )


      when(repo.getCtiUser("ctiuser"))
        .thenReturn(Some(XivoUser(5, Some(2L), None, "ctiuser", None, Some("ctiuser"), None, None, None)))
      when(repo.getLineConfig(LineConfigQueryById(5)))
        .thenReturn(Some(LineConfig("3", "3000", Some(Line(1, "default", "PJSIP", "linename", None, None, "127.0.0.1",
          true, None, None, true, CallerId("toto", "1000"), SipDriver.PJSIP, false)))))

      val ctrl: Future[Result] =
        call(getCtrl.getLineConfig(), rq)

      status(ctrl) shouldBe OK
      contentAsString(ctrl).shouldBe(s"""{"res":"success","data":{"id":"3","name":"linename","xivoIp":"127.0.0.1","hasDevice":false,"isUa":true,"webRtc":true,"vendor":null,"number":"3000","mobileApp":false,"password":"","sipProxyName":"default"}}""")
    }

    "try to get a non existing user line config" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        s"/xuc/api/2.0/user/line",
        fakeAuth,
        AnyContentAsEmpty
      )


      when(repo.getCtiUser("ctiuser"))
        .thenReturn(Some(XivoUser(5, Some(2L), None, "ctiuser", None, Some("ctiuser"), None, None, None)))
      when(repo.getLineConfig(LineConfigQueryById(5)))
        .thenReturn(None)

      val ctrl: Future[Result] =
        call(getCtrl.getLineConfig(), rq)

      status(ctrl) shouldBe OK
      contentAsString(ctrl).shouldBe(s"""{"res":"success"}""")
    }

    "get a list of queues corresponding to the requested agent id" in new Helper {
      val rq: FakeRequest[AnyContentAsEmpty.type] = FakeRequest(
        "GET",
        s"/xuc/api/2.0/agent/queue_members",
        fakeAuth,
        AnyContentAsEmpty
      )

      when(repo.getCtiUser("ctiuser"))
        .thenReturn(Some(XivoUser(5, Some(2L), None, "ctiuser", None, Some("ctiuser"), None, None, None)))

      val ctrl: Future[Result] =
        call(getCtrl.getAgentQueueMembers(), rq)

      status(ctrl) shouldBe OK
      contentAsString(ctrl).shouldBe(s"""{"res":"success","data":[{"agentId":2,"queueId":1,"penalty":1},{"agentId":2,"queueId":2,"penalty":1}]}""")
    }
  }
}