package services.userPreference

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestKit, TestProbe}
import pekkotest.TestKitSpec
import models.{XivoUser, XucUser}
import org.mockito.Mockito.{timeout, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import services.ClientConnected
import services.config.{ConfigRepository, ConfigServerRequester}
import services.request.*
import xivo.models.{UserPreference, UserPreferenceKey}
import xivo.network.LoggedOn
import xivo.websocket.WebSocketEvent
import xivo.websocket.WsBus.WsContent
import xuctest.XucUserHelper

import scala.concurrent.Future

class UserPreferenceServiceSpec
    extends TestKitSpec("UserPreferenceServiceSpec")
    with MockitoSugar
    with XucUserHelper {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class Helper {
    val configRepo: ConfigRepository = mock[ConfigRepository]
    val configServerRequester: ConfigServerRequester =
      mock[ConfigServerRequester]

    def actor(
    ): (TestActorRef[UserPreferenceService], UserPreferenceService) = {

      val a = TestActorRef[UserPreferenceService](
        Props(new UserPreferenceService(configRepo))
      )
      (a, a.underlyingActor)
    }
  }

  "UserPreference Service" should {
    "Forward SetUserPreference message to configRequester" in new Helper {
      val (ref, _)                 = actor()
      val ctiRouterMock: TestProbe = TestProbe()
      when(configRepo.configServerRequester).thenReturn(
        configServerRequester
      )

      when(
        configServerRequester.getUserPreferences(
          42
        )
      ).thenReturn(
        Future.successful(
          List(UserPreference(42, "PREFERRED_DEVICE", "Webrtc", "String"))
        )
      )

      when(
        configServerRequester.setUserPreference(
          UserPreference(42, "PREFERRED_DEVICE", "TypePhoneDevice", "String")
        )
      )
        .thenReturn(Future.unit)

      ref.tell(
        SetUserPreferenceRequest(
          Some(42),
          "PREFERRED_DEVICE",
          "TypePhoneDevice",
          "String"
        ),
        ctiRouterMock.ref
      )

      verify(configServerRequester, timeout(500))
        .setUserPreference(
          UserPreference(42, "PREFERRED_DEVICE", "TypePhoneDevice", "String")
        )
    }

    "Request all user preferences on UserPreference init and send it to the ctiRouter" in new Helper {
      val (ref, _) = actor()
      val userId   = 1L
      when(configRepo.configServerRequester).thenReturn(
        configServerRequester
      )

      val ctiMockRef: TestProbe = TestProbe()

      when(
        configServerRequester.getUserPreferences(
          42
        )
      ).thenReturn(
        Future.successful(
          List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
        )
      )

      ref ! UserPreferencesInit(
        Some(42L),
        ctiMockRef.ref
      )

      verify(configServerRequester, timeout(500))
        .getUserPreferences(
          42
        )

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
        )
      )
    }

    "Return userPreferences from cache if it's there's already an active connection" in new Helper {
      val (ref, _) = actor()
      val userId   = 42L
      when(configRepo.configServerRequester).thenReturn(
        configServerRequester
      )

      val ctiMockRef: TestProbe = TestProbe()

      when(
        configServerRequester.getUserPreferences(
          userId
        )
      ).thenReturn(
        Future.successful(
          List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
        ),
        Future.failed(new Exception("Should not happen"))
      )

      ref ! UserPreferencesInit(
        Some(userId),
        ctiMockRef.ref
      )

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
        )
      )

      ref ! UserPreferencesInit(
        Some(userId),
        ctiMockRef.ref
      )

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
        )
      )
    }

    "Request all user preferences on client connected and send it to the WSActor" in new Helper {
      val (ref, _)               = actor()
      val user: XucUser          = getXucUser("bob", "pwd")
      val userId: Long           = user.xivoUser.id
      val wsActorMock: TestProbe = TestProbe()

      when(configRepo.configServerRequester).thenReturn(
        configServerRequester
      )

      when(
        configServerRequester.getUserPreferences(
          userId
        )
      ).thenReturn(
        Future.successful(
          List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
        )
      )

      ref ! ClientConnected(
        wsActorMock.ref,
        LoggedOn(user, userId.toString)
      )

      verify(configServerRequester, timeout(500))
        .getUserPreferences(
          userId
        )

      wsActorMock.expectMsg(
        WsContent(
          WebSocketEvent.createUserPreferenceEvent(
            List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
          )
        )
      )
    }

    "Remove user from the local cache when disconnected" in new Helper {
      val (ref, _) = actor()
      val userId   = 42L
      when(configRepo.configServerRequester).thenReturn(
        configServerRequester
      )

      val ctiMockRef: TestProbe = TestProbe()

      when(
        configServerRequester.getUserPreferences(
          userId
        )
      ).thenReturn(
        Future.successful(
          List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
        ),
        Future.successful(
          List(UserPreference(userId, "PREFERRED_DEVICE", "webrtc", "String"))
        )
      )

      ref ! UserPreferencesInit(
        Some(userId),
        ctiMockRef.ref
      )

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(UserPreference(userId, "PREFERRED_DEVICE", "phone", "String"))
        )
      )

      ref ! UserPreferenceDisconnect(Some(userId))
      ref ! UserPreferencesInit(
        Some(userId),
        ctiMockRef.ref
      )

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(UserPreference(userId, "PREFERRED_DEVICE", "webrtc", "String"))
        )
      )
    }

    "Update user preference on change and forward the info" in new Helper {
      val (ref, _) = actor()
      val userId   = 42L
      when(configRepo.configServerRequester).thenReturn(
        configServerRequester
      )

      val ctiMockRef: TestProbe = TestProbe()

      when(
        configServerRequester.getUserPreferences(
          userId
        )
      ).thenReturn(
        Future.successful(
          List()
        ),
        Future.successful(
          List(
            UserPreference(
              userId,
              UserPreferenceKey.MobileAppInfo,
              "true",
              "String"
            )
          )
        )
      )

      ref ! UserPreferencesInit(
        Some(userId),
        ctiMockRef.ref
      )

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List()
        )
      )

      ref ! UserPreferenceCreated(userId)

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(
            UserPreference(
              userId,
              UserPreferenceKey.MobileAppInfo,
              "true",
              "String"
            )
          )
        )
      )
    }

    "Doesn't send lineConfig if mobileAppInfo doesn't change" in new Helper {
      val (ref, _) = actor()
      val userId   = 42L
      when(configRepo.configServerRequester).thenReturn(
        configServerRequester
      )

      val ctiMockRef: TestProbe = TestProbe()

      when(
        configServerRequester.getUserPreferences(
          userId
        )
      ).thenReturn(
        Future.successful(
          List(
            UserPreference(
              userId,
              UserPreferenceKey.MobileAppInfo,
              "true",
              "String"
            )
          )
        ),
        Future.successful(
          List(
            UserPreference(
              userId,
              UserPreferenceKey.MobileAppInfo,
              "true",
              "String"
            )
          )
        )
      )

      ref ! UserPreferencesInit(
        Some(userId),
        ctiMockRef.ref
      )

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(
            UserPreference(
              userId,
              UserPreferenceKey.MobileAppInfo,
              "true",
              "String"
            )
          )
        )
      )

      ref ! UserPreferenceCreated(userId)

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(
            UserPreference(
              userId,
              UserPreferenceKey.MobileAppInfo,
              "true",
              "String"
            )
          )
        )
      )
      ctiMockRef.expectNoMessage()
    }

    "Unregister mobile app notification token from user preferences" in new Helper {
      val (ref, _)                 = actor()
      val userId                   = 42L
      val ctiRouterMock: TestProbe = TestProbe()

      when(configRepo.configServerRequester).thenReturn(configServerRequester)
      when(configRepo.getCtiUser("user"))
        .thenReturn(
          Some(XivoUser(42, None, None, "user", None, None, None, None, None))
        )

      when(configServerRequester.getUserPreferences(42))
        .thenReturn(
          Future.successful(
            List(
              UserPreference(
                42,
                UserPreferenceKey.MobileAppInfo,
                "false",
                "String"
              )
            )
          )
        )
      val ctiMockRef: TestProbe = TestProbe()

      ref ! UserPreferencesInit(
        Some(userId),
        ctiMockRef.ref
      )

      ctiMockRef.expectMsg(
        WebSocketEvent.createUserPreferenceEvent(
          List(
            UserPreference(
              userId,
              UserPreferenceKey.MobileAppInfo,
              "false",
              "String"
            )
          )
        )
      )

      when(configServerRequester.deleteMobileAppPushToken(Some("user")))
        .thenReturn(Future.unit)

      ref ! UnregisterMobileApp(Some("user"))

      verify(configServerRequester, timeout(500))
        .deleteMobileAppPushToken(Some("user"))
    }
  }
}
