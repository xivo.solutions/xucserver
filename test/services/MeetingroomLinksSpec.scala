package services

import models.{ContactSheetDisplayView, DirSearchItem, Relations}
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.{never, times, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import pekkotest.TestKitSpec
import services.config.ConfigServerRequester
import xivo.models.MeetingRoomAlias
import xivo.xuc.XucConfig
import fixtures.DirectoryFixtures

import scala.concurrent.Future

class MeetingroomLinksSpec
    extends TestKitSpec("MeetingroomLinksSpec")
    with MockitoSugar {

  class Helper extends DirectoryFixtures {
    val configRequester: ConfigServerRequester = mock[ConfigServerRequester]
    val config: XucConfig                      = mock[XucConfig]
    val displayViewMock: ContactSheetDisplayView   = mock[ContactSheetDisplayView]
    val meetingroomlinks: MeetingroomLinks =
      new MeetingroomLinks(configRequester, config)
    val sourceEntryId: String = "someSourceEntryId"
  }

  "MeetingroomLinksSpec" should {
    "not return a sharing link if result source is not meetingroom" in new Helper {
      val searchItem: DirSearchItem = dirSearchItem(
        displayViewMock,
        relations(),
        source = List(dirSource("ldap",Some(sourceEntryId)))
      )

      meetingroomlinks.getSharingLink(searchItem) should be(None)
      verify(configRequester, never()).getMeetingRoomAlias(any[String])
    }

    "not return a sharing link if config requester dont return alias" in new Helper {
      val searchItem: DirSearchItem = dirSearchItem(
        displayViewMock,
        relations(),
        source = List(dirSource("xivo_meetingroom",Some(sourceEntryId)))
      )
      when(configRequester.getMeetingRoomAlias(sourceEntryId))
        .thenReturn(Future.successful(MeetingRoomAlias(None)))

      meetingroomlinks.getSharingLink(searchItem) should be(None)
      verify(configRequester, times(1)).getMeetingRoomAlias(sourceEntryId)
    }

    "build meetingroom link properly" in new Helper {
      val searchItem: DirSearchItem = dirSearchItem(
        item = displayViewMock,
        source = List(dirSource("xivo_meetingroom",Some("1234")))
      )
      when(configRequester.getMeetingRoomAlias("1234"))
        .thenReturn(Future.successful(MeetingRoomAlias(Some("1234"))))

      meetingroomlinks.getSharingLink(searchItem) should be(
        Some(s"/meet?id=1234")
      )
      verify(configRequester, times(1)).getMeetingRoomAlias("1234")
    }
  }
}
