package services.request;

import play.api.libs.json.{JsError, Json}
import xuctest.BaseTest

class UserGroupActionRequestSpec extends BaseTest {
    "An userGroup pause request" should {
        "be read from json" in {
            val pauseRequest = Json.obj(
                "claz" -> XucRequest.WebClass,
                XucRequest.Cmd -> "pauseUserGroup",
                "groupId" -> 78,
            )

            val result = UserGroupPauseRequest.userGroupRequestReads.reads(pauseRequest)
            result.get should be(UserGroupPauseRequest(78, None))
        }
        "report an error if groupId is a string" in {
          val pauseRequest = Json.obj(
            "claz" -> XucRequest.WebClass,
            XucRequest.Cmd -> "pauseUserGroup",
            "groupId" -> "78",
          )

          val result = UserGroupPauseRequest.userGroupRequestReads.reads(pauseRequest)
          result shouldBe a[JsError]
        }
        "report an error if no groupId" in {
          val pauseRequest = Json.obj(
            "claz" -> XucRequest.WebClass,
            XucRequest.Cmd -> "pauseUserGroup",
          )

          val result = UserGroupPauseRequest.userGroupRequestReads.reads(pauseRequest)
          result shouldBe a[JsError]
        }
    }
    "An userGroup unpause request" should {
        "be read from json" in {
          val unpauseRequest = Json.obj(
            "claz" -> XucRequest.WebClass,
            XucRequest.Cmd -> "unpauseUserGroup",
            "groupId" -> 78,
          )

          val result = UserGroupUnpauseRequest.userGroupRequestReads.reads(unpauseRequest)
          result.get should be(UserGroupUnpauseRequest(78, None))
        }
        "report an error if groupId is a string" in {
          val unpauseRequest = Json.obj(
            "claz" -> XucRequest.WebClass,
            XucRequest.Cmd -> "unpauseUserGroup",
            "groupId" -> "78",
          )
          val result = UserGroupUnpauseRequest.userGroupRequestReads.reads(unpauseRequest)
          result shouldBe a[JsError]
        }
        "report an error if no groupId" in {
          val unpauseRequest = Json.obj(
            "claz" -> XucRequest.WebClass,
            XucRequest.Cmd -> "unpauseUserGroup",
          )
          val result = UserGroupUnpauseRequest.userGroupRequestReads.reads(unpauseRequest)
          result shouldBe a[JsError]
        }
    }
}
