package services.channel

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.testkit.TestKit
import org.asteriskjava.manager.action.*
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import pekkotest.TestKitSpec
import services.XucAmiBus
import services.XucAmiBus.{AmiAction, ChannelRequest}
import services.channel.ChannelRequestProc.*
import xivo.xucami.models.*

import java.util

class ChannelRequestProcessorSpec
    extends TestKitSpec("ChannelRequestProcessor")
    with MockitoSugar
    with AnyWordSpecLike
    with Matchers {

  trait agentChannels {
    val agentNb       = "3450"
    val cid: CallerId = CallerId("eddie", "2014")
    val monitoredChannel = new Channel(
      "444332.22",
      "SIP/uwert",
      cid,
      "444332.10",
      monitored = MonitorState.ACTIVE,
      agentNumber = Some(agentNb)
    )

    val pausedChannel: Channel = Channel(
      "567",
      "SIP/unpause",
      cid,
      "567",
      monitored = MonitorState.PAUSED,
      agentNumber = Some(agentNb)
    )
  }

  class Helper() {
    val amiBus: XucAmiBus              = mock[XucAmiBus]
    val channelRepo: ChannelRepository = mock[ChannelRepository]

    val crProcessor = new ChannelRequestProc(amiBus)
  }

  trait interfaceChannels {
    val interface     = "SIP/ouisdfh"
    val cid: CallerId = CallerId("John", "1032")
    val hangupRequest: ChannelRequest = ChannelRequest(
      HangupActionReq(interface, cid.number)
    )

    val channel: Channel = Channel("567", "SIP/ouisdfh-0000115", cid, "567")
  }

  "channel request processor in hangup action" should {
    "request channel hangup and set hangupsource accordingly" in new Helper()
      with interfaceChannels {

      when(channelRepo.toHangup(interface, cid.number))
        .thenReturn(List(channel))

      crProcessor.processChannelReq(hangupRequest, channelRepo)

      val args: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus, times(2)).publish(args.capture)
      val list: util.List[AmiAction] = args.getAllValues

      list.get(0).message match {
        case setvar: SetVarAction =>
          setvar.getChannel shouldBe channel.name
          setvar.getVariable shouldBe "CHANNEL(hangupsource)"
          setvar.getValue shouldBe channel.name
        case any =>
          fail(s"Should get SetVarAction, got: $any")
      }

      list.get(1).message match {
        case hangup: HangupAction =>
          hangup.getChannel shouldBe channel.name
        case any =>
          fail(s"Should get HangupAction, got: $any")
      }
    }
  }

  "Channel request processor in transfer" should {
    val interface = "SIP/ouisdfh"
    val cid       = CallerId("John", "1032")

    trait AttendedXferChannels {
      val destination = "030983043"
      val context     = "myCtx"
      val atXferReq: ChannelRequest =
        ChannelRequest(AtxFerActionReq(interface, destination, context))

      val channel: Channel = Channel("567", "SIP/ouisdfh-0000115", cid, "567")
    }

    "request attended transfer" in new Helper() with AttendedXferChannels {

      when(channelRepo.toXfer(interface)).thenReturn(Some(channel))

      crProcessor.processChannelReq(atXferReq, channelRepo)

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case atrxfer: AtxferAction =>
          atrxfer.getChannel shouldBe channel.name
          atrxfer.getContext shouldBe context
          atrxfer.getExten shouldBe destination + crProcessor.dialEnd
          atrxfer.getPriority shouldBe 1
        case any =>
          fail(s"Should get AtxferAction, got: $any")
      }
    }

    trait CompleteXferChannels {
      val channel: Channel = Channel("567", s"$interface-0000115", cid, "567")

      val completeXferActionReq: ChannelRequest =
        ChannelRequest(CompleteXferActionReq(interface))

    }
    "request complete transfer and set hangupsource accordingly" in new Helper()
      with CompleteXferChannels {

      when(channelRepo.toCompleteXfer(interface)).thenReturn(Some(channel))

      crProcessor.processChannelReq(completeXferActionReq, channelRepo)

      val args: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus, times(2)).publish(args.capture)
      val list: util.List[AmiAction] = args.getAllValues

      list.get(0).message match {
        case setvar: SetVarAction =>
          setvar.getChannel shouldBe channel.name
          setvar.getVariable shouldBe "CHANNEL(hangupsource)"
          setvar.getValue shouldBe channel.name
        case any =>
          fail(s"Should get SetVarAction, got: $any")
      }

      list.get(1).message match {
        case hangup: HangupAction =>
          hangup.getChannel shouldBe channel.name
        case any =>
          fail(s"Should get HangupAction, got: $any")
      }
    }

    trait CancelXferChannels {
      val channel: Channel = Channel("567", s"$interface-0000115", cid, "567")

      val cancelXferActionReq: ChannelRequest =
        ChannelRequest(CancelXferActionReq(interface, cid.number))

    }

    "request cancel transfer and set hangupsource accordingly" in new Helper()
      with CancelXferChannels {

      when(channelRepo.toCancelXfer(interface, cid.number))
        .thenReturn(Some(channel))

      crProcessor.processChannelReq(cancelXferActionReq, channelRepo)

      val args: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus, times(2)).publish(args.capture)
      val list: util.List[AmiAction] = args.getAllValues

      list.get(0).message match {
        case setvar: SetVarAction =>
          setvar.getChannel shouldBe channel.name
          setvar.getVariable shouldBe "CHANNEL(hangupsource)"
          setvar.getValue shouldBe channel.name
        case any =>
          fail(s"Should get SetVarAction, got: $any")
      }

      list.get(1).message match {
        case hangup: HangupAction =>
          hangup.getChannel shouldBe channel.name
        case any =>
          fail(s"Should get HangupAction, got: $any")
      }

    }
    trait DirectXferChannels {
      val destination = "030983043"
      val context     = "myCtx"
      val directXferReq: ChannelRequest =
        ChannelRequest(DirectXferActionReq(interface, destination, context))

      val channel: Channel = Channel("567", "SIP/ouisdfh-0000115", cid, "567")
    }

    "request direct transfer" in new Helper() with DirectXferChannels {

      when(channelRepo.toXfer(interface)).thenReturn(Some(channel))

      crProcessor.processChannelReq(directXferReq, channelRepo)

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case blindTransfer: BlindTransferAction =>
          blindTransfer.getChannel shouldBe channel.name
          blindTransfer.getContext shouldBe context
          blindTransfer.getExten shouldBe destination
          blindTransfer.getPriority shouldBe 1
        case any =>
          fail(s"Should get BlindTransferAction, got: $any")
      }
    }

    trait SetDataChannels {
      val phoneNb: String                = cid.number
      var variables: Map[String, String] = Map("Var" -> "Value")

      val setDataReq: ChannelRequest =
        ChannelRequest(SetDataActionReq(phoneNb, variables))
      val channels: List[Channel] =
        List(Channel("56711111.24", "SIP/ouisdfh-0000115", cid, "567"))

    }

    "Set Data" in new Helper() with SetDataChannels {
      when(channelRepo.toSetData(phoneNb)).thenReturn(channels)

      crProcessor.processChannelReq(setDataReq, channelRepo)

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case setData: SetVarAction =>
          setData.getChannel shouldBe channels(0).name
          setData.getVariable shouldBe "Var"
          setData.getValue shouldBe "Value"
        case any =>
          fail(s"Should get setDataAction, got: $any")
      }

    }
  }

}
