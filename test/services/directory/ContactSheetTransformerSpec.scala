package services.directory

import fixtures.DirectoryFixtures
import models.ContactSheetActionEnum.{Action, Call, Chat, Edit, Mail, ShareLink, Video}
import models.{ContactSheet, ContactSheetAction, ContactSheetActionEnum, ContactSheetDataType, ContactSheetDetailCategorie, ContactSheetDetailField, ContactSheetDisplayView, ContactSheetSources, ContactSheetStatus, ContactSheetValueBoolean, ContactSheetValueString, ContactSheetValueWithLabel, ContactSheetWrapper, DirSearchItem, DirSearchResult, Relations, XivoUser}
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.model.PhoneHintStatus
import pekkotest.TestKitSpec
import services.MeetingroomLinks
import services.config.{ConfigRepository, ConfigServerRequester}
import services.video.model.VideoEvents
import xivo.xuc.XucConfig

class ContactSheetTransformerSpec
    extends TestKitSpec("ContactSheetTransformerSpec")
    with MockitoSugar
    with DirectoryFixtures {

  val tmourierCtiUser: Option[XivoUser] = Some(
    XivoUser(
      id = 1,
      agentId = None,
      voicemailid = None,
      firstname = "tony",
      lastname = Some("mourier"),
      username = Some("tmourier"),
      password = None,
      mobile_phone_number = None,
      ctiProfileId = Some(1)
    )
  )

  val tmourierCtiUserEmpty: Option[XivoUser] = Some(
    XivoUser(
      id = 2,
      agentId = None,
      voicemailid = None,
      firstname = "tony",
      lastname = Some("mourier"),
      username = Some("tmourier"),
      password = None,
      mobile_phone_number = None,
      ctiProfileId = Some(1)
    )
  )

  val tmourierRaw: DirSearchItem = dirSearchItem(
    item = contactSheetDisplayView(
      name = ContactSheetValueString("Tony Mourier"),
      subtitle1 = ContactSheetValueString("Dev"),
      subtitle2 = ContactSheetValueString("RD"),
      title = ContactSheetValueWithLabel("Title", "Fullstack developper"),
      service =
        ContactSheetValueWithLabel("Department", "Research and development"),
      phone = ContactSheetValueWithLabel("Internal phone", "41142"),
      phonePro = ContactSheetValueWithLabel("Professional phone", "0612345678"),
      phoneMobile = ContactSheetValueWithLabel("Mobile phone", ""),
      phoneHome = ContactSheetValueWithLabel("Home phone", ""),
      mail = ContactSheetValueWithLabel(
        "Personal email",
        "mourier.tony@outlook.com"
      ),
      fax = ContactSheetValueWithLabel("Fax", ""),
      manager = ContactSheetValueWithLabel("Manager", "Laurent Meiller"),
      company = ContactSheetValueWithLabel("Company", "Avencall"),
      website = ContactSheetValueWithLabel("Website", "avencall.com"),
      office = ContactSheetValueWithLabel("Office", "Software Factory"),
      location = ContactSheetValueWithLabel("Office location", "Dardilly"),
      favorite = ContactSheetValueBoolean(true),
      personal = ContactSheetValueBoolean(false),
      meetingroom = ContactSheetValueBoolean(false),
      picture = ContactSheetValueString("njifgijufgdijufgijfghijfghnj")
    ),
    relations = relations(
      userId = Some(1)
    ),
    source = List(dirSource("ldap",Some("1")))
  )

  val tmourierRawEmpty: DirSearchItem = tmourierRaw.copy(
    item = tmourierRaw.item.copy(
      phone = ContactSheetValueWithLabel("Internal phone", ""),
      mail = ContactSheetValueWithLabel(
        "Personal email",
        ""
      ),
      phonePro = ContactSheetValueWithLabel("Professional phone", "")
    ),
    relations = relations(),
    source = List(dirSource("ldap",Some("404")))
  )

  val tmourierResult: ContactSheet = ContactSheet(
    name = "Tony Mourier",
    subtitle1 = "Dev",
    subtitle2 = "RD",
    picture = "njifgijufgdijufgijfghijfghnj",
    isPersonal = false,
    isFavorite = true,
    isMeetingroom = false,
    canBeFavorite = true,
    status = ContactSheetStatus(
      phone = PhoneHintStatus.AVAILABLE,
      video = VideoEvents.Available
    ),
    actions = Map(
      ContactSheetActionEnum.Call -> ContactSheetAction(List("41142"), false),
      ContactSheetActionEnum.Chat -> ContactSheetAction(
        List("tmourier"),
        false
      ),
      ContactSheetActionEnum.Mail -> ContactSheetAction(
        List("mourier.tony@outlook.com"),
        false
      ),
      ContactSheetActionEnum.Video -> ContactSheetAction(
        List("tmourier"),
        false
      )
    ),
    sources = List(ContactSheetSources("ldap", "1")),
    details = List(
      ContactSheetDetailCategorie(
        "Contacts",
        List(
          ContactSheetDetailField(
            "Internal phone",
            "41142",
            ContactSheetDataType.PhoneNumber
          ),
          ContactSheetDetailField(
            "Professional phone",
            "0612345678",
            ContactSheetDataType.PhoneNumber
          ),
          ContactSheetDetailField(
            "Mobile phone",
            "",
            ContactSheetDataType.PhoneNumber
          ),
          ContactSheetDetailField(
            "Home phone",
            "",
            ContactSheetDataType.PhoneNumber
          ),
          ContactSheetDetailField(
            "Personal email",
            "mourier.tony@outlook.com",
            ContactSheetDataType.Mail
          ),
          ContactSheetDetailField("Fax", "", ContactSheetDataType.PhoneNumber)
        )
      ),
      ContactSheetDetailCategorie(
        "Général",
        List(
          ContactSheetDetailField(
            "Title",
            "Fullstack developper",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Department",
            "Research and development",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Manager",
            "Laurent Meiller",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Company",
            "Avencall",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Website",
            "avencall.com",
            ContactSheetDataType.Url
          )
        )
      ),
      ContactSheetDetailCategorie(
        "Lieu d'affectation",
        List(
          ContactSheetDetailField(
            "Office",
            "Software Factory",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Office location",
            "Dardilly",
            ContactSheetDataType.String
          )
        )
      )
    )
  )

  val tmourierEmpty: ContactSheet = tmourierResult.copy(
    actions = tmourierResult.actions.map((key, value) =>
      (key, ContactSheetAction(List.empty, true))
    ),
    status = ContactSheetStatus(
      phone = PhoneHintStatus.UNEXISTING,
      video = VideoEvents.Available
    ),
    sources = List(ContactSheetSources("ldap", "404")),
    details = tmourierResult.details.map(cat =>
      if cat.name == "Contacts" then
        cat.copy(fields =
          cat.fields.map(field =>
            if field.name == "Internal phone" || field.name == "Personal email" || field.name == "Professional phone"
            then field.copy(data = "")
            else field
          )
        )
      else cat
    )
  )

  val tmourierRawPC: DirSearchItem = tmourierRaw.copy(
    item = tmourierRaw.item.copy(personal = ContactSheetValueBoolean(true))
  )

  val tmourierResultPC: ContactSheet = tmourierResult.copy(
    actions = tmourierResult.actions ++ Map(
      ContactSheetActionEnum.Edit -> ContactSheetAction(
        List(tmourierRawPC.source.head.entry.get),
        false
      )
    ),
    isPersonal = true
  )

  val meetingroomRaw: DirSearchItem = dirSearchItem(
    item = ContactSheetDisplayView(
      name = ContactSheetValueString("Daily"),
      subtitle1 = ContactSheetValueString("Meeting"),
      subtitle2 = ContactSheetValueString("Room"),
      title = ContactSheetValueWithLabel("Title", "Meeting"),
      service = ContactSheetValueWithLabel("Department", "Room"),
      phone = ContactSheetValueWithLabel("Internal phone", "4000"),
      phonePro = ContactSheetValueWithLabel("Professional phone", ""),
      phoneMobile = ContactSheetValueWithLabel("Mobile phone", ""),
      phoneHome = ContactSheetValueWithLabel("Home phone", ""),
      mail = ContactSheetValueWithLabel(
        "Personal email",
        "room@outlook.com"
      ),
      fax = ContactSheetValueWithLabel("Fax", ""),
      manager = ContactSheetValueWithLabel("Manager", "Laurent Meiller"),
      company = ContactSheetValueWithLabel("Company", "Avencall"),
      website = ContactSheetValueWithLabel("Website", "avencall.com"),
      office = ContactSheetValueWithLabel("Office", "Software Factory"),
      location = ContactSheetValueWithLabel("Office location", "Dardilly"),
      favorite = ContactSheetValueBoolean(true),
      personal = ContactSheetValueBoolean(false),
      meetingroom = ContactSheetValueBoolean(true),
      picture = ContactSheetValueString("")
    ),
    relations = relations(),
    source = List(dirSource("xivo_meetingroom",Some("42")))
  )

  val meetingroomRawEmpty: DirSearchItem = meetingroomRaw.copy(
    item = meetingroomRaw.item.copy(
      phone = ContactSheetValueWithLabel("Internal phone", ""),
      mail = ContactSheetValueWithLabel(
        "Personal email",
        ""
      )
    ),
    relations = relations(),
    source = List(dirSource("xivo_meetingroom"))
  )

  val meetingroomResult: ContactSheet = ContactSheet(
    name = "Daily",
    subtitle1 = "Meeting",
    subtitle2 = "Room",
    picture = "",
    isPersonal = false,
    isFavorite = true,
    isMeetingroom = true,
    canBeFavorite = true,
    status = ContactSheetStatus(
      phone = PhoneHintStatus.AVAILABLE,
      video = VideoEvents.Available
    ),
    actions = Map(
      ContactSheetActionEnum.Call -> ContactSheetAction(
        List("4000"),
        disable = false
      ),
      ContactSheetActionEnum.Video -> ContactSheetAction(List("42"), false),
      ContactSheetActionEnum.ShareLink -> ContactSheetAction(
        List("/meet?id=2"),
        false
      )
    ),
    sources = List(ContactSheetSources("xivo_meetingroom", "42")),
    details = List(
      ContactSheetDetailCategorie(
        "Contacts",
        List(
          ContactSheetDetailField(
            "Internal phone",
            "4000",
            ContactSheetDataType.PhoneNumber
          ),
          ContactSheetDetailField(
            "Professional phone",
            "",
            ContactSheetDataType.PhoneNumber
          ),
          ContactSheetDetailField(
            "Mobile phone",
            "",
            ContactSheetDataType.PhoneNumber
          ),
          ContactSheetDetailField(
            "Home phone",
            "",
            ContactSheetDataType.PhoneNumber
          ),
          ContactSheetDetailField(
            "Personal email",
            "room@outlook.com",
            ContactSheetDataType.Mail
          ),
          ContactSheetDetailField("Fax", "", ContactSheetDataType.PhoneNumber)
        )
      ),
      ContactSheetDetailCategorie(
        "Général",
        List(
          ContactSheetDetailField(
            "Title",
            "Meeting",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Department",
            "Room",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Manager",
            "Laurent Meiller",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Company",
            "Avencall",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Website",
            "avencall.com",
            ContactSheetDataType.Url
          )
        )
      ),
      ContactSheetDetailCategorie(
        "Lieu d'affectation",
        List(
          ContactSheetDetailField(
            "Office",
            "Software Factory",
            ContactSheetDataType.String
          ),
          ContactSheetDetailField(
            "Office location",
            "Dardilly",
            ContactSheetDataType.String
          )
        )
      )
    )
  )

  val meetingroomEmpty: ContactSheet = meetingroomResult.copy(
    canBeFavorite = false,
    actions = meetingroomResult.actions.map((key, value) =>
      (key, ContactSheetAction(List.empty, true))
    ),
    status = ContactSheetStatus(
      phone = PhoneHintStatus.UNEXISTING,
      video = VideoEvents.Available
    ),
    sources = List(ContactSheetSources("xivo_meetingroom", "")),
    details = meetingroomResult.details.map(cat =>
      if cat.name == "Contacts" then
        cat.copy(fields =
          cat.fields.map(field =>
            if field.name == "Internal phone" || field.name == "Personal email" || field.name == "Professional phone"
            then field.copy(data = "")
            else field
          )
        )
      else cat
    )
  )

  val pmrRaw: DirSearchItem = meetingroomRaw.copy(
    item = meetingroomRaw.item.copy(personal = ContactSheetValueBoolean(true))
  )

  val pmrResult: ContactSheet = meetingroomResult.copy(
    actions = meetingroomResult.actions ++ Map(
      ContactSheetActionEnum.Edit -> ContactSheetAction(
        List(pmrRaw.source.head.entry.get),
        false
      )
    ),
    isPersonal = true
  )

  class Helper() {
    val repo: ConfigRepository                 = mock[ConfigRepository]
    val configRequester: ConfigServerRequester = mock[ConfigServerRequester]
    val config: XucConfig                      = mock[XucConfig]
    val meetingroomLinks: MeetingroomLinks     = mock[MeetingroomLinks]
    val contactSheetTransformer: ContactSheetTransformer = {
      new ContactSheetTransformer(
        repo,
        configRequester,
        config,
        meetingroomLinks
      )
    }

    def singleDirSearchResult(dirSearchItem: DirSearchItem): DirSearchResult =
      DirSearchResult(
        columnHeaders = List("someColumnHeader"),
        columnTypes = List("someColumnType"),
        items = List(dirSearchItem)
      )

  }

  "ContactSheetTransformer" should {
    "wrap a contact sheet" in new Helper {
      val unwrappedContact: DirSearchResult = singleDirSearchResult(tmourierRaw)
      val id: Long                          = 1
      val expectedResult: ContactSheetWrapper = ContactSheetWrapper(
        contactSheets = List(tmourierResult)
      )
      when(repo.getCtiUser(id)).thenReturn(tmourierCtiUser)
      when(repo.getUserVideoStatus("tmourier"))
        .thenReturn(Some(VideoEvents.Available))
      when(repo.richPhones).thenReturn(
        Map("41142" -> PhoneHintStatus.AVAILABLE)
      )

      val result: ContactSheetWrapper = contactSheetTransformer
        .mapDirSearchResultToContactSheet(unwrappedContact.items, id)
      result shouldBe expectedResult
    }

    "wrap a contact sheet with disable actions" in new Helper {
      val unwrappedContact: DirSearchResult =
        singleDirSearchResult(tmourierRawEmpty)
      val expectedResult: ContactSheetWrapper = ContactSheetWrapper(
        contactSheets = List(tmourierEmpty)
      )
      val result: ContactSheetWrapper = contactSheetTransformer
        .mapDirSearchResultToContactSheet(unwrappedContact.items, 404)
      result shouldBe expectedResult
    }

    "wrap a personal contact sheet" in new Helper {
      private val id = 1
      val unwrappedContact: DirSearchResult =
        singleDirSearchResult(tmourierRawPC)
      val expectedResult: ContactSheetWrapper = ContactSheetWrapper(
        contactSheets = List(tmourierResultPC)
      )
      when(repo.getCtiUser(id)).thenReturn(tmourierCtiUser)
      when(repo.getUserVideoStatus("tmourier"))
        .thenReturn(Some(VideoEvents.Available))
      when(repo.richPhones).thenReturn(
        Map("41142" -> PhoneHintStatus.AVAILABLE)
      )
      val result: ContactSheetWrapper = contactSheetTransformer
        .mapDirSearchResultToContactSheet(unwrappedContact.items, id)
      result shouldBe expectedResult
    }

    "wrap a meetingroom sheet" in new Helper {
      val unwrappedMeetingroom: DirSearchResult =
        singleDirSearchResult(meetingroomRaw)
      val id: Long = 42
      val expectedResult: ContactSheetWrapper = ContactSheetWrapper(
        contactSheets = List(meetingroomResult)
      )
      when(repo.richPhones).thenReturn(
        Map("4000" -> PhoneHintStatus.AVAILABLE)
      )
      when(meetingroomLinks.getSharingLink(meetingroomRaw))
        .thenReturn(Some("/meet?id=2"))

      val result: ContactSheetWrapper = contactSheetTransformer
        .mapDirSearchResultToContactSheet(unwrappedMeetingroom.items, id)
      result shouldBe expectedResult
    }

    "wrap a meetingroom sheet with disabled actions" in new Helper {
      val unwrappedMeetingroom: DirSearchResult = singleDirSearchResult(meetingroomRawEmpty)
      val id: Long = 400
      val expectedResult: ContactSheetWrapper = ContactSheetWrapper(
        contactSheets = List(meetingroomEmpty)
      )
      when(repo.richPhones).thenReturn(
        Map("4000" -> PhoneHintStatus.AVAILABLE)
      )
      when(meetingroomLinks.getSharingLink(meetingroomRawEmpty))
        .thenReturn(None)

      val result: ContactSheetWrapper = contactSheetTransformer
        .mapDirSearchResultToContactSheet(unwrappedMeetingroom.items, id)
      result shouldBe expectedResult
    }

    "wrap a personal meetingroom sheet" in new Helper {
      val unwrappedMeetingroom: DirSearchResult =
        singleDirSearchResult(pmrRaw)
      val expectedResult: ContactSheetWrapper = ContactSheetWrapper(
        contactSheets = List(pmrResult)
      )
      when(repo.richPhones).thenReturn(
        Map("4000" -> PhoneHintStatus.AVAILABLE)
      )
      when(meetingroomLinks.getSharingLink(pmrRaw))
        .thenReturn(Some("/meet?id=2"))

      val result: ContactSheetWrapper = contactSheetTransformer
        .mapDirSearchResultToContactSheet(unwrappedMeetingroom.items, 89653)
      result shouldBe expectedResult
    }

    "choose with callAction by default the internal phone" in new Helper:
      val callAction: ContactSheetAction =
        contactSheetTransformer.getCallAction(tmourierRaw.item)
      callAction shouldEqual ContactSheetAction(List("41142"), false)

    "choose with callAction second the professional phone" in new Helper:
      val callActionWithoutInternal: ContactSheetAction =
        contactSheetTransformer
          .getCallAction(
            tmourierRaw.item
              .copy(phone = ContactSheetValueWithLabel("Internal Phone", ""))
          )

      callActionWithoutInternal shouldEqual ContactSheetAction(
        List("0612345678"),
        false
      )

    "choose with callAction third the mobile phone" in new Helper:
      val callActionInOrder: ContactSheetAction =
        contactSheetTransformer
          .getCallAction(
            tmourierRaw.item.copy(
              phone = ContactSheetValueWithLabel("Internal Phone", ""),
              phonePro = ContactSheetValueWithLabel("Professional phone", ""),
              phoneMobile =
                ContactSheetValueWithLabel("Mobile phone", "0123456789"),
              phoneHome = ContactSheetValueWithLabel("Home phone", "0412233445")
            )
          )

      callActionInOrder shouldEqual ContactSheetAction(
        List("0123456789"),
        false
      )

    "choose with callAction fourth the home phone" in new Helper:
      val callActionInOrder: ContactSheetAction =
        contactSheetTransformer
          .getCallAction(
            tmourierRaw.item.copy(
              phone = ContactSheetValueWithLabel("Internal Phone", ""),
              phonePro = ContactSheetValueWithLabel("Professional phone", ""),
              phoneMobile = ContactSheetValueWithLabel("Mobile phone", ""),
              phoneHome = ContactSheetValueWithLabel("Home phone", "0412233445")
            )
          )
      callActionInOrder shouldEqual ContactSheetAction(
        List("0412233445"),
        false
      )

    "disable callAction (as Some(List.empty)) if no phone was given" in new Helper:
      val tmourierWithoutPhones: DirSearchItem = tmourierRaw.copy(item =
        tmourierRaw.item.copy(
          phone = ContactSheetValueWithLabel("Internal Phone", ""),
          phonePro = ContactSheetValueWithLabel("Professional phone", "")
        )
      )
      val emptyCallAction: ContactSheetAction =
        contactSheetTransformer
          .getCallAction(
            tmourierWithoutPhones.item
          )
      emptyCallAction shouldEqual ContactSheetAction(List.empty, true)

    "action formatter returns the value if it exists and is not empty" in new Helper:
      val nonEmptyOption: Option[String] = Some("myOption")
      val expectedFormatting: ContactSheetAction =
        ContactSheetAction(List("myOption"), false)

      contactSheetTransformer.formatSingleStringOption(
        nonEmptyOption
      ) shouldEqual expectedFormatting

    "action formatter returns Some(List.empty) if the value doesn't exist or is empty" in new Helper:
      val emptyOption: Option[String] = None
      val optionOfEmptyString: Option[String] = Some("")
      val expectedEmptyOptionList: ContactSheetAction =
        ContactSheetAction(List.empty, true)

      contactSheetTransformer.formatSingleStringOption(
        emptyOption
      ) shouldEqual expectedEmptyOptionList

      contactSheetTransformer.formatSingleStringOption(
        optionOfEmptyString
      ) shouldEqual expectedEmptyOptionList

    "action formatter returns Some(List.empty) if chatDisable is true" in new Helper:
      val nonEmptyOption: Option[String] = Some("myOption")
      val expectedFormatting: ContactSheetAction =
        ContactSheetAction(List.empty, true)

      contactSheetTransformer.formatSingleStringOption(
      nonEmptyOption, true
    ) shouldEqual expectedFormatting

    "can edit a personal contact from the search" in new Helper:
      val editMapFromNonPC: Map[Action, ContactSheetAction] =
        contactSheetTransformer.getEditMap(tmourierRaw)
      assert(editMapFromNonPC.isEmpty)

      val editMapFromPC: Map[Action, ContactSheetAction] =
        contactSheetTransformer.getEditMap(
          tmourierRawPC.copy(source = List(dirSource(
            tmourierRaw.source.head.id,
            Some("tonySourceEntryId")
          )))
        )
      assert(editMapFromPC == Map(
        Edit -> ContactSheetAction(List("tonySourceEntryId"), false)
      ))

    "can edit a personal meeting room from the search" in new Helper:
      val editMapFromMR: Map[Action, ContactSheetAction] =
        contactSheetTransformer.getEditMap(meetingroomRaw)
      assert(editMapFromMR.isEmpty)

      val editMapFromPMR: Map[Action, ContactSheetAction] =
        contactSheetTransformer.getEditMap(
          pmrRaw.copy(source = List(dirSource(
            pmrRaw.source.head.id,
            Some("pmrSourceEntryId")
          )))
        )
      assert(editMapFromPMR == Map(
        Edit -> ContactSheetAction(List("pmrSourceEntryId"), false)
      ))

  }
}
