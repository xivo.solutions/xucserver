package services.directory

import models.*
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.{verify, verifyNoInteractions, when}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import org.xivo.cti.message.PhoneConfigUpdate
import org.xivo.cti.model.PhoneHintStatus
import services.MeetingroomLinks
import services.config.{ConfigRepository, ConfigServerRequester}
import xivo.models.{
  AgentFactory,
  AgentQueueMemberFactory,
  GroupMemberFactory,
  LineFactory,
  MeetingRoomAlias
}
import xivo.xuc.{IceServerConfig, SipConfig, XucConfig}
import fixtures.DirectoryFixtures
import models.Relations.ResultGrouped

import scala.collection.mutable
import scala.concurrent.Future
import scala.language.reflectiveCalls
import services.video.model.UserVideoEvent
import services.video.model.VideoEvents
import xivo.services.XivoDirectory.{DirLookupResult, DirectoryResult, Favorites}

import scala.collection.immutable.ListSet

class DirectoryTransformerLogicSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar {

  class Helper extends DirectoryFixtures {

    def fixture(phoneNumber: String, lineId: Int = 34): fixture =
      new fixture(phoneNumber, lineId)
    class fixture(phoneNumber: String, lineId: Int = 34) {
      val LineId: Int = lineId
      val userId: Int = 52
      val status: Int = PhoneHintStatus.AVAILABLE.getHintStatus

      val phoneConfig: PhoneConfigUpdate = {
        val phoneConfig = new PhoneConfigUpdate
        phoneConfig.setId(LineId)
        phoneConfig.setNumber(phoneNumber)
        phoneConfig.setUserId(userId)
        phoneConfig
      }

      val xivoDirectoryResult: DirSearchResult = {
        dirSearchResult(
          columnHeaders =
            List("Name", "Number", "Mobile", "Other number", "Favorite"),
          columnTypes = List("name", "name", "number", "number", "favorite"),
          items = List(
            dirSearchItem(
              item = contactSheetDisplayView(
                name = ContactSheetValueString("peter pan"),
                title = ContactSheetValueWithLabel("Title", "sometitle"),
                service = ContactSheetValueWithLabel("Service", "someService"),
                phone =
                  ContactSheetValueWithLabel("Internal Number", phoneNumber),
                phonePro = ContactSheetValueWithLabel("Pro Number", "87"),
                phoneMobile = ContactSheetValueWithLabel("Mobile Phone", "7"),
                company = ContactSheetValueWithLabel("Company", "Some Company"),
                website =
                  ContactSheetValueWithLabel("Website", "company.website.com"),
                picture = ContactSheetValueString("nuhsdfsisdjosdfoisdfo")
              ),
              relations = relations(
                userId = Some(userId),
                xivoId = Some("xivo-id1")
              ),
              source = List(dirSource("xivo-directory"))
            )
          )
        )
      }
    }

    val repo = new ConfigRepository(
      mock[MessageFactory],
      mock[LineFactory],
      mock[AgentFactory],
      mock[AgentQueueMemberFactory],
      mock[GroupMemberFactory],
      mock[ConfigServerRequester],
      mock[IceServerConfig],
      mock[SipConfig]
    )
    val configMgtMock: ConfigServerRequester = mock[ConfigServerRequester]
    val xucConfig: XucConfig                 = mock[XucConfig]
    val meetingroomLinks: MeetingroomLinks   = mock[MeetingroomLinks]
    val logic = new DirectoryTransformerLogic(
      repo,
      configMgtMock,
      xucConfig,
      meetingroomLinks
    )
    val testFixture: fixture = fixture("44500")
  }

  "DirectoryTransformerLogic" should {
    "update and transform xivo directory entries" in new Helper() {
      when(meetingroomLinks.getSharingLink(any[DirSearchItem])).thenReturn(None)
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)
      repo.updatePhoneStatus(
        testFixture.phoneConfig.getNumber,
        testFixture.status
      )

      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries.head
      val expectedRichEntry: RichEntry = richEntry(
        status = PhoneHintStatus.AVAILABLE,
        fields = mutable.Buffer("peter pan", "44500", "87", "7", false, ""),
        source = List("xivo-directory"),
        favorite = Some(false)
      )

      assert(richEntry.status == PhoneHintStatus.AVAILABLE)
      assert(richEntry.favorite.contains(false))
      assert(richDirectoryResult.headers == RichDirectoryEntries.defaultHeaders)
      assert(richDirectoryResult.getEntries.head == expectedRichEntry)
    }

    "update xivo directory entries with UNEXISTING when line not found" in new Helper() {
      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries.head

      assert(richEntry.status == PhoneHintStatus.UNEXISTING)
      assert(richDirectoryResult.headers == RichDirectoryEntries.defaultHeaders)
    }

    "update xivo directory entries with UNEXISTING when status not found" in new Helper() {
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)

      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries.head

      assert(richEntry.status == PhoneHintStatus.UNEXISTING)
      assert(richDirectoryResult.headers == RichDirectoryEntries.defaultHeaders)
    }

    "update and transform xivo favorites entries" in new Helper() {
      repo.onPhoneConfigUpdate(testFixture.phoneConfig)
      repo.updatePhoneStatus(
        testFixture.phoneConfig.getNumber,
        testFixture.status
      )

      val favorites =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      favorites.entries =
        logic.getEnrichResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = favorites.getEntries.head

      assert(richEntry.status == PhoneHintStatus.AVAILABLE)
      assert(richEntry.favorite.contains(false))
      assert(favorites.headers == RichDirectoryEntries.defaultHeaders)
    }

    "update results with username" in new Helper() {
      val xivoUser: XivoUser =
        XivoUser(52, None, None, "foo", None, Some("foobar"), None, None, None)
      repo.updateUser(xivoUser)

      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries.head

      assert(richEntry.username.contains("foobar"))

    }

    "update results with video status" in new Helper() {
      val xivoUser: XivoUser =
        XivoUser(52, None, None, "foo", None, Some("foobar"), None, None, None)
      repo.updateUser(xivoUser)
      repo.updateVideoStatus(e = UserVideoEvent("foobar", "videoStart"))

      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries.head

      assert(richEntry.videoStatus.contains(VideoEvents.Busy))
    }

    "username should be None if there is no XivoUser associated to the line" in new Helper {
      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(testFixture.xivoDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries.head

      assert(richEntry.username.isEmpty)
    }

    "update results with personal meeting room link if source is meeting room" in new Helper() {
      when(meetingroomLinks.getSharingLink(any[DirSearchItem]))
        .thenReturn(Some("/meet?id=abcd-1234"))
      val meetingRoomAlias: MeetingRoomAlias = MeetingRoomAlias(
        Some("abcd-1234")
      )
      val xivoMeetingDirectoryResult: DirSearchResult =
        dirSearchResult(
          columnHeaders =
            List("Name", "Number", "Mobile", "Other number", "Favorite"),
          columnTypes = List("name", "number", "number", "number", "favorite"),
          items = List(
            dirSearchItem(
              item = contactSheetDisplayView(
                name = ContactSheetValueString("Meetingroom"),
                phone = ContactSheetValueWithLabel("Internal Number", "4004"),
                favorite = ContactSheetValueBoolean(true),
                personal = ContactSheetValueBoolean(true),
                meetingroom = ContactSheetValueBoolean(true)
              ),
              relations = relations(
                xivoId = Some("xivo-id1")
              ),
              source = List(dirSource("xivo_meetingroom", Some("1")))
            )
          )
        )
      val expectedFields: mutable.Buffer[Any] = mutable.Buffer(
        "Meetingroom",
        "4004",
        "",
        "",
        true,
        ""
      )

      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(xivoMeetingDirectoryResult, 1)

      assert(richDirectoryResult.getEntries.head.fields == expectedFields)
      assert(
        richDirectoryResult.getEntries.head.url.contains("/meet?id=abcd-1234")
      )
      assert(richDirectoryResult.getEntries.head.personal)
    }

    "update results with static meeting room link if source is meeting room" in new Helper() {
      when(meetingroomLinks.getSharingLink(any[DirSearchItem]))
        .thenReturn(Some("/meet?id=abcd-1234"))
      val meetingRoomAlias: MeetingRoomAlias = MeetingRoomAlias(
        Some("abcd-1234")
      )

      val xivoMeetingDirectoryResult: DirSearchResult =
        dirSearchResult(
          columnHeaders =
            List("Name", "Number", "Mobile", "Other number", "Favorite"),
          columnTypes = List("name", "number", "number", "number", "favorite"),
          items = List(
            dirSearchItem(
              item = contactSheetDisplayView(
                name = ContactSheetValueString("Meetingroom"),
                phone = ContactSheetValueWithLabel("Internal Number", "4004"),
                favorite = ContactSheetValueBoolean(true),
                meetingroom = ContactSheetValueBoolean(true)
              ),
              relations = relations(
                xivoId = Some("xivo-id1")
              ),
              source = List(dirSource("xivo_meetingroom", Some("1")))
            )
          )
        )

      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(xivoMeetingDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries.head

      richDirectoryResult.getEntries.head.fields shouldEqual
        mutable.Buffer(
          "Meetingroom",
          "4004",
          "",
          "",
          true,
          ""
        )

      assert(
        richDirectoryResult.getEntries.head.url.contains("/meet?id=abcd-1234")
      )
      assert(!richDirectoryResult.getEntries.head.personal)
    }

    "update results with email if source is different than meeting room" in new Helper() {
      val meetingRoomAlias: MeetingRoomAlias = MeetingRoomAlias(
        Some("abcd-1234")
      )
      when(configMgtMock.getMeetingRoomAlias("1"))
        .thenReturn(Future.successful(meetingRoomAlias))

      val xivoMeetingDirectoryResult: DirSearchResult =
        dirSearchResult(
          columnHeaders =
            List("Name", "Number", "Mobile", "Other number", "Favorite"),
          columnTypes = List("name", "number", "number", "number", "favorite"),
          items = List(
            dirSearchItem(
              item = contactSheetDisplayView(
                name = ContactSheetValueString("Meetingroom"),
                phone = ContactSheetValueWithLabel("Internal Number", "4004"),
                mail = ContactSheetValueWithLabel("Mail", "email@email.com"),
                favorite = ContactSheetValueBoolean(true),
                meetingroom = ContactSheetValueBoolean(true)
              ),
              relations = relations(
                xivoId = Some("xivo-id1")
              ),
              source = List(dirSource("xivo_meetingroom", Some("1")))
            )
          )
        )

      val richDirectoryResult =
        new RichResult(RichDirectoryEntries.defaultHeaders)
      richDirectoryResult.entries =
        logic.getEnrichResult(xivoMeetingDirectoryResult, 1)
      val richEntry: RichEntry = richDirectoryResult.getEntries.head

      richDirectoryResult.getEntries.head.fields shouldEqual
        mutable.Buffer(
          "Meetingroom",
          "4004",
          "",
          "",
          true,
          "email@email.com"
        )

      verifyNoInteractions(configMgtMock)
    }

    class ElizHelper extends Helper:
      val ecampbellInternal: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayViewFR(
          name = ContactSheetValueString("Elizabeth Campbell"),
          phone = ContactSheetValueWithLabel("Téléphone interne", "1630"),
          mail = ContactSheetValueWithLabel(
            "E-mail",
            "elizabeth.campbell@xivo-dev.com"
          )
        ),
        relations = relations(
          endpointId = Some(160),
          userId = Some(171),
          xivoId = Some("4e288243-ee47-4bc7-91f5-5eca844ab05b")
        ),
        source = List(dirSource("internal", Some("171")))
      )
      val eswannInternal: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayViewFR(
          name = ContactSheetValueString("Elizabeth Swann"),
          phone = ContactSheetValueWithLabel("Téléphone interne", "1234"),
          mail =
            ContactSheetValueWithLabel("E-mail", "elizabeth.swann@potc.com")
        ),
        source = List(dirSource("internal", Some("123"))),
        rank = 1
      )
      val ecampbellLdap: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayViewFR(
          name = ContactSheetValueString("Elizabeth Campbell"),
          subtitle1 = ContactSheetValueString("Petroleum engineer"),
          subtitle2 = ContactSheetValueString("Ressources Humaines"),
          title = ContactSheetValueWithLabel("Fonction", "Petroleum engineer"),
          service =
            ContactSheetValueWithLabel("Service", "Ressources Humaines"),
          phone = ContactSheetValueWithLabel("Téléphone interne", "1568"),
          phonePro = ContactSheetValueWithLabel("Téléphone pro", "0544195920"),
          phoneMobile =
            ContactSheetValueWithLabel("Téléphone mobile", "0658840188"),
          mail = ContactSheetValueWithLabel(
            "E-mail",
            "elizabeth.campbell@xivo-dev.com"
          )
        ),
        source = List(dirSource("ldap")),
        rank = 2
      )
      val eswannInternalWithoutEmail: DirSearchItem = eswannInternal.copy(
        item = eswannInternal.item
          .copy(mail = ContactSheetValueWithLabel("mail", "")),
        rank = 3
      )
      val ecampbellLdapWithoutEmail: DirSearchItem =
        ecampbellLdap.copy(
          item = ecampbellLdap.item
            .copy(mail = ContactSheetValueWithLabel("mail", "")),
          rank = 4
        )

      // merge with the strategy ldap first but internal phone number first
      val ecampbellMerged: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayViewFR(
          name = ContactSheetValueString("Elizabeth Campbell"),
          subtitle1 = ContactSheetValueString("Petroleum engineer"),
          subtitle2 = ContactSheetValueString("Ressources Humaines"),
          title = ContactSheetValueWithLabel("Fonction", "Petroleum engineer"),
          service =
            ContactSheetValueWithLabel("Service", "Ressources Humaines"),
          phone = ContactSheetValueWithLabel("Téléphone interne", "1630"),
          phonePro = ContactSheetValueWithLabel("Téléphone pro", "0544195920"),
          phoneMobile =
            ContactSheetValueWithLabel("Téléphone mobile", "0658840188"),
          mail = ContactSheetValueWithLabel(
            "E-mail",
            "elizabeth.campbell@xivo-dev.com"
          )
        ),
        relations = relations(
          endpointId = Some(160),
          userId = Some(171),
          xivoId = Some("4e288243-ee47-4bc7-91f5-5eca844ab05b")
        ),
        source = List(dirSource("ldap"), dirSource("internal", Some("171")))
      )
      val elizDirSearchResult: DirSearchResult = dirSearchResult(
        items = List(
          ecampbellInternal,
          eswannInternal,
          ecampbellLdap,
          eswannInternalWithoutEmail,
          ecampbellLdapWithoutEmail
        )
      )
      val elizDirectoryResultMock: DirectoryResult = mock[DirectoryResult]

    "not merge DirectoryResult if directorySourcesPriority is empty" in new ElizHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(ListSet.empty)

      val result: DirectoryResult = logic.mergeContacts(elizDirectoryResultMock)
      assert(result == elizDirectoryResultMock)

    "not merge DirectoryResult if directorySourcesPriority has a single source" in new ElizHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(ListSet("internal"))

      val result: DirectoryResult = logic.mergeContacts(elizDirectoryResultMock)
      assert(result == elizDirectoryResultMock)

    "maintain order from dird even for unmerged results" in new ElizHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(
        ListSet("anotherLdap", "anotherInternal")
      )
      when(xucConfig.directoryInternalSource).thenReturn(ListSet.empty)

      val sortedResult: DirSearchResult = dirSearchResult(
        items = List(
          ecampbellInternal,
          eswannInternal,
          ecampbellLdap,
          eswannInternalWithoutEmail,
          ecampbellLdapWithoutEmail
        )
      )

      val result: DirSearchResult =
        logic.mergeDirSearchResult(elizDirSearchResult)
      assert(result == sortedResult)

    "maintain order from dird through merge" in new ElizHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(
        ListSet("ldap", "internal")
      )
      when(xucConfig.directoryInternalSource).thenReturn(
        ListSet("internal", "ldap")
      )
      val elizabethComstock: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayViewFR(
          name = ContactSheetValueString("Elizabeth Comstock")
        )
      )
      val eliseSpider: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayViewFR(
          name = ContactSheetValueString("Elise Spider"),
          mail = ContactSheetValueWithLabel("E-mail", "elise@lol.com")
        ),
        source = List(dirSource("internal", Some("135")))
      )
      val lizzyWizzy: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayViewFR(
          name = ContactSheetValueString("Lizzy Wizzy"),
          mail = ContactSheetValueWithLabel("E-mail", "lizzy.wizzy@cdpr.com")
        ),
        source = List(dirSource("ldap", Some("77")))
      )

      val elizLargeDirSearchResult: DirSearchResult = dirSearchResult(
        items = List(
          eswannInternal.copy(rank = 0),
          ecampbellInternal.copy(rank = 1),
          elizabethComstock.copy(rank = 2),
          eliseSpider.copy(rank = 3),
          ecampbellLdap.copy(rank = 4),
          lizzyWizzy.copy(rank = 5)
        )
      )
      val expectedMergedResult: DirSearchResult = dirSearchResult(
        items = List(
          eswannInternal.copy(rank = 0),
          ecampbellMerged.copy(rank = 1),
          elizabethComstock.copy(rank = 2),
          eliseSpider.copy(rank = 3),
          lizzyWizzy.copy(rank = 5)
        )
      )

      val mergedResult: DirSearchResult =
        logic.mergeDirSearchResult(elizLargeDirSearchResult)
      assert(mergedResult == expectedMergedResult)

    "not merge emailless contacts from different sources" in new ElizHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(
        ListSet("ldap", "internal")
      )
      when(xucConfig.directoryInternalSource).thenReturn(
        ListSet("internal", "ldap")
      )
      val emailLessDirSearchResult: DirSearchResult = dirSearchResult(
        items = List(
          eswannInternalWithoutEmail,
          ecampbellLdapWithoutEmail
        )
      )
      val expectedMergedResult: DirSearchResult = emailLessDirSearchResult

      val mergedResult: DirSearchResult =
        logic.mergeDirSearchResult(emailLessDirSearchResult)
      assert(mergedResult == expectedMergedResult)

    "merge a DirectoryResult (DirLookupResult)" in new ElizHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(
        ListSet("ldap", "internal")
      )
      when(xucConfig.directoryInternalSource).thenReturn(
        ListSet("internal", "ldap")
      )
      val elizDirLookupResult: DirLookupResult = DirLookupResult(
        result = elizDirSearchResult,
        toContactSheet = true
      )
      val expectedMergedResult: DirectoryResult = elizDirLookupResult.copy(
        result = dirSearchResult(
          items = List(
            ecampbellMerged,
            eswannInternal,
            eswannInternalWithoutEmail,
            ecampbellLdapWithoutEmail
          )
        )
      )

      val mergedResult: DirectoryResult =
        logic.mergeContacts(elizDirLookupResult)
      assert(mergedResult == expectedMergedResult)

    "merge a DirectoryResult (Favorites)" in new ElizHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(
        ListSet("ldap", "internal")
      )
      when(xucConfig.directoryInternalSource).thenReturn(
        ListSet("internal", "ldap")
      )
      val elizFavorites: Favorites = Favorites(
        result = elizDirSearchResult,
        toContactSheet = true
      )
      val expectedMergedResult: DirectoryResult = elizFavorites.copy(
        result = dirSearchResult(
          items = List(
            ecampbellMerged,
            eswannInternal,
            eswannInternalWithoutEmail,
            ecampbellLdapWithoutEmail
          )
        )
      )

      val mergedResult: DirectoryResult = logic.mergeContacts(elizFavorites)
      assert(mergedResult == expectedMergedResult)

    class AliceHelper extends Helper:
      val aliceMail: ContactSheetValueWithLabel = ContactSheetValueWithLabel(
        label = "email",
        value = "alice@wonderland.com"
      )
      val aliceAlphaSource: DirSource = DirSource(
        id = "alpha",
        entry = Some("123")
      )
      val aliceBetaSource: DirSource = DirSource(
        id = "beta",
        entry = Some("456")
      )
      val aliceGammaSource: DirSource = DirSource(
        id = "gamma",
        entry = Some("789")
      )
      val aliceOmegaSource: DirSource = DirSource(
        id = "omega",
        entry = Some("0")
      )

    "not merge contacts without sources nor contact with several occurrence of the same source" in new AliceHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(
        ListSet("alpha", "beta", "gamma")
      )
      when(xucConfig.directoryInternalSource).thenReturn(
        ListSet("alpha", "beta", "gamma")
      )
      val aliceAlpha: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          subtitle1 = ContactSheetValueString(" "),
          subtitle2 = ContactSheetValueString("  "),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource)
      )
      val aliceAlphaTwo: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Disney"),
          subtitle1 = ContactSheetValueString(" "),
          subtitle2 = ContactSheetValueString("  "),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource)
      )
      val aliceBeta: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Cooper"),
          subtitle1 =
            ContactSheetValueString("American rock singer and songwriter"),
          subtitle2 = ContactSheetValueString("  "),
          mail = aliceMail
        ),
        source = List(aliceBetaSource)
      )
      val aliceGamma: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Gulliman"),
          subtitle1 = ContactSheetValueString("Strong Alice"),
          subtitle2 = ContactSheetValueString("Commander"),
          mail = aliceMail
        ),
        source = List(aliceGammaSource)
      )
      val aliceUnsourced: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Winter"),
          subtitle1 = ContactSheetValueString(""),
          subtitle2 = ContactSheetValueString("  "),
          mail = aliceMail
        ),
        source = List.empty
      )
      val aliceMerged: DirSearchItem =
        dirSearchItem(
          item = contactSheetDisplayView(
            name = aliceBeta.item.name,
            subtitle1 = aliceBeta.item.subtitle1,
            subtitle2 = aliceGamma.item.subtitle2,
            mail = aliceMail
          ),
          source = List(aliceBetaSource, aliceGammaSource)
        )

      val aliceExpected: List[DirSearchItem] = List(
        aliceMerged,
        aliceUnsourced,
        aliceAlpha,
        aliceAlphaTwo
      )

      val mergedGroup: List[DirSearchItem] = logic
        .mergeDirSearchResult(
          dirSearchResult(items =
            List(
              aliceUnsourced,
              aliceAlpha,
              aliceAlphaTwo,
              aliceBeta,
              aliceGamma
            )
          )
        )
        .items
      assert(mergedGroup == aliceExpected)

    "complete merge with the default strategy for ContactSheetValueString" in new AliceHelper():
      val aliceAlpha: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          subtitle1 = ContactSheetValueString(" "),
          subtitle2 = ContactSheetValueString("  "),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource)
      )
      val aliceBeta: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Cooper"),
          subtitle1 =
            ContactSheetValueString("American rock singer and songwriter"),
          subtitle2 = ContactSheetValueString("  "),
          mail = aliceMail
        ),
        source = List(aliceBetaSource)
      )
      val aliceGamma: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice In Chains"),
          subtitle1 = ContactSheetValueString("American rock band"),
          subtitle2 = ContactSheetValueString("In Seattle"),
          mail = aliceMail
        ),
        source = List(aliceGammaSource)
      )
      val aliceMerged: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          subtitle1 =
            ContactSheetValueString("American rock singer and songwriter"),
          subtitle2 = ContactSheetValueString("In Seattle"),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource, aliceBetaSource, aliceGammaSource)
      )

      val mergedContact: DirSearchItem = ResultGrouped(
        List(
          aliceAlpha,
          aliceGamma,
          aliceBeta
        ),
        ListSet("alpha", "beta", "gamma"),
        ListSet("alpha", "beta", "gamma")
      ).toDirSearchItem.get
      assert(mergedContact == aliceMerged)

    "complete merge with the default strategy for ContactSheetValueWithLabel" in new AliceHelper():
      val aliceAlpha: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          title = contactSheetValueWithLabel(
            label = "Profession",
            value = "Wonderland explorer"
          ),
          phone = contactSheetValueWithLabel(),
          location = contactSheetValueWithLabel(),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource)
      )
      val aliceBeta: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Cooper"),
          title =
            contactSheetValueWithLabel(label = "Title", value = "Rock artist"),
          phone = contactSheetValueWithLabel(label = "Telephone"),
          location = contactSheetValueWithLabel(value = "United States"),
          mail = aliceMail
        ),
        source = List(aliceBetaSource)
      )
      val aliceGamma: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice In Chains"),
          title =
            contactSheetValueWithLabel(label = "Title", value = "Rock artist"),
          phone = contactSheetValueWithLabel(value = "666"),
          location =
            contactSheetValueWithLabel(label = "Country", value = "US"),
          mail = aliceMail
        ),
        source = List(aliceGammaSource)
      )
      val aliceMerged: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          title = contactSheetValueWithLabel(
            label = "Profession",
            value = "Wonderland explorer"
          ),
          phone =
            contactSheetValueWithLabel(label = "Telephone", value = "666"),
          location = contactSheetValueWithLabel(
            label = "Country",
            value = "United States"
          ),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource, aliceBetaSource, aliceGammaSource)
      )

      val mergedContact: DirSearchItem = ResultGrouped(
        List(
          aliceAlpha,
          aliceGamma,
          aliceBeta
        ),
        ListSet("alpha", "beta", "gamma"),
        ListSet("alpha", "beta", "gamma")
      ).toDirSearchItem.get

      assert(mergedContact == aliceMerged)

    "complete merge with the default strategy for ContactSheetValueBoolean" in new AliceHelper():
      val aliceAlpha: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          favorite = ContactSheetValueBoolean(true),
          personal = ContactSheetValueBoolean(false),
          meetingroom = ContactSheetValueBoolean(false),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource)
      )
      val aliceBeta: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Cooper"),
          favorite = ContactSheetValueBoolean(false),
          personal = ContactSheetValueBoolean(false),
          meetingroom = ContactSheetValueBoolean(false),
          mail = aliceMail
        ),
        source = List(aliceBetaSource)
      )
      val aliceGamma: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice In Chains"),
          favorite = ContactSheetValueBoolean(true),
          personal = ContactSheetValueBoolean(true),
          meetingroom = ContactSheetValueBoolean(false),
          mail = aliceMail
        ),
        source = List(aliceGammaSource)
      )
      val aliceMerged: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          favorite = ContactSheetValueBoolean(true),
          personal = ContactSheetValueBoolean(true),
          meetingroom = ContactSheetValueBoolean(false),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource, aliceBetaSource, aliceGammaSource)
      )

      val mergedContact: DirSearchItem = ResultGrouped(
        List(
          aliceAlpha,
          aliceGamma,
          aliceBeta
        ),
        ListSet("alpha", "beta", "gamma"),
        ListSet("alpha", "beta", "gamma")
      ).toDirSearchItem.get

      assert(mergedContact == aliceMerged)

    "complete merge with the default strategy for Relations" in new AliceHelper():
      val aliceAlpha: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          mail = aliceMail
        ),
        relations(
          agentId = Some(123),
          endpointId = None,
          userId = None,
          xivoId = None
        ),
        source = List(aliceAlphaSource)
      )
      val aliceBeta: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Cooper"),
          mail = aliceMail
        ),
        relations(
          agentId = Some(456),
          endpointId = None,
          userId = None,
          xivoId = Some("cooperId")
        ),
        source = List(aliceBetaSource)
      )
      val aliceGamma: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice In Chains"),
          mail = aliceMail
        ),
        relations(
          agentId = None,
          endpointId = None,
          userId = Some(789),
          xivoId = Some("chainsId")
        ),
        source = List(aliceGammaSource)
      )
      val aliceMerged: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          mail = aliceMail
        ),
        relations = relations(
          agentId = Some(123),
          endpointId = None,
          userId = Some(789),
          xivoId = Some("cooperId")
        ),
        source = List(aliceAlphaSource, aliceBetaSource, aliceGammaSource)
      )

      val mergedContact: DirSearchItem = ResultGrouped(
        List(
          aliceAlpha,
          aliceGamma,
          aliceBeta
        ),
        ListSet("alpha", "beta", "gamma"),
        ListSet("alpha", "beta", "gamma")
      ).toDirSearchItem.get

      assert(mergedContact == aliceMerged)

    "update merge with specific internal source" in new AliceHelper():
      val aliceAlpha: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          phone = contactSheetValueWithLabel(label = "phone", value = "123"),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource)
      )
      val aliceBeta: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Cooper"),
          phone = contactSheetValueWithLabel(label = "tel", value = "456"),
          mail = aliceMail
        ),
        source = List(aliceBetaSource)
      )
      val aliceGamma: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice In Chains"),
          phone = contactSheetValueWithLabel(label = "redacted"),
          mail = aliceMail
        ),
        source = List(aliceGammaSource)
      )
      val aliceMerged: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          phone = contactSheetValueWithLabel(label = "phone", value = "123"),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource, aliceBetaSource, aliceGammaSource)
      )
      val aliceUpdated: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          phone = contactSheetValueWithLabel(label = "redacted", value = "456"),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource, aliceBetaSource, aliceGammaSource)
      )

      val updatedMergedContact: DirSearchItem = ResultGrouped(
        List(
          aliceAlpha,
          aliceGamma,
          aliceBeta
        ),
        ListSet("alpha", "beta", "gamma"),
        ListSet("gamma", "beta", "alpha")
      ).toDirSearchItem.get
      assert(updatedMergedContact == aliceUpdated)

    "append unmerged contacts behind group" in new AliceHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(
        ListSet("alpha", "beta", "gamma")
      )
      when(xucConfig.directoryInternalSource).thenReturn(
        ListSet("alpha", "beta", "gamma")
      )

      val aliceAlpha: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource)
      )
      val aliceBeta: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Cooper"),
          mail = aliceMail
        ),
        source = List(aliceBetaSource)
      )
      val aliceGamma: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice In Chains"),
          mail = aliceMail
        ),
        source = List(aliceGammaSource)
      )
      val aliceOmega: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Is Missing"),
          mail = aliceMail
        ),
        source = List(aliceOmegaSource)
      )
      val aliceMerged: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource, aliceBetaSource, aliceGammaSource)
      )
      val expectedGroup: List[DirSearchItem] = List(aliceMerged, aliceOmega)

      val mergedGroup: List[DirSearchItem] = logic
        .mergeDirSearchResult(
          dirSearchResult(items =
            List(
              aliceAlpha,
              aliceOmega,
              aliceGamma,
              aliceBeta
            )
          )
        )
        .items
      assert(mergedGroup == expectedGroup)

    "return the single merged contact in a list" in new AliceHelper():
      when(xucConfig.directorySourcesPriority).thenReturn(
        ListSet("alpha", "beta", "gamma", "omega")
      )
      val aliceAlpha: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          mail = aliceMail
        ),
        source = List(aliceAlphaSource)
      )
      val aliceBeta: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Cooper"),
          mail = aliceMail
        ),
        source = List(aliceBetaSource)
      )
      val aliceGamma: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice In Chains"),
          mail = aliceMail
        ),
        source = List(aliceGammaSource)
      )
      val aliceOmega: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Is Missing"),
          mail = aliceMail
        ),
        source = List(aliceOmegaSource)
      )
      val aliceMerged: DirSearchItem = dirSearchItem(
        item = contactSheetDisplayView(
          name = ContactSheetValueString("Alice Liddel"),
          mail = aliceMail
        ),
        source = List(
          aliceAlphaSource,
          aliceBetaSource,
          aliceGammaSource,
          aliceOmegaSource
        )
      )
      val expectedGroup: List[DirSearchItem] = List(aliceMerged)

      val mergedGroup: Option[DirSearchItem] = ResultGrouped(
        List(aliceAlpha, aliceOmega, aliceGamma, aliceBeta),
        ListSet("alpha", "beta", "gamma", "omega"),
        ListSet("alpha", "beta", "gamma", "omega")
      ).toDirSearchItem
      assert(mergedGroup.toList == expectedGroup)
  }
}
