package services.directory

import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.*
import org.scalatestplus.mockito.MockitoSugar
import DirectoryTransformer.{EnrichDirectoryResult, RawDirectoryResult}
import models.ResultType.*
import xivo.services.XivoDirectory.{Action, DirLookupResult, DirectoryResult, FavoriteUpdated}
import xivo.websocket.WebSocketEvent
import xivo.websocket.WsBus.WsContent
import org.mockito.Mockito.{verify, when}
import fixtures.DirectoryFixtures
import services.config.ConfigDispatcher.{MonitorPhoneHint, MonitorVideoStatus}

class DirectoryTransformerSpec
    extends TestKitSpec("DirectoryTransformerSpec")
    with MockitoSugar {

  class Helper extends DirectoryFixtures {
    val testCtiLink: TestProbe            = TestProbe()
    val testCtiFilter: TestProbe          = TestProbe()
    val configDispatcher: TestProbe       = TestProbe()
    val agentConfig: TestProbe            = TestProbe()
    val amiBusConnector: TestProbe        = TestProbe()
    val callHistoryManager: TestProbe     = TestProbe()
    val xivoDirectoryInterface: TestProbe = TestProbe()
    val callbackMgrInterface: TestProbe   = TestProbe()

    val logic: DirectoryTransformerLogic = mock[DirectoryTransformerLogic]
    val contactSheetTransformer: ContactSheetTransformer =
      mock[ContactSheetTransformer]
    def actor(): (TestActorRef[DirectoryTransformer], DirectoryTransformer) = {
      val a = TestActorRef(
        new DirectoryTransformer(
          logic,
          contactSheetTransformer,
          configDispatcher.ref
        )
      )
      (a, a.underlyingActor)
    }

    val (ref, _) = actor()
    val requester: TestProbe = TestProbe()
    val listPhoneNumbers: List[String] = List.empty
    val listUsernames: List[String] = List.empty
    val requesterUserId: Int = 42
    val richEntries: List[RichEntry] = List(richEntry())
    val contactSheet: ContactSheetWrapper = ContactSheetWrapper(List(contactSheet(name = "Elizabeth Campbell")))

    val mergedDirectoryResult: DirLookupResult = mock[DirLookupResult] //needed because WebSocketEvent.createEvent is not a mock, so it infers ResultType.Research
    when(mergedDirectoryResult.result).thenReturn(dirSearchResult())
    when(mergedDirectoryResult.toContactSheet).thenReturn(false)

    val directoryResult: DirectoryResult = mock[DirectoryResult]
    when(logic.mergeContacts(directoryResult)).thenReturn(mergedDirectoryResult)

  }

  "DirectoryTransformer" should {

    """on DirectoryResult without toContactSheet send presence information with RichEntry and send RichResult to WebsocketEvent""".stripMargin in new Helper():
      val headerList: List[String] =
        List("name", "number", "mobile", "external_number", "favorite", "email")
      when(logic.getEnrichResult(mergedDirectoryResult.result,requesterUserId)).thenReturn(richEntries)
      when(logic.getPhoneNumbers(mergedDirectoryResult.result.items)).thenReturn(listPhoneNumbers)
      when(logic.getUsernames(richEntries)).thenReturn(listUsernames)
      val expectedRichResult: RichResult = new RichResult(
        headers = headerList
      )
      expectedRichResult.entries = richEntries

      ref ! RawDirectoryResult(
        requester.ref,
        directoryResult,
        requesterUserId
      )
      configDispatcher.expectMsg(
        MonitorPhoneHint(
          requester.ref,
          listPhoneNumbers
        )
      )
      configDispatcher.expectMsg(
        MonitorVideoStatus(
          requester.ref,
          listUsernames
        )
      )
      requester.expectMsg(WsContent(WebSocketEvent.createEvent(expectedRichResult, ResultType.Research)))

    """on DirectoryResult with toContactSheet send presence information with RichEntry and send ContactSheetWrapper to WebsocketEvent""".stripMargin in new Helper():
      when(mergedDirectoryResult.toContactSheet).thenReturn(true)
      when(logic.getEnrichResult(mergedDirectoryResult.result, requesterUserId)).thenReturn(richEntries)
      when(logic.getPhoneNumbers(mergedDirectoryResult.result.items)).thenReturn(listPhoneNumbers)
      when(logic.getUsernames(richEntries)).thenReturn(listUsernames)
      when(
        contactSheetTransformer.mapDirSearchResultToContactSheet(
          mergedDirectoryResult.result.items,
          requesterUserId
        )
      )
        .thenReturn(contactSheet)

      ref ! RawDirectoryResult(
        requester.ref,
        directoryResult,
        requesterUserId
      )
      configDispatcher.expectMsg(
        MonitorPhoneHint(
          requester.ref,
          listPhoneNumbers
        )
      )
      configDispatcher.expectMsg(
        MonitorVideoStatus(
          requester.ref,
          listUsernames
        )
      )
      requester.expectMsg(
        WsContent(
          WebSocketEvent.createEvent(contactSheet, ResultType.Research)
        )
      )

    "on FavoriteUpdated send it to WebsocketEvent" in new Helper():
      val searchResult: FavoriteUpdated =
        FavoriteUpdated(Action.Added, "contactId", "sourceDirectory")

      ref ! RawDirectoryResult(requester.ref, searchResult, 42)
      requester.expectMsg(WsContent(WebSocketEvent.createEvent(searchResult)))

    "on EnrichDirectory enrich DirSearchResult then send it back" in new Helper():
      val searchResult: DirSearchResult = mock[DirSearchResult]
      val richResult = new RichResult(List("testHeader"))
      when(logic.getEnrichResult(searchResult, requesterUserId))
        .thenReturn(richResult.entries)
      val expectedEntries: List[RichEntry] = richResult.entries

      ref ! EnrichDirectoryResult(searchResult, requesterUserId)
      verify(logic).getEnrichResult(searchResult, requesterUserId)
      val returnedEntries: List[RichEntry] = expectMsgClass(classOf[RichResult]).entries
      assert(returnedEntries == expectedEntries)
  }

}
