package services.agent

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.joda.time.DateTime
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import services.AgentStateFSM._
import services.XucEventBus
import stats.Statistic.ResetStat
import us.theatr.pekko.quartz.AddCronSchedule
import xivo.events.AgentState.AgentOnPause
import xivo.events.{AgentState, CallDirection}
import xivo.xucstats.XucBaseStatsConfig

class AgentStatCollectorSpec
    extends TestKitSpec("AgentStatCollectorSpec")
    with MockitoSugar {

  class Helper() {
    val eventBus: XucEventBus    = mock[XucEventBus]
    val agentId                  = 54
    val cronScheduler: TestProbe = TestProbe()
    val mockRegister: (AgentState, StatCollector) => Unit =
      mock[(AgentState, StatCollector) => Unit]
    var calculators: List[AgentStatCalculator] = List()
    val statConfig: XucBaseStatsConfig         = mock[XucBaseStatsConfig]

    trait TestStatRegistrar extends StatRegistrar {
      override val getStats: List[AgentStatCalculator] = calculators
      override def register(
          astate: AgentState,
          statCollector: StatCollector
      ): Unit =
        mockRegister(astate, statCollector)
    }

    when(statConfig.resetSchedule).thenReturn("0 32 0 * * ?")

    def actor(
        calcs: List[AgentStatCalculator] = List()
    ): (TestActorRef[AgentStatCollector], AgentStatCollector) = {
      calculators = calcs
      val a = TestActorRef[AgentStatCollector](
        Props(
          new AgentStatCollector(
            agentId,
            eventBus,
            cronScheduler.ref,
            statConfig
          ) with TestStatRegistrar
        )
      )
      (a, a.underlyingActor)
    }
  }

  "an agent stat collector" should {
    """self register to quartz scheduler
       subscribe to event bus on agent state
       subscribe to event bus on agent transition
       subscribe to event bus on agent statistics""" in new Helper {
      val (ref, _) = actor()

      val cronSchedule: AddCronSchedule =
        AddCronSchedule(ref, "0 32 0 * * ?", ResetStat)

      cronScheduler.expectMsg(cronSchedule)
      verify(eventBus).subscribe(ref, XucEventBus.agentEventTopic(agentId))
      verify(eventBus).subscribe(ref, XucEventBus.agentTransitionTopic(agentId))
      verify(eventBus).subscribe(ref, XucEventBus.statEventTopic(agentId))
    }

    "register stat calculator" in new Helper {
      val (ref, statCollector)                     = actor()
      val agentStatCalculator: AgentStatCalculator = mock[AgentStatCalculator]
      when(agentStatCalculator.name).thenReturn("testName")

    }

    "register and forward any events to agent stat calculator" in new Helper {
      val stateEvent: AgentState = mock[AgentState]
      val agentStatCalculator: AgentStatCalculatorByEvent =
        mock[AgentStatCalculatorByEvent]
      when(agentStatCalculator.name).thenReturn("testName")
      val (ref, statCollector) = actor(List(agentStatCalculator))

      ref ! stateEvent

      verify(agentStatCalculator).processEvent(stateEvent)
      verify(mockRegister)(stateEvent, statCollector)

    }

    "forward transitions to agent stat calculator" in new Helper {
      val fromContext: AgentStateContext = AgentStateContext(None, None, None)
      val toContext: AgentStateContext = AgentStateContext(
        None,
        None,
        Some(AgentCall(AgentCallState.OnCall, false, CallDirection.Incoming))
      )
      val transition: AgentTransition =
        AgentTransition(MAgentReady, fromContext, MAgentReady, toContext)
      val agentStatCalculator: AgentStatCalculatorByTransition =
        mock[AgentStatCalculatorByTransition]
      when(agentStatCalculator.name).thenReturn("testName")
      val (ref, statCollector) = actor(List(agentStatCalculator))

      ref ! transition

      verify(agentStatCalculator).processTransition(transition)
    }

    "on statcalculator changed publish event on bus" in new Helper {
      val (ref, statCollector)                     = actor()
      val agentStatCalculator: AgentStatCalculator = mock[AgentStatCalculator]

      val statValue: StatValue = mock[StatValue]

      statCollector.onStatCalculated("statname", statValue)

      verify(eventBus).publish(
        AgentStatistic(agentId, List(Statistic("statname", statValue)))
      )

    }

    "call reset stat on reset event" in new Helper {
      val agentStatCalculator: AgentStatCalculator = mock[AgentStatCalculator]
      val (ref, statCollector)                     = actor(List(agentStatCalculator))

      ref ! ResetStat

      verify(agentStatCalculator).reset()
    }
  }

  class RegistrarHelper {
    val statCollector: AgentStatCollector = mock[AgentStatCollector]
    class Reg extends StatRegistrar {}
    val aRegistrar = new Reg()
  }

  "a registrar" should {
    "register a stat for event on pause with a cause" in new RegistrarHelper {

      aRegistrar.register(
        AgentOnPause(
          34,
          new DateTime,
          "1000",
          List(),
          Some("outtolunch"),
          "2000"
        ),
        statCollector
      )

      aRegistrar.getStats should contain(
        AgentPausedTotalTimeWithCause("outtolunch", statCollector)
      )
    }
    "no register twice with same cause" in new RegistrarHelper {
      val ap1: AgentOnPause = AgentOnPause(
        34,
        new DateTime,
        "1000",
        List(),
        Some("outtolunch"),
        "2000"
      )
      aRegistrar.register(ap1, statCollector)
      val calc1: AgentStatCalculator = aRegistrar.getStats(0)
      val ap2: AgentOnPause = AgentOnPause(
        34,
        new DateTime().plusSeconds(100),
        "1000",
        List(),
        Some("outtolunch"),
        agentNb = "2000"
      )
      aRegistrar.register(ap2, statCollector)
      val calc2: AgentStatCalculator = aRegistrar.getStats(0)

      calc2 should be theSameInstanceAs calc1
    }
  }
}
