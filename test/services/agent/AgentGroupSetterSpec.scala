package services.agent

import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import org.mockito.Mockito.verify
import org.scalatestplus.mockito.MockitoSugar
import services.config.ConfigDispatcher.RefreshAgent
import services.request.SetAgentGroup
import xivo.models.AgentFactory

class AgentGroupSetterSpec
    extends TestKitSpec("AgentGroupSetter")
    with MockitoSugar {

  class Helper {
    val configDispatcher: TestProbe = TestProbe()
    val agentGroup: AgentFactory    = mock[AgentFactory]

    def actor: (TestActorRef[AgentGroupSetter], AgentGroupSetter) = {
      val a = TestActorRef[AgentGroupSetter](
        Props(new AgentGroupSetter(agentGroup, configDispatcher.ref))
      )
      (a, a.underlyingActor)
    }
  }

  "An AgentGroupSetter" should {
    "use the agentGroup object to update agent group association" in new Helper {
      val (agentId, groupId)     = (54, 3)
      val (ref, setter)          = actor
      val request: SetAgentGroup = SetAgentGroup(agentId, groupId)

      ref ! request

      verify(agentGroup).moveAgentToGroup(agentId, groupId)
      configDispatcher.expectMsg(RefreshAgent(agentId))
    }
  }

}
