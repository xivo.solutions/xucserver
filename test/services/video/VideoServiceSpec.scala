package services.video

import org.apache.pekko.actor.{ActorRef, Props}
import org.apache.pekko.testkit.{TestActorRef, TestKit, TestProbe}
import pekkotest.TestKitSpec
import models.XivoUser
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import services.chat.ChatService.Username
import services.config.ConfigRepository
import services.video.VideoService.{VideoUser, VideoUserState}
import services.video.model.{
  MeetingRoomInvite,
  MeetingRoomInviteAckReply,
  VideoInviteAck
}
import services.VideoChat.{VideoChatStates, VideoStatus}

import scala.concurrent.duration.DurationInt

class VideoServiceSpec
    extends TestKitSpec("VideoServiceSpec")
    with MockitoSugar {

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class Helper {
    val configRepo: ConfigRepository = mock[ConfigRepository]

    val chatLinkFrom: TestProbe  = TestProbe()
    val ctiRouterFrom: TestProbe = TestProbe()
    val ctiRouterTo: TestProbe   = TestProbe()

    val userIdFrom    = 56
    val usernameFrom  = "jdoe"
    val firstNameFrom = "John"
    val lastNameFrom  = "Doe"

    val usernameTo  = "asample"
    val firstNameTo = "Alice"
    val lastNameTo  = "Sample"
    val userIdTo    = 43

    def createVideoUserState(
        id: Long,
        username: String,
        firstName: String,
        lastName: String,
        status: VideoStatus,
        ctiRouterRef: Option[ActorRef]
    ): VideoUserState =
      VideoUserState(
        VideoUser(username, Some(s"$firstName $lastName")),
        status,
        ctiRouterRef,
        XivoUser(
          id,
          None,
          None,
          firstName,
          Some(lastName),
          Some(username),
          None,
          None,
          None
        )
      )

    def actor(
        mockUsers: Map[
          Username,
          VideoChatStates[VideoUserState, VideoStatus, VideoUser]
        ] = Map.empty
    ): (TestActorRef[VideoService], VideoService) = {

      val a = TestActorRef[VideoService](
        Props(new VideoService(configRepo) {
          users = mockUsers
        })
      )
      (a, a.underlyingActor)
    }
  }

  "Video Service" should {
    "Send MeetingRoomInvite to the dest user router" in new Helper {
      val usersConnected: Map[Username, VideoUserState] = Map(
        usernameFrom -> createVideoUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          VideoStatus.Available,
          Some(ctiRouterFrom.ref)
        )
      )

      val (ref, _) = actor(usersConnected)

      val mrInvite: MeetingRoomInvite =
        MeetingRoomInvite(42, "some.token", usernameFrom, "jbond", "James Bond")

      ref ! mrInvite

      ctiRouterFrom expectMsg mrInvite
    }

    "Send MeetingRoomInviteAckReply to the des user router" in new Helper {
      val usersConnected: Map[Username, VideoUserState] = Map(
        usernameFrom -> createVideoUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          VideoStatus.Available,
          Some(ctiRouterFrom.ref)
        )
      )

      val (ref, _) = actor(usersConnected)

      val mrAck: MeetingRoomInviteAckReply =
        MeetingRoomInviteAckReply(
          42,
          usernameFrom,
          "James Bond",
          VideoInviteAck.ACK
        )

      ref ! mrAck

      ctiRouterFrom expectMsg mrAck
    }

    "Send a NACK to the sender if the dest user is not connected for a meetingRoom invitation" in new Helper {
      when(configRepo.getCtiUserDisplayName(usernameFrom))
        .thenReturn(Some("John Doe"))

      val (ref, _)          = actor(Map.empty)
      val sender: TestProbe = TestProbe()

      ref.tell(
        MeetingRoomInvite(
          42,
          "some.token",
          usernameFrom,
          "jbond",
          "James Bond"
        ),
        sender.ref
      )

      ctiRouterFrom expectNoMessage 100.millis
      sender expectMsg MeetingRoomInviteAckReply(
        42,
        "jbond",
        "John Doe",
        VideoInviteAck.NACK
      )
    }

    "Send a NACK to the sender if the dest user is unavailable for a meetingRoom invitation" in new Helper {
      val usersConnected: Map[Username, VideoUserState] = Map(
        usernameFrom -> createVideoUserState(
          userIdFrom,
          usernameFrom,
          "John",
          "Doe",
          VideoStatus.Unavailable,
          Some(ctiRouterFrom.ref)
        )
      )

      when(configRepo.getCtiUserDisplayName(usernameFrom))
        .thenReturn(Some("John Doe"))

      val (ref, _)                   = actor(usersConnected)
      val senderCtiRouter: TestProbe = TestProbe()

      ref.tell(
        MeetingRoomInvite(
          42,
          "some.token",
          usernameFrom,
          "jbond",
          "James Bond"
        ),
        senderCtiRouter.ref
      )

      ctiRouterFrom expectNoMessage 100.millis
      senderCtiRouter expectMsg MeetingRoomInviteAckReply(
        42,
        "jbond",
        "John Doe",
        VideoInviteAck.NACK
      )
    }
  }
}
