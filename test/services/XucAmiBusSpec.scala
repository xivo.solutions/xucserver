package services

import org.apache.pekko.testkit.TestProbe
import pekkotest.TestKitSpec
import models.XivoUser
import org.asteriskjava.manager.action.{MuteAudioAction, OriginateAction}
import org.asteriskjava.manager.event.{AgentCompleteEvent, AgentConnectEvent}
import org.asteriskjava.manager.response.CoreStatusResponse
import org.scalatest.BeforeAndAfter
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus.*
import services.calltracking.DeviceConferenceAction.getMeetmeOptions
import services.calltracking.SipDriver
import xivo.phonedevices.WebRTCDeviceBearer.MobileApp
import xivo.xucami.models.Channel.VarNames.ConferenceInvitation
import xivo.xucami.models.{CallerId, Channel, ChannelOptions}

class XucAmiBusSpec
    extends TestKitSpec("XucAmiBusSpec")
    with BeforeAndAfter
    with MockitoSugar
    with AmiEventHelper {

  var statsBus: XucAmiBus = scala.compiletime.uninitialized
  var actor: TestProbe    = scala.compiletime.uninitialized

  before {
    statsBus = new XucAmiBus
    actor = TestProbe()
  }

  object DummyOriginate extends OriginateActionRequest {
    val variables: Map[String, String] = Map.empty[String, String]

    def buildAction() = new OriginateAction()

    def buildDummyOriginate() = new OriginateAction()

    def checkAutoAnswer(
        action: OriginateAction,
        xivoHost: String,
        variablePrefix: String = "",
        driver: SipDriver.SipDriver,
        vendor: Option[String] = None
    ): Unit = {
      if (driver == SipDriver.SIP) {
        action.getVariables.get(
          s"${variablePrefix}SIPADDHEADER51"
        ) shouldBe s"Call-Info:<sip:$xivoHost>;answer-after=0"
        action.getVariables.get(
          s"${variablePrefix}SIPADDHEADER52"
        ) shouldBe "Alert-Info: xivo-autoanswer"
        action.getVariables.get(
          s"${variablePrefix}SIPADDHEADER53"
        ) shouldBe "Alert-Info: info = alert-autoanswer"
      } else if (driver == SipDriver.PJSIP) {
        action.getVariables.get(
          s"${variablePrefix}XIVO_AUTOANSWER_HDR1_NAME"
        ) shouldBe "Call-Info"
        action.getVariables.get(
          s"${variablePrefix}XIVO_AUTOANSWER_HDR1_CONTENT"
        ) shouldBe s"<sip:$xivoHost>;answer-after=0"
        if (vendor.contains("Aastra")) {
          action.getVariables.get(
            s"${variablePrefix}XIVO_AUTOANSWER_HDR2_NAME"
          ) shouldBe null
          action.getVariables.get(
            s"${variablePrefix}XIVO_AUTOANSWER_HDR2_CONTENT"
          ) shouldBe null
          action.getVariables.get(
            s"${variablePrefix}XIVO_AUTOANSWER_HDR3_NAME"
          ) shouldBe null
          action.getVariables.get(
            s"${variablePrefix}XIVO_AUTOANSWER_HDR3_CONTENT"
          ) shouldBe null
        } else {
          action.getVariables.get(
            s"${variablePrefix}XIVO_AUTOANSWER_HDR2_NAME"
          ) shouldBe "Alert-Info"
          action.getVariables.get(
            s"${variablePrefix}XIVO_AUTOANSWER_HDR2_CONTENT"
          ) shouldBe s"xivo-autoanswer"
          action.getVariables.get(
            s"${variablePrefix}XIVO_AUTOANSWER_HDR3_NAME"
          ) shouldBe "Alert-Info"
          action.getVariables.get(
            s"${variablePrefix}XIVO_AUTOANSWER_HDR3_CONTENT"
          ) shouldBe s"info = alert-autoanswer"
        }
        action.getVariables.get(
          s"${variablePrefix}X-XIVO-DEVICE"
        ) shouldBe "WebApp"
      }
    }
  }

  "XucAmiBus" should {

    "send event to the subscriber on channel" in {

      val mockEvent = AmiEvent(new AgentConnectEvent("test"))
      statsBus.subscribe(actor.ref, AmiType.AmiEvent)
      statsBus.publish(mockEvent)

      actor.expectMsg(mockEvent)
    }

    "not send event to the subscriber on other channel" in {
      val mockEvent = AmiEvent(new AgentCompleteEvent("test"))
      statsBus.subscribe(actor.ref, AmiType.AmiResponse)
      statsBus.publish(mockEvent)

      actor.expectNoMessage(expectMsgTimeout)
    }

    "send events to the subscriber on all channels" in {
      val amiEvent    = AmiEvent(new AgentCompleteEvent("test"))
      val amiResponse = AmiResponse((new CoreStatusResponse(), None))
      statsBus.subscribe(actor.ref, AmiType.separator)
      statsBus.publish(amiEvent)
      statsBus.publish(amiResponse)

      actor.expectMsgAllOf(amiEvent, amiResponse)
    }

    "not send events to the subscriber on empty string" in {
      val amiEvent    = AmiEvent(new AgentCompleteEvent("test"))
      val amiResponse = AmiResponse((new CoreStatusResponse(), None))
      statsBus.subscribe(actor.ref, "")
      statsBus.publish(amiEvent)
      statsBus.publish(amiResponse)

      actor.expectNoMessage(expectMsgTimeout)
    }

    "implement equality of case classes like ChannelEvent" in {
      val event1 = ChannelEvent(new Channel("1", "a", CallerId("b", "2"), "1"))
      val event2 = ChannelEvent(new Channel("1", "a", CallerId("b", "2"), "1"))
      event1.equals(event2) shouldBe true
    }

  }

  "OriginateActionRequest" should {
    "enable auto answer by adding variable to sip channel" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritChannelOnly,
        "SIP/abcd",
        None,
        SipDriver.SIP
      )
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "", SipDriver.SIP)
    }

    "enable auto answer by adding variable to sip channel and its children" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritChildren,
        "SIP/abcd",
        None,
        SipDriver.SIP
      )
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "_", SipDriver.SIP)
    }

    "enable auto answer by adding variable to sip channel and all its descendants" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritDescendants,
        "SIP/abcd",
        None,
        SipDriver.SIP
      )
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "__", SipDriver.SIP)
    }

    "enable auto answer by adding variable to pjsip channel" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritChannelOnly,
        "PJSIP/abcd",
        None,
        SipDriver.PJSIP
      )

      originate.getVariables.get(
        s"PJSIP_HEADER(add,Call-Info)"
      ) shouldBe s"<sip:$xivoHost>;answer-after=0"
      originate.getVariables.get(
        s"PJSIP_HEADER(add,Alert-Info)"
      ) shouldBe "info = alert-autoanswer;xivo-autoanswer"
    }

    "enable auto answer by adding variable to pjsip channel for Aastra device" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritChannelOnly,
        "PJSIP/abcd",
        None,
        SipDriver.PJSIP,
        Some("Aastra")
      )

      originate.getVariables.get(
        s"PJSIP_HEADER(add,Call-Info)"
      ) shouldBe s"<sip:$xivoHost>;answer-after=0"
      originate.getVariables.get(
        s"PJSIP_HEADER(add,Alert-Info)"
      ) shouldBe null
    }

    "enable auto answer by adding variable to pjsip channel and its children" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritChildren,
        "PJSIP/abcd",
        None,
        SipDriver.PJSIP
      )
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "_", SipDriver.PJSIP)
    }

    "enable auto answer by adding variable to pjsip channel and all its children (for Aastra)" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      val vendor    = Some("Aastra")
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritChildren,
        "PJSIP/abcd",
        None,
        SipDriver.PJSIP,
        vendor
      )
      DummyOriginate.checkAutoAnswer(
        originate,
        xivoHost,
        "_",
        SipDriver.PJSIP,
        vendor
      )

    }

    "enable auto answer by adding variable to pjsip channel and all its descendants" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritDescendants,
        "PJSIP/abcd",
        None,
        SipDriver.PJSIP
      )
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "__", SipDriver.PJSIP)
    }

    "enable auto answer by adding variable to pjsip channel and all its descendants (for Aastra)" in {
      val originate = DummyOriginate.buildDummyOriginate()
      val xivoHost  = "123.123.123.123"
      val vendor    = Some("Aastra")
      DummyOriginate.enableAutoAnswer(
        originate,
        xivoHost,
        InheritDescendants,
        "PJSIP/abcd",
        None,
        SipDriver.PJSIP,
        vendor
      )
      DummyOriginate.checkAutoAnswer(
        originate,
        xivoHost,
        "__",
        SipDriver.PJSIP,
        vendor
      )
    }
  }

  "OutboundDialAction" should {
    "build originate for SIP" in {
      val queueNumber = "3000"
      val destination = "1200"
      val skill       = "agent_sortant(agent=agent_67)"
      val variables   = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val xivoHost    = "192.168.12.145"
      val vendor      = None
      val action = OutBoundDialActionRequest(
        destination,
        skill,
        queueNumber,
        variables,
        xivoHost,
        123L,
        SipDriver.SIP,
        vendor
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/$queueNumber@default/n"
      originate.getContext shouldBe "default"
      originate.getExten shouldBe destination
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe destination
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getVariables.get("XIVO_QUEUESKILLRULESET") shouldBe skill
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "__", SipDriver.SIP)
      originate.getVariables.get(
        "__" + Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOutboundOriginate
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
      originate.getVariables.get("_XIVO_USERID") shouldBe "123"
    }

    "build originate for PJSIP" in {
      val queueNumber = "3000"
      val destination = "1200"
      val skill       = "agent_sortant(agent=agent_67)"
      val variables   = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val xivoHost    = "192.168.12.145"
      val vendor      = None
      val action = OutBoundDialActionRequest(
        destination,
        skill,
        queueNumber,
        variables,
        xivoHost,
        123L,
        SipDriver.PJSIP,
        vendor
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/$queueNumber@default/n"
      originate.getContext shouldBe "default"
      originate.getExten shouldBe destination
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe destination
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getVariables.get("XIVO_QUEUESKILLRULESET") shouldBe skill
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "__", SipDriver.PJSIP)
      originate.getVariables.get(
        "__" + Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOutboundOriginate
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
      originate.getVariables.get("_XIVO_USERID") shouldBe "123"
    }
  }

  "DialAction" should {
    "build originate" in {
      val sourceChannel = "SIP/456re"
      val destination   = "1200"
      val context       = "default"
      val callerId      = CallerId("James Bond", "1000")
      val variables     = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val xivoHost      = "192.168.12.145"
      val action = DialActionRequest(
        sourceChannel,
        context,
        callerId,
        destination,
        variables,
        xivoHost,
        SipDriver.SIP,
        Some("vendor")
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe sourceChannel
      originate.getContext shouldBe context
      originate.getExten shouldBe destination
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe destination
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "", SipDriver.SIP)
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get("XIVO_ORIG_CID_NUM") shouldBe "1000"
      originate.getVariables.get("XIVO_ORIG_CID_NAME") shouldBe "James Bond"
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
    }

    "build originate for sip uri" in {
      val sourceChannel = "SIP/456re"
      val destination   = "jbond@mi6.gov.uk"
      val callerContext = "default"
      val callerId      = CallerId("James Bond", "1000")
      val variables     = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val xivoHost      = "192.168.12.145"
      val action = DialActionRequest(
        sourceChannel,
        callerContext,
        callerId,
        destination,
        variables,
        xivoHost,
        SipDriver.SIP,
        Some("vendor")
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe sourceChannel
      originate.getContext shouldBe callerContext
      originate.getExten shouldBe "jbond"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe destination
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "", SipDriver.SIP)
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get(
        Channel.VarNames.XiVOSipDomain
      ) shouldBe "mi6.gov.uk"
      originate.getVariables.get("XIVO_ORIG_CID_NUM") shouldBe "1000"
      originate.getVariables.get("XIVO_ORIG_CID_NAME") shouldBe "James Bond"
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
    }
  }

  "DialFromMobileAction" should {
    "build originate" in {
      val destination  = "1200"
      val mobileNum    = "123456789"
      val variables    = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val xivoHost     = "192.168.12.145"
      val callerNumber = "12345"
      val callerName   = "Isaac Newton"
      val action = DialFromMobileActionRequest(
        mobileNum,
        destination,
        callerNumber,
        callerName,
        variables,
        xivoHost,
        1
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/$mobileNum@default/n"
      originate.getContext shouldBe DefaultContext
      originate.getExten shouldBe destination
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe "\"Isaac Newton\" <12345>"
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get(
        "_" + Channel.VarNames.xucCallSource
      ) shouldBe Channel.callSourceMobile
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
      originate.getVariables.get("XIVO_ORIG_CID_NAME") shouldBe "Isaac Newton"
      originate.getVariables.get("XIVO_ORIG_CID_NUM") shouldBe "12345"
      originate.getVariables.get("XIVO_USERID") shouldBe "1"
    }

    "build originate with empty caller name" in {
      val destination  = "1200"
      val mobileNum    = "123456789"
      val variables    = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val xivoHost     = "192.168.12.145"
      val callerNumber = "12345"
      val callerName   = ""
      val action = DialFromMobileActionRequest(
        mobileNum,
        destination,
        callerNumber,
        callerName,
        variables,
        xivoHost,
        1
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/$mobileNum@default/n"
      originate.getContext shouldBe DefaultContext
      originate.getExten shouldBe destination
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe callerNumber
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get(
        "_" + Channel.VarNames.xucCallSource
      ) shouldBe Channel.callSourceMobile
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
      originate.getVariables.get("XIVO_USERID") shouldBe "1"
    }
  }

  "DialFromQueueActionRequest" should {
    "build originate" in {
      val request = DialFromQueueActionRequest(
        "123456789",
        "33333",
        "Thomas",
        Map("foo" -> "bar"),
        "123.123.123.123"
      )
      val originate = request.buildAction()

      originate.getChannel shouldBe s"Local/${request.queueNumber}@default/n"
      originate.getContext shouldBe "default"
      originate.getExten shouldBe request.destination
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe request.callerId
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getVariables.get(
        "XUC_CALLTYPE"
      ) shouldBe Channel.callTypeValOutboundOriginate
      originate.getVariables.get("USR_foo") shouldBe "bar"
    }
  }

  "DialToQueueActionRequest" should {
    "build originate" in {
      val request = DialToQueueActionRequest(
        "123456789",
        "4000",
        "Thomas",
        Map("foo" -> "bar"),
        "123.123.123.123"
      )
      val originate = request.buildAction()

      originate.getChannel shouldBe s"Local/${request.destination}@default/n"
      originate.getContext shouldBe "default"
      originate.getExten shouldBe request.queueNumber
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe request.callerId
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getVariables.get(
        "XUC_CALLTYPE"
      ) shouldBe Channel.callTypeValOutboundOriginate
      originate.getVariables.get("__USR_foo") shouldBe "bar"
    }
  }

  "DialWithLocalChannelCommand" should {
    "build originate" in {

      val request = DialWithLocalChannelRequest(
        "SIP/abcd",
        "Jason Bourne",
        "1000",
        "1007",
        None,
        "123.123.123.123",
        123,
        "usercontext",
        Map.empty,
        SipDriver.SIP,
        None
      )
      val originate = request.buildAction()

      originate.getChannel shouldBe "SIP/abcd"
      originate.getApplication shouldBe "Dial"
      originate.getData.contains("Local/1007@usercontext/n") shouldBe true
      originate.getCallerId shouldBe "1007"
      originate.getTimeout shouldBe 18000L
      originate.getExten shouldBe null
      originate.getContext shouldBe null
      originate.getPriority shouldBe null
      DummyOriginate.checkAutoAnswer(
        originate,
        "123.123.123.123",
        "",
        SipDriver.SIP
      )
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get("_XIVO_USERID") shouldBe "123"
      originate.getVariables.get("_XIVO_ORIG_CID_NUM") shouldBe "1000"
      originate.getVariables.get("_XIVO_ORIG_CID_NAME") shouldBe "Jason Bourne"
      originate.getVariables.get("X-XIVO-DEVICE") shouldBe "WebApp"
    }

    "build originate with MobileApp X-XIVO-DEVICE header" in {

      val request = DialWithLocalChannelRequest(
        "SIP/abcd",
        "Jason Bourne",
        "1000",
        "1007",
        Some(MobileApp),
        "123.123.123.123",
        123,
        "usercontext",
        Map.empty,
        SipDriver.SIP,
        None
      )
      val originate = request.buildAction()

      originate.getChannel shouldBe "SIP/abcd"
      originate.getApplication shouldBe "Dial"
      originate.getData.contains("Local/1007@usercontext/n") shouldBe true
      originate.getCallerId shouldBe "1007"
      originate.getTimeout shouldBe 18000L
      originate.getExten shouldBe null
      originate.getContext shouldBe null
      originate.getPriority shouldBe null
      DummyOriginate.checkAutoAnswer(
        originate,
        "123.123.123.123",
        "",
        SipDriver.SIP
      )
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get("_XIVO_USERID") shouldBe "123"
      originate.getVariables.get("_XIVO_ORIG_CID_NUM") shouldBe "1000"
      originate.getVariables.get("_XIVO_ORIG_CID_NAME") shouldBe "Jason Bourne"
      originate.getVariables.get("X-XIVO-DEVICE") shouldBe "MobileApp"
    }

    "copy user variables" in {
      val variables = Map(
        "USR_1"     -> "Value 1",
        "USR_2"     -> "Value 2",
        "OTHER_VAR" -> "Value 3"
      )
      val request = DialWithLocalChannelRequest(
        "SIP/abcd",
        "Jason Bourne",
        "1000",
        "1007",
        None,
        "123.123.123.123",
        123,
        "usercontext",
        variables,
        SipDriver.SIP,
        None
      )
      val originate = request.buildAction()

      originate.getVariables.get("USR_1") shouldBe "Value 1"
      originate.getVariables.get("USR_2") shouldBe "Value 2"
      originate.getVariables.get("OTHER_VAR") shouldBe null
    }
  }

  "ListenCallbackMessage" should {
    "build originate" in {
      val sourceChannel = "SIP/456re"
      val xivoHost      = "192.168.12.145"
      val voiceMsgRef   = "1499074373.5"
      val variables     = Map("VAR1" -> "Value 1", "VAR2" -> "Value 2")
      val action = ListenCallbackMessageActionRequest(
        sourceChannel,
        voiceMsgRef,
        variables,
        xivoHost,
        SipDriver.SIP,
        None
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe sourceChannel
      originate.getContext shouldBe "cback_listen-message"
      originate.getExten shouldBe "s"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe "Message"
      DummyOriginate.checkAutoAnswer(originate, xivoHost, "", SipDriver.SIP)
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get("CBACK_VOICEMSG_ID") shouldBe "1499074373.5"
      originate.getVariables.get("USR_VAR1") shouldBe "Value 1"
      originate.getVariables.get("USR_VAR2") shouldBe "Value 2"
    }
  }

  "BrigeActionRequest" should {
    "build a BridgeAction" in {
      val c1 = "SIP/abcd-00001"
      val c2 = "SIP/efgh-00001"

      val action = BridgeActionRequest(c1, c2).buildAction()

      action.getChannel1 should be(c1)
      action.getChannel2 should be(c2)
    }
  }

  "ListenActionRequest" should {
    "build originate going to the ChanSpy application" in {
      val listener = "SIP/abcd"
      val listened = "SIP/efgh"
      val request  = ListenActionRequest(listener, listened)
      val action   = request.buildAction()

      action.getChannel shouldBe listener
      action.getAsync shouldBe true
      action.getData shouldBe s"$listened,bdqsS"
      action.getApplication shouldBe "ChanSpy"
    }
  }

  "BeepActionRequest" should {
    "build originate going to the PLayback application" in {
      val listened = "SIP/efgh"
      val request  = BeepActionRequest(listened)
      val action   = request.buildAction()

      action.getChannel shouldBe "Local/s@xivo-play-beep-to-agent"
      action.getAsync shouldBe true
      action.getData shouldBe s"beep"
      action.getApplication shouldBe "Playback"
      action.getVariables.get("XIVO_CHANNEL_TO_BEEP") shouldBe listened
    }
  }

  "QueueLogEventRequest" should {
    "build a QueueLogAction" in {
      val request =
        QueueLogEventRequest("cars", "MY_EVENT", "123456789.001", "Some data")
      val a = request.buildAction()

      a.getQueue shouldBe "cars"
      a.getEvent shouldBe "MY_EVENT"
      a.getUniqueId shouldBe "123456789.001"
      a.getMessage shouldBe "Some data"
      a.getInterface shouldBe "NONE"
    }

    "build a QueueLogAction with an interface" in {
      val request = QueueLogEventRequest(
        "cars",
        "MY_EVENT",
        "123456789.001",
        "Some data",
        Some("Agent/2000")
      )
      val a = request.buildAction()

      a.getQueue shouldBe "cars"
      a.getEvent shouldBe "MY_EVENT"
      a.getUniqueId shouldBe "123456789.001"
      a.getMessage shouldBe "Some data"
      a.getInterface shouldBe "Agent/2000"
    }
  }

  "QueuePauseActionRequest" should {
    "build a QueuePauseAction" in {
      val request =
        QueuePauseActionRequest("Local/id-2@agentcallback", Some("testReason"))
      val a = request.buildAction()

      a.getAction shouldBe "QueuePause"
      a.getInterface shouldBe "Local/id-2@agentcallback"
      a.getPaused shouldBe true
      a.getQueue shouldBe null
      a.getReason shouldBe "testReason"
    }

    "build a QueuePauseAction without reason" in {
      val request = QueuePauseActionRequest("Local/id-2@agentcallback")
      val a       = request.buildAction()

      a.getAction shouldBe "QueuePause"
      a.getInterface shouldBe "Local/id-2@agentcallback"
      a.getPaused shouldBe true
      a.getQueue shouldBe null
      a.getReason shouldBe null
    }
  }

  "QueuePauseRequest" should {
    "be of type QueuePauseActionRequest" in {
      QueuePauseRequest(
        mock[QueuePauseActionRequest]
      ).classifier shouldEqual XucAmiBus.AmiType.QueuePauseActionRequest
    }
  }

  "QueueUnpauseActionRequest" should {
    "build a QueueUnpauseAction" in {
      val request = QueueUnpauseActionRequest("Local/id-2@agentcallback")
      val a       = request.buildAction()

      a.getAction shouldBe "QueuePause"
      a.getInterface shouldBe "Local/id-2@agentcallback"
      a.getPaused shouldBe false
      a.getQueue shouldBe null
    }
  }

  "QueueUnpauseRequest" should {
    "be of type QueueUnpauseActionRequest" in {
      QueueUnpauseRequest(
        mock[QueueUnpauseActionRequest]
      ).classifier shouldEqual XucAmiBus.AmiType.QueueUnpauseActionRequest
    }
  }

  "GroupPauseRequest" should {
    "be of type QueuePauseActionRequest" in {
      GroupPauseRequest(
        mock[UserGroupPauseActionRequest]
      ).classifier shouldEqual XucAmiBus.AmiType.UserGroupPauseActionRequest
    }

    "build a UserGroupActionRequest" in {
      val userId = 10L
      val request = UserGroupPauseActionRequest(s"Local/$userId@usercallback", "support")
      val a       = request.buildAction()

      a.getAction shouldBe "QueuePause"
      a.getInterface shouldBe s"Local/$userId@usercallback"
      a.getPaused shouldBe true
      a.getQueue shouldBe "support"
    }
  }

  "GroupUnpauseRequest" should {
    "be of type QueueUnpauseActionRequest" in {
      GroupUnpauseRequest(
        mock[UserGroupUnpauseActionRequest]
      ).classifier shouldEqual XucAmiBus.AmiType.UserGroupUnpauseActionRequest
    }

    "build a UserGroupActionRequest" in {
      val userId = 10L
      val request = UserGroupUnpauseActionRequest(s"Local/$userId@usercallback", "support")
      val a = request.buildAction()

      a.getAction shouldBe "QueuePause"
      a.getInterface shouldBe s"Local/$userId@usercallback"
      a.getPaused shouldBe false
      a.getQueue shouldBe "support"
    }
  }

  "QueueStatusRequest" should {
    "build with member and provide type" in {
      val request = QueueStatusByMemberRequest("Agent/1002")
      val action  = request.buildAction()
      action.getMember shouldBe "Agent/1002"
      action.getAction shouldBe "QueueStatus"
    }
  }

  "MeetMeKickRequest" should {
    "build" in {
      val request = MeetMeKickRequest("3450", 34)
      val action  = request.buildAction()
      action.getCommand shouldBe "meetme kick 3450 34"
    }
  }

  "MeetMeDeafenRequest" should {
    "build" in {
      val request = MeetMeDeafenRequest("SIP/abcd", 34)
      val action  = request.buildAction()
      action.getChannel shouldBe "SIP/abcd"
      action.getDirection shouldBe MuteAudioAction.Direction.OUT
      action.getState shouldBe MuteAudioAction.State.MUTE
    }
  }

  "MeetMeUndeafenRequest" should {
    "build" in {
      val request = MeetMeUndeafenRequest("SIP/abcd", 34)
      val action  = request.buildAction()
      action.getChannel shouldBe "SIP/abcd"
      action.getDirection shouldBe MuteAudioAction.Direction.OUT
      action.getState shouldBe MuteAudioAction.State.UNMUTE
    }
  }

  "RetrieveQueueCallAction" should {
    "build originate" in {
      val sourceNumber  = "1200"
      val sourceName    = "Isaac Asimov"
      val sourceChannel = "SIP/abcd"
      val callerNumber  = "12345"
      val callerName    = "Graham Bell"
      val callerChannel = "SIP/defg"
      val callId        = "123456.789"
      val queueName     = Some("__switchboard")
      val agentNum      = Some("2100")
      val autoAnswer    = true

      val variables = Map[String, String]().empty
      val xivoHost  = "192.168.12.145"

      val action = RetrieveQueueCallActionRequest(
        sourceChannel,
        Some(sourceNumber),
        sourceName,
        callerChannel,
        callerNumber,
        Some(callerName),
        variables,
        xivoHost,
        callId,
        queueName,
        agentNum,
        autoAnswer,
        SipDriver.SIP,
        None
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe sourceChannel
      originate.getContext shouldBe "xivocc_switchboard_retrieve"
      originate.getExten shouldBe "s"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getPriority shouldBe 1
      originate.getCallerId shouldBe "\"Graham Bell\" <12345>"
      originate.getVariables.get("XIVO_CID_NAME") shouldBe callerName
      originate.getVariables.get("XIVO_CID_NUM") shouldBe callerNumber
      originate.getVariables.get("XIVO_CHANNEL") shouldBe callerChannel
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get("XIVO_ORIG_CID_NAME") shouldBe sourceName
      originate.getVariables.get("XIVO_ORIG_CID_NUM") shouldBe sourceNumber
    }
  }

  "InviteInConferenceActionRequest" should {
    "build originate when inviting extension as user" in {
      val guest                = "0102030405"
      val confNo               = "4000"
      val pin                  = Some("1234")
      val isAdmin              = false
      val marked               = false
      val leaveWhenMarkedLeave = false
      val context              = "default"
      val variables            = Map(("someVariable", "someValue"))
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        false,
        context,
        variables,
        None,
        None,
        getMeetmeOptions(isAdmin, false, marked, leaveWhenMarkedLeave)
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/$guest@$context"
      originate.getExten shouldBe s"conf-$confNo"
      originate.getContext shouldBe s"xivo-join-conf"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getVariables.get(
        Channel.VarNames.ConferenceOptions
      ) shouldBe s"dTM(default)"
      originate.getCallerId shouldBe s""""Conference" <$confNo>"""
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get(
        Channel.VarNames.ConferencePinVariable
      ) shouldBe pin.get
      originate.getVariables.get("XIVO_MEETMENUMBER") shouldBe confNo
      originate.getVariables.get("XIVO_MEETMEROLE") shouldBe "USER"
    }

    "build originate when inviting extension as admin" in {
      val guest                = "0102030405"
      val confNo               = "4000"
      val pin                  = Some("1234")
      val isAdmin              = true
      val marked               = false
      val leaveWhenMarkedLeave = false
      val context              = "default"
      val variables            = Map(("someVariable", "someValue"))
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        false,
        context,
        variables,
        None,
        None,
        getMeetmeOptions(isAdmin, false, marked, leaveWhenMarkedLeave)
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/$guest@$context"
      originate.getExten shouldBe s"conf-$confNo"
      originate.getContext shouldBe s"xivo-join-conf"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getVariables.get(
        Channel.VarNames.ConferenceOptions
      ) shouldBe s"dTaM(default)"
      originate.getCallerId shouldBe s""""Conference" <$confNo>"""
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get(
        Channel.VarNames.ConferencePinVariable
      ) shouldBe pin.get
      originate.getVariables.get("XIVO_MEETMENUMBER") shouldBe confNo
      originate.getVariables.get("XIVO_MEETMEROLE") shouldBe "ADMIN"
    }

    "build originate when inviting a sip uri as user" in {
      val guest                = "jbond@mi6.gov.uk"
      val confNo               = "4000"
      val pin                  = Some("1234")
      val isAdmin              = false
      val marked               = false
      val leaveWhenMarkedLeave = false
      val context              = "default"
      val variables            = Map(("someVariable", "someValue"))
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        false,
        context,
        variables,
        None,
        None,
        getMeetmeOptions(isAdmin, false, marked, leaveWhenMarkedLeave)
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/jbond@$context"
      originate.getContext shouldBe s"xivo-join-conf"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getVariables.get(
        Channel.VarNames.ConferenceOptions
      ) shouldBe s"dTM(default)"
      originate.getCallerId shouldBe s""""Conference" <$confNo>"""
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get(
        Channel.VarNames.XiVOSipDomain
      ) shouldBe "mi6.gov.uk"
      originate.getVariables.get(
        Channel.VarNames.ConferencePinVariable
      ) shouldBe pin.get
      originate.getVariables.get("XIVO_MEETMENUMBER") shouldBe confNo
      originate.getVariables.get("XIVO_MEETMEROLE") shouldBe "USER"
    }

    "set dialplan variables with child inheritance when some are passed" in {
      val guest   = "jbond@mi6.gov.uk"
      val confNo  = "4000"
      val pin     = Some("1234")
      val isAdmin = false
      val context = "default"
      val variables =
        Map(("someVariableA", "someValueA"), ("someVariableB", "someValueB"))
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        false,
        context,
        variables,
        None,
        None,
        ChannelOptions.MeetMe.Defaults
      )

      val originate = action.buildAction()

      originate.getVariables.get("_USR_someVariableA") shouldBe "someValueA"
      originate.getVariables.get("_USR_someVariableB") shouldBe "someValueB"
    }

    "set meetme invitation variables with child inheritance" in {
      val guest   = "jbond@mi6.gov.uk"
      val confNo  = "4000"
      val pin     = Some("1234")
      val isAdmin = false
      val context = "default"
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        false,
        context,
        Map(),
        None,
        None,
        ChannelOptions.MeetMe.Defaults
      )

      val originate = action.buildAction()

      originate.getVariables.get(ConferenceInvitation) shouldBe "true"
    }

    "build specific originate when asking for early join a user" in {
      val guest     = "0102030405"
      val confNo    = "4000"
      val pin       = Some("1234")
      val isAdmin   = false
      val context   = "default"
      val variables = Map(("someVariable", "someValue"))
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        true,
        context,
        variables,
        None,
        None,
        ChannelOptions.MeetMe.Defaults
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/conf-$confNo@xivo-join-conf"
      originate.getApplication shouldBe "Dial"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getData shouldBe s"Local/$guest@$context"
      originate.getCallerId shouldBe s""""Conference" <$confNo>"""
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get("XIVO_MEETMEPIN") shouldBe "1234"
      originate.getVariables.get("XIVO_ORIG_CID_NUM") shouldBe "0102030405"
      originate.getVariables.get("XIVO_ORIG_CID_NAME") shouldBe " "
    }

    "build specific originate when asking for early join a user and set callerid name" in {
      val guest     = "0102030405"
      val confNo    = "4000"
      val pin       = Some("1234")
      val isAdmin   = false
      val context   = "default"
      val variables = Map(("someVariable", "someValue"))
      val user =
        XivoUser(1, None, None, "User", Some("One"), None, None, None, None)
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        true,
        context,
        variables,
        None,
        Some(user),
        ChannelOptions.MeetMe.Defaults
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/conf-$confNo@xivo-join-conf"
      originate.getApplication shouldBe "Dial"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getData shouldBe s"Local/$guest@$context"
      originate.getCallerId shouldBe s""""Conference" <$confNo>"""
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get("XIVO_MEETMEPIN") shouldBe "1234"
      originate.getVariables.get("XIVO_ORIG_CID_NUM") shouldBe "0102030405"
      originate.getVariables.get("XIVO_ORIG_CID_NAME") shouldBe "User One"
    }

    "build specific originate when asking for early join an admin" in {
      val guest     = "0102030405"
      val confNo    = "4000"
      val pin       = Some("4321")
      val isAdmin   = true
      val context   = "default"
      val variables = Map(("someVariable", "someValue"))
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        true,
        context,
        variables,
        None,
        None,
        ChannelOptions.MeetMe.Defaults
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/conf-$confNo@xivo-join-conf"
      originate.getApplication shouldBe "Dial"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getData shouldBe s"Local/$guest@$context"
      originate.getCallerId shouldBe s""""Conference" <$confNo>"""
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get("XIVO_MEETMEPIN") shouldBe "4321"
      originate.getVariables.get("XIVO_ORIG_CID_NUM") shouldBe "0102030405"
      originate.getVariables.get("XIVO_ORIG_CID_NAME") shouldBe " "
    }

    "build specific originate when asking for early join a sip user" in {
      val guest     = "jbond@mi6.gov.uk"
      val confNo    = "4000"
      val pin       = Some("1234")
      val isAdmin   = false
      val context   = "default"
      val variables = Map(("someVariable", "someValue"))
      val action = InviteInConferenceActionRequest(
        guest,
        confNo,
        pin,
        isAdmin,
        true,
        context,
        variables,
        None,
        None,
        ChannelOptions.MeetMe.Defaults
      )

      val originate = action.buildAction()

      originate.getChannel shouldBe s"Local/conf-$confNo@xivo-join-conf"
      originate.getApplication shouldBe "Dial"
      originate.getTimeout shouldBe XucAmiBus.OriginateTimeout
      originate.getAsync shouldBe true
      originate.getData shouldBe s"Local/jbond@$context"
      originate.getCallerId shouldBe s""""Conference" <$confNo>"""
      originate.getVariables.get(
        Channel.VarNames.xucCallType
      ) shouldBe Channel.callTypeValOriginate
      originate.getVariables.get(
        Channel.VarNames.XiVOSipDomain
      ) shouldBe "mi6.gov.uk"
      originate.getVariables.get("XIVO_MEETMEPIN") shouldBe "1234"
      originate.getVariables.get(
        "XIVO_ORIG_CID_NUM"
      ) shouldBe "jbond@mi6.gov.uk"
      originate.getVariables.get("XIVO_ORIG_CID_NAME") shouldBe " "
    }

  }
}
