package services.group

import controllers.helpers.RequestError
import org.apache.pekko.actor.Props
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.MessageFactory
import pekkotest.TestKitSpec
import services.XucAmiBus.{GroupPauseRequest, GroupUnpauseRequest}
import services.XucEventBus
import services.config.ConfigRepository
import services.request.{BaseRequest, UserGroupPauseRequest, UserGroupUnpauseRequest}
import xivo.models.{GroupConfig, MembershipStatus, UserGroupMember}

class GroupActionSpec extends TestKitSpec("GroupAction") with MockitoSugar {
  val ctiLink: TestProbe = TestProbe()
  val configDispatcher: TestProbe = TestProbe()
  val mockMessageFactory: MessageFactory = mock[MessageFactory]
  val messageFactory = new MessageFactory()
  val configRepo: ConfigRepository = mock[ConfigRepository]
  val amiBusConnector: TestProbe = TestProbe()
  val eventBus: XucEventBus = mock[XucEventBus]

  class Helper {
    def actor: (TestActorRef[GroupAction], GroupAction) = {
      val a = TestActorRef[GroupAction](
        Props(
          new GroupAction(
            ctiLink.ref,
            mockMessageFactory,
            configRepo,
            eventBus,
            amiBusConnector.ref
          )
        )
      )
      (a, a.underlyingActor)
    }
  }

  "group action service" should {
    "send user group pause request to ami bus" in new Helper {
      val (ref, _) = actor
      val groupId = 12L
      val userId = 2L
      val group: GroupConfig = GroupConfig(List(UserGroupMember(userId, MembershipStatus.Available)), groupId, "support", "2000")
      when(configRepo.getGroup(groupId)).thenReturn(Some(group))

      ref ! BaseRequest(
        self,
        UserGroupPauseRequest(groupId, Some(userId))
      )

      val request: GroupPauseRequest = amiBusConnector.expectMsgType[GroupPauseRequest]
      request.message.iface shouldBe s"Local/id-$userId@usercallback"
      request.message.groupName shouldBe group.groupName
    }

    "return request error on pause request when no group found" in new Helper {
      val (ref, _) = actor
      val groupId = 12L
      val userId = 2L
      val group: GroupConfig = GroupConfig(List(UserGroupMember(userId, MembershipStatus.Available)), groupId, "support", "2000")
      when(configRepo.getGroup(groupId)).thenReturn(None)

      ref ! BaseRequest(
        self,
        UserGroupPauseRequest(groupId, Some(userId))
      )

      expectMsgClass(classOf[RequestError])
    }

    "send user group unpause request to ami bus" in new Helper {
      val (ref, _) = actor
      val groupId = 12L
      val userId = 2L
      val group: GroupConfig = GroupConfig(List(UserGroupMember(userId, MembershipStatus.Available)), groupId, "support", "2000")
      when(configRepo.getGroup(groupId)).thenReturn(Some(group))

      ref ! BaseRequest(
        self,
        UserGroupUnpauseRequest(groupId, Some(userId))
      )

      val request: GroupUnpauseRequest = amiBusConnector.expectMsgType[GroupUnpauseRequest]
      request.message.iface shouldBe s"Local/id-$userId@usercallback"
      request.message.groupName shouldBe group.groupName
    }

    "return request error on unpause request when no group found" in new Helper {
      val (ref, _) = actor
      val groupId = 12L
      val userId = 2L
      val group: GroupConfig = GroupConfig(List(UserGroupMember(userId, MembershipStatus.Available)), groupId, "support", "2000")
      when(configRepo.getGroup(groupId)).thenReturn(None)

      ref ! BaseRequest(
        self,
        UserGroupUnpauseRequest(groupId, Some(userId))
      )

      expectMsgClass(classOf[RequestError])
    }
  }
}

