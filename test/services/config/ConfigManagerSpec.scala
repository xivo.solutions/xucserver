package services.config

import org.apache.pekko.actor.{Actor, ActorRef, Props}
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import pekkotest.TestKitSpec
import models.{XivoUser, XivoUserDao, XucUser}
import org.mockito.Mockito.{reset, verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.xivo.cti.message.{
  AgentStatusUpdate,
  IpbxCommandResponse,
  PhoneStatusUpdate
}
import org.xivo.cti.model.{
  AgentStatus,
  Availability,
  PhoneHintStatus,
  StatusReason
}
import services.Start
import services.agent.AgentAction
import services.config.ConfigManager.{InitConfig, PublishUserPhoneStatuses}
import services.request.{AgentLogout, BaseRequest}
import xivo.events.{AgentLoginError, AgentQueues}
import xivo.network.CtiLinkKeepAlive.{StartKeepAlive, StopKeepALive}
import xivo.network.LoggedOn
import xivo.websocket.LinkState.up
import xivo.websocket.{LinkState, LinkStatusUpdate}
import xivo.xuc.XucBaseConfig
import xuctest.XucUserHelper

import java.util.Date
import scala.concurrent.Future
import scala.jdk.CollectionConverters.*
import org.scalatest.Retries
import org.scalatest.tagobjects.Retryable

class ConfigManagerSpec
    extends TestKitSpec("ConfigManagerSpec")
    with XucUserHelper
    with MockitoSugar
    with Retries {

  class ConfigManagerOptPrestart(
    agentActionFactory: AgentAction.Factory,
    agentManagerRef: ActorRef,
    configDispatcherRef: ActorRef,
    statusPublisherRef: ActorRef,
    config: XucBaseConfig,
    xivoUserDao: XivoUserDao,
    withPrestart: Boolean = false
  ) extends ConfigManager(
    agentActionFactory,
    agentManagerRef,
    configDispatcherRef,
    statusPublisherRef,
    config,
    xivoUserDao
  ) {
    override def preStart(): Unit = { if (withPrestart) super.preStart() else log.debug("Started ConfigManager with empty prestart") }
  }

  class Helper(withPrestart: Boolean = false) {
    val testLink: TestProbe           = TestProbe()
    val testKeepAlive: TestProbe      = TestProbe()
    val agentManager: TestProbe       = TestProbe()
    val configDispatcher: TestProbe   = TestProbe()
    val statusPublisher: TestProbe    = TestProbe()
    val agentActionService: TestProbe = TestProbe()
    val xivoUserDao: XivoUserDao      = mock[XivoUserDao]
    val agentActionFactory: AgentAction.Factory = new AgentAction.Factory {
      def apply(ctiLink: ActorRef): Actor =
        createActorProxy(agentActionService.ref)
    }
    val config: XucBaseConfig = mock[XucBaseConfig]

    def actor(
        user: XucUser,
        makeReady: Boolean = true
    ): (TestActorRef[ConfigManager], ConfigManager) = {
      reset(config)
      when(config.EventUser).thenReturn(user.username)
      when(xivoUserDao.getCtiUserByLogin(user.username))
        .thenReturn(Future.successful(user.xivoUser))
      val a = TestActorRef[ConfigManager](
        Props(
          new ConfigManagerOptPrestart(
            agentActionFactory,
            agentManager.ref,
            configDispatcher.ref,
            statusPublisher.ref,
            config,
            xivoUserDao,
            withPrestart
          )
        )
      )
      val configManager = a.underlyingActor
      if (makeReady) {
        configManager.context.become(
          configManager.ready(
            user,
            testLink.ref,
            agentActionService.ref,
            testKeepAlive.ref
          )
        )
      }
      (a, configManager)
    }
  }

  "Config manager" should {
    "load xuc user at startup" taggedAs Retryable in new Helper(withPrestart = true) {
      val user: XucUser = XucUser(
        "configManagerTest",
        XivoUser(
          0L,
          None,
          None,
          "EventUser",
          None,
          None,
          password = Some("4etr,wqd"),
          None,
          None
        )
      )

      val (ref, configManager) = actor(user, false)
      verify(xivoUserDao).getCtiUserByLogin("configManagerTest")

    }

    "send start message to ctilink on link status up" taggedAs Retryable in new Helper {
      val user: XucUser = XucUser(
        "configManagerTest",
        XivoUser(
          0L,
          None,
          None,
          "EventUser",
          None,
          None,
          password = Some("4etr,wqd"),
          None,
          None
        )
      )
      val (ref, configManager) = actor(user, false)
      configManager.context.become(
        configManager.ctiLinkDown(
          user,
          testLink.ref,
          agentActionService.ref,
          testKeepAlive.ref
        )
      )

      ref ! LinkStatusUpdate(up)
      testLink.expectMsg(Start(user))

    }

    "send stop message to ctilinkKeepAlive on ctiLinkStarting" taggedAs Retryable in new Helper {
      val user: XucUser        = getXucUser("configManagerTest", "4etr,wqd")
      val (ref, configManager) = actor(user, false)
      val loggedOn: LoggedOn   = LoggedOn(user, "4")

      configManager.context.become(
        configManager.ctiLinkStarting(
          user,
          testLink.ref,
          agentActionService.ref,
          testKeepAlive.ref
        )
      )

      ref ! LinkStatusUpdate(LinkState.up)

      testKeepAlive.expectMsg(
        StopKeepALive
      )
      testKeepAlive.expectMsg(
        StartKeepAlive(user.xivoUser.id.toString, testLink.ref)
      )
    }

    "on LoggedOn send init config to configDispatcher and start keepalive" taggedAs Retryable in new Helper {
      val user: XucUser        = getXucUser("configManagerTest", "4etr,wqd")
      val (ref, configManager) = actor(user, false)
      val loggedOn: LoggedOn   = LoggedOn(user, "4")

      configManager.context.become(
        configManager.ctiLinkStarting(
          user,
          testLink.ref,
          agentActionService.ref,
          testKeepAlive.ref
        )
      )

      ref ! loggedOn

      configDispatcher.expectMsg(InitConfig(testLink.ref))
      testKeepAlive.expectMsg(
        StartKeepAlive(loggedOn.userId, testLink.ref)
      )
    }

    "Send queues from agentStatus message to agent manager" taggedAs Retryable in new Helper {
      val (ref, configManager) =
        actor(getXucUser("configManagerTest", "4etr,wqd"))
      val agentStatus =
        new AgentStatus("2547", Availability.AVAILABLE, StatusReason.NONE)
      agentStatus.setQueues(List[Integer](1, 2, 5).asJava)
      val agentStatusUpdate = new AgentStatusUpdate(23, agentStatus)

      ref ! agentStatusUpdate

      agentManager.expectMsg(AgentQueues(23, List(1, 2, 5)))

    }
    "forward CtiMessage to configDispatcher" taggedAs Retryable in new Helper {
      val (ref, configManager) =
        actor(getXucUser("configManagerTest", "4etr,wqd"))
      val ctiMessage = new PhoneStatusUpdate()
      ref ! ctiMessage

      configDispatcher.expectMsg(ctiMessage)
    }

    "send user phone status to publisher" taggedAs Retryable in new Helper {
      val (ref, configManager) =
        actor(getXucUser("statusPublisher", "4etr,wqd"))

      val phoneStatus: UserPhoneStatus =
        UserPhoneStatus("user", PhoneHintStatus.getHintStatus(0))

      ref ! phoneStatus

      statusPublisher.expectMsg(phoneStatus)
    }
    "send user phone status refresh request to config dispatcher" taggedAs Retryable in new Helper {
      val (ref, configManager) =
        actor(getXucUser("userphoneStatusRefresh", "4etr,wqd"))

      ref ! PublishUserPhoneStatuses

      configDispatcher.expectMsg(PublishUserPhoneStatuses)

    }
    "forward base requests to agent action service" taggedAs Retryable in new Helper {
      val (ref, _) = actor(getXucUser("agentLogout", "4rrrre"))

      ref ! BaseRequest(self, AgentLogout("1011"))

      agentActionService.expectMsg(BaseRequest(self, AgentLogout("1011")))
    }
    "transform IpbxCommandResponse to AgentError and send it to agentActionService" taggedAs Retryable in new Helper {

      val (ref, _) = actor(getXucUser("agentLoginErro", "feerre"))

      ref ! new IpbxCommandResponse("testCause", new Date())

      agentActionService.expectMsg(AgentLoginError("Unknown:testCause"))

    }

  }
}
