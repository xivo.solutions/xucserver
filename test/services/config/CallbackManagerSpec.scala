package services.config

import java.util.UUID

import org.apache.pekko.actor.Props
import org.apache.pekko.pattern.ask
import org.apache.pekko.testkit.{TestActorRef, TestProbe}
import org.apache.pekko.util.Timeout
import pekkotest._
import controllers.helpers.{RequestError, RequestResult, RequestSuccess}
import models._
import org.joda.time.{DateTime, LocalDate, LocalTime}
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.time.{Millis, Seconds, Span}
import services.XucEventBus
import services.request.PhoneRequest.Dial
import services.request._
import xivo.events.{PhoneEvent, PhoneEventType}
import xivo.models.{Agent, QueueConfigUpdate}
import xivo.network.{RecordingWS, WebServiceException}
import xivo.websocket.WsBus.WsContent
import xivo.websocket.{WSMsgType, WebSocketEvent}

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import org.apache.pekko.actor.ActorRef

class CallbackManagerSpec
    extends TestKitSpec("ConfigServerSpec")
    with MockitoSugar
    with ScalaFutures {

  implicit val defaultPatience: PatienceConfig =
    PatienceConfig(timeout = Span(2, Seconds), interval = Span(5, Millis))

  class Helper {
    val requester: ConfigServerRequester = mock[ConfigServerRequester]
    val recordingws: RecordingWS         = mock[RecordingWS]
    val bus: XucEventBus                 = mock[XucEventBus]
    val configRepo: ConfigRepository     = mock[ConfigRepository]
    val router: TestProbe                = TestProbe()

    val routerFactory: ActorRef = system.actorOf(
      Props(createParentActor(createActorProxy(router.ref), "j.doeCtiRouter"))
    )

    def actor(): (TestActorRef[CallbackManager], CallbackManager) = {
      val a = TestActorRef(
        new CallbackManager(
          requester,
          recordingws,
          bus,
          configRepo,
          routerFactory
        )
      )
      (a, a.underlyingActor)
    }

  }

  "CallbackManager actor " should {
    "retrieve the callback lists" in new Helper {
      val listUuid: UUID = UUID.randomUUID()
      val cbUuid: UUID   = UUID.randomUUID()
      val callbacks: List[CallbackList] = List(
        CallbackList(
          Some(listUuid),
          "The List",
          3,
          List(
            CallbackRequest(
              Some(cbUuid),
              listUuid,
              Some("1000"),
              None,
              None,
              None,
              Some("The company"),
              None
            )
          )
        )
      )
      when(requester.getCallbackLists).thenReturn(Future.successful(callbacks))
      var (ref, a) = actor()

      ref ! BaseRequest(self, GetCallbackLists)

      expectMsg(CallbackLists(callbacks))
    }

    "find CallbackRequest list" in new Helper {
      val listUuid: UUID = UUID.randomUUID()
      val cbUuid: UUID   = UUID.randomUUID()
      val callbacks: List[CallbackRequest] = List(
        CallbackRequest(
          Some(cbUuid),
          listUuid,
          Some("1000"),
          None,
          None,
          None,
          Some("The company"),
          None
        )
      )

      val find: FindCallbackRequest = FindCallbackRequest(List.empty)
      val findWithId: FindCallbackRequestWithId =
        FindCallbackRequestWithId(1, find)
      val resp: FindCallbackResponse = FindCallbackResponse(1, callbacks)

      when(requester.findCallbackRequest(find))
        .thenReturn(Future.successful(resp))
      var (ref, a) = actor()

      ref ! BaseRequest(self, findWithId)

      expectMsg(FindCallbackResponseWithId(1, resp))
    }

    "retrieve the preferred periods" in new Helper {
      val listUuid: UUID   = UUID.randomUUID()
      val periodUuid: UUID = UUID.randomUUID()
      val periods: List[PreferredCallbackPeriod] = List(
        PreferredCallbackPeriod(
          Some(periodUuid),
          "morning",
          new LocalTime(8, 0, 0),
          new LocalTime(11, 0, 0),
          true
        )
      )
      when(requester.getPreferredCallbackPeriods())
        .thenReturn(Future.successful(periods))
      var (ref, a) = actor()

      ref ! BaseRequest(self, GetCallbackPreferredPeriods)

      expectMsg(PreferredCallbackPeriodList(periods))
    }

    "return an error if the webservice call fails" in new Helper {
      when(requester.getCallbackLists).thenReturn(
        Future.failed(new WebServiceException("Error message"))
      )
      var (ref, a) = actor()

      ref ! BaseRequest(self, GetCallbackLists)

      expectMsg(
        WsContent(WebSocketEvent.createError(WSMsgType.Error, "Error message"))
      )
    }

    "send the CSV, return RequestSuccess and publish the callback list on the bus" in new Helper {
      val listUuid: UUID = UUID.randomUUID()
      val cbUuid: UUID   = UUID.randomUUID()
      val csv            = ""
      val callbacks: List[CallbackList] = List(
        CallbackList(
          Some(listUuid),
          "The List",
          3,
          List(
            CallbackRequest(
              Some(cbUuid),
              listUuid,
              Some("1000"),
              None,
              None,
              None,
              Some("The company"),
              None
            )
          )
        )
      )
      when(requester.importCsvCallback(listUuid.toString, csv))
        .thenReturn(Future.successful(()))
      when(requester.getCallbackLists).thenReturn(Future.successful(callbacks))
      var (ref, a) = actor()

      ref ! ImportCsvCallback(listUuid.toString, csv)

      expectMsg(RequestSuccess("CSV successfully imported"))
      verify(bus, Mockito.timeout(5000)).publish(callbacks)
    }

    "send the CSV and return RequestError in case of error" in new Helper {
      val uuid: String = UUID.randomUUID().toString
      val csv          = ""
      when(requester.importCsvCallback(uuid, csv))
        .thenReturn(Future.failed(new WebServiceException("Error message")))
      var (ref, a) = actor()

      ref ! ImportCsvCallback(uuid.toString, csv)

      expectMsg(RequestError("Error message"))
    }

    "take a callback for an agent" in new Helper {
      val uuid: UUID   = UUID.randomUUID()
      val userName     = "doe"
      val agent: Agent = Agent(12L, "John", "Doe", "1000", "default")
      when(configRepo.agentFromUsername(userName)).thenReturn(Some(agent))
      when(requester.takeCallback(uuid.toString, agent))
        .thenReturn(Future.successful(()))
      var (ref, a) = actor()

      ref ! TakeCallbackWithAgent(uuid.toString, userName)

      verify(requester, Mockito.timeout(5000))
        .takeCallback(uuid.toString, agent)
      verify(bus, Mockito.timeout(5000)).publish(CallbackTaken(uuid, agent.id))
    }

    "release a callback" in new Helper {
      val uuid: UUID = UUID.randomUUID()
      when(requester.releaseCallback(uuid.toString))
        .thenReturn(Future.successful(()))
      var (ref, a) = actor()

      ref ! ReleaseCallback(uuid.toString)

      verify(requester, Mockito.timeout(5000)).releaseCallback(uuid.toString)
      verify(bus, Mockito.timeout(5000)).publish(CallbackReleased(uuid))
    }

    "release pending callbacks for an agent" in new Helper {
      val agent: Agent               = Agent(12L, "John", "Doe", "1000", "default")
      val uuid: UUID                 = UUID.randomUUID()
      val takenCallbacks: List[UUID] = List(uuid)

      when(requester.getTakenCallbacks(agent.id))
        .thenReturn(Future.successful(takenCallbacks))

      val f: Future[Unit] = Future.successful(())
      when(requester.releaseCallback(uuid.toString)).thenReturn(f)

      val (ref, _) = actor()

      ref ! BaseRequest(self, ReleaseAllCallbacks(agent.id))

      verify(bus, Mockito.timeout(5000)).publish(CallbackReleased(uuid))
    }

    """upon reception of StartCallbackWithUser:
      |- retrieve the callback request
      |- create a callback ticket
      |- dial the given number with the provided uuid
      |- return CallbackTaken""" in new Helper {
      val rqUuid: UUID             = UUID.randomUUID()
      val listUuid: UUID           = UUID.randomUUID()
      val ticketUuid: UUID         = UUID.randomUUID()
      val number                   = "3235648"
      val username                 = "j.doe"
      val agent: Agent             = Agent(12L, "John", "Doe", "1000", "default")
      val queue: QueueConfigUpdate = mock[QueueConfigUpdate]
      when(queue.name).thenReturn("thequeue")
      val request: CallbackRequest = CallbackRequest(
        Some(rqUuid),
        listUuid,
        Some("1000"),
        None,
        None,
        None,
        None,
        None,
        Some(agent.id),
        Some(12)
      )
      val ticket: CallbackTicket = CallbackTicket(
        Some(ticketUuid),
        listUuid,
        rqUuid,
        "thequeue",
        agent.number,
        request.dueDate,
        DateTime.now(),
        None
      )
      when(configRepo.agentFromUsername(username)).thenReturn(Some(agent))
      when(requester.getCallbackRequest(rqUuid.toString))
        .thenReturn(Future.successful(request))
      when(configRepo.getQueue(12)).thenReturn(Some(queue))
      when(recordingws.createCallbackTicket(any(classOf[CallbackTicket])))
        .thenReturn(Future.successful(ticket))
      var (ref, a) = actor()

      ref ! BaseRequest(
        self,
        StartCallbackWithUser(rqUuid.toString, "1000", "j.doe")
      )

      router.expectMsg(
        BaseRequest(
          self,
          Dial("1000", Map("CALLBACK_TICKET_UUID" -> ticketUuid.toString))
        )
      )
      expectMsg(CallbackStarted(rqUuid, ticketUuid))
    }

    "upon reception of UpdateCallbackTicket, transform it to CallbackTicketPatch and ask for update" in new Helper {
      val uuid: UUID = UUID.randomUUID()
      val msg: UpdateCallbackTicket = UpdateCallbackTicket(
        uuid.toString,
        Some(CallbackStatus.Answered),
        Some("Small comment")
      )
      var (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      verify(recordingws).updateCallbackTicket(
        uuid.toString,
        CallbackTicketPatch(
          Some(CallbackStatus.Answered),
          Some("Small comment"),
          None
        )
      )
    }

    """upon reception of UpdateCallbackTicket, if the status is not Callback:
      - cloture the associated request
      - send a CallbackClotured notification""" in new Helper {
      val ticketUuid: UUID  = UUID.randomUUID()
      val requestUuid: UUID = UUID.randomUUID()
      val ticket: CallbackTicket = CallbackTicket(
        Some(ticketUuid),
        UUID.randomUUID(),
        requestUuid,
        "theQueue",
        "1000",
        new LocalDate(),
        new DateTime,
        None
      )
      val msg: UpdateCallbackTicket = UpdateCallbackTicket(
        ticketUuid.toString,
        Some(CallbackStatus.Answered),
        Some("Small comment")
      )
      when(recordingws.getCallbackTicket(ticketUuid.toString))
        .thenReturn(Future.successful(ticket))
      when(requester.clotureRequest(requestUuid))
        .thenReturn(Future.successful(()))
      var (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      verify(requester, Mockito.timeout(5000)).clotureRequest(requestUuid)
      verify(bus, Mockito.timeout(5000)).publish(CallbackClotured(requestUuid))
    }

    "upon reception of UpdateCallbackTicket, uncloture the associated request if the status is Callback" in new Helper {
      val ticketUuid: UUID  = UUID.randomUUID()
      val requestUuid: UUID = UUID.randomUUID()
      val ticket: CallbackTicket = CallbackTicket(
        Some(ticketUuid),
        UUID.randomUUID(),
        requestUuid,
        "theQueue",
        "1000",
        new LocalDate(),
        new DateTime,
        None
      )
      val msg: UpdateCallbackTicket = UpdateCallbackTicket(
        ticketUuid.toString,
        Some(CallbackStatus.Callback),
        Some("Small comment")
      )
      when(recordingws.getCallbackTicket(ticketUuid.toString))
        .thenReturn(Future.successful(ticket))
      var (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      verify(requester, Mockito.timeout(5000)).unclotureRequest(requestUuid)
      verify(requester, never).rescheduleCallback(
        requestUuid,
        RescheduleCallback(DateTime.parse("2016-08-09"), "")
      )
    }

    "upon reception of UpdateCallbackTicket, uncloture the associated request if the status is Callback and reschedule if asked" in new Helper {
      val ticketUuid: UUID       = UUID.randomUUID()
      val requestUuid: UUID      = UUID.randomUUID()
      val periodUuid: UUID       = UUID.randomUUID()
      val listUuid: UUID         = UUID.randomUUID()
      val rescheduleTo: DateTime = DateTime.parse("2016-08-09")
      val request: CallbackRequest = CallbackRequest(
        Some(requestUuid),
        listUuid,
        Some("1000"),
        None,
        None,
        None,
        Some("The company"),
        None
      )

      val ticket: CallbackTicket = CallbackTicket(
        Some(ticketUuid),
        UUID.randomUUID(),
        requestUuid,
        "theQueue",
        "1000",
        new LocalDate(),
        new DateTime,
        None
      )
      val msg: UpdateCallbackTicket = UpdateCallbackTicket(
        ticketUuid.toString,
        Some(CallbackStatus.Callback),
        Some("Small comment"),
        Some(rescheduleTo),
        Some(periodUuid.toString)
      )
      when(recordingws.getCallbackTicket(ticketUuid.toString))
        .thenReturn(Future.successful(ticket))
      when(
        requester.rescheduleCallback(
          requestUuid,
          RescheduleCallback(rescheduleTo, periodUuid.toString)
        )
      ).thenReturn(Future.successful(()))
      when(requester.getCallbackRequest(requestUuid.toString))
        .thenReturn(Future.successful(request))

      var (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      verify(requester, Mockito.timeout(5000)).unclotureRequest(requestUuid)
      verify(requester, Mockito.timeout(5000)).rescheduleCallback(
        requestUuid,
        RescheduleCallback(rescheduleTo, periodUuid.toString)
      )
      verify(bus, Mockito.timeout(5000))
        .publish(CallbackRequestUpdated(request))
    }

    "upon reception of UpdateCallbackTicket, not uncloture nor cloture the associated request if the status is undefined" in new Helper {
      val ticketUuid: UUID  = UUID.randomUUID()
      val requestUuid: UUID = UUID.randomUUID()
      val ticket: CallbackTicket = CallbackTicket(
        Some(ticketUuid),
        UUID.randomUUID(),
        requestUuid,
        "theQueue",
        "1000",
        new LocalDate(),
        new DateTime,
        None
      )
      val msg: UpdateCallbackTicket =
        UpdateCallbackTicket(ticketUuid.toString, None, Some("Small comment"))
      when(recordingws.getCallbackTicket(ticketUuid.toString))
        .thenReturn(Future.successful(ticket))
      var (ref, a) = actor()

      ref ! BaseRequest(self, msg)

      verifyNoMoreInteractions(requester)
    }

    "ask for the CSV of callback tickets" in new Helper {
      val uuid: String              = UUID.randomUUID().toString
      val csv                       = "the csv content"
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      when(recordingws.exportTicketsCSV(uuid))
        .thenReturn(Future.successful(csv))
      var (ref, a) = actor()

      val res: Future[RequestResult] =
        (ref ? ExportTicketsCsv(uuid)).mapTo[RequestResult]

      res.futureValue shouldEqual RequestSuccess(csv)
    }

    "ask for the CSV of callback tickets and return RequestError in case of failure" in new Helper {
      val uuid: String              = UUID.randomUUID().toString
      implicit val timeout: Timeout = Timeout(100.milliseconds)
      when(recordingws.exportTicketsCSV(uuid))
        .thenReturn(Future.failed(new WebServiceException("Error message")))
      var (ref, a) = actor()

      val res: Future[RequestResult] =
        (ref ? ExportTicketsCsv(uuid)).mapTo[RequestResult]

      res.futureValue shouldEqual RequestError("Error message")
    }

    "subscribe to PhoneEvents when starting" in new Helper {
      var (ref, a) = actor()
      verify(bus, Mockito.timeout(5000))
        .subscribe(ref, XucEventBus.allPhoneEventsTopic)
    }

    "update the proper callback ticket with a callid when receiving an EventDialing with a variable USR_CALLBACK_TICKET_UUID" in new Helper {
      val callid           = "123456.777"
      val ticketUuid: UUID = UUID.randomUUID()
      val event: PhoneEvent = PhoneEvent(
        PhoneEventType.EventDialing,
        "Test",
        "Test",
        "Samuel",
        linkedId = callid,
        uniqueId = "123456.888",
        userData = Map(
          "XIVO_SRCNUM"              -> "1000",
          "USR_CALLBACK_TICKET_UUID" -> ticketUuid.toString
        )
      )

      var (ref, a) = actor()
      ref ! event

      verify(recordingws, Mockito.timeout(5000)).updateCallbackTicket(
        ticketUuid.toString,
        CallbackTicketPatch(None, None, Some(callid))
      )
    }
  }
}
