package services.calltracking

import scala.util.Random
import xivo.events.{CallDirection, PhoneEvent, PhoneEventType, UserData}
import xivo.xucami.models.{CallerId, Channel, ChannelDirection, ChannelState}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class DeviceCallToPhoneEventSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar {
  import xivo.models.LineHelper.makeLine

  def mkChannel(
      state: ChannelState.ChannelState,
      interface: String = "SIP/abcd",
      number: String = "1000",
      originating: Boolean = false,
      queue: Option[String] = None
  ): Channel = {
    val channelName = Random.nextInt(5).formatted("SIP/abcd-%05d")
    val callId      = Random.nextInt(9).toString
    val queueVar =
      queue.map(q => Map(UserData.QueueNameKey -> q)).getOrElse(Map.empty)
    val variables: Map[String, String] =
      if (originating)
        queueVar ++ Map(
          Channel.VarNames.xucCallType -> Channel.callTypeValOriginate
        )
      else queueVar

    Channel(
      callId,
      channelName,
      CallerId("Someone", number),
      "",
      state,
      variables = variables,
      direction = Some(ChannelDirection.OUTGOING)
    )
  }

  def mkCall(channel: Channel, party: Option[Channel] = None): DeviceCall =
    DeviceCall(
      channel.name,
      Some(channel),
      Set.empty,
      party.map(p => Map(p.name -> p)).getOrElse(Map.empty)
    )

  "channelStateToPhoneEventType" should {

    "create PhoneEvent from DeviceCall and others" in {
      val channel = mkChannel(ChannelState.UP, originating = true)
      val remote =
        mkChannel(ChannelState.DIALING, queue = Some("queueTestName"))
      val call = mkCall(channel, Some(remote))
      val line = makeLine(
        1,
        "default",
        "sip",
        "name",
        None,
        number = Some("11"),
        xivoIp = "xivoIp"
      )
      val phoneEvent = DeviceCallToPhoneEvent(call, line)

      phoneEvent.get shouldEqual PhoneEvent(
        PhoneEventType.EventDialing,
        "11",
        "1000",
        "Someone",
        channel.linkedChannelId,
        channel.id,
        None,
        Map(Channel.VarNames.xucCallType -> Channel.callTypeValOriginate),
        CallDirection.Outgoing
      )

    }

    "return EventDialing when originating channel is ringing" in {
      val channel = mkChannel(ChannelState.RINGING, originating = true)
      val call    = mkCall(channel)
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventDialing))
    }

    "return EventRinging when non-originating channel Ringing" in {
      val channel = mkChannel(ChannelState.RINGING)
      val call    = mkCall(channel)
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventRinging))
    }

    "return EventEstablished when originating channel is UP and a remote party is up" in {
      val channel = mkChannel(ChannelState.UP, originating = true)
      val party   = mkChannel(ChannelState.UP)
      val call    = mkCall(channel, Some(party))
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventEstablished))

    }

    "return EventEstablished when originating channel is UP and a remote party is HOLD" in {
      val channel = mkChannel(ChannelState.UP, originating = true)
      val party   = mkChannel(ChannelState.HOLD)
      val call    = mkCall(channel, Some(party))
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventEstablished))

    }

    "return EventEstablished when originating channel is UP, no remote party but got XIVO_PICKEDUP set" in {
      /*
       This is the case when calling a voicemail (*98).
       In this case, there is only one channel up immediately (because it's an originate) and there is no
       remote party. So we rely on the XIVO_PICKEDUP variable set by the diaplan.
       */
      val channel = mkChannel(ChannelState.UP, originating = true)
        .updateVariable(
          Channel.VarNames.XiVOAnsweredVariable,
          Channel.callAnsweredValue
        )
      val call   = mkCall(channel)
      val result = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventEstablished))

    }

    "return EventEstablished when originating channel is UP, no remote party but got DIALSTATUS set to ANSWER" in {
      /*
       This is the case when transfering using local channles to an application (voicemail or conference room).
       In this case, there is only one channel up immediately (because it's an originate) and there is no
       remote party. So we rely on the DIALSTATUS variable set by the Dial application and not only on the
       XIVO_PICKEDUP variable as it is only set on the second local channel.
       */
      val channel = mkChannel(ChannelState.UP, originating = true)
        .updateVariable(
          Channel.VarNames.AsteriskDialStatusVariable,
          Channel.callAnsweredDialStatus
        )
      val call   = mkCall(channel)
      val result = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventEstablished))

    }

    "return EventDialing when originating channel UP" in {
      val channel = mkChannel(ChannelState.UP, originating = true)
      val call    = mkCall(channel)
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventDialing))
    }

    "return EventEstablished when non-originating channel UP" in {
      val channel = mkChannel(ChannelState.UP)
      val call    = mkCall(channel)
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventEstablished))
    }

    "return EventHold when channel HOLD" in {
      val channel = mkChannel(ChannelState.HOLD)
      val call    = mkCall(channel)
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventOnHold))
    }

    "return EventReleased when channel HUNGUP" in {
      val channel = mkChannel(ChannelState.HUNGUP)
      val call    = mkCall(channel)
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventReleased))
    }

    "return EventDialing when channel ORIGINATING" in {
      val channel = mkChannel(ChannelState.ORIGINATING)
      val call    = mkCall(channel)
      val result  = DeviceCallToPhoneEvent.channelStateToPhoneEventType(call)

      result should be(Some(PhoneEventType.EventDialing))
    }
  }

}
