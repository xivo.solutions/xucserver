package services.calltracking

import org.joda.time.DateTime
import xivo.xucami.models.CallerId
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import play.api.Logger

class ConferenceRoomRepositorySpec extends AnyWordSpec with Matchers {

  "ConferenceRoomRepository" should {
    "get a conference room" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val repo = ConferenceRoomRepository(Map("4000" -> emptyRoom), Map.empty, Logger(getClass.getName))
      repo.getConference("4000") shouldBe Some(emptyRoom)
    }

    "add a conference room" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val result = ConferenceRoomRepository.empty
        .updateConference(emptyRoom)

      result.getConference("4000") shouldBe Some(emptyRoom)
    }

    "remove a conference room" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val result = ConferenceRoomRepository.empty
        .updateConference(emptyRoom)
        .removeConference("4000")

      result.getConference("4000") shouldBe None
    }

    "do not remove a conference room if existing users" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val repo = ConferenceRoomRepository.empty
        .updateConference(emptyRoom)
        .addParticipant(participant, "default", "xivohost")

      val result = repo.removeConference("4000")

      result.getConference("4000") shouldBe defined
      result.getConference("4000") shouldBe repo.getConference("4000")
    }

    "get all conference room numbers" in {
      val c1 = ConferenceRoom(
        "4000",
        "MySuperConference 1",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val c2 = ConferenceRoom(
        "4001",
        "MySuperConference 2",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val c3 = ConferenceRoom(
        "4012",
        "MySuperConference 3",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val result = ConferenceRoomRepository.empty
        .updateConference(c1)
        .updateConference(c2)
        .updateConference(c3)
        .numbers

      result should contain.only("4000", "4001", "4012")
    }

    "get a participant" in {
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val room = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceBusy,
        Some(DateTime.now),
        List(participant),
        "default",
        "xivohost"
      )
      val repo = ConferenceRoomRepository(Map("4000" -> room), Map.empty, Logger(getClass.getName))

      repo.getParticipant("4000", 1) shouldBe Some(participant)
    }

    "add participant in a room" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val result = ConferenceRoomRepository.empty
        .updateConference(emptyRoom)
        .addParticipant(participant, "default", "xivohost")

      result.getParticipant("4000", 1) shouldBe Some(participant)
    }

    "remove a participant from a room" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val result = ConferenceRoomRepository.empty
        .updateConference(emptyRoom)
        .addParticipant(participant, "default", "xivohost")
        .removeParticipant(participant, "default", "xivohost")

      result.getParticipant("4000", 1) shouldBe None
    }

    "update a participant in a room" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val result = ConferenceRoomRepository.empty
        .updateConference(emptyRoom)
        .addParticipant(participant, "default", "xivohost")
        .updateParticipant("4000", 1)(_.copy(isTalking = true))

      val expectedParticipant = participant.copy(isTalking = true)
      result.getParticipant("4000", 1) shouldBe Some(expectedParticipant)
    }

    "get a participant by its channel name" in {
      val emptyRoom = ConferenceRoom(
        "4000",
        "MySuperConference",
        ConferenceAvailable,
        None,
        List.empty,
        "default",
        "xivohost"
      )
      val participant = ConferenceParticipant(
        "4000",
        1,
        "SIP/abcd-00001",
        CallerId("Some One", "1001"),
        DateTime.now
      )
      val result = ConferenceRoomRepository.empty
        .updateConference(emptyRoom)
        .addParticipant(participant, "default", "xivohost")

      result.getParticipantByChannel("SIP/abcd-00001") shouldBe Some(
        participant
      )
    }
  }
}
