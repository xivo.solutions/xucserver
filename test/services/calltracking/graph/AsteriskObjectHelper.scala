package services.calltracking.graph

import services.calltracking.graph.NodeBridge.BridgeCreator
import services.calltracking.{graph => g}

trait AsteriskObjectHelper {
  def NodeChannel(name: String): g.NodeChannel = g.NodeChannel(name, "default")
  def NodeChannel(name: String, m: String): g.NodeChannel =
    g.NodeChannel(name, m)
  def NodeLocalChannel(name: String): g.NodeLocalChannel =
    g.NodeLocalChannel(name, "default")
  def NodeLocalChannel(name: String, m: String): g.NodeLocalChannel =
    g.NodeLocalChannel(name, m)
  def NodeBridge(name: String, bc: Option[BridgeCreator]): g.NodeBridge =
    g.NodeBridge(name, "default", bc)
  def NodeBridge(
      name: String,
      m: String,
      bc: Option[BridgeCreator]
  ): g.NodeBridge = g.NodeBridge(name, m, bc)
}
