package services.calltracking.graph

import GraphBuilder._
import services.calltracking.graph.NodeBridge.BridgeCreator
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class AsteriskGraphSpec
    extends AnyWordSpec
    with Matchers
    with AsteriskObjectHelper {

  "AsteriskGraph" should {

    "store links between channels and bridge" in {
      // Checks compilation only
      AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b",
          Some(new BridgeCreator("bc"))
        ) ~ NodeChannel("c2")
      )
    }

    "retrieve connected channels of a given channel" in {
      val graph = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b",
          Some(new BridgeCreator("bc"))
        ) ~ NodeChannel("c2")
      )

      graph.remoteChannels(NodeChannel("c1")) should contain only NodeChannel(
        "c2"
      )
      graph.remoteChannels(NodeChannel("c2")) should contain only NodeChannel(
        "c1"
      )
    }

    "retrieve connected channels of a given channel without mixing paths" in {
      val graph = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b1",
          Some(new BridgeCreator("bc1"))
        ) ~ NodeChannel("c2"),
        NodeChannel("c3") ~ NodeBridge(
          "b2",
          Some(new BridgeCreator("bc2"))
        ) ~ NodeChannel("c4")
      )

      graph.remoteChannels(NodeChannel("c1")) should contain only NodeChannel(
        "c2"
      )
      graph.remoteChannels(NodeChannel("c2")) should contain only NodeChannel(
        "c1"
      )

      graph.remoteChannels(NodeChannel("c3")) should contain only NodeChannel(
        "c4"
      )
      graph.remoteChannels(NodeChannel("c4")) should contain only NodeChannel(
        "c3"
      )
    }

    // "retrieve endpoint channels of a given channel" in {
    //   val graph = AsteriskGraph(
    //     NodeChannel("c1") ~ NodeBridge("b") ~ NodeChannel("c2")
    //   )

    //   graph.remoteChannelLikes(NodeChannel("c1")) should contain only (NodeChannel("c2"))
    //   graph.remoteChannelLikes(NodeChannel("c2")) should contain only (NodeChannel("c1"))
    // }

    // "retrieve endpoint local channel of a given channel" in {
    //   val graph = AsteriskGraph(
    //     NodeChannel("c") ~ NodeBridge("b") ~ NodeLocalChannel("lc")
    //   )

    //   graph.remoteChannelLikes(NodeChannel("c")) should contain only (NodeLocalChannel("lc"))
    // }

    "remove a channel from a bridge" in {
      val graph = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b",
          Some(new BridgeCreator("bc"))
        ) ~ NodeChannel("c2")
      )

      AsteriskGraph.del(
        graph,
        NodeChannel("c1") ~ NodeBridge("b", Some(new BridgeCreator("bc")))
      )
    }

    "track channel optimization" in {
      val c1  = NodeChannel("c1")
      val c2  = NodeChannel("c2")
      val lc1 = NodeLocalChannel("lc1")
      val lc2 = NodeLocalChannel("lc2")
      val b1  = NodeBridge("b1", Some(new BridgeCreator("bc1")))
      val b2  = NodeBridge("b2", Some(new BridgeCreator("bc2")))
      val lb  = NodeLocalBridge(lc1, lc2)

      val graph = AsteriskGraph(
        c1 ~ b1 ~ lc1 ~ lb ~ lc2 ~ b2 ~ c2
      )

      graph.endpointsOf(c1) should contain only c2
      val optimized = AsteriskGraph
        .del(graph, b1 ~ lc1, lc1 ~ lb, lb ~ lc2, b2 ~ lc2, c2 ~ b2)
        .add(c2 ~ b1)

      optimized.endpointsOf(c1) should contain only c2
    }

    "draw representation of simple graph" in {
      val graph = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b",
          Some(new BridgeCreator("bc"))
        ) ~ NodeChannel("c2")
      )

      val str = graph.prettyPrint()

      str should be("""NodeChannel(c1) ─ NodeBridge(b, bc) ─ NodeChannel(c2)""")
    }

    "draw representation of two simple paths" in {
      val graph = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b1",
          Some(new BridgeCreator("bc1"))
        ) ~ NodeChannel("c2"),
        NodeChannel("c3") ~ NodeBridge(
          "b2",
          Some(new BridgeCreator("bc2"))
        ) ~ NodeChannel("c4")
      )

      val str = graph.prettyPrint()

      str should be(
        """NodeChannel(c1) ─ NodeBridge(b1, bc1) ─ NodeChannel(c2)
                     |NodeChannel(c3) ─ NodeBridge(b2, bc2) ─ NodeChannel(c4)""".stripMargin
      )
    }

    "draw representation of complex graph" in {
      val graph = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b",
          Some(new BridgeCreator("bc"))
        ) ~ NodeChannel("c2"),
        NodeBridge("b", Some(new BridgeCreator("bc"))) ~ NodeChannel("c3"),
        NodeBridge("b", Some(new BridgeCreator("bc"))) ~ NodeChannel("c6"),
        NodeChannel("c4") ~ NodeBridge(
          "b2",
          Some(new BridgeCreator("bc2"))
        ) ~ NodeChannel("c5")
      )

      val str = graph.prettyPrint()

      str should be(
        """NodeChannel(c1) ─ NodeBridge(b, bc) ┬ NodeChannel(c2)
                      |                                    ├ NodeChannel(c3)
                      |                                    └ NodeChannel(c6)
                      |NodeChannel(c4) ─ NodeBridge(b2, bc2) ─ NodeChannel(c5)""".stripMargin
      )
    }

    "draw representation of graph with different path length" in {
      val graph = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b",
          Some(new BridgeCreator("bc"))
        ) ~ NodeChannel("c2") ~ NodeBridge(
          "b3",
          Some(new BridgeCreator("bc3"))
        ) ~ NodeChannel("c3"),
        NodeBridge("b", Some(new BridgeCreator("bc"))) ~ NodeChannel("c6"),
        NodeChannel("c4") ~ NodeBridge(
          "b2",
          Some(new BridgeCreator("bc2"))
        ) ~ NodeChannel("c5")
      )

      val str = graph.prettyPrint()

      str should be(
        """NodeChannel(c1) ─ NodeBridge(b, bc) ┬ NodeChannel(c2) ─ NodeBridge(b3, bc3) ─ NodeChannel(c3)
                      |                                    └ NodeChannel(c6)
                      |NodeChannel(c4) ─ NodeBridge(b2, bc2) ─ NodeChannel(c5)""".stripMargin
      )
    }

    "draw representation of graph with multiple split" in {
      val graph = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b",
          Some(new BridgeCreator("bc"))
        ) ~ NodeChannel("c2") ~ NodeBridge(
          "b3",
          Some(new BridgeCreator("bc3"))
        ) ~ NodeChannel("c3"),
        NodeBridge("b3", Some(new BridgeCreator("bc3"))) ~ NodeChannel("c7"),
        NodeBridge("b", Some(new BridgeCreator("bc"))) ~ NodeChannel("c6"),
        NodeChannel("c4") ~ NodeBridge(
          "b2",
          Some(new BridgeCreator("bc2"))
        ) ~ NodeChannel("c5")
      )

      val str = graph.prettyPrint()

      str should be(
        """NodeChannel(c1) ─ NodeBridge(b, bc) ┬ NodeChannel(c2) ─ NodeBridge(b3, bc3) ┬ NodeChannel(c3)
                      |                                    │                                       └ NodeChannel(c7)
                      |                                    └ NodeChannel(c6)
                      |NodeChannel(c4) ─ NodeBridge(b2, bc2) ─ NodeChannel(c5)""".stripMargin
      )
    }

  }

}
