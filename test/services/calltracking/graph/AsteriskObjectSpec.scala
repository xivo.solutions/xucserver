package services.calltracking.graph

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class AsteriskObjectSpec extends AnyWordSpec with Matchers {

  "NodeChannel" should {

    "build a NodeChannel from a non-local channel" in {
      NodeChannel.from("SIP/abcd-00001", "main") should be(
        Some(NodeChannel("SIP/abcd-00001", "main"))
      )
    }

    "build a NodeLocalChannel from a Local channel" in {
      NodeChannel.from("Local/1000@default", "main") should be(
        Some(NodeLocalChannel("Local/1000@default", "main"))
      )
    }

    "not build a NodeChannel from a null string" in {
      NodeChannel.from(null, "main") should be(None)
    }

  }

  "NodeChannelLikeOrdering" should {

    "sort local channel after standard channel" in {
      val channels =
        List(NodeLocalChannel("a", "main"), NodeChannel("b", "main"))
      val sorted = channels.sorted(NodeChannelLikeOrdering)

      sorted should be(channels.reverse)
    }

    "sort channels based on their name if same type" in {
      val channels = List(NodeChannel("b", "main"), NodeChannel("a", "main"))
      val sorted   = channels.sorted(NodeChannelLikeOrdering)

      sorted should be(channels.sortWith(_.name < _.name))
    }

    "sort channels based on their name if same type (local)" in {
      val channels =
        List(NodeLocalChannel("b", "main"), NodeLocalChannel("a", "main"))
      val sorted = channels.sorted(NodeChannelLikeOrdering)

      sorted should be(channels.sortWith(_.name < _.name))
    }
  }

}
