package services.calltracking.graph

import GraphBuilder._
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class GraphBuilderSpec extends AnyWordSpec with Matchers {

  "GraphBuilder" should {

    "build a list of edge from a node graph" in {
      val edges = ("a" ~ "b" ~ "c").toEdges()

      edges should contain.only(Edge("a", "b"), Edge("b", "c"))
    }

    "remove one edge from a multi-edge path using builder" in {
      val graph = BaseGraphTest("a" ~ "b" ~ "c")

      BaseGraphTest.del(graph, "a" ~ "b").neighbors("a") shouldBe empty
      BaseGraphTest
        .del(graph, "a" ~ "b")
        .neighbors("b") should contain only "c"
      BaseGraphTest
        .del(graph, "a" ~ "b")
        .neighbors("c") should contain only "b"
    }

  }

}
