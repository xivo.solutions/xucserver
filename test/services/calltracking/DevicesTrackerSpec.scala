package services.calltracking

import helpers.MediatorHelperSpec
import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import org.mockito.Mockito.*
import org.scalatest.*
import org.scalatestplus.mockito.MockitoSugar

import scala.concurrent.{Future, Promise}
import scala.concurrent.duration.*
import services.XucAmiBus.{AmiConnected, AmiType}
import xivo.xuc.{DeviceTrackerConfig, XucConfig}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import pekkotest.TestKitSpec
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.Subscribe
import services.calltracking.ConferenceTracker.DeviceJoinConference
import services.{XucAmiBus, XucEventBus}
import system.ClusterAsync
import xivo.models.*

import scala.concurrent.duration.*
import scala.concurrent.{Future, Promise}
import scala.reflect.ClassTag

class DevicesTrackerSpec
    extends TestKitSpec("DevicesTracker")
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll {
  import xivo.models.LineHelper.makeLine

  private val topic: String =
    ConferenceTracker.conferenceParticipantEventTopic("my-conf", "10.181.0.2")

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  class DummyActor extends Actor {
    override def receive: Receive = Actor.emptyBehavior
  }

  class DummyDeviceFactory extends DeviceActorFactory {
    def props(xivoFeature: XivoFeature): Props = {
      Props(new DummyActor())
    }
  }

  case class DummyMessage(channelName: String) extends DeviceMessage {
    def isFor(interface: String): Boolean = channelName.startsWith(interface)
  }

  abstract class TrackerHelper() {
    val lineFactory: LineFactory          = mock[LineFactory]
    val deviceFactory: DummyDeviceFactory = spy(new DummyDeviceFactory)
    val xucAmiBus: XucAmiBus              = mock[XucAmiBus]
    val config: XucConfig                 = mock[XucConfig]
    when(config.sipDriver).thenReturn(SipDriver.SIP)
    val mediator: TestProbe = TestProbe()
    val mediatorHelper = new MediatorHelperSpec(mediator)

    def actor(): ActorRef = {
      system.actorOf(
        Props(
          new DevicesTracker(
            lineFactory,
            deviceFactory,
            xucAmiBus,
            config,
            mediator.ref
          )
        )
      )
    }
  }

  "DevicesTracker" should {

    "subscribe to AmiService (AmiConnected) events" in new TrackerHelper {
      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))

      val ref: ActorRef = actor()
      verify(xucAmiBus, timeout(500)).subscribe(ref, AmiType.AmiService)
    }

    "spawn two trackers per SIP media server" in new TrackerHelper {
      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))
      when(config.sipDriver).thenReturn(SipDriver.SIP)

      val ref: ActorRef = actor()

      ref ! AmiConnected("mds1")
      verify(deviceFactory, timeout(100)).props(
        MediaServerTrunk("from-mds1", SipDriver.SIP)
      )
      verify(deviceFactory, timeout(100)).props(
        MediaServerTrunk("to-mds1", SipDriver.SIP)
      )
    }

    "spawn two trackers per PJSIP media server" in new TrackerHelper {
      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))
      when(config.sipDriver).thenReturn(SipDriver.PJSIP)

      val ref: ActorRef = actor()

      ref ! AmiConnected("mds1")
      verify(deviceFactory, timeout(100)).props(
        MediaServerTrunk("from-mds1", SipDriver.PJSIP)
      )
      verify(deviceFactory, timeout(100)).props(
        MediaServerTrunk("to-mds1", SipDriver.PJSIP)
      )
    }

    "spawn one SingleDeviceTracker per line found" in new TrackerHelper {
      val line1: Line =
        makeLine(1, "default", "sip", "abcd", None, None, "123.123.123.1")
      val line2: Line =
        makeLine(2, "default", "sip", "efgh", None, None, "123.123.123.1")
      val line3: Line =
        makeLine(3, "default", "sccp", "ijkl", None, None, "123.123.123.1")

      val trunk1: Trunk =
        Trunk(1, "default1", "sip", "trunk-test", SipDriver.SIP)
      val trunk2: Trunk =
        Trunk(2, "default2", "sip", "trunk-test", SipDriver.PJSIP)

      val lines: List[Line]   = List(line1, line2, line3)
      val trunks: List[Trunk] = List(trunk1, trunk2)

      when(lineFactory.all()).thenReturn(Future.successful(lines))
      when(lineFactory.trunks).thenReturn(Future.successful(trunks))

      actor()
      verify(deviceFactory, timeout(100)).props(line1)
      verify(deviceFactory, timeout(100)).props(line2)
      verify(deviceFactory, timeout(100)).props(line3)

      verify(deviceFactory, timeout(100)).props(trunk1)
      verify(deviceFactory, timeout(100)).props(trunk2)

    }

    "spawn one DeviceTracker to track all DAHDI channels, local channels and anonymous channels" in new TrackerHelper {
      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))
      when(config.sipDriver).thenReturn(SipDriver.SIP)

      actor()

      verify(deviceFactory, timeout(100)).props(DAHDITrunk)
      verify(deviceFactory, timeout(100)).props(LocalChannelFeature)
      verify(deviceFactory, timeout(100)).props(
        AnonymousChannelFeature(SipDriver.SIP)
      )
    }

    "spawn one DeviceTracker when asked to track a new Line" in new TrackerHelper {
      val line: Line =
        makeLine(1, "default", "sip", "abcd", None, None, "123.123.123.1")

      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))

      val ref: ActorRef = actor()
      ref ! DevicesTracker.EnsureTrackerFor(line)
      verify(deviceFactory, timeout(100)).props(line)

    }

    "prevent creation of actor for a given line if already spawned even if SingleDeviceTracker is not initialized" in new TrackerHelper {

      val line: Line =
        makeLine(1, "default", "sip", "abcd", None, None, "123.123.123.1")

      val lines: List[Line] = List(line)
      val trunks            = List.empty[Trunk]

      val linePromise: Promise[List[Line]] = Promise[List[Line]]()

      when(lineFactory.all()).thenReturn(linePromise.future)
      when(lineFactory.trunks).thenReturn(Future.successful(trunks))

      val ref: ActorRef = actor()
      // ensure nothing is called before resolving the promise
      verify(deviceFactory, never()).props(line)

      // We can receive an initialization request from an agent
      // logging on the given line in the mean time
      ref ! DevicesTracker.EnsureTrackerFor(line)
      verify(deviceFactory, timeout(100)).props(line)

      reset(deviceFactory)

      // Ensure we do not create the line a second time
      linePromise.success(lines)
      verify(deviceFactory, never()).props(line)

    }

    "Forward message to correct actor" in new TrackerHelper {
      val iface1 = "SIP/probe1"
      val iface2 = "SIP/probe2"

      val probe1: TestProbe = TestProbe()
      val probe2: TestProbe = TestProbe()

      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))

      val ref: ActorRef = actor()

      ref ! DevicesTracker.RegisterActor(iface1, probe1.ref)
      ref ! DevicesTracker.RegisterActor(iface2, probe2.ref)

      val msg1: DummyMessage = DummyMessage(iface1)
      ref ! msg1
      probe1.expectMsg(msg1)

      val msg2: DummyMessage = DummyMessage(iface2)
      ref ! msg2
      probe2.expectMsg(msg2)

    }

    "Spool message for actor during creation" in new TrackerHelper {

      val line: Line =
        makeLine(1, "default", "sip", "abcd", None, None, "123.123.123.1")

      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))

      val ref: ActorRef = actor()

      ref ! DevicesTracker.EnsureTrackerFor(line)
      verify(deviceFactory, timeout(100)).props(line)

      // send message before actor is ready
      ref ! DummyMessage(line.trackingInterface)

      val probe: TestProbe = TestProbe()
      ref ! DevicesTracker.RegisterActor(line.trackingInterface, probe.ref)
      probe.expectMsg(DummyMessage(line.trackingInterface))

    }

    "allow multiple actors to register on same interface (message will be broadcast)" in new TrackerHelper {

      val iface1 = "SIP/probe1"

      val probe1: TestProbe = TestProbe()
      val probe2: TestProbe = TestProbe()

      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))

      val ref: ActorRef = actor()

      ref ! DevicesTracker.RegisterActor(iface1, probe1.ref)
      ref ! DevicesTracker.RegisterActor(iface1, probe2.ref)

      val msg1: DummyMessage = DummyMessage(iface1)
      ref ! msg1
      probe1.expectMsg(msg1)
      probe2.expectMsg(msg1)

    }

    "broadcast message to all matching interface/channel" in new TrackerHelper {

      val iface1              = "SIP/probe1"
      val channelName: String = iface1 + "-00001"

      case class DummyMessage(interface: String) extends DeviceMessage {
        def isFor(i: String): Boolean = interface.startsWith(i)
      }

      val probe1: TestProbe = TestProbe()
      val probe2: TestProbe = TestProbe()

      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))

      val ref: ActorRef = actor()

      ref ! DevicesTracker.RegisterActor(iface1, probe1.ref)
      ref ! DevicesTracker.RegisterActor(channelName, probe2.ref)

      val msg1: DummyMessage = DummyMessage(channelName)
      ref ! msg1
      probe2.expectMsg(msg1)
      probe1.expectMsg(msg1)
    }

    "subscribe correctly to the mediator" in new TrackerHelper {

      val iface1              = "SIP/probe1"
      val channelName: String = iface1 + "-00001"

      val conference: ConferenceRoom = ConferenceRoom(
        "1234",
        "abcd",
        ConferenceAvailable,
        None,
        List.empty,
        "mds1",
        "xivohost"
      )

      val probe1: TestProbe = TestProbe()
      val probe2: TestProbe = TestProbe()

      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))

      val ref: ActorRef = actor()

      private val msg: DeviceJoinConference =
        DeviceJoinConference(conference, channelName, topic)
      mediator.ref ! Subscribe(ConferenceTracker.conferenceEventTopic, self)

      ref ! DevicesTracker.RegisterActor(iface1, probe1.ref)
      val registerA = mediatorHelper.expectOnMediator[Subscribe].get
      registerA.topic.shouldEqual("ConferenceEventTopic")

      ref ! DevicesTracker.RegisterActor(channelName, probe2.ref)
      val registerB = mediatorHelper.expectOnMediator[Subscribe].get
      registerB.topic.shouldEqual("ConferenceEventTopic")
    }

    "unregister actor when asked to" in new TrackerHelper {
      val iface               = "SIP/probe1"
      val channelName: String = iface + "-00001"

      case class DummyMessage(interface: String) extends DeviceMessage {
        def isFor(i: String): Boolean = interface.startsWith(i)
      }

      val probe: TestProbe = TestProbe()

      when(lineFactory.all()).thenReturn(Future.successful(List.empty))
      when(lineFactory.trunks).thenReturn(Future.successful(List.empty))

      val ref: ActorRef = actor()

      ref ! DevicesTracker.RegisterActor(iface, probe.ref)

      val msg: DummyMessage = DummyMessage(channelName)
      ref ! msg
      probe.expectMsg(msg)

      ref ! DevicesTracker.UnRegisterActor(iface, probe.ref)

      ref ! msg
      probe.expectNoMessage(250.millis)
    }

  }

  "deviceActor" should {

    val channelTracker      = TestProbe()
    val graphTracker        = TestProbe()
    val bus                 = mock[XucEventBus]
    val amiBus              = mock[XucAmiBus]
    val deviceTrackerConfig = mock[DeviceTrackerConfig]
    val configDispatcher    = TestProbe()
    val mediator            = TestProbe()
    val mediatorHelper      = new MediatorHelperSpec(mediator)
    val xivoHost = "someHost"
    val xucConfig: XucConfig = mock[XucConfig]
    when(xucConfig.xivoHost).thenReturn(xivoHost)

    val factory = new DeviceActorFactoryImpl(
      channelTracker.ref,
      graphTracker.ref,
      bus,
      amiBus,
      deviceTrackerConfig,
      configDispatcher.ref,
      mediator.ref,
      xucConfig
    )

    "spawn a SipDeviceTracker for a Line object" in {
      val line1 =
        makeLine(1, "default", "sip", "abcd", None, None, "123.123.123.1")

      val device = factory.props(line1)
      device.actorClass().getSimpleName.shouldBe("SipDeviceTracker")
    }

    "spawn a CustomDeviceTracker for a custom Line object" in {
      val line1 = makeLine(
        1,
        "default",
        "custom",
        "Local/01230041302@default/n",
        None,
        None,
        "123.123.123.1"
      )

      val device = factory.props(line1)
      device.actorClass().getSimpleName.shouldBe("CustomDeviceTracker")
    }

    "spawn a TrunkDeviceTracker for a Trunk object" in {
      val trunk1 = Trunk(1, "default", "sip", "trunk-test", SipDriver.SIP)

      val device = factory.props(trunk1)
      device.actorClass().getSimpleName.shouldBe("TrunkDeviceTracker")
    }

    "spawn a MediaServerTrunkDeviceTracker for a MediaServerTrunk object" in {
      val trunk1 = MediaServerTrunk("from-mds1", SipDriver.SIP)

      val device = factory.props(trunk1)
      device
        .actorClass()
        .getSimpleName
        .shouldBe("MediaServerTrunkDeviceTracker")
    }

    "spawn a TrunkDeviceTracker for a DAHDITrunk object" in {
      val device = factory.props(DAHDITrunk)
      device.actorClass().getSimpleName.shouldBe("TrunkDeviceTracker")
    }

    "spawn an UnknownDeviceTracker for other objects" in {
      case object OtherXivoFeature extends XivoFeature {
        def interface = "Unknown"
      }

      val device = factory.props(OtherXivoFeature)
      device.actorClass().getSimpleName.shouldBe("UnknownDeviceTracker")
    }
  }
}
