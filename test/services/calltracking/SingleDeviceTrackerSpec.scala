package services.calltracking

import helpers.MediatorHelperSpec
import org.apache.pekko.actor.*
import org.apache.pekko.testkit.*
import org.asteriskjava.manager.action.{
  MixMonitorAction,
  PauseMixMonitorAction,
  SetVarAction,
  StopMixMonitorAction
}
import org.mockito.Mockito.*
import org.mockito.{ArgumentCaptor, Mockito}
import org.scalatest.*
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatestplus.mockito.MockitoSugar
import services.XucAmiBus.*
import services.calltracking.AsteriskGraphTracker.{
  AsteriskPath,
  LoopDetected,
  PathsFromChannel
}
import services.calltracking.SingleDeviceTracker.*
import services.calltracking.graph.AsteriskObjectHelper
import services.{XucAmiBus, XucEventBus}
import xivo.models.{LocalChannelFeature, MediaServerTrunk, Trunk, XivoFeature}
import xivo.xuc.{DeviceTrackerConfig, XucConfig}
import xivo.xucami.models
import xivo.xucami.models.*
import xivo.xucami.models.Channel.VarNames.XucRecordUnpaused
import xivo.xucami.models.ChannelState.ChannelState
import xivo.xucami.models.MonitorState.MonitorState

import scala.concurrent.duration.*

class SingleDeviceTrackerSpec
    extends TestKit(ActorSystem("SingleDeviceTracker"))
    with AnyWordSpecLike
    with Matchers
    with MockitoSugar
    with ImplicitSender
    with BeforeAndAfterAll
    with AsteriskObjectHelper {

  import xivo.models.LineHelper.makeLine

  override def afterAll(): Unit = {
    TestKit.shutdownActorSystem(system)
  }

  val defaultConfig: DeviceTrackerConfig = new DeviceTrackerConfig {
    def stopRecordingUponExternalXfer: Boolean = true
    def enableRecordingRules: Boolean          = true
  }

  class DeviceActorWrapper(
      val feature: XivoFeature,
      stopRecording: Boolean,
      enableRules: Boolean
  ) {
    val channelTracker: TestProbe = TestProbe()
    val graphTracker: TestProbe   = TestProbe()
    val bus: XucEventBus          = mock[XucEventBus]
    val amiBus: XucAmiBus         = mock[XucAmiBus]
    val parent: TestProbe         = TestProbe()
    val xivoHost: String          = "someHost"
    val xucConfig: XucConfig      = mock[XucConfig]
    when(xucConfig.xivoHost).thenReturn(xivoHost)

    val configDispatcher: TestProbe         = TestProbe()
    val mediator: TestProbe                 = TestProbe()
    val mediatorWrapper: MediatorHelperSpec = new MediatorHelperSpec(mediator)

    val deviceTrackerConfig: DeviceTrackerConfig = new DeviceTrackerConfig {
      def stopRecordingUponExternalXfer: Boolean = stopRecording
      def enableRecordingRules: Boolean          = enableRules
    }

    val factory: DeviceActorFactoryImpl = new DeviceActorFactoryImpl(
      channelTracker = channelTracker.ref,
      graphTracker = graphTracker.ref,
      eventBus = bus,
      xucAmiBus = amiBus,
      deviceTrackerConfig = deviceTrackerConfig,
      configDispatcher = configDispatcher.ref,
      mediator = mediator.ref,
      configuration = xucConfig
    )

    val device: TestActorRef[SingleDeviceTracker] =
      TestActorRef[SingleDeviceTracker](
        props = factory.props(feature),
        supervisor = parent.ref,
        name = "MySDTActor"
      )
  }

  def deviceWrapper(
      feature: XivoFeature,
      stopRecording: Boolean = true,
      enableRules: Boolean = true
  ) = new DeviceActorWrapper(
    feature = feature,
    stopRecording = stopRecording,
    enableRules = enableRules
  )

  trait IncomingSipTrunkRecorded:
    val trunkName: String           = "trunk-provider"
    val trunk: Trunk                = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
    val wrapper: DeviceActorWrapper = deviceWrapper(trunk)

    val incomingChannelName: String = "SIP/" + trunkName + "-00000001"
    def incomingChannel(
        id: String = "1522138782.9",
        name: String = incomingChannelName,
        callerId: CallerId = CallerId("0123456789", "0123456789"),
        linkedChannelId: String = "",
        state: ChannelState = ChannelState.UP,
        monitored: MonitorState = MonitorState.DISABLED
    ): Channel = Channel(
      id = id,
      name = name,
      callerId = callerId,
      linkedChannelId = linkedChannelId,
      state = state,
      monitored = monitored
    )

    val bridgedChannelName: String = "SIP/ml9k87j-000000a1"
    def bridgedChannel(
        id: String = "1522140032.14",
        name: String = bridgedChannelName,
        callerId: CallerId = CallerId("", ""),
        linkedChannelId: String = "",
        state: ChannelState = ChannelState.UP,
        monitored: MonitorState = MonitorState.DISABLED
    ): Channel = Channel(
      id = id,
      name = name,
      callerId = callerId,
      linkedChannelId = linkedChannelId,
      state = state,
      monitored = monitored
    )

    def sendPartyInformation(
        incomingChannel: Channel,
        bridgedChannel: Channel,
        wrapper: DeviceActorWrapper = wrapper,
        sourceTrackerType: DeviceTrackerType = TrunkDeviceTrackerType
    ): Unit = {
      val pfc: PathsFromChannel = PathsFromChannel(
        NodeChannel(incomingChannel.name),
        Set(AsteriskPath(NodeChannel(bridgedChannel.name)))
      )
      wrapper.device ! incomingChannel
      wrapper.device ! pfc
      val party: PartyInformation = SingleDeviceTracker.PartyInformation(
        incomingChannel.name,
        bridgedChannel,
        sourceTrackerType
      )
      wrapper.device ! party
    }

  "TrunkDeviceTracker" should {
    "have the tracker type TrunkDeviceTracker" in {
      val trunk: Trunk                = mock[Trunk]
      val wrapper: DeviceActorWrapper = deviceWrapper(trunk)

      wrapper.device.underlyingActor.deviceTrackerType should be(
        TrunkDeviceTrackerType
      )
    }
  }

  "UnknownDeviceTracker" should {
    "have the tracker type UnknownDeviceTracker" in {
      val unknown: XivoFeature        = mock[XivoFeature]
      val wrapper: DeviceActorWrapper = deviceWrapper(unknown)

      wrapper.device.underlyingActor.deviceTrackerType should be(
        UnknownDeviceTrackerType
      )
    }
  }

  "MediaServerTrunkDeviceTracker" should {
    "have the tracker type MediaServerTrunkDeviceTrackerType" in {
      val mdsTrunk: MediaServerTrunk =
        MediaServerTrunk("from-mds1", driver = SipDriver.SIP)
      val wrapper: DeviceActorWrapper = deviceWrapper(mdsTrunk)

      wrapper.device.underlyingActor.deviceTrackerType should be(
        MediaServerTrunkDeviceTrackerType
      )
    }
  }

  "SingleDeviceTracker" should {
    import BaseTracker.*

    "watch graph for its interface for trunks" in {
      val wrapper: DeviceActorWrapper =
        deviceWrapper(feature =
          Trunk(
            id = 1,
            context = "default",
            protocol = "sip",
            name = "trunk-test",
            driver = SipDriver.SIP
          )
        )
      wrapper.graphTracker.expectMsg(
        WatchChannelStartingWith(name = "SIP/trunk-test")
      )
    }

    "watch channels for its interface for SIP trunks" in {
      val wrapper =
        deviceWrapper(Trunk(1, "default", "sip", "trunk-test", SipDriver.SIP))
      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith("SIP/trunk-test")
      )
    }

    "watch channels for its interface for PJSIP trunks" in {
      val wrapper =
        deviceWrapper(Trunk(1, "default", "sip", "trunk-test", SipDriver.PJSIP))
      wrapper.channelTracker.expectMsg(
        WatchChannelStartingWith("PJSIP/trunk-test")
      )
    }

    "get tracked calls" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! c
      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(DeviceCall(cname, Some(c), Set.empty, Map.empty))
        )
      )
    }

    "should not list hungup calls" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )

      wrapper.device ! c
      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }

    "schedule call removal 2 seconds after hangup" in {
      val mockScheduler       = mock[Scheduler]
      val channelTracker      = TestProbe()
      val graphTracker        = TestProbe()
      val bus                 = mock[XucEventBus]
      val amiBus              = mock[XucAmiBus]
      val deviceTrackerConfig = mock[DeviceTrackerConfig]
      val mediator            = TestProbe()
      val mediatorWrapper     = new MediatorHelperSpec(mediator)

      val device = TestActorRef(
        new UnknownDeviceTracker(
          "SIP/abcd",
          channelTracker.ref,
          graphTracker.ref,
          amiBus,
          deviceTrackerConfig,
          mediator.ref
        ) {
          override def scheduler: Scheduler = mockScheduler
        }
      )

      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.HUNGUP
      )

      device ! c

      verify(mockScheduler, timeout(500)).scheduleOnce(
        2.seconds,
        device,
        device.underlyingActor.RemoveChannel(cname)
      )(scala.concurrent.ExecutionContext.Implicits.global, device)
    }

    "track remote party information" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname             = "SIP/abcd-00000001"
      val remoteCname       = "SIP/efgh-0000001"
      val sourceTrackerType = SipDeviceTrackerType
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val bridgedChannel = Channel(
        "123456789.124",
        remoteCname,
        CallerId("Jason Bourne", "1666"),
        "",
        ChannelState.RINGING
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        bridgedChannel,
        sourceTrackerType
      )

      wrapper.device ! channel
      wrapper.device ! party
      wrapper.device ! pfc

      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(
            DeviceCall(
              cname,
              Some(channel),
              pfc.paths,
              Map(bridgedChannel.name -> bridgedChannel)
            )
          )
        )
      )
    }

    "track remote party information (alternative message order)" in {
      val wrapper = deviceWrapper(
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      )

      val cname             = "SIP/abcd-00000001"
      val remoteCname       = "SIP/efgh-0000001"
      val sourceTrackerType = SipDeviceTrackerType
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val bridgedChannel = Channel(
        "123456789.124",
        remoteCname,
        CallerId("Jason Bourne", "1666"),
        "",
        ChannelState.RINGING
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        bridgedChannel,
        sourceTrackerType
      )

      wrapper.device ! channel
      wrapper.device ! pfc
      wrapper.device ! party

      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(
        SingleDeviceTracker.Calls(
          List(
            DeviceCall(
              cname,
              Some(channel),
              pfc.paths,
              Map(bridgedChannel.name -> bridgedChannel)
            )
          )
        )
      )
    }

    "ensure remote party information are sent when receiving channel and then path" in {
      val line =
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      val wrapper = deviceWrapper(line)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val cname       = "SIP/abcd-00000001"
      val remoteCname = "SIP/efgh-0000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )

      wrapper.device ! channel
      wrapper.device ! pfc

      val expectedTrackerType = SipDeviceTrackerType
      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        channel,
        expectedTrackerType
      )

      wrapper.parent.expectMsg(expected)
    }

    "ensure remote party information are NOT sent to local channels when receiving channel and then path" in {
      val line =
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      val wrapper = deviceWrapper(line)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val cname             = "SIP/abcd-00000001"
      val remoteCname       = "Local/1234@default-00000002"
      val sourceTrackerType = SipDeviceTrackerType
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeLocalChannel(remoteCname)))
      )

      wrapper.device ! channel
      wrapper.device ! pfc

      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        channel,
        sourceTrackerType
      )

      wrapper.parent.expectNoMessage(100.millis)
    }

    "ensure remote party information are sent when receiving path and then channel" in {
      val line =
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      val wrapper = deviceWrapper(line)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val cname       = "SIP/abcd-00000001"
      val remoteCname = "SIP/efgh-0000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! channel

      val expectedSourceTrackerType = SipDeviceTrackerType
      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        channel,
        expectedSourceTrackerType
      )

      wrapper.parent.expectMsg(expected)
    }

    "ensure remote party information contains correct source SIP device tracker type" in {
      val trunkName = "trunk-test"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.SIP)
      val wrapper   = deviceWrapper(trunk)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val trunkCName = "SIP/" + trunkName + "-00000001"
      val trunkChannel = Channel(
        "123456789.123",
        trunkCName,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      val remoteCname = "SIP/efgh-0000001"
      val pfc = PathsFromChannel(
        NodeChannel(trunkCName),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! trunkChannel

      val expectedSourceTrackerType = TrunkDeviceTrackerType
      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        trunkChannel,
        expectedSourceTrackerType
      )

      wrapper.parent.expectMsg(expected)
    }

    "ensure remote party information contains correct source PJSIP device tracker type" in {
      val trunkName = "trunk-test"
      val trunk     = Trunk(1, "default", "sip", trunkName, SipDriver.PJSIP)
      val wrapper   = deviceWrapper(trunk)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val trunkCName = "PJSIP/" + trunkName + "-00000001"
      val trunkChannel = Channel(
        "123456789.123",
        trunkCName,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )

      val remoteCname = "PJSIP/efgh-0000001"
      val pfc = PathsFromChannel(
        NodeChannel(trunkCName),
        Set(AsteriskPath(NodeChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! trunkChannel

      val expectedSourceTrackerType = TrunkDeviceTrackerType
      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        trunkChannel,
        expectedSourceTrackerType
      )

      wrapper.parent.expectMsg(expected)
    }

    "ensure remote party information are NOT sent to local channel when receiving path and then channel" in {
      val line =
        makeLine(1, "default", "sip", "efgh", None, None, "123.123.123.1")
      val wrapper = deviceWrapper(line)

      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      val cname       = "SIP/abcd-00000001"
      val remoteCname = "Local/1234@default-00000002"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeLocalChannel(remoteCname)))
      )

      wrapper.device ! pfc
      wrapper.device ! channel

      val expected = SingleDeviceTracker.PartyInformation(
        remoteCname,
        channel,
        SipDeviceTrackerType
      )

      wrapper.parent.expectNoMessage(100.millis)
    }

    "stop recording if a recorded channel is bridged to another TrunkDeviceTracker" in new IncomingSipTrunkRecorded {
      /* Here we simulate the case where
       - a recorded channel from a trunk (followed by a TrunkDeviceTracker)
       - is bridged with another channel from a trunk (followed by a TrunkDeviceTracker)
       => Recording should be stopped
       */
      val incomingChannel: Channel =
        incomingChannel(monitored = MonitorState.ACTIVE)
      val bridgedChannel: Channel = bridgedChannel(
        name = "SIP/" + trunkName + "-000000a1"
      )

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus).publish(arg.capture)
      arg.getValue.message match {
        case stop: StopMixMonitorAction =>
          stop.getChannel shouldBe incomingChannelName
        case any =>
          fail(
            s"Should get StopMixMonitorAction on channel $incomingChannelName, got: $any"
          )
      }
    }

    "not stop recording of remote recorded channel" in new IncomingSipTrunkRecorded {
      /* Here we simulate the case where
       - a channel from a trunk (followed by a TrunkDeviceTracker)
       - is bridged with another channel from a trunk which is recorded (followed by a TrunkDeviceTracker)
       => Recording should not be stopped here (we should not stop the remote channel)
          the recording will be stopped when the recorded channel receives the partyinformation
       */

      val incomingChannel: Channel = incomingChannel()
      val bridgedChannel: Channel =
        bridgedChannel(
          name = "SIP/" + trunkName + "-000000a1",
          monitored = MonitorState.ACTIVE
        )

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "not stop recording if both party are not a TrunkDeviceTracker" in new IncomingSipTrunkRecorded {
      /* Here, we simulate the case where
       - a recorded channel from a trunk (followed by a TrunkDeviceTracker)
       - is bridged with an internal channel (followed by a SipDeviceTracker)
       Therefore recording should not be stopped
       */

      val incomingChannel: Channel =
        incomingChannel(monitored = MonitorState.ACTIVE)
      val bridgedChannel: Channel = bridgedChannel(
        callerId = CallerId("Agent Secret", "8002")
      )

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel,
        sourceTrackerType = SipDeviceTrackerType
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "not stop recording if channel is not recorded" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel()
      val bridgedChannel: Channel = bridgedChannel(
        name = "SIP/" + trunkName + "-000000a1"
      )

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "not stop recording if option is deactivated" in new IncomingSipTrunkRecorded {
      val unrecordedTrunkDeviceWrapper: DeviceActorWrapper =
        deviceWrapper(trunk, stopRecording = false)
      val incomingChannel: Channel =
        incomingChannel(monitored = MonitorState.ACTIVE)
      val bridgedChannel: Channel = bridgedChannel(
        name = "SIP/" + trunkName + "-000000a1"
      )

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel,
        wrapper = unrecordedTrunkDeviceWrapper
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(unrecordedTrunkDeviceWrapper.amiBus, Mockito.after(500).never())
        .publish(arg.capture)
    }

    /* Toggle recording
        Case 1: channel is being recorded
          Given the device call channel's **monitor state** (which is ACTIVE)
          Stop or Not the recording according to the **recording mode** of the end channel

        - XXX: the code written may conflict with the code in onPartyInformation
         in the case of two TrunkDeviceTracker
     */
    "if channel is being recorded, do nothing about recording if end channel is in mode recorded or recordedondemand" in {
      val recordingModes: Vector[String] =
        Vector("recorded", "recordedondemand")

      recordingModes.foreach(recordingMode =>
        new IncomingSipTrunkRecorded {
          val incomingChannel: Channel =
            incomingChannel(monitored = MonitorState.ACTIVE)
          val bridgedChannel: Channel = bridgedChannel()
            .updateVariable(Channel.VarNames.RecordingMode, recordingMode)

          sendPartyInformation(
            incomingChannel = incomingChannel,
            bridgedChannel = bridgedChannel,
            sourceTrackerType = SipDeviceTrackerType
          )

          val arg: ArgumentCaptor[AmiAction] =
            ArgumentCaptor.forClass(classOf[AmiAction])
          verify(wrapper.amiBus, Mockito.after(500).never())
            .publish(arg.capture)
        }
      )
    }

    "if channel is being recorded, stop recording if end channel is in mode notrecorded" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel =
        incomingChannel(monitored = MonitorState.ACTIVE)
      val bridgedChannel: Channel = bridgedChannel().updateVariable(
        Channel.VarNames.RecordingMode,
        value = "notrecorded"
      )

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel,
        sourceTrackerType = SipDeviceTrackerType
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus).publish(arg.capture)
      arg.getValue.message match {
        case stop: StopMixMonitorAction =>
          stop.getChannel shouldBe incomingChannelName
        case any =>
          fail(
            s"Should get StopMixMonitorAction on channel $incomingChannelName, got: $any"
          )
      }
    }

    "if channel is being recorded, stop recording if end channel is in mode notrecorded, only if rules enabled" in new IncomingSipTrunkRecorded {
      val unruledDeviceWrapper: DeviceActorWrapper =
        deviceWrapper(trunk, enableRules = false)
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.ACTIVE
      )
      val bridgedChannel: Channel = bridgedChannel()
        .updateVariable(Channel.VarNames.RecordingMode, "notrecorded")

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel,
        wrapper = unruledDeviceWrapper
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    /* Toggle recording
        Case 2: channel is recorded. The recording is currently paused.
          Given the device call channel's **monitor state** (which is PAUSED)
          Unpause or Stop the recording according to the **recording mode** of the end channel
     */
    "if channel is being recorded, if recording is paused, unpause recording if end channel is in recorded mode" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.PAUSED
      )
      val bridgedChannel: Channel = bridgedChannel()
        .updateVariable(Channel.VarNames.RecordingMode, value = "recorded")

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg = ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus).publish(arg.capture)
      arg.getValue.message match {
        case unPauseMonitor: PauseMixMonitorAction =>
          unPauseMonitor.getChannel should be(incomingChannelName)
          unPauseMonitor.getState should be(0)
          unPauseMonitor.getDirection should be("both")
        case any =>
          fail(s"Should get PauseMixMonitorAction, got: $any")
      }
    }

    /* Toggle recording
            Case 2: channel is recorded. The recording is currently paused.
              Given the device call channel's **monitor state** (which is PAUSED)
              Unpause or Stop the recording according to the **recording mode** of the end channel
     */
    "if channel is being recorded on demand and was unpaused at least once, if recording is paused, unpause recording if end channel is in recorded mode" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.PAUSED
      ).addVariables(
        Map(
          "__MONITOR_PAUSED"    -> "true",
          "recordingid"         -> "bsc-1737133222.22",
          "XUC_RECORD_UNPAUSED" -> "true"
        )
      )
      val bridgedChannel: Channel = bridgedChannel()
        .updateVariable(Channel.VarNames.RecordingMode, value = "recorded")

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus).publish(arg.capture)
      arg.getValue.message match {
        case unPauseMonitor: PauseMixMonitorAction =>
          unPauseMonitor.getChannel should be(incomingChannelName)
          unPauseMonitor.getState should be(0)
          unPauseMonitor.getDirection should be("both")
        case any =>
          fail(s"Should get PauseMixMonitorAction, got: $any")
      }
    }

    /* Toggle recording
           channel is recorded on demand, and recording wasn't started
              Given the device call channel's **monitor state** (which is PAUSED)
              Unpause or Stop the recording according to the **recording mode** of the end channel
     */
    "if channel is in mode recorded on demand, its state is paused and it has never been started, if remote channel mode is 'recorded', it must start recording" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.PAUSED
      ).addVariables(
        Map(
          "__MONITOR_PAUSED" -> "true",
          "recordingid"      -> "bsc-1737133222.22"
        )
      )

      val bridgedChannel: Channel = bridgedChannel()
        .updateVariable(Channel.VarNames.RecordingMode, "recorded")

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, times(2)).publish(arg.capture)
      arg.getAllValues.get(0).message match {
        case setVar: SetVarAction =>
          setVar.getChannel should be(incomingChannel.name)
          setVar.getVariable should be(XucRecordUnpaused)
          setVar.getValue should be("true")
        case any =>
          fail(s"Should get SetVarAction, got: $any")
      }
      arg.getAllValues.get(1).message match {
        case startMixMonitor: MixMonitorAction =>
          startMixMonitor.getChannel should be(incomingChannelName)
          startMixMonitor.getFile should be(
            "/var/spool/xivocc-recording/audio/bsc-1737133222.22.wav"
          )
          startMixMonitor.getCommand should be(
            "/usr/bin/xivocc-synchronize-file /var/spool/xivocc-recording/audio/bsc-1737133222.22.wav"
          )

        case any =>
          fail(s"Should get StartMixMonitorAction, got: $any")
      }
    }

    "if channel is in mode recorded on demand, its state is paused and it has never been started, if remote channel mode is not 'recorded', it must not start recording" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.PAUSED
      ).addVariables(
        Map(
          "__MONITOR_PAUSED" -> "true",
          "recordingid"      -> "bsc-1737133222.22"
        )
      )
      val bridgedChannel: Channel = bridgedChannel()
        .updateVariable(Channel.VarNames.RecordingMode, "recordedondemand")

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "if channel is being recorded on pause, do nothing about recording if end channel is in recordedondemand mode" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.PAUSED
      )
      val bridgedChannel: Channel = bridgedChannel()
        .updateVariable(
          Channel.VarNames.RecordingMode,
          value = "recordedondemand"
        )

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "if channel is being recorded on pause, stop recording if end channel is in notrecorded mode" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.PAUSED
      )
      val bridgedChannel: Channel = bridgedChannel()
        .updateVariable(Channel.VarNames.RecordingMode, "notrecorded")

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus).publish(arg.capture)
      arg.getValue.message match {
        case stop: StopMixMonitorAction =>
          stop.getChannel shouldBe incomingChannelName
        case any =>
          fail(
            s"Should get StopMixMonitorAction on channel $incomingChannelName, got: $any"
          )
      }
    }

    "if channel is being recorded on pause, stop recording if end channel is in notrecorded mode, only if rules enabled" in new IncomingSipTrunkRecorded {
      val unruledWrapper: DeviceActorWrapper =
        deviceWrapper(trunk, enableRules = false)
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.PAUSED
      )
      val bridgedChannel: Channel = bridgedChannel()
        .updateVariable(Channel.VarNames.RecordingMode, "notrecorded")

      sendPartyInformation(
        incomingChannel = incomingChannel,
        bridgedChannel = bridgedChannel,
        wrapper = unruledWrapper
      )

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(unruledWrapper.amiBus, Mockito.after(500).never())
        .publish(arg.capture)
    }

    /* Toggle recording
      Case 3: channel is not being recorded
       Given the device call channel's **monitor state** (which is DISABLED)
       AND given the device call channel's **recording mode** (which is notrecorded)
       Stop the recording on remote channel if it is activted
     */
    "if channel is not being recorded and is in notrecorded mode, stop recording of the given remotechannel if it is being recorded (whatever its recording mode)" in {
      val remoteRecordingModes = Vector("recorded", "recordedondemand")
      val remoteChanMonitorStates =
        Vector(MonitorState.ACTIVE, MonitorState.PAUSED)

      remoteRecordingModes.foreach(remoteRecordingMode => {
        remoteChanMonitorStates.foreach(remoteChanMonitorState =>
          new IncomingSipTrunkRecorded {
            val incomingChannel: Channel = incomingChannel()
              .updateVariable(Channel.VarNames.RecordingMode, "notrecorded")

            val startNotRecordedChannelName = "Local/3000@default-000000a1;1"
            val startNotRecordedChannel: Channel = bridgedChannel(
              id = "1522140030.12",
              name = startNotRecordedChannelName
            )

            val remoteRecordedChannelName = "Local/3000@default-000000a1;2"
            val remoteRecordedChannel: Channel = bridgedChannel(
              id = "1522140030.14",
              name = remoteRecordedChannelName,
              monitored = remoteChanMonitorState
            )
              .updateVariable(
                Channel.VarNames.RecordingMode,
                remoteRecordingMode
              )

            val endNotRecordedChannelName =
              "Local/id-118@agentcallback-000000a1;1"
            val endNotRecordedChannel: Channel = bridgedChannel(
              id = "1522140032.12",
              name = endNotRecordedChannelName
            )
              .updateVariable(
                Channel.VarNames.RecordingMode,
                remoteRecordingMode
              )

            val pfc: PathsFromChannel = PathsFromChannel(
              NodeChannel(incomingChannelName),
              Set(
                AsteriskPath(
                  NodeChannel(startNotRecordedChannelName),
                  NodeChannel(remoteRecordedChannelName),
                  NodeChannel(endNotRecordedChannelName)
                )
              )
            )
            wrapper.device ! incomingChannel
            wrapper.device ! pfc
            val endParty: PartyInformation =
              SingleDeviceTracker.PartyInformation(
                incomingChannelName,
                endNotRecordedChannel,
                UnknownDeviceTrackerType
              )
            wrapper.device ! endParty

            val arg1: ArgumentCaptor[AmiAction] =
              ArgumentCaptor.forClass(classOf[AmiAction])
            verify(wrapper.amiBus, Mockito.after(500).never())
              .publish(arg1.capture)

            val recParty: PartyInformation =
              SingleDeviceTracker.PartyInformation(
                incomingChannelName,
                remoteRecordedChannel,
                UnknownDeviceTrackerType
              )
            wrapper.device ! recParty

            val arg2: ArgumentCaptor[AmiAction] =
              ArgumentCaptor.forClass(classOf[AmiAction])
            verify(wrapper.amiBus).publish(arg2.capture)

            arg2.getValue.message match {
              case stop: StopMixMonitorAction =>
                stop.getChannel shouldBe remoteRecordedChannelName
              case any =>
                fail(
                  s"Should get StopMixMonitorAction on channel $remoteRecordedChannelName, got: $any"
                )
            }

            reset(wrapper.amiBus)
            val startParty: PartyInformation =
              SingleDeviceTracker.PartyInformation(
                incomingChannelName,
                startNotRecordedChannel,
                UnknownDeviceTrackerType
              )
            wrapper.device ! startParty

            val arg3: ArgumentCaptor[AmiAction] =
              ArgumentCaptor.forClass(classOf[AmiAction])
            verify(wrapper.amiBus, Mockito.after(500).never())
              .publish(arg3.capture)
          }
        )
      })
    }

    "if channel is not being recorded and is in notrecorded mode, do nothing if no channel in call is not being recorded" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel(
        monitored = MonitorState.DISABLED
      )
        .updateVariable(Channel.VarNames.RecordingMode, value = "notrecorded")
      val remoteNotRecordedChannelName = "Local/3000@default-000000a1;2"
      val remoteNotRecordedChannel: Channel = bridgedChannel(
        name = remoteNotRecordedChannelName,
        monitored = MonitorState.DISABLED
      )
        .updateVariable(Channel.VarNames.RecordingMode, value = "notrecorded")

      val pfc: PathsFromChannel = PathsFromChannel(
        NodeChannel(incomingChannelName),
        Set(
          AsteriskPath(
            NodeChannel("Local/3000@default-000000a1;1"),
            NodeChannel(remoteNotRecordedChannelName),
            NodeChannel("Local/id-118@agentcallback-000000a1;")
          )
        )
      )
      wrapper.device ! incomingChannel
      wrapper.device ! pfc
      val party: PartyInformation = SingleDeviceTracker.PartyInformation(
        incomingChannelName,
        remoteNotRecordedChannel,
        UnknownDeviceTrackerType
      )

      wrapper.device ! party

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "if channel is not being recorded but has not its mode in notrecorded, do nothing" in new IncomingSipTrunkRecorded {
      val incomingChannel: Channel = incomingChannel()
        .updateVariable(Channel.VarNames.RecordingMode, value = "something")
      val remoteRecordedChannelName = "Local/3000@default-000000a1;2"
      val remoteRecordedChannel: Channel = bridgedChannel(
        remoteRecordedChannelName,
        monitored = MonitorState.ACTIVE
      )
        .updateVariable(Channel.VarNames.RecordingMode, value = "recorded")

      val pfc: PathsFromChannel = PathsFromChannel(
        NodeChannel(incomingChannelName),
        Set(
          AsteriskPath(
            NodeChannel("Local/3000@default-000000a1;1"),
            NodeChannel(remoteRecordedChannelName),
            NodeChannel("Local/id-118@agentcallback-000000a1;")
          )
        )
      )
      wrapper.device ! incomingChannel
      wrapper.device ! pfc
      val party: PartyInformation = SingleDeviceTracker.PartyInformation(
        incomingChannelName,
        remoteRecordedChannel,
        UnknownDeviceTrackerType
      )

      wrapper.device ! party

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])
      verify(wrapper.amiBus, Mockito.after(500).never()).publish(arg.capture)
    }

    "really remove call when asked to" in {
      val unknown = mock[XivoFeature]
      val wrapper = deviceWrapper(unknown)

      val cname = "SIP/abcd-00000001"
      val c = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )

      wrapper.device ! c
      wrapper.device ! wrapper.device.underlyingActor.RemoveChannel(cname)

      wrapper.device ! SingleDeviceTracker.GetCalls

      expectMsg(SingleDeviceTracker.Calls(List.empty))
    }

    "send initial call state when actor wants to be notified of calls" in {
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        None,
        "123.123.123.1",
        number = Some("1007")
      )
      val wrapper = deviceWrapper(line)

      wrapper.device ! SingleDeviceTracker.MonitorCalls(line.trackingInterface)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))
    }

    "allow other actors to monitor calls for a device" in {
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        None,
        "123.123.123.1",
        number = Some("1007")
      )
      val wrapper = deviceWrapper(line)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val calls = DeviceCalls(
        line.trackingInterface,
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))
      )

      wrapper.device ! SingleDeviceTracker.MonitorCalls(line.trackingInterface)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))

      wrapper.device ! channel
      expectMsg(calls)
    }

    "allow other actors to monitor calls for a device excluding hangup calls" in {
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        None,
        "123.123.123.1",
        number = Some("1007")
      )
      val wrapper = deviceWrapper(line)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val calls = DeviceCalls(
        line.trackingInterface,
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))
      )

      wrapper.device ! SingleDeviceTracker.MonitorCalls(line.trackingInterface)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))

      wrapper.device ! channel
      expectMsg(calls)

      wrapper.device ! channel.copy(state = ChannelState.HUNGUP)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))
    }

    "allow other actors to unmonitor calls for a device" in {
      val line = makeLine(
        1,
        "default",
        "sip",
        "abcd",
        None,
        None,
        "123.123.123.1",
        number = Some("1007")
      )
      val wrapper = deviceWrapper(line)

      val cname = "SIP/abcd-00000001"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.ORIGINATING
      )
      val calls = DeviceCalls(
        line.trackingInterface,
        Map(cname -> DeviceCall(cname, Some(channel), Set.empty, Map.empty))
      )

      wrapper.device ! SingleDeviceTracker.MonitorCalls(line.trackingInterface)
      expectMsg(DeviceCalls(line.trackingInterface, Map.empty))
      wrapper.device ! SingleDeviceTracker.UnMonitorCalls(
        line.trackingInterface
      )
      wrapper.device ! channel

      expectNoMessage(100.millis)
    }

    "drop calls concerning a loop after it's been detected" in {
      val wrapper = deviceWrapper(LocalChannelFeature)
      val cname   = "Local/1002@default-00000047;2"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      wrapper.device ! channel

      wrapper.device ! GetCalls
      expectMsg(Calls(List(call)))

      wrapper.device ! LoopDetected(Set(NodeLocalChannel(cname)))

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))
    }

    "ignore Channel events concerning a loop after it's been detected" in {
      val wrapper = deviceWrapper(LocalChannelFeature)
      val cname   = "Local/1002@default-00000047;2"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      wrapper.device ! channel
      wrapper.device ! LoopDetected(Set(NodeLocalChannel(cname)))

      wrapper.device ! channel

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))

    }

    "ignore PathsFromChannel events concerning a loop after it's been detected" in {
      val wrapper = deviceWrapper(LocalChannelFeature)
      val cname   = "Local/1002@default-00000047;2"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      wrapper.device ! channel
      wrapper.device ! LoopDetected(Set(NodeLocalChannel(cname)))

      val pfc = PathsFromChannel(
        NodeChannel(cname),
        Set(AsteriskPath(NodeChannel("SIP/abcd-00001")))
      )

      wrapper.device ! pfc

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))

    }

    "ignore PartyInformation events concerning a loop after it's been detected" in {
      val wrapper = deviceWrapper(LocalChannelFeature)
      val cname   = "Local/1002@default-00000047;2"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      wrapper.device ! channel
      wrapper.device ! LoopDetected(Set(NodeLocalChannel(cname)))

      val bridgedChannel = Channel(
        "123456789.124",
        "SIP/abcd-000001",
        CallerId("James Bond", "1007"),
        "",
        ChannelState.RINGING
      )
      val party = SingleDeviceTracker.PartyInformation(
        cname,
        bridgedChannel,
        UnknownDeviceTrackerType
      )

      wrapper.device ! party

      wrapper.device ! GetCalls
      expectMsg(Calls(List.empty))

    }

    "schedule looped channel removal 30 seconds after detection" in {
      val mockScheduler   = mock[Scheduler]
      val channelTracker  = TestProbe()
      val graphTracker    = TestProbe()
      val bus             = mock[XucEventBus]
      val amiBus          = mock[XucAmiBus]
      val mediator        = TestProbe()
      val mediatorWrapper = new MediatorHelperSpec(mediator)

      val device = TestActorRef(
        new UnknownDeviceTracker(
          "Local/",
          channelTracker.ref,
          graphTracker.ref,
          amiBus,
          defaultConfig,
          mediator.ref
        ) {
          override def scheduler: Scheduler = mockScheduler
        }
      )

      val cname1 = "Local/1002@default-00000047;1"
      val cname2 = "Local/1002@default-00000047;2"

      device ! LoopDetected(
        Set(NodeLocalChannel(cname1), NodeLocalChannel(cname2))
      )

      verify(mockScheduler, timeout(500)).scheduleOnce(
        30.seconds,
        device,
        device.underlyingActor.RemoveLoopChannel(cname1)
      )(scala.concurrent.ExecutionContext.Implicits.global, device)
      verify(mockScheduler, timeout(500)).scheduleOnce(
        30.seconds,
        device,
        device.underlyingActor.RemoveLoopChannel(cname2)
      )(scala.concurrent.ExecutionContext.Implicits.global, device)
    }

    "ensure loop block list is cleaned after receiving RemoveLoopChannel" in {
      val mockScheduler   = mock[Scheduler]
      val channelTracker  = TestProbe()
      val graphTracker    = TestProbe()
      val bus             = mock[XucEventBus]
      val amiBus          = mock[XucAmiBus]
      val mediator        = TestProbe()
      val mediatorWrapper = new MediatorHelperSpec(mediator)

      val device = TestActorRef(
        new UnknownDeviceTracker(
          "Local/",
          channelTracker.ref,
          graphTracker.ref,
          amiBus,
          defaultConfig,
          mediator.ref
        ) {
          override def scheduler = mockScheduler
        }
      )

      val cname = "Local/1002@default-00000047;1"
      val channel = Channel(
        "123456789.123",
        cname,
        CallerId("James Bond", "1007"),
        "",
        ChannelState.UP
      )
      val call = DeviceCall(cname, Some(channel), Set.empty, Map.empty)

      device ! LoopDetected(Set(NodeLocalChannel(cname)))
      device ! device.underlyingActor.RemoveLoopChannel(cname)

      device ! channel

      device ! GetCalls
      expectMsg(Calls(List(call)))
    }
  }

  "DeviceMessageWithInterface trait" should {

    case class DummyDM(interface: DeviceMessage.DeviceInterface)
        extends DeviceMessageWithInterface

    "compare inner interface with interface with trailing dash" in {
      DummyDM("SIP/abcdefgh").isFor("SIP/abcdefgh-") should be(true)
    }

    "compare inner interface with interface without trailing dash" in {
      DummyDM("SIP/abcdefgh").isFor("SIP/abcdefgh") should be(true)
    }

    "compare inner interface with trailing dash with interface with trailing dash" in {
      DummyDM("SIP/abcdefgh-").isFor("SIP/abcdefgh-") should be(true)
    }

    "compare inner interface with trailing dash with interface without trailing dash" in {
      DummyDM("SIP/abcdefgh-").isFor("SIP/abcdefgh") should be(true)
    }

  }
}
