package services.calltracking

import org.asteriskjava.manager.event._
import graph._
import graph.GraphBuilder._
import services.calltracking.graph.NodeBridge.BridgeCreator
import services.calltracking.{AmiEventToAsteriskEdge => AmiEventToAsteriskEdge_}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class AmiEventToAsteriskEdgeSpec
    extends AnyWordSpec
    with Matchers
    with AsteriskObjectHelper {
  def AmiEventToAsteriskEdge(
      graph: AsteriskGraph,
      event: ManagerEvent
  ): (AsteriskGraph, Set[NodeChannelLike]) =
    AmiEventToAsteriskEdge_.apply(graph, event, "default")

  "AmiEventToAsteriskEdgeSpec" should {
    "connect a channel to a bridge when it enters a bridge" in {
      val c1name      = "c1"
      val bridgeEvent = new BridgeEnterEvent(this)
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")
      bridgeEvent.setBridgeCreator("bc1")
      val (graph, _) = AmiEventToAsteriskEdge(AsteriskGraph(), bridgeEvent)

      graph.neighbors(NodeChannel(c1name)) should contain only NodeBridge(
        "b1",
        Some(new BridgeCreator("bc1"))
      )
    }

    "connect a channel to a bridge when it enters a bridge with creator null" in {
      val c1name      = "c1"
      val bridgeEvent = new BridgeEnterEvent(this)
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")
      bridgeEvent.setBridgeCreator(null)
      val (graph, _) = AmiEventToAsteriskEdge(AsteriskGraph(), bridgeEvent)

      graph.neighbors(NodeChannel(c1name)) should contain only NodeBridge(
        "b1",
        None
      )
    }

    "connect two channels together when they enter the same bridge" in {
      val c1name = "c1"
      val c2name = "c2"

      val bridgeEvent = new BridgeEnterEvent(this)
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")
      bridgeEvent.setBridgeCreator("bc1")

      val (graph, _) = AmiEventToAsteriskEdge(AsteriskGraph(), bridgeEvent)

      bridgeEvent.setChannel(c2name)
      val (graph2, _) = AmiEventToAsteriskEdge(graph, bridgeEvent)

      graph2.remoteChannels(
        NodeChannel(c1name)
      ) should contain only NodeChannel(c2name)
    }

    "connect two channels together when they have the same call id (SIPCALLID)" in {
      val c1name = "c1"
      val c2name = "c2"

      val varSetEvent = new VarSetEvent(this)
      varSetEvent.setChannel(c1name)
      varSetEvent.setVariable("SIPCALLID")
      varSetEvent.setValue("740e0c6f42fb6b3771c5d2bb4fc93296@10.49.0.2:5060")

      val (graph, _) = AmiEventToAsteriskEdge(AsteriskGraph(), varSetEvent)

      varSetEvent.setChannel(c2name)
      val (graph2, _) = AmiEventToAsteriskEdge(graph, varSetEvent)

      graph2.remoteChannels(
        NodeChannel(c1name)
      ) should contain only NodeChannel(c2name)
    }

    "connect two channels together when they have the same call id (XIVO_SIPCALLID)" in {
      val c1name = "c1"
      val c2name = "c2"

      val varSetEvent = new VarSetEvent(this)
      varSetEvent.setChannel(c1name)
      varSetEvent.setVariable("XIVO_SIPCALLID")
      varSetEvent.setValue("740e0c6f42fb6b3771c5d2bb4fc93296@10.49.0.2:5060")

      val (graph, _) = AmiEventToAsteriskEdge(AsteriskGraph(), varSetEvent)

      varSetEvent.setChannel(c2name)
      val (graph2, _) = AmiEventToAsteriskEdge(graph, varSetEvent)

      graph2.remoteChannels(
        NodeChannel(c1name)
      ) should contain only NodeChannel(c2name)
    }

    "disconnect two channels together when one leave the bridge" in {
      val c1name = "c1"
      val c2name = "c2"

      val bridgeEvent = new BridgeEnterEvent(this)
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")
      bridgeEvent.setBridgeCreator("bc1")

      val (graph, _) = AmiEventToAsteriskEdge(AsteriskGraph(), bridgeEvent)

      bridgeEvent.setChannel(c2name)
      val (graph2, _) = AmiEventToAsteriskEdge(graph, bridgeEvent)

      val leaveEvent = new BridgeLeaveEvent(this)
      leaveEvent.setChannel(c2name)
      leaveEvent.setBridgeUniqueId("b1")
      leaveEvent.setBridgeCreator("bc1")
      val (graph3, _) = AmiEventToAsteriskEdge(graph2, leaveEvent)

      graph3.remoteChannels(NodeChannel(c1name)) shouldBe empty
    }

    "connect two channels together when one dial the other" in {
      val c1name = "c1"
      val c2name = "c2"

      val dialEvent = new DialBeginEvent(this)
      dialEvent.setChannel(c1name)
      dialEvent.setDestination(c2name)

      val (graph, _) = AmiEventToAsteriskEdge(AsteriskGraph(), dialEvent)

      graph.remoteChannels(NodeChannel(c1name)) should contain only NodeChannel(
        c2name
      )
    }

    "ignore DialEvent when one of the channel name is null" in {
      val c2name = "c2"

      val dialEvent = new DialBeginEvent(this)
      dialEvent.setChannel(null)
      dialEvent.setDestination(c2name)
      val initial = AsteriskGraph()

      val (graph, _) = AmiEventToAsteriskEdge(initial, dialEvent)

      graph should be(initial)
    }

    "disconnect two channels together dial ends" in {
      val c1name = "c1"
      val c2name = "c2"

      val dialEvent = new DialBeginEvent(this)
      dialEvent.setChannel(c1name)
      dialEvent.setDestination(c2name)

      val (graph, _) = AmiEventToAsteriskEdge(AsteriskGraph(), dialEvent)

      val dialEndEvent = new DialEndEvent(this)
      dialEndEvent.setChannel(c1name)
      dialEndEvent.setDestination(c2name)

      val (graph2, _) = AmiEventToAsteriskEdge(graph, dialEndEvent)

      graph2.remoteChannels(NodeChannel(c1name)) shouldBe empty
    }

    "connect two channels together when bridged with a local bridge" in {
      val c1name  = "c1"
      val c2name  = "c2"
      val lc1name = "Local/c1"
      val lc2name = "Local/c2"

      val evt = new LocalBridgeEvent(this)
      evt.setLocalOneChannel(lc1name)
      evt.setLocalTwoChannel(lc2name)
      val (graph1, _) = AmiEventToAsteriskEdge(AsteriskGraph(), evt)

      val bridgeEvent = new BridgeEnterEvent(this)
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")
      bridgeEvent.setBridgeCreator("bc1")
      val (graph2, _) = AmiEventToAsteriskEdge(graph1, bridgeEvent)

      bridgeEvent.setChannel(lc1name)
      val (graph3, _) = AmiEventToAsteriskEdge(graph2, bridgeEvent)

      bridgeEvent.setChannel(c2name)
      bridgeEvent.setBridgeUniqueId("b2")
      bridgeEvent.setBridgeCreator("bc2")
      val (graph4, _) = AmiEventToAsteriskEdge(graph3, bridgeEvent)

      bridgeEvent.setChannel(lc2name)
      val (graph5, _) = AmiEventToAsteriskEdge(graph4, bridgeEvent)

      graph5.remoteChannels(NodeChannel(c1name)) should contain.only(
        NodeChannel(c2name),
        NodeLocalChannel(lc1name),
        NodeLocalChannel(lc2name)
      )
    }

    "get impacted channels when two channels are bridged" in {
      val c1name = "c1"
      val c2name = "c2"

      val bridgeEvent = new BridgeEnterEvent(this)
      bridgeEvent.setChannel(c1name)
      bridgeEvent.setBridgeUniqueId("b1")
      bridgeEvent.setBridgeCreator("bc1")

      val (graph1, channels1) =
        AmiEventToAsteriskEdge(AsteriskGraph(), bridgeEvent)

      channels1 should contain only NodeChannel(c1name)

      bridgeEvent.setChannel(c2name)
      val (graph2, channels2) = AmiEventToAsteriskEdge(graph1, bridgeEvent)

      graph2.remoteChannels(
        NodeChannel(c1name)
      ) should contain only NodeChannel(c2name)
      channels2 should contain.only(NodeChannel(c1name), NodeChannel(c2name))
    }

    "get impacted channels when two channels are bridged through a local bridge" in {
      val lc1 = NodeLocalChannel("Local/lc1")
      val lc2 = NodeLocalChannel("Local/lc2")
      val lb  = NodeLocalBridge(lc1, lc2)
      val c1  = NodeChannel("c1")
      val c2  = NodeChannel("c2")
      val b1  = NodeBridge("b1", Some(new BridgeCreator("bc1")))
      val b2  = NodeBridge("b2", Some(new BridgeCreator("bc2")))

      val graphInitial = AsteriskGraph(
        c1 ~ b1 ~ lc1 ~ lb ~ lc2,
        c2 ~ b2
      )

      val bridgeEvent = new BridgeEnterEvent(this)
      bridgeEvent.setChannel(lc2.name)
      bridgeEvent.setBridgeUniqueId(b2.name)
      bridgeEvent.setBridgeCreator("bc2")

      val (_, channels) = AmiEventToAsteriskEdge(graphInitial, bridgeEvent)

      channels should contain.only(c1, c2, lc1, lc2)
    }

    "get impacted channels when two channels are disconnected" in {

      val graphInitial = AsteriskGraph(
        NodeChannel("c1") ~ NodeBridge(
          "b1",
          Some(new BridgeCreator("bc1"))
        ) ~ NodeChannel("c2")
      )

      val bridgeEvent = new BridgeLeaveEvent(this)
      bridgeEvent.setChannel("c1")
      bridgeEvent.setBridgeUniqueId("b1")
      bridgeEvent.setBridgeCreator("bc1")

      val (_, channels) = AmiEventToAsteriskEdge(graphInitial, bridgeEvent)

      channels should contain.only(NodeChannel("c1"), NodeChannel("c2"))
    }

    "get impacted channels when two channels are disconnected with a local bridge" in {
      val lc1 = NodeLocalChannel("Local/lc1")
      val lc2 = NodeLocalChannel("Local/lc2")
      val lb  = NodeLocalBridge(lc1, lc2)
      val c1  = NodeChannel("c1")
      val c2  = NodeChannel("c2")
      val b1  = NodeBridge("b1", Some(new BridgeCreator("bc1")))
      val b2  = NodeBridge("b2", Some(new BridgeCreator("bc2")))

      val graphInitial = AsteriskGraph(
        c1 ~ b1 ~ lc1 ~ lb ~ lc2 ~ c2 ~ b2
      )

      val bridgeEvent = new BridgeLeaveEvent(this)
      bridgeEvent.setChannel(lc2.name)
      bridgeEvent.setBridgeUniqueId(b2.name)
      bridgeEvent.setBridgeCreator("bc2")

      val (_, channels) = AmiEventToAsteriskEdge(graphInitial, bridgeEvent)

      channels should contain.only(c1, c2, lc1, lc2)
    }

    "keep connection of two local channels together when optimization begins" in {
      val lc1name = "Local/c1"
      val lc2name = "Local/c2"

      val optimizeBeginEvent = new LocalOptimizationBeginEvent(this)
      optimizeBeginEvent.setLocalOneChannel(lc1name)
      optimizeBeginEvent.setLocalTwoChannel(lc2name)
      optimizeBeginEvent.setId(1)

      val (graph, _) =
        AmiEventToAsteriskEdge(AsteriskGraph(), optimizeBeginEvent)

      graph.remoteChannels(
        NodeLocalChannel(lc1name)
      ) should contain only NodeLocalChannel(lc2name)
    }

    "remove connection of two local channels when optimization ends" in {
      val lc1name = "Local/c1"
      val lc2name = "Local/c2"

      val optimizeBeginEvent = new LocalOptimizationBeginEvent(this)
      optimizeBeginEvent.setLocalOneChannel(lc1name)
      optimizeBeginEvent.setLocalTwoChannel(lc2name)
      optimizeBeginEvent.setId(1)

      val (graph, _) =
        AmiEventToAsteriskEdge(AsteriskGraph(), optimizeBeginEvent)

      val optimizeEndEvent = new LocalOptimizationEndEvent(this)
      optimizeEndEvent.setLocalOneChannel(lc1name)
      optimizeEndEvent.setLocalTwoChannel(lc2name)
      optimizeEndEvent.setId(1)
      val (graph2, _) = AmiEventToAsteriskEdge(graph, optimizeEndEvent)

      graph2.remoteChannels(NodeLocalChannel(lc1name)) shouldBe empty
    }
  }
}
