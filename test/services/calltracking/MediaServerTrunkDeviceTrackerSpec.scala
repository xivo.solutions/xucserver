package services.calltracking

import org.joda.time.DateTime
import services.calltracking.AsteriskGraphTracker.{
  AsteriskPath,
  PathsFromChannel
}
import services.calltracking.ConferenceTracker._
import services.calltracking.SingleDeviceTracker._
import services.calltracking.graph.NodeBridge.BridgeCreator
import services.calltracking.graph._
import xivo.models.MediaServerTrunk

// MediaServerTrunkTracker should inherit the behavior of the UnknownDevicetracker
class MediaServerTrunkDeviceTrackerSpec extends UnknownDeviceTrackerSpec {

  private val mdsTrunk = MediaServerTrunk("from-mds1", SipDriver.SIP)
  private val topic: String =
    ConferenceTracker.conferenceParticipantEventTopic("my-conf", "10.181.0.2")

  "MediaServerTrunkDeviceTracker" should {

    val conference = ConferenceRoom(
      "4000",
      "Superconf",
      ConferenceBusy,
      Some(DateTime.now),
      List.empty,
      "default",
      "xivohost"
    )

    "be of correct type" in {
      val wrapper = deviceWrapper(mdsTrunk)
      wrapper.device.underlyingActor.deviceTrackerType should be(
        MediaServerTrunkDeviceTrackerType
      )
    }

    "forward join conference event to correct channel" in {
      val cname       = "SIP/from-mds1-00000010"
      val remoteCname = "SIP/abcd-0000001"
      val pfc = PathsFromChannel(
        NodeChannel(cname, "main"),
        Set(
          AsteriskPath(
            NodeMdsTrunkBridge("SIP_CALLID-123456"),
            NodeChannel("SIP/to-mds1-00000009", "mds1"),
            NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1"))),
            NodeChannel(remoteCname, "mds1")
          )
        )
      )

      val wrapper = deviceWrapper(mdsTrunk)
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc

      wrapper.device ! DeviceJoinConference(conference, cname, topic)
      wrapper.parent.expectMsg(
        DeviceJoinConference(
          conference,
          remoteCname,
          topic,
          localChannel = Some(cname)
        )
      )
    }

    "forward leave conference event to correct channel" in {
      val cname       = "SIP/from-mds1-00000010"
      val remoteCname = "SIP/abcd-0000001"
      val pfc = PathsFromChannel(
        NodeChannel(cname, "main"),
        Set(
          AsteriskPath(
            NodeMdsTrunkBridge("SIP_CALLID-123456"),
            NodeChannel("SIP/to-mds1-00000009", "mds1"),
            NodeBridge("b1", "mds1", Some(new BridgeCreator("bc1"))),
            NodeChannel(remoteCname, "mds1")
          )
        )
      )

      val wrapper = deviceWrapper(mdsTrunk)
      wrapper.parent.expectMsgType[DevicesTracker.RegisterActor]

      wrapper.device ! pfc

      wrapper.device ! DeviceLeaveConference(conference, cname, topic)
      wrapper.parent.expectMsg(
        DeviceLeaveConference(conference, remoteCname, topic)
      )
    }
  }
}
