package services.line

import org.asteriskjava.manager.action.{
  MixMonitorAction,
  PauseMixMonitorAction,
  SetVarAction
}
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.{times, verify}
import pekkotest.TestKitSpec
import services.XucAmiBus
import services.XucAmiBus.AmiAction
import services.calltracking.DeviceCall
import xivo.xucami.models.Channel.VarNames.XucRecordUnpaused
import xivo.xucami.models.{Channel, MonitorState}
import xuctest.{ChannelGen, ScalaTestTools}

class RecordingActionControllerSpec
    extends TestKitSpec("RecordingActionControllerSpec")
    with ScalaTestTools
    with ChannelGen {

  class Helper {
    val amiBus: XucAmiBus = mock[XucAmiBus]

  }

  "A recording action controller" should {
    "return first recorded channel" in new Helper {

      val interface        = "SIP/abcd"
      val c: Channel       = bchan(interface).copy(monitored = MonitorState.ACTIVE)
      val call: DeviceCall = DeviceCall(interface, Some(c), Set.empty, Map())

      new RecordingActionControllerImpl(amiBus).getMonitoredChannel(
        Map(interface -> call)
      ) should be(Some(c))

    }
    "return first channel recorded found" in new Helper {

      val interface      = "SIP/abcd"
      val otherinterface = "SIP/sklmdjf"
      val c: Channel     = bchan(interface).copy(monitored = MonitorState.DISABLED)
      val rc: Channel    = bchan(interface).copy(monitored = MonitorState.ACTIVE)
      val call: DeviceCall =
        DeviceCall(otherinterface, Some(c), Set.empty, Map(rc.name -> rc))

      new RecordingActionControllerImpl(amiBus).getMonitoredChannel(
        Map(interface -> call)
      ) should be(Some(rc))

    }

    "return first recorded channel in pause" in new Helper {

      val interface        = "SIP/abcd-0000001"
      val c: Channel       = bchan(interface).copy(monitored = MonitorState.PAUSED)
      val call: DeviceCall = DeviceCall(interface, Some(c), Set.empty, Map())

      new RecordingActionControllerImpl(amiBus).getMonitoredChannelOnPause(
        Map(interface -> call)
      ) should be(Some(c))

    }

    "return first channel recorded in pause when remote channels exists" in new Helper {

      val interface      = "SIP/abcd"
      val otherinterface = "SIP/sklmdjf"
      val c: Channel     = bchan(interface, m = MonitorState.DISABLED)
      val rc: Channel    = bchan(interface, m = MonitorState.PAUSED)
      val call: DeviceCall =
        DeviceCall(otherinterface, Some(c), Set.empty, Map(rc.name -> rc))

      new RecordingActionControllerImpl(amiBus).getMonitoredChannelOnPause(
        Map(interface -> call)
      ) should be(Some(rc))

    }

    "pause a recorded channel" in new Helper {

      val interface           = "SIP/ilsudf"
      val recChannel: Channel = bchan(interface, m = MonitorState.ACTIVE)

      new RecordingActionControllerImpl(amiBus).pauseRecording(recChannel)

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case pauseMonitor: PauseMixMonitorAction =>
          pauseMonitor.getChannel should be(recChannel.name)
          pauseMonitor.getState should be(1)
          pauseMonitor.getDirection should be("both")
        case any =>
          fail(s"Should get PauseMixMonitorAction, got: $any")
      }

      arg.getValue.reference should be(Some(recChannel.id))

    }

    "unpause a paused recorded channel" in new Helper {

      val interface = "SIP/ilsudf"
      val recChannel: Channel = bchan(
        interface,
        m = MonitorState.PAUSED
      )

      new RecordingActionControllerImpl(amiBus).unPauseRecording(recChannel)

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case unPauseMonitor: PauseMixMonitorAction =>
          unPauseMonitor.getChannel should be(recChannel.name)
          unPauseMonitor.getState should be(0)
          unPauseMonitor.getDirection should be("both")
        case any =>
          fail(s"Should get PauseMixMonitorAction, got: $any")
      }
      arg.getValue.reference should be(Some(recChannel.id))
    }

    "activate recording on recorded on demand channel" in new Helper {

      val interface = "SIP/ilsudf"
      val recChannel: Channel = bchan(
        interface,
        m = MonitorState.PAUSED
      ).addVariables(
        Map(
          "__MONITOR_PAUSED" -> "true",
          "recordingid"    -> "bsc-1737133222.22"
        )
      )

      new RecordingActionControllerImpl(amiBus).unPauseRecording(recChannel)

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus, times(2)).publish(arg.capture)

      arg.getAllValues.get(0).message match {
        case setVar: SetVarAction =>
          setVar.getChannel should be(recChannel.name)
          setVar.getVariable should be(XucRecordUnpaused)
          setVar.getValue should be("true")
        case any =>
          fail(s"Should get SetVarAction, got: $any")
      }
      arg.getAllValues.get(0).reference should be(Some(recChannel.id))

      arg.getAllValues.get(1).message match {
        case startMixMonitor: MixMonitorAction =>
          startMixMonitor.getChannel should be(recChannel.name)
          startMixMonitor.getFile should be(
            "/var/spool/xivocc-recording/audio/bsc-1737133222.22.wav"
          )
          startMixMonitor.getCommand should be(
            "/usr/bin/xivocc-synchronize-file /var/spool/xivocc-recording/audio/bsc-1737133222.22.wav"
          )
        case any =>
          fail(s"Should get MixMonitorAction, got: $any")
      }
      arg.getAllValues.get(1).reference should be(Some(recChannel.id))
    }

    "pause recording on recorded on demand channel" in new Helper {

      val interface = "SIP/ilsudf"
      val recChannel: Channel = bchan(
        interface,
        m = MonitorState.ACTIVE
      ).addVariables(
        Map(
          "__MONITOR_PAUSED"      -> "true",
          "XUC_RECORD_UNPAUSED" -> "true",
          "recordingid"         -> "bsc-1737133222.22"
        )
      )

      new RecordingActionControllerImpl(amiBus).pauseRecording(recChannel)

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case pauseMonitor: PauseMixMonitorAction =>
          pauseMonitor.getChannel should be(recChannel.name)
          pauseMonitor.getState should be(1)
          pauseMonitor.getDirection should be("both")
        case any =>
          fail(s"Should get PauseMixMonitorAction, got: $any")
      }

      arg.getValue.reference should be(Some(recChannel.id))

    }

    "unpause recording recorded on demand channel" in new Helper {

      val interface = "SIP/ilsudf"
      val recChannel: Channel = bchan(
        interface,
        m = MonitorState.PAUSED
      ).addVariables(
        Map(
          "__MONITOR_PAUSED"      -> "true",
          "XUC_RECORD_UNPAUSED" -> "true",
          "recordingid"         -> "bsc-1737133222.22"
        )
      )

      new RecordingActionControllerImpl(amiBus).unPauseRecording(recChannel)

      val arg: ArgumentCaptor[AmiAction] =
        ArgumentCaptor.forClass(classOf[AmiAction])

      verify(amiBus).publish(arg.capture)

      arg.getValue.message match {
        case unPauseMonitor: PauseMixMonitorAction =>
          unPauseMonitor.getChannel should be(recChannel.name)
          unPauseMonitor.getState should be(0)
          unPauseMonitor.getDirection should be("both")
        case any =>
          fail(s"Should get PauseMixMonitorAction, got: $any")
      }
      arg.getValue.reference should be(Some(recChannel.id))

    }
  }
}
