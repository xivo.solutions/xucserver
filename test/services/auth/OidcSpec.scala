package services.auth

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.testkit.TestProbe
import models.XivoUser
import models.ws.auth.{
  AuthenticationException,
  AuthenticationInformation,
  SoftwareType
}
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito.*
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.Json
import play.api.libs.ws.*
import services.config.ConfigRepository
import system.TimeProvider
import xivo.xuc.XucBaseConfig
import xuctest.XucUserHelper

import java.time.{Clock, Instant, ZoneOffset}
import scala.concurrent.Future
import scala.util.{Failure, Success}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.joda.time.DateTime
import models.XucUser
import org.mockito.stubbing.OngoingStubbing

object OidcSampleToken extends XucUserHelper {
  val testUrl  = "http://localhost:8080/myrealm"
  val clientId = "xuc"
  val audience = "phone"

  val azureToken: String =
    """eyJ0eXAiOiJKV1QiLCJub25jZSI6Im9yWFJzT1FqT0JVMThfTUoxZlFIeTVFN1diOGNfbVduNllVVkZ3dXUzSUEiLCJhbGciOiJSUzI1NiIsIng1dCI6ImtXYmthYTZxczh3c1RuQndpaU5ZT2hIYm5BdyIsImtpZCI6ImtXYmthYTZxczh3c1RuQndpaU5ZT2hIYm5BdyJ9.eyJhdWQiOiIwMDAwMDAwMy0wMDAwLTAwMDAtYzAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC81NzQ4MDBmYS02MDgxLTRiNTEtYTg4NS02ZmI1MDllZTg2NDcvIiwiaWF0IjoxNzA2NzE4ODc3LCJuYmYiOjE3MDY3MTg4NzcsImV4cCI6MTcwNjcyNDE5NSwiYWNjdCI6MCwiYWNyIjoiMSIsImFjcnMiOlsidXJuOnVzZXI6cmVnaXN0ZXJzZWN1cml0eWluZm8iXSwiYWlvIjoiQVRRQXkvOFZBQUFBWUtYVFFFdnJmTWF5QXEyMk9rcWd5RjE0eDdlQU9oQkd1L0VJMXlFdmxyOWp3ZW81b2Z5REpKOXlYNHBEZ3h5UiIsImFtciI6WyJwd2QiXSwiYXBwX2Rpc3BsYXluYW1lIjoib2lkYy1kZW1vIiwiYXBwaWQiOiJiODQxNzRmNi00MDczLTQ1ZDItOWRhMC03Zjg2ODVjZDA3OTEiLCJhcHBpZGFjciI6IjAiLCJmYW1pbHlfbmFtZSI6IkJvbmQiLCJnaXZlbl9uYW1lIjoiSmFtZXMiLCJpZHR5cCI6InVzZXIiLCJpcGFkZHIiOiI4MC4xNC4xMzIuMjIxIiwibmFtZSI6Impib25kIiwib2lkIjoiY2U5NjJhZDktYmNiMy00ZGVhLWE5MGYtOGJkZTI4ZjA4YzRkIiwicGxhdGYiOiI4IiwicHVpZCI6IjEwMDMyMDAzMThBNTc3NjgiLCJyaCI6IjAuQWE0QS1nQklWNEZnVVV1b2hXLTFDZTZHUndNQUFBQUFBQUFBd0FBQUFBQUFBQUNyQU93LiIsInNjcCI6ImVtYWlsIG9wZW5pZCBwcm9maWxlIFVzZXIuUmVhZCIsInN1YiI6IlhtYkNOcHR3MWhkN3owak1yZjhIYmFzWTZqVDc0OE4yUzQ4QUhjZ3lMX2ciLCJ0ZW5hbnRfcmVnaW9uX3Njb3BlIjoiRVUiLCJ0aWQiOiI1NzQ4MDBmYS02MDgxLTRiNTEtYTg4NS02ZmI1MDllZTg2NDciLCJ1bmlxdWVfbmFtZSI6Impib25kQGFkbWlueGl2by5vbm1pY3Jvc29mdC5jb20iLCJ1cG4iOiJqYm9uZEBhZG1pbnhpdm8ub25taWNyb3NvZnQuY29tIiwidXRpIjoiajVrNWJpS1NuRUdGd244UVg4NFlBQSIsInZlciI6IjEuMCIsInhtc19zdCI6eyJzdWIiOiJlTmdUck1ReWN1UV9RZFU1QWJScHlSaXFlUUtlXzVtVjV6X190aTN5YnJBIn0sInhtc190Y2R0IjoxNjk5NDU0NTk3LCJ4bXNfdGRiciI6IkVVIn0.WFEzpKMkvIKiFZxO3tf-EhqlIC8_t62h3H54aFVED3wlQDCxxOgHELAOBJmDSB7rjF5Hm4rLfrdcgDgXXQWH437fMJLFDDmPmmGTo7U0ezi6l3sRebqRKyKwEyPUnxMyPXPyBCBqwUuB7XoUiwIncfOPqxkhJaLKoCpsDpHmHlYPufMzOBP1Noa4LG_Y2M6tXfs_9qaCdsZ5UI4hBvwj4-4I8udt1XAL_kEX5_MOFs0iP9ZgWHtLcYrDoaa_pdfBRaHP2heNTvbaAEz8Hdb02ZTbdyVIXQylauQ6ja5yUtTq96WL3WqKWLQrEbp1c22DEKRnq8lr12tcqAtjHXC1gQ"""

  val keycloakToken: String =
    """eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJQT2djN0gtc0pSVFNiNk9xdmlhQnZXRnJUbVhXZk9wVnRxcGo3THZmWEpBIn0.eyJleHAiOjE3MDY4MDY5MDgsImlhdCI6MTcwNjgwNjAwOCwiYXV0aF90aW1lIjoxNzA2ODA2MDA4LCJqdGkiOiI4YzNiZTlkMi0yZWM2LTQzYzItOTRjYy0wOGIzY2UwNjZiZGYiLCJpc3MiOiJodHRwczovLzE3Mi4yOS4xNDkuODo4NDQzL3JlYWxtcy9teXJlYWxtIiwiYXVkIjpbInh1YyIsImFjY291bnQiXSwic3ViIjoiNWQ4OGVkMmUtNTRiZC00MDc5LWFjZGEtMGM2NTdhNTNlNjk3IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoieHVjIiwibm9uY2UiOiJub25jZSIsInNlc3Npb25fc3RhdGUiOiJkYjAyOTk5Mi0xNWQ3LTQ4OTctYjVhMy02MGRhNWJiZDFlMjQiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIiIsIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtbXlyZWFsbSIsIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6Im9wZW5pZCBwcm9maWxlIGVtYWlsIiwic2lkIjoiZGIwMjk5OTItMTVkNy00ODk3LWI1YTMtNjBkYTViYmQxZTI0IiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJvaWRjdXNlciJ9.dgRnOL3CY6Z78UPNy3j-a5DHzqkpEESjtQ18V_w77UmBjhGH4t0ndzmy-TY__J5eRulwm4mxu8H78ysLgmH9LiJvsuXFPZzfDkqQQ_dShRMC3-bOH3iL_cR9KXCow13FiBevbqF0h4H-l-loljwh3V28tU8I9URqcW8xzO46R_qZTycfR6lM0hC0HyU3Yjzan7K2MdIkFF_IwCCS3_FvbPXXOAGsq2V1AMW6u9FdtGsDQ3JQh0nmqdxHWgXHYHlBIur5pAwnzL1YfSZrhoPIFQGkcbwwebA-LOtD2hArFF-pAIWUpMMCvjsPpvP47U4A_jtK7mk4wYI-DLRxCFjtaQ"""
  val key: String =
    """MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzyis1ZjfNB0bBgKFMSv
                |vkTtwlvBsaJq7S5wA+kzeVOVpVWwkWdVha4s38XM/pa/yr47av7+z3VTmvDRyAHc
                |aT92whREFpLv9cj5lTeJSibyr/Mrm/YtjCZVWgaOYIhwrXwKLqPr/11inWsAkfIy
                |tvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0
                |e+lf4s4OxQawWD79J9/5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWb
                |V6L11BWkpzGXSW4Hv43qa+GSYOD2QU68Mb59oSk2OB+BtOLpJofmbGEGgvmwyCI9
                |MwIDAQAB""".stripMargin.replaceAll("[ \t\n\r]*", "")

  val azureKid: String = "\"kWbkaa6qs8wsTnBwiiNYOhHbnAw\""
  val x5c: String =
    "\"MIIC/TCCAeWgAwIBAgIISlx9oAuA2/MwDQYJKoZIhvcNAQELBQAwLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDAeFw0yMzEyMDUxNzE2NTdaFw0yODEyMDUxNzE2NTdaMC0xKzApBgNVBAMTImFjY291bnRzLmFjY2Vzc2NvbnRyb2wud2luZG93cy5uZXQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDft/FsFDx4A/vOeqTwHyRTBUUR1Xs3xZdJ+WiMcl/200LKqsx3iCwC7hFTOHCUGUAfBguGbAw2BGz4HiAuwRRpYlSPHk7g3Fx+iQL7pMvWn8igswZ6rAU4xIG3FrU6ecvd4BAPUPs7Sk6MkGy3X9Jk+zCF1XNMQahah3/W8wUO7TZ3eiA/+26/bOXkl33a3Xti6pvrXAMovMegJ5QxNUTNBLjifSZetYmeJpjT7/OzyinDCZQdFbckn+bSLkHWb2UWZxVRQqHlVhk9p5zl10I7jsn3WdmLS1yAAOGo/OEAkXbRwES+QI/2RImKK/ayx52URtNJkZBO4Ls6U/0by2+9AgMBAAGjITAfMB0GA1UdDgQWBBR6Y4Oi5GGItIomQ0yZfH/woCAogzANBgkqhkiG9w0BAQsFAAOCAQEAaNbWUtHv3+ryZecDc7m6V1V1rWrVkUwC2QO78a2TprEN3owOeP0IHP42fbd/wcSsufTTtkk/J+fqL5dtsQ6zk2kDQfY5CgOyVCsaxVqHsg3t8fAWBkHiNScjZvRhLx4ll9QMOtLAwL4Os3Of0qtvP61zONP9sCJoUB6hkB33SRma1OyPZnYK/l3r0Y49+Ov0wahcdI4yZI72hFXlyyLnOT8dMbJDwZ9LNXA/BauEff4qTI4nIQk/lQKS6BjHzvXZbkHYEV/6M7r1g1syeahDmnaII+ZiBwp6tmAZKZC0Q0O7y3DmcPrHiZdv35AHadZY5cGWy1rw8NIMkaHWZ0mP6Q==\""

  val jwkSetAzure: String =
    s"""{
       | "keys":[
       |   {
       |     "kty":"RSA",
       |     "use":"sig",
       |     "kid":$azureKid,
       |     "x5t":$azureKid,
       |     "n":"37fxbBQ8eAP7znqk8B8kUwVFEdV7N8WXSflojHJf9tNCyqrMd4gsAu4RUzhwlBlAHwYLhmwMNgRs-B4gLsEUaWJUjx5O4NxcfokC-6TL1p_IoLMGeqwFOMSBtxa1OnnL3eAQD1D7O0pOjJBst1_SZPswhdVzTEGoWod_1vMFDu02d3ogP_tuv2zl5Jd92t17Yuqb61wDKLzHoCeUMTVEzQS44n0mXrWJniaY0-_zs8opwwmUHRW3JJ_m0i5B1m9lFmcVUUKh5VYZPaec5ddCO47J91nZi0tcgADhqPzhAJF20cBEvkCP9kSJiiv2ssedlEbTSZGQTuC7OlP9G8tvvQ",
       |     "e":"AQAB",
       |     "x5c":[$x5c]
       |   }
       | ]
       |}""".stripMargin

  val keycloakKid: String = "\"POgc7H-sJRTSb6OqviaBvWFrTmXWfOpVtqpj7LvfXJA\""

  val jwkSetKeycloak: String =
    s"""{
       | "keys":[
       |   {
       |     "kty":"RSA",
       |     "use":"sig",
       |     "kid":$keycloakKid,
       |     "x5t":$keycloakKid,
       |     "n":"37fxbBQ8eAP7znqk8B8kUwVFEdV7N8WXSflojHJf9tNCyqrMd4gsAu4RUzhwlBlAHwYLhmwMNgRs-B4gLsEUaWJUjx5O4NxcfokC-6TL1p_IoLMGeqwFOMSBtxa1OnnL3eAQD1D7O0pOjJBst1_SZPswhdVzTEGoWod_1vMFDu02d3ogP_tuv2zl5Jd92t17Yuqb61wDKLzHoCeUMTVEzQS44n0mXrWJniaY0-_zs8opwwmUHRW3JJ_m0i5B1m9lFmcVUUKh5VYZPaec5ddCO47J91nZi0tcgADhqPzhAJF20cBEvkCP9kSJiiv2ssedlEbTSZGQTuC7OlP9G8tvvQ",
       |     "e":"AQAB",
       |     "x5c":[$x5c]
       |   }
       | ]
       |}""".stripMargin

  val wellKnownResp: String = s"""{
                                 | "token_endpoint":"https://localhost:8080/574800fa-6081-4b51-a885-6fb509ee8647/oauth2/token",
                                 | "token_endpoint_auth_methods_supported": ["client_secret_post", "private_key_jwt", "client_secret_basic"],
                                 | "jwks_uri": "https://localhost:8080/common/discovery/keys",
                                 | "response_modes_supported": ["query", "fragment", "form_post"],
                                 | "subject_types_supported": ["pairwise"],
                                 | "id_token_signing_alg_values_supported": ["RS256"],
                                 | "response_types_supported": ["code", "id_token", "code id_token", "token id_token", "token"],
                                 | "scopes_supported": ["openid"],
                                 | "issuer": "https://sts.windows.net/574800fa-6081-4b51-a885-6fb509ee8647/",
                                 | "microsoft_multi_refresh_token": true,
                                 | "authorization_endpoint": "https://localhost:8080/574800fa-6081-4b51-a885-6fb509ee8647/oauth2/authorize"
                                 |}""".stripMargin

  val azureUser: XucUser    = getXucUser("jbond", "passwd")
  val keycloakUser: XucUser = getXucUser("oidcuser", "passwd")

  val xivoUser: XivoUser =
    XivoUser(
      1,
      None,
      None,
      "James",
      Some("Bond"),
      Some("jbond"),
      Some("passwd"),
      None,
      None
    )

  val oidcUser: XivoUser = XivoUser(
    1,
    None,
    None,
    "oidc",
    Some("user"),
    Some("oidcuser"),
    Some("passwd"),
    None,
    None
  )

  val xivoAuthAcls: Seq[String] = List("alias.ctiuser")
}

class OidcSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar
    with ScalaFutures
    with XucUserHelper {

  "Oidc" should {

    import OidcSampleToken._

    class OidcTestService extends MockitoSugar {
      implicit val system: ActorSystem = ActorSystem()
      val ws: WSClient                 = mock[WSClient]

      val mockRequest: WSRequest           = mock[WSRequest]
      val mockResponse: WSResponse         = mock[WSResponse]
      val config: XucBaseConfig            = mock[XucBaseConfig]
      val auth: config.Authentication.type = mock[config.Authentication.type]
      val repo: ConfigRepository           = mock[ConfigRepository]
      val xivoAuthentication: TestProbe    = TestProbe()
      implicit val xivoAuthRef: ActorRef   = xivoAuthentication.ref
      val timeProvider: TimeProvider       = mock[TimeProvider]
      val now: Long                        = new DateTime(2024, 1, 31, 16, 40, 0, 0).getMillis / 1000
      when(timeProvider.getJodaTime).thenReturn(now)
      when(timeProvider.getJavaClock).thenReturn(
        Clock.fixed(
          Instant.parse("2024-01-31T16:40:00Z"),
          ZoneOffset.UTC
        )
      )

      val oidcImpl: OidcImpl = new OidcImpl()(timeProvider)

      when(auth.expires).thenReturn(10000L)
      when(config.oidcEnable).thenReturn(true)
      when(config.oidcServerUrl).thenReturn(Some(testUrl))
      when(config.oidcClientId).thenReturn(Some(clientId))
      when(config.Authentication).thenReturn(auth)
      when(ws.url(any[String])).thenReturn(mockRequest)
      when(mockRequest.withQueryStringParameters(any[(String, String)]))
        .thenReturn(mockRequest)

      def mockResponseAndStatus(): OngoingStubbing[Int] = {
        when(mockRequest.get()).thenReturn(Future.successful(mockResponse))
        when(mockResponse.status).thenReturn(200)
      }

      def mockAzure(): OngoingStubbing[List[String]] = {
        when(repo.getCtiUser(azureUser.username)).thenReturn(Some(xivoUser))
        when(config.oidcAdditionalTrustedServersUrl).thenReturn(
          List(
            "https://sts.windows.net/574800fa-6081-4b51-a885-6fb509ee8647/"
          )
        )
        when(config.oidcAdditionalTrustedClientIDs).thenReturn(
          List("b84174f6-4073-45d2-9da0-7f8685cd0791")
        )
        when(config.oidcUsernameField).thenReturn(Some("name"))
        when(config.oidcAudience).thenReturn(
          List("00000003-0000-0000-c000-000000000000")
        )
      }

      def mockKeycloak(): OngoingStubbing[Option[String]] = {
        when(repo.getCtiUser(keycloakUser.username)).thenReturn(Some(oidcUser))
        when(config.oidcAdditionalTrustedServersUrl).thenReturn(
          List(
            "https://172.29.149.8:8443/realms/myrealm",
            "http://localhost:8080/myrealm",
            "https://www.foo.bar/someOtherRealm"
          )
        )
        when(config.oidcAdditionalTrustedClientIDs).thenReturn(
          List("xuc")
        )
        when(config.oidcAudience).thenReturn(List("xuc"))
        when(config.oidcUsernameField).thenReturn(None)
      }

    }

    "Azure ad" should {
      "allow Xuc authentication for a valid token" in new OidcTestService() {
        mockAzure()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetAzure))

        oidcImpl.authenticate(Some(azureToken), config, repo, ws, None, None)(
          xivoAuthentication.ref
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe xivoUser
            auth.login shouldBe "jbond"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe None
          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token in an alternate trusted issuer domain" in new OidcTestService() {
        mockAzure()
        when(config.oidcServerUrl).thenReturn(
          Some("https://some.unused.url/oauth")
        )
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetAzure))

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe xivoUser
            auth.login shouldBe "jbond"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe None

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with one audience" in new OidcTestService() {
        mockAzure()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetAzure))

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          Some(SoftwareType.webBrowser),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe xivoUser
            auth.login shouldBe "jbond"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.webBrowser)

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with an audience among multiple" in new OidcTestService() {
        mockAzure()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetAzure))
        when(config.oidcAudience).thenReturn(
          List(
            "someAudience",
            "xuc",
            "phone",
            "00000003-0000-0000-c000-000000000000"
          )
        )

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          Some(SoftwareType.mobile),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe xivoUser
            auth.login shouldBe "jbond"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.mobile)

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with a valid client id" in new OidcTestService() {
        mockAzure()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetAzure))
        when(config.oidcAudience).thenReturn(
          List(
            "00000003-0000-0000-c000-000000000000"
          )
        )
        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          Some(SoftwareType.mobile),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe xivoUser
            auth.login shouldBe "jbond"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.mobile)

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with a valid client id among multiples" in new OidcTestService() {
        mockAzure()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetAzure))
        when(config.oidcClientId).thenReturn(Some("wrongClientID"))
        when(config.oidcAdditionalTrustedClientIDs).thenReturn(
          List(
            "wrongClientID2",
            "wrongClientID3",
            "b84174f6-4073-45d2-9da0-7f8685cd0791"
          )
        )

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          Some(SoftwareType.mobile),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe xivoUser
            auth.login shouldBe "jbond"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.mobile)

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with a valid audience as client ID fallback" in new OidcTestService() {
        mockAzure()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetAzure))
        when(config.oidcClientId).thenReturn(Some(clientId))

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          Some(SoftwareType.mobile),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe xivoUser
            auth.login shouldBe "jbond"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.mobile)

          case _ => fail()
        }
      }

      "return a failure when oidc is not configured" in new OidcTestService() {
        when(config.oidcEnable).thenReturn(false)

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when access token is missing" in new OidcTestService() {

        oidcImpl.authenticate(None, config, repo, ws, None, None) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when the authorization server url is not configured" in new OidcTestService() {
        when(config.oidcServerUrl).thenReturn(None)

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when token is invalid due to missing required audience/clientId" in new OidcTestService() {
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))

        when(config.oidcClientId).thenReturn(Some("fakeAudience"))

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when token is invalid due to wrong issuer" in new OidcTestService() {
        mockResponseAndStatus()
        when(mockResponse.json).thenReturn(Json.parse(wellKnownResp))

        when(config.oidcServerUrl).thenReturn(Some("fakeIss"))

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when token is invalid due to XiVO user not found" in new OidcTestService() {
        mockAzure()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetAzure))

        when(repo.getCtiUser(azureUser.username)).thenReturn(None)

        oidcImpl.authenticate(
          Some(azureToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }
    }
    "Keycloak" should {
      "allow Xuc authentication for a valid token" in new OidcTestService() {
        mockKeycloak()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetKeycloak))

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          None,
          None
        )(
          xivoAuthentication.ref
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe oidcUser
            auth.login shouldBe "oidcuser"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe None
            verify(ws).url(
              "https://172.29.149.8:8443/realms/myrealm/.well-known/openid-configuration"
            )
          case e => println(e)
        }
      }

      "allow Xuc authentication for a valid token in an alternate trusted issuer domain" in new OidcTestService() {
        mockKeycloak()
        when(config.oidcServerUrl).thenReturn(
          Some("https://some.unused.url/oauth")
        )
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetKeycloak))

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe oidcUser
            auth.login shouldBe "oidcuser"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe None
            verify(ws).url(
              "https://172.29.149.8:8443/realms/myrealm/.well-known/openid-configuration"
            )

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with one audience" in new OidcTestService() {
        mockKeycloak()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetKeycloak))

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          Some(SoftwareType.webBrowser),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe oidcUser
            auth.login shouldBe "oidcuser"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.webBrowser)

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with an audience among multiple" in new OidcTestService() {
        mockKeycloak()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetKeycloak))
        when(config.oidcAudience).thenReturn(
          List(
            "someAudience",
            "xuc",
            "phone",
            "00000003-0000-0000-c000-000000000000"
          )
        )

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          Some(SoftwareType.mobile),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe oidcUser
            auth.login shouldBe "oidcuser"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.mobile)

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with a valid client id" in new OidcTestService() {
        mockKeycloak()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetKeycloak))
        when(config.oidcAudience).thenReturn(
          List(
            "xuc"
          )
        )
        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          Some(SoftwareType.mobile),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe oidcUser
            auth.login shouldBe "oidcuser"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.mobile)

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with a valid client id among multiples" in new OidcTestService() {
        mockKeycloak()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetKeycloak))
        when(config.oidcClientId).thenReturn(Some("wrongClientID"))
        when(config.oidcAdditionalTrustedClientIDs).thenReturn(
          List(
            "wrongClientID2",
            "wrongClientID3",
            "xuc"
          )
        )

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          Some(SoftwareType.mobile),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe oidcUser
            auth.login shouldBe "oidcuser"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.mobile)

          case _ => fail()
        }
      }

      "allow Xuc authentication for a valid token with a valid audience as client ID fallback" in new OidcTestService() {
        mockKeycloak()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetKeycloak))
        when(config.oidcClientId).thenReturn(Some(clientId))

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          Some(SoftwareType.mobile),
          None
        ) match {
          case Success(result) =>
            val (auth, user) = result
            auth shouldBe a[AuthenticationInformation]
            user shouldBe oidcUser
            auth.login shouldBe "oidcuser"
            auth.expiresAt should be <= now + config.Authentication.expires
            auth.issuedAt should be <= now
            auth.userType shouldBe "cti"
            auth.acls shouldBe xivoAuthAcls
            auth.softwareType shouldBe Some(SoftwareType.mobile)

          case _ => fail()
        }
      }

      "return a failure when oidc is not configured" in new OidcTestService() {
        when(config.oidcEnable).thenReturn(false)

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when access token is missing" in new OidcTestService() {

        oidcImpl.authenticate(None, config, repo, ws, None, None) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when the authorization server url is not configured" in new OidcTestService() {
        when(config.oidcServerUrl).thenReturn(None)

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when token is invalid due to missing required audience/clientId" in new OidcTestService() {
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))

        when(config.oidcClientId).thenReturn(Some("fakeAudience"))

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when token is invalid due to wrong issuer" in new OidcTestService() {
        mockResponseAndStatus()
        when(mockResponse.json).thenReturn(Json.parse(wellKnownResp))

        when(config.oidcServerUrl).thenReturn(Some("fakeIss"))

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }

      "return a failure when token is invalid due to XiVO user not found" in new OidcTestService() {
        mockKeycloak()
        mockResponseAndStatus()
        when(mockResponse.json)
          .thenReturn(Json.parse(wellKnownResp))
          .thenReturn(Json.parse(jwkSetKeycloak))

        when(repo.getCtiUser(keycloakUser.username)).thenReturn(None)

        oidcImpl.authenticate(
          Some(keycloakToken),
          config,
          repo,
          ws,
          None,
          None
        ) match {
          case Failure(result) => result shouldBe a[AuthenticationException]
          case _               => fail()
        }
      }
    }
  }
}
