package api

import org.apache.pekko.actor.ActorSystem
import org.mockito.Mockito._
import org.scalatest.Tag
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneServerPerSuite
import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.test.Helpers._
import play.api.test._
import services.config.ConfigRepository
import xuctest.BaseTest
import play.api.libs.ws.JsonBodyWritables.writeableOf_JsValue
import play.api.libs.ws.WSBodyWritables.writeableOf_JsValue
import play.api.libs.ws.writeableOf_JsValue
import scala.concurrent.duration.DurationInt
import scala.concurrent.{Await, Future}
import play.api.Application
import play.api.libs.ws.WSRequest
import scala.concurrent.duration.FiniteDuration

object WSApiSpecTest extends Tag("xuc.tags.WSApiSpecTest")

// For required configuration see ## Integration Tests in the /README.md file

class WsApiSpec extends BaseTest with GuiceOneServerPerSuite with MockitoSugar {
  val xivohost = "xivo-integration"

  val xucRespApiTimeout: FiniteDuration = 25.seconds

  implicit val system: ActorSystem = ActorSystem()

  val configRepositoryMock: ConfigRepository = mock[ConfigRepository]

  private def await[T](future: Future[T]) =
    Await.result(future, xucRespApiTimeout)

  override def fakeApplication(): Application =
    GuiceApplicationBuilder(configuration =
      Configuration.from(xivoIntegrationConfig)
    )
      .overrides(bind[ConfigRepository].to(configRepositoryMock))
      .build()

  def rootApi: String = s"http://localhost:$port/xuc/api/1.0/"

  def getClient(url: String)(implicit client: WSClient): WSRequest =
    client.url(url).withHttpHeaders(("Content-Type", "application/json"))

  def apiUrl(method: String, username: String)(implicit
      client: WSClient
  ): WSRequest =
    getClient(s"$rootApi$method/avencall.com/$username/")

  def apiUrlWithParameters(
      method: String,
      username: String,
      parameters: String
  )(implicit client: WSClient): WSRequest =
    getClient(s"$rootApi$method/avencall.com/$username?$parameters")

  def globalApiUrl(method: String)(implicit client: WSClient): WSRequest =
    getClient(s"$rootApi$method/")

  "Api" should {
    "be able to connect a user" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"
        val password = "0000"

        val data = Json.obj("password" -> password)

        await(apiUrl("connect", username).post(data)).status shouldBe OK
      }
    }

    "get error on connecting an unknown user" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "unknown"
        val password = "pwd"

        val data = Json.obj("password" -> password)

        await(
          apiUrl("connect", username).post(data)
        ).status shouldBe UNAUTHORIZED
      }
    }

    "toggle pause an agent in a number" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val number = "1000"
        val data   = Json.obj("phoneNumber" -> number)

        await(globalApiUrl("togglePause").post(data)).status shouldBe OK
      }

    }
    "be able to dnd on using a disconnected user" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"
        val password = "0000"

        val data = Json.obj("state" -> true)

        await(apiUrl("dnd", username).post(data)).status shouldBe OK
      }
    }
    "be able to dnd off " taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"

        val data = Json.obj("state" -> false)

        await(apiUrl("dnd", username).post(data)).status shouldBe OK
      }
    }

    "be able to inconditionnal forward a user" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"

        val data = Json.obj("state" -> true, "destination" -> "1102")

        await(apiUrl("uncForward", username).post(data)).status shouldBe OK
      }
    }

    "be able to remove inconditionnal forward a user" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"

        val data = Json.obj("state" -> false, "destination" -> "1102")

        await(apiUrl("uncForward", username).post(data)).status shouldBe OK
      }
    }

    "be able to forward on no answer a user" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"

        val data = Json.obj("state" -> true, "destination" -> "1255")

        await(apiUrl("naForward", username).post(data)).status shouldBe OK
      }
    }

    "be able to forward on busy a user" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"

        val data = Json.obj("state" -> true, "destination" -> "5546")

        await(apiUrl("busyForward", username).post(data)).status shouldBe OK
      }
    }

    "be able to dial" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"

        val data = Json.obj("number" -> "1102")

        await(apiUrl("dial", username).post(data)).status shouldBe OK
      }
    }

    "be able to dialFromQueue" taggedAs WSApiSpecTest in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"

        val data = Json.parse(
          """{"destination": "1102", "queueId": 1, "callerIdName": "Thomas", "callerIdNumber": "1234"}"""
        )

        await(apiUrl("dialFromQueue", username).post(data)).status shouldBe OK
      }
    }

    "be able to get the call history by size" in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"
        val size     = 10

        val query =
          apiUrlWithParameters("historyByUsername", username, s"size=$size")

        val result = await(query.get())

        result.status shouldBe 200
      }
    }

    "be able to get the call history by days" in {
      WsTestClient.withClient { implicit client =>
        val username = "bruce"
        val days     = 10

        val query =
          apiUrlWithParameters("historyByDays", username, s"days=$days")

        val result = await(query.get())

        result.status shouldBe 200
      }
    }
  }
}
