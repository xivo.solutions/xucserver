package helpers

import org.apache.pekko.actor.{ActorRef, ActorSystem}
import org.apache.pekko.cluster.pubsub.DistributedPubSubMediator.Publish
import org.apache.pekko.testkit.TestProbe

import scala.reflect.ClassTag

class MediatorHelperSpec(mediator: TestProbe) {
  
  def getMediatorRef: ActorRef = mediator.ref

  def expectOnMediator[T: ClassTag]: Option[T] = {
    var msgOpt: Option[T] = None
    mediator.fishForMessage() {
      case msg: T =>
        msgOpt = Some(msg)
        true
      case _ => false
    }
    msgOpt
  }

  def expectPublishOf[T: ClassTag]: Option[T] = {
    var msgOpt: Option[T] = None
    mediator.fishForMessage() {
      case Publish(_, msg: T, _) =>
        msgOpt = Some(msg)
        true
      case _ => false
    }
    msgOpt
  }

}
